<?php

/**
 *
 * Desktops/set_deleg.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<script type="text/javascript">
    function reloadDelegPlanifiee(id) {
        gui.request({
            url: "/desktops/setDeleg/" + id,
            updateElement: $('#divRD'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }

</script>
<div class="panel" id ="divSetDelege" >
    <div class="panel-default">
        <div class ="panel-heading">
            <h4 class="panel-title">
                <?php echo __d('desktop', 'Desktop.Desktop') . ' : ' . $desktopName; ?>
            </h4>
        </div>
        <div class="panel-body">
            <div class="col-sm-5">
                <div class="delegEnCours">
                    <legend><?php echo __d('desktop', 'Desktop.deleg'); ?></legend>
                    <?php
                        if (!empty($userDesktopNameDelegated)) {
                            $ulContent = '';
                            foreach ($userDesktopNameDelegated as $desktopuser) {
                                $spanUnset = "";
                                $sameUser = ( $desktopuser['User']['id'] != $userSessionId) ? true : false;
                                $sameProfil = ( $userProfilSessionId == ADMIN_GID ) ? true : false;
                                $urlDelete = '/desktops/setDeleg/'. $desktopId . '/' . $desktopuser['User']['id'] . '/1';

                                if( $isAdmin || $sameUser || $sameProfil || in_array($desktopuser['Desktop']['id'], $baseDesktops) ) {
									$spanUnset = "<span class='removeDelegeEnCours'><a href=$urlDelete><i class='btn fa fa-trash' aria-hidden='true' style='color:#E51809;' alt='Supprimer la délégation' title='Supprimer la délégation'></i> </a></span>";
								}
                                $ulContent .= $this->Html->tag('li', $desktopuser['User']['nom'].' '.$desktopuser['User']['prenom'] . $spanUnset, array('class' => 'delegateToUser'));
                            }
                            echo  $this->Html->tag('ul', $ulContent, array('class' => 'delegateToUserList'));
                        } else {
                            echo  $this->Html->tag('div', __d('desktop', 'Desktop.deleg.void'), array('class' => 'alert alert-warning'));
                        }
                    ?>
                </div>
                <div class="delegPlan">
                    <legend><?php echo __d('desktop', 'Desktop.deleg.plan'); ?></legend>
                    <?php
                    $time = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
                    if (!empty($plandelegations)) {
                        $ulContent = '';
                        foreach ($plandelegations as $plandelegation) {

                            $spanUnset = '';
                            $sameUser = ( $plandelegation['User']['id'] != $userSessionId ) ? true : false;
                            $sameProfil = ( $userProfilSessionId == ADMIN_GID ) ? true : false;
                            $tmp = explode(' ', $plandelegation['Plandelegation']['date_start']);
                            $tmp2 = explode('-', $tmp[0]);
                            $start = mktime(0, 0, 0, $tmp2[1], $tmp2[2], $tmp2[0]);
                            $urlPlanDelete = '/desktops/deleteDeleg/' . $plandelegation['Plandelegation']['id'];
                            $urlPlanEdit = '/desktops/editDeleg/' . $desktopId . '/' . $plandelegation['Plandelegation']['user_id'];
                            if (intval($start) - intval($time) > 0 ) {
                                if ( $sameUser || $sameProfil || in_array($plandelegation['Desktop']['id'], $baseDesktops)) {
                                // Suppression de la délégation supprimée
                                    $spanUnset .= "<span class='deleteDeleg'><a href=$urlPlanDelete><i class='btn fa fa-trash' aria-hidden='true' style='color:#E51809;' alt='Supprimer la délégation' title='Supprimer la délégation'></i> </a></span>";
                                    $spanUnset .= "<span class='editDeleg'><a href=$urlPlanEdit><i class='btn fa fa-pencil' aria-hidden='true' style='color:#5397a7;' alt='Modifier la délégation' title='Modifier la délégation'></i> </a></span>";
                                }
                            } else {
                                $tmp = explode(' ', $plandelegation['Plandelegation']['date_end']);
                                $tmp2 = explode('-', $tmp[0]);
                                $end = mktime(0, 0, 0, $tmp2[1], $tmp2[2], $tmp2[0]);

                                if (intval($end) - intval($time) < 0) {
                                   $spanUnset .= "<span class='delegation_fullend'><i class='btn fa fa-check' style='color:#2ec07e;'aria-hidden='true' alt='Délégation terminée' title='Délégation terminée'></i> </span>";
                               } else {
                                    $spanUnset .= "<span class='deleteDeleg'><a href=$urlPlanDelete><i class='btn fa fa-trash' aria-hidden='true' style='color:#E51809;' alt='Supprimer la délégation' title='Supprimer la délégation'></i> </a></span>";
                                    $spanUnset .= "<span class='editDeleg'><a href=$urlPlanEdit><i class='btn fa fa-pencil' aria-hidden='true' style='color:#5397a7;' alt='Modifier la délégation' title='Modifier la délégation'></i> </a></span>";
                               }
                           }
                            $liContent = $this->Html->tag('div', $plandelegation['User']['nom'].' '.$plandelegation['User']['prenom'] . $spanUnset);
                            $liContent .= $this->Html->tag('div', '(' . $this->Html2->ukToFrenchDateWithSlashes($plandelegation['Plandelegation']['date_start']) . ' - ' . $this->Html2->ukToFrenchDateWithSlashes($plandelegation['Plandelegation']['date_end']) . ')');
                            $ulContent .= $this->Html->tag('li', $liContent, array('class' => 'delegateToUser'));
                        }
                        echo  $this->Html->tag('ul', $ulContent, array('class' => 'delegateToUserList'));
                    } else {
                        echo $this->Html->tag('div', __d('desktop', 'Desktop.deleg.plan.void'), array('class' => 'alert alert-warning'));
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-6">
                <legend><?php echo __d('desktop', 'Desktop.deleg.add'); ?></legend>
                    <?php
                    $formEditDeleg = array(
                        'name' => 'DesktopsUser',
                        'label_w' => 'col-sm-4',
                        'input_w' => 'col-sm-8',
                        'form_url' => array( 'controller' => 'desktops', 'action' => 'setDeleg',$desktopId),
                        'input' => array(
                            'DesktopsUser.desktop_id' => array(
                                'inputType' => 'hidden',
                                'items'=>array(
                                    'type'=>'hidden',
                                    'value'=>$desktopId
                                )
                            ),
                            'DesktopsUser.user_id' => array(
                                'labelText' => __d('user', 'Utilisateur'),
                                'inputType' => 'select',
                                'items'=>array(
                                    'type'=>'select',
                                    'options' => $chooseusers,
                                    'empty' => true,
                                    'required' => true
                                )
                            ),
                            'DesktopsUser.delegation' => array(
                                'inputType' => 'hidden',
                                'items'=>array(
                                    'type'=>'hidden',
                                    'value'=>true
                                )
                            ),
                            'DesktopsUser.planification' => array(
                                'labelText' =>__d('desktop', 'Desktop.delegation.planification.active'),
                                'inputType' => 'checkbox',
                                'items'=>array(
                                    'type'=>'checkbox'
                                )
                            )
                        )
                    );
                    $formEditDelegPlannifier = array(
                        'name' => 'Plandelegation',
                        'label_w' => 'col-sm-4',
                        'input_w' => 'col-sm-6',
                        'form_url' => array( 'controller' => 'desktops', 'action' => 'setDeleg',$desktopId),
                        'input' => array(
                            'Plandelegation.date_start' => array(
                                'labelText' =>__d('desktop', 'Desktop.deleg.datestart'),
                                'inputType' => 'text',
                                'dateInput' =>true,
                                'items'=>array(
                                    'type'=>'text',
									'required' => true
                                )
                            ),
                            'Plandelegation.date_end' => array(
                                'labelText' =>__d('desktop', 'Desktop.deleg.dateend'),
                                'inputType' => 'text',
                                'dateInput' =>true,
                                'items'=>array(
                                    'type'=>'text',
									'required' => true
                                )
                            )
                        )
                    );
                ?>
                <div class="newDeleg">
                    <?php echo $this->Formulaire->createForm($formEditDeleg); ?>
                    <div class="infoPlan" id="fieldsetPlan">
                        <?php echo $this->Formulaire->createForm($formEditDelegPlannifier); ?>
                    </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
        <?php if(isset($admin) && $admin):?>
        <div class="panel-footer controls delegFooter  " role="group"></div>
        <?php endif ?>
    </div>
</div>

<script type="text/javascript">
    function showPlanification() {
        if ($('#DesktopsUserPlanification').prop("checked")) {
            $('#fieldsetPlan').css("display", "block");
        } else {
            $('#fieldsetPlan').css("display", "none");
        }
    }

    $('.form_datetime input').datepicker({
        language: 'fr-FR',
        format: "dd/mm/yyyy",
        weekStart: 1,
        autoclose: true,
        todayBtn: 'linked'
    });

    gui.addbuttons({
        element: $('.panel-footer.delegFooter'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse",
                action: function () {
                    $("#divSetDelege").remove();
                    $("#habilsPerson").show();
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success",
                action: function () {
                    if (form_validate($('#DesktopsUserSetDelegForm'))) {
                    	var planified = $('#DesktopsUserPlanification').attr('checked');
                    	var planifieddatestart = $('#PlandelegationDateStart').val();
                    	var planifieddateend = $('#PlandelegationDateEnd').val();

						if( ( planified == 'checked' && planifieddatestart != '' && planifieddateend != '' ) || planified == undefined) {
							var updateElement = $('.divRD');
							if (updateElement.length == 0) {
								updateElement = $('.ui-dialog-content');
							}
							var url = $('#DesktopsUserSetDelegForm').attr('action');
							gui.request({
								url: url,
								data: $('#DesktopsUserSetDelegForm').serialize(),
								loader: true,
								updateElement: updateElement,
								loaderMessage: gui.loaderMessage
							}, function (data) {
								getJsonResponse(data);
								gui.request({
									url: url,
									loader: true,
									updateElement: updateElement,
									loaderMessage: gui.loaderMessage
								}, function (data) {
									updateElement.html(data);
									reloadDelegDependances();
								});
							});
							$("#divSetDelege").remove();
							$("#habilsPerson").show();
						}
                    	else {
							swal({
								showCloseButton: true,
								title: "Oops...",
								text: "Veuillez vérifier votre formulaire!",
								type: "error",

							});
						}
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });

    $('#DesktopsUserSetDelegForm input[type=submit]').remove();
    $('.delegateToUser span.removeDelegeEnCours').each(function () {
        var container = $(this);
        var link = $('a', container);
        var url = link.attr('href');
        var img = $('i', container);

        container.append(img);
        link.remove();

        var updateElement = $('.container');
        if (updateElement.length == 0) {
            updateElement = $('.modal-body');
        }
        img.button().click(function () {
            swal({
                showCloseButton: true,
                title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler'
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: url,
                        loader: true,
                        updateElement: updateElement,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        <?php if(isset($admin) && $admin){ ?>
                        loadHabilis();
                        $("#habilsPerson").show();
                        <?php }else{ ?>
                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/users/getInfosCompte"; ?>";
                        <?php } ?>
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
            $('.swal2-cancel').css('border', 'solid 1px');
            $('.swal2-cancel').hover(function () {
                $('.swal2-cancel').css('background-color', '#5397a7');
                $('.swal2-cancel').css('color', 'white');
                $('.swal2-cancel').css('border-color', '#5397a7');
            }, function () {
                $('.swal2-cancel').css('background-color', 'transparent');
                $('.swal2-cancel').css('color', '#5397a7');
                $('.swal2-cancel').css('border-color', '#3C7582');
            });
        });
    });

    $('.delegateToUser span.deleteDeleg').each(function () {
        var container = $(this);
        var link = $('a', container);
        var url = link.attr('href');
        var img = $('i', container);
        container.append(img);
        link.remove();


        var updateElement = $('.container');
        if (updateElement.length == 0) {
            updateElement = $('.modal-body');
        }
        img.button().click(function () {
            swal({
                showCloseButton: true,
                title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: url,
                        loader: true,
                        updateElement: updateElement,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        <?php if(isset($admin) && $admin){ ?>
                        loadHabilis();
                        $("#habilsPerson").show();
                        <?php }else{ ?>
                            window.location.href = "<?php echo Configure::read('BaseUrl') . "/users/getInfosCompte"; ?>";
                        <?php } ?>
                    });
                } else {
                    swal({
                        showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n'avez pas supprimé, ;) .",
                        type: "error",

                    });
                }
            });

            $('.swal2-cancel').css('margin-left', '-320px');
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
            $('.swal2-cancel').css('border', 'solid 1px');
            $('.swal2-cancel').hover(function () {
                $('.swal2-cancel').css('background-color', '#5397a7');
                $('.swal2-cancel').css('color', 'white');
                $('.swal2-cancel').css('border-color', '#5397a7');
            }, function () {
                $('.swal2-cancel').css('background-color', 'transparent');
                $('.swal2-cancel').css('color', '#5397a7');
                $('.swal2-cancel').css('border-color', '#3C7582');
            });
        });
    });

    $('#DesktopsUserPlanification').change(function () {
        showPlanification();
    })

    showPlanification();

    $('#DesktopsUserUserId').select2();



    $('.delegateToUser span.editDeleg').each(function () {

        var container = $(this);
        var link = $('a', container);
        var url = link.attr('href');
        var img = $('i', container);

        container.append(img);
        link.remove();

        <?php if(isset($admin) && $admin){ ?>
        url = url + "/admin";
        <?php } ?>

        var updateElement = $('.divRD');
        if (updateElement.length == 0) {
            updateElement = $('.ui-dialog-content');
        }
        img.button().click(function () {
            var form = $(this).parents('.modal').find('form');
            gui.formMessage({
                updateElement: $('body'),
                loader: true,
                width: '550px',
                loaderMessage: gui.loaderMessage,
                title: 'Modification de la délégation',
//                data: $(form).serialize(),
                url: url,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var url = '/desktops/editDeleg/<?php echo $desktopId; ?>/<?php echo $userId; ?>';
                        if (form_validate($('#PlandelegationEditDelegForm'))) {
                            gui.request({
                                url: url,
                                data: $('#PlandelegationEditDelegForm').serialize(),
                                loaderElement: $('#PlandelegationEditDelegForm'),
                                loaderMessage: gui.loaderMessage
                            }, function (data) {
                                gui.request({
                                    url: '/desktops/setDeleg/<?php echo $desktopId; ?>',
                                    loader: true,
                                    updateElement: $('.delegPlan' ),
                                }, function (data) {
                                    layer.msg('Les informations ont été enregistrées', {});
                                    $('.modal-content #edit_deleg_footer').remove();
//                                    $('.modal-body').remove();
                                    $('.modal-footer').show();
                                    $('.model-body' ).html(data);
                                });
                            });
                            $(this).parents('.modal').modal('hide');
                            $(this).parents('.modal').empty();
                        } else {
                            swal({
                                showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
            });
        });
    });

</script>
