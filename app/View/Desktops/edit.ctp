<?php

/**
 *
 * Desktops/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="panel panel-default zone-form" id="editRole">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo __d('desktop', 'Desktop.edit'); ?></h4>
    </div>
    <div class="panel-body">
        <?php
        $formDesktops = array(
            'name' => 'Desktop',
            'label_w' => 'col-sm-3',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Desktop.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'Desktop.profil_id' => array(
                    'labelText' =>__d('desktop', 'Desktop.profil_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'required'=>true,
                        'type'=>'select',
                        'empty' => true,
                        'options' => $groups
                    )
                ),
                'Desktop.name' => array(
                    'labelText' =>__d('desktop', 'Desktop.name'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text',
                        'required'=>true/*,
                        'pattern'=>"^[_A-z0-9 ]{1,}$",
                        'data-error' => "Les caractères suivants ne sont pas autorisés (/[](){},$\_*+.]/' )"*/
                    )
                ),
                'Service.Service' => array(
                    'labelText' => 'services*',
                    'inputType' => 'select',
                    'items'=>array(
                        'type' => 'select',
                        'required'=>true,
                        'escape' => false,
                        'options' => $services,
                        'multiple' => true,
                        'default' => false,
                        'multiple' => 'multiple',
                        'class' => 'selectMultiple',
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formDesktops);
        echo $this->Form->end();
        ?>
    </div>
    <div class="panel-footer controls " role="group">
        <a class="btn btn-danger-webgfc btn-inverse " id="cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn btn-success " id="saveEditRole"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script>
    // Mise en palce d'un select plus clair
    $('#ServiceService').select2({allowClear: true, placeholder: "Sélectionner un service"});
    $('#DesktopProfilId').select2({allowClear: true, placeholder: "Sélectionner un groupe"});
    var selectedOptionHtml = $('#DesktopProfilId option:selected').html();
    $('#DesktopProfilId').change(function () {
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('desktop', 'Desktop.changeProfil.title'); ?>",
            text: "<?php echo __d('desktop', 'Desktop.changeProfil.msg'); ?>",
            type: "warning",

        });
        var newSelectedOptionHtml = $('#DesktopProfilId option:selected').html();
        $('#DesktopName').val($('#DesktopName').val().replace(selectedOptionHtml, newSelectedOptionHtml));
        selectedOptionHtml = newSelectedOptionHtml;
    }).blur(function () {
        if ($('#DesktopName').val() == '' && $('#DesktopProfilId').val() != '') {
            $('#DesktopName').val('<?php echo __d('desktop', 'Desktop.sccsq'); ?> ' + $('#DesktopProfilId option:selected').html());
            checkDesktopNameUnique($('#DesktopEditForm'));
        }
    });

    $('#DesktopName').blur(function () {
        checkDesktopNameUnique($('#DesktopEditForm'));
    });

    $('#saveEditRole').click(function () {
        if ($("#DesktopEditForm .errorJsonFormCheck").length == 0) {
            var id = <?php echo $id;?>;
            if (form_validate($('#DesktopEditForm'))) {
                gui.request({
                    url: '/desktops/edit/' + id,
                    data: $('#DesktopEditForm').serialize(),
                    loaderElement: $('.ui-dialog-content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    getJsonResponse(data);
                        $('.ui-dialog-content').html(data);
                        $('.loader').remove();
//                    gui.request({
//                        url: '/users/habilPerson/' + id,
//                        loader: false
//                    }, function (data) {
//                        $('.ui-dialog-content').html(data);
//                        $('.loader').remove();
//                    });
                });
                loadHabilis();
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        }
    });

    $('#cancel').click(function () {
        $("#editRole").remove();
        $("#habilsPerson").show();
    });


</script>
