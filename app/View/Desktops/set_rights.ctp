<?php

/**
 *
 * Desktops/set_rights.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$options = array(
    'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('alt' => 'no')),
    'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('alt' => 'yes')),
//    'parentNodeMarkOpen' => $this->Html->image('/DataAcl/img/parent_node_mark_close.png', array( 'alt' => 'closed')),
//    'parentNodeMarkClose' => $this->Html->image('/DataAcl/img/parent_node_mark_open.png', array( 'alt' => 'opened')),
    'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'edit' => true,
    'tableRootClass' => '',
    'legendClass' => '',
    'fieldsetClass' => '',
    'fieldsetStyle' => '',
    'legendStyle' => '',
    'requester' => $requester
);

if ($mode == 'func') {
    if (!$editableProfil){
        echo $this->Html->tag('div', __d('profil', 'Profil.notEditable.txt'), array('class' => 'flushRightsTab'));
    }
    if (Configure::read('debug') >= 2) {
    }
    if ($rights['Right']['mode_classique']) {
        echo $this->Html->div('alert alert-warning', __d('right', 'mode_classique.active') . __d('right', 'mode_classique.active.edit'));
    }
    echo $this->Element('rights', array('rights' => $rights, 'tabsForSets' => $tabsForSets, 'edit' => $editableProfil, 'url' => Router::url(array('controller' => 'desktops', 'action' => 'setRights'))));
} else if ($mode == 'func_classique') {
    echo $this->Html->tag('span', __d('right', 'mode_simplifie'), array('class' => 'modeSimplifieBttn btn btn-info-webgfc'));
    //show func rigths
    $options['fields'] = array(
        'ressource' => 'Fonctions',
        'read' => 'accès'
    );
    $options['fieldsetLegend'] = 'Fonctions';
    $options['aclType'] = 'Acl';
    $options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_rights';
    $this->DataAcl->drawTable($rights[0], true, $options);
} else if ($mode == 'data') {
    //show data rigths
    $options['fields'] = array(
        'ressource' => __d('typesoustype', 'Types/Soustypes'),
        'read' => 'accès'
    );
    $options['aclType'] = 'DataAcl';
    foreach ($dataRights as $desktopService => $rights) {
        $requester = array('model' => 'DesktopsService', 'foreign_key' => $desktopService);
        $options['requester'] = $requester;
        $options['fieldsetLegend'] = 'Service : ' . $servicesNames[$desktopService];
        $options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_data';
        $this->DataAcl->drawTable($rights, false, $options);
    }

} else if ($mode == 'meta') {
    //show meta rigths
    $options['fields'] = array(
        'ressource' => 'Metadonnees',
        'create' => 'création',
        'read' => 'lecture',
        'update' => 'édition',
        'delete' => 'suppression'
    );
    $options['aclType'] = 'DataAcl';
    foreach ($metaRights as $desktopService => $rights) {
        $requester = array('model' => 'DesktopsService', 'foreign_key' => $desktopService);
        $options['requester'] = $requester;
        $options['fieldsetLegend'] = 'Service : ' . $servicesNames[$desktopService];
        $options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_meta';
        $this->DataAcl->drawTable($rights, false, $options);
    }
}
?>

<script type="text/javascript">
    initTables(true);
    <?php if ($mode == 'func' && Configure::read('debug') >= 2) { ?>
    $('.modeClassiqueBttn').button().click(function () {
        var cpt = 0;
        $('#rightsTabs a').each(function () {
            if (cpt == 2) {
                var href = $.data(this, 'href.tabs');
                href = href.replace('func', 'func_classique');
                $('#rightsTabs').tabs('url', cpt, href);
            }
            cpt++;
        });
        $('#rightsTabs').tabs('load', 2);
    });
    <?php } else if ($mode == 'func_classique') { ?>
    $('.modeSimplifieBttn').button().click(function () {
        var cpt = 0;
        $('#rightsTabs a').each(function () {
            if (cpt == 2) {
                var href = $.data(this, 'href.tabs');
                href = href.replace('func_classique', 'func');
                $('#rightsTabs').tabs('url', cpt, href);
            }
            cpt++;
        });
        $('#rightsTabs').tabs('load', 2);
    });
    <?php } ?>
    <?php
        $btnConfigContainerId = "";
        if (isset($options['formName'])) {
                $btnConfigContainerId = '#' . $options['formName'] . 'SetRightsForm ';
        }
    ?>
    $('<?php echo $btnConfigContainerId; ?>.submit').addClass('panel-footer ').css('width', '100%').each(function () {
        $('input[type=submit]', this).remove();
        var span_cancel = $('<span></span>').button().html("<?php echo __d('default', 'Button.cancel'); ?>").addClass('btn btn-danger-webgfc btn-inverse ').click(function () {
            $(".editRD").remove();
            $("#habilsPerson").show();
        });
        var span = $('<span></span>').button().html("<?php echo __d('default', 'Button.submit'); ?>").addClass('btn btn-success ').click(function () {
            var form = $('form', $(this).parents('.active .in'));
//            var selectedTab = $(this).parents('#rightsEditTabs').tabs('option', 'selected');
            var selectedTab = $('#divEditRD');
            gui.request({
                url: form.attr('action'),
                data: form.serialize(),
                loader: true,
                updateElement: form,
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $(".editRD").remove();
                $("#habilsPerson").show();
                getJsonResponse(data);
                $('#rightsEditTabs').tabs('load', selectedTab);
            });
        });
        $(this).append(span_cancel);
        $(this).append(span);
    });
<?php
if ($mode == 'meta') {
    foreach ($metaRights as $desktopService => $rights) {
?>
    $('#<?php echo $options['formName']; ?>SetRightsForm table tr').each(function () {
        var firstChk = $('td:nth-child(2) input[type="checkbox"]', this);
        var secondChk = $('td:nth-child(3) input[type="checkbox"]', this);
        if (secondChk.prop("checked")) {
            firstChk.prop('checked', true);
        }
        if (typeof firstChk.attr('checked') == "undefined") {
            secondChk.prop('checked', false);
        }
        secondChk.change(function () {
            if ($(this).prop("checked")) {
                firstChk.prop('checked', true);
            }
        });
        firstChk.change(function () {
            if (typeof $(this).attr('checked') == "undefined") {
                secondChk.prop('checked', false);
            }
        });
    });
// Ajout d'un clic bouton sur la colonne lecture et édition pour tout cocher /décocher
    $('#<?php echo $options['formName']; ?>SetRightsForm table th').css({cursor: 'pointer'});
    $('#<?php echo $options['formName']; ?>SetRightsForm table th').click(function () {
        <?php $foreignKey = 0;
        foreach( $rights as $key => $value ):?>
            <?php $foreignKey = $value['foreign_key'];?>
        if ($('#<?php echo $options['formName']; ?>SetRightsForm table th')[1].innerHTML == 'lecture') {
            if ($("#RightsMetadonnee<?php echo $foreignKey;?>Read").attr("state") == "checked") {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").prop("checked", false);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").attr("state", "unchecked");
            }
            else {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").prop("checked", true);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Read").attr("state", "checked");
            }
        }
        if ($('#<?php echo $options['formName']; ?>SetRightsForm table th')[2].innerHTML == 'édition') {

            if ($("#RightsMetadonnee<?php echo $foreignKey;?>Update").attr("state") == "checked") {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").prop("checked", false);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").attr("state", "unchecked");
            }
            else {
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").prop("checked", true);
                $("#RightsMetadonnee<?php echo $foreignKey;?>Update").attr("state", "checked");
            }
        }
        <?php endforeach;?>
    });
<?php
    }
}
?>
<?php
    if ($mode == 'data') {
        foreach ($dataRights as $desktopServiceType => $rightsData) {
?>
    $('#<?php echo $options['formName']; ?>SetRightsForm table th').css({cursor: 'pointer'});
    $('#<?php echo $options['formName']; ?>SetRightsForm table th').click(function () {
            <?php
            $foreignKey = 0;
            foreach( $rightsData as $key => $value ):?>
                <?php $foreignKey = $value['foreign_key'];?>
        if ($("#RightsType<?php echo $foreignKey;?>Read").prop("checked")) {
            $("#RightsType<?php echo $foreignKey;?>Read").prop("checked", false);
            $("#RightsType<?php echo $foreignKey;?>Read").attr("state", "unchecked");
        } else {
            $("#RightsType<?php echo $foreignKey;?>Read").prop("checked", true);
            $("#RightsType<?php echo $foreignKey;?>Read").attr("state", "checked");
        }
                <?php if(isset($value['children']) && !empty($value['children'])) :?>
                    <?php foreach( $value['children'] as $keyChildren => $valueChildren ) :?>
                        <?php $foreignKeyChildren = $valueChildren['foreign_key'];?>
        if ($("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked")) {
            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", false);
            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "unchecked");
        }
        else {
            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", true);
            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "checked");
        }
                    <?php endforeach;?>
                <?php endif;?>
            <?php endforeach;?>
    });
    <?php $foreignKey = 0;
        foreach( $rightsData as $key => $value ):?>
            <?php $foreignKey = $value['foreign_key'];?>
            $("#RightsType<?php echo $foreignKey;?>Read").click(function () {
                <?php if(isset($value['children']) && !empty($value['children'])) :?>
                    <?php foreach( $value['children'] as $keyChildren => $valueChildren ) :?>
                        <?php $foreignKeyChildren = $valueChildren['foreign_key'];?>
                        if ($("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked")) {
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", false);
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "unchecked");
                        }
                        else {
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", true);
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "checked");
                        }
                    <?php endforeach;?>
                <?php endif;?>
            });


    <?php endforeach;?>
<?php
        }
    }
?>
</script>
