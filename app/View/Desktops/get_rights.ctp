<?php

/**
 *
 * Desktops/get_rights.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$options = array(
    'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('alt' => 'no')),
    'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('alt' => 'yes')),
//    'parentNodeMarkOpen' => $this->Html->image('/DataAcl/img/parent_node_mark_close.png', array('alt' => 'closed')),
//    'parentNodeMarkClose' => $this->Html->image('/DataAcl/img/parent_node_mark_open.png', array('alt' => 'opened')),
    'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'edit' => false,
    'requester' => $requester
);

if ($mode == 'func') {
    echo $this->Element('rights', array('rights' => $rights, 'edit' => false));
} else if ($mode == 'data') {
    //show data rigths
    $options['aclType'] = 'DataAcl';
    foreach ($dataRights as $desktopAgent => $rights) {
        $requester = array('model' => 'DesktopsAgent', 'foreign_key' => $desktopAgent);
        $options['requester'] = $requester;
        $options['fieldsetLegend'] = 'DataAcl: Types/Soustypes - DesktopsAgent ' . $desktopAgent;
        $options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_data';
        $this->DataAcl->drawTable($rights, false, $options);
    }
} else if ($mode == 'meta') {
    //show meta rigths
    $options['fields'] = array(
        'create' => 'création',
        'read' => 'lecture',
        'update' => 'édition',
        'delete' => 'suppression'
    );
    $options['aclType'] = 'DataAcl';
    foreach ($metaRights as $desktopAgent => $rights) {
        $requester = array('model' => 'DesktopsAgent', 'foreign_key' => $desktopAgent);
        $options['requester'] = $requester;
        $options['fieldsetLegend'] = 'DataAcl: Metadonnees - DesktopsAgent ' . $desktopAgent;
        $options['formName'] = $requester['model'] . '_' . $requester['foreign_key'] . '_meta';
        $this->DataAcl->drawTable($rights, false, $options);
    }
}
?>

<script type="text/javascript">
    initTables(true);
</script>
