<?php

/**
 *
 * Rmodels/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
if (empty($message)) {
?>
<div class="panel panel-default" id="rmodel_add">
    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
        Ajouter un modèle
    </div>
    <div class="panel-body">
<?php
        $formRmodel = array(
            'name' => 'Rmodel',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-5',
            'form_url' => array('controller' => 'rmodels', 'action' => 'add'),
            'form_target' =>'modelUploadFrame',
            'form_type' =>'file',
            'input' => array(
                'Rmodel.soustype_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden',
                        'value'=>$soustype_id
                    )
                ),
                'Rmodel.name' => array(
                    'labelText' =>__d('rmodel', 'Rmodel.name'),
                    'inputType' => 'text',
                    'items'=>array(
						'required' => true,
                        'type'=>'text'
                    )
                ),
                'Rmodel.file' => array(
                    'labelText' =>__d('rmodel', 'Rmodel.file'),
                    'inputType' => 'file',
                    'items'=>array(
						'required' => true,
                        'type' => 'file',
                        'name' => 'myfile',
                        'class' => 'fileField'
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formRmodel);
        echo $this->Form->end();

?>
    </div>
    <div class="panel-footer controls " role="group">

    </div>
</div>
<script>
    gui.addbuttons({
        element: $('#rmodel_add .panel-footer'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#rmodel_add').remove();
                    gui.request({
                        url: "<?php echo "/Soustypes/setRmodels/" . $soustype_id; ?>",
                        updateElement: $('#stype_tabs_3'),
                    });
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var formrmodel = $('#RmodelAddForm');
                    if (form_validate(formrmodel)) {
                        formrmodel.submit();
                        $('#rmodel_add').remove();
                        gui.request({
                            url: "<?php echo "/Soustypes/setRmodels/" . $soustype_id; ?>",
                            updateElement: $('#stype_tabs_3'),
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });
    $("#modelUploadFrame").remove();
    $('body').append('<iframe name="modelUploadFrame" id="modelUploadFrame" src="#" style="width:0;height:0;border:none;"></iframe>');

</script>
<?php } else { ?>
<script type="text/javascript">
<?php if (!$create) { ?>
<?php } ?>
    layer.msg("<div class='info'><?php echo $message; ?></div>", {offset: '70px'});
    window.top.window.stype_tabs_reload();
</script>
<?php } ?>

