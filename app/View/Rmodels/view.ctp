<?php
/**
 *
 * Modeldocs/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<h2><?php echo __d('model', 'Model'); ?></h2>
<div>
    <iframe
        src="<?php echo Configure::read('BaseUrl') . '/files/models/' . $model['Model']['name']; ?>"
        width="500" height="380" align="middle">
	</iframe>
</div>
<br/>
<br/>
<div class="submit"></div>
