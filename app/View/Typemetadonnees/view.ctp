<?php
/**
 *
 * Typemetadonnees/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="typemetadonnees view">
	<h2><?php __('Typemetadonnee'); ?></h2>
	<ul class="actions">
		<li><?php echo $this->Html->link(__('Edit Typemetadonnee', true), array('action' => 'edit', $typemetadonnee['Typemetadonnee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete Typemetadonnee', true), array('action' => 'delete', $typemetadonnee['Typemetadonnee']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $typemetadonnee['Typemetadonnee']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('New Typemetadonnee', true), array('action' => 'add')); ?> </li>
	</ul>

	<dl><?php $i = 0;
$class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('typemetadonnee', 'Typemetadonnee.id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $typemetadonnee['Typemetadonnee']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('typemetadonnee', 'Typemetadonnee.name'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $typemetadonnee['Typemetadonnee']['name']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('typemetadonnee', 'Typemetadonnee.created'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $typemetadonnee['Typemetadonnee']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('typemetadonnee', 'Typemetadonnee.modified'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $typemetadonnee['Typemetadonnee']['modified']; ?>
			&nbsp;
		</dd>
	</dl>

	<div class="related">
		<h3><?php __('Related Metadonnees'); ?></h3>
		<?php if (!empty($typemetadonnee['Metadonnee'])): ?>
			<table cellpadding = "0" cellspacing = "0">
				<tr>
					<th><?php __('Id'); ?></th>
					<th><?php __('Name'); ?></th>
					<th><?php __('Typemetadonnee Id'); ?></th>
					<th><?php __('Created'); ?></th>
					<th><?php __('Modified'); ?></th>
					<th class="actions"><?php echo __d('default', 'Actions'); ?></th>
				</tr>
				<?php
				$i = 0;
				foreach ($typemetadonnee['Metadonnee'] as $metadonnee):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class; ?>>
						<td><?php echo $metadonnee['id']; ?></td>
						<td><?php echo $metadonnee['name']; ?></td>
						<td><?php echo $metadonnee['typemetadonnee_id']; ?></td>
						<td><?php echo $metadonnee['created']; ?></td>
						<td><?php echo $metadonnee['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('View', true), array('controller' => 'metadonnees', 'action' => 'view', $metadonnee['id'])); ?>
							<?php echo $this->Html->link(__('Edit', true), array('controller' => 'metadonnees', 'action' => 'edit', $metadonnee['id'])); ?>
							<?php echo $this->Html->link(__('Delete', true), array('controller' => 'metadonnees', 'action' => 'delete', $metadonnee['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $metadonnee['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>

		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Metadonnee', true), array('controller' => 'metadonnees', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>
</div>
