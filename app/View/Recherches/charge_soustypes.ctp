<?php

/**
 *
 * Recherches/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$option = $this->Html->tag('option','',array('class'=>'item'));
if(!empty($resultats)){
    foreach ($resultats as $key => $value) {
        $option.= $this->Html->tag('option',$value,array('value'=>$key,'class'=>'item','sid'=>$key));
    }
}
echo $option;
?>
<script>
    if ($('#TypeTypeList li').length == 0) {
        typeChoix = new Array();
    }
</script>



