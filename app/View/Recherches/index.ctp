<?php

/**
 *
 * Recherches/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    //echo $this->Html->css(array('administration'), null, array('inline' => false));
//    echo $this->Html->css(array('zones/recherche/index'), null, array('inline' => false));
    echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
?>

<div class="container">
    <div class="row">
        <div class="table-list rechercheCondition" id="liste">
            <h3>Conditions</h3>
            <div class="content"></div>
            <div class="controls panel-footer " role="group"></div>
        </div>
    </div>
    <div class="row">
        <div class="table-list rechercheCondition" id="infos">
            <h3>Résultats</h3>
            <div class="content"></div>
            <div class="controls panel-footer  " role="group"></div>
        </div>
    </div>
</div>
<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls'),
        align: 'center'
    });

    gui.addbuttons({
        element: $('#liste .controls'),
        buttons: [
            {
                content: '<i class="fa fa-eye-slash" aria-hidden="true"></i> Masquer',
                class: "hide-search-formulaire btn-inverse btn-info-webgfc ",
                title: "<?php echo __d('default', 'Button.hide'); ?>",
                action: function () {
                    $('#liste').hide();
                }
            },
            {
                content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
                title: "<?php echo __d('default', 'Button.reset'); ?>",
                class: 'btn-info-webgfc btn-inverse',
                action: function () {
                    resetJSelect('#RechercheFormulaireForm');
                    var formCheckbox = $('.formSearch input');
                    $('#RechercheFormulaireForm .select2-search-choice').remove();
                    formCheckbox.each(function (index, item) {
                        var idInput = item.id;
                        if ($('#' + idInput + '').prop('checked')) {
                            $('.' + idInput + '').show();
                        } else {
                            $('.' + idInput + '').hide();
                        }

                    });
                    if ($('.searcheSaveTrue').is(':visible')) {
                        $('#searchSavaBool').click();
                    }
                }
            },
            {
                content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
                title: "<?php echo __d('default', 'Button.search'); ?>",
                class: 'btn-info-webgfc searchBtn',
                action: function () {
                    gui.request({
                        url: "<?php echo Configure::read('BaseUrl') . "/Recherches/recherche"; ?>",
                        data: $('#RechercheFormulaireForm').serialize(),
                        updateElement: $('#infos .content'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    });
                    $('#liste').hide();
                    $('#infos').show();
                }
            }
        ]
    });

    gui.disablebutton({
        element: $('#liste .controls'),
        button: "<?php echo __d('default', 'Button.hide'); ?>"
    });

    $('#infos').hide();
    $('.hide-search-formulaire').hide();

    gui.request({
        url: "<?php echo Configure::read('BaseUrl') . "/Recherches/formulaire"; ?>",
        updateElement: $('#liste .content'),
        loader: true,
        loaderMessage: gui.loaderMessage
    });



</script>


