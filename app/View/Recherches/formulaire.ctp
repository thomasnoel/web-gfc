<?php

/**
 *
 * Recherches/formulaire.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
echo $this->Html->script('jquery/jquery.ui.autocomplete.html.js', array('inline' => true));
//debug($this->request->data);
?>
<?php
 $formRecherche = array(
            'name' => 'Recherche',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-5',
            'input' => array()
     );
 echo $this->Formulaire->createForm($formRecherche);
// Bloc Recherche enregistrée
if ($mode != "search") {
    $formRechercheSaveName = array(
        'name' => 'Recherche',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-4',
        'input' => array(
            'Recherche.name' => array(
                'labelText' =>__d('recherche', 'Recherche.nameinform'),
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text',
                    'required' =>true
                )
            )
        )
    );
    if ($mode == 'edit') {
        $formRechercheSaveName['input']['Recherche.id'] = array(
            'inputType' => 'hidden',
            'items'=>array(
                'type' => 'hidden',
                'value' => $this->request->data['Recherche']['id']
            )
        );
    }
   echo $this->Formulaire->createForm($formRechercheSaveName);
} else {
?>
<!--<div class="form-group swithc-save" >
    <div class="col-sm-9"></div>
    <a id="searchSavaBool" class="btn btn-info-webgfc" href="#" style="margin-top:5px;"><i class="fa fa-toggle-off" aria-hidden="true"></i> Recherches enregistrées</a>
</div>-->
<?php
}

?>
<div style="display: none" class="panel formSearch searcheSaveTrue">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title"><?php echo __d('recherche', 'Recherche'); ?></h4>
        </div>
        <div class="panel-body">
            <?php
            $formRechercheEnregistre = array(
                'name' => 'Recherche',
                'label_w' => 'col-sm-4',
                'input_w' => 'col-sm-4',
                'input' => array(
                    'RecherchesEnregistrees' =>array(
                        'labelText' =>'',
                        'inputType' => 'select',
                        'items'=>array(
                            'type' => 'select',
                            "options" => $recherches,
                            'empty' => true,
                            'class'=>'selectpicker'
                        )
                    )
                )
            );
            echo $this->Formulaire->createForm($formRechercheEnregistre);
            ?>
        </div>
    </div>
</div>
<div class="panel formSearch searcheSaveFalse">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title">Choisir vos critères :</h4>
        </div>
        <div class="panel-body">
            <div class="">
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3" for="infoFlux">Informations du flux</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="infoFlux" value="" class="form-control">
                    </div>
                </div>
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3" for="serviceFlux">Service du flux</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="serviceFlux" value="" class="form-control">
                    </div>
                </div>
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3" for="ContactsFlux">Contacts</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="ContactsFlux" value="" class="form-control">
                    </div>
                </div>
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3 " for="qualification">Qualifications</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="qualification" value="" class="form-control">
                    </div>
                </div>
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3 " for="metadonnees">Métadonnées</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="metadonnees" value="" class="form-control">
                    </div>
                </div>
				<div class="rechercheCheckbox">
					<label class="control-label col-sm-3" for="circuit">Circuit</label>
					<div class="controls col-sm-1">
						<input type="checkbox" id="circuit" value="" class="form-control">
					</div>
				</div>
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3" for="dossierAffaire">Dossiers/Affaires</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="dossierAffaire" value="" class="form-control">
                    </div>
                </div>
				<div class="rechercheCheckbox">
					<label class="control-label col-sm-3" for="tache">Tâches</label>
					<div class="controls col-sm-1">
						<input type="checkbox" id="tache" value="" class="form-control">
					</div>
				</div>
                <div class="rechercheCheckbox">
                    <label class="control-label col-sm-3" for="commentaire">Commentaire</label>
                    <div class="controls col-sm-1">
                        <input type="checkbox" id="commentaire" value="" class="form-control">
                    </div>
                </div>

				<div class="rechercheCheckbox">
					<label class="control-label col-sm-3" for="document">Documents</label>
					<div class="controls col-sm-1">
						<input type="checkbox" id="document" value="" class="form-control">
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
<div id="detail_recherche">
    <div class = 'panel infoFlux' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Informations'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <div class="col-sm-12">
                    <div class="col-sm-6">
                        <legend>Information base flux</legend>
                        <?php
                        $priorites = array(
                            0 => __d('courrier', 'Courrier.priorite_basse'),
                            1 => __d('courrier', 'Courrier.priorite_moyenne'),
                            2 => __d('courrier', 'Courrier.priorite_haute')
                        );

                        $formRechercheInfoFlux = array(
                            'name' => 'Recherche',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-6',
                            'input' => array(
                                'Recherche.creference' =>array(
                                    'labelText' =>__d('recherche', 'Recherche.creference'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text'
                                    )
                                ),
                                'Recherche.cname' =>array(
                                    'labelText' =>__d('recherche', 'Recherche.cname'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text'
                                    )
                                ),
                                'Recherche.corigineflux_id' =>array(
                                    'labelText' =>__d('recherche', 'Recherche.origineflux_id'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type' => 'select',
                                        'options' => $origineflux,
                                        'empty' => true
                                    )
                                ),
                                'Recherche.cobjet' =>array(
                                    'labelText' =>__d('recherche', 'Recherche.cobjet'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text'
                                    )
                                ),
                                'Recherche.cpriorite' =>array(
                                    'labelText' =>__d('recherche', 'Recherche.cpriorite'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type' => 'select',
                                        'options' => $priorites,
                                        'empty' => true
                                    )
                                ),
                                'Desktop.Desktop' =>array(
                                    'labelText' => __d('recherche', 'Recherche.caffairesuiviepar_id'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type' => 'select',
                                        'options' => $affairesuiviepars,
                                        'empty' => true,
//                                        'multiple' => true,
                                        'class' => 'JSelectMultiple'
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formRechercheInfoFlux);
                        ?>
                    </div>
                    <div class="col-sm-6">
                        <legend><?php echo __d('recherche', 'Recherche.cdatereception'); ?></legend>
                        <?php
                        $formRechercheDateEntreFlux = array(
                            'name' => 'Recherche',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-6',
                            'input' => array(
                                'Recherche.cdatereception' =>array(
                                    'labelText' =>'Debut',
                                    'inputType' => 'text',
                                    'dateInput' =>true,
                                    'items'=>array(
                                        'type' => 'text',
                                        'readonly' => true,
                                        'class' => 'datepicker',
                                        'data-format'=>'dd/MM/yyyy',
                                    )
                                ),
                                'Recherche.cdatereceptionfin' =>array(
                                    'labelText' =>'Fin',
                                    'inputType' => 'text',
                                    'dateInput' =>true,
                                    'items'=>array(
                                        'type' => 'text',
                                        'readonly' => true,
                                        'class' => 'datepicker',
                                        'data-format'=>'dd/MM/yyyy',
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formRechercheDateEntreFlux);
                        ?>
                        <legend><?php echo __d('recherche', 'Recherche.cdate'); ?></legend>
                        <?php
                        $formRechercheDateCreateFlux = array(
                            'name' => 'Recherche',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-6',
                            'input' => array(
                                'Recherche.cdate' =>array(
                                    'labelText' =>'Debut',
                                    'inputType' => 'text',
                                    'dateInput' =>true,
                                    'items'=>array(
                                        'type' => 'text',
                                        'readonly' => true,
                                        'class' => 'datepicker',
                                        'data-format'=>'dd/MM/yyyy',
                                    )
                                ),
                                'Recherche.cdatefin' =>array(
                                    'labelText' =>'Fin',
                                    'inputType' => 'text',
                                    'dateInput' =>true,
                                    'items'=>array(
                                        'type' => 'text',
                                        'readonly' => true,
                                        'class' => 'datepicker',
                                        'data-format'=>'dd/MM/yyyy',
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formRechercheDateCreateFlux);
                        ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <hr>
                    <div class="col-sm-6">
                        <?php
                        $formRechercheInfoFluxEtat = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-5',
                                'input_w' => 'col-sm-6',
                                'input' => array(
                                    'Recherche.cetat' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.cetat'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
//                                            'multiple' => true,
//                                            'class' => 'JSelectMultiple',
                                            'options' => $etat,
                                            'empty' => true
                                        )
                                    ),
                                    'Recherche.cretard' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.cretard'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'options' => array( 'Oui' => 'Oui', 'Non' => 'Non'),
                                            'empty' => true
                                        )
                                    ),
									'Recherche.cdirection' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.cdirection'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'options' => array( '0' => 'Sortant / Départ', '1' => 'Entrant / Arrivé', '2' => 'Interne' ),
                                            'empty' => true
                                        )
                                    ),
									'Recherche.creponse' =>array(
                                        'labelText' => "Présence d'une réponse ?",
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'options' => array( '0' => 'Sans réponse', '1' => 'Avec réponse' ),
                                            'empty' => true
                                        )
                                    )
                                )
                            );
                        echo $this->Formulaire->createForm($formRechercheInfoFluxEtat);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class = 'panel serviceFlux' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('recherche', 'Recherche.Services'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                        $formRechercheServices = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Service.Service' =>array(
                                        'labelText' => 'Choisir un service',
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
//                                            'multiple' => true,
//                                            'class' => 'JSelectMultiple',
                                            'options' =>$services,
                                            'empty' => true
                                        )
                                    ),
									'Bancontenu.desktop_id' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.DesktopId'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
//                                            'multiple' => true,
                                            'options' => $desktops,
                                            'empty' => true
                                        )
                                    ),
									'Bancontenu.user_id' =>array(
										'labelText' => 'Agent étant intervenu sur le flux',
										'inputType' => 'select',
										'items'=>array(
											'type' => 'select',
											'options' => $users,
											'empty' => true
										)
									)
                                )
                            );
                        echo $this->Formulaire->createForm($formRechercheServices);
                        ?>
            </div>
        </div>
    </div>
    <div class = 'panel ContactsFlux' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('recherche', 'Recherche.Contact'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <div class="col-sm-6">
                    <legend><?php echo  __d('recherche', 'Recherche.Organismes'); ?></legend>
                    <?php
                        $formRechercheOrganismes = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-6',
                                'input_w' => 'col-sm-6',
                                'input' => array(
                                    'Recherche.organismename' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.organismename'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    ),
                                    'Organisme.Organisme' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Organisme'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
//                                            'multiple' => true,
                                            'class' =>'JSelectMultiple',
                                            'options' =>$organismes,
                                        )
                                    ),
                                    'Recherche.organismeadresse' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.organismeadresse'),
                                        'labelPlaceholder' =>__d('recherche', 'Recherche.organismeadressePlaceholder'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    ),
                                    'Recherche.organismedepartement' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.organismedepartement'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'options' => array()
                                        )
                                    ),
                                    'Recherche.organismecommune' =>array(
                                        'labelText' => __d('recherche', 'Recherche.organismecommune'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'options' => array()
                                        )
                                    ),
                                    'Recherche.organismenomvoie' =>array(
                                        'labelText' => __d('recherche', 'Recherche.organismenomvoie'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'options' => array()
                                        )
                                    ),
                                )
                            );
                        if( Configure::read('CD') == 81 ) {
                            $formRechercheOrganismes['input']['Recherche.organismecanton'] = array(
                                'labelText' => __d('organisme', 'Organisme.canton'),
                                'inputType' => 'text',
                                'items'=>array(
                                    'type' => 'text'
                                )
                            );
                        }
                        echo $this->Formulaire->createForm($formRechercheOrganismes);
                    ?>
                </div>

                <div class="col-sm-6">
                    <legend><?php echo  __d('recherche', 'Recherche.Contacts'); ?></legend>
                        <?php
                        $formRechercheContacts = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-6',
                                'input_w' => 'col-sm-6',
                                'input' => array(
                                    'Recherche.contactname' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.contactname'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    ),
                                    'Contact.Contact' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Contactchoose'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
//                                            'multiple' => true,
                                            'class' =>'JSelectMultiple',
                                            'options' =>$contacts,
                                        )
                                    ),
                                    'Recherche.contactadresse' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.contactadresse'),
                                        'labelPlaceholder' =>__d('recherche', 'Recherche.contactadressePlaceholder'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    ),
                                    'Recherche.contactdept' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.contactdepartement'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'options' => array()
                                        )
                                    ),
                                    'Recherche.contactcommune' =>array(
                                        'labelText' => __d('recherche', 'Recherche.contactcommune'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'options' => array()
                                        )
                                    ),
                                    'Recherche.contactnomvoie' =>array(
                                        'labelText' => __d('recherche', 'Recherche.contactnomvoie'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'options' => array()
                                        )
                                    ),
									'Recherche.contacttitreid' =>array(
										'labelText' => 'Titre du contact',
										'inputType' => 'select',
										'items'=>array(
											'type' => 'select',
											'options' => $titres,
											'empty' => true
										)
									)
								)
                            );
                        if( Configure::read('CD') == 81 ) {
                            $formRechercheContacts['input']['Recherche.contactcanton'] = array(
                                'labelText' => __d('contact', 'Contact.canton'),
                                'inputType' => 'text',
                                'items'=>array(
                                    'type' => 'text'
                                )
                            );
                        }
                        echo $this->Formulaire->createForm($formRechercheContacts);

                    ?>
                </div>
                <div class="col-sm-6">
                    <hr/>
					<?php
						$formRechercheOrganismeActivite = array(
							'name' => 'Recherche',
							'label_w' => 'col-sm-6',
							'input_w' => 'col-sm-6',
							'input' => array(
								'Activite.Activite' => array(
									'labelText' =>__d('organisme', 'Organisme.activite_id'),
									'inputType' => 'select',
									'items'=>array(
										'type' => 'select',
										'multiple' => true,
										'options' => $activites,
										'empty' => true
									)
								)
							)
						);
						echo $this->Formulaire->createForm($formRechercheOrganismeActivite);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('<span>ou</span>').insertAfter($('#RechercheOrganismename'));
        $('<span>ou</span>').insertAfter($('#RechercheContactname'));
    </script>
    <div class = 'panel qualification' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Qualifications'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                        $formRechercheContacts = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Type.Type' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Type'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>array(),
                                        )
                                    ),
                                    'Soustype.Soustype' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Soustype'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>array(),
                                        )
                                    )
                                )
                            );
                        echo $this->Formulaire->createForm($formRechercheContacts);

                    ?>
            </div>
        </div>
    </div>
    <div class = 'panel metadonnees' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Metadonnées'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                $formRechercheMeta = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Metadonnee.Metadonnee' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Metadonnee'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>$metadonnees
                                        )
                                    )
                                )
                            );
                echo $this->Formulaire->createForm($formRechercheMeta);
                ?>
                <legend><?php echo __d('recherche', 'Valeurs par Métadonnées'); ?> </legend>
                <?php
                $formRechercheMetaValeur = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                )
                            );
                foreach ($metas as $i => $meta) {
                    $valOfSelect =  Configure::read('Selectvaluemetadonnee.id');
                    if ($meta['Metadonnee']['typemetadonnee_id'] == 1) {//date
                        $formRechercheMetaValeur['input'] ['Selectvaluemetadonnee.Selectvaluemetadonnee.'.$meta['Metadonnee']['id']] = array(
                            'labelText' =>$meta['Metadonnee']['name'],
                            'inputType' => 'text',
                            'dateInput' =>true,
                            'items'=>array(
                                'type' => 'text',
                                'readonly' => true,
                                'class' => 'datepicker',
                                'data-format'=>'dd/MM/yyyy',
                            )
                        );
                    }else if ($meta['Metadonnee']['typemetadonnee_id'] == 3) {//boolean
                        $formRechercheMetaValeur['input'] ['Selectvaluemetadonnee.Selectvaluemetadonnee.'.$meta['Metadonnee']['id']] = array(
                            'labelText' => $meta['Metadonnee']['name'],
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'empty' => true,
                                'options' => $valueSelectMeta
                            )
                        );
                    }else if ($meta['Metadonnee']['typemetadonnee_id'] == $valOfSelect) {//select
                        $options = array();
                        if(!empty($meta['Selectvaluemetadonnee'])){
                            foreach ($meta['Selectvaluemetadonnee'] as $i => $value) {
                                $options[$value['id']] = $value['name'];
                            }
                        }

                        $formRechercheMetaValeur['input'] ['Selectvaluemetadonnee.Selectvaluemetadonnee.'.$meta['Metadonnee']['id']] = array(
                            'labelText' => $meta['Metadonnee']['name'],
                            'inputType' => 'select',
                            'items'=>array(
                                'type' => 'select',
                                'empty' => true,
                                'options' => $options
                            )
                        );
                    }else{//text
                        $formRechercheMetaValeur['input'] ['Selectvaluemetadonnee.Selectvaluemetadonnee.'.$meta['Metadonnee']['id']] = array(
                            'labelText' => $meta['Metadonnee']['name'],
                            'inputType' => 'text',
                            'items'=>array(
                                'type' => 'text'
                            )
                        );
                    }
                }
                echo $this->Formulaire->createForm($formRechercheMetaValeur);
                ?>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                <?php foreach ($metas as $i => $meta):?>
                    <?php if( in_array( $meta['Metadonnee']['typemetadonnee_id'], array( $valOfSelect , 3) ) ) :?>
                		$("#SelectvaluemetadonneeSelectvaluemetadonnee<?php echo $meta['Metadonnee']['id'];?>").select2({allowClear: true, placeholder: "Sélectionner une valeur"});
                    <?php endif;?>

					<?php if( $meta['Metadonnee']['typemetadonnee_id'] == 1 ) :?>
						$("#SelectvaluemetadonneeSelectvaluemetadonnee<?php echo $meta['Metadonnee']['id'];?>").removeAttr('readonly');
					<?php endif;?>
                <?php endforeach;?>
            });
        </script>
    </div>
    <div class = 'panel circuit' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Circuit'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                $formRechercheCircuit = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Circuit.Circuit' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Circuit'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>$circuits
                                        )
                                    )
                                )
                            );
                echo $this->Formulaire->createForm($formRechercheCircuit);
                ?>
            </div>
        </div>
    </div>
    <div class = 'panel dossierAffaire' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Dossiers / Affaires'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                $formRechercheDossierAffaire = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Dossier.Dossier' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Dossier'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>$dossiers,
                                        )
                                    ),
                                    'Affaire.Affaire' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Affaire'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>$affaires
                                        )
                                    )
                                )
                            );
                echo $this->Formulaire->createForm($formRechercheDossierAffaire);
                ?>
            </div>
        </div>
    </div>
    <div class = 'panel tache' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Tache'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                $formRechercheTache = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Tache.Tache' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.Tache'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type' => 'select',
                                            'empty' => true,
                                            'multiple' => true,
//                                            'class' =>'JSelectMultiple',
                                            'options' =>$taches,
                                        )
                                    )
                                )
                            );
                echo $this->Formulaire->createForm($formRechercheTache);
                ?>
            </div>
        </div>
    </div>
    <div class = 'panel commentaire' style='display: none'>
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo __d('recherche', 'Commentaires'); ?></h4>
            </div>
            <div class="panel-body form-horizontal">
                <?php
                $formRechercheTache = array(
                                'name' => 'Recherche',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-5',
                                'input' => array(
                                    'Recherche.ccomment' =>array(
                                        'labelText' =>__d('recherche', 'Recherche.ccomment'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type' => 'text'
                                        )
                                    )
                                )
                            );
                echo $this->Formulaire->createForm($formRechercheTache);
                ?>
            </div>
        </div>
    </div>

	<div class = 'panel document' style='display: none'>
		<div class="panel panel-default">
			<div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
				<h4 class="panel-title"><?php echo __d('recherche', 'Documents'); ?></h4>
			</div>
			<div class="panel-body form-horizontal">
				<?php
				$formRechercheDocument = array(
						'name' => 'Recherche',
						'label_w' => 'col-sm-4',
						'input_w' => 'col-sm-5',
						'input' => array(
							'Recherche.cocrdata' =>array(
								'labelText' =>__d('document', 'Document.ocr_data'),
								'inputType' => 'text',
								'items'=>array(
									'type' => 'text'
								)
							)
						)
				);
				echo $this->Formulaire->createForm($formRechercheDocument);
				?>
			</div>
		</div>
	</div>

    <script type="text/javascript">
		$('#RechercheCdatereception').removeAttr('readonly');
		$('#RechercheCdatereceptionfin').removeAttr('readonly');
		$('#RechercheCdate').removeAttr('readonly');
		$('#RechercheCdatefin').removeAttr('readonly');


        $(document).ready(function () {
            $('.form_datetime input').datepicker({
                language: 'fr-FR',
                format: "dd/mm/yyyy",
                weekStart: 1,
                autoclose: true,
                todayBtn: 'linked'
            });
        });


        // //TODO à reparer les select2
        $('#DesktopDesktop').select2({allowClear: true, placeholder: "Sélectionner un profil"});
        $('#MetadonneeMetadonnee').select2({allowClear: true, placeholder: "Sélectionner une métadonnéee"});
        $('#CircuitCircuit').select2({allowClear: true, placeholder: "Sélectionner un circuit"});
        $('#TacheTache').select2({allowClear: true, placeholder: "Sélectionner une tâche"});
        $('#DossierDossier').select2({allowClear: true, placeholder: "Sélectionner un dossier"});
        $('#AffaireAffaire').select2({allowClear: true, placeholder: "Sélectionner une affaire"});
        $("#RechercheCoriginefluxId").select2({allowClear: true, placeholder: "Sélectionner une origine"});
        $("#RechercheCpriorite").select2({allowClear: true, placeholder: "Sélectionner une priorité"});

        $('#RechercheFormulaireForm button').remove();
        $('#RechercheFormulaireForm input[type=submit]').remove();

        var formCheckbox = $('.formSearch input');
        formCheckbox.each(function (index, item) {
            var idInput = item.id;
            $('#' + idInput + '').change(function () {
                $('.' + idInput + '').toggle();
                if (!$('#' + idInput + '').prop('checked')) {
                    $('.' + idInput + '').find('input').val('');
                    $('.' + idInput + '').find('select').select2('val', 'All');
                    var delBttn = $('.' + idInput).find('.deleteAllBtn');
                    delBttn.remove();
                    var listSelect = $('.' + idInput).find('.itemSelectList');
                    listSelect.empty();
                }
            });
        });

        $('#RechercheFormulaireForm select.JSelectMultiple').each(function () {
            setJselectNew($(this));

        });


        // Chargement des listes déroulantes lors de l'édition des recherches enregistrées
        $('.item').each(function () {
            if ($(this).is(':selected')) {
                $(this).trigger('change');
            }
        });

        $('#RechercheRecherchesEnregistrees').change(function () {
        });

        // Ajout de l'action de recherche via la touche Entrée
        $('#RechercheFormulaireForm').keypress(function (e) {
            if (e.keyCode == 13) {
                $('#liste .controls .searchBtn').trigger('click');
                return false;
            }
        });



        //contruction du tableau de types / soustypes

        $("#TypeType").select2();
        $("#SoustypeSoustype").select2();


        $('#RechercheCetat').select2({allowClear: true, placeholder: "Sélectionner un statut"});
        $('#RechercheCretard').select2({allowClear: true, placeholder: "Oui ou Non"});
		$('#RechercheCdirection').select2({allowClear: true, placeholder: "Sélectionner un sens"});
		$('#BancontenuDesktopId').select2({allowClear: true, placeholder: "Sélectionner un profil"});
		$('#BancontenuUserId').select2({allowClear: true, placeholder: "Sélectionner un agent"});

		$('#RechercheCreponse').select2({allowClear: true, placeholder: "Sélectionner une valeur"});



        // Pour l'adresse
        // Using jQuery UI's autocomplete widget:
        // Using jQuery UI's autocomplete widget:

        $.widget("custom.catcomplete", $.ui.autocomplete, {
            _renderMenu: function (ul, items) {
                var self = this,
                        currentCategory = "";

                $.each(items, function (index, item) {
                    if (item.category != currentCategory) {
                        ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                        currentCategory = item.category;
                    }
                    self._renderItem(ul, item);
                });

            }

        });
        $('#RechercheContactadresse').bind("keydown", function (event) {
            //keycodes - maj : 16, ctrl : 17, alt : 18
            if (event.keyCode != 16 &&
                    event.keyCode != 17 &&
                    event.keyCode != 18) {
                $('#ContactinfoBanId').val(0);
                $('#ContactinfoBancommuneId').html('');
            }
            if ($(this).attr('value') != '') {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            }
        }).catcomplete({
            minLength: 3,
            source: '/contacts/searchAdresse'
        });


		$("#ActiviteActivite").select2({allowClear: true, placeholder: "Sélectionner une activité"});
        $("#OrganismeOrganisme").select2({allowClear: true, placeholder: "Sélectionner un organisme"});
        $('#ContactContact').select2({allowClear: true, placeholder: "Sélectionner un contact"});
        $('#ServiceService').select2({allowClear: true, placeholder: "Sélectionner un service"});


        // test
        //contruction du tableau des départements et villes

        $('#RechercheOrganismedepartement').select2({allowClear: true, placeholder: "Sélectionner un département"});
        $('#RechercheOrganismecommune').select2({allowClear: true, placeholder: "Sélectionner une commune"});
        $('#RechercheOrganismenomvoie').select2({allowClear: true, placeholder: "Sélectionner un nom de voie"});
        $('#RechercheContactdept').select2({allowClear: true, placeholder: "Sélectionner un département"});
        $('#RechercheContactcommune').select2({allowClear: true, placeholder: "Sélectionner une commune"});
        $('#RechercheContactnomvoie').select2({allowClear: true, placeholder: "Sélectionner un nom de voie"});
		$('#RechercheContacttitreid').select2({allowClear: true, placeholder: "Sélectionner un titre"});

		var bans = [];
		<?php  foreach ($bans as $ban) { ?>
        	var ban = {
				id: "<?php echo $ban['Ban']['id']; ?>",
				name: "<?php echo $ban['Ban']['name']; ?>",
				banscommunes: []
			};
			<?php foreach ($ban['Bancommune'] as $bancommune) { ?>
				var bancommune = {
					id: "<?php echo $bancommune['id']; ?>",
					name: "<?php echo addslashes($bancommune['name']); ?>"
				};
				ban.banscommunes.push(bancommune);
			<?php } ?>
			bans.push(ban);
		<?php } ?>


		$("#RechercheContactdept").append($("<option value=''> --- </option>"));
		//remplissage de la liste des bans
		for (i in bans) {
			$("#RechercheContactdept").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
		}
		//definition de l action sur bancommune
		$("#RechercheContactdept").bind('change', function () {
			var ban_id = $('option:selected', this).attr('value');
			fillBanscommunes(ban_id);
		});

		function fillBanscommunes(ban_id) {
            $("#RechercheContactcommune").empty();
            $("#RechercheContactcommune").append($("<option value=''> --- </option>"));

            for (i in bans) {
                if (bans[i].id == ban_id) {
                    for (j in bans[i].banscommunes) {
						$("#RechercheContactcommune").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
		<?php if (!empty($this->request->data['Bancommune']['id'])) {?>
			$("#RechercheContactcommune option").each(function () {
				if ($(this).attr('value') == "<?php echo $this->request->data['Bancommune']['ban_id']; ?>") {
					$(this).attr('selected', 'selected');
					var ban_id = parseInt("<?php echo $this->request->data['Bancommune']['ban_id']; ?>");
					fillBanscommunes(ban_id);
				}
			});
		<?php } ?>

        function fillBanscommunesOrgs(banorg_id) {
            $("#RechercheOrganismecommune").empty();
            $("#RechercheOrganismecommune").append($("<option value=''> --- </option>"));

            for (i in bans) {
                if (bans[i].id == banorg_id) {
                    for (j in bans[i].banscommunes) {
						$("#RechercheOrganismecommune").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }

        $("#RechercheOrganismedepartement").append($("<option value=''> --- </option>"));
        //remplissage de la liste des bans
        for (i in bans) {
            $("#RechercheOrganismedepartement").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
        }
        //definition de l action sur bancommune
        $("#RechercheOrganismedepartement").bind('change', function () {
            var banorg_id = $('option:selected', this).attr('value');
            fillBanscommunesOrgs(banorg_id);
        });

		<?php if (!empty($this->request->data['Bancommune']['id'])) { ?>
			$("#RechercheOrganismecommune option").each(function () {
				if ($(this).attr('value') == "<?php echo $this->request->data['Bancommune']['ban_id']; ?>") {
					$(this).attr('selected', 'selected');
					var banorg_id = parseInt("<?php echo $this->request->data['Bancommune']['ban_id']; ?>");
					fillBanscommunesOrgs(banorg_id);
				}
			});
		<?php } ?>

        var checkAdressesOrgs = function (bancommuneorg_id) {
            $("#RechercheOrganismenomvoie").empty();
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/Organismes/getAdresses/' + bancommuneorg_id,
                data: $('#RechercheFormulaireForm').serialize(),
                success: function (data) {
                    if (data.length > 0) {
                        $("#RechercheOrganismenomvoie").append($("<option value=''> --- </option>"));
                        // remplissage de la partie adresse
                        for (var i in data) {
                            $("#RechercheOrganismenomvoie").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                        }
                    }
                }
            });
        }

        $("#RechercheOrganismecommune").bind('change', function () {
            var bancommuneorg_id = $("#RechercheOrganismecommune").attr('value');
            checkAdressesOrgs(bancommuneorg_id);
        });
        var checkAdresses = function (bancommune_id) {
            $("#RechercheContactnomvoie").empty();
            $.ajax({
                type: 'post',
                dataType: 'json',
                url: '/Organismes/getAdresses/' + bancommune_id,
                data: $('#RechercheFormulaireForm').serialize(),
                success: function (data) {
                    if (data.length > 0) {
                        $("#RechercheContactnomvoie").append($("<option value=''> --- </option>"));
                        // remplissage de la partie adresse
                        for (var i in data) {
                            $("#RechercheContactnomvoie").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                        }
                    }
                }
            });
        }

        $("#RechercheContactcommune").bind('change', function () {
            var bancommune_id = $("#RechercheContactcommune").attr('value');
            checkAdresses(bancommune_id);
        });


// Using jQuery UI's autocomplete widget:
        $('#RechercheOrganismeadresse').bind("keydown", function (event) {
            //keycodes - maj : 16, ctrl : 17, alt : 18
            if (event.keyCode != 16 &&
                    event.keyCode != 17 &&
                    event.keyCode != 18) {
                $('#RechercheOrganismedepartement').val(0);
                $('#RechercheOrganismecommune').html('');
            }
            if ($(this).attr('value') != '') {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            }
        }).catcomplete({
            minLength: 3,
            source: '/contacts/searchAdresse',
            select: function (event, ui) {
                gui.request({
                    url: '/contacts/searchAdresseSpecifique/' + ui.item.id
                }, function (data) {
                    $('#RechercheOrganismecommune').val(ui.item.id);
                    var json = jQuery.parseJSON(data);
                    var banadresse = json['Banadresse'];
                    var banville = json['Bancommune'];
                    var ban = json['Ban'];
                    if (typeof banadresse['id'] !== 'undefined') {
                        $('#RechercheOrganismedepartement').val(ban['id']).change();
                        $('#RechercheOrganismecommune').val(banville['id']).change();
                        $("#RechercheOrganismenomvoie").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                        $("#RechercheOrganismenomvoie").val(banadresse['nom_afnor']).change();
                    }
                });
            }
        });
// Using jQuery UI's autocomplete widget:
        $('#RechercheContactadresse').bind("keydown", function (event) {
            //keycodes - maj : 16, ctrl : 17, alt : 18
            if (event.keyCode != 16 &&
                    event.keyCode != 17 &&
                    event.keyCode != 18) {
                $('#RechercheContactdept').val(0);
                $('#RechercheContactcommune').html('');
            }
            if ($(this).attr('value') != '') {
                if (event.keyCode === $.ui.keyCode.TAB &&
                        $(this).data("autocomplete").menu.active) {
                    event.preventDefault();
                }
            }
        }).catcomplete({
            minLength: 3,
            source: '/contacts/searchAdresse',
            select: function (event, ui) {
                gui.request({
                    url: '/contacts/searchAdresseSpecifique/' + ui.item.id
                }, function (data) {
                    $('#RechercheContactcommune').val(ui.item.id);
                    var json = jQuery.parseJSON(data);
                    var banadresse = json['Banadresse'];
                    var banville = json['Bancommune'];
                    var ban = json['Ban'];
                    if (typeof banadresse['id'] !== 'undefined') {
                        $('#RechercheContactdept').val(ban['id']).change();
                        $('#RechercheContactcommune').val(banville['id']).change();
                        $("#RechercheContactnomvoie").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                        $("#RechercheContactnomvoie").val(banadresse['nom_afnor']).change();
                    }
                });
            }
        });


        function zoneRechercheDetailShow(input) {
            if (input.parents('.infoFlux').length != 0) {
                $('#infoFlux').prop('checked', true);
                $('.infoFlux').show();
            }
            if (input.parents('.contactAdresse').length != 0 || $('#ContactContactList li').length != 0) {
                $('#contactAdresse').prop('checked', true);
                $('.contactAdresse').show();
            }
            if (input.parents('.qualification').length != 0 || $('#TypeTypeList li').length != 0 || $('#SoustypeSoustypeList li').length != 0) {
                $('#qualification').prop('checked', true);
                $('.qualification').show();
            }
            if (input.parents('.metadonnees').length != 0 || $('#MetadonneeMetadonneeList li').length != 0) {
                $('#metadonnees').prop('checked', true);
                $('.metadonnees').show();
            }
            if (input.parents('.valeurMetadonnees').length != 0 && input.attr('type') != 'hidden') {
                $('#valeurMetadonnees').prop('checked', true);
                $('.valeurMetadonnees').show();
            }
            if (input.parents('.circuit').length != 0 || $("#CircuitCircuitList li").length != 0) {
                $('#circuit').prop('checked', true);
                $('.circuit').show();
            }
            if (input.parents('.dossierAffaire').length != 0 || $('#DossierDossierList li').length != 0 || $('#AffaireAffaireList li').length != 0) {
                $('#dossierAffaire').prop('checked', true);
                $('.dossierAffaire').show();
            }
            if (input.parents('.tache').length != 0 || $('#TacheTacheList li').length != 0) {
                $('#tache').prop('checked', true);
                $('.tache').show();
            }
            if (input.parents('.commentaire').length != 0) {
                $('#commentaire').prop('checked', true);
                $('.commentaire').show();
            }

			if (input.parents('.document').length != 0) {
				$('#document').prop('checked', true);
				$('.document').show();
			}
        }

        $('#RechercheFormulaireForm').find('input').each(function () {
            var i = 0;
            if ($(this).val() != '') {
                if ($(this).val() != 'Sélectionner un profil' && $(this).val() != 'Sélectionner une métadonnéee' &&
                        $(this).val() != 'Sélectionner un circuit' && $(this).val() != 'Sélectionner un dossier'
                        && $(this).val() != 'Sélectionner une tâche' && $(this).val() != 'Sélectionner une affaire'
                        && $(this).val() != 'Oui ou Non') {
                    zoneRechercheDetailShow($(this));
                }
            }
        });


        //contruction du tableau de types / soustypes
        var types = [];
        <?php foreach ($types as $type) { ?>
        var type = {
            id: "<?php echo $type['Type']['id']; ?>",
            name: "<?php echo addslashes( $type['Type']['name'] ); ?>",
            soustypes: []
        };
                <?php foreach ($type['Soustype'] as $stype) { ?>
        var soustype = {
            id: "<?php echo $stype['id']; ?>",
            name: "<?php echo addslashes( $stype['name'] ); ?>",
            entrant: "<?php echo $stype['entrant']; ?>"
        };
        type.soustypes.push(soustype);
                <?php } ?>
        types.push(type);
        <?php } ?>
        function fillSoustypes(type_id) {
            $("#SoustypeSoustype").empty();
            $("#SoustypeSoustype").append($("<option value=''> --- </option>"));
            for (i in types) {
                if (types[i].id == type_id) {
                    for (j in types[i].soustypes) {
						$("#SoustypeSoustype").append($("<option value='" + types[i].soustypes[j].id + "'>" + types[i].soustypes[j].name + "</option>"));
                    }
                }
            }
        }


        $("#TypeType").append($("<option value=''> --- </option>"));
        //remplissage de la liste des types
        for (i in types) {
            $("#TypeType").append($("<option value='" + types[i].id + "'>" + types[i].name + "</option>"));
        }
        //definition de l action sur type
        $("#TypeType").bind('change', function () {
            var type_id = $('option:selected', this).attr('value');
            fillSoustypes(type_id);
        });


        $("#TypeType").select2();
        $("#SoustypeSoustype").select2();

    </script>

<?php
if(!empty($this->request->data['Metadonnee'])){
?>
    <script>
        $('.metadonnees').show();
        $('#metadonnees').prop("checked", true);
    </script>
    <?php
    $selectMetadonnees =  $this->request->data['Metadonnee'];
    foreach ($selectMetadonnees as $selectMetadonnee) {
        ?>
    <script>
        var optionText = "<?php echo $selectMetadonnee['name'];?>";
        var optionVal = "<?php echo $selectMetadonnee['id'];?>";
        var selectedName = $('#MetadonneeMetadonnee').attr('name');
        var idInput = "MetadonneeMetadonnee";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Circuit'])){
    ?>
    <script>
        $('.circuit').show();
        $('#circuit').prop("checked", true);
    </script>
    <?php
    $selectCircuits =  $this->request->data['Circuit'];
    foreach ($selectCircuits as $selectCircuit) {
        ?>
    <script>
        var optionText = "<?php echo $selectCircuit['nom'];?>";
        var optionVal = "<?php echo $selectCircuit['id'];?>";
        var selectedName = $('#CircuitCircuit').attr('name');
        var idInput = "CircuitCircuit";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Dossier'])){
    ?>
    <script>
        $('.dossierAffaire').show();
        $('#dossierAffaire').prop("checked", true);
    </script>
    <?php
    $selectDossiers =  $this->request->data['Dossier'];
    foreach ($selectDossiers as $selectDossier) {
        ?>
    <script>
        var optionText = "<?php echo $selectDossier['name'];?>";
        var optionVal = "<?php echo $selectDossier['id'];?>";
        var selectedName = $('#DossierDossier').attr('name');
        var idInput = "DossierDossier";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Affaire'])){
    ?>
    <script>
        $('.dossierAffaire').show();
        $('#dossierAffaire').prop("checked", true);
    </script>
    <?php
    $selectAffaires =  $this->request->data['Affaire'];
    foreach ($selectAffaires as $selectAffaire) {
        ?>
    <script>
        var optionText = "<?php echo $selectAffaire['name'];?>";
        var optionVal = "<?php echo $selectAffaire['id'];?>";
        var selectedName = $('#AffaireAffaire').attr('name');
        var idInput = "AffaireAffaire";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Type'])){
    ?>
    <script>
        $('.qualification').show();
        $('#qualification').prop("checked", true);
    </script>
    <?php
    $selectTypes =  $this->request->data['Type'];
    foreach ($selectTypes as $selectType) {
        ?>
    <script>
        var optionText = "<?php echo $selectType['name'];?>";
        var optionVal = "<?php echo $selectType['id'];?>";
        var selectedName = $('#TypeType').attr('name');
        var idInput = "TypeType";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Soustype'])){
    ?>
    <script>
        $('.qualification').show();
        $('#qualification').prop("checked", true);
    </script>
    <?php
    $selectSoustypes =  $this->request->data['Soustype'];
    foreach ($selectSoustypes as $selectSoustype) {
        ?>
    <script>
        var optionText = "<?php echo $selectSoustype['name'];?>";
        var optionVal = "<?php echo $selectSoustype['id'];?>";
        var selectedName = $('#SoustypeSoustype').attr('name');
        var idInput = "SoustypeSoustype";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Tache'])){
    ?>
    <script>
        $('.tache').show();
        $('#tache').prop("checked", true);
    </script>
    <?php
    $selectTaches =  $this->request->data['Tache'];
    foreach ($selectTaches as $selectTache) {
        ?>
    <script>
        var optionText = "<?php echo $selectTache['name'];?>";
        var optionVal = "<?php echo $selectTache['id'];?>";
        var selectedName = $('#TacheTache').attr('name');
        var idInput = "TacheTache";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Contact'])){
    ?>
    <script>
        $('.ContactsFlux').show();
        $('#ContactsFlux').prop("checked", true);
    </script>
    <?php
    $selectContacts =  $this->request->data['Contact'];
    foreach ($selectContacts as $selectContact) {
        ?>
    <script>
        var optionText = "<?php echo $selectContact['name'];?>";
        var optionVal = "<?php echo $selectContact['id'];?>";
        var selectedName = $('#ContactContact').attr('name');
        var idInput = "ContactContact";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Desktop'])){
    ?>
    <script>
        $('.infoFlux').show();
        $('#infoFlux').prop("checked", true);
    </script>
<?php
$selectDesktops =  $this->request->data['Desktop'];
foreach ($selectDesktops as $selectDesktop) {
    ?>
    <script>
        var optionText = "<?php echo $selectDesktop['name'];?>";
        var optionVal = "<?php echo $selectDesktop['id'];?>";
        var selectedName = $('#DesktopDesktop').attr('name');
        var idInput = "DesktopDesktop";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Organisme'])){
    ?>
    <script>
        $('.ContactsFlux').show();
        $('#ContactsFlux').prop("checked", true);
    </script>
    <?php
    $selectOrganismes =  $this->request->data['Organisme'];
    foreach ($selectOrganismes as $selectOrganisme) {
        ?>
    <script>
        var optionText = "<?php echo $selectOrganisme['name'];?>";
        var optionVal = "<?php echo $selectOrganisme['id'];?>";
        var selectedName = $('#OrganismeOrganisme').attr('name');
        var idInput = "OrganismeOrganisme";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
if(!empty($this->request->data['Service'])){
    ?>
    <script>
        $('.serviceFlux').show();
        $('#serviceFlux').prop("checked", true);
    </script>
    <?php
    $selectServices =  $this->request->data['Service'];
    foreach ($selectServices as $selectService) {
        ?>
    <script>
        var optionText = "<?php echo $selectService['name'];?>";
        var optionVal = "<?php echo $selectService['id'];?>";
        var selectedName = $('#ServiceService').attr('name');
        var idInput = "ServiceService";
        addItemToSelectList(optionText, optionVal, selectedName, idInput);
    </script>
        <?php
    }
}
?>

