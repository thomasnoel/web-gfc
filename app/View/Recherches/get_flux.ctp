<?php

/**
 *
 * Recherches/get_flux.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (!empty($flux)) {
       $nbItem = Hash::get($this->request->params, 'paging.Courrier.count');
?>
<div class="panel">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title">Total: <?php echo $nbItem; ?></h4>
        </div>
        <div class="bannette_panel panel-body">
            <table class="bannette-table"
                   data-toggle="table"
                   data-height="519"
                   data-show-refresh="false"
                   data-show-toggle="false"
                   data-show-columns="true"
                   data-search="true"
                   data-locale = "fr-CA"
                   data-select-item-name="toolbar1"
                   id="table_res">
                <thead>
                    <tr>
                        <?php
                            echo $this->Bannette->drawTableHead($flux, "Total ", array('view' => 'historiqueGlobal', 'delete' => 'delete'), true);
                        ?>
                    </tr>
                </thead>
            </table>
             <?php
            echo $this->Bannette->drawPaginator($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete'));
            ?>
        </div>
    </div>
</div>



<?php
echo $this->Js->writeBuffer();// Écrit les scripts en mémoire cache
} else {
        echo $this->Html->div('alert alert-warning',__d('repertoire', 'Element.void'));
}
?>

<script type="text/javascript">

    $(document).ready(function () {
        var fluxChoix = [];
        <?php $data =!empty($this->Bannette->drawTableTd($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete')))?  $this->Bannette->drawTableTd($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete')) : '[]' ?>;
        data = <?php echo $data; ?>;
        $('.bannette-table').bootstrapTable();
        $("#table_res").bootstrapTable('load', data);
        $("#table_res")
                .on("click-row.bs.table", function (e, row, element) {
                    if (!$(element.context).hasClass("action")) {
                        var href = element.find('.thView').find('a').attr('href');
                        $(location).attr("href", href);
                    } else if ($(element.context).hasClass("thView")) {
                    }
                })
                .on('check.bs.table', function (e, row) {
                    var fluxId = row['id'];
                    fluxChoix.push(fluxId);
                })
                .on('uncheck.bs.table', function (e, row) {
                    var fluxId = row['id'];
                    fluxChoix = jQuery.grep(fluxChoix, function (value) {
                        return value != fluxId;
                    });
                })
                .on('check-all.bs.table', function (e, rows) {
                    $.each(rows, function (i, row) {

                        var fluxId = row['id'];
                        if (jQuery.inArray(fluxId, fluxChoix)) {
                            fluxChoix.push(fluxId);
                        }
                    });
                })
                .on('uncheck-all.bs.table', function (e, rows) {
                    $.each(rows, function (i, row) {
                        var fluxId = row['id'];
                        fluxChoix = jQuery.grep(fluxChoix, function (value) {
                            return value != fluxId;
                        });
                    });
                });

        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });

    gui.removebutton({
        element: $('#repertoires_skel_infos .controls'),
        button: 1
    });
</script>
