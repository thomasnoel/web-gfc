<?php

/**
 *
 * Origineflux/get_originesflux.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
if (!empty($originesflux)) {

    $fields = array(
        'name'
    );

    $actions = array(
        'edit' => array(
            "url" => Configure::read('BaseUrl') . '/originesflux/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        'delete' => array(
            "url" => Configure::read('BaseUrl') . '/originesflux/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadOrigineflux();"
        )
    );

    $options = array('check_inactive' => true, 'inactive_value' => 0, 'inactive_field' => 'active', 'inactive_model' => 'Collectivite');
    $data = $this->Liste->drawTbody($originesflux, 'Origineflux', $fields, $actions, $options);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_originesflux"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>

<script>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($originesflux,$options); ?>');
    $('#table_originesflux')
        .bootstrapTable({
            data:<?php echo $data;?>,
            columns: [
                {
                    field: "name",
                    title: "<?php echo __d("origineflux","Origineflux.name"); ?>",
                    class: "name"
                },
                {
                    field: "active",
                    title: "<?php echo __d('origineflux', 'Origineflux.active'); ?>",
                    class: "active_column"
                },
                {
                    field: "edit",
                    title: "Modifier",
                    class: "actions thEdit",
                    width: "80px",
                    align: "center"
                },
                {
                    field: "delete",
                    title: "Supprimer",
                    class: "actions thDelete",
                    width: "80px",
                    align: "center"
                }
            ]
        })
        .on('sort.bs.table', function (e, name, order) {
            var colonne;
            var tri;
            var ancien_order = 'asc';
            var ancien_name = 'name';
            if (ancien_order == 'desc') {
                order = 'asc';
            } else {
                order = 'desc';
            }

            switch (name) {
                case 'name':
                    tri = 'Origineflux.name' + order;
                    break;
            }
            var data = $('#OriginefluxGetOrigineflux').serialize() + '&tri=' + tri;
            gui.request({
                url: "<?php echo Configure::read('BaseUrl') . "/originesflux/get_origineflux/"; ?>",
                data: data,
                updateElement: $('#infos .content'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        })
        .on('page-change.bs.table', function (number, size) {
            addClassActive();
        });
    $(document).ready(function () {
        addClassActive();
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });


    function addClassActive() {
        $('#table_originesflux .active_column').hide();
        $('#table_originesflux .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
</script>
<?php
    echo $this->LIste->drawScript($originesflux, 'Origineflux', $fields, $actions, $options);
} else {
    echo $this->Html->div('alert alert-warning',__d('origineflux', 'Origineflux.void'));
}
?>
