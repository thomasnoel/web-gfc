<?php

/**
 *
 * Consultation/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    //echo $this->Html->css(array('administration'), null, array('inline' => false));
    echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<script type="text/javascript">
    function loadConsultation() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/consultations/getConsultations"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });

        $('#infos .content').empty();
    }

</script>

<div class="container">
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('consultation', 'Consultation.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer " role="group">
        </div>
    </div>
</div>

<div id="infos" class="modal fade " role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('consultation', 'Consultation.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

<?php if( !empty($operations) ):?>
    gui.addbutton({
        element: $('#liste .controls'),
        class: 'btn  btn-info-webgfc',
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter une consultation',
            title: "<?php echo __d('default', 'Button.add'); ?>",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/consultations/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });
<?php else :?>
    gui.addbutton({
        element: $('#liste .controls'),
        class: 'btn  btn-info-webgfc',
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter une consultation',
            title: "<?php echo __d('default', 'Button.add'); ?>",
            action: function () {
                swal({
                    showCloseButton: true,
                    title: "<?php echo __d('consultation', 'Consultation.forbidden'); ?>",
                    text: "<?php echo __d('consultation', 'Consultation.add.forbidden'); ?>",
                    type: "error",

                });
            }
        }
    });

    gui.disablebutton({
        element: $('#liste .controls'),
        button: '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter une consultation'
    });
<?php endif;?>

    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#infos').modal('hide');
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var form = $(this).parents('.modal').find('form');
                    if (form_validate(form)) {
                        var url = form.attr('action');
                        var id = form.attr('id');
                        gui.request({
                            url: url,
                            data: $('#' + id).serialize()
                        }, function (data) {
                            getJsonResponse(data);
                            loadConsultation();
                        });
                        $('#infos').modal('hide');
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Oops...",
                            text: "Veuillez vérifier votre formulaire!",
                            type: "error",

                        });
                    }
                }
            }
        ]
    });

    loadConsultation();


    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
            title: "<?php echo __d('default', 'Button.exportmarche'); ?>",
            class: 'btn  btn-info-webgfc export_consultations',
            action: function () {
                window.location.href = "<?php echo Router::url( array( 'controller' => 'consultations', 'action' => 'export' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }
    });
</script>



