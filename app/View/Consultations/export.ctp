<?php

$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Informations sur les consultations' ));
    
    $this->Csv->addRow(
        array(
            'N° de la consultation',
            'Objet de la consultation',
            'Date et Heure limite',
            'N° de l\'OP',
            
            'Nb de plis reçus'
        )
	);

	foreach( $results as $m => $consultation ){

        $numeroConsultation = $consultation['Consultation']['numero'];
        $objetConsultation = $consultation['Consultation']['objet'];
        $numeroOP = $consultation['Operation']['name'];
        $datesConsultation = $this->Html2->ukToFrenchDateWithSlashes($consultation['Consultation']['datelimite']).' à '.$consultation['Consultation']['heurelimite']; 

        $rowConsultation[$m] = array_merge(
            array($numeroConsultation),
            array($objetConsultation),
            array($datesConsultation),
            array($numeroOP)
        );
        
        if( !empty($consultation['Pliconsultatif']) ) {
             $nbPlis = count($consultation['Pliconsultatif']);
                $rowPli[$m] = array_merge(
                    array($nbPlis)
                );
                $this->Csv->addRow(
                    array_merge( $rowConsultation[$m], $rowPli[$m])
                );
            /*
            foreach( $consultation['Pliconsultatif'] as $os => $valuePli ) {
                $datesPli = $this->Html2->ukToFrenchDateWithSlashes($valuePli['date']).' à '.$valuePli['heure']; 
                if( !empty( $valuePli['name'])) {
                    $rowPli[$m] = array_merge(
                        array($valuePli['numero']),
                        array($valuePli['objet']),
                        array($valuePli['lot']),
                        array($datesPli)
                    );
                }
                else {
                    $rowPli[$m] = array_merge(
                        array(''),
                        array(''),
                        array(''),
                        array('')
                    );
                }
                
                $this->Csv->addRow(
                    array_merge( $rowConsultation[$m], $rowPli[$m])
                );

            }
             * 
             */
        }
        else {
            $this->Csv->addRow(
                array_merge(
                    $rowConsultation[$m],
                    array(''),
                    array(''),
                    array(''),
                    array('')
                )
            );
        }
    }


    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>