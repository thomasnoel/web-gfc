<?php

/**
 *
 * Contactinfos/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
asort($civilite);
$formContactinfoIdentity = array(
        'name' => 'Contactinfo',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Contactinfo.civilite' => array(
                'labelText' => __d('addressbook', 'Contact.civilite'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'options' => $civilite
                )
            ),
            'Contactinfo.nom' => array(
                'labelText' =>__d('addressbook', 'Contact.nom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.prenom' => array(
                'labelText' =>__d('addressbook', 'Contact.prenom'),
               'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
$formContactinfoFieldsetIdentity = array(
        'name' => 'Desktopmanager',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Contactinfo.civilite' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Contactinfo.nom' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Contactinfo.prenom' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Contactinfo.contact_id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            )
        )
    );
$formContactinfoCom = array(
        'name' => 'Contactinfo',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Contactinfo.email' => array(
                'labelText' => __d('addressbook', 'Contact.email'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.tel' => array(
                'labelText' =>__d('addressbook', 'Contact.tel'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.portable' => array(
                'labelText' =>__d('addressbook', 'Contact.portable'),
               'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
$formContactinfoWork = array(
        'name' => 'Contactinfo',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Contactinfo.role' => array(
                'labelText' => __d('addressbook', 'Contact.role'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.organisation' => array(
                'labelText' =>__d('addressbook', 'Contact.organisation'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
$formContactinfoLoc = array(
        'name' => 'Contactinfo',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Contactinfo.adressecomplete' => array(
                'labelText' => __d('addressbook', 'Contact.adressecomplete'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.numvoie' => array(
                'labelText' =>__d('addressbook', 'Contact.numvoie'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.typevoie' => array(
                'labelText' =>__d('addressbook', 'Contact.typevoie'),
               'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'options' => $options['Addressbook']['typevoie'],
                    'empty' => true,
                )
            ),
            'Contactinfo.nomvoie' => array(
                'labelText' => __d('addressbook', 'Contact.nomvoie'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.adresse' => array(
                'labelText' =>__d('addressbook', 'Contact.adresse'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.compl' => array(
                'labelText' =>__d('addressbook', 'Contact.compl'),
               'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.cp' => array(
                'labelText' => __d('addressbook', 'Contact.cp'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.ville' => array(
                'labelText' =>__d('addressbook', 'Contact.ville'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.region' => array(
                'labelText' =>__d('addressbook', 'Contact.region'),
               'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Contactinfo.pays' => array(
                'labelText' => __d('addressbook', 'Contact.pays'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );

?>
<br>

<?php
    echo $this->Form->input('Contactinfo.name', array('label' =>array('text'=>  __d('addressbook', 'Contactinfo.name'),'class'=>'control-label  col-sm-3')));
?>
<div class="col-sm-8"></div>
<div class="row">
    <div class="panel" id="addContactIdent" style="margin-top: 45px;" >
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('addressbook', 'Contact.fieldset.identity') ?></h4>
            </div>
            <div class="panel-body">
                <?php
                    echo $this->Formulaire->createForm($formContactinfoIdentity);
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="panel col-sm-6" id="addContactFicheCoor" >
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.com') ?></h4>
            </div>
            <div class="panel-body">
                <?php
                    $fieldsetIdentity = $this->Html->tag('legend', __d('addressbook', 'Contactinfo.fieldset.identity'));
                    $fieldsetIdentity .= $this->Formulaire->createForm($formContactinfoFieldsetIdentity);
                    echo $this->Html->tag('fieldset', $fieldsetIdentity, array('style' => 'display:none;'));
                    echo $this->Formulaire->createForm($formContactinfoCom);
                ?>
            </div>
        </div>
    </div>
    <div class="panel col-sm-6" id="addContactLien" >
        <div class="panel panel-default">
            <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                <h4 class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.work') ?></h4>
            </div>
            <div class="panel-body">
                <?php
                    echo $this->Formulaire->createForm($formContactinfoWork);
                ?>
            </div>
        </div>
    </div>
</div>
<div class="panel" id="addContactAdd" >
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.loc') ?></h4>
        </div>
        <div class="panel-body">
            <?php
            echo $this->Formulaire->createForm($formContactinfoLoc);
            ?>
        </div>
    </div>
</div>
<?php
    echo $this->Form->end();
?>

<script type="text/javascript">

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";

            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }

    });

    // Using jQuery UI's autocomplete widget:
    $('#ContactinfoAdressecomplete').bind("keydown", function (event) {
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#ContactinfoBanId').val(0);
            $('#ContactinfoBancommuneId').html('');
        }
        if (event.keyCode === $.ui.keyCode.TAB &&
                $(this).data("autocomplete").menu.active) {
            event.preventDefault();
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#ContactinfoBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var ban = json['Ban'];
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#ContactinfoNumvoie').val(banadresse['numero']);
                    $('#ContactinfoNomvoie').val(banadresse['nom_voie']);
                    $('#ContactinfoCp').val(banadresse['code_post']);
                    $('#ContactinfoVille').val(banadresse['nom_commune']);
                    $('#ContactinfoRegion').val(ban['name']);
                }
            });
        }
    });
    $('select').css('width', '170px');
</script>
