<?php

/**
 *
 * Connecteurs/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
      //echo $this->Html->css(array('administration'), null, array('inline' => false));
?>
<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('connecteur', 'Connecteur.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer">
        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog scanemails">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('connecteur', 'Connecteur.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

    gui.buttonbox({
        element: $('#liste .controls')
    });

    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

//    gui.addbuttons({
//        element: $('#infos .controls'),
//        buttons: [
//            {
//                content: "<i class='fa fa-times' aria-hidden='true'></i> Fermer",
//                action: function () {
//                    $(this).parents('.modal').modal('hide');
//                }
//            }]
//    });

    function loadConnecteurlist() {
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/connecteurs/getConnecteurs"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });

        $('#infos .content').empty();
    }
    loadConnecteurlist();

    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin"; ?>";
                }
            }
        ]
    });
</script>
