<?php

//INSERT INTO connecteurs VALUES (5, 'PASTELL', '2018-08-01 11:58:03.398832', '2018-08-01 11:58:03.398832');

$true_false = array('true' => 'Oui', 'false' => 'Non');
$formPastell = array(
        'name' => 'Connecteur',
        'label_w' => 'col-sm-6',
        'input_w' => 'col-sm-6',
        'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'pastell'),
        'input' => array(
            'Connecteur.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type' => 'hidden',
                    'value' => $this->params['pass'][0]
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formPastell);
?>
<fieldset>
        <legend>Activation de PASTELL</legend>
        <?php
        $formPastellRadio = array(
            'name' => 'Connecteur',
            'label_w'=>'col-sm-5',
            'input_w'=>'col-sm-6',
            'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'pastell'),
            'input' =>array(
                'use_pastell' => array(
                    'labelText' =>"",
                    'inputType' => 'radio',
                    'items'=>array(
                        'legend' => false,
                        'separator'=> '</div><div  class="radioUserGedLabel">',
                        'before' => '<div class="radioUserGedLabel">',
                        'after' => '</div>',
                        'label' => true,
                        'type'=>'radio',
                        'options' => $true_false,
                        'value' => ( isset( $this->request->data['Connecteur']['use_pastell'] ) && $this->request->data['Connecteur']['use_pastell'] ) ? 'true' : 'false'
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formPastellRadio);
        ?>
    </fieldset>

    <div id='config_content' >
        <fieldset>
            <legend>Connecteur PASTELL</legend>
            <?php
            $formPASTELLProtocol = array(
                    'name' => 'Connecteur',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-6',
                    'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'signature'),
                    'input' => array(
                        'host' => array(
                            'labelText' =>'URL',
                            'labelPlaceholder' => "https://pastell.x.org",
                            'inputType' => 'text',
                            'items'=>array(
								'required' => true,
                                'type' => 'text'
                            )
                        ),
                        'login' => array(
                            'labelText' =>'Login',
                            'labelPlaceholder' => "Nom d'utilisateur",
                            'inputType' => 'text',
                            'items'=>array(
								'required' => true,
                                'type' => 'text'
                            )
                        ),
                        'pwd' => array(
                            'labelText' =>'Mot de passe',
                            'labelPlaceholder' => "Mot de passe utilisateur",
                            'inputType' => 'password',
                            'items'=>array(
								'required' => true,
                                'type' => 'password'
                            )
                        )
                    )
                );
            echo $this->Formulaire->createForm($formPASTELLProtocol);
            ?>
        </fieldset>
		<fieldset class="Pastell-refreshColl">

		</fieldset>
        <fieldset class='pastell-infos'>
			<legend><?php echo __('Configuration'); ?></legend>
			<?php
			$formPastellConf = array(
					'name' => 'Connecteur',
					'label_w' => 'col-sm-5',
					'input_w' => 'col-sm-6',
					'form_url' =>array('controller' => 'connecteurs', 'action' => 'makeconf', 'pastell'),
					'input' => array(
							'id_entity' => array(
									'labelText' =>'Collectivité cible',
									'labelPlaceholder' => "Collectivité",
									'inputType' => 'select',
									'items'=>array(
											'type' => 'select',
											'options' => isset( $entities ) ? $entities : array(),
											'selected' => isset($selected) ? $selected : null,
											'value'  => isset($selected) ? $selected : null
									)
							),
							'json_pastell' => array(
								'inputType' => 'hidden',
								'items'=>array(
									'type' => 'hidden'
								)
							),
							'type_doc_pastell' => array(
								'labelText' => 'Type de document PASTELL',
								'labelPlaceholder' => "exemple : gfc-dossier",
								'inputType' => 'text',
								'items'=>array(
									'type' => 'text',
									'readonly' => true,
									'class' => 'fakeDisabledInputText',
									'value' => Configure::read('Pastell.fluxStudioName')
								)
							)
					)
			);
			echo $this->Formulaire->createForm($formPastellConf);

			?>
			<legend>Activation du service Mail Sécurisé</legend>
			<?php
			$formMailsecRadio = array(
					'name' => 'Connecteur',
					'label_w'=>'col-sm-5',
					'input_w'=>'col-sm-6',
					'form_url' => array('controller' => 'connecteurs', 'action' => 'makeconf', 'mailsec'),
					'input' =>array(
						'use_mailsec' => array(
							'labelText' =>"",
							'inputType' => 'radio',
							'items'=>array(
								'legend' => false,
								'separator'=> '</div><div  class="radioUseMailsecLabel">',
								'before' => '<div class="radioUseMailsecLabel">',
								'after' => '</div>',
								'label' => true,
								'type'=>'radio',
								'options' => $true_false,
								'value' => ( isset( $this->request->data['Connecteur']['use_mailsec'] ) && $this->request->data['Connecteur']['use_mailsec'] ) ? 'true' : 'false'
							)
						)
					)
			);
			echo $this->Formulaire->createForm($formMailsecRadio);
			?>
		</fieldset>
    </div>

    <div class="boutons">    </div>

        <?php
            echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 50%;'));
            echo $this->Html->link(' <i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler', array('controller' => 'connecteurs', 'action' => 'index'), array('class' => 'btn btn-danger-webgfc btn-inverse closeModal', 'escape' => false, 'title' => 'Annuler'));
            echo $this->Html->tag('/div', null);
            echo $this->Html->tag('div', null, array('class' => 'btn-group', 'style' => 'margin-top:10px; margin-left: 1%;'));
            echo $this->Form->button(' <i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer', array('type' => 'submit', 'id' => 'boutonValider', 'class' => 'btn btn-success closeModal', 'escape' => false, 'title' => 'Modifier la configuration'));
            echo $this->Html->tag('/div', null);
            echo $this->Form->end();
        ?>
<script type="text/javascript">
    gui.addbutton({
        element: $('.Pastell-refreshColl'),
        button: {
            id: 'btnRefreshEntities',
            class: 'btn-default btn-primary',
            content: "<i class='fa fa-refresh'> Recherche de collectivités</i>",
            title: ' Recherche de collectivités'
        }
    });

    $('#btnRefreshEntities').css('margin-left','275px');

    $('#btnRefreshEntities').on('click', function () {
		$('#ConnecteurIdEntity').empty();

        $('.Pastell-refreshColl').show();

        $.ajax({
            type: 'GET',
            url: '/connecteurs/listEntities',
            dataType: 'json',
            success: function (options) {
                var data = jQuery.map(options, function (element, number) {
                    return {
                        id: number,
                        text: element
                    }
                });

                $.each(options, function (index, value) {
                    $('#ConnecteurIdEntity').append($("<option value='" + index + "'>" + value + "</option>"));
                });
                $('#ConnecteurJsonPastell').val(JSON.stringify(data));
                layer.msg("La liste des collectivités a bien été récupérée");

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                layer.msg("<strong>Erreur: </strong>Erreur de récupération de la liste des collectivités, veuillez vérfier les informations de connexion");
            }
        });
    });

    $(document).ready(function () {

		// $('.Pastell-refreshColl').hide();
		<?php if( isset($this->request->data['Connecteur']['json_pastell'] ) && !empty( $this->request->data['Connecteur']['json_pastell'] ) ):?>
			$('.Pastell-refreshColl').show();
		<?php endif;?>

		if ($('#ConnecteurUsePastellTrue').attr('checked') == 'checked') {
			$('#config_content').show();
		}

		if ($('#ConnecteurUsePastellFalse').attr('checked') == 'checked') {
			$('#config_content').hide();
		}

		$('#ConnecteurMakeconfForm input[type=radio]').change(function () {
			// $('#config_content').toggle();
			if ($('#ConnecteurUsePastellTrue').attr('checked') == 'checked') {
				$('#config_content').show();
			}

			if ($('#ConnecteurUsePastellFalse').attr('checked') == 'checked') {
				$('#config_content').hide();
			}
		});
		$('.fakeDisabledInputText' ).css('cursor', 'not-allowed');

		// $('#ConnecteurTypeDocPastell').addClass('ui-state-disabled');
		// $('#ConnecteurTypeDocPastell').css('cursor', 'not-allowed');
    });

    $('#ConnecteurIdEntity').select2();

	$('.pastell-infos').show();
	<?php if( empty( $this->request->data['Connecteur']['host']) ) :?>
		$('.pastell-infos').hide();
		$('.Pastell-refreshColl').hide();
	<?php endif;?>
</script>
