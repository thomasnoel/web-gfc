<?php

/**
 *
 * Collectivites/upload.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    if (window.top.window.getLogo) {
        window.top.window.getLogo(false);
    }
<?php if (!empty($message)) { ?>
    layer.msg("<div class='info'><?php echo $message; ?></div>", {offset: '70px'});
<?php } ?>
</script>


