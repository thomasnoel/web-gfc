<?php

/**
 *
 * Affaires/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>

<script type="text/javascript">
    $(document).ready(function () {
        $("<div id='dossier'>").insertBefore($('#DossierName').parents('.form-group'));
        $("#dossier").append($('#DossierName').parents('.form-group'));
        $("#dossier").append($('#DossierName').parents('.form-group'));
        $("#dossier").append($('#DossierComment').parents('.form-group'));
        $("#dossier").append($('#DossierComment').parents('.form-group'));
    });

    function selectDossier() {
        if ($('#AffaireDossierId option:selected').val() != "new") {
            $('#dossier').hide();
        } else {
            $('#dossier').show();
        }
    }
</script>
<?php
$formAffaire_newAffaireName = array(
    'name' => 'Affaire',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'name' => array(
            'labelText' =>__d('affaire', 'Affaire.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'comment' => array(
            'labelText' =>__d('affaire', 'Affaire.commentaire'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        ),
        'dossier_id' => array(
            'labelText' =>__d('affaire', 'Affaire.dossier'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $dossiers,
                'empty' => true
            )
        ),
        'Dossier.name' => array(
            'labelText' =>__d('dossier', 'Dossier.name'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
//                'value'=>$newDossierName
            )
        ),
        'Dossier.comment' => array(
            'labelText' =>__d('dossier', 'Dossier.comment'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        )
    )
);

$formAffaire = array(
    'name' => 'Affaire',
    'label_w' => 'col-sm-3',
    'input_w' => 'col-sm-7',
    'input' => array(
        'name' => array(
            'labelText' =>__d('affaire', 'Affaire.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'comment' => array(
            'labelText' =>__d('affaire', 'Affaire.commentaire'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        ),
        'dossier_id' => array(
            'labelText' =>__d('affaire', 'Affaire.dossier'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $dossiers,
                'empty' => true
            )
        )
    )
);

echo $this->Formulaire->createForm($formAffaire_newAffaireName	);
echo $this->Form->end();

?>
<script type="text/javascript">
    $('#AffaireDossierId').select2();
    $('#AffaireDossierId').change(function () {
        selectDossier();
    });
    selectDossier();
</script>
