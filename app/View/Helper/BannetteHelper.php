<?php

/**
 *
 * Bannette helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class BannetteHelper extends Helper {

    /**
     *
     * @var type
     */
    public $helpers = array('String', 'Html', 'Form', 'Paginator', 'Js');

    /**
     *
     * @param type $priority
     * @return type
     */
    private function _convertPriority($priority) {
        $img = 'priority_unknown.png';
        $alt = $priority;
        $title = __d('courrier', 'Courrier.priorite_indefinie');
        if ($priority == 0) {
            $img = 'priority_low.png';
            $title = __d('courrier', 'Courrier.priorite_basse');
        } else if ($priority == 1) {
            $img = 'priority_mid.png';
            $title = __d('courrier', 'Courrier.priorite_moyenne');
        } else if ($priority == 2) {
            $img = 'priority_high.png';
            $title = __d('courrier', 'Courrier.priorite_haute');
        }
        return $this->Html->image('/img/' . $img, array('alt' => $alt, 'title' => $title));
    }

    /**
     *
     */
    private function _convertRetard($retard) {
        return $retard ? $this->Html->image('/img/retard_16.png', array('alt' => 'retard', 'title' => __d('courrier', 'Courrier.retard'))) : '';
    }

    /**
     *
     * @param type $fileassoc
     * @return type
     */
    private function _convertFileassoc($fileassoc) {
//        return $fileassoc ? $this->Html->image('/img/fileassoc_16.png', array('alt' => 'présent', 'alt' => __d('courrier', 'Courrier.fileassoc'))) : '';
        return $fileassoc ? '<i class="fa fa-paperclip" aria-hidden="true"></i>' : '';
    }

    /**
     *
     * @param type $direction
     * @return type
     */
    private function _convertDirection($direction) {
        $return = "";
        $directionDef = array(
            '1' => array(
//                'img' => 'folder_inbox.png',
//                'img' => '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
                'img' => 'fa fa-sign-in',
                'title' => __d('courrier', 'Courrier.courrier_entrant'),
                'alt' => __d('courrier', 'Courrier.courrier_entrant'),
                'style' => "color:#2ec07e;"
            ),
            '0' => array(
//                'img' => 'folder_outbox.png',
//                'img' => '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
                'img' => 'fa fa-sign-out',
                'title' => __d('courrier', 'Courrier.courrier_sortant'),
                'alt' => __d('courrier', 'Courrier.courrier_sortant'),
                'style' => "color:#e51809;"
            ),
//            '3' => array(
//                'img' => 'flux_interne.png',
//                'title' => __d('courrier', 'Courrier.courrier_interne')
//            )
            '2' => array(
//                'img' => '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
                'img' => 'fa fa-level-up',
                'title' => __d('courrier', 'Courrier.courrier_interne'),
                'alt' => __d('courrier', 'Courrier.courrier_interne'),
                'style' => "color:#5397a7;"
            )
        );

        if (!is_null($direction) && in_array($direction, array_keys($directionDef))) {
            $return = $this->Html->tag('i', '', array('alt' => $directionDef[$direction]['alt'], 'title' => $directionDef[$direction]['title'], 'class' => $directionDef[$direction]['img'],  'style' => $directionDef[$direction]['style']));
        }
        return $return;
    }

    /**
     *
     * @param type $etat
     * @return type
     */
    private function _convertEtat($etat, $bannetteId) {
		$class = 'fa fa-flag-checkered';
        $title = __d('courrier', 'Courrier.courrier_valide');
        $alt = 'validé';
		$style = 'cursor:pointer;';
        if ($etat == -1) {
			$class = 'fa fa-exclamation-circle';
            $title = __d('courrier', 'Courrier.courrier_refuse');
            $alt = 'refusé';
			$style = 'color:red;cursor:pointer;';
        }  else if ($etat == 1 && $bannetteId == 2 ) {
			$class = 'fa fa-exclamation-circle';
			$title = __d('courrier', 'Courrier.courrier_refuse');
			$alt = 'refusé';
			$style = 'color:red;cursor:pointer;';
		} else if ($etat == 1 || $etat == 0) {
			$class = 'fa fa-cogs';
			$title = __d('courrier', 'Courrier.courrier_a_traite');
			$alt = 'traitement';
			$style = 'cursor:pointer;';
		} else if ($etat == 2) {
            $class = 'fa fa-flag-checkered';
            $title = __d('courrier', 'Courrier.courrier_cloture');
            $alt = 'clos';
			$style = 'color:#2ec07e;cursor:pointer;';
        } else if ($etat == 3) {
			$class = 'fa fa-cogs';
            $title = __d('courrier', 'Courrier.courrier_a_traite');
            $alt = 'traitement';
			$style = 'cursor:pointer;';
        }
		return $this->Html->tag('i', '', array('class' => $class, 'style' => $style, 'alt' => $alt, 'title' => $title ));
//		return $this->Html->image('/img/' . $img, array('alt' => $alt, 'title' => $title));
    }

    /**
     *
     * @param type $objet
     * @return type
     */
    private function _convertObjet($objet) {
        return $this->String->troncateStr($objet, 90);
    }

    /**
     *
     * @param type $date
     * @return type
     */
    private function _convertDate($date) {
        return strftime("%d/%m/%Y", strtotime($date));
    }

    /**
     *
     * @param type $date
     * @return type
     */
    private function _convertDateTime($date) {
        return strftime("%d/%m/%Y %T", strtotime($date));
    }

    /**
     *
     * @param type $edit
     * @param type $fluxId
     * @return type
     */
    private function _convertEdit($edit, $fluxId, $desktopId = null) { //Arnaud
        return $edit ? $this->Html->link($this->Html->tag('i', '', array('alt' => __d('default', 'Button.edit'), 'title' => __d('default', 'Button.edit'), 'class' => 'fa fa-pencil')), array('plugin' => null, 'controller' => 'courriers', 'action' => 'view', $fluxId, $desktopId), array(/* 'target' => '_blank', */ 'escape' => false)) : '';
    }

    /**
     *
     * @param type $view
     * @param type $fluxId
     * @return type
     */
    private function _convertView($view, $fluxId, $desktopId = null) {
        $url = array('plugin' => null, 'controller' => 'courriers', 'action' => 'view', $fluxId, $desktopId);
        if ($this->action == 'historique') {
            $url = array('plugin' => null, 'controller' => 'courriers', 'action' => 'historiqueGlobal', $fluxId);
        }
//		return $view ? $this->Html->image('/img/search.png', array('alt' => __d('default', 'Button.view'), 'title' => __d('default', 'Button.view'), 'url' => array('plugin' => null, 'controller' => 'courriers', 'action' => 'view', $fluxId, $desktopId))) : '';
        return $view ? $this->Html->link($this->Html->tag('i', '', array('alt' => __d('default', 'Button.view'), 'title' => __d('default', 'Button.view'), 'class' => 'fa fa-eye')), $url, array('target' => ($this->action == 'historique') ? '_blank' : '', 'escape' => false)) : '';
    }

    /**
     *
     * @param type $view
     * @param type $fluxId
     * @return type
     */
    private function _convertPreviewDocument($reference, $desktopId = null) {
//        debug($fluxId);
//        return $reference;
//		return $view ? $this->Html->image('/img/search.png', array('alt' => __d('default', 'Button.view'), 'title' => __d('default', 'Button.view'), 'url' => array('plugin' => null, 'controller' => 'courriers', 'action' => 'view', $fluxId, $desktopId))) : '';
        return $this->Html->link($this->Html->tag('i', '', array('alt' => __d('default', 'Button.view'), 'title' => __d('default', 'Button.view'), 'class' => 'fa fa-file')), "#", array(/* 'target' => '_blank', */ 'escape' => false, 'onclick' => "return preveiwdocumentflux(" . $reference . ")"));
    }

    /**
     *
     * @param type $delete
     * @param type $fluxId
     * @return type
     */
    private function _convertDelete($delete, $fluxId, $isParent, $creatorId, $userconnectedId, $bannetteName) {
        $isAdmin =false;
        $userProfils = array( CakeSession::read('Auth.User.Env.main') );
        $profils = CakeSession::read('Auth.User.Env.secondary');
        foreach( $profils as $name ) {
            $userProfils[] = $name;
        }
        if (in_array( 'admin', $userProfils )) {
            $isAdmin = true;
        }

        $delete = false;
        if (Configure::read('Suppression.Ok') ) {
            if ($isParent || $creatorId != $userconnectedId) {
                $delete = false;
            }

            if ( $bannetteName == 'aiguillage' || empty( $creatorId )) {
                $delete = true;
            }
			if($isAdmin) {
				$delete = true;
			}
			if ($creatorId == $userconnectedId) {
				$delete = true;
			}

        }
        else if($isAdmin) {
            $delete = true;
        }

		if( Configure::read( 'Suppression.Permanente' ) ){
			$delete = true;
		}
		return $delete ? $this->Html->link($this->Html->tag('i', '', array('alt' => __d('default', 'Button.delete'), 'title' => __d('default', 'Button.delete'), 'class' => 'fa fa-trash', 'style' => 'color: #FF0000;cursor:pointer')), '#', array(/* 'target' => '_blank', */ 'escape' => false, 'onclick' => "return deletflux(" . $fluxId . ")")) : '';
    }

    /**
     *
     * @param type $edit
     * @param type $fluxId
     * @return type
     */
    private function _convertDetache($detache, $fluxId, $desktopId) { //Arnaud
        $imageDetache = $this->Html->image('/img/link.png', array('alt' => __d('default', 'Button.detache'), 'title' => __d('default', 'Button.unlink')));
//        return $detache ? $this->Html->image('/img/link.png', array('alt' => __d('default', 'Button.detache'), 'title' => __d('default', 'Button.unlink'), 'url' => array('plugin' => null, 'controller' => 'courriers', 'action' => 'detache', $fluxId, $desktopId))) : '';
        return $detache ? $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-unlock-alt')), '#', array(/* 'target' => '_blank', */ 'escape' => false, 'onclick' => 'detacheflux(' . $fluxId . ',' . $desktopId . ');return false;')) : '';
    }

    /**
     *
     * @param type $flux
     * @return type
     */
    private function _convertFlux($flux, $date, $bannetteName = null) {
        if ($date == 'datereception') {
            $keydate = __d('bannette', 'Courrier.datereception');
            if (!empty($flux['date'])) {
                $valdate = $this->_convertDate($flux['date']);
            } else {
                $valdate = $this->_convertDateTime($flux['created']);
                $tmp = explode(' ', $valdate);
                $valdate = $tmp[0];
            }
        } else if ($date == 'created') {
            $keydate = __d('bannette', 'Courrier.created');
            $valdate = $this->_convertDateTime($flux['created']);
        }

        $alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));

        $options = array(
            'model' => $alias,
            'url' => (array) $this->passedArgs + array('model' => $alias),
        );

		$retard = "";
		/*if (isset($flux['retard']) && $flux['retard']) {
			$alt = $title = __d('courrier', 'Courrier.alertRetard');
			if( isset($flux['delairetard']) && !empty($flux['delairetard']) ) {
				$alt = $title = __d('courrier', 'Courrier.alertRetard').' de '.$flux['delairetard'].' jours';
			}
			$retard = $this->Html->image("/img/retard_16.png", array('height' => '16px;', 'style' => 'vertical-align: middle;', 'alt' => $alt, 'title' => $title, 'class' => 'delairetard'));
		}*/

		if( $flux['etat'] != 2 ) {
			if (!empty($flux['delairetard']) && $flux['delairetard'] > 0 ) {
				$alt = $title = __d('courrier', 'Courrier.alertRetard');
				if (isset($flux['delairetard']) && !empty($flux['delairetard'])) {
					$alt = $title = __d('courrier', 'Courrier.alertRetard') . ' de ' . $flux['delairetard'] . ' jours';
				}
				$retard = $this->Html->image("/img/retard_16.png", array('height' => '16px;', 'style' => 'vertical-align: middle;', 'alt' => $alt, 'title' => $title, 'class' => 'delairetard'));
			}
			if (isset($flux['delairetard']) && !empty($flux['delairetard'])) {
				$delai = ' J' . $flux['delairetard'];
				$retard = $retard . $delai;
			}
		}

        if ($this->request->params['action'] == 'dispenv') {
            $classRead = 1;
            if (!($flux['read'])) {
                $classRead = -1;
            }

            if ($bannetteName == 'aiguillage') {
                $return = array(
                    "id" => $flux['id'],
                    "desktopId" => $flux['desktopId'],
                    "creatorId" => $flux['creatorId'],
                    "userconnectedId" => $flux['userconnectedId'],
                    "read" => $flux['read'],
                    "priorite" => $this->_convertPriority($flux['priorite']) . $this->Form->input('checkDesktop_' . $flux['desktopId'], array('type' => 'hidden', 'class' => 'checkDesktop', 'desktopid' => $flux['desktopId'])),
                    "retard" => $retard,
                    "ficassoc" => $this->_convertFileassoc($flux['ficassoc']),
                    "direction" => $this->_convertDirection($flux['direction']),
                    "etat" => $this->_convertEtat($flux['etat'], $flux['bannette_id']),
                    "reference" => $flux['reference'] . $this->Form->input('unread', array('type' => 'hidden', 'class' => 'unread', 'value' => $classRead)),
                    "nom" => $flux['nom'] . $this->Form->input('checkItem_' . $flux['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $flux['id'])),
                    "contact" => $flux['contact'],
                    "date" => $valdate,
                    "typesoustype" => $flux['typesoustype'],
                    "bureau" => $flux['bureau'],
                    "commentaire" => $flux['commentaire'],
                    "modified" => $flux['modified'],
                    "previewdocument" => !empty($this->_convertFileassoc($flux['ficassoc'])) ? $this->_convertPreviewDocument($flux['reference'], $flux['desktopId']) : '',
                    "edit" => $this->_convertEdit($flux['view'], $flux['id'], $flux['desktopId']), //Arnaud
                    "delete" => $this->_convertDelete($flux['delete'], $flux['id'], $flux['isParent'], $flux['creatorId'], $flux['userconnectedId'], $bannetteName),
                    "detache" => $this->_convertDetache($flux['detache'], $flux['id'], $flux['desktopId']), //Arnaud +
                    'isParent' => $flux['isParent'],
                    'hasParent' => !empty($flux['parent_id']),
                    'parentInfo' => $this->_convertParentInfo($flux['parentInfo'], $flux['id'])
                );
            } else {
                if( $bannetteName == 'copy' ) {
                    $return = array(
                        "id" => $flux['id'],
                        "desktopId" => $flux['desktopId'],
                        "creatorId" => $flux['creatorId'],
                        "userconnectedId" => $flux['userconnectedId'],
                        "read" => $flux['read'],
                        "priorite" => $this->_convertPriority($flux['priorite'])  . $this->Form->input('checkDesktop_' . $flux['desktopId'], array('type' => 'hidden', 'class' => 'checkDesktop', 'desktopid' => $flux['desktopId'])),
                        "retard" => $retard,
                        "ficassoc" => $this->_convertFileassoc($flux['ficassoc']),
                        "direction" => $this->_convertDirection($flux['direction']),
                        "etat" => $this->_convertEtat($flux['etat'], $flux['bannette_id']),
                        "reference" => $flux['reference'] . $this->Form->input('unread', array('type' => 'hidden', 'class' => 'unread', 'value' => $classRead)),
                        "nom" => $flux['nom'] . $this->Form->input('checkItem_' . $flux['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $flux['id'])),
						"objet" => preg_replace("/[\n\r]/", " ", $this->String->troncateStr($flux['objet'], 90)),
                        "contact" => $flux['contact'],
                        "date" => $valdate,
                        "typesoustype" => $flux['typesoustype'],
                        "bureau" => $flux['bureau'],
                        "commentaire" => $flux['commentaire'],
                        "modified" => $flux['modified'],
                        "view" => $this->_convertView($flux['view'], $flux['id'], $flux['desktopId']),
                        "previewdocument" => !empty($this->_convertFileassoc($flux['ficassoc'])) ? $this->_convertPreviewDocument($flux['reference'], $flux['desktopId']) : '',
                        "delete" => $this->_convertDelete($flux['delete'], $flux['id'], $flux['isParent'], $flux['creatorId'], $flux['userconnectedId'], $bannetteName),
                        "detache" => $this->_convertDetache($flux['detache'], $flux['id'], $flux['desktopId']), //Arnaud +
                        'isParent' => $flux['isParent'],
                        'hasParent' => !empty($flux['parent_id']),
                        'parentInfo' => $this->_convertParentInfo($flux['parentInfo'], $flux['id'])
                    );
                }
                else {
                    $return = array(
                        "id" => $flux['id'],
                        "desktopId" => $flux['desktopId'],
                        "creatorId" => $flux['creatorId'],
                        "userconnectedId" => $flux['userconnectedId'],
                        "read" => $flux['read'],
                        "priorite" => $this->_convertPriority($flux['priorite'])  . $this->Form->input('checkDesktop_' . $flux['desktopId'], array('type' => 'hidden', 'class' => 'checkDesktop', 'desktopid' => $flux['desktopId'])),
                        "retard" => $retard,
                        "ficassoc" => $this->_convertFileassoc($flux['ficassoc']),
                        "direction" => $this->_convertDirection($flux['direction']),
                        "etat" => $this->_convertEtat($flux['etat'], $flux['bannette_id']),
                        "reference" => $flux['reference'] . $this->Form->input('unread', array('type' => 'hidden', 'class' => 'unread', 'value' => $classRead)),
                        "nom" => $flux['nom'] . $this->Form->input('checkItem_' . $flux['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $flux['id'])),
						"objet" => preg_replace("/[\n\r]/", " ", $this->String->troncateStr($flux['objet'], 90)),
                        "contact" => $flux['contact'],
                        "date" => $valdate,
                        "typesoustype" => $flux['typesoustype'],
                        "bureau" => $flux['bureau'],
                        "commentaire" => $flux['commentaire'],
                        "modified" => $flux['modified'],
                        "edit" => $this->_convertEdit($flux['view'], $flux['id'], $flux['desktopId']),
                        "previewdocument" => !empty($this->_convertFileassoc($flux['ficassoc'])) ? $this->_convertPreviewDocument($flux['reference'], $flux['desktopId']) : '',
                        "delete" => $this->_convertDelete($flux['delete'], $flux['id'], $flux['isParent'], $flux['creatorId'], $flux['userconnectedId'], $bannetteName),
                        "detache" => $this->_convertDetache($flux['detache'], $flux['id'], $flux['desktopId']), //Arnaud +
                        'isParent' => $flux['isParent'],
                        'hasParent' => !empty($flux['parent_id']),
                        'parentInfo' => $this->_convertParentInfo($flux['parentInfo'], $flux['id'])
                    );
                }
            }
        } else {
            $infoparent = "";
            if (!empty($flux['parent_id'])) {
                $viewParent = $this->Html->image('/img/help-hint.png', array(
                    //'title' => 'Informations sur le flux d\'origine',
                    'alt' => 'Informations sur le flux d\'origine',
                    'class' => 'btnParentInfo'
                ));
                $infoparent = '&nbsp; <i class="fa fa-hand-o-right" aria-hidden="true" id="hand_' . $flux['id'] . '"></i> &nbsp;&nbsp;';
                $infoparent .=$this->_convertParentInfo($flux['parentInfo'], $flux['id']);
            }

            $classRead = 1;
            if (!($flux['read'])) {
                $classRead = -1;
            }
            if( $bannetteName == 'copy' || $this->request->params['action'] == 'historique') {
                $return = array(
                    "id" => $flux['id'],
                    "desktopId" => $flux['desktopId'],
                    "creatorId" => $flux['creatorId'],
                    "userconnectedId" => $flux['userconnectedId'],
                    "read" => $flux['read'],
                    "priorite" => $this->_convertPriority($flux['priorite'])  . $this->Form->input('checkDesktop_' . $flux['desktopId'], array('type' => 'hidden', 'class' => 'checkDesktop', 'desktopid' => $flux['desktopId'])),
                    "retard" => $retard,
                    "ficassoc" => $this->_convertFileassoc($flux['ficassoc']),
                    "direction" => $this->_convertDirection($flux['direction']),
                    "etat" => $this->_convertEtat($flux['etat'], $flux['bannette_id']),
                    "reference" => $flux['reference'] . $this->Form->input('unread', array('type' => 'hidden', 'class' => 'unread', 'value' => $classRead)),
                    "nom" => $flux['nom'] . $infoparent . $this->Form->input('checkItem_' . $flux['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $flux['id'])),
					"objet" => preg_replace("/[\n\r]/", " ", $this->String->troncateStr($flux['objet'], 90)),
                    "contact" => $flux['contact'],
                    "date" => $valdate, //FIXME
                    "typesoustype" => $flux['typesoustype'],
                    "bureau" => $flux['bureau'],
                    "commentaire" => $flux['commentaire'],
                    "modified" => $this->_convertDateTime($flux['modified']), //FIXME
                    "view" => $this->_convertView($flux['view'], $flux['id'], $flux['desktopId']),
                    "delete" => $this->_convertDelete($flux['delete'], $flux['id'], $flux['isParent'], $flux['creatorId'], $flux['userconnectedId'], $bannetteName),
                    "detache" => $this->_convertDetache($flux['detache'], $flux['id'], $flux['desktopId']), // Arnaud +
                    'isParent' => $flux['isParent'],
                    'hasParent' => !empty($flux['parent_id']),
                    'parentInfo' => $this->_convertParentInfo($flux['parentInfo'], $flux['id'])
                );
            }
            else {
                $return = array(
                    "id" => $flux['id'],
                    "desktopId" => $flux['desktopId'],
                    "creatorId" => $flux['creatorId'],
                    "userconnectedId" => $flux['userconnectedId'],
                    "read" => $flux['read'],
                    "priorite" => $this->_convertPriority($flux['priorite'])  . $this->Form->input('checkDesktop_' . $flux['desktopId'], array('type' => 'hidden', 'class' => 'checkDesktop', 'desktopid' => $flux['desktopId'])),
                    "retard" => $retard,
                    "ficassoc" => $this->_convertFileassoc($flux['ficassoc']),
                    "direction" => $this->_convertDirection($flux['direction']),
                    "etat" => $this->_convertEtat($flux['etat'], $flux['bannette_id']),
                    "reference" => $flux['reference'] . $this->Form->input('unread', array('type' => 'hidden', 'class' => 'unread', 'value' => $classRead)),
                    "nom" => $flux['nom'] . $infoparent . $this->Form->input('checkItem_' . $flux['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $flux['id'])),
					"objet" => preg_replace("/[\n\r]/", " ", $this->String->troncateStr($flux['objet'], 90)),
                    "contact" => $flux['contact'],
                    "date" => $valdate, //FIXME
                    "typesoustype" => $flux['typesoustype'],
                    "bureau" => $flux['bureau'],
                    "commentaire" => $flux['commentaire'],
                    "modified" => $this->_convertDateTime($flux['modified']), //FIXME
                    "edit" => $this->_convertEdit($flux['view'], $flux['id'], $flux['desktopId']), //Arnaud
                    "delete" => $this->_convertDelete($flux['delete'], $flux['id'], $flux['isParent'], $flux['creatorId'], $flux['userconnectedId'], $bannetteName),
                    "detache" => $this->_convertDetache($flux['detache'], $flux['id'], $flux['desktopId']), // Arnaud +
                    'isParent' => $flux['isParent'],
                    'hasParent' => !empty($flux['parent_id']),
                    'parentInfo' => $this->_convertParentInfo($flux['parentInfo'], $flux['id'])
                );
            }
        }

        return $return;
    }

    /**
     *
     * @param type $parentInfo
     */
    private function _convertParentInfo($parentInfo, $fluxId) {
        $return = '';
        if (!empty($parentInfo)) {
            $tableHead = '';
            $tableBody = '';
            $thContent = '';
            $trContent = '';
            $tableTitle = '';

            $tableTitle .= 'Informations sur le flux original';
            $thContent .= $this->Html->tag('th', __d('courrier', 'Courrier.reference'));
            $thContent .= $this->Html->tag('th', __d('courrier', 'Courrier.name'));
            $thContent .= $this->Html->tag('th', __d('courrier', 'Courrier.objet'));
            $thContent .= $this->Html->tag('th', __d('courrier', 'Courrier.contact'));

            $thContent .= $this->Html->tag('th', 'Date de réception');
            $thContent .= $this->Html->tag('th', __d('bannette', 'Courrier.typesoustype'));
            $thContent .= $this->Html->tag('th', __d('bannette', 'Courrier.bureau'));
            $thContent .= $this->Html->tag('th', __d('bannette', 'Courrier.commentaire'));
            $thContent .= $this->Html->tag('th', __d('bannette', 'Courrier.modified'));
            $tableHead .= $this->Html->tag('thead', $this->Html->tag('tr', $thContent));

            $trContent .= $this->Html->tag('td', $parentInfo['Courrier']['reference']);
            $trContent .= $this->Html->tag('td', $parentInfo['Courrier']['name']);
            $trContent .= $this->Html->tag('td', '' );
            $trContent .= $this->Html->tag('td', $parentInfo['Contact']['name']);
            $trContent .= $this->Html->tag('td', $this->_convertDate($parentInfo['Courrier']['datereception']));
            $trContent .= $this->Html->tag('td', $parentInfo['Type']['name'] . ' / ' . $parentInfo['Soustype']['name']);
            $trContent .= $this->Html->tag('td', $parentInfo['Desktop']['name']);
            $tableBody .= $this->Html->tag('tbody', $this->Html->tag('tr', $trContent));

            $return = $this->Html->tag(
                    'div', $this->Html->tag(
                            'table', $this->Html->tag(
                                    'caption', $tableTitle, array('style' => 'font-weight: bold;')
                            )
                            . $tableHead . $tableBody, array("data-toggle" => "table", "data-locale" => "fr-CA")
                    ), array('class' => 'parentInfo', 'id' => 'flux_' . $fluxId, 'style' => 'display:none')
            );
        }
        return $return;
    }

    /**
     *
     * @param type $bannette
     * @return type
     */
    private function _convertBannette($bannette, $date, $bannetteName = null) {
        for ($i = 0; $i < count($bannette); $i++) {
            $bannette[$i] = $this->_convertFlux($bannette[$i], $date, $bannetteName);
        }
        return $bannette;
    }

    public function drawPaginator($courriers, $titre, $actions = array(), $checkable = false) {

        $controller = Hash::get($this->request->params, 'controller'); // FIXME
        $nbItem = Hash::get($this->request->params, 'paging.Courrier.count');
        if ($controller == 'repertoires') { // FIXME
            $nbItem = Hash::get($this->request->params, 'paging.CourrierRepertoire.count'); // FIXME
        } // FIXME
        $nbAction = count($actions);
        /* Pagination */
        $pagination = null;
        $idRecherche = "#infos";
        if ($controller == 'recherches') {
            $idRecherche = "#recherche_skel_infos";
        } else if ($controller == 'repertoires') {
            $idRecherche = "#repertoires_skel_infos";
        }
        $this->Paginator->options(array(
            'update' => "$idRecherche .content",
            'evalScripts' => true,
            'before' => 'gui.loader({ element: $("' . $idRecherche . ' .content"), message: gui.loaderMessage });',
            'complete' => '$(".loader").remove()',
//                				'url' => $this->passedArgs,
            'url' => $this->request->params['pass'],
            'data' => http_build_query($this->request->data),
            'method' => 'POST'
        ));


        //TODO:résodre le problème de sort(not work)
        if ($nbItem > 1) {
            $optionsNumber['separator'] = false;
            $pagination = '<ul class="pagination">' . implode('', Set::filter(array(
                                $this->Html->tag('li', $this->Paginator->first('<<', $optionsNumber, null, array('class' => 'disabled'))),
                                $this->Html->tag('li', $this->Paginator->prev('<', array(), null, array('class' => 'disabled')), array('class' => 'previous')),
                                $this->Html->tag('li', $this->Paginator->numbers($optionsNumber)),
                                $this->Html->tag('li', $this->Paginator->next('>', array(), null, array('class' => 'disabled')), array('class' => 'previous')),
                                $this->Html->tag('li', $this->Paginator->last('>>', $optionsNumber, null, array('class' => 'disabled')))))) . '
                                                    </ul>';
        }
        if (!empty($pagination)) {
            return $pagination;
        }
        /* FIN Pagination */
    }

    public function drawTableHead($courriers, $actions = array(), $checkable = false) {
        $idSuffix = str_replace(array(' ', '.'), '', microtime());
        $controller = Hash::get($this->request->params, 'controller'); // FIXME
        $nbAction = count($actions);

        $ths = "";
        $ths .= $this->Html->tag('th', __d('courrier', 'Courrier.reference'), array('class' => 'reference tri', 'data-sortable' => 'true', 'data-field' => 'reference'));
        $ths .= $this->Html->tag('th', __d('bannette', 'Courrier.direction'), array('class' => 'direction tri', 'data-sortable' => 'true', 'data-field' => 'direction'));
        $ths .= $this->Html->tag('th', __d('bannette', 'Courrier.etat'), array('class' => 'etat tri', 'data-sortable' => 'true', 'data-field' => 'etat'));
        $ths .= $this->Html->tag('th', __d('courrier', 'Courrier.intitule'), array('class' => 'nom tri', 'data-sortable' => 'true', 'data-field' => 'nom', 'data-width' => '220'));
        $ths .= $this->Html->tag('th', __d('courrier', 'Courrier.objet'), array('class' => 'objet tri', 'data-sortable' => 'true', 'data-field' => 'objet'));
        $ths .= $this->Html->tag('th', __d('courrier', 'Courrier.contact_id'), array('class' => 'expediteur tri', 'data-sortable' => 'true', 'data-field' => 'expediteur'));
        $ths .= $this->Html->tag('th', __d('courrier', 'Desktop.name'), array('class' => 'agenttraitant tri', 'data-sortable' => 'true', 'data-field' => 'agenttraitant'));
        $ths .= $this->Html->tag('th', __d('recherche', 'Courrier.datereception'), array('class' => 'datereception tri', 'data-sortable' => 'true', 'data-field' => 'datereception'));
        $ths .= $this->Html->tag('th', __d('courrier', 'Courrier.retard'), array('class' => 'retard tri delairetard', /* 'data-sortable' => 'true',  */ 'data-field' => 'retard', 'data-align' => 'center', 'data-width' => '80px'));
        if ($nbAction != 0) {
            $ths .= $this->Html->tag('th', 'Actions', array('class' => 'actions thView', 'data-field' => 'view', 'data-align' => 'center', 'data-width' => '80px'));
        }
        if ($checkable) {
            $ths .= $this->Html->tag('th', $value, array('class' => 'state', 'data-sortable' => 'true', 'data-field' => 'state', 'data-checkbox' => 'true', 'id' => 'thCheckable_' . $idSuffix));
        }
        return $ths;
    }

    public function drawTableTd($courriers, $actions = array(), $checkable = false) {
        if (!empty($courriers)) {
            $drawTds = array();
            foreach ($courriers as $flux) {
                array_push($drawTds, $this->_convertDrawFlux($flux, $actions, $checkable));
            }
            return json_encode($drawTds);
        }
    }

    private function _convertDrawFlux($courrier, $actions = array(), $checkable = false) {

        $contactInfos = '';
        if (!empty($courrier['Organisme']['name']) && !empty($courrier['Contact']['name'])) {
            if ($courrier['Organisme']['id'] == Configure::read('Sansorganisme.id')) {
                $contactInfos = $courrier['Contact']["name"];
            } else {
                $contactInfos = $courrier['Organisme']['name'] . ' / ' . $courrier['Contact']["name"];
            }
        }
        if (!empty($courrier['Organisme']['name']) && empty($courrier['Contact']['name'])) {
            $contactInfos = $courrier['Organisme']["name"];
        }
        if (empty($courrier['Organisme']['name']) && !empty($courrier['Contact']['name'])) {
            $contactInfos = $courrier['Contact']["name"];
        }

        $idSuffixcreated = strtotime($courrier['Courrier']['datereception']);

        $retard = "";
		if( $courrier['Bancontenu']['etat'] != 2 ) {
			if (!empty($courrier['delairetard']) && $courrier['delairetard'] > 0 ) {
				$alt = $title = __d('courrier', 'Courrier.alertRetard');
				if (isset($courrier['delairetard']) && !empty($courrier['delairetard'])) {
					$alt = $title = __d('courrier', 'Courrier.alertRetard') . ' de ' . $courrier['delairetard'] . ' jours';
				}
				$retard = $this->Html->image("/img/retard_16.png", array('height' => '16px;', 'style' => 'vertical-align: middle;', 'alt' => $alt, 'title' => $title, 'class' => 'delairetard'));
			}
			if (isset($courrier['delairetard']) && !empty($courrier['delairetard'])) {
				$delai = ' J' . $courrier['delairetard'];
				$retard = $retard . $delai;
			}
		}
//        if (isset($courrier['retard']) && $courrier['retard']) {
//            $retard = $this->Html->image("/img/retard_16.png", array('height' => '16px;', 'style' => 'vertical-align: middle;', 'alt' => __d('courrier', 'Courrier.alertRetard'), 'title' => __d('courrier', 'Courrier.alertRetard'), 'class' => 'delairetard'));
//        }
//        if( isset($courrier['delairetard']) && !empty($courrier['delairetard']) ) {
//			$delai = ' J'.$courrier['delairetard'];
//			$retard = $retard.$delai;
//        }
        $view = "";
        if (isset($courrier['right_read'])) {
            if ($courrier['right_read']) {
                $view = $this->Html->tag('i', '', array(
                        'title' => __d('default', 'Button.view'),
                        'alt' => __d('default', 'Button.view'),
                        'class' => "fa fa-eye"
                    )
                );
            }
        }

        $delete = "";
        if (isset($courrier['right_delete'])) {
			$delete = $this->Html->link($this->Html->tag('i', '', array(
						'title' => __d('default', 'Button.delete'),
						'alt' => __d('default', 'Button.delete'),
						'class' => "fa fa-trash",
						'style' => 'color: #FF0000;cursor:pointer')), array(
				'controller' => 'courriers',
				'action' => $actions['delete'],
				$courrier['Courrier']['id']
			));
        }

        mb_internal_encoding('UTF-8');
        $current_encoding = mb_detect_encoding($courrier['Courrier']['objet'], 'auto');
        $text = iconv($current_encoding, 'UTF-8', $courrier['Courrier']['objet']);
        $objet = $text;

        $infoparent = "";
        if (!empty($courrier['Courrier']['parent_id'])) {
            $infoparent = '&nbsp; <i class="fa fa-hand-o-right" aria-hidden="true" id="hand_' . $courrier['Courrier']['id'] . '"></i> &nbsp;&nbsp;';
            $infoparent .=$this->_convertParentInfo($courrier['parentInfo'], $courrier['Courrier']['id']);
        }

		if( strpos( $objet, '<!DOCTYPE') !== false || strpos( $objet, '<html>') !== false || strpos( $objet, '--_000') !== false ) {
			$objet = strip_tags( $objet, "<html>" );
			$substring = substr($objet ,strpos($objet ,"<style"),strpos($objet ,"</style>"));
			$objet = str_replace($substring,"",$objet);
			$objet = trim($objet);
		}

		if( !empty( $objet) ) {
			if( !Configure::read('ScanMail.GetObjet') ) {
				$objet = quoted_printable_decode($objet);
			}
			else {
				$objet = replace_accents(substr($objet, 0, 90)) . '...';
			}
		}


		$agentEncours = implode( "\n ", $courrier['agenttraitant'] );
        $return = array(
            'reference' => $courrier['Courrier']["reference"],
            'direction' => $this->_convertDirection($courrier['Courrier']["direction"]),
            "etat" => $this->_convertEtat($courrier['Bancontenu']['etat'], $courrier['Bancontenu']['bannette_id']),
            'nom' => $courrier['Courrier']['name'] .  $infoparent . $this->Form->input('checkItem_' . $courrier['Courrier']['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $courrier['Courrier']['id'])),
            'objet' => $objet,
            'expediteur' => $contactInfos,
            'agenttraitant' => !empty($agentEncours) ? " ".nl2br($agentEncours) : null,
            'datereception' => strftime("%d/%m/%Y", $idSuffixcreated),
            'retard' => $retard,
            'view' => $view,
            'delete' => $delete,
            'state' => '',
            'parentInfo' => $this->_convertParentInfo($courrier['parentInfo'], $courrier['Courrier']['id'])
        );
        return $return;
    }

    public function bannetteTableTd($bannetteContent, $bannetteName, $optionsnew) {
        $options = array("sortable" => false,
            "checkable" => false,
            "filter" => false,
            "translateBannetteName" => true,
            "date" => "datereception",
            "disp" => false,
            "unwantedFields" => array());
        foreach ($optionsnew as $key => $value) {
            $options[$key] = $value;
        }
        if (!empty($bannetteContent)) {
            $bannetteTds = array();
            foreach ($bannetteContent as $flux) {
                array_push($bannetteTds, $this->_convertFlux($flux, $options["date"], $bannetteName));
            }
            return json_encode($bannetteTds);
        }
    }

    public function bannetteHead($bannetteContent, $bannetteName, $options = array("sortable" => false, "checkable" => false, "filter" => false, "translateBannetteName" => true, "date" => "datereception", "disp" => false, "unwantedFields" => array())) {
        $isAdmin = false;
		$visible = 'false';
        if( Configure::read('Affichage.Colonnesup') ) {
			$visible = 'true';
		}
        $userProfils = array( CakeSession::read('Auth.User.Env.main') );
        $profils = CakeSession::read('Auth.User.Env.secondary');
        foreach( $profils as $name ) {
            $userProfils[] = $name;
        }
        if (in_array( 'admin', $userProfils )) {
            $isAdmin = true;
        }


        if (!isset($options['disp'])) {
            $options['disp'] = false;
        }
        if (!empty($bannetteContent)) {
            foreach ($bannetteContent as $flux) {
                $deletable[] = false;
                if( Configure::read( 'Suppression.Ok') ) {
                    if (!$options['disp']) { // Si non aiguillage, suppression uniquement pour le propriétaire du flux
                        if ($flux['isParent'] || $flux['creatorId'] != $flux['userconnectedId'] || !$flux['delete']) {
                            $deletable[] = false;
                        } else {
                            $deletable[] = true;
                        }
                        // Pour les flux issus des mails
                        if(empty( $flux['creatorId'])) {
                            $deletable[] = true;
                        }
                    } else {
                        $deletable[] = true;
                    }

                    if($flux['isInCircuit']) {
                        $deletable[] = false;
                    }
                }
                else if($isAdmin) {
                    $deletable[] = true;
                }
            }
//debug($deletable);
            $deletable = in_array(true, $deletable);
			if( Configure::read( 'Suppression.Permanente' ) ){
				$deletable = true;
			}
			$bannetteHead = $this->_convertFluxHead(@$bannetteContent[0], $options["date"], $bannetteName);
			$checkable = $options['checkable'];
            $unwantedFields = array();
            if (!empty($options['unwantedFields'])) {
                foreach ($options['unwantedFields'] as $field) {
                    $unwantedFields[] = __d('bannette', 'Courrier.' . $field);
                }
            }
            $ths = "";
            if ($checkable && $bannetteName != 'services') {
                $ths .= $this->Html->tag('th', '', array('class' => 'bs-checkbox', 'data-field' => 'state', 'data-checkbox' => 'true'));
            }
            foreach ($bannetteHead as $key => $value) {
                if ($key != "id" && $key != "creatorId" && $key != "mailRetardEnvoye" && $key != "userconnectedId" && $key != "read" && $key != "desktopId" && $key != "hasParent" && $key != "isParent" && $key != "parentInfo" && !in_array($key, $unwantedFields)) { //Arnaud
                    if ($key == "priorite") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thPriorite thState', 'data-sortable' => 'true', 'data-field' => 'priorite', 'data-visible' => 'true', 'data-switchable' => 'true', 'data-align' => 'center', 'data-width' => '40px'));
                    } else if ($key == "retard" && !Configure::read('Conf.SAERP')) {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thRetard thState', 'data-sortable' => 'true', 'data-field' => 'retard', 'data-visible' => 'true', 'data-align' => 'center', 'data-width' => '40px'));
                    } else if ($key == "ficassoc") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thFicassoc thState', 'data-sortable' => 'true', 'data-field' => 'ficassoc', 'data-visible' => 'false', 'data-align' => 'center', 'data-width' => '40px'));
                    } else if ($key == "direction") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thDirection thState', 'data-sortable' => 'true', 'data-field' => 'direction', 'data-visible' => 'true', 'data-switchable' => 'true', 'data-align' => 'center', 'data-width' => '40px'));
                    } else if ($key == "etat") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thEtat thState', 'data-sortable' => 'true', 'data-field' => 'etat', 'data-visible' => 'true', 'data-align' => 'center', 'data-width' => '40px'));
                    } else if ($key == "reference") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thRef', 'data-sortable' => 'true', 'data-field' => 'reference', 'data-switchable' => 'false'));
                    } else if ($key == "nom") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thName','data-sortable' => 'true',  'data-field' => 'nom', 'data-switchable' => 'false', 'data-width' => '220'));
                    } else if ($key == "objet") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thObjet', 'data-sortable' => 'true', 'data-field' => 'objet', 'data-switchable' => 'false'));
                    } else if ($key == "contact") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thContact', 'data-sortable' => 'true', 'data-field' => 'contact', 'data-visible' => 'true'));
                    } else if ($key == "date") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thDate', 'data-order' => 'desc', 'data-sortable' => 'true', 'data-field' => 'date', 'data-visible' => 'true'));
                    } else if ($key == "typesoustype") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thTypesoustype', 'data-field' => 'typesoustype', 'data-sortable' => 'true', 'data-switchable' => 'false'));
                    } else if ($key == "bureau") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thBureau', 'data-field' => 'bureau', 'data-sortable' => 'true', 'data-visible' => $visible) );
                    } else if ($key == "commentaire") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thCommentaire', 'data-field' => 'commentaire', 'data-sortable' => 'true', 'data-visible' => 'true'));
                    } else if ($key == "modified") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thActions thModified', 'data-field' => 'modified', 'data-sortable' => 'true', 'data-visible' => 'false', 'data-align' => 'center', 'data-width' => '80px'));
                    } else if ($key == "view") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thActions thView', 'data-field' => 'view', 'data-switchable' => 'false', 'data-align' => 'center', 'data-width' => '80px'));
                    } else if ($key == "previewdocument") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thActions thPreviewDocument', 'data-field' => 'previewdocument', 'data-switchable' => 'false', 'data-align' => 'center', 'data-width' => '80px'));
                    } else if ($key == "edit") {
                        $ths .= $this->Html->tag('th', $value, array('class' => 'thActions thEdit', 'data-field' => 'edit', 'data-switchable' => 'false', 'data-align' => 'center', 'data-width' => '80px'));
                    } else if ($key == "delete") {
                        if ($deletable && $bannetteName != 'flux') {
                            $ths .= $this->Html->tag('th', $value, array('class' => 'thActions thdelete', 'data-field' => 'delete', 'data-switchable' => 'false', 'data-align' => 'center', 'data-width' => '80px'));
                        }
                    } else if ($key == "detache") {
                        if (!($options['disp'] || in_array($bannetteName, array('services', 'docs', 'refus', 'reponses', 'flux', 'infos')) || ( $this->action == 'historique' ))) { // Détachement du flux uniquement au niveau de la bannette copie
                            $ths .= $this->Html->tag('th', $value, array('class' => 'thActions thdetache', 'data-field' => 'detache', 'data-switchable' => 'false', 'data-align' => 'center', 'data-width' => '80px'));
                        }
                    }
                }
            }

            return $ths;
        }
    }

    private function _convertFluxHead($flux, $date, $bannetteName = null) {
        if ($date == 'datereception') {
            $keydate = __d('bannette', 'Courrier.datereception');
        } else if ($date == 'created') {
            $keydate = __d('bannette', 'Courrier.created');
        }

        $alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));

        $options = array(
            'model' => $alias,
//            'url' => (array) $this->passedArgs + array('model' => $alias),
            'url' => array($bannetteName) + array('model' => $alias)
        );
        if ($this->request->params['action'] == 'dispenv') {
            if ($bannetteName == 'aiguillage') {
                $return = array(
                    "id" => "id",
                    "desktopId" => "desktopId",
                    "creatorId" => "creatorId",
                    "userconnectedId" => "userconnectedId",
                    "read" => "read",
                    "priorite" => __d('bannette', 'Courrier.priorite'),
                    "ficassoc" => __d('bannette', 'Courrier.fichier_associe'),
                    "direction" => __d('bannette', 'Courrier.direction'),
                    "etat" => __d('bannette', 'Courrier.etat'),
                    "reference" => __d('bannette', 'Courrier.reference'),
                    "nom" => __d('bannette', 'Courrier.name'),
                    "contact" => __d('bannette', 'Courrier.contact'),
                    "date" => $keydate,
                    "typesoustype" => __d('bannette', 'Courrier.typesoustype'),
                    "bureau" => __d('bannette', 'Courrier.bureau'),
                    "commentaire" => __d('bannette', 'Courrier.commentaire'),
                    "modified" => __d('bannette', 'Courrier.modified'),
                    "previewdocument" => __d('bannette', 'Courrier.preview'),
                    "edit" => __d('bannette', 'Courrier.edit'), //Arnaud
                    "delete" => __d('bannette', 'Courrier.delete'),
                    "detache" => __d('bannette', 'Courrier.detache'), //Arnaud +
                    "isParent" => "isParent",
                    "hasParent" => "hasParent",
                    "parentInfo" => "parentInfo"
                );
            } else {
                if ($bannetteName == 'copy') {
                   $return = array(
                        "id" => "id",
                        "desktopId" => "desktopId",
                        "creatorId" => "creatorId",
                        "userconnectedId" => "userconnectedId",
                        "read" => "read",
                        "priorite" => __d('bannette', 'Courrier.priorite'),
                        "ficassoc" => __d('bannette', 'Courrier.fichier_associe'),
                        "direction" => __d('bannette', 'Courrier.direction'),
                        "etat" => __d('bannette', 'Courrier.etat'),
                        "reference" => __d('bannette', 'Courrier.reference'),
                        "nom" => __d('bannette', 'Courrier.name'),
                        "contact" => __d('bannette', 'Courrier.contact'),
                        "date" => $keydate,
                        "typesoustype" => __d('bannette', 'Courrier.typesoustype'),
                        "bureau" => __d('bannette', 'Courrier.bureau'),
                        "commentaire" => __d('bannette', 'Courrier.commentaire'),
                        "modified" => __d('bannette', 'Courrier.modified'),
                        "previewdocument" => __d('bannette', 'Courrier.preview'),
                        "view" => __d('bannette', 'Courrier.view'), //Arnaud
                        "delete" => __d('bannette', 'Courrier.delete'),
                        "detache" => __d('bannette', 'Courrier.detache'), //Arnaud +
                        "isParent" => "isParent",
                        "hasParent" => "hasParent",
                        "parentInfo" => "parentInfo"
                    );
                }
                else {
                    $return = array(
                        "id" => "id",
                        "desktopId" => "desktopId",
                        "creatorId" => "creatorId",
                        "userconnectedId" => "userconnectedId",
                        "read" => "read",
                        "priorite" => __d('bannette', 'Courrier.priorite'),
                        "ficassoc" => __d('bannette', 'Courrier.fichier_associe'),
                        "direction" => __d('bannette', 'Courrier.direction'),
                        "etat" => __d('bannette', 'Courrier.etat'),
                        "reference" => __d('bannette', 'Courrier.reference'),
                        "nom" => __d('bannette', 'Courrier.name'),
                        "contact" => __d('bannette', 'Courrier.contact'),
                        "date" => $keydate,
                        "typesoustype" => __d('bannette', 'Courrier.typesoustype'),
                        "bureau" => __d('bannette', 'Courrier.bureau'),
                        "commentaire" => __d('bannette', 'Courrier.commentaire'),
                        "modified" => __d('bannette', 'Courrier.modified'),
                        "edit" => __d('bannette', 'Courrier.edit'),
                        "previewdocument" => __d('bannette', 'Courrier.preview'),
                        "delete" => __d('bannette', 'Courrier.delete'),
                        "detache" => __d('bannette', 'Courrier.detache'), //Arnaud +
                        "isParent" => "isParent",
                        "hasParent" => "hasParent",
                        "parentInfo" => "parentInfo"
                    );
                }
            }
        } else {
            if( $bannetteName == 'copy' || $this->request->params['action'] == 'historique') {
                $return = array(
                    "id" => "id",
                    "desktopId" => "desktopId",
                    "creatorId" => "creatorId",
                    "userconnectedId" => "userconnectedId",
                    "read" => "read",
                    "priorite" => $this->Html->tag('span', __d('bannette', 'Courrier.priorite'), array('class' => 'tri_Courrier.priorite', 'escape' => false)),
                    "retard" => $this->Html->tag('span', __d('bannette', 'Courrier.retard'), array('class' => 'tri_Courrier.mail_retard_envoye delairetard', 'escape' => false)),
                    "ficassoc" => __d('bannette', 'Courrier.fichier_associe'),
                    "direction" => __d('bannette', 'Courrier.direction'),
                    "etat" => __d('bannette', 'Courrier.etat'),
                    "reference" => $this->Html->tag('span', __d('bannette', 'Courrier.reference'), array('class' => 'tri_Courrier.reference', 'escape' => false)),
                    "nom" => __d('bannette', 'Courrier.name'),
                    "objet" => __d('bannette', 'Courrier.objet'),
                    "contact" => $this->Html->tag('span', __d('bannette', 'Courrier.contact'), array('class' => 'tri_Organisme.name', 'escape' => false)),
                    "date" => $this->Html->tag('span', $keydate, array('class' => 'tri_Courrier.datereception', 'escape' => false)),
                    "typesoustype" => $this->Html->tag('span', __d('bannette', 'Courrier.typesoustype'), array('class' => 'tri_Type.name', 'escape' => false)),
                    "bureau" => $this->Html->tag('span', __d('bannette', 'Courrier.bureau'), array('class' => 'tri_Desktop.name', 'escape' => false)),
                    "commentaire" => $this->Html->tag('span', __d('bannette', 'Courrier.commentaire'), array('class' => 'tri_Courrier.commentaire', 'escape' => false)),
                    "modified" => $this->Html->tag('span', __d('bannette', 'Courrier.modified'), array('class' => 'tri_Courrier.modified', 'escape' => false)),
                    "view" => __d('bannette', 'Courrier.view'),
                    "delete" => __d('bannette', 'Courrier.delete'),
                    "detache" => __d('bannette', 'Courrier.detache'), //Arnaud +
                    "isParent" => "isParent",
                    "hasParent" => "hasParent",
                    "parentInfo" => "parentInfo"
                );
            }
            else {
                $return = array(
                    "id" => "id",
                    "desktopId" => "desktopId",
                    "creatorId" => "creatorId",
                    "userconnectedId" => "userconnectedId",
                    "read" => "read",
                    "priorite" => $this->Html->tag('span', __d('bannette', 'Courrier.priorite'), array('class' => 'tri_Courrier.priorite', 'escape' => false)),
                    "retard" => $this->Html->tag('span', __d('bannette', 'Courrier.retard'), array('class' => 'tri_Courrier.mail_retard_envoye delairetard', 'escape' => false)),
                    "ficassoc" => __d('bannette', 'Courrier.fichier_associe'),
                    "direction" => __d('bannette', 'Courrier.direction'),
                    "etat" => __d('bannette', 'Courrier.etat'),
                    "reference" => $this->Html->tag('span', __d('bannette', 'Courrier.reference'), array('class' => 'tri_Courrier.reference', 'escape' => false)),
                    "nom" => __d('bannette', 'Courrier.name'),
                    "objet" => __d('bannette', 'Courrier.objet'),
                    "contact" => $this->Html->tag('span', __d('bannette', 'Courrier.contact'), array('class' => 'tri_Organisme.name', 'escape' => false)),
                    "date" => $this->Html->tag('span', $keydate, array('class' => 'tri_Courrier.datereception', 'escape' => false)),
                    "typesoustype" => $this->Html->tag('span', __d('bannette', 'Courrier.typesoustype'), array('class' => 'tri_Type.name', 'escape' => false)),
                    "bureau" => $this->Html->tag('span', __d('bannette', 'Courrier.bureau'), array('class' => 'tri_Desktop.name', 'escape' => false)),
                    "commentaire" => $this->Html->tag('span', __d('bannette', 'Courrier.commentaire'), array('class' => 'tri_Courrier.commentaire', 'escape' => false)),
                    "modified" => $this->Html->tag('span', __d('bannette', 'Courrier.modified'), array('class' => 'tri_Courrier.modified', 'escape' => false)),
                    "edit" => __d('bannette', 'Courrier.edit'),
                    "delete" => __d('bannette', 'Courrier.delete'),
                    "detache" => __d('bannette', 'Courrier.detache'), //Arnaud +
                    "isParent" => "isParent",
                    "hasParent" => "hasParent",
                    "parentInfo" => "parentInfo"
                );
            }
        }

        return $return;
    }

    public function importScript() {
        echo $this->Html->script('bannette.js');
    }

    public function bannettePaginator($bannetteName) {
        /* Pagination */
        // Paramètres pour la pagination
        $pagination = null;
        $alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));
        $path = "paging.{$alias}.pageCount";
        $pageCount = Set::classicExtract($this->params, $path);
        $options = array(
            'model' => $alias,
            'url' => (array) $this->passedArgs + array('model' => $alias),
        );
        $optionsNumber = array(
            'model' => $alias,
            'url' => (array) $this->request->params['pass'] + array('model' => $alias),
            'separator' => false,
        );
        if ($pageCount > 1) {
            $pagination = '<ul class = "' . $bannetteName . '_pagination pagination">' . implode('', Set::filter(array(
                                $this->Html->tag('li', $this->Paginator->first('<<', $optionsNumber, null, array('class' => 'disabled'))),
                                $this->Html->tag('li', $this->Paginator->prev('<', $options, null, array('class' => 'disabled')), array('class' => 'previous')),
                                $this->Html->tag('li', $this->Paginator->numbers($optionsNumber)),
                                $this->Html->tag('li', $this->Paginator->next('>', $options, null, array('class' => 'disabled')), array('class' => 'previous')),
                                $this->Html->tag('li', $this->Paginator->last('>>', $optionsNumber, null, array('class' => 'disabled')))))) . '
        </ul>';
        }
        return $pagination;
    }

}

?>
