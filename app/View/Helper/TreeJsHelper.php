<?php

/**
 *
 * TreeJs helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class TreeJsHelper extends AppHelper {

	/**
	 *
	 * @var type
	 */
	var $tabstart = "&nbsp;&nbsp;&nbsp;&nbsp;";

	/**
	 *
	 * @var type
	 */
	var $tabend = "";

	/**
	 *
	 * @var type
	 */
	var $hasChildren = false;

	/**
	 *
	 *
	 *  $action = array(
	 *          'src' => "/un/chemin/vers/icone.ext"
	 *          'alt' => "un texte qui apparaitra en alt et title"
	 *          'url' => "/controller/action/param"
	 *          'msg' => "nom du titre de openModal"
	 *          'type' => 'del' / 'add' / 'mod'
	 *          'ajaxAction' => "fonction ajax"
	 *
	 * @param type $data
	 * @param type $model
	 * @param type $field
	 * @param type $actions
	 * @param type $lvl
	 * @return type
	 */
	function tree($data, $model, $field, $actions, $lvl = 0) {
		$output = "<table>";
		if ($lvl == 0) {
			$output .= "<thead><th></th><th>" . $field . "</th><th class='actions' colspan='" . count($actions) . "'>" . __d('default', 'Actions') . "</th></thead>";
		}
		$output .= "<tbody>";

		foreach ($data as $item) {
			$hasChildren = isset($item['children'][0]);

			//ligne du tableau
			$output .= "<tr class='bottomLine' id='service_" . $item[$model]['id'] . "' onmouseover='$(this).addClass(\"hilight\");hilightServiceReferences(\"" . $item[$model]['id'] . "\" ,true);' onmouseout='$(this).removeClass(\"hilight\");hilightServiceReferences(\"" . $item[$model]['id'] . "\" ,false);'>";

			//icone de deploiement
			$output .= "<td class='icoarbo' width='20'>";
			if ($hasChildren) {
				$output .= "<img src='" . Configure::read('BaseUrl') . "/img/icons/replier.png' alt='Déplier' title='Déplier' onclick='$(\"#srv_" . $item[$model]['id'] . "\").slideToggle();toggleFoldingIcon(\"service_" . $item[$model]['id'] . "\");' style='cursor: pointer;' />";
			}
			$output .= "</td>";

			//nom du champ
			$output .= "<td>" . $item[$model][$field] . "</td>";

			//actions
			foreach ($actions as $action) {
				$output .= "<td class='action'>";
				$output .= "<img height='24px' width='24px'
                            src='" . Configure::read('BaseUrl') . $action["src"] . "'
                            alt='" . $action['alt'] . "'
                            title='" . $action['alt'] . "'";

				if ($action['type'] == 'del') {
					if ($hasChildren) {
						$output .= "onclick='alert(\"Impossible de supprimer ce service.\\nDes services en dépendent.\");'";
					} else {
						$output .= "onclick='if (confirm(\"" . $action['confirmMsg'] . "\")) { ajaxLink(\"" . Configure::read('BaseUrl') . $action['url'] . $item[$model]['id'] . "\", \"" . $action['ajaxAction'] . "\", " . $action['ajaxLoader'] . "); }'";
					}
				} else {
					$output .= "onclick='openModal(\"" . Configure::read('BaseUrl') . $action['url'] . $item[$model]['id'] . "\", \"" . $action['msg'] . "\");'";
				}


				$output .= "/>";
				$output .= "</action>";
			}
			$output .= "</tr>";

			//si il y a des enfants, on fait une nouvelle ligne contenant la table enfant
			if ($hasChildren) {
				$lvl++;
				$output .= "<tr><td colspan='" . (count($actions) + 2) . "'><div id='srv_" . $item[$model]['id'] . "' style='padding-left: 15px;'>";
				$output .= $this->tree($item['children'], $model, $field, $actions, $lvl);
				$output .= "</div></td></tr>";
			}
		}
		return $output . "</tbody></table>";
	}

	/**
	 *
	 * @param type $baseUrl
	 * @param type $actions
	 * @param type $modelName
	 * @param type $id
	 * @param type $hasChildren
	 * @return type
	 */
	function buildActions($baseUrl, $actions, $modelName, $id, $hasChildren) {
		$ret = "";
		if (!empty($actions)) {
			$urls = "";
			foreach ($actions as $key => $value) {
				$controllerName = Inflector::pluralize(strtolower($modelName));
				if (preg_match("/\/webgfc\/.*/", $_SERVER["REQUEST_URI"])) {
					$urlToAdd = "<td><a href=\"" . $baseUrl . "/" . Inflector::camelize($controllerName) . "/" . $value . "/" . $id . "\" >" . $key . "</a></td>";
				} else {
					$urlToAdd = "<td><a href=\"/" . Inflector::camelize($controllerName) . "/" . $value . "/" . $id . "\" >" . $key . "</a></td>";
				}
				$urls .= $urlToAdd;
			}
			$ret = substr($urls, 0, -3);
		}
		return $ret;
	}

}

?>
