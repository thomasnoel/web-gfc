<?php

/**
 *
 * Bannette helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class FormulaireHelper extends Helper {

    /**
     *
     * @var type
     */
    public $helpers = array('String', 'Html', 'Form', 'Paginator', 'Js');

    /**
     * Pour créer votre formulaire, il faut réaliser un array qui contient les infos nécessaire
     *
     * un model de creation formulaire
     *
     * $formNom = array(
     *      'name' => 'nomDeForm', //(oblige)
     *      'label_w' => 'col-sm-*', //(oblige : largeur de label avec facon de bootstrap Grid system)
     *      'input_w' =>'col-sm-*', //(oblige : largeur de input avec facon de bootstrap Grid system)
     *      'form_url' =>array('controller'=>'votrecontroller' ,'action' => 'votreaction' , $parametre),
     *      'form_target' => 'modelUploadFrame',
     *      'form_type' => 'post',
     *      'form_class' => 'si_vous_voulez_ajouter_un_class_personnelle',
     *      'input' => array(            //(oblige : indiquer votre champs de form)
     *          'nom_de_champ_text' => array (
     *              'labelText' => 'votre text de label',
     *              'inputType' => 'text',
     *              'labelPlaceholder' => 'votre placeholder', //(pas oblige)
     *              'changeWidth' =>true/false, //(si changement largeur input & label),
     *              'items' => array(
     *                  'type' => 'text',
     *                  'required' => true, //(si required)
     *                  'class' => 'class_personnelle_input', //(si besoin)
     *                  'value' => 'value_input', //(si besoin)
     *                  'new_label_w' => 'col-sm-*', //(si changeWidth == ture)
     *                  'new_input_w' => 'col-sm-*', //(si changeWidth == ture)
     *              )
     *          ),
     *          'nom_de_champ_select' => array (
     *              'labelText' => 'votre text de label',
     *              'inputType' => 'select',
     *              'labelPlaceholder' => 'votre placeholder', //(pas oblige)
     *              'changeWidth' =>true/false, //(si changement largeur input & label),
     *              'items' => array(
     *                  'type' => 'select',
     *                  'required' => true, //(si required)
     *                  'class' => 'class_personnelle_input', //(si besoin)
     *                  'options' => $votreOptions,
     *                  'empty' => true, //(si besoin)
     *                  'multiple' => true, //(si multiple)
     *                  'selected' => $valueSelected
     *              )
     *          ),
     *         'nom_de_champ_hidden' => array (
     *              'inputType' => 'hidden',
     *              'items' => array(
     *                  'type' => 'hidden',
     *                  'value' => 'value_input', //(si besoin)
     *              )
     *          ),
     *          'nom_de_champ_radio' => array (
     *              'labelText' => '',
     *              'inputType' => 'radio',
     *              'items' => array(
     *                  'type' => 'radio',
     *                  'legend' => false,
     *                  'separator'=> '</div><div  class="radioUserGedLabel">',
     *                  'before' => '<div class="radioUserGedLabel">',
     *                  'after' => '</div>',
     *                  'label' => true,
     *                  'type'=>'radio',
     *                  'options' => $votre_options,
     *                  'value' => $votre_value_selected
     *              )
     *          ),
     *          'nom_de_champ_file' => array(
     *              'labelText' => 'votre text de label',
     *              'inputType' => 'file',
     *              'items'=>array(
     *                   'type'=>'file',
     *                   'name' => 'myfile',
     *                   'class' => 'fileField'
     *              )
     *           ),
     *          'nom_de_champ_number' => array(
     *              'labelText' => 'votre text de label',
     *              'inputType' => 'number',
     *              'items'=>array(
     *                   'type' => 'number',
     *                   'required' => true,
     *                   'max'=>"99999",
     *                   'min'=>"0",
     *                   'data-minlength'=>"5"
     *               )
     *          ),
     *          'nom_de_champ_password' => array(
     *              'labelText' => 'votre text de label',
     *              'inputType' => 'password',
     *              'items'=>array(
     *                  'type' => 'password',
     *                  'required' => true,
     *                  'data-match'=>'#UserNewpassword',  //(si besoin)
     *                  'data-error'=>"Whoops, ceux-ci ne correspondent pas" //(si besoin)
     *               )
     *          ),
     *          'nom_de_champ_checkbox' => array(
     *              'labelText' => 'votre text de label',
     *              'inputType' => 'checkbox',
     *              'items'=>array(
     *                  'type'=>'checkbox',
     *                  'checked'=>true/false  //(si modification oblige, example : $this->request->data['Activite']['active'])
     *          )
     *      )
     *  );
     * echo $this->Formulaire->createForm($formNom);
     *
     *
     *
     */
    public function createForm($datas) {
    	// Cas pur gérer le plugin CakePHP
		$iscakeflow = false;
    	if( isset($datas['form_url']) && !isset($datas['form_url']['controller']) ) {
			$iscakeflow = (strpos($datas['form_url'], 'Compositions') !== false);
		}

		if( !empty($datas['form_url']['controller']) ) {
			$datas['form_url']['controller'] = Inflector::underscore($datas['form_url']['controller']);
		}
		if( empty($datas['url']['controller']) && empty($datas['form_url']['controller']) && !$iscakeflow ) {
			if (!empty($datas['name'])) {
				$datas['form_url']['controller'] = Inflector::underscore(Inflector::pluralize($datas['name']));
			} elseif (!empty($this->request->params['controller'])) {
				$datas['form_url']['controller'] = Inflector::underscore($this->request->params['controller']);
			}
		}
        $div_w = (isset($datas['div_w'])) ? $datas['div_w'] : "";
        $formUrl = (isset($datas['form_url'])) ? $datas['form_url'] : "";
        $formTarget = (isset($datas['form_target'])) ? $datas['form_target'] : "";
        $formType = (isset($datas['form_type'])) ? $datas['form_type'] : "";
        $formClass = (isset($datas['form_class'])) ? $datas['form_class'] : "";
        $resultats = $this->Form->create($datas['name'], array(
            'class' => 'form-horizontal ' . $formClass,
            'url' => $formUrl,
            'target' => $formTarget,
            'type' => $formType,
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'form-group ' . $div_w),
                'label' => array('class' => 'control-label ' . $datas['label_w']),
                'between' => '<div class=" controls-input ' . $datas['input_w'] . ' ">',
                'after' => '<div class="help-block with-errors"></div></div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                'data-toggle' => "validator"
        )));

        $resultats .= $this->_createInput($datas);
        return $resultats;
    }

    private function _createInput($datas) {

        $inputs = $datas['input'];
        $lable_w = $datas['label_w'];

        $resultats = "";
        foreach ($inputs as $name => $value) {

            if( isset( $value['items']['required'] ) && $value['items']['required'] == true ) {
                $input['labelText'] = (isset($value['labelText'])) ? $value['labelText'].' *' : "";
            }
            else {
                $input['labelText'] = (isset($value['labelText'])) ? $value['labelText'] : "";
            }
            $input['placeholder'] = (isset($value['labelPlaceholder'])) ? $value['labelPlaceholder'] : "";
            $input['type'] = (isset($value['items']['type'])) ? $value['items']['type'] : "";
            $input['required'] = (isset($value['items']['required'])) ? $value['items']['required'] : "";
            $input['escape'] = (isset($value['items']['escape'])) ? $value['items']['escape'] : "";
            $input['multiple'] = (isset($value['items']['multiple'])) ? $value['items']['multiple'] : "";
            $input['empty'] = (isset($value['items']['empty'])) ? $value['items']['empty'] : "";
            $input['checked'] = (isset($value['items']['checked'])) ? $value['items']['checked'] : "";
            $input['valueinput'] = (isset($value['items']['value'])) ? $value['items']['value'] : "";
            $input['classinput'] = (isset($value['items']['class'])) ? $value['items']['class'] : "";
            $input['nameinput'] = (isset($value['items']['name'])) ? $value['items']['name'] : "";
            $input['input_options'] = (!empty($value['items']['options'])) ? $value['items']['options'] : "";
            $input['selected'] = (!empty($value['items']['selected'])) ? $value['items']['selected'] : "";
            $input['numberMax'] = (isset($value['items']['max'])) ? $value['items']['max'] : "";
            $input['pattern'] = (isset($value['items']['pattern'])) ? $value['items']['pattern'] : "";
            $input['data-error'] = (isset($value['items']['data-error'])) ? $value['items']['data-error'] : "";
            $input['numberMin'] = (isset($value['items']['min'])) ? $value['items']['min'] : "";
            $input['numberMinLength'] = (isset($value['items']['data-minlength'])) ? $value['items']['data-minlength'] : "";
            $input['passwordDataMatch'] = (isset($value['items']['data-match'])) ? $value['items']['data-match'] : "";
            $input['passwordDataMatchError'] = (isset($value['items']['data-error'])) ? $value['items']['data-error'] : "";
            $input['disabled'] = (!empty($value['items']['disabled']) && $value['items']['disabled']) ? $value['items']['disabled'] : "";
            $input['readonly'] = (!empty($value['items']['readonly']) && $value['items']['readonly']) ? $value['items']['readonly'] : "";
            $input['between'] = (isset($value['items']['between'])) ? $value['items']['between'] : "";


            if (isset($value['changeWidth']) && $value['changeWidth']) {
                $new_label_w = (!empty($value['items']['new_label_w'])) ? $value['items']['new_label_w'] : "";
                $new_input_w = (!empty($value['items']['new_input_w'])) ? $value['items']['new_input_w'] : "";
                $resultats .=$this->_reinitialeFormHead($datas, $input, $name, $lable_w, $new_label_w, $new_input_w);
            } else if (isset($value['dateInput']) && $value['dateInput']) {
                $resultats .=$this->_initDateInput($datas, $input, $name, $lable_w);
            } else {
                $resultats .=$this->_initialeFormInput($input, $name, $lable_w);
            }
        }
        return $resultats;
    }

    private function _array_depth(array $array) {
        $max_depth = 1;

        foreach ($array as $value) {
            if (is_array($value)) {
                $depth = $this->_array_depth($value) + 1;

                if ($depth > $max_depth) {
                    $max_depth = $depth;
                }
            }
        }

        return $max_depth;
    }

    private function _reinitialeFormHead($datas, $input, $name, $lable_w, $new_label_w, $new_input_w) {

        $div_w = (isset($datas['div_w'])) ? $datas['div_w'] : "";
        $formUrl = (isset($datas['form_url'])) ? $datas['form_url'] : "controller";
        $formTarget = (isset($datas['form_target'])) ? $datas['form_target'] : "";
        $formType = (isset($datas['form_type'])) ? $datas['form_type'] : "";

        $resultats = $this->Form->create($datas['name'], array(
            'class' => 'form-horizontal',
            'url' => $formUrl,
            'target' => $formTarget,
            'type' => $formType,
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'form-group ' . $div_w),
                'label' => array('class' => 'control-label ' . $new_label_w),
                'between' => '<div class=" controls-input  ' . $new_input_w . ' ">',
                'after' => '<div class="help-block with-errors"></div></div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                'data-toggle' => "validator"
        )));

        $resultats .=$this->_initialeFormInput($input, $name, $new_label_w);


        $resultats .= $this->Form->create($datas['name'], array(
            'class' => 'form-horizontal',
            'url' => $formUrl,
            'target' => $formTarget,
            'type' => $formType,
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'form-group ' . $div_w),
                'label' => array('class' => 'control-label ' . $datas['label_w']),
                'between' => '<div class = " controls-input ' . $datas['input_w'] . ' ">',
                'after' => '<div class = "help-block with-errors"></div></div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                'data-toggle' => "validator"
        )));

        return $resultats;
    }

    private function _initialeFormInput($input, $name, $lable_w) {
        $resultats = "";
        $inputArray = array(
            'label' => array('text' => $input['labelText'], 'class' => 'control-label ' . $lable_w),
            'type' => $input['type'],
            'placeholder' => $input['placeholder'],
            'required' => $input['required'],
            'escape' => false,
            'class' => 'form-control ' . $input['classinput'],
        );
        if ($input['nameinput'] != "") {
            $inputArray['name'] = $input['nameinput'];
        }
        if ($input['valueinput'] != "") {
            $inputArray['value'] = $input['valueinput'];
        }
        if (isset($input['readonly']) && $input['readonly'] != "") {
            $inputArray['readonly'] = $input['readonly'];
        }
        if (isset($input['disabled']) && $input['disabled'] != "") {
            $inputArray['disabled'] = $input['disabled'];
        }
        if (isset($input['pattern']) && $input['pattern'] != "") {
            $inputArray['pattern'] = $input['pattern'];
        }
        if (isset($input['data-error']) && $input['data-error'] != "") {
            $inputArray['data-error'] = $input['data-error'];
        }


        $options = array();
        if (!empty($input['input_options']) && $this->_array_depth($input['input_options']) > 1) {
            $options = $input['input_options'];
        } else {
            if (!empty($input['input_options'])) {
                foreach ($input['input_options'] as $key => $option) {
                    $options[$key] = html_entity_decode($option);
                }
            }
        }

        switch ($input['type']) {
            case 'select':
                $inputArray['multiple'] = $input['multiple'];
                $inputArray['empty'] = $input['empty'];
                $inputArray['options'] = $options;
                if (isset($input['selected']) && $input['selected'] != "") {
                    $inputArray['selected'] = $input['selected'];
                }
                break;
            case 'hidden':
                $inputArray = array('type' => 'hidden');
                if ($input['valueinput'] != "") {
                    $inputArray['value'] = $input['valueinput'];
                }
                break;
            case 'radio':
                $inputArray['legend'] = false;
                $inputArray['options'] = $options;
                $inputArray['separator'] =  "&nbsp; &nbsp; &nbsp;";
                $inputArray['before'] = "<label class='control-label col-sm-4' style='padding-top:0px;'>{$input['labelText']}</label><div class = 'radioLabel'>";
                $inputArray['after'] = '</div>';
                $inputArray['label'] = true;
                $inputArray['class'] = '';
                break;
            case 'file':
                $inputArray['name'] = $input['nameinput'];
                break;
            case 'number':
                $inputArray['max'] = $input['numberMax'];
                $inputArray['min'] = $input['numberMin'];
                $inputArray['data-minlength'] = $input['numberMinLength'];
                break;
            case 'password':
                $inputArray['data-match'] = $input['passwordDataMatch'];
                $inputArray['data-error'] = $input['passwordDataMatchError'];
                break;
            case 'time':
                $inputArray['timeFormat'] = '24';
                $inputArray['interval'] = 05;
                break;
            default :
                $inputArray['checked'] = $input['checked'];
        }
        $resultats .= $this->Form->input($name, $inputArray);
        return $resultats;
    }

    private function _initDateInput($datas, $input, $name, $lable_w) {

        $div_w = (isset($datas['div_w'])) ? $datas['div_w'] : "";
        $formUrl = (isset($datas['form_url'])) ? $datas['form_url'] : "controller";
        $formTarget = (isset($datas['form_target'])) ? $datas['form_target'] : "";
        $formType = (isset($datas['form_type'])) ? $datas['form_type'] : "";

        $resultats = $this->Form->create($datas['name'], array(
            'class' => 'form-horizontal',
            'url' => $formUrl,
            'target' => $formTarget,
            'type' => $formType,
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'form-group ' . $div_w),
                'label' => array('class' => 'control-label ' . $datas['label_w']),
                'between' => '<div class = " controls-input controls date form_datetime ' . $datas['input_w'] . ' ">',
                'after' => '<div class = "input-group-addon-calendar"><i class = "fa fa-calendar" aria-hidden = "true"></i></div></div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
        )));

        $resultats .=$this->Form->input($name, array('type' => 'text',
            'class' => ' form-control',
            'readonly' => true,
            'data-format' => 'dd/MM/yyyy',
            'placeholder' => $input['valueinput'],
            'label' => array('text' => $input['labelText'], 'class' => 'control-label ' . $lable_w)
        ));

        $resultats .= $this->Form->create($datas['name'], array(
            'class' => 'form-horizontal',
            'url' => $formUrl,
            'target' => $formTarget,
            'type' => $formType,
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'form-group ' . $div_w),
                'label' => array('class' => 'control-label ' . $datas['label_w']),
                'between' => '<div class = " controls-input ' . $datas['input_w'] . ' ">',
                'after' => '<div class = "help-block with-errors"></div></div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                'data-toggle' => "validator"
        )));

        return $resultats;
    }

}

?>
