<?php

/**
 *
 * String helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class StringHelper extends AppHelper {

	/**
	 *
	 * @param type $chaine
	 * @return type
	 */
	function convertFR($chaine) {
		// Utilisation de la fonction htmlentities() pour convertir les caractères spéciaux qu'elle peut faire
		// Création d'un tableau groupant les caractères spéciaux problématiques (j'en ai sélectionné 20, mais d'autres peuvent compléter ce tableau)
		//$caracteres_non_codes = array('Œ', 'œ', 'Ÿ', '–', '—', '‘', '’', '‚', '“', '”', '„', '†', '‡', '•', '…', '‰', '‹', '›', '€', '™');
		$caracteres_non_codes = array('Œ', 'œ', '’');
		// Création d'un autre tableau groupant les codes HTML respectifs de ces caractères
		$caracteres_codes = array('OE', 'oe', "'");
		//$caracteres_codes = array('&OElig;', '&oelig;', '&Yuml;', '&ndash;', '&mdash;', '&lsquo;', '&rsquo;', '&sbquo;', '&ldquo;', '&rdquo;', '&bdquo;', '&dagger;', '&Dagger;', '&bull;', '&hellip;', '&permil;', '&lsaquo;', '&rsaquo;', '&euro;', '&trade;');
		// On remplace les valeurs du premier tableau par ceux du second avec la fonction str_replace()
		$converted = str_replace($caracteres_non_codes, $caracteres_codes, $chaine);
		// On retourne le résultat
		return $converted;
	}

	/**
	 *
	 * @param type $str
	 * @return type
	 */
	function strInline($str) {
		return str_replace(CHR(10), "", str_replace(CHR(13), "", nl2br(addslashes(htmlentities(utf8_decode(convertFR($str)))))));
	}

	/**
	 *
	 * @param type $str
	 * @param type $length
	 * @return string
	 */
	function troncateStr($str, $length) {
		$ret = $str;
		if (strlen($str) > $length) {
			$spacePos = strpos($str, " ", $length);

			if($spacePos >= $length ) {
				$ret = substr($str, 0, 90) . " ...";
			}
			else {
				if ($spacePos !== false) {
					$ret = substr($str, 0, $spacePos) . " ...";
				} else {
					$ret = substr($str, 0, $length) . " ...";
				}
			}
		}
		return $ret;
	}

	/**
	 *
	 * @param type $string
	 * @return type
	 */
	function ucname($string) {
		$string = ucwords(strtolower($string));
		foreach (array('-', '\'') as $delimiter) {
			if (strpos($string, $delimiter) !== false) {
				$string = implode($delimiter, array_map('ucfirst', explode($delimiter, $string)));
			}
		}
		return $string;
	}

}

?>
