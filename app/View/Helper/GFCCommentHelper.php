<?php

App::uses('SCCommentHelper', 'SimpleComment.View/Helper');
App::uses('CakeTime', 'Utility');

/**
 *
 * GFCComment helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		cake.app
 * @subpackage	cake.app.view.helper
 */
class GFCCommentHelper extends Helper {

    /**
     *
     * @var type
     */
    public $helpers = array('Html', 'Html2','Form', 'SimpleComment.SCComment');

    /**
     *
     * @param type $comments
     * @return type
     * @throws BadMethodCallException
     */
    public function drawComments($comments, $edit = false) {

        if (empty($comments)) {
            throw new BadMethodCallException('Empty comments');
        }
        if (!isset($comments['public'])) {
            throw new BadMethodCallException('No public key');
        }
        if (!isset($comments['private'])) {
            throw new BadMethodCallException('No private key');
        }
        if (!isset($comments['written'])) {
            throw new BadMethodCallException('No written key');
        }
        $privateImg = $this->Html->image('/img/comment_private_16.png', array('title' => __d('comment', 'Comment.privateComments'), 'alt' => __d('comment', 'Comment.privateComments')));
        $commentsPublic = '';
        if (!empty($comments['public'])) {
            $commentsPublic = $this->SCComment->draw($this->_prepareComments($comments['public']), array('title' => __d('comment', 'Comment.publicComments'), 'empty_message' => __d('comment', 'Comment.empty')));
        }
        $commentsPrivate = '';
        if (!empty($comments['private'])) {
            $commentsPrivate = $this->SCComment->draw($this->_prepareComments($comments['private']), array('title' => $privateImg . ' ' . __d('comment', 'Comment.privateComments'), 'empty_message' => __d('comment', 'Comment.empty')));
        }
        $commentsWritten = '';
        if (!empty($comments['written'])) {
            $commentsWritten = $this->SCComment->draw($this->_prepareComments($comments['written'], true, $edit), array('title' => __d('comment', 'Comment.writtenComments'), 'empty_message' => __d('comment', 'Comment.empty')));
        }

        if (empty($comments['public']) && empty($comments['private']) && empty($comments['written'])) {
            return $this->Html->tag('div', __d('comment', 'Comment.empty'), array('class' => 'alert alert-warning'));
        }


        return $commentsPublic . $commentsPrivate . $commentsWritten;
    }

    /**
     *
     * @param type $comments
     * @param type $written
     * @return string
     */
    private function _prepareComments($comments, $written = false, $editMode = false) {
        $return = array();

        if (!empty($comments)) {
            foreach ($comments as $comment) {
                $author = '';
                if (!empty($comment['User']['prenom']) && !empty($comment['User']['nom'])) {
                    $author = $comment['User']['prenom'] . ' ' . $comment['User']['nom'];
                } else if (!empty($comment['SecondaryUser']['prenom']) && !empty($comment['SecondaryUser']['nom'])) {
                    $author = $comment['SecondaryUser']['prenom'] . ' ' . $comment['SecondaryUser']['nom'];
                }
                $author .= (empty($author) ? '' : ' ( ') . $comment['Owner']['name'] . (empty($author) ? '' : ' )');


                $objet = htmlentities( $comment['Comment']['objet'], ENT_QUOTES );

                $return[] = array(
                    'id' => $comment['Comment']['id'],
//                    'date' => CakeTime::format($comment['Comment']['modified'], '%d/%m/%Y %H:%I:%S'),
                    'date' => $this->Html2->ukToFrenchDateWithHourAndSlashes($comment['Comment']['modified']),
                    'author' => $author,
//                    'comment' => htmlentities($comment['Comment']['content'], ENT_QUOTES ) ,
                    'comment' => !empty(htmlentities($comment['Comment']['content'], ENT_QUOTES )) ? htmlentities($comment['Comment']['content'], ENT_QUOTES ) : $objet,
//                    'comment' => str_replace(array("<p>", "</p>", "<br />", "\n"), " ", quoted_printable_decode($comment['Comment']['content'] ) ),
//                    'comment' => preg_replace( '#\s+#', ' ', $comment['Comment']['content'] ),
                    'private' => $comment['Comment']['private'],
                    'editable' => $written && $editMode,
                    'readers' => !empty($comment['readers']) ? $comment['readers'] : array()
                );
            }
        }
//debug($return);
        return $return;
    }

    /**
     *
     * @param type $readers
     * @return type
     */
    public function drawForm($readers) {
        return $this->Form->create('Comment')
                . $this->Html->tag('legend', __d('courrier', 'Courrier.commentFieldset'))
//                . $this->Form->input('Comment.content', array('type' => 'textarea', 'label' => __d('comment', 'Comment.content')))
                . $this->Form->input('Comment.objet', array('type' => 'textarea', 'label' => __d('comment', 'Comment.objet')))
                . $this->Form->input('Comment.private', array('label' => __d('comment', 'Comment.private')))
                . $this->Form->input('Reader.Reader', array('label' => __d('comment', 'Comment.reader'), 'options' => $readers))
                . $this->Form->end();
    }

}

?>
