<?php

/**
 *
 * Statistiques/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('SimpleComment.comment.css'), null, array('inline' => false));
echo $this->Html->script(array('jsapi'),array('inline'=>false));
echo $this->Html->script(array('google_charts_loader.js'),array('inline'=>false));
?>
<div class="container">
    <div id="content_page" class="row">
        <div id ="" class="table-list statistiques">
            <h3><?php echo __d('statistique', 'Indicateurquant.title'); ?></h3>
            <div class="formule-recherche">
        <?php
            $formStatistique = array(
                    'name' => 'Statistique',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-3',
					'form_url' => array('controller' => 'statistiques', 'action' => 'indicateursQuant' ),
                    'input' => array(
                        'Statistique.annee' => array(
                            'labelText' =>__d('statistique', 'Indicateurquant.annee'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options' => array_combine( range( date( 'Y' ), 2012, -1 ), range( date( 'Y' ), 2012, -1 ) ),
                            )
                        ),
                        'Statistique.origineflux_id' => array(
                            'labelText' =>__d('statistique', 'Indicateurquant.origineflux_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options' => $origines,
                                'empty' => true
                            )
                        )
                    )
                );
                if(Configure::read('CD') == 81 ) {
                    $formStatistique['input']['Statistique.service'] = array(
                        'labelText' =>__d('statistique', 'Indicateurqual.service'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'multiple' => true,
//                            'class' => 'JSelectMultiple',
                            'options' => $services,
                            'required' => true
                        )
                    );
                }
                else {
                    $formStatistique['input']['Statistique.service'] = array(
                        'labelText' =>__d('statistique', 'Indicateurqual.service'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'multiple' => true,
                            'options' => $services
                        )
                    );
                }

                if(!empty($metas)){
                    foreach( $metas as $meta ) {
                        $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}.checked"] = array(
                            'labelText' =>$meta['Metadonnee']['name'],
                            'inputType' => 'checkbox',
                            'items'=>array(
                                'type'=>'checkbox'
                            )
                        );
                        $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}.selectvaluemetadonnee_id"] = array(
                            'labelText' =>'Valeurs disponibles',
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options' => !empty($meta['ValueInSelect'])?$meta['ValueInSelect']:array(),
                                'empty' => true
                            )
                        );
                        $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}.typemetadonnee_id"] = array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $meta['Metadonnee']['typemetadonnee_id']
                            )
                        );
                        if( $meta['Metadonnee']['typemetadonnee_id'] == 3) {
                            $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}.selectvaluemetadonnee_id"] = array(
                                'labelText' =>'Valeurs disponibles',
                                'inputType' => 'select',
                                'items'=>array(
                                    'type'=>'select',
                                    'options' => array('Oui' => 'Oui', 'Non' => 'Non'),
                                    'empty' => true
                                )
                            );
                        }
                        if( $meta['Metadonnee']['typemetadonnee_id'] == 2) {
                            $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}.value"] = array(
                                'labelText' =>'Valeurs',
                                'inputType' => 'text',
                                'items'=>array(
                                    'type'=>'text'
                                )
                            );
                        }
                        if( $meta['Metadonnee']['typemetadonnee_id'] == 1) {
                            $formStatistique['input']["Metadonnee.{$meta['Metadonnee']['id']}.value"] = array(
                                'labelText' =>'Valeurs',
                                'inputType' => 'text',
                                'dateInput' =>true,
                                'items'=>array(
                                    'type' => 'text',
                                    'readonly' => true,
                                    'class' => 'datepicker',
                                    'data-format'=>'dd/MM/yyyy',
                                    'empty' => true
                                )
                            );
                        }
                    }
                }
            echo $this->Formulaire->createForm($formStatistique);
            echo $this->Form->end();
            ?>
            </div>
            <div id="formule-recherche-controls">
                <a class="btn btn-info-webgfc" id="searchButton" title="<?php echo __d('statistique', 'btn.recherche'); ?>"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
            </div>

            <!-- zone resultat -->
        <?php $annee = Hash::get( $this->request->data, 'Statistique.annee' );?>
        <?php if( isset($results)): ?>
        <?php
            $origine = '';
            if(!empty($origineSelected)) {
                $origine = " dont l'origine est ".'"'.Hash::get( $origines, $origineSelected ).'"';
            }
        ?>
            <legend class="legend-resultat" style="margin-left:200px; width: 70%"><?php echo __d('statistique', 'Indicateurquant.legend.Resultat') . $annee .$origine; ?></legend>
            <div class="panel-body">
                <div id="gauche" class="panel col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                            <h4 class="panel-title"><?php echo __d('statistique', 'Indicateurquant.Resultat.etat'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <div id="myPieChart">

                            </div>
                        <?php foreach( array_keys( $results['Indicateurquant'] ) as $indicateur ):?>
                            <?php if( in_array( $indicateur, array( 'cloture', 'encours', 'valide', 'refuse' ) ) ):?>
                            <div class="results control-group">
                                <label class="control-label  col-sm-8"> <?php echo __d( 'statistique', "Indicateurquant.{$indicateur}" );?></label>
                                <div class="controls  col-sm-3 statistiquesResultatNb">
                                    <span class="number"><?php
                                    $value = Hash::get( $results, "Indicateurquant.{$indicateur}" );
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $value );
                                    }
                                ?></span>
                                </div>
                            </div>
                            <?php endif;?>
                            <?php if( $indicateur === 'total' ):?>
                            <div class="results control-group">
                                <label  class="control-label  col-sm-8"> <?php echo __d( 'statistique', "Indicateurquant.{$indicateur}" );?></label>
                                <div class="controls  col-sm-3 statistiquesResultatNb">
                                    <span class="number">
                                            <?php
                                                $value = Hash::get( $results, "Indicateurquant.{$indicateur}" );
                                                if( is_null( $value ) ) {
                                                    echo 'ND';
                                                }
                                                else {
                                                    echo $this->Locale->number( $value );
                                                }
                                            ?>
                                    </span>
                                </div>
                            </div>
                            <?php endif;?>
                        <?php endforeach;?>
                        </div>
                        <div class="panel-footer" style="min-height: 50px;">

                        </div>
                    </div>
                </div>
        <?php endif;?>
        <?php if( isset($typemetaCounts['Indicateurmeta'])): ?>
                <!-- Informations liées aux métadonnées-->

                <div id="droite" class="panel col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                            <h4 class="panel-title"><?php echo __d('statistique', 'Indicateurquant.Resultat.meta'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <div id="myPieChartMeta">

                            </div>
                        <?php ksort($typemetaCounts['Indicateurmeta']);?>
                        <?php foreach( array_keys( $typemetaCounts['Indicateurmeta'] ) as $indicateur ):?>
                        <?php if( $indicateur != 'typedemandetotale' ):?>
                            <div class="results control-group">
                                <label  class="control-label  col-sm-8"><?php echo $indicateur;?></label>
                                <div class="controls  col-sm-3 statistiquesResultatNb">
                                    <span class="number"><?php
                                    $value = Hash::get( $typemetaCounts, "Indicateurmeta.{$indicateur}" );
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $value );
                                    }
                                ?></span>
                                </div>
                            </div>
                        <?php endif;?>

                        <?php endforeach;?>
                        </div>
                        <div class="panel-footer" style="min-height: 50px;">

                        </div>
                    </div>
                </div>
        <?php endif;?>
        <?php if( isset($typemetaCounts['Indicateurservice'])): ?>
                <!-- Informations liées aux métadonnées-->

                <div id="droite" class="panel col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                            <h4 class="panel-title"><?php echo __d('statistique', 'Indicateurquant.Resultat.service'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                            <div id="myPieChartService">

                            </div>
                        <?php ksort($typemetaCounts['Indicateurservice']);?>
                        <?php foreach( array_keys( $typemetaCounts['Indicateurservice'] ) as $indicateur ):?>
                        <?php if( $indicateur != 'typedemandetotale' ):?>
                            <div class="results control-group">
                                <label  class="control-label  col-sm-8"><?php echo $indicateur;?></label>
                                <div class="controls  col-sm-3 statistiquesResultatNb">
                                    <span class="number"><?php
                                    $value = Hash::get( $typemetaCounts, "Indicateurservice.{$indicateur}" );
                                    if( is_null( $value ) ) {
                                        echo 'ND';
                                    }
                                    else {
                                        echo $this->Locale->number( $value );
                                    }
                                ?></span>
                                </div>
                            </div>
                        <?php endif;?>

                        <?php endforeach;?>
                        </div>
                        <div class="panel-footer" style="min-height: 50px;">

                        </div>
                    </div>
                </div>
        <?php endif;?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var border_quantitatifs = $('#StatistiqueService').parents('.form-group');
    $("<legend style='margin-left:200px; width: 70%'><?php echo __d('statistique', 'Indicateurquant.legend.Meta'); ?></legend>").insertAfter(border_quantitatifs);

    $('#formulaireButton').button();
    $('#searchButton').button();
    $('#StatistiqueAnnee').select2({allowClear: true, placeholder: "Sélectionner une année"});
    $('#StatistiqueTypedemande').select2({allowClear: true, placeholder: "Sélectionner un type de demande"});
    $('#StatistiqueOriginefluxId').select2({allowClear: true, placeholder: "Sélectionner une origine"});
    <?php if( isset($results)): ?>
    $('.formule-recherche').hide();
    $('#formule-recherche-controls').html('<a class="btn btn-info-webgfc btn-inverse" id="reset" title="<?php echo __d('statistique', 'btn.reset'); ?>"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>');
    $('#reset').click(function () {
        location.reload();
    });
    <?php endif ?>

    $('#searchButton').click(function () {
        var dataSerialize = $("#StatistiqueIndicateursQuantForm").serialize();
        if (!$('.legend-resultat').size() < 1) {
            dataSerialize += '&alreadyLoad=true';
        }

        gui.request({
            url: $("#StatistiqueIndicateursQuantForm").attr('action'),
            data: dataSerialize,
            loader: true,
            updateElement: $('.formule-recherche'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('#content_page').empty();
            $('#content_page').html(data);
        });
    });

    //**************************************************************//
    //statisitique
    <?php if( isset($results)): ?>
    <?php if(!isset($alreadyLoad)):?>
    google.charts.load('41', {packages: ['corechart', 'bar']});
    <?php endif?>
    google.charts.setOnLoadCallback(drawChart);
    function drawChart() {
        // Define the chart to be drawn.
        var data = google.visualization.arrayToDataTable([
            ['Variable de flux', 'Nombre', ],
            ['<?php echo __d('statistique', 'Indicateurquant.bar.encours'); ?>', <?php echo (int)$results['Indicateurquant']['encours']; ?>],
            ['<?php echo __d('statistique', 'Indicateurquant.bar.refuse'); ?>', <?php echo (int)$results['Indicateurquant']['refuse']; ?>],
            ['<?php echo __d('statistique', 'Indicateurquant.bar.valide'); ?>', <?php echo (int)$results['Indicateurquant']['valide']; ?>],
            ['<?php echo __d('statistique', 'Indicateurquant.bar.cloture'); ?>', <?php echo (int)$results['Indicateurquant']['cloture']; ?>],
        ]);
        var options = {
            title: '',
            is3D: true,
        };
        // Instantiate and draw the chart.
        var chart = new google.visualization.PieChart(document.getElementById('myPieChart'));
        chart.draw(data, options);

        var dataMeta = google.visualization.arrayToDataTable([
            ['Nom de metadonnée', 'Nombre'],
            <?php if( isset($typemetaCounts['Indicateurmeta'])): ?>
                <?php foreach( array_keys( $typemetaCounts['Indicateurmeta'] ) as $indicateur ):?>
                    <?php if( $indicateur != 'typedemandetotale' ):?>
            ['<?php echo $indicateur; ?>',<?php echo$this->Locale->number( Hash::get( $typemetaCounts, "Indicateurmeta.{$indicateur}" ) );?>],
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
        ]);
        var optionsMeta = {
            title: '',
            is3D: true,
        };
        <?php if( isset($typemetaCounts['Indicateurmeta'])): ?>
          var chartMeta = new google.visualization.PieChart(document.getElementById('myPieChartMeta'));
          chartMeta.draw(dataMeta, optionsMeta);
        <?php endif;?>

        var dataService = google.visualization.arrayToDataTable([
            ['Nom du service', 'Nombre'],
            <?php if( isset($typemetaCounts['Indicateurservice'])): ?>
                <?php foreach( array_keys( $typemetaCounts['Indicateurservice'] ) as $indicateur ):?>
                    <?php if( $indicateur != 'typedemandetotale' ):?>
            ['<?php echo $indicateur; ?>',<?php echo$this->Locale->number( Hash::get( $typemetaCounts, "Indicateurservice.{$indicateur}" ) );?>],
                    <?php endif;?>
                <?php endforeach;?>
            <?php endif;?>
        ]);
        var optionsService = {
            title: '',
            is3D: true,
        };
      <?php if( isset($typemetaCounts['Indicateurservice'])): ?>
        var chartMeta = new google.visualization.PieChart(document.getElementById('myPieChartService'));
        chartMeta.draw(dataService, optionsService);
      <?php endif;?>
    }
    <?php endif;?>
    /********************************************************/


    // Ajout de l'action de recherche via la touche Entrée
    $('#StatistiqueIndicateursQuantForm').keypress(function (e) {
        var dataSerialize = $("#StatistiqueIndicateursQuantForm").serialize();
        if (!$('.legend-resultat').size() < 1) {
            dataSerialize += '&alreadyLoad=true';
        }
        if (e.keyCode == 13) {
            gui.request({
                url: $("#StatistiqueIndicateursQuantForm").attr('action'),
                data: dataSerialize,
                loader: true,
                updateElement: $('#content_page'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                $('#content_page').empty();
                $('#content_page').html(data);
            });
        }
    });
    //    $('#StatistiqueIndicateursQuantForm.folded').hide();


    gui.addbutton({
        element: $('#gauche .panel-footer'),
        button: {
            content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
            title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
            class: 'btn btn-info-webgfc pull-right',
            action: function () {
                //                window.location.href = "/statistiques/exportquant";
                window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportquant' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }

    });

    <?php if( !empty( $results ) ) { ?>
    gui.enablebutton({
        element: $('#gauche .panel-footer'),
        content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
        title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
        class: 'btn btn-info-webgfc pull-right',
        action: function () {
            window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportquant' ) + Hash::flatten( $this->request->data, '__' ) );?>";
        }
    });
        <?php }else{ ?>
    gui.disablebutton({
        element: $('#gauche .panel-footer'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>'
    });
        <?php } ?>

    // Bouton pour le tableau de nb de flux métadonnées
    gui.addbutton({
        element: $('#droite  .panel-footer'),
        button: {
            content: '<i class="fa fa-download" aria-hidden="true"></i> Télécharger les résultats',
            title: "<?php echo __d('default', 'Button.exportcsv'); ?>",
            class: 'btn btn-info-webgfc pull-right',
            action: function () {
                //                window.location.href = "/statistiques/exportquantmeta";
                window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportquantmeta' ) + Hash::flatten( $this->request->data, '__' ) );?>";
            }
        }

    });

    <?php if( !empty( $results ) ) { ?>
    gui.enablebutton({
        element: $('#droite  .panel-footer'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>',
        class: 'btn btn-info-webgfc pull-right',
        action: function () {
            window.location.href = "<?php echo Router::url( array( 'controller' => 'statistiques', 'action' => 'exportquantmeta' ) + Hash::flatten( $this->request->data, '__' ) );?>";
        }
    });
        <?php }else{ ?>
    gui.disablebutton({
        element: $('#droite  .panel-footer'),
        button: '<i class="fa fa-download" aria-hidden="true"></i>'
    });
        <?php } ?>


    <?php foreach( $metas as $meta ):?>
        $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>SelectvaluemetadonneeId").select2({allowClear: true, placeholder: "Sélectionner une valeur"});
        $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Checked").change(function () {
            if ($("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Checked").attr('checked') == 'checked') {
                // Si on coche une case, on affiche els valeurs
                <?php if(in_array( $meta['Metadonnee']['typemetadonnee_id'], array( 3, 4 ) ) ): ?>
                    $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>SelectvaluemetadonneeId").parent().parent().show();
                <?php else :?>
                    $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Value").parent().parent().show();
                <?php endif;?>
            }
            else {
                // Si aucune case de cochée, alors on masque les valeurs
                <?php if(in_array( $meta['Metadonnee']['typemetadonnee_id'], array( 3, 4 ) ) ): ?>
                    $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>SelectvaluemetadonneeId").parent().parent().hide();
                <?php else :?>
                    $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Value").parent().parent().hide();
                <?php endif;?>
            }
        });
        // Par défaut, on masque les valeurs
        $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>SelectvaluemetadonneeId").parent().parent().hide();
        $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Value").parent().parent().hide();
        // Si la case est cochée, on affiche la valeur
        if ($("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Checked").attr('checked') == 'checked') {
            <?php if(in_array( $meta['Metadonnee']['typemetadonnee_id'], array( 3, 4 ) ) ): ?>
                $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>SelectvaluemetadonneeId").parent().parent().show();
            <?php else :?>
                $("#Metadonnee<?php echo $meta['Metadonnee']['id'];?>Value").parent().parent().show();
            <?php endif;?>
        }

    <?php endforeach;?>
    $('#StatistiqueService').select2({allowClear: true, placeholder: "Sélectionner un service"});

        $(document).ready(function () {
            $('.form_datetime input').datepicker({
                language: 'fr-FR',
                format: "dd/mm/yyyy",
                weekStart: 1,
                autoclose: true,
                todayBtn: 'linked'
            });
        });
</script>
