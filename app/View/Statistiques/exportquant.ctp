<?php

$this->Csv->preserveLeadingZerosInExcel = true;

    if( !empty($origine)) {
        $this->Csv->addRow( array( 'Etat statistique du nombre de flux pour l\'année '.$annee.' dont l\'origine est "'.$origineName.'"' ));
    }
    else {
        $this->Csv->addRow( array( 'Etat statistique du nombre de flux pour l\'année '.$annee ));
    }
    
    $this->Csv->addRow(
        array(
            '',
            'Total'
        )
	);
    
	foreach( array_keys( $results['Indicateurquant'] ) as $indicateur ){
        $nomEtat = __d( 'statistique', "Indicateurquant.{$indicateur}" );
        $val = Hash::get( $results, "Indicateurquant.{$indicateur}" );
        echo $val;
        if( is_null( $val ) ) {
            echo 'ND';
        }
        else {
            $val = $this->Locale->number( $val );
        }

        $this->Csv->addRow(
            array(
                $nomEtat,
                $val
            )
        );
	}

    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );

?>