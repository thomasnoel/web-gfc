<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Etat statistique du nombre de flux par agent pour l\'année '.$annee ));
    
    $this->Csv->addRow(
        array(
            'Nom de l\'agent',
            'Intitulé de l\'état',
            'Total'
        )
	);

	foreach( $results as $agentId => $valueSelected ){
        foreach( $valueSelected as $idSelected => $indicateurs) {
            $nomAgent = isset( $agentName[$idSelected] ) ? $agentName[$idSelected] : 'Tout agent confondu';
            $this->Csv->addRow(
                array($nomAgent)
            );
      
            foreach( $indicateurs as $title => $value ) {
                $this->Csv->addRow(
                    array_merge(
                        array(''),
                        array(__d( 'statistique', "Indicateurquant.$title" )),
                        array($value)
                    )
                );
            }
        }
	}

    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>
