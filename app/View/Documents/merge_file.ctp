<?php

/**
 *
 * Documents/get_document_list.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */

?>
<?php
$options = array();
foreach($fichierScanes as $fichierScane){
        $fichierNom = basename($fichierScane);
        $options[$fichierScane] = $fichierNom;
}

$formMergeFile = array(
    'name' => 'nouveau_flux',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-6',
    'input' => array(
        'nomFLux' => array(
            'labelText' =>__d('courrier', 'Label.nomFLux'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
				'empty' => false,
                'type'=>'text'
            )
        ),
        'main_doc' => array(
            'labelText' =>__d('courrier', 'Label.mainDoc'),
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'options'=>$options
            )
        )
    )
);
echo $this->Formulaire->createForm($formMergeFile);
echo $this->Form->end();
?>
<script type="text/javascript">
    $('input[name="fichierScanes[]"]').attr("disabled", "disabled");
    var listFiles = <?php echo json_encode($fichierScanes) ?>;

    gui.addbutton({
        element: $('.modal-footer'),
        button: {
            content: "<?php echo __d('default', 'Button.cancel'); ?>",
            class: "btn btn-danger-webgfc btn-inverse",
            action: function () {
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/documents/getFichierScane"; ?>",
                    updateElement: $('#dispenv_tabs'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    $('#dispenv_tabs').html(data);
                });
                $(this).parents('.modal').modal('hide');
                $(this).parents('.modal').empty();
            }
        }
    });

    gui.addbutton({
        element: $('.modal-footer'),
        button: {
            content: "<?php echo __d('default','Button.submit') ?>",
            class: "btn btn-success",
            action: function () {
				var form = $('#nouveau_fluxMergeFileForm');
				if (form_validate(form)) {
					gui.request({
						data: $('#nouveau_fluxMergeFileForm').serialize() + '&listFiles=' + listFiles,
						url: "<?php echo Configure::read('BaseUrl') . "/documents/sousRepertoireMerge"; ?>",
						updateElement: $('.modal-footer'),
						loader: true,
						loaderMessage: gui.loaderMessage
					}, function (data) {
						window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/disp"; ?>";
					});
				} else {
					swal({
						showCloseButton: true,
						title: "Oops...",
						text: "Veuillez vérifier votre formulaire!",
						type: "error"
					});
				}

            }
        }
    });
    gui.disablebutton({
        element: $(".modal-footer"),
        button: "<?php echo __d('default','Button.submit') ?>",
        unbindAction: true

    });


    $('#nouveau_fluxMainDoc').change(function () {
        if ($('#nouveau_fluxMainDoc').val() != "" && $('#nouveau_fluxNomFLux').val() != '') {
            gui.enablebutton({
                element: $('.modal-footer'),
                button: "<?php echo __d('default','Button.submit') ?>",
                action: function () {
                    gui.request({
                        data: $('#nouveau_fluxMergeFileForm').serialize() + '&listFiles=' + listFiles,
                        url: "<?php echo Configure::read('BaseUrl') . "/documents/sousRepertoireMerge"; ?>",
                        updateElement: $('.modal-footer'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {

                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/disp"; ?>";
                    });
                }
            });
        } else {
            gui.disablebutton({
                element: $(".modal-footer"),
                button: "<?php echo __d('default','Button.submit') ?>",
                unbindAction: true

            });
        }
    });

    $('#nouveau_fluxNomFLux').blur(function () {
        if ($('#nouveau_fluxMainDoc').val() != "" && $('#nouveau_fluxNomFLux').val() != '') {
            gui.enablebutton({
                element: $('.modal-footer'),
                button: "<?php echo __d('default','Button.submit') ?>",
                action: function () {
                    gui.request({
                        data: $('#nouveau_fluxMergeFileForm').serialize() + '&listFiles=' + listFiles,
                        url: "<?php echo Configure::read('BaseUrl') . "/documents/sousRepertoireMerge"; ?>",
                        updateElement: $('.modal-footer'),
                        loader: true,
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/disp"; ?>";
                    });
                }
            });
        } else {
            gui.disablebutton({
                element: $(".modal-footer"),
                button: "<?php echo __d('default','Button.submit') ?>",
                unbindAction: true

            });
        }
    });
</script>
