
<?php
/**
 *
 * Documents/upload.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('/js/contextFunction.js', array('inline' => false));
?>
<script type="text/javascript">
<?php if ($uploadValid && isset($fluxId)) { ?>
    if (window.top.window.$('#documents').length != 0) {
        gui.request({
            url: '/courriers/getDocuments/<?php echo $fluxId; ?>',
            updateElement: window.top.window.$('#documents')
        });
    }
    saveAuto2();
    function saveAuto2() {
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopId = urlCouper[urlCouper.length - 1];

        gui.request({
            url: '/courriers/setInfos/<?php echo $fluxId; ?>',
            updateElement: window.top.window.$('#infos')
        });
    }
<?php } ?>
    if (window.top.window.layer.msg) {
        window.top.window.layer.msg("<?php echo __d('default', $uploadValid ? 'Upload.valid' : $uploadError); ?>");
    }
</script>
