<?php

/**
 *
 * Documents/get_fichier_scane.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Yuzhu JIN
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="row" id="listeFichierScanner">
    <div class="table-list">
        <h3 class="panel-title"><?php echo  __d('courrier', 'H3.getFichierScane'); ?><span class="action-title"><a href="/environnement" class="btn btn-primary"><i class="fa fa-arrow-left" aria-hidden="true"></i></a></span></h3>
        <div class="panel-body">
        <?php
            echo $this->Html->tag('hr');
            echo $this->Html->tag('div', __d('document', 'Document.fichierscan.info2'), array('class' => 'alert alert-warning','role'=>"alert"));
            if(count($pathFichiers)!=0 && $setRepo){
                echo '<div class="table_fchier_scane">';
                echo $this->Form->create('listFichierScane');
        ?>
            <table class="fchier_scane"
                   data-toggle="table"
                   data-height="399"
                   data-show-refresh="false"
                   data-show-toggle="false"
                   data-search="true"
                   data-pagination="true"
                   data-page-size ="8"
                   data-locale = "fr-CA"
                   data-select-item-name="toolbar1">
                <thead>
                    <tr>
                        <th class="filescan" data-align="center" data-width="50px"><i class="fa fa-dot-circle-o" aria-hidden="true" style="margin-bottom: 5px;"></i></th>
                        <th><?php echo __d('courrier','TableHead.nom'); ?></th>
                        <th>Taille</th>
                        <th class="actions">Visualiser</th>
                        <th class="actions">Supprimer</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                    foreach($pathFichiers as $i => $pathFichier):
                            $fichierNom = basename($pathFichier);
                            $fichierTaille = filesize($pathFichier);
                            $checkbox = $this->Form->input('',array('type'=>'checkbox','value'=>$pathFichier,'name'=>'fichierScanes[]','label'=>false));
                ?>
                    <tr>
                        <td class="checkFile"><?php echo $checkbox; ?></td>
                        <td id='<?php echo $i; ?>' class="nomFichier"><?php echo $fichierNom; ?></td>
                        <td><?php echo $fichierTaille; ?> Ko</td>
                        <td class="actions show-file"><a href="#"  onclick = "preview(this);
                                return false;"><i class="fa fa-eye" alt="Prévisualisation" title="Prévisualisation"></i></a></td>
                        <td class="actions moveFromImport delete-file"><i class="fa fa-trash" alt="Déplacement du fichier PDF" title="Déplacement du fichier PDF"></i></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
                <?php echo $this->Form->end();?>

        </div>
        <div class="panel-footer">
            <div id="controls_fcihierScane" class=" controls" role="group"></div>
        </div>
    </div>
        <?php
            }else{
                if( !$setRepo ) {
                    echo '<div class="table_fchier_scane">';
                    echo '<div class="errorFileScan">';
                        echo __d('document', 'Document.fichierscan.setRepo');

                    echo '</div>';
                    echo '</div>';
                }else if ( $setRepo && !$isdirMerge) {
                    echo '<div class="table_fchier_scane">';
                        echo '<div class="errorFileScan">';
                        echo sprintf( __d('document', 'Document.fichierscan.setMerge'), $repo );
                        echo '</div>';
                        echo '</div>';
                }else if ( $setRepo && !$isdirSingle) {
                     echo '<div class="table_fchier_scane">';
                    echo '<div class="errorFileScan">';
                    echo sprintf( __d('document', 'Document.fichierscan.setSingle'), $repo );
                    echo '</div>';
                    echo '</div>';
                }else {
                    echo '<div class="table_fchier_scane"><p class="alert alert-info">Votre répertoire ne contient aucun fichier !</p></div>';
                    echo '</div>';
                    echo '<div class="panel-footer">';
                    echo '<div id="controls_fcihierScane" class=" controls" role="group"></div>';
                    echo '</div>';
                }
            }
        ?>
</div>

<script type="text/javascript">

    $('#flux_context').empty();
    $('.fchier_scane').bootstrapTable({});
    $(document).ready(function () {
        $('th.actions').css({'width': '80px', 'text-align': 'center'});
        $('td.actions').css('text-align', 'center');
    });
    gui.addbutton({
        element: $('#controls_fcihierScane'),
        button: {
            content: '<i class="fa fa-plus" aria-hidden="true"></i> <?php echo __d('courrier','Bouton.NouveauFLuxParSeulFichier') ?>',
            class: 'btn-info-webgfc ',
            action: function () {
                gui.request({
                    data: $('#listFichierScaneGetFichierScaneForm').serialize(),
                    url: "<?php echo Configure::read('BaseUrl') . "/documents/nouveauFlux"; ?>",
                    updateElement: $('#controls_nouveauFlux'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/disp"; ?>";
                });
            }
        }
    });
    gui.addbutton({
        element: $('#controls_fcihierScane'),
        button: {
//            content: '<i class="fa fa-random" aria-hidden="true"></i> <?php echo __d('courrier','Bouton.NouveauFLuxParFusionnerFichier') ?>',
            content: '<img src="/img/fusion_white_24px.png" style="width:14px; height:14px;"></img> <?php echo __d('courrier','Bouton.NouveauFLuxParFusionnerFichier') ?>',
            class: 'btn-info-webgfc ',
            action: function () {
                gui.formMessage({
                    data: $('#listFichierScaneGetFichierScaneForm').serialize(),
                    url: "<?php echo Configure::read('BaseUrl') . "/documents/mergeFile"; ?>",
                    title: '<?php echo  __d('courrier', 'H3.mergeFileH3'); ?>',
                    loaderMessage: gui.loaderMessage,
                    width: 600,
                    buttons: {
                    }
                });
            }
        }
    });

    gui.disablebutton({
        element: $("#controls_fcihierScane"),
        button: ' <?php echo __d('courrier','Bouton.NouveauFLuxParFusionnerFichier') ?>',
    });

    gui.disablebutton({
        element: $("#controls_fcihierScane"),
        button: ' <?php echo __d('courrier','Bouton.NouveauFLuxParSeulFichier') ?>'
    });

    $('.checkFile input').change(function () {
        if ($('.checkFile input:checked').length > 1) {
            gui.enablebutton({
                element: $("#controls_fcihierScane"),
                button: ' <?php echo __d('courrier','Bouton.NouveauFLuxParFusionnerFichier') ?>',
            });
        } else {
            if ($('.checkFile input:checked').length != 0) {
                gui.enablebutton({
                    element: $("#controls_fcihierScane"),
                    button: ' <?php echo __d('courrier','Bouton.NouveauFLuxParSeulFichier') ?>',
                });
            } else {
                gui.disablebutton({
                    element: $("#controls_fcihierScane"),
                    button: ' <?php echo __d('courrier','Bouton.NouveauFLuxParSeulFichier') ?>',
                });
            }
            gui.disablebutton({
                element: $("#controls_fcihierScane"),
                button: ' <?php echo __d('courrier','Bouton.NouveauFLuxParFusionnerFichier') ?>',
            });
        }
    });
    gui.addbutton({
        element: $('#controls_fcihierScane'),
        button: {
            content: "<?php echo __d('default', 'Button.cancel'); ?>",
            class: 'btn-danger-webgfc btn-inverse ',
            action: function () {
                location.reload();
            }
        }
    });

    $('.fchier_scane td.checkFile').each(function () {
        var container = $(this);
        $(container).click(function () {
        });
    });
<?php if(count($pathFichiers)==0){ ?>
    gui.disablebutton({
        element: $("#controls_fcihierScane"),
        button: '<i class="fa fa-plus" aria-hidden="true"></i> <?php echo __d('courrier','Bouton.NouveauFLuxParSeulFichier') ?>',
        unbindAction: true
    });
<?php } ?>

    function preview(e) {
        var fileName = $(e).parents('tr').find('.nomFichier').html();
        gui.formMessage({
            updateElement: $('.table_fchier_scane'),
            loader: true,
            width: '550px',
            loaderMessage: gui.loaderMessage,
            title: '<?php echo __d('document', 'Document.previewScan'); ?>',
            url: "<?php echo Configure::read('BaseUrl') . "/documents/getFileScannePreview/";?>" + fileName,
            buttons: {
                '<i class="fa fa-times" aria-hidden="true"></i> Fermer': function () {
                    $(this).parents('.modal').modal('hide');
//                    var filePath = $(this).parents('.modal').find('#filePath').val();

                    $(this).parents('.modal').empty();
                }
            }
        });
    }

    // Action de suppression (en réalite simple déplacement du fichier vers import/old/invalid
    $('.fchier_scane td.moveFromImport').click(function () {
        var hrefFile = $(this).parents('tr').find('#listFichierScane').val();
        var scanFile = hrefFile.substring(hrefFile.lastIndexOf("/") + 1);
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
            text: "<?php echo __d('default', 'alertRemoveFromImport'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
            cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/documents/removeFromImport/";?>" + scanFile,
                    data: $('#listFichierScaneGetFichierScaneForm').serialize(),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                }, function (data) {
                    window.location.reload();
                    getJsonResponse(data);
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n\'avez pas supprimé.",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });

</script>
