<?php

/**
 *
 * Services/set_droits_data.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
$options = array(
	'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('alt' => 'no')),
	'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('alt' => 'yes')),
    'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
    'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
//	'parentNodeMarkOpen' => $this->Html->image('/DataAcl/img/parent_node_mark_close.png', array('style' => 'vertical-align: middle;', 'alt' => 'closed')),
//	'parentNodeMarkClose' => $this->Html->image('/DataAcl/img/parent_node_mark_open.png', array('style' => 'vertical-align: middle;', 'alt' => 'opened')),
	'edit' => true,
	'tableRootClass' => 'ui-widget ui-widget-content ui-corner-all',
	'legendClass' => 'ui-widget ui-widget-content ui-corner-all ui-state-focus',
	'fieldsetClass' => /*'ui-widget ui-widget-content ui-corner-all'*/'',
	'fieldsetStyle' => 'margin-top: 15px;',
	'legendStyle' => 'padding-left: 55px;padding-right: 55px;padding-top: 3px;padding-bottom: 3px;',
	'requester' => array(
		'model' => 'Service',
		'foreign_key' => $id
	),
	'aclType' => 'DataAcl',
	'fields' => array(
		'ressource' => __d('typesoustype', 'Types/Soustypes'),
		'read' => 'Accès'
	)
);
$this->DataAcl->drawTable($dataRights, false, $options);
?>
<script type="text/javascript">
    initTables(true);
    $('.submit').each(function () {
        $('input[type=submit]', this).remove();
    });


    $('#ServiceSetDroitsDataForm table th').css({cursor: 'pointer'});
    $('#ServiceSetDroitsDataForm table th').click(function () {
        <?php $foreignKey = 0;
            foreach( $dataRights as $key => $value ):?>
                <?php $foreignKey = $value['foreign_key'];?>
                if ($("#RightsType<?php echo $foreignKey;?>Read").prop("checked")) {
                    $("#RightsType<?php echo $foreignKey;?>Read").prop("checked", false);
                    $("#RightsType<?php echo $foreignKey;?>Read").attr("state", "unchecked");
                }
                else {
                    $("#RightsType<?php echo $foreignKey;?>Read").prop("checked", true);
                    $("#RightsType<?php echo $foreignKey;?>Read").attr("state", "checked");
                }
                <?php if(isset($value['children']) && !empty($value['children'])) :?>
                    <?php foreach( $value['children'] as $keyChildren => $valueChildren ) :?>
                        <?php $foreignKeyChildren = $valueChildren['foreign_key'];?>
                        if ($("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked")) {
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", false);
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "unchecked");
                        }
                        else {
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", true);
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "checked");
                        }
                    <?php endforeach;?>
                <?php endif;?>
        <?php endforeach;?>
    });

    <?php $foreignKey = 0;
        foreach( $dataRights as $key => $value ):?>
            <?php $foreignKey = $value['foreign_key'];?>
            $("#RightsType<?php echo $foreignKey;?>Read").click(function () {
                <?php if(isset($value['children']) && !empty($value['children'])) :?>
                    <?php foreach( $value['children'] as $keyChildren => $valueChildren ) :?>
                        <?php $foreignKeyChildren = $valueChildren['foreign_key'];?>
                        if ($("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked")) {
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", false);
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "unchecked");
                        }
                        else {
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").prop("checked", true);
                            $("#RightsSoustype<?php echo $foreignKeyChildren;?>Read").attr("state", "checked");
                        }
                    <?php endforeach;?>
                <?php endif;?>
            });


    <?php endforeach;?>
</script>
