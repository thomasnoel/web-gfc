<?php

/**
 *
 * Addressbooks/get_addressbooks.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="panel-body container" id="zone_addressbook">
    <div class="col-sm-8 col-xs-8" id="zone_carnet">
        <div class="panel panel-primary" style="border-color:#6AB3C4;">
            <div class="panel-heading" style="color:black;background-color: #6AB3C4;border-color:#6AB3C4;">
                Carnet
                <span>
                    <a class="btn btn-primary" href="#" id="add_carnet" title="Ajouter un carnet" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                    <a class="btn btn-primary" href="#" id="show_carnet" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                </span>
            </div>
            <div class="panel-body">
                <table id="table_carnet"
                       data-toggle="table"
                       data-locale = "fr-CA"
                       data-height="550"
                       >
                </table>
            </div>
        </div>
    </div>
    <div class="col-sm-2 col-xs-2" id="zone_organisme">
        <div class="panel panel-info">
            <div class="panel-heading" style="color:#000;background-color: #A1DBEA;border-color:#A1DBEA;">
                Organisme
                <span class="" role="group">
                    <a class="btn btn-primary" href="#" id="add_organisme" title="Ajouter un organisme" ><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                    <a class="btn btn-primary " href="#" id="show_organism" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                </span>
            </div>
            <div class="panel-body">
                <div class="alert alert-warning">Consulter un carnet pour afficher les organismes ou lancer une recherche</div>
            </div>
        </div>
    </div>
    <div class="col-sm-2 col-xs-2" id="zone_contact">
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color: #D4F2FA;border-color:#D4F2FA;">
                Contact
                <span  class="" role="group">
                    <a class="btn btn-primary" href="#" id="add_contact"  title="Ajouter un contact"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                    <a class="btn btn-primary " href="#" id="show_contact" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                </span>
            </div>
            <div class="panel-body">
                <div class="alert alert-warning">Consulter un organisme pour afficher les contacts ou lancer une recherche</div>
            </div>
        </div>
    </div>
</div>


<div id="searchModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">Recherche</h4>
                    </div>
                    <div class="panel-body content">
                        <?php
                        $searchZone = array('organisme' => 'Organisme', 'contact' => 'Contact');
                        $formSearch = array(
                            'name' => 'Addressbook',
                            'label_w' => 'col-sm-4',
                            'input_w' => 'col-sm-6',
                            'form_url' =>array('controller' => 'addressbooks', 'action' => 'searchResultat'),
                            'input' => array(
                                'Search.search_zone' => array(
                                    'labelText' =>"",
                                    'inputType' => 'radio',
                                    'items'=>array(
                                        'legend' => false,
                                        'separator'=> '</div><div  class="radioUserGedLabel">',
                                        'before' => '<div class="radioUserGedLabel">',
                                        'after' => '</div>',
                                        'label' => true,
                                        'type'=>'radio',
                                        'options' => $searchZone
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formSearch);
                        ?>
                        <div class="searchZoneOrganisme">
                            <?php
                            $formSearchOrganisme = array(
                                'name' => 'Addressbook',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-6',
                                'form_url' =>array('controller' => 'addressbooks', 'action' => 'searchResultat'),
                                'input' => array(
                                    'Search.Organisme.addressbook_id' => array(
                                        'labelText' =>__d('organisme', 'Organisme.addressbook_id'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'options' => $addressbooksList,
                                            'empty' => true,
                                            'required' => false
                                        )
                                    ),
                                    'Search.Organisme.name' => array(
                                        'labelText' =>__d('addressbook', 'Organisme.name'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'required' => false
                                        )
                                    ),
                                    'Search.Organisme.active' => array(
                                        'labelText' =>'Organisme actif ?',
                                        'inputType' => 'checkbox',
                                        'items'=>array(
                                            'type'=>'checkbox',
                                            'default' => true,
                                            'checked' => true,
                                            'required' => false
                                        )
                                    )
                                )
                            );
                            if( Configure::read('Conf.SAERP')) {
                                $formSearchOrganisme['input']['Search.Organisme.Operation.id'] = array(
                                    'labelText' =>"N° de l'OP",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $operations,
                                        'empty' => true,
                                        'required' => false
                                    )
                                );
                                $formSearchOrganisme['input']['Search.Organisme.Activite.id'] = array(
                                    'labelText' =>"Activités",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $activites,
                                        'empty' => true,
                                        'required' => false
                                    )
                                );
                                /*$formSearchOrganisme['input']['Search.Organisme.Fonction.id'] = array(
                                    'labelText' =>"Fonction",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $fonctions,
                                        'empty' => true,
                                        'required' => false
                                    )
                                );
                                $formSearchOrganisme['input']['Search.Organisme.Event.id'] = array(
                                    'labelText' =>"Evénements",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $events,
                                        'empty' => true,
                                        'required' => false,
                                        'class' => 'JSelectMultiple'
                                    )
                                );*/
                            }
                            echo $this->Formulaire->createForm($formSearchOrganisme);
                            ?>
                        </div>
                        <div class="searchZoneContact">
                            <?php
                            $formSearchContact = array(
                                'name' => 'Addressbook',
                                'label_w' => 'col-sm-4',
                                'input_w' => 'col-sm-6',
                                'form_url' =>array('controller' => 'addressbooks', 'action' => 'searchResultat'),
                                'input' => array(
                                    'Search.Contact.organisme_id' => array(
                                        'labelText' =>__d('organisme', 'Organisme.name'),
                                        'inputType' => 'select',
                                        'items'=>array(
                                            'type'=>'select',
                                            'options' => $organsmeList,
                                            'empty' => true,
                                            'required' => false
                                        )
                                    ),
                                    'Search.Contact.name' => array(
                                        'labelText' =>__d('contact', 'Contact.name'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'required' => false
                                        )
                                    ),
                                    'Search.Contact.nom' => array(
                                        'labelText' =>__d('contact', 'Contact.nom'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'required' => false
                                        )
                                    ),
                                    'Search.Contact.prenom' => array(
                                        'labelText' =>__d('contact', 'Contact.prenom'),
                                        'inputType' => 'text',
                                        'items'=>array(
                                            'type'=>'text',
                                            'required' => false
                                        )
                                    ),
                                    'Search.Contact.active' => array(
                                        'labelText' =>'Contact actif ?',
                                        'inputType' => 'checkbox',
                                        'items'=>array(
                                            'type'=>'checkbox',
                                            'default' => true,
                                            'checked' => true,
                                            'required' => false
                                        )
                                    )
                                )
                            );
                            if( Configure::read('Conf.SAERP')) {
                                $formSearchContact['input']['Search.Contact.Operation.id'] = array(
                                    'labelText' =>"N° de l'OP",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $operations,
                                        'empty' => true,
                                        'required' => false
                                    )
                                );
                                $formSearchContact['input']['Search.Contact.Fonction.id'] = array(
                                    'labelText' =>"Fonction",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $fonctions,
                                        'empty' => true,
                                        'required' => false
                                    )
                                );
                                $formSearchContact['input']['Search.Contact.Event.id'] = array(
                                    'labelText' =>"Evénements",
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $events,
                                        'empty' => true,
                                        'required' => false,
                                        'class' => 'JSelectMultiple'
                                    )
                                );
                            }
                            echo $this->Formulaire->createForm($formSearchContact);
                            ?>
                        </div>
                    </div>
                    <div class="panel-footer controls " role="group">
                        <a type="button" class="btn btn-danger-webgfc btn-inverse cancelBtn" data-dismiss="modal"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler </a>
                        <a type="button" class="btn btn-success searchBtn" id="searchBtn" ><i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<script>

    $(document).ready(function () {
        $("#searchModal .searchZoneContact").hide();
        $("#searchModal .searchZoneOrganisme").hide();
        $("#searchModal .searchZoneOrganisme #SearchOrganismeAddressbookId").select2({allowClear: true, placeholder: "Sélectionner un carnet d'adresse"});
        $("#searchModal .searchZoneContact #SearchContactOrganismeId").select2({allowClear: true, placeholder: "Renseigner un organisme"});
        <?php if (Configure::read('Conf.SAERP')): ?>
        $("#searchModal .searchZoneOrganisme #SearchOrganismeOperationId").select2({allowClear: true, placeholder: "Sélectionner une OP"});
        $("#searchModal .searchZoneOrganisme #SearchOrganismeActiviteId").select2({allowClear: true, placeholder: "Sélectionner une activité"});
//        $("#searchModal .searchZoneOrganisme #SearchOrganismeFonctionId").select2({allowClear: true, placeholder: "Sélectionner une fonction"});
//        $("#searchModal .searchZoneOrganisme #SearchOrganismeEventId").select2({allowClear: true, placeholder: "Sélectionner un évenement"});
        $("#searchModal .searchZoneContact #SearchContactOperationId").select2({allowClear: true, placeholder: "Sélectionner une OP"});
        $("#searchModal .searchZoneContact #SearchContactFonctionId").select2({allowClear: true, placeholder: "Sélectionner une fonction"});
        $("#searchModal .searchZoneContact #SearchContactEventId").select2({allowClear: true, placeholder: "Sélectionner un évenement"});
        <?php endif; ?>

        $('#searchModal #searchBtn').click(function () {
            var forms = $('#searchModal').find('form');
            $.each(forms, function (index, form) {
                var url = form.action;
                var id = form.id;
                if (form_validate($('#' + id))) {
                    gui.request({
                        url: url,
                        data: $('#' + id).serialize()
                    }, function (data) {
                        var obj = JSON.parse(data);
                        var data_serialization = $.param(obj);
                        if (obj.organismes != undefined) {
                            var href = "/organismes/searchOrganismes/";
                            var chargeZone = '#zone_organisme .panel-body';
                            $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
                            $("#zone_organisme").removeAttr('class').addClass("col-sm-8 col-sm-8");
                            $("#zone_contact").removeAttr('class').addClass("col-sm-2 col-sm-2");
                            gui.request({
                                url: href,
                                data: "data=" + data_serialization,
                                updateElement: $(chargeZone),
                                loader: true,
                                loaderMessage: gui.loaderMessage
                            });
                        } else if (obj.contacts != undefined) {
                            var href = "/contacts/searchContacts/";
                            var chargeZone = '#zone_contact .panel-body';
                            $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
                            $("#zone_organisme").removeAttr('class').addClass("col-sm-2 col-sm-2");
                            $("#zone_contact").removeAttr('class').addClass("col-sm-8 col-sm-8");
                            gui.request({
                                url: href,
                                data: data_serialization,
                                updateElement: $(chargeZone),
                                loader: true,
                                loaderMessage: gui.loaderMessage
                            });
                        }

                    });
                    $('#searchModal').modal('hide');
                } else {
                    swal({
                        showCloseButton: true,
                        title: "Oops...",
                        text: "Veuillez vérifier votre formulaire!",
                        type: "error",

                    });
                }
            });
        });
    });

    $('#AddressbookSearchResultatForm input[type=radio]').change(function () {
        if ($(this).val() == 'organisme') {
            $("#searchModal .searchZoneContact").hide();
            $("#searchModal .searchZoneOrganisme").show();
        } else if ($(this).val() == 'contact') {
            $("#searchModal .searchZoneContact").show();
            $("#searchModal .searchZoneOrganisme").hide();
        }
    });

    $('#table_carnet')
            .bootstrapTable({
                data: <?php echo json_encode($addressbooks); ?>,
                columns: [
                    {
                        field: "name",
                        title: "Nom",
                        class: "name"
                    },
                    {
                        field: "view",
                        title: "",
                        align: "center"

                    },
                    {
                        field: "edit",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "exportcsv",
                        title: "",
                        align: "center"
                    }
                ]
            }).on('search.bs.table', function (e, text) {
        chargerFunctionJquery();
    });

    $(document).ready(function () {
        chargerFunctionJquery();
    });

    $('#show_carnet').click(function () {
        $("#zone_carnet").removeAttr('class').addClass("col-sm-8 col-sm-8");
        $("#zone_organisme").removeAttr('class').addClass("col-sm-2 col-sm-2");
        $("#zone_contact").removeAttr('class').addClass("col-sm-2 col-sm-2");
    });

    $('#show_organism').click(function () {
        $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
        $("#zone_organisme").removeAttr('class').addClass("col-sm-8 col-sm-8");
        $("#zone_contact").removeAttr('class').addClass("col-sm-2 col-sm-2");
    });

    $('#show_contact').click(function () {
        $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
        $("#zone_organisme").removeAttr('class').addClass("col-sm-2 col-sm-2");
        $("#zone_contact").removeAttr('class').addClass("col-sm-8 col-sm-8");
    });

    $('#add_carnet').click(function () {
        $('#infos').modal('show');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . '/addressbooks/add' ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderShowDiv: $('#liste .content'),
            loaderMessage: gui.loaderMessage
        });
        $('#infos .panel-title').html("<?php echo __d('addressbook', 'Addressbook.add'); ?>");
    });
    $('#add_organisme').click(function () {
        $('#infos').modal('show');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . '/organismes/add/' ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderShowDiv: $('#liste .content'),
            loaderMessage: gui.loaderMessage
        });
        $('#infos .panel-title').html("Ajout d'un organisme");
    });
    $('#add_contact').click(function () {
        $('#infos').modal('show');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') .'/contacts/addParCarnet' ?>",
            updateElement: $('#infos .content'),
            loader: true,
            loaderShowDiv: $('#liste .content'),
            loaderMessage: gui.loaderMessage
        });
        $('#infos .panel-title').html("<?php echo __d('contact', 'Contact.add'); ?>");
    });

    function chargerFunctionJquery() {
        $.getScript("/js/addressbook.js");

        // visualistion des carnets d'adresse
        $('.viewAddressbook').click(function () {
            var id = $(this).attr('id').substring(12);
            var href = '/addressbooks/get_organisme/' + id;
            var chargeZone = '#zone_organisme .panel-body';
            $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
            $("#zone_organisme").removeAttr('class').addClass("col-sm-8 col-sm-8");
            $("#zone_contact").removeAttr('class').addClass("col-sm-2 col-sm-2");
            viewItem(href, chargeZone);
        });

        function viewItem(href, chargeZone) {
            gui.request({
                url: href,
                updateElement: $(chargeZone),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            if (chargeZone == "#infos .content") {
                $('#infos .panel-title').empty();
                $('#infos').modal('show');
            }
        }

        // édition des carnets d'adresse
        $('.editAddressbook').click(function () {
            var id = $(this).attr('id').substring(12);
            var href = '/addressbooks/edit/' + id;
            editItem(href);
        });
        function editItem(href) {
            $('#infos').modal('show');
            gui.request({
                url: href,
                updateElement: $('#infos .content'),
                loader: true,
                loaderShowDiv: $('#liste .content'),
                loaderMessage: gui.loaderMessage
            });
        }

        // export des carnets d'adresse
        $('.exportAddressbook').click(function () {
            var id = $(this).attr('id').substring(12);
            var href = '/addressbooks/export/' + id;
            exportItem(href);
        });
        function exportItem(href) {
            window.location.href = href;
        }
    }

    $('#searchModal').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#searchModal .searchBtn').trigger('click');
            return false;
        }
        if (e.keyCode === 27) {
            $('#searchModal .cancelBtn').trigger('click');
            return false;
        }
    });

//
</script>
