<?php

/**
 *
 * Uniterifs/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php if( !empty($uniterifs) ) {
    $fields = array(
        'name',
        'description',
        'active'
    );
    $actions = array(
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/uniterifs/edit/',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/uniterifs/delete/',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadUniterif();;"
        )
    );
    $options = array();
    $data = $this->Liste->drawTbody($uniterifs, 'Uniterif', $fields, $actions, $options);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_uniterif"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>

<script type="text/javascript">
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($uniterifs,$options); ?>');
    $('#table_uniterif')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d('uniterif', 'Uniterif.name'); ?>",
                        class: "name"
                    },
                    {
                        field: "description",
                        title: "<?php echo __d('uniterif', 'Uniterif.description'); ?>",
                        class: "description"
                    },
                    {
                        field: "active",
                        title: "<?php echo 'Active'; ?>",
                        class: "active_column"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            });
    $(document).ready(function () {
        addClassActive();
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
    function addClassActive() {
        $('#table_uniterif .active_column').hide();
        $('#table_uniterif .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
<?php
    echo $this->LIste->drawScript($uniterifs, 'Uniterif', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('uniterif', 'Uniterif.void'), array('class' => 'alert alert-warning'));
 }
 ?>
