<?php

    $this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Informations sur les plis concernant la consultation n° '.$consultationNumero ));
    
    $this->Csv->addRow(
        array(
            'N° du pli',
            'Origine du pli',
            'Date et Heure du pli',
            'Observations du pli',
            'N° du lot'
        ));

    foreach( $results as $m => $pli ){
        $numeroPli = $pli['Pliconsultatif']['numero'];
        $originePli = @$origineflux[$pli['Pliconsultatif']['origineflux_id']];
        $objetPli = $pli['Pliconsultatif']['objet'];
        $datesPli = $this->Html2->ukToFrenchDateWithSlashes($pli['Pliconsultatif']['date']).' à '.$pli['Pliconsultatif']['heure']; 
        $lotPli = $pli['Pliconsultatif']['lot'];

        $this->Csv->addRow(
            array(
                $numeroPli,
                $originePli,
                $datesPli,
                $objetPli,
                $lotPli
            )
        );
    }


    Configure::write( 'debug', 0 );
    echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>
