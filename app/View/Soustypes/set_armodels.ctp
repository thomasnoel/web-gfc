<?php

/**
 *
 * Soustypes/set_armodels.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="armodels">
    <?php
    $ulContent = '';
    if (empty($armodels)) {
        $ulContent = $this->Html->tag('li', __d('soustype', 'Soustype.Armodel.void'), array('class' => 'alert alert-warning'));
    } else {
        foreach ($armodels as $armodel) {

            $liContent = $this->Element('ctxdoc', array('gfcDocType' => 'Armodel', 'ctxdoc' => $armodel['Armodel'], 'adminMode' => true,'sendCourrier'=>false,'sendGED'=>false));

            if ($armodel['Armodel']['format'] == 'email') {
                $dlContent = $this->Html->tag('dt', __d('armodel', 'Armodel.email_title'), array('class' => 'ui-state-focus'));
                $dlContent .= $this->Html->tag('dd', $armodel['Armodel']['email_title']);
                $dlContent .= $this->Html->tag('dt', __d('armodel', 'Armodel.email_content'), array('class' => 'ui-state-focus'));
                $dlContent .= $this->Html->tag('dd', $armodel['Armodel']['email_content']);
                $liContent .= $this->Html->tag('dl', $dlContent);
            }

            if ($armodel['Armodel']['format'] == 'sms') {
                $dlContent = $this->Html->tag('dt', __d('armodel', 'Armodel.sms_content'), array('class' => 'ui-state-focus'));
                $dlContent .= $this->Html->tag('dd', $armodel['Armodel']['sms_content']);
                $liContent .= $this->Html->tag('dl', $dlContent);
            }

            $ulContent .= $this->Html->tag('li', $liContent, array('class' => 'ctxdoc'));
        }
    }
    echo $this->Html->tag('ul', $ulContent, array('id' => 'armodelList'));
    echo $this->Html->tag('span', '<i class="fa fa-plus-circle" aria-hidden="true"></i> '.__d('armodel', 'Armodel.bttn.add'), array('class' => 'armodel_bttn_add btn btn-info-webgfc'));
    ?>
</div>
<script type="text/javascript">
	$('div.modal-footer').show();
    $('.armodel_bttn_add').button().click(function () {
        $('#armodels').hide();

        gui.request({
            url: '/armodels/add/' + "<?php echo $soustype_id; ?>",
            updateElement: $('#stype_tabs_2'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
		$('div.modal-footer').hide();

    });
    $('.ctxdocBttnDelete').click(function () {

        var docId = $(this).parent().parent().parent().attr('itemId');
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: '/armodels/delete/' + docId,
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                }, function (data) {
                    getJsonResponse(data);
                    stype_tabs_reload();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });

    // bouton edit
    <?php if( !empty($armodel) ) :?>
        $('.ctxdocBttnEditRmodel').click(function () {
            var armodelId = $(this).parent().parent().parent().attr('itemId');
    //console.log(armodelId);
            gui.formMessage({
                updateElement: $('body'),
                loader: true,
                width: '550px',
                loaderMessage: gui.loaderMessage,
                title: "Modèle d'accusé de réception",
                url: " /armodels/edit/" + armodelId,
                buttons: {
                    '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler': function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    },
                    '<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer': function () {
                        var form = $('#ArmodelEditForm');
                        if (form_validate(form)) {
                            form.submit();
                            $('#ArmodelEditForm').remove();
                            gui.request({
                                url: "<?php echo "/Soustypes/setArmodels/" . $armodel['Armodel']['soustype_id']; ?>",
                                updateElement: $('#stype_tabs_2')
                            });
                            stype_tabs_reload();
                        }
                        else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                    }
                }
            });

        });
    <?php endif;?>

</script>
