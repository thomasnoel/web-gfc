<?php

/**
 *
 * Contacts/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
echo $this->Html->script('jquery/jquery.ui.autocomplete.html.js', array('inline' => true));
?>
<div class="row">
    <?php
    $formContact = array(
        'name' => 'Contact',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-8',
        'input' =>array()
    );
    echo $this->Formulaire->createForm($formContact);
    asort( $civilite );
    ?>
    <div class="col-sm-6">
        <div class="panel" id="addContactIdent" >
            <legend class="panel-title"><?php echo  __d('addressbook', 'Contact.fieldset.identity') ?></legend>
            <div class="panel-body">
                <?php
                 $formAddContactIdent = array(
                    'name' => 'Courrier',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-7',
                    'input' => array(
                        'Contact.organisme_id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $orgId
                            )
                        ),
                        'Contact.addressbook_id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $addressbook_id
                            )
                        ),
                        'Contact.civilite' => array(
                            'labelText' =>__d('addressbook', 'Contact.civilite'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'empty' => true,
                                'options' => $civilite
                            )
                        ),
                        'Contact.nom' => array(
                            'labelText' =>__d('contact', 'Contact.nom'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text',
                                'required' => true
                            )
                        ),
                        'Contact.prenom' => array(
                            'labelText' =>__d('contact', 'Contact.prenom'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.role' => array(
                            'labelText' =>__d('contact', 'Contact.role'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.titre_id' => array(
                            'labelText' =>__d('contact', 'Contact.titre_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options'=> $titres,
                                'empty' => true
                            )
                        ),
                        'Contact.name' => array(
                            'labelText' =>__d('contact', 'Contact.name'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        )
                    )
                );
                 echo $this->Formulaire->createForm($formAddContactIdent);
                ?>
            </div>
        </div>

        <div class="panel " id="addContactLien" >
            <legend class="panel-title"><?php echo  __d('addressbook', 'Contactinfo.fieldset.work') ?></legend>
            <div class="panel-body">
                <?php
                $formAddContactLien = array(
                    'name' => 'Courrier',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-7',
                    'input' => array(
                         'Contact.email' => array(
                            'labelText' =>__d('contact', 'Contact.email'),
                            'inputType' => 'email',
                            'items'=>array(
                                'type'=>'email'
                            )
                        ),
                        'Contact.tel' => array(
                            'labelText' =>__d('contact', 'Contact.tel'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.portable' => array(
                            'labelText' =>__d('contact', 'Contact.portable'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        ),
                        'Contact.lignedirecte' => array(
                            'labelText' =>__d('contact', 'Contact.lignedirecte'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        )
                    )
                );
                if(Configure::read('Conf.SAERP')){
                    $formAddContactLien['input']['Operation.Operation'] = array(
                        'labelText' =>__d('contact', 'Contact.operation_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'empty' => true,
                            'multiple' => true,
                            'options' => $operations
                        )
                    );
                    $formAddContactLien['input']['Event.Event'] = array(
                        'labelText' =>__d('contact', 'Contact.event_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'empty' => true,
                            'multiple' => true,
                            'options' => $events
                        )
                    );
                    $formAddContactLien['input']['Contact.fonction_id'] = array(
                        'labelText' =>__d('contact', 'Contact.fonction_id'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'empty' => true,
                                'options' => $fonctions
                            )
                    );
                    $formAddContactLien['input']['Contact.observations'] = array(
                        'labelText' =>__d('contact', 'Contact.observations'),
                        'inputType' => 'textarea',
                        'items'=>array(
                            'type'=>'textarea'
                        )
                    );
                }
                 echo $this->Formulaire->createForm($formAddContactLien);
                ?>
            </div>
        </div>
    </div>
    <div class="panel col-sm-6" id="addContactAdd" >
        <legend class="panel-title"><?php echo __d('contact', 'Contact.adresses') ?></legend>
        <div class="panel-body">
            <?php
            $formAddContactAdd = array(
                'name' => 'Courrier',
                'label_w' => 'col-sm-6',
                'input_w' => 'col-sm-6',
                'input' => array(
                    'Contact.adressecomplete' => array(
                        'labelText' =>__d('contact', 'Contact.adressecompleteLable'),
                        'inputType' => 'text',
                        'items'=>array(
                            'title'=>__d('contact', 'Contact.adressecompleteTitle'),
                            'type'=>'text'
                        )
                    ),
                    'Contact.ban_id' => array(
                        'labelText' =>__d('contact', 'Contact.ban_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array()
                        )
                    ),
                    'Contact.bancommune_id' => array(
                        'labelText' =>__d('contact', 'Contact.bancommune_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array()
                        )
                    ),
                    'Contact.banadresse' => array(
                        'labelText' =>__d('contact', 'Contact.banadresse'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type'=>'select',
                            'options' => array()
                        )
                    ),
                    'Contact.numvoie' => array(
                        'labelText' =>__d('contact', 'Contact.numvoie'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Contact.nomvoie' => array(
                        'labelText' =>__d('contact', 'Contact.nomvoie'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Contact.compl' => array(
                        'labelText' =>__d('contact', 'Contact.compl'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Contact.cp' => array(
                        'labelText' =>__d('contact', 'Contact.cp'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Contact.ville' => array(
                        'labelText' =>__d('contact', 'Contact.ville'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Contact.pays' => array(
                        'labelText' =>__d('contact', 'Contact.pays'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Contact.organisme_id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden',
                            'value' => $orgId
                        )
                    ),
                    'Contact.addressbook_id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden',
                            'value' => $addressbook_id
                        )
                    ),
                )
            );
            if( Configure::read('CD') == 81 ) {
                $formAddContactAdd['input']['Contact.canton'] = array(
                    'labelText' =>__d('contact', 'Contact.canton'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                );
            }
            echo $this->Formulaire->createForm($formAddContactAdd);
            ?>
        </div>
    </div>
<?php
echo $this->Form->end();
?>
</div>

<div id='addViewOrganisme' class='panel-footer'>
    <a id="cancelNewContact" class="btn btn-danger-webgfc btn-inverse"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
    <a id="saveNewContact" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
</div>

<script type="text/javascript">
    $('#ContactCivilite').select2({allowClear: true, placeholder: "Sélectionner une civilité"});
    $('#ContactBanId').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#ContactBancommuneId').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#ContactBanadresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});

    gui.enablebutton({
        element: $('#infos .controls'),
        button: "Valider"
    });

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";

            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }

    });

    //contruction du tableau de types / soustypes
    var bans = [];
    <?php foreach ($bans as $ban) { ?>
    var ban = {
        id: "<?php echo $ban['Ban']['id']; ?>",
        name: "<?php echo $ban['Ban']['name']; ?>",
        banscommunes: []
    };
    <?php foreach ($ban['Bancommune'] as $communeB) { ?>
    var bancommune = {
        id: "<?php echo $communeB['id']; ?>",
        name: "<?php echo $communeB['name']; ?>"
    };
    ban.banscommunes.push(bancommune);
        <?php } ?>
    bans.push(ban);
    <?php } ?>

    function fillBanscommunes(ban_id, bancommune_id) {
        $("#ContactBancommuneId").empty();
        $("#ContactBancommuneId").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#ContactBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {
                        $("#ContactBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }


    $("#ContactBanId").append($("<option value=''> --- </option>"));
    //remplissage de la liste des types
    for (i in bans) {
        $("#ContactBanId").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }
    //definition de l'action sur type
    $("#ContactBanId").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunes(ban_id);
    });

    var checkAdresses = function (bancommune_id) {
        $("#ContactBanadresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getAdresses/' + bancommune_id,
            data: $('#ContactAddForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#ContactBanadresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        $("#ContactBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                    }
                }
            }
        });
    }

    $("#ContactBancommuneId").bind('change', function () {
        var bancommune_id = $('option:selected', this).attr('value');
        checkAdresses(bancommune_id);
    });
    $('#addressbook_valide').unbind("click");
    $('#addressbook_annuler').unbind("click");
    $('#addressbook_valide').bind('click', function () {
        if (form_validate($('#ContactAddForm'))) {
            gui.request({
                url: '/contacts/add/<?php echo $orgId; ?>',
                data: $('#ContactAddForm').serialize(),
                loaderElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                gui.request({
                    loader: true,
                    updateElement: $('#infos .content'),
                }, function (data) {
                    $('.ui-dialog-content').html(data);
                    $('.loader').remove();
                    loadAddressbooks();
                });
            });
            $(this).parents('.modal').modal('hide');
            $(this).parents('.modal').empty();
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }
    });

    $('#addressbook_annuler').bind('click', function () {
        $(this).parents('.modal').modal('hide');
        $(this).parents('.modal').empty();
        gui.formMessage({
            url: '/organismes/edit/<?php echo $orgId; ?>',
            loader: true,
            loaderMessage: gui.loaderMessage,
            updateElement: $('#liste .content'),
            buttons: {"<i class='fa fa-floppy-o' aria-hidden='true'></i> Valider": {
                    text: '<i class="fa fa-check" aria-hidden="true"></i> Valider'',
                            id: "addressbook_valide",
                    click: function () {
                        var form = $(this).parents('.modal').find('form');
                        if (form_validate(form)) {
                            gui.request({
                                url: '/organismes/edit/<?php echo $orgId; ?>',
                                data: form.serialize()
                            }, function (data) {
                                getJsonResponse(data);
                                loadAddressbooks();
                            });
                            $(this).parents(".modal").modal('hide');
                            $(this).parents(".modal").empty();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }

                    }
                }, "<i class='fa fa-times-circle-o' aria-hidden='true'></i> Annuler": {
                    text: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
                    id: "addressbook_annuler",
                    click: function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    }
                }}
        }, function () {
            loadAddressbooks();
        });
    });


    var checkHomonyms = function () {
        $('.homonyms').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Contacts/getHomonyms',
            data: $('#ContactAddForm').serialize(),
            success: function (data) {

                if (data.length > 0) {
                    var homonyms = $('<div></div>').html('<?php echo __d('contact', 'Contact.HomonymsListTitle'); ?>').addClass('homonyms').addClass('alert alert-warning');
                    var homonymList = $('<ul></ul>');

                    for (var i in data) {
						if( data[i].Contact.adresse == null ) {
							homonymList.append($('<li></li>').html(data[i].Contact.name + ' (' + data[i].Addressbook.name + ' / ' + data[i].Organisme.name + ')'));
						}
						else {
							homonymList.append($('<li></li>').html(data[i].Contact.name + ' (' + data[i].Addressbook.name + ' / ' + data[i].Organisme.name  + ' / <br />' + data[i].Contact.adresse + ' ' + data[i].Contact.cp + ' ' + data[i].Contact.ville + ')'));
						}
                    }
                    homonyms.append(homonymList);

                    $('#ContactName').parent().after(homonyms);

                }
            }
        });

    }
    $('#ContactNom, #ContactPrenom').blur(function () {
        if ($('#ContactPrenom').val() != "" || $('#ContactNom').val() != "") {
            $('#ContactName').val($('#ContactPrenom').val() + " " + $('#ContactNom').val());
            checkHomonyms();
        }
    });

    $('#ContactName').change(function () {
        checkHomonyms();
    });
    $('#ContactAddForm').addClass('checkHomonyms');

    // Using jQuery UI's autocomplete widget:
    $('#ContactAdressecomplete').bind("keydown", function (event) {
        //keycodes - maj : 16, ctrl : 17, alt : 18
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#ContactBanId').val(0);
            $('#ContactBancommuneId').html('');
        }
        if ($(this).attr('value') != '') {
            if (event.keyCode === $.ui.keyCode.TAB &&
                    $(this).data("autocomplete").menu.active) {
                event.preventDefault();
            }
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#ContactBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var banville = json['Bancommune'];
                var ban = json['Ban'];
                var num = banadresse['numero'];
                if (typeof banadresse['rep'] !== 'undefined') {
                    num = banadresse['numero'] + " " + banadresse['rep'];
                }
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#ContactBanId').val(ban['id']).change();
                    $('#ContactBancommuneId').val(banville['id']).change();
                    $("#ContactBanadresse").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                    $("#ContactBanadresse").val(banadresse['nom_afnor']).change();
                    $('#ContactNumvoie').val(num);
                    if( banadresse['nom_afnor'] != "" ) {
                        $('#ContactNomvoie').val(banadresse['nom_afnor']);
                    }
                    else {
                        $('#ContactNomvoie').val(banadresse['nom_voie']);
                    }
//                    $('#ContactNomvoie').val(banadresse['nom_afnor']);
                    $('#ContactCp').val(banadresse['code_post']);
                    $('#ContactVille').val(banville['name']);
                    <?php if( Configure::read('CD') == 81 ):?>
                        $('#ContactCanton').val(banadresse['canton']);
                    <?php endif;?>
                }
            });
        }
    });


    <?php if( !Configure::read('Conf.SAERP')) { ?>
    //$("#ContactBanId").val("<?php /*echo isset( $bans[0]['Ban']['id'] ) ? $bans[0]['Ban']['id'] : '';*/ ?>").change();
    <?php } ?>

    // Partie pour mettre à jour le code postal selon le nom de voie sélectionné
    $('#ContactBanadresse').change(function () {
//        refreshCodepostal($("#ContactBancommuneId").val(), $("#ContactBanadresse").val());
    });
    var refreshCodepostal = function (bancommuneId, banadresseName) {
        var params = '';
        if (banadresseName != '') {
            var params = bancommuneId + '/' + banadresseName;
        }
        else {
            params = bancommuneId;
        }
        gui.request({
            url: '/bansadresses/getCodepostal/' + params
        }, function (data) {
            var json = jQuery.parseJSON(data);
            var codepostal = json;
            if( codepostal['nom_afnor'] != "" ) {
                $('#ContactNomvoie').val(codepostal['nom_afnor']);
            }
            else {
                $('#ContactNomvoie').val(codepostal['nom_voie']);
            }
//            $('#ContactNomvoie').val(codepostal['nom_afnor']);
            $('#ContactCp').val(codepostal['codepostal']);
            $('#ContactVille').val(codepostal['ville']);
            <?php if( Configure::read('CD') == 81 ):?>
                $('#ContactCanton').val(codepostal['canton']);
            <?php endif;?>
        });
    }
    <?php if( !empty( $organisme ) ) :?>
    $("#ContactNomvoie").val("<?php echo isset( $organisme['Organisme']['banadresse'] ) ? $organisme['Organisme']['banadresse'] : $organisme['Organisme']['nomvoie'];?>").change();
    $("#ContactNumvoie").val("<?php echo $organisme['Organisme']['numvoie'];?>").change();
    $("#ContactCompl").val("<?php echo $organisme['Organisme']['compl'];?>").change();
    $("#ContactCp").val("<?php echo $organisme['Organisme']['cp'];?>").change();
    $("#ContactVille").val("<?php echo $organisme['Organisme']['ville'];?>").change();
    <?php if(Configure::read('CD') == 81 ) :?>
        $("#ContactCanton").val("<?php echo $organisme['Organisme']['canton'];?>").change();
    <?php endif;?>
    <?php endif;?>

    $('#addViewOrganisme').hide();
    if ($('#infos #ContactAddForm').is(':visible')) {
        $('#infos .panel-footer').hide();
        $('#infos .panel-default').append($('#addViewOrganisme').show());
    }
    $('#cancelNewContact').click(function () {
        gui.request({
            url: '/organismes/get_organismes/<?php echo $orgId; ?>',
            updateElement: $('#infos .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
        $('#addViewOrganisme').remove();
        $('#infos .panel-footer').show();
    });
    $('#saveNewContact').click(function () {
        var form = $('#ContactAddForm');
        if (form_validate(form)) {
            gui.request({
                url: form.attr('action'),
                data: form.serialize(),
                updateElement: $('#infos .content'),
                loader: true,
            }, function (data) {
                getJsonResponse(data);
                gui.request({
                    url: '/organismes/get_organismes/<?php echo $orgId; ?>',
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
                $('#addViewOrganisme').remove();
                $('#infos .panel-footer').show();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }
    });


 <?php if( Configure::read('Conf.SAERP') ) :?>
    $('#OperationOperation').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    $('#EventEvent').select2({allowClear: true, placeholder: "Sélectionner un événement"});
    $('#ContactFonctionId').select2({allowClear: true, placeholder: "Sélectionner une fonction"});
<?php endif;?>
    $('#ContactTitreId').select2({allowClear: true, placeholder: "Sélectionner un titre"});
</script>
