<?php

/**
 *
 * Contacts/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author JIN Yuzhu
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="panel panel-default">
    <div class="panel-body">
        <table id="view_contact">
            <tbody>
                <tr>
                    <td class="title">Organisme:</td><td colspan="3" class="content"><?php echo$contact['Organisme']['name'] ?></td>
                </tr>
                <tr>
                    <td class="title">Tél:</td><td colspan="3" class="content"><?php echo$contact['Contact']['tel'] ?></td>
                </tr>
                <tr>
                    <td class="title">Email:</td><td colspan="3" class="content"><?php echo$contact['Contact']['email'] ?></td>
                </tr>
                <tr>
                    <td class="title">Adresse:</td><td colspan="3" class="content"><?php echo$contact['Contact']['adresse'].' '.$contact['Contact']['compl'].' '.$contact['Contact']['cp'].' '.$contact['Contact']['ville'] ?></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<script>
    $('#infos .panel-title').append('<?php echo $contact['Contact']['name']; ?>');
</script>
