<?php

$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Informations sur le contact'));
    
    $this->Csv->addRow(
        array_merge(
            array(
                "ORGANISME",
                "ANTENNE",
                "ACTIVITE",
//                "OPERATIONS DE L'ORGANISME",
                "SIRET",
                "FAX",
                'DATE DE CREATION FICHE OU DERNIERE MODIFICATION',
                "CIVILITE",
                "NOM",
                "PRENOM",
                "FONCTION",
                "TELEPHONE",
                "LIGNE DIRECTE",
                "LIGNE MOBILE",
                "MAIL",
                "ADRESSE",
                "COMPL ADRESSE",
                "CODE POSTAL",
                "VILLE",
                "OPERATIONS DU CONTACT",
                "OBSERVATIONS"
            ),
            $eventColumns
        )
	);

    if( !isset( $results[0] ) && empty($results[0]) ) {
        $neweventColumns = array();
        if( !empty($results['Contact']['events']) ) {
            foreach( $results['Contact']['events'] as $eventsIds => $eventsName ) {
                if( in_array( $eventsName, $eventColumns ) ) {
                    $neweventColumns[$eventsIds] = 'Oui';
                }
                else {
                    $neweventColumns[$eventsIds] = 'Non';
                }
            }
        }

        $modified = $this->Html2->ukToFrenchDateWithHourAndSlashes($results['Contact']['modified']);
    //debug($events);
        $rowContact = array_merge(
            array($results['Organisme']['name']),
            array($results['Organisme']['antenne']),
            array($results['Contact']['operationsactivites']),
    //        array($results['Contact']['operationsorg']),
            array($results['Organisme']['siret']),
            array($results['Organisme']['fax']),
            array($modified),
            array($results['Contact']['civilite']),
            array($results['Contact']['nom']),
            array($results['Contact']['prenom']),
            array(@$results['Fonction']['name']),
            array($results['Contact']['tel']),
            array($results['Contact']['lignedirecte']),
            array($results['Contact']['portable']),
            array($results['Contact']['email']),
            array($results['Contact']['adresse']),
            array($results['Contact']['compl']),
            array($results['Contact']['cp']),
            array($results['Contact']['ville']),
            array($results['Contact']['operationscontact']),
            array($results['Contact']['observations']),
            $neweventColumns
        );

        $this->Csv->addRow(
            array_merge( $rowContact)
        );
    }
    else {
        foreach( $results as $resultats ) {
            $neweventColumns = array();
            if( !empty($resultats['Contact']['events']) ) {
                foreach( $resultats['Contact']['events'] as $eventsIds => $eventsName ) {
                    if( in_array( $eventsName, $eventColumns ) ) {
                        $neweventColumns[$eventsIds] = 'Oui';
                    }
                    else {
                        $neweventColumns[$eventsIds] = 'Non';
                    }
                }
            }
            $modified = $this->Html2->ukToFrenchDateWithHourAndSlashes($resultats['Contact']['modified']);
    //debug($events);
            $rowContact = array_merge(
                array($resultats['Organisme']['name']),
                array($resultats['Organisme']['antenne']),
                array($resultats['Contact']['operationsactivites']),
        //        array($results['Contact']['operationsorg']),
                array($resultats['Organisme']['siret']),
                array($resultats['Organisme']['fax']),
                array($modified),
                array($resultats['Contact']['civilite']),
                array($resultats['Contact']['nom']),
                array($resultats['Contact']['prenom']),
                array(@$resultats['Fonction']['name']),
                array($resultats['Contact']['tel']),
                array($resultats['Contact']['lignedirecte']),
                array($resultats['Contact']['portable']),
                array($resultats['Contact']['email']),
                array($resultats['Contact']['adresse']),
                array($resultats['Contact']['compl']),
                array($resultats['Contact']['cp']),
                array($resultats['Contact']['ville']),
                array($resultats['Contact']['operationscontact']),
                array($resultats['Contact']['observations']),
                $neweventColumns
            );

            $this->Csv->addRow(
                array_merge( $rowContact)
            );
        }
    }


    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>