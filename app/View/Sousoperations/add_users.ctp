<script type="text/javascript">
    //contruction du tableau de intitulesagents / bureaux
    var intitulesagents = [];
    <?php foreach ($intitulesagents as $intitule) { ?>
    var intitule = {
        id: "<?php echo $intitule['Intituleagent']['id']; ?>",
        name: "<?php echo addslashes( $intitule['Intituleagent']['name'] ); ?>",
        desktopsmanagers: []
    };
    intitulesagents.push(intitule);
    <?php } ?>

    //remplissage de la liste des intitulesagents
    for (i in intitulesagents) {
        $("#DesktopmanagerSousoperationIntituleagentId").append($("<option value='" + intitulesagents[i].id + "'>" + intitulesagents[i].name + "</option>"));
    }
    //definition de l action sur type
    $("#DesktopmanagerSousoperationIntituleagentId").bind('change', function () {
        var intituleagent_id = $('option:selected', this).attr('value');
    });
</script>
<div class="col-sm-12 zone-form">
    <legend class="panel-title"><?php echo __d('operation', 'Operation.newSousoperations'); ?></legend>
    <div class="panel-body">
            <?php
                $formSousoperation = array(
                    'name' => 'Sousoperation',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-5',
                    'form_url' => array( 'controller' => 'sousoperations', 'action' => 'addUsers'),
                    'input' => array(
                        'Sousoperation.id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $sousoperationId
                            )
                        ),
                        'DesktopmanagerSousoperation.sousoperation_id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $sousoperationId
                            )
                        ),
                        'DesktopmanagerSousoperation.typeetape' => array(
                            'labelText' => __d('operation', 'Operation.typeetape'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options' => $types, 
                                'empty' => true,
                            )
                        ),
                        'DesktopmanagerSousoperation.intituleagent_id' => array(
                            'labelText' =>__d('operation', 'Operation.intitule'),
                            'inputType' => 'select',
                            'items'=>array(
                                'type'=>'select',
                                'options' => array(), 
                                'empty' => true,
                                'multiple' => true,
                                'required' => true
                            )
                        ),
                        'DesktopmanagerSousoperation.ordre' => array(
                            'labelText' =>__d('operation', 'Operation.ordre'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text'
                            )
                        )
                    )
                );

                echo $this->Formulaire->createForm($formSousoperation);
                echo $this->Form->end();
            ?>
    </div>
    <div class="controls text-center" role="group">
        <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type='text/javascript'>
    $('#DesktopmanagerSousoperationTypeetape').select2({allowClear: true, placeholder: "Sélectionner un type d'étape"});
    $('#DesktopmanagerSousoperationIntituleagentId').select2({allowClear: true, placeholder: "Sélectionner un poste"});
</script>

<script type="text/javascript">
    $('#valid').click(function () {
        if (form_validate($('#SousoperationAddUsersForm'))) {
            gui.request({
                url: '<?php echo Router::url(array('controller' => 'sousoperations', 'action' => 'addUsers', $sousoperationId)); ?>',
                data: $('#SousoperationAddUsersForm').serialize(),
                loaderElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                getJsonResponse(data);
                loadSousOperation();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",
                
            });
        }

    });
    $('#cancel').click(function () {
        loadSousOperation();
    });





</script>
