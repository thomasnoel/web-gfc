
<div class="col-sm-12 zone-form">
    <legend class="panel-title"><?php echo __d('operation', 'Operation.editSousoperations'); ?></legend>
    <div class="panel-body">
            <?php
                $formSousoperationName = array(
                    'name' => 'Sousoperation',
                    'label_w' => 'col-sm-5',
                    'input_w' => 'col-sm-5',
                    'form_url' => array( 'controller' => 'sousoperations', 'action' => 'edit'),
                    'input' => array(
                        'Sousoperation.id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $sousoperationId
                            )
                        ),
                        'Sousoperation.operation_id' => array(
                            'inputType' => 'hidden',
                            'items'=>array(
                                'type'=>'hidden',
                                'value' => $operationId
                            )
                        ),
                        'Sousoperation.name' => array(
                            'labelText' =>__d('operation', 'Sousoperation.name'),
                            'inputType' => 'text',
                            'items'=>array(
                                'type'=>'text',
                                'value' => $sousoperationName
                            )
                        )
                    )
                );
                echo $this->Formulaire->createForm($formSousoperationName);
                echo '<hr>';

                foreach( $intitules as $ordre => $info ) {
                    if(isset($info['DesktopmanagerSousoperation'])){
                        foreach( $info['DesktopmanagerSousoperation'] as $t => $values) {
                            $typeetape[$t] = $values['typeetape'];
                            $ordreetape[$t] = $values['ordre'];
                            $intituleagentId[$t] = $values['intituleagent_id'];
                            $sousoperationId[$t] = $values['sousoperation_id'];
                            $deskIds[$t] = $values['id'];

                            $typeetapeValue = $typeetape;
                            $ordreEtapeValue = $ordreetape;
                            $intituleagentIdValue = $intituleagentId;
                            $desktopId = $deskIds;
                        }
                        $valueIntitulesagentsIds = Hash::extract( $info, "DesktopmanagerSousoperation.{n}.intituleagent_id" );
                        $valueDesktopsIds = Hash::extract( $info, "DesktopmanagerSousoperation.{n}.id" );


                        echo '<hr width="80%">';
                        echo "<div class='col-sm-5' style='text-align:right'> Etape {$ordre}</div>";
                        echo $this->Html->div('col-sm-7', $this->Html->image(
                            '/img/delete_16.png',
                            array(
                                'title' => __d('default', 'Button.delete'),
                                'alt' => __d('default', 'Button.delete'),
                                'class' => "itemDeleteBureau",
                                'itemId' => $ordre
                            )
                        ));
                        echo "<br/>";

                        $formSousoperation = array(
                            'name' => 'Sousoperation',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-5',
                            'input' => array(
                                'DesktopmanagerSousoperation.DesktopmanagerSousoperation.'.$ordre.'.intituleagent_id' => array(
                                    'labelText' =>__d('operation', 'Operation.intitule'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'required'=>true,
                                        'type'=>'select',
                                        'multiple' => true,
                                        'options' => $intitulesagents2,
                                        'selected' =>  $valueIntitulesagentsIds,
                                        'empty' => true
                                    )
                                ),
                                'DesktopmanagerSousoperation.DesktopmanagerSousoperation.'.$ordre.'.ordre' => array(
                                    'labelText' =>__d('operation', 'Operation.ordre'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'required'=>true,
                                        'type'=>'text',
                                        'value'=>$values['ordre']
                                    )
                                ),
                                'DesktopmanagerSousoperation.DesktopmanagerSousoperation.'.$ordre.'.typeetape' => array(
                                    'labelText' =>__d('operation', 'Operation.typeetape'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'options' => $types,
                                        'empty' => true,
                                        'value' => $values['typeetape']
                                    )
                                ),
                                'DesktopmanagerSousoperation.DesktopmanagerSousoperation.sousoperation_id'.$ordre => array(
                                    'inputType' => 'hidden',
                                    'items'=>array(
                                        'type'=>'hidden',
                                        'value' => $values['sousoperation_id']
                                    )
                                )
                            )
                        );
//                        $formSousoperation['label'] = array(
//                            'labelText' => 'Etape '.$ordre
//                        );
//
//
//                        $formSousoperation['input']['DesktopmanagerSousoperation.DesktopmanagerSousoperation.'.$ordre.'.intituleagent_id'] = array(
//                            'labelText' =>__d('operation', 'Operation.intitule'),
//                            'inputType' => 'select',
//                            'items'=>array(
//                                'required'=>true,
//                                'type'=>'select',
//                                'multiple' => true,
//                                'options' => $intitulesagents2,
//                                'selected' =>  $valueIntitulesagentsIds,
//                                'empty' => true
//                            )
//                        );
//                        $formSousoperation['input']['DesktopmanagerSousoperation.DesktopmanagerSousoperation.'.$ordre.'.ordre'] = array(
//                            'labelText' =>__d('operation', 'Operation.ordre'),
//                            'inputType' => 'text',
//                            'items'=>array(
//                                'required'=>true,
//                                'type'=>'text',
//                                'value'=>$values['ordre']
//                            )
//                        );
//                        $formSousoperation['input']['DesktopmanagerSousoperation.DesktopmanagerSousoperation.'.$ordre.'.typeetape'] = array(
//                            'labelText' =>__d('operation', 'Operation.typeetape'),
//                            'inputType' => 'select',
//                            'items'=>array(
//                                'type'=>'select',
//                                'options' => $types,
//                                'empty' => true,
//                                'value' => $values['typeetape']
//                            )
//                        );
//                        $formSousoperation['input']['DesktopmanagerSousoperation.DesktopmanagerSousoperation.sousoperation_id.'.$ordre] = array(
//                            'inputType' => 'hidden',
//                               'items'=>array(
//                                   'type'=>'hidden',
//                                   'value' => $values['sousoperation_id']
//                               )
//                        );
                    }
                    echo $this->Formulaire->createForm($formSousoperation);
                }
                echo $this->Form->end();
            ?>
    </div>
    <div class="controls text-center" role="group">
        <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type='text/javascript'>
    <?php foreach( $intitules as $b => $info ) :?>
    var b = $.trim('<?php echo ucfirst($b); ?>');
    $("#DesktopmanagerSousoperationDesktopmanagerSousoperation" + b + "IntituleagentId").select2({allowClear: true, placeholder: "Sélectionner un poste"});
    $("#DesktopmanagerSousoperationDesktopmanagerSousoperation" + b + "Typeetape").select2({allowClear: true, placeholder: "Sélectionner un type d'étape"});
    <?php endforeach;?>
</script>
<script type="text/javascript">
    $('#valid').click(function () {

        var item = $(this);
        var ordreId = $(this).attr("itemId");
        var sousoperationId = $('#SousoperationId').val();


        var url = '<?php echo Router::url(array('controller' => 'sousoperations', 'action' => 'edit')); ?>/' + sousoperationId;
        if (form_validate($('#SousoperationEditForm'))) {
            gui.request({
                url: url,
                data: $('#SousoperationEditForm').serialize(),
                loaderElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                getJsonResponse(data);
                loadSousOperation();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });
    $('#cancel').click(function () {
        loadSousOperation();
    });

    $('img.itemDeleteBureau').click(function () {
        var item = $(this);
        var ordreId = $(this).attr("itemId");
        var sousoperationId = $('#SousoperationId').val();

        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
            text: "<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#d33",
            cancelButtonColor: "#3085d6",
            confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
            cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/sousoperations/deleteAssociation/" + sousoperationId + "/" + ordreId,
                    loader: true,
                    loaderMessage: gui.loaderMessage,
                    updateElement: $('#liste .content')
                }, function (data) {
                    getJsonResponse(data);
                    loadSousOperation();

                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n\'avez pas supprimé.",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
    });
</script>

