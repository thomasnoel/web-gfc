<?php

/**
 *
 * Desktopsmanagers/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
      //echo $this->Html->css(array('administration'), null, array('inline' => false));
?>

<script type="text/javascript">
    function loadBureaux() {
        $('#infos .content').empty();
        $('#liste table tr').removeClass('ui-state-focus');
        gui.request({
            url: "<?php echo Configure::read('BaseUrl') . "/desktopsmanagers/get_bureaux"; ?>",
            updateElement: $('#liste .content'),
            loader: true,
            loaderMessage: gui.loaderMessage
        });
    }
</script>

<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div id="liste" class="row">
        <div  class="table-list">
            <h3><?php echo __d('desktopmanager', 'Desktopmanager.liste'); ?></h3>
            <div class="content">
            </div>
        </div>
        <div class="controls panel-footer">
        </div>
    </div>
</div>

<div id="infos" class="modal fade" role="dialog">
    <div class="modal-dialog" style="width: 1000px">
        <div class="modal-content">
            <div class="panel">
                <div class="panel panel-default">
                    <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
                        <h4 class="panel-title"><?php echo __d('desktopmanager', 'Desktopmanager.infos'); ?></h4>
                    </div>
                    <div class="panel-body content">
                    </div>
                    <div class="panel-footer controls " role="group">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    gui.buttonbox({
        element: $('#liste .controls')
    });

	gui.addbutton({
		element: $('#liste .controls'),
		button: {
			content: "<i class='fa fa-download' aria-hidden='true'></i> Télécharger la liste",
			title: "<?php echo __d('desktop', 'Button.exporttype'); ?>",
			class: 'btn  btn-info-webgfc export_types',
			action: function () {
				window.location.href = "<?php echo Router::url( array( 'controller' => 'desktopsmanagers', 'action' => 'export' ) + Hash::flatten( $this->request->data, '__' ) );?>";
			}
		}
	});

    // FIXME: A remettre lorsque l'on aura trouvé une façon plus rapide d'importer les gros fichiers
    gui.addbutton({
        element: $('#liste .controls'),
        button: {
            content: '<i class="fa fa-plus-circle" aria-hidden="true"></i> Ajouter un bureau',
            class: "btn-info-webgfc ",
            title: "<?php echo __d('default', 'Button.add'); ?>",
            action: function () {
                $('#infos').modal('show');
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/Desktopsmanagers/add"; ?>",
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }
        }
    });

    gui.buttonbox({
        element: $('#infos .controls'),
        align: "center"
    });

    gui.addbuttons({
        element: $('#infos .controls'),
        buttons: [
            {
                content: "<?php echo __d('default', 'Button.cancel'); ?>",
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {
                    $('#infos').modal('hide');
                }
            },
            {
                content: "<?php echo __d('default', 'Button.submit'); ?>",
                class: "btn-success ",
                action: function () {
                    var forms = $(this).parents('.modal').find('form');
                    $.each(forms, function (index, form) {
                        var url = form.action;
                        var id = form.id;
                        if (form_validate($('#' + id))) {
                            gui.request({
                                url: url,
                                data: $('#' + id).serialize()
                            }, function (data) {
                                getJsonResponse(data);
                                loadBureaux();
                            });
                            $(this).parents('.modal').modal('hide');
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    });
                }
            }
        ]
    });

    loadBureaux();

    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/admin"; ?>";
                }
            }
        ]
    });
</script>
