<?php

$this->Csv->preserveLeadingZerosInExcel = true;

$this->Csv->addRow(array('Informations sur les bureaux'));

$this->Csv->addRow(
	array(
		'Bureau',
		'Agents'
	)
);

foreach ($results as $m => $bureau) {
	$nomDesktopmanager = $bureau['Desktopmanager']['name'];
	$rowDesktopmanager[$m] = array_merge(
		array($nomDesktopmanager)
	);
	if (!empty($bureau['Desktop'])) {
		foreach ($bureau['Desktop'] as $s => $sstype) {
			$nameDesktops = $sstype['name'];

			$rowDesktop[$m] = array_merge(
				array($nameDesktops)
			);
			$this->Csv->addRow(
				array_merge($rowDesktopmanager[$m], $rowDesktop[$m])
			);
		}
	} else {
		$this->Csv->addRow(
			array_merge(
				$rowDesktopmanager[$m], array('')
			)
		);
	}
}
Configure::write('debug', 0);
echo $this->Csv->render("{$this->request->params['controller']}_{$this->request->params['action']}_" . date('Ymd-His') . '.csv');
?>
