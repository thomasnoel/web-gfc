<?php

/**
 *
 * Courriers/sendmail.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php // echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true)); ?>
<div class="row">
 <?php
 $formSendmail = array(
    'name' => 'Courrier',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Sendmail.courrier_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>$fluxId
            )
        ),
        'Sendmail.desktop_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>$desktopId
            )
        ),
        'Sendmail.document_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>$documentId
            )
        ),
        'Sendmail.email' => array(
            'labelText' =>'Destinataire(s) *',
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'class' => 'JSelectMultiple',
                'options' => $allMails,
                'empty' => true
            )
        ),
        'Sendmail.email2' => array(
            'labelText' =>nl2br( "Destinataire(s)" ),
            'labelPlaceholder' => "Si non présent(s) ci-dessus. Séparés par un ; suivi d'un espace",
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
                'title'=>nl2br( "Destinataire(s) (Si non présent(s) ci-dessus. Séparés par un ; suivi d'un espace)" )
            )
        ),
        'Sendmail.used' => array(
            'labelText' =>nl2br( "Afficher les destinataires en copie" ),
            'inputType' => 'checkbox',
            'items'=>array(
                'type' => 'checkbox',
                'options' => array()
            )
        ),
        'Sendmail.ccmail' => array(
            'labelText' =>'Destinataire(s) en copie',
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'class' => 'JSelectMultiple',
                'options' => $allMails,
                'empty' => true
            )
        ),
        'Sendmail.ccmail2' => array(
            'labelText' =>nl2br("Destinataire(s) en copie "),
            'labelPlaceholder' => "Si non présent(s) ci-dessus. Séparés par un ; suivi d'un espace",
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text',
                'title'=>nl2br( "Destinataire(s) en copie (Si non présent(s) ci-dessus. Séparés par un ; suivi d'un espace)" )
            )
        ),
        'Sendmail.usedcci' => array(
            'labelText' =>nl2br( "Afficher les destinataires en copie cachée" ),
            'inputType' => 'checkbox',
            'items'=>array(
                'type' => 'checkbox',
                'options' => array()
            )
        ),
        'Sendmail.ccimail' => array(
            'labelText' =>'Destinataire(s) en copie cachée',
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'class' => 'JSelectMultiple',
                'options' => $allMails,
                'empty' => true
            )
        ),
        'Sendmail.ccimail2' => array(
            'labelText' =>nl2br("Destinataire(s) en copie cachée "),
            'labelPlaceholder' => "Si non présent(s) ci-dessus. Séparés par un ; suivi d'un espace",
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text',
                'title'=>nl2br( "Destinataire(s) en copie cachée (Si non présent(s) ci-dessus. Séparés par un ; suivi d'un espace)" )
            )
        ),
        'Sendmail.templatemail_id' => array(
            'labelText' => 'Sélection d\'un mail-type',
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'options' => array()
            )
        ),
        'Sendmail.sujet' => array(
            'labelText' => 'Sujet du mail',
            'inputType' => 'text',
            'items'=>array(
                'type' => 'text',
				'required' => true
            )
        ),
        'Sendmail.message' => array(
            'labelText' => 'Contenu du mail',
            'inputType' => 'textarea',
            'items'=>array(
                'type' => 'textarea',
				'required' => true
            )
        ),
        'Document.Document' => array(
            'labelText' => 'Autres PJ',
            'inputType' => 'select',
            'items'=>array(
                'type' => 'select',
                'class' => 'JSelectMultiple',
                'options' => $documentsLies,
                'empty' => true
            )
        )
    )
);

echo $this->Formulaire->createForm($formSendmail);
echo $this->Form->end();
?>
</div>

<script type="text/javascript">
    $('#SendmailTemplatemailId').select2({allowClear: true, placeholder: "Sélectionner un mail-type"});
    $('#SendmailEmail').select2({allowClear: true, placeholder: "Sélectionner une ou plusieurs adresse(s) mail"});
    $('#SendmailCcmail').select2({allowClear: true, placeholder: "Sélectionner une ou plusieurs adresse(s) mail en copie"});
    $('#SendmailCcimail').select2({allowClear: true, placeholder: "Sélectionner une ou plusieurs adresse(s) mail en copie cachée"});
    $('#DocumentDocument').select2({allowClear: true, placeholder: "Sélectionner une ou plusieurs pièce(s) jointe(s) supplémentaire(s)"});

    $('<hr class="col-sm-2"><span class="col-sm-1" id="email2" style="color:#3071A9">ou</span><hr class="col-sm-7">').insertBefore($('#SendmailEmail2').parents('.form-group '));
    $('<hr class="col-sm-2"><span class="col-sm-1" id="ccmail2" style="color:#3071A9">ou</span><hr class="col-sm-7">').insertBefore($('#SendmailCcmail2').parents('.form-group '));
    $('<hr class="col-sm-2"><span class="col-sm-1" id="ccimail2" style="color:#3071A9">ou</span><hr class="col-sm-7">').insertBefore($('#SendmailCcimail2').parents('.form-group '));
<?php if ( !empty($templatemails) ):?>
    //contruction des valeurs de mail-type
    var templatemails = [];
    <?php foreach ($templatemails as $template) { ?>
    var template = {
        id: "<?php echo $template['Templatemail']['id']; ?>",
        name: "<?php echo js_escape($template['Templatemail']['name'] ); ?>",
        subject: "<?php echo js_escape($template['Templatemail']['subject'] ); ?>",
        object: "<?php echo js_escape($template['Templatemail']['object'] ); ?>"
    };
    templatemails.push(template);
    <?php } ?>

    $("#SendmailTemplatemailId").append($("<option value=''> --- </option>"));
    //remplissage de la liste des types
    for (i in templatemails) {
        $("#SendmailTemplatemailId").append($("<option value='" + templatemails[i].id + "'>" + templatemails[i].name + "</option>"));
    }

    $("#SendmailTemplatemailId").bind('change', function () {
        var selected = $("#SendmailTemplatemailId").val();
        for (i in templatemails) {
            if (selected == templatemails[i].id) {
                $("#SendmailSujet").val(templatemails[i].subject).change();
                $("#SendmailMessage").val(templatemails[i].object).change();
            }
        }
    });
    <?php endif;?>

    $('#CourrierSendmailForm select.JSelectMultiple').each(function () {
        setJselectNew($(this));
    });


    $('#s2id_SendmailCcmail').parents('.form-group ').hide();
    $('#SendmailCcmail2').parents('.form-group ').hide();
    $('#SendmailCcmail2 span.col-sm-1').hide();
    $('#SendmailCcmail2').parents('.form-group ').hide();

    $('#s2id_SendmailCcimail').parents('.form-group ').hide();
    $('#SendmailCcimail2').parents('.form-group ').hide();
    $('#SendmailCcimail2 span.col-sm-1').hide();
    $('#SendmailCcimail2').parents('.form-group ').hide();

//    $('#email2').hide();
    $('#ccmail2').hide();
    $('#ccimail2').hide();

    $('#SendmailUsed').change(function () {
        if ($('#SendmailUsed').prop("checked")) {
            $('#s2id_SendmailCcmail').parents('.form-group ').show();
            $('#SendmailCcmail2').parents('.form-group ').show();
            $('#SendmailCcmail2 span.col-sm-1').show();
            $('#ccmail2').show();
        }
        else {
            $('#s2id_SendmailCcmail').parents('.form-group ').hide();
            $('#SendmailCcmail2').parents('.form-group ').hide();
            $('#SendmailCcmail2 span.col-sm-1').hide();
            $('#ccmail2').hide();
        }
    });

    $('#SendmailUsedcci').change(function () {
        if ($('#SendmailUsedcci').prop("checked")) {
            $('#s2id_SendmailCcimail').parents('.form-group ').show();
            $('#SendmailCcimail2').parents('.form-group ').show();
            $('#SendmailCcimail2 span.col-sm-1').show();
            $('#ccimail2').show();
        }
        else {
            $('#s2id_SendmailCcimail').parents('.form-group ').hide();
            $('#SendmailCcimail2').parents('.form-group ').hide();
            $('#SendmailCcimail2 span.col-sm-1').hide();
            $('#ccimail2').hide();
        }
    });

	gui.addbuttons({
		element: $('#SendmailEmail2').parent()	,
		buttons: [
			{
				content: "<i class='fa fa-plus' aria-hidden='true'></i> Récupérer le mail du contact",
				class: "btn-info-webgfc",
				action: function () {
					if( '<?php echo $contactMail;?>' != '') {
						$('#SendmailEmail2').val('<?php echo $contactMail;?>');
					}
					else {
						alert('Aucun mail présent pour le contact');
					}
				}
			}
		]
	});
</script>
