<?php
if (isset($viewOnly) && $viewOnly === true) {
    echo $this->Element('viewOnly');
}
?>
<?php
if (empty($comments['public']) && empty($comments['private'] )&& empty($comments['written'])) {
    echo $this->Html->tag('div', __d('comment', 'Comment.empty'), array('class' => 'alert alert-warning'));
}
echo $this->GFCComment->drawComments($comments);
?>
