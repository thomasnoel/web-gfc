<?php

/**
 *
 * Courriers/reponse.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$filedsetContent = $this->Html->tag('legend', __d('courrier', 'Courrier.response_choice_fieldset_label'));
$filedsetContent .= $this->Html->div('alert alert-info',__d('courrier', 'Courrier.response_choice_message'));
$formCourrierReponse = array(
    'name' => 'CourrierReponse',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Soustypes' => array(
            'labelText' =>__d('soustype', 'Soustypes'),
            'inputType' => 'select',
            'items'=>array(
                'required'=>true,
                'type'=>'select',
                'empty' => true,
                'options' => $soustypes_lies
            )
        ),
        'InsertAuto' => array(
            'labelText' =>__d('courrier', 'Courrier.response_insert_auto'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked' => $checked,
                'value' => $checked
            )
        )
    )
);
echo $this->Formulaire->createForm($formCourrierReponse);
echo $this->Form->end();
?>
<script type="text/javascript">
    $('#CourrierReponseSoustypes').select2();

    <?php if( $checked) :?>
        $('#CourrierReponseInsertAuto').prop('checked',true);
    <?php endif;?>
</script>
