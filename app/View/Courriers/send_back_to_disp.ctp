<?php

/**
 *
 * Courriers/send_back_to_disp.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
if($messageAlert) {?>

	<div class="">
    	<p class="error"></p>
		<script type="text/javascript">
			$('.modal').remove();
			swal(
				'Oops...',
				"Ré-aiguillage impossible.<br>Au moins un des flux sélectionnés est déjà dans un circuit.",
				'warning'
			);
		</script>
	</div>
<?php
}
else if($fluxIsInCopy) {
	?>
	<div class="">
    	<p class="error"></p>
		<script type="text/javascript">
			$('.modal').remove();
			swal(
					'Oops...',
					"Ré-aiguillage impossible.<br>Au moins un des flux sélectionnés est en copie.",
					'warning'
			);
		</script>
	</div>
<?php }
else {
?>
<div class="">
    <legend class="col-sm-9">Veuillez choisir un destinataire pour le flux</legend>
    <?php
        $formCourrier = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-4',
            'input' => array(
                'Courrier.dispInitDesktop' => array(
                    'labelText' =>__d('courrier', 'chooseInitDesktopService'),
                    'inputType' => 'select',
                    'items'=>array(
                        'required'=>true,
                        'type'=>'select',
                        'options' => $services,
                        'empty' => true
                    )
                ),
				'Comment.objet' => array(
					'labelText' =>__d('comment', 'Comment.objet'),
					'inputType' => 'textarea',
					'items'=>array(
						'type'=>'textarea'
					)
				)
            )
        );
        echo $this->Formulaire->createForm($formCourrier);
        foreach ($checkItem['checkItem'] as $item) {
    ?>
    <input type="hidden" name="data[checkItem][]" value="<?php echo $item; ?>" />
    <?php
        }

    echo $this->Form->end();
?>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#CourrierDispInitDesktop").select2();
    });
</script>
<?php
}
?>
