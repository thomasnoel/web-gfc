<?php

/**
 *
 * Courriers/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('SimpleComment.comment.css'), null, array('inline' => false));
//echo $this->Html->script('jquery/jquery.ui.autocomplete.html.js', array('inline' => true));
echo $this->Html->script('contactinfo.js', array('inline' => true));
echo $this->Html->script('contextFunction.js', array('inline' => true));

?>

<script type="text/javascript">
    function findForm() {
        //definition du formulaire a envoyer
        var form = false;
        $('.col-sm-11').each(function () {
            if ($(this).css('display') == "block" && $('form', this).attr('action') !== undefined) {
                form = $('form', this);
            }
        });
        return form;
    }
</script>

<div class="container">
    <div class="row">
        <!-- formulaire new flux -->
        <div class="table-list" id="createFlux">
            <h3>Nouveau Flux</h3>
            <!-- Progress bar-->
            <div class="container-fluid" id="flux_infos">
                <div class="row bs-wizard">
                        <?php
						$etapBarre = 'col-xs-3';
                        $cptTab = 1;
                        if ($rights['createInfos']) {
                            echo '
                            <div class="'.$etapBarre.' bs-wizard-step active etape_'.$cptTab.'">
                            <div class="text-center bs-wizard-stepnum">Étape '.$cptTab.'</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#infos" id="clickInfo" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Information</div>
                            </div>';
                            $cptTab++;
                        }
                        if ($rights['setMeta']) {
                            echo '<div class="'.$etapBarre.' bs-wizard-step disabled etape_'.$cptTab.'"">
                            <div class="text-center bs-wizard-stepnum">Étape '.$cptTab.'</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#medas" class="bs-wizard-dot"></a>
                            <div class="bs-wizard-info text-center">Méta-données</div>
                            </div>';
                            $cptTab++;
                        }
                        echo '
                        <div class="'.$etapBarre.' bs-wizard-step disabled etape_'.$cptTab.'"">
                        <div class="text-center bs-wizard-stepnum">Étape '.$cptTab.'</div>
                        <div class="progress"><div class="progress-bar"></div></div>
                        <a href="#commentaires" class="bs-wizard-dot"></a>
                        <div class="bs-wizard-info text-center">Commentaire</div>
                        </div>';
                        ?>
                </div>
                <!-- fin Progress bar-->
                <!-- panel -->
                    <?php
        //createInfos
                    if ($rights['createInfos']) {
                        ?>
                <div class="panel col-sm-11" id="infos"  style="display: none">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Information</h4>
                        </div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                        <?php
                    }
        //set Metadonnees
                    if ($rights['setMeta']) {
                        ?>
                <div class="panel col-sm-11" id="medas"  style="display: none">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<h4 class="panel-title refunique">N° de référence: </h4>
							<br />
							<h4 class="panel-title">Méta-données</h4>
                        </div>
                        <div class="panel-body">
                        </div>
                    </div>
                </div>
                        <?php
                    }
                    ?>
                <!--ajouter commentaire-->
                <div class="panel col-sm-11" id="commentaires" style="display: none">
                    <div class="panel panel-default">
                        <div class="panel-heading">
							<h4 class="panel-title  refunique">N° de référence: </h4>
							<br />
							<h4 class="panel-title">Commentaire</h4>
                        </div>
                        <div class="panel-body">
                        </div>
                        <div  id="commentFooter">
                        </div>
                    </div>
                </div>
                <!-- fin panel -->
                <div id="flux_context_add"  class="context row tabbable tabs-left">
                    <!-- nav bar context -->
                    <ul class="nav nav-tabs">
                            <?php
                            echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-arrow-left"  aria-hidden="true"></i>', '#', array('escape' => false,"data-toggle"=>"tab","id"=>"cacheLateral")));

                            echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-camera-retro" title="'.__d('courrier', 'Context.preview').'" aria-hidden="true"></i>', '#preview', array('escape' => false,"data-toggle"=>"tab")),array('class'=>'active'));

                            if ($rights['getDocuments']) {
                                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-file" title="'.__d('courrier', 'Documents').'" aria-hidden="true"></i>', '#documents', array('escape' => false,"data-toggle"=>"tab")));
                            }
                            if ($rights['getRelements'] && Configure::read('DOC_FROM_GFC') || Configure::read( 'Conf.SAERP') || Configure::read('Flux.interne') || Configure::read( 'Conf.Gabarit') || Configure::read( 'Conf.CD') ) {
                                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-file-text" title="'.__d('courrier', 'Relements.noresp').'" aria-hidden="true"></i>', '#relements', array('escape' => false,"data-toggle"=>"tab")));
                            }
                            if ($rights['getArs'] && Configure::read('DOC_FROM_GFC')) {
                                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-envelope" title="'.__d('courrier', 'Ars').'" aria-hidden="true"></i>', '#ars', array('escape' => false,"data-toggle"=>"tab")));
                            }
                            if ($rights['getCircuit']) {
                                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-road" title="'.__d('courrier', 'Circuit').'" aria-hidden="true"></i>', '#circuit', array('escape' => false,"data-toggle"=>"tab")));
                            }
                            ?>
                    </ul>
                    <!-- fin nav bar context -->
                    <!-- contents context -->
                    <div class="tab-content row">
                        <div  class="tab-pane fade in active"  id="preview">
                            <div class="document_preview">
                                <h4 class="header"><?php echo __d('document', 'Document.preview'); ?> : <span class="selected_document"></span>
                                        <?php if (!empty($mainDoc)) { ?>
                                    <a  href="/Documents/getPreview/<?php echo $mainDoc['Document']['id']; ?>/true" target="_blank" >
										<i class="fa fa-compress" aria-hidden="true" title="Plein écran"></i>
                                    </a>
                                        <?php } ?>
                                </h4>
                                <div class="content"><div class="alert alert-warning"><?php echo __d('document', 'Document.preview.disable'); ?></div></div>
                            </div>
                        </div>
                            <?php
                            if ($rights['getDocuments']) {
                                echo '
                                <div  class="tab-pane fade" id="documents">';
                                if (empty($fluxId)) {
                                    echo '<div class="alert alert-warning">
                                    '.__d('courrier', 'Document.void').'<br />
                                    <br />
                                    <br />
                                    <br />'.
                                    __d('courrier', 'Document.void.upload')
                                    .'<br />
                                    <br />
                                    <br />
                                    <br />
                                    </div>';
                                }
                                echo'</div>';
                            }
                            if ($rights['getRelements'] &&  Configure::read('DOC_FROM_GFC') || Configure::read( 'Conf.SAERP')) {
                                echo '
                                <div  class="tab-pane fade" id="relements">
                                </div>';
                            }
                            if ($rights['getArs'] /*&& empty($parent) && !$sortant */&& Configure::read('DOC_FROM_GFC')) {
                                echo '
                                <div  class="tab-pane fade" id="ars">
                                </div>';
                            }
                            if ($rights['getCircuit']) {
                                echo '
                                <div  class="tab-pane fade" id="circuit">
                                </div>';
                            }
                            ?>
                    </div>
                    <!-- fin contents context -->
                </div>
            </div>
            <!-- button box-->
            <div class="panel-footer controls " role="group" id="controls">
                <a id="next" class="btn btn-success " ><?php echo __d('courrier', 'Suivant'); ?> <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
            <!-- fin button box-->
        </div>
        <!-- fin formulaire new flux -->

        <!-- -->
        <!-- context -->
        <!--        <div class="table-list">
                        <h3>PLus:</h3>

                    </div>-->
        <!-- fin context -->
    </div>
</div>


<script type="text/javascript">

    $('#flux_infos >.panel').css('margin-right', '0px');
    $('#flux_context_add .tab-content').hide();
    $('#cacheLateral').toggle(function () {


		$('#flux_infos>.panel').removeClass('col-sm-7').addClass('col-sm-11');
		if ($('#flux_infos').width() > 1012) {
			$('#flux_infos >.panel.col-sm-11 .panel.col-sm-12').removeClass('col-sm-12').addClass('col-sm-6');
		}
		$('#flux_context_add .tab-content').hide();
		$('#flux_context_add').removeClass('col-sm-5').addClass('col-sm-1').css('margin-right', '0px');
		$('#cacheLateral').html('<i class="fa fa-arrow-left" aria-hidden="true"></i>');

		$('#controls').css('margin-top', '0px');
    }, function () {
		$('#flux_infos >.panel').removeClass('col-sm-11').addClass('col-sm-7');
		$('#flux_infos >.panel.col-sm-7 .panel.col-sm-6').removeClass('col-sm-6').addClass('col-sm-12');
		$('#flux_context_add .tab-content').show();
		$('#flux_context_add').addClass('col-sm-5').removeClass('col-sm-1').css('margin-right', '0px');
		$('#cacheLateral').html('<i class="fa fa-arrow-right" aria-hidden="true"></i>');


		$('#controls').css('margin-top', '120px');

	});




    gui.buttonbox({element: $('#createFlux .panel-footer')});

    var fluxId = null;

//afficahge les contents au dabut
    function affichiagedebut() {
        if ($('.etape_1').hasClass('active')) {
            var tab = $('.etape_1').children('a').attr("href");
            if (tab == "#infos") {
                gui.request({
                    url: "<?php echo Configure::read('BaseUrl') . "/courriers/createInfos"; ?>",
                    updateElement: $('#infos .panel-body'),
                });
            }

        }
    }
    affichiagedebut();

// chargement de contents de page(panel)
    function affichageEtapes(courrierId) {
        if ($('#infos').length > 0 && $('#infos').is(':visible')) {
            $('#infos .panel-body').empty();
            gui.request({
                url: "/courriers/setInfos/" + courrierId,
                updateElement: $('#infos .panel-body'),
            });
        }
        if ($('#medas').length > 0 && $('#medas').is(':visible')) {
            $('#medas .panel-body').empty();
            gui.request({
                url: "/courriers/setMeta/" + courrierId,
                updateElement: $('#medas .panel-body'),
            });
        }
        if ($('#commentaires').length > 0 && $('#commentaires').is(':visible')) {
            $('#commentaires .panel-body').empty();
            gui.request({
                url: "/courriers/setComments/" + courrierId,
                updateElement: $('#commentaires .panel-body'),
            });
        }
    }

//decide afficher quel panel
    function showDiv() {
        if (!$(".bs-wizard-step").last().hasClass('active')) {
            $('.bs-wizard-step').each(function () {
                var etape_class = $(this).attr("class");
                //affichage
                if (etape_class.indexOf("active") >= 0) {
                    $($(this).children('a').attr("href")).show();
                    if ($(this).hasClass('etape_1')) {
                        gui.disablebutton({
                            element: $('#createFlux .panel-footer'),
                            button: " <?php echo __d('courrier', 'Precedent'); ?>"
                        });
                        gui.disablebutton({
                            element: $('#createFlux .panel-footer'),
                            button: "<?php echo __d('courrier', 'Suivant'); ?> "
                        });
                    } else {
                        gui.enablebutton({
                            element: $('#createFlux .panel-footer'),
                            button: '<i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo __d("courrier", "Precedent"); ?>'
                        });
                    }
                } else {
                    $($(this).children('a').attr("href")).hide();
                }
                //click
                if (etape_class.indexOf("disabled") >= 0) {
                    $(this).children('a').unbind('click');
                }

            });
        } else {
            var activePanelId = $('.panel:visible').attr('id');
            var flagEtap = $('a[href="#' + activePanelId + '"]').parent();
            var etape_class = flagEtap.attr("class");
            etape_class.search("etape");
            var nbEtpe = etape_class[etape_class.search("etape") + 6]
            var nextEtape = parseInt(nbEtpe) + 1;
            $('.bs-wizard-step').each(function () {
                $($(this).children('a').attr("href")).hide();
                if ($(this).hasClass('etape_1')) {
                    gui.disablebutton({
                        element: $('#createFlux .panel-footer'),
                        button: '<i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo __d("courrier", "Precedent"); ?>'
                    });
					gui.disablebutton({
						element: $('#createFlux .panel-footer'),
						button: '<?php echo __d("courrier", "Suivant"); ?> <i class="fa fa-arrow-right" aria-hidden="true"></i> '
					});
                } else {
                    gui.enablebutton({
                        element: $('#createFlux .panel-footer'),
                        button: '<i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo __d("courrier", "Precedent"); ?>'
                    });
                }
            });
            $($('.etape_' + nextEtape).find('a').attr('href')).show();
        }
    }
    showDiv();

//Vérification le button Terminer et Inserer
    function getInsertEnd(fluxId) {
        gui.request({
            url: "/courriers/getInsertEnd/" + fluxId,
            loaderElement: $('.table-list'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $("body").append(data);
        });
    }

//Vérification le button Suivant/Terminer
    function showSaveBtn() {

        var activePanelId = $('.panel:visible').attr('id');
        var flagEtap = $('a[href="#' + activePanelId + '"]').parent();
        if (flagEtap.hasClass('active') && flagEtap.is(':last-child')) {
            $('#next').html("<i class='fa fa-floppy-o' aria-hidden='true' ></i> <?php echo __d('courrier', 'Terminer'); ?>");
            $('#next').attr('id', 'terminer');
            gui.removebutton({
                element: $('#controls'),
                button: "<i class='fa fa-floppy-o' aria-hidden='true' ></i> <?php echo __d('courrier', 'Insert Courrier End'); ?>"
            });
            getInsertEnd(fluxId);
            gui.removebutton({
                element: $('#webgfc_content .panel-footer'),
                button: "<i class='fa fa-floppy-o' aria-hidden='true' ></i> <?php echo __d('courrier', 'Terminer'); ?>"
            });

            gui.addbutton({
                element: $('#webgfc_content .panel-footer'),
                button: {
                    content: '<i class="fa fa-floppy-o" aria-hidden="true"></i> <?php echo __d('courrier', 'Terminer'); ?>',
                    title: ' <?php echo __d('courrier', 'Terminer'); ?>',
                    class: 'btn-success',
                    action: function () {
                        var form = findForm();
                        if (form) {
                            if (form_validate(form)) {
                                gui.request({
                                    data: form.serialize(),
                                    url: form.attr('action'),
                                    loaderElement: $('.table-list'),
                                    loaderMessage: gui.loaderMessage
                                }, function (data) {
                                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/user"; ?>";
                                });
                            } else {
                                swal({
                                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez vérifier votre formulaire!",
                                    type: "error"
                                });
                            }
                        } else {
                            window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/index/0/user"; ?>";
                        }
                    }
                }
            });
        } else if ($(".bs-wizard-step").last().hasClass('active')) {
            if ($("#terminer").length > 0) {
                $('#terminer').html("<?php echo __d('courrier', 'Suivant'); ?> <i class='fa fa-arrow-right' aria-hidden='true'></i>");
                $('#terminer').attr('id', 'next');
                $('#next').unbind();
                $('#next').click(function () {
                    nextClick();
                });
            }
            gui.removebutton({
                element: $('#controls'),
                button: "<i class='fa fa-floppy-o' aria-hidden='true' ></i> <?php echo __d('courrier', 'Insert Courrier End'); ?>"
            });
            getInsertEnd(fluxId);
        }
    }

	$('.nav-tabs').hide();

//gerer navigation nav
    if (fluxId != null) {
        affichageTabContext(fluxId);
    }
// Change hash for page-reload:context
    $('#flux_context_add .nav-tabs a').on('shown.bs.tab', function (e) {
        if (fluxId != null) {
            affichageTabContext(fluxId);
        }
    });
//Click sur Progress bar link
    $('.bs-wizard-step').on('click', 'a', function () {
        if ($(this).parent().hasClass('etape_1')) {
            gui.disablebutton({
                element: $('#createFlux .panel-footer'),
                button: '<i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo __d("courrier", "Precedent"); ?>'
            });
        } else {
            gui.enablebutton({
                element: $('#createFlux .panel-footer'),
                button: '<i class="fa fa-arrow-left" aria-hidden="true"></i> <?php echo __d("courrier", "Precedent"); ?>'
            });
        }
        var divShow = $(this).attr("href");
        $('.bs-wizard-step').each(function () {
            $($(this).children('a').attr("href")).hide();
        });
        $(divShow).show();
        if (fluxId != null) {
            affichageEtapes(fluxId);
//todo save all the dataTabs complete
        }
        showSaveBtn();
        return false;
    });
//chargement affichage Progress bar
    function barProgressNext() {
        var panelShowId = $('.panel:visible').attr('id');
        $('.bs-wizard-step').each(function () {
            var etapeLink = $(this).children('a').attr("href");
            if (etapeLink.indexOf(panelShowId) >= 0) {
                var etapeActiveClass = $(this).attr('class');
                etapeActiveClass.search("etape");
                var nbEtpe = etapeActiveClass[etapeActiveClass.search("etape") + 6]
                $(this).removeClass('active');
                $(this).addClass('complete');
                var nextEtape = parseInt(nbEtpe) + 1;
                $('.etape_' + nextEtape).removeClass('disabled');
                if (!$('.bs-wizard-step').last().hasClass('active')) {
                    $('.etape_' + nextEtape).addClass('active');
                }
            }
        });
    }




	//button Suivant
    function nextClick() {
    	// On affiche le panneau latéral lors de la 2eme etape
		$('.nav-tabs').show();

		// On gère la taille des écrans à ce moment là
		$('#flux_infos >.panel').removeClass('col-sm-11').addClass('col-sm-7');
		$('#flux_infos >.panel.col-sm-7 .panel.col-sm-6').removeClass('col-sm-6').addClass('col-sm-12');
		$('#flux_context_add .tab-content').show();
		$('#flux_context_add').addClass('col-sm-5').removeClass('col-sm-1').css('margin-right', '0px');
		$('#cacheLateral').html('<i class="fa fa-arrow-right" aria-hidden="true"></i>');
		$('#controls').css('margin-top', '120px');

		$('.fa-file').trigger('click'); // déclenche l'action au chargement du script

		var panelShowId = $('.panel:visible').attr('id');
        var form = $('#' + panelShowId).find('form');
        if (form.length != 0) {

//            form.validator('validate');
            if (form_validate(form)) {
                gui.request({
                    data: form.serialize(),
                    url: form.attr('action'),
                }, function (data) {
                    var json = getJsonResponse(data);
                    if (json != undefined) {
                        fluxId = json.newCourrierId;
                        affichageEtapes(fluxId);
                        affichageTabContext(fluxId);
						getReference(fluxId);
                    }
                });
                barProgressNext()
                showDiv();
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        } else {
            barProgressNext()
            showDiv();
            affichageEtapes(fluxId);
        }
        showSaveBtn();
    }
    $('#next').click(function () {
        nextClick();
    });



//Variable pour contextFunction.js
    var buttonSubmit = "<?php echo __d('default', 'Button.submit'); ?>";
    var windowLocationHref = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
    var buttonCancel = "<?php echo __d('default', 'Button.cancel'); ?>";
    var titleModalSupp = "<?php echo __d('default', 'Modal.suppressionTitle'); ?>";
    var msgModalSupp = "<?php echo __d('default', 'Modal.suppressionContents'); ?>";
    var titleModalSendmail = "<?php echo __d('default', 'Confirm.sendmail'); ?>";
    var titleModalGED = "<?php echo __d('default', 'Versement en GED'); ?>";
    var msgModalGED = "<?php echo __d('default', 'Etes-vous sûr de vouloir verser ce flux en GED ?'); ?>";
    var mainDoc = null;
    var mainDocId = null;
<?php if (!empty($mainDoc)) { ?>
    mainDoc = '<?php echo $mainDoc;?>';
    mainDocId = <?php echo $mainDoc['Document']['id']; ?>;
<?php } ?>

	function getReference(fluxId) {
		gui.request({
			url: "/courriers/ajaxreference/" + fluxId,
			loaderElement: $('.table-list'),
			loaderMessage: gui.loaderMessage
		}, function (data) {
			$("#medas .refunique").append(data);
			$("#commentaires .refunique").html(null);
			$("#commentaires .refunique").append('N° de référence: ' + data);
		});
	}

</script>
