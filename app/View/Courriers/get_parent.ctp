<?php

/**
 *
 * Courriers/get_parent.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$priority = array(
    0 => array(
        'img' => $this->Html->image('/img/priority_low.png'),
        'txt' => __d('courrier', 'Courrier.priorite_0')
    ),
    1 => array(
        'img' => $this->Html->image('/img/priority_mid.png'),
        'txt' => __d('courrier', 'Courrier.priorite_1')
    ),
    2 => array(
        'img' => $this->Html->image('/img/priority_high.png'),
        'txt' => __d('courrier', 'Courrier.priorite_2')
    )
);
?>
<?php
    if( !empty( $flux['Courrier']['motifrefus'] ) || !empty( $flux['Courrier']['parapheur_commentaire'] )):
?>
<legend class="panel-title"><?php echo __d('courrier', 'Courrier.refusFieldset'); ?></legend>
<dl>
    <dt><?php echo __d('courrier', 'Courrier.motifrefus'); ?></dt><dd><?php echo !empty( $flux['Courrier']['motifrefus'] ) ? $flux['Courrier']['motifrefus'] :  $flux['Courrier']['parapheur_commentaire']; ?></dd>
    <?php if( !empty( $desktopName ) && !empty($flux['Bancontenu'][0]['modified']) ) :?>
    <dt><?php echo 'Emis par :' ?></dt><dd><?php echo !empty( $desktopName ) ? $desktopName :  ''; ?></dd>
    <dt><?php echo 'Le :' ?></dt><dd><?php echo !empty( $flux['Bancontenu'][0]['modified'] ) ? $this->Html2->ukToFrenchDateWithHour($flux['Bancontenu'][0]['modified']) :  ''; ?></dd>
    <?php endif;?>
</dl>
<?php
    endif;
?>
<legend class="panel-title"><?php echo __d('courrier', 'Courrier.fluxFieldset'); ?></legend>
<dl>
    <dt><?php echo __d('courrier', 'Courrier.name'); ?></dt><dd><?php echo $flux['Courrier']['name']; ?></dd>
    <dt><?php echo __d('courrier', 'Courrier.objet'); ?></dt><dd><?php echo $flux['Courrier']['objet']; ?></dd>
    <dt><?php echo __d('courrier', 'Courrier.priorite'); ?></dt><dd>
        <?php
        echo $priority[$flux['Courrier']['priorite']]['img'] . " " . $priority[$flux['Courrier']['priorite']]['txt'];
        ?>
    </dd>
</dl>
<legend class="panel-title"><?php echo __d('courrier', 'Courrier.contactFieldset'); ?></legend>
<dl>
    <dt><?php echo __d('courrier', 'Courrier.contact'); ?></dt><dd><?php echo!empty($flux['Organisme']) ? $flux['Organisme']['name'] : ''; ?></dd>
    <dt><?php echo __d('courrier', 'Courrier.contactinfo'); ?></dt><dd><?php echo isset($flux['Contact']['name']) ? $flux['Contact']['name'] : null; ?></dd>
</dl>
<legend class="panel-title"><?php echo __d('courrier', 'Courrier.dateFieldset'); ?></legend>
<dl>
    <dt><?php echo __d('courrier', 'Courrier.date'); ?></dt><dd><?php echo $this->Html2->ukToFrenchDateWithSlashes($flux['Courrier']['date']); ?></dd>
    <dt><?php echo __d('courrier', 'Courrier.datereception'); ?></dt><dd><?php echo $this->Html2->ukToFrenchDateWithSlashes($flux['Courrier']['datereception']); ?></dd>
</dl>
<legend class="panel-title"><?php echo __d('courrier', 'Qualification'); ?></legend>
<dl>
    <dt><?php echo __d('courrier', 'Courrier.reference'); ?></dt><dd><?php echo $flux['Courrier']['reference']; ?></dd>
    <dt><?php echo __d('soustype', 'Type'); ?></dt><dd><?php echo $flux['Soustype']['Type']['name']; ?></dd>
    <dt><?php echo __d('soustype', 'Soustype'); ?></dt><dd><?php echo $flux['Soustype']['name'];?></dd>
</dl>
<legend class="panel-title"><?php echo __d('courrier', 'Méta-données'); ?></legend>
<dl>
    <?php
    $divContent = $this->Html->tag('div', __d('metadonnee', 'Metadonnee.void'), array('class' => 'alert alert-warning'));
    $dlContent = '';
    if( !empty( $metadonnees ) ) {
        foreach( $metadonnees as $key => $meta ) {
//            if ($meta['typemetadonnee_id'] != 3 || $meta['typemetadonnee_id'] == 3 && $meta['CourrierMetadonnee']['valeur'] != 0) {
                $dlContent .= $this->Html->tag('dt', $meta['name'] );
                if ($meta['typemetadonnee_id'] == 3) {
                    $dlContent .= $this->Html->tag('dd', $this->Html->image('/img/valid_16.png') . ' ' . __d('metadonnee', 'Metadonnee.checked'));
                } else {
                    $dlContent .= $this->Html->tag('dd', (isset( $selects[$meta['CourrierMetadonnee']['valeur']] ) ) ? $selects[$meta['CourrierMetadonnee']['valeur']] : 'Aucune information');
//                    $dlContent .= $this->Html->tag('dd', ($meta['CourrierMetadonnee']['valeur'] != '') ? $meta['CourrierMetadonnee']['valeur'] : 'Aucune information');
                }
//            }
        }
    }
    else {
        echo $this->Html->tag('div', 'Aucune méta-donnée renseignée', array('class' => 'alert alert-warning'));
    }
    $divContent = $this->Html->tag('dl', $dlContent);
    echo $divContent;
    ?>
</dl>
<legend class="panel-title"><?php echo __d('document', 'Document.main_doc'); ?></legend>
<?php
    if (!empty($mainDoc)) {
?>
<div class="ctxdoc">
        <?php
        echo $this->Element('ctxdoc', array(
           'ctxdoc' => $mainDoc['Document'],
           'gfcDocType' => 'Document',
           'cssClass' => 'mainDoc'
           ));
        ?>
</div>
<?php
   } else {
     echo $this->Html->tag('div', __d('document', 'Document.mainDoc.void'), array('class' => 'alert alert-warning'));
   }
?>
<legend class="panel-title"><?php echo __d('document', 'Document.list'); ?></legend>
<?php
    if (!empty($documents)) {
?>
<ul>
        <?php echo $this->Element('documentList', array('documentList' => $documents)); ?>
</ul>
<?php
    } else {
        echo $this->Html->tag('div', __d('courrier', 'Document.void'), array('class' => 'alert alert-warning'));
    }
 ?>
