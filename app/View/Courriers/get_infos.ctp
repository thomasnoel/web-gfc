<?php

/**
 *
 * Courriers/get_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (isset($viewOnly) && $viewOnly === true) {?>
    <?php echo $this->Element('viewOnly');?>
    <script type="text/javascript">
        // 'use strict';

		$('.saveEditBtn').hide();
		$('.undoEditBtn').hide();
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopcurrentId = urlCouper[urlCouper.length - 1];
        var fluxId = "<?php echo $flux['Courrier']['id'];?>";

        $('.detacheFlux').click(function () {
            swal({
                    showCloseButton: true,
                title: 'Détacher un flux',
                text: 'Vous voulez détacher ce flux depuis votre bannette de copie ? ',
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                confirmButtonText: '<i class="fa fa-check" aria-hidden="true"></i> Valider',
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
            }).then(function (data) {
                if (data) {
                    var form = $(this).parents('.modal').find('form');
                    gui.request({
                        url: '/courriers/detache/' + fluxId + '/' + desktopcurrentId,
                        data: form.serialize()
                    }, function (data) {
                        window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>"; // anciennement usernenv/copy
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas détaché,;) .",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
            $('.swal2-cancel').css('border', 'solid 1px');

            $('.swal2-cancel').hover(function () {
                $('.swal2-cancel').css('background-color', '#5397a7');
                $('.swal2-cancel').css('color', 'white');
                $('.swal2-cancel').css('border-color', '#5397a7');
            }, function () {
                $('.swal2-cancel').css('background-color', 'transparent');
                $('.swal2-cancel').css('color', '#5397a7');
                $('.swal2-cancel').css('border-color', '#3C7582');
            });
        });

        $('.envoiCopieFlux').click(function () {
            gui.formMessage({
                title: '<?php echo __d('default', 'Button.send'); ?>',
                width: '600px',
                url: '/courriers/send/<?php echo $flux['Courrier']['id'] . '/1' ; ?>',
                buttons: {
					"<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents(".modal").modal('hide');
                        $(this).parents(".modal").empty();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        var form = $(this).parents('.modal').find('form');
						if( form_validate(form)) {
							gui.request({
								url: '/courriers/send/<?php echo $flux['Courrier']['id'] . '/1'; ?>',
								data: form.serialize()
							}, function (data) {
								if (data.length > 4) { //FIXME avant avec 0 cela marchait
									getJsonResponse(data);
								} else {
									window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
								}
							});
							$(this).parents(".modal").modal('hide');
							$(this).parents(".modal").empty();
						}
                    }
                }
            });
        });

    </script>
   <a href="#" title="<?php echo  __d('default', 'Button.detachcopie'); ?>"  alt="<?php echo  __d('default', 'Button.detachcopie'); ?>" style="float: right; margin-left: 2px;" class="detacheFlux btn btn-info-webgfc btn-secondary"><i class="fa fa-unlock-alt" aria-hidden="true"></i></a>
   <a href="#" title="<?php echo  __d('default', 'Button.envoicopie'); ?>"  alt="<?php echo  __d('default', 'Button.envoicopie'); ?>" style="float: right;" class="envoiCopieFlux btn btn-info-webgfc btn-secondary"><i class="fa fa-paper-plane" aria-hidden="true"></i></a> <br />
    <?php
}

$priority = array(
    0 => array(
        'img' => $this->Html->image('/img/priority_low.png'),
        'txt' => __d('courrier', 'Courrier.priorite_0')
    ),
    1 => array(
        'img' => $this->Html->image('/img/priority_mid.png'),
        'txt' => __d('courrier', 'Courrier.priorite_1')
    ),
    2 => array(
        'img' => $this->Html->image('/img/priority_high.png'),
        'txt' => __d('courrier', 'Courrier.priorite_2')
    )
);

$direction = array(
    0 => array(
        'img' => '<i class="fa fa-sign-out" style="color:#e51809;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_sortant'),
        'contact' => 'dest'
    ),
    1 => array(
        'img' => '<i class="fa fa-sign-in" style="color:#2ec07e;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_entrant'),
        'contact' => 'exp'
    ),
    2 => array(
        'img' => '<i class="fa fa-level-up" style="color:#5397a7;" aria-hidden="true"></i>',
        'txt' => __d('courrier', 'Courrier.courrier_interne'),
        'contact' => 'exp'
    )
);

$tabDelaiUnite = array('jour', 'semaine', 'mois');
?>
<div class="zone col-sm-12">
    <legend id="qualif" ><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Courrier.fluxFieldset'); ?></legend>
    <div  id="qualifdata" class="panel-body fieldsetView">
        <dl>
            <dt><?php echo __d('courrier', 'Courrier.name'); ?></dt>
            <dd><?php echo $flux['Courrier']['name']; ?></dd>
            <dt><?php echo __d('courrier', 'Courrier.origineflux_id'); ?></dt>
            <dd><?php echo $flux['Origineflux']['name']; ?></dd>
            <dt><?php echo __d('courrier', 'Courrier.objet'); ?></dt>
            <dd><?php echo $flux['Courrier']['objet']; ?></dd>
            <dt><?php echo __d('courrier', 'Courrier.priorite'); ?></dt>
            <dd>
                    <?php
                    echo $priority[$flux['Courrier']['priorite']]['img'] . " " . $priority[$flux['Courrier']['priorite']]['txt'];
                    ?>
            </dd>
            <dt><?php echo __d('courrier', 'Courrier.affairesuiviepar_id'); ?></dt>
            <dd><?php echo $flux['Affairesuivie']['name']; ?></dd>
        </dl>
    </div>
</div>

<script type="text/javascript">
    $('#qualif').click(function () {
        $('#qualifdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>

<div class="zone col-sm-12">
    <legend id="acteur"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Qualification'); ?></legend>
    <div id="acteurdata" class="panel-body fieldsetView">
<?php if( Configure::read('Conf.SAERP')) :?>
    <?php if(!empty($flux['Pliconsultatif']["marche_id"]) && !empty($flux['Courrier']["typedocument"])): ?>
        <dl>
            <?php if(!empty($flux['Pliconsultatif']["marche_id"]) ):?>
            <dt ><?php echo __d('courrier', 'Courrier.marche_id'); ?></dt>
            <dd><?php echo $marches[$flux['Courrier']['marche_id']]; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Courrier']["typedocument"]) ):?>
            <dt><?php echo __d('courrier', 'Courrier.typedocument'); ?></dt>
            <dd><?php echo $typesdocument[$flux['Courrier']['typedocument']]; ?></dd>
            <?php endif;?>
        </dl>
    <?php endif;?>
    <?php if($flux['Pliconsultatif']['id']!=null): ?>
        <dl id="pli">
            <?php /*if(!empty($flux['Pliconsultatif']["name"]) ):?>
                <dt ><?php echo __d('pliconsultatif', 'Pliconsultatif.name'); ?></dt>
                <dd><?php echo $flux['Pliconsultatif']['name']; ?></dd>
            <?php endif;*/?>
            <?php if(!empty($flux['Pliconsultatif']["origineflux_id"]) ):?>
            <dt ><?php echo __d('pliconsultatif', 'Pliconsultatif.origineflux_id'); ?></dt>
            <dd><?php echo $origineflux[$flux['Pliconsultatif']['origineflux_id']] ; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Pliconsultatif']["numero"]) ):?>
            <dt ><?php echo __d('pliconsultatif', 'Pliconsultatif.numero'); ?></dt>
            <dd><?php echo $flux['Pliconsultatif']['numero']; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Pliconsultatif']["objet"]) ):?>
            <dt ><?php echo __d('pliconsultatif', 'Pliconsultatif.objet'); ?></dt>
            <dd><?php echo $flux['Pliconsultatif']['objet'] ; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Pliconsultatif']["date"]) ):?>
            <dt ><?php echo __d('pliconsultatif', 'Pliconsultatif.date'); ?></dt>
            <dd><?php echo $this->Html2->ukToFrenchDateWithSlashes($flux['Pliconsultatif']['date']).' à '.$flux['Pliconsultatif']['heure']; ?></dd>
            <?php endif;?>
        </dl>
    <?php endif; ?>
    <?php if($flux['Ordreservice']['id']!=null): ?>
        <dl id="os">
            <?php if(!empty($flux['Ordreservice']["name"]) ):?>
            <dt ><?php echo __d('ordreservice', 'Ordreservice.name'); ?></dt>
            <dd><?php echo $flux['Ordreservice']['name']; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Ordreservice']["numero"]) ):?>
            <dt ><?php echo __d('ordreservice', 'Ordreservice.numero'); ?></dt>
            <dd><?php echo $flux['Ordreservice']['numero']; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Ordreservice']["objet"]) ):?>
            <dt ><?php echo __d('ordreservice', 'Ordreservice.objet'); ?></dt>
            <dd><?php echo $flux['Ordreservice']['objet'] ; ?></dd>
            <?php endif;?>
            <?php if(!empty($flux['Ordreservice']["montant"]) ):?>
            <dt ><?php echo __d('ordreservice', 'Ordreservice.montant'); ?></dt>
            <dd><?php echo $flux['Ordreservice']['montant'] ; ?></dd>
            <?php endif;?>
        </dl>
    <?php endif; ?>
<?php endif;?>
        <dl>
            <dt><?php echo __d('type', 'Type'); ?></dt>
            <dd><?php echo (!empty($flux['Soustype']["type_id"]) ? $types[$flux['Soustype']['type_id']] : ""); ?></dd>
            <dt><?php echo __d('soustype', 'Soustype'); ?></dt>
            <dd><?php echo $flux['Soustype']['name']; ?></dd>
        </dl>

        <dl>
            <dt><?php echo __d('courrier', 'Courrier.reference'); ?></dt>
            <dd><?php echo $flux['Courrier']['reference']; ?></dd>
            <dt><?php echo __d('courrier', 'Courrier.entrant/sortant'); ?></dt>
            <dd>
                    <?php echo (isset($flux["Soustype"]['entrant']) ? $direction[$flux['Soustype']['entrant']]['img'] . " " . $direction[$flux['Soustype']['entrant']]['txt'] : ''); ?>
            </dd>
            <dt><?php echo __d('soustype', 'Soustype.delai'); ?></dt>
            <dd><?php echo (isset($flux['Courrier']['delai_nb']) && isset($flux['Courrier']['delai_unite'])) ? $flux['Courrier']['delai_nb'] . " " . $tabDelaiUnite[$flux['Courrier']['delai_unite']] . ((@$flux['Courrier']['delai_unite'] != 2 && $flux['Courrier']['delai_nb'] > 1) ? 's' : '' ) : @$flux['Soustype']['delai_nb'] . " " . @$tabDelaiUnite[$flux['Soustype']['delai_unite']] . ((@$flux['Soustype']['delai_unite'] != 2 && @$flux['Soustype']['delai_nb'] > 1) ? 's' : '' ); ?></dd>
        </dl>
    </div>
</div>

<script type="text/javascript">
    $('#acteur').click(function () {
        $('#acteurdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>
<?php if (Configure::read('Affiche.Cycledevie')):?>
    <div class="zone col-sm-12">
        <legend id="cycle"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Acteurs'); ?></legend>
        <div id="cycledata" class="panel-body form-horizontal fieldsetView">
            <table  id="acteursTable" data-toggle="table"  data-locale = "fr-CA" data-show-columns="true">
                <thead>
                <th><i class="fa fa-desktop"  style="color:grey;" aria-hidden="true" title="Nom du profil ayant traité le flux"></i></th>
                <th><i class="fa fa-user"  style="color:grey;" aria-hidden="true" title="Nom de l'agent traitant le flux"></i></th>
				<th><i class="fa fa-users" style="color:grey;" aria-hidden="true" title="Utilisateur ayant réalisé l'action"></i></th>
				<th><i class="fa fa-id-card-o"  style="color:grey;" aria-hidden="true" title="Profil de l'agent ayant traité le flux"></i></th>
                <th><i class="fa fa-calendar-check-o"  style="color:grey;" aria-hidden="true" title="Date d'arrivée chez l'agent"></i></th>
                <th><i class="fa fa-calendar-check-o"  style="color:grey;" aria-hidden="true" title="Date de dernière modification"></i></th>
                <th><i class="fa fa-calendar-check-o"  style="color:grey;" aria-hidden="true" title="Date de validation"></i></th>
                <th><i class="fa fa-files-o"  style="color:grey;" aria-hidden="true"  title="Pour copie ?"></i></th>
                </thead>
                <tbody>
                <?php foreach( $bannettes as $key => $bannette ):?>
                    <?php
                    // Affichage de l'icône pour copie ou non
                        $bannetteName = $bannette['Bannette']['name'];
                        // Utilisateur ayant réalisé l'action
                        $user = @$bannette['Desktop']['User'][0]['nom'].' '.@$bannette['Desktop']['User'][0]['prenom'];
                        if( $user == ' ' ) {
                            $user = @$bannette['Desktop']['SecondaryUser'][0]['nom'].' '.@$bannette['Desktop']['SecondaryUser'][0]['prenom'];
                        }
						if(isset($bannette['Bancontenu']['parapheurname']) && !empty($bannette['Bancontenu']['parapheurname'])) {
							$user = $bannette['Bancontenu']['parapheurname'];
						}

                        $gras = @$bannette['Bancontenu']['gras'];
                        if($gras) {
                            $val = 'bold';
                        }
                        else {
                            $val = 'thin';
                        }
						$lineColored = '';
						if( empty($bannette['Bancontenu']['bannette_id'])) {
							$lineColored = 'Colored';
						}
						$userValideur = $bannette['Bancontenuuser']['nom']. ' '.$bannette['Bancontenuuser']['prenom'];
						if( !empty( $bannette['Desktop']['User'] )) {
							if ($bannette['Desktop']['User'][0]['username'] != $bannette['Bancontenuuser']['username']) {
								$userValideur = $bannette['Bancontenuuser']['nom'] . ' ' . $bannette['Bancontenuuser']['prenom'] . nl2br("\n (par délégation)");
							}
						}

						$rejet ='';
						if( empty($bannette['Bancontenu']['bannette_id'] ) ){
							$rejet = 'Refus du flux par '. $bannette['Desktop']['name'];
						}

						if(isset($bannette['Bancontenu']['pastellname']) && !empty($bannette['Bancontenu']['pastellname'])) {
							$user = $bannette['Bancontenu']['pastellname'];
						}
					?>
					<tr class="trCycle<?php echo $val.$lineColored;?>" >
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo $bannette['Desktop']['name'];?></td>
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo $user;?></td>
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo isset($bannette['Bancontenuuser']['username']) ? $userValideur : null;?></td>
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo $bannette['Desktop']['Profil']['name'];?></td>
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['created'] ) );?></td>
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo !empty($bannette['Bancontenu']['modified']) ? strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['modified'] ) ) : '';?></td>
						<td class="tdDate" title="<?php echo $rejet;?>"><?php echo !empty($bannette['Bancontenu']['validated']) ? strftime( '%d/%m/%Y %T', strtotime( $bannette['Bancontenu']['validated'] ) ) : '';?></td>
						<td class="tdState"><?php echo ($bannetteName == 'copy' ) ? $this->Html->image('/img/valid_16.png', array('alt' => 'Pour copie', 'title' => 'Flux envoyé pour copie' ) ) : null;?></td>
					</tr>
                            <?php endforeach;?>
                </tbody>
            </table>
            <div style="margin-left: 15px;"><?php echo '<b>* Les lignes en gras représentent les agents possédant le flux actuellement.</b>';?></div>
        </div>
    </div>
<?php endif;?>
<?php
    $contactInfos = "";
    if( !empty( $flux['Organisme']['name']) && empty($flux['Contact']["name"]) ) {
      $contactInfos = $flux['Organisme']["name"];
    }
    if( !empty( $flux['Organisme']['name']) && !empty($flux['Contact']["name"]) ) {
        $contactInfos = $flux['Organisme']["name"]. ' / '.$flux['Contact']["name"];
    }
    if( empty( $flux['Organisme']['name']) && !empty($flux['Contact']["name"]) ) {
        $contactInfos = $flux['Contact']["name"];
    }
    asort($civilite);
?>

<script type="text/javascript">
    $('#cycle').click(function () {
        $('#cycledata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>

<div class="zone col-sm-12">
	<legend id="organisme"><i class="fa fa-minus" aria-hidden="true"></i> Informations de l'organisme</legend>
	<div id="organismedata" class="panel-body fieldsetView">
		<?php if(!empty($flux['Organisme']['id']) && $flux['Organisme']['id'] != Configure::read('Sansorganisme.id')  ):?>
			<dl class="col-sm-5">
				<dt class="strong">Nom de l'organisme</dt>
				<dd class="tdField"><?php echo $flux['Organisme']['name'];?></dd>

				<dt class="strong"><?php echo __d('organisme', 'Organisme.numvoie');?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['numvoie'];?></dd>

				<dt class="strong"><?php echo __d('organisme', 'Organisme.compl');?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['compl'];?></dd>

				<dt class="strong"><?php echo __d('organisme', 'Organisme.cp');?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['cp'];?></dd>

				<dt class="strong"><?php echo __d('Organisme', 'Organisme.email');?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['email'];?></dd>


			</dl>
			<dl class="col-sm-5">
				<dt class="strong"><?php echo 'Adresse complète';?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['adressecomplete'];?></dd>


				<dt class="strong"><?php echo __d('organisme', 'Organisme.nomvoie');?></dt>
				<dd class="tdField"><?php echo !empty($flux['Organisme']['banadresse']) ? $flux['Organisme']['banadresse'] : $flux['Organisme']['nomvoie'];?></dd>


				<dt class="strong"><?php echo __d('Organisme', 'Organisme.tel');?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['tel'];?></dd>

				<dt class="strong"><?php echo __d('organisme', 'Organisme.ville');?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['ville'];?></dd>

				<dt class="strong"><?php echo 'Pays';?></dt>
				<dd class="tdField"><?php echo $flux['Organisme']['pays'];?></dd>

			</dl>
		<?php else:?>
			<dl class="col-sm-5">
				<dt class="strong">Nom de l'organisme</dt>
				<dd class="tdField"><?php echo $flux['Organisme']['name'];?></dd>
			</dl>
		<?php endif;?>
	</div>
</div>

<div class="zone col-sm-12">
    <legend id="contact"><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Contactinfo'); ?></legend>
    <div id="contactdata" class="panel-body fieldsetView">
		<?php if(!empty($flux['Contact']['id']) && $flux['Contact']['id'] != Configure::read('Sanscontact.id')  ):?>
            <dl class="col-sm-5">
                <dt class="strong">Nom / Prénom</dt>
                <dd class="tdField"><?php echo ( isset( $civilite[$flux['Contact']['civilite']] ) ? $civilite[$flux['Contact']['civilite']] : null  ).' '.$flux['Contact']['nom'].' '.$flux['Contact']['prenom'];?></dd>

                <dt class="strong"><?php echo __d('contact', 'Contact.numvoie');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['numvoie'];?></dd>

                <dt class="strong"><?php echo __d('contact', 'Contact.nomvoie');?></dt>
                <dd class="tdField"><?php echo !empty($flux['Contact']['banadresse']) ? $flux['Contact']['banadresse'] : $flux['Contact']['nomvoie'];?></dd>

                <dt class="strong"><?php echo __d('contact', 'Contact.compl');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['compl'];?></dd>
                <dt class="strong"><?php echo __d('contact', 'Contact.cp');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['cp'];?></dd>
                <dt class="strong"><?php echo __d('contact', 'Contact.ville');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['ville'];?></dd>
            </dl>
            <dl class="col-sm-5">
                <dt class="strong"><?php echo __d('contact', 'Contact.name');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['name'];?></dd>

                <dt class="strong"><?php echo 'Adresse complète';?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['adressecomplete'];?></dd>

                <dt class="strong"><?php echo 'Pays';?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['pays'];?></dd>


                <dt class="strong"><?php echo __d('contact', 'Contact.tel');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['tel'];?></dd>
                <dt class="strong"><?php echo __d('contact', 'Contact.email');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['email'];?></dd>

                <dt class="strong"><?php echo __d('contact', 'Contact.role');?></dt>
                <dd class="tdField"><?php echo $flux['Contact']['role'];?></dd>

            </dl>
        <?php else:?>
            <?php echo $this->Html->tag('div', __d('addressbook', 'Contactinfo.void'), array('class' => 'alert alert-warning'));?>
        <?php endif;?>
    </div>
</div>

<script type="text/javascript">
    $('#contact').click(function () {
        $('#contactdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>

<div class="zone col-sm-12">
    <legend id="dates" ><i class="fa fa-minus" aria-hidden="true"></i> <?php echo __d('courrier', 'Courrier.dateFieldset'); ?></legend>
    <div id="datesdata" class="panel-body fieldsetView">
        <dl>
            <dt><?php echo __d('courrier', 'Courrier.datereception'); ?></dt><dd><?php echo $this->Html2->ukToFrenchDateWithSlashes($flux['Courrier']['datereception']); ?></dd>
            <dt><?php echo __d('courrier', 'Courrier.date'); ?></dt><dd><?php echo $this->Html2->ukToFrenchDateWithSlashes($flux['Courrier']['date']); ?></dd>
        </dl>
    </div>
</div>

<script type="text/javascript">
    $('#dates').click(function () {
        $('#datesdata').toggle();
        var btn = $(this).find('i');
        if( btn.hasClass('fa-minus') ) {
            btn.removeClass('fa-minus').addClass('fa-plus');
        }
        else if( btn.hasClass('fa-plus') ) {
            btn.removeClass('fa-plus').addClass('fa-minus');
        }
    });
</script>


<script type="text/javascript">
    $("#acteursTable").bootstrapTable({});
    var jsContactinfo = $.parseJSON('<?php echo json_encode($contactinfosTreated, JSON_HEX_APOS); ?>');
    if (jsContactinfo != '') {
        $('#contactData').append(createContactinfo(jsContactinfo['Contactinfo']).css({'display': 'table', 'width': '100%', 'margin': '10px 0 10px -12px'}));
    }

    var jsOrganismeinfo = $.parseJSON('<?php echo json_encode($organismeTreated, JSON_HEX_APOS); ?>');
    if (jsOrganismeinfo != '') {
        $('#organismeData').append(createOrganismeinfo(jsOrganismeinfo['Organisme']).css({'display': 'table', 'width': '100%', 'margin': '10px 0 10px -12px'}));
    }

    $('.trCyclebold').css('font-weight', 'bold') ;
	$('.trCyclethinColored').css('font-style', 'italic') ;
	$('.trCyclethinColored').css('color', '#b11526') ;
	$('.trCycleboldColored').css('font-style', 'italic') ;
	$('.trCycleboldColored').css('color', '#b11526') ;

    $('.organismeInfoCard dd' ).css('margin', '10px 0');
    $('.contactInfoCard dd' ).css('margin', '10px 0');
</script>
