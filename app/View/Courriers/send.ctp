<?php

/**
 *
 * Courriers/send.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    function setCopyMode() {
        $('#SendGetback').attr('disabled', 'disabled');
        $('label', $('#SendGetback').parent()).css('color', '#c0c0c0');

        $('#SendDesktopId').parent().css('display', 'none');
        $('#SendDesktopId').parent().parent().css('display', 'none');

        $('#SendCopyId').parent().css('display', 'block');
        $('#SendCopyId').parent().parent().css('display', 'block');

        $('#SendGetback').parent().css('display', 'none');
        $('#SendGetback').parent().parent().css('display', 'none');
        $('#SendCopy').parent().css('display', 'block');

		$('#SendCopyId').prop('required', true );
    }

    function setNormalMode() {
		$('#SendGetback').removeAttr('disabled');
		$('label', $('#SendGetback').parent()).css('color', '#fff');
		$('#SendGetback').parent().css('display', 'block');
		$('#SendGetback').parent().parent().css('display', 'block');

		$('#SendDesktopId').parent().css('display', 'block');
		$('#SendDesktopId').parent().parent().css('display', 'block');
		$('#SendDesktopId').prop('required', true );

        $('#SendCopyId').parent().css('display', 'none');
        $('#SendCopyId').parent().parent().css('display', 'none');

        $('#SendCopy').parent().css('display', 'block');
        $('#SendCopy').parent().parent().css('display', 'block');

    }

    function setEnvoiMode() {
        $('#SendGetback').removeAttr('disabled');
        $('label', $('#SendGetback').parent()).css('color', '#000');
        $('#SendDesktopId').parent().css('display', 'block');
		$('#SendDesktopId').prop('required', true );

        $('#SendGetback').parent().css('display', 'block');
        $('#SendCopyId').parent().css('display', 'none');
        $('#SendCopy').parent().css('display', 'none');
        $('#SendCopy').parent().parent().css('display', 'none');
    }
</script>

<?php
$formSend = array(
    'name' => 'Send',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Send.flux_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value' => $fluxId
            )
        ),
        'Send.desktop_id' => array(
            'labelText' =>__d('courrier', 'Courrier.desktop_to_send'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => @$desktopsToSend,
                'empty' => true
            )
        ),
        'Send.getback' => array(
            'labelText' =>__d('courrier', 'Courrier.getback'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox'
            )
        ),
        'Send.copy' => array(
            'labelText' =>__d('courrier', 'Courrier.copy'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox'
            )
        ),
        'Send.copy_id' => array(
            'labelText' =>__d('courrier', 'Courrier.desktop_to_send_to'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'class'=>'copy',
//                'multiple' => true,
                'options' => @$allDesktops,
                'empty' => true
            )
        ),
        'Comment.objet' => array(
            'labelText' =>__d('comment', 'Comment.objet'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        )
    )
);
    if( !Configure::read('DOC_FROM_GFC') ){
        $formSend['input']['Send.signature_manuelle'] = array(
            'labelText' =>__d('courrier', 'Courrier.signature_manuelle'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox'
            )
        );
    }
    if( isset($typeParapheur) && !empty($typeParapheur) ) {
        $formSend['input']['Send.signature'] = array(
            'labelText' =>__d('courrier', 'Courrier.signature'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $typeParapheur,
                'empty' => true,
                'required' => true
            )
        );
    }

if( !empty($hasPastellActif) && isset($typePastell) && !empty($typePastell) ) {
	$formSend['input']['Send.pastell_type_document'] = array(
		'labelText' => 'Cheminement attendu',
		'inputType' => 'select',
		'items'=>array(
			'type'=>'select',
			'options' => $typePastell,
			'empty' => true,
			'required' => true
		)
	);

	$formSend['input']['Send.pastell_inforequired_type_document'] = array(
			'labelText' => 'Liste des sous-types de visa/signature',
			'inputType' => 'select',
			'items'=>array(
				'type'=>'select',
				'options' => $listSoustypeIp,
				'empty' => true,
				'required' => true
			)
	);
}
echo $this->Formulaire->createForm($formSend);
?>

<?php if( !empty($hasPastellActif) ) :?>
	<div class="form-group "  id="SendPastellDocIsNotValid">
		<label for="SendPastellInforequiredTypeDocument" class="control-label col-sm-4">Liste des sous-types pour visa/signature *</label>
		<div class=" controls-input col-sm-6 ">
			<b style='color:red;'>Le document principal n'est pas valide<br /> Le flux ne peut être adressé au i-Parapheur</b>
		</div>
		<br />
		<br />
	</div>

	<?php if( !empty($this->request->data['Send']['pastell_inforequired_type_document']) ):?>
		<div class="form-group "  id="SoustypeIpInSoustype">
			<label for="SendPastellInforequiredTypeDocument" class="control-label col-sm-4">Liste des sous-types pour visa/signature *</label>
			<div class=" controls-input col-sm-6 " style="margin-top:8px;">
				<?php echo $this->request->data['Send']['pastell_inforequired_type_document'];?>
			</div>
			<br />
			<br />
		</div>
	<?php endif;?>

	<div class="form-group "  id="SendPastellContactEmail">
		<?php
			$formContactEmail = array(
				'name' => 'Send',
				'label_w' => 'col-sm-4',
				'input_w' => 'col-sm-6',
				'input' => array(
					'Send.email' => array(
						'labelText' => 'E-mail du contact pour le mail sécurisé',
						'inputType' => 'text',
						'items'=>array(
							'type'=>'text',
							'required' => true,
							'value' => $courrier['Contact']['email']
						)
					),
				)
			);
		echo $this->Formulaire->createForm($formContactEmail);
		?>
	</div>
<?php endif;?>
<?php
echo $this->Form->end();
?>
<script type="text/javascript">
    $('#SendCopy').change(function () {
        if ($(this).prop("checked")) {
            $('#SendCopyId').select2();
            setCopyMode();
        }
        else {
            setNormalMode();
        }
    });


<?php if (($sendcpy && !$sendwkf) || $cpyOnly) { ?>
    setCopyMode();
    $('#SendCopy').prop('checked', true);
    $('#SendCopy').css('display', 'none');
    $('#SendCopy').after($('<input  />').attr('type', 'checkbox').prop('checked', true).attr('disabled', 'disabled'));
<?php } else if ($sendwkf && !$sendcpy) { ?>
    setNormalMode();
    $('#SendCopy').prop('checked', false).attr('disabled', 'disabled');
    $('label', $('#SendCopy').parent()).css('color', '#c0c0c0');
    $('#SendCopy').parents('.form-group').css('display', 'none');
<?php } else if ($sendwkf && $sendcpy) { ?>
    setNormalMode();
<?php } ?>

    $('#SendSendForm select.copy').each(function () {
        setJselectNew($(this));
    });

    $('#SendDesktopId').select2({allowClear: true, placeholder: "Sélectionner un destinataire"});

    $('#SendCopyId').select2({allowClear: true, placeholder: "Sélectionner un destinataire"});


    <?php if( !Configure::read('DOC_FROM_GFC') ):?>
    $('#SendSignatureManuelle').change(function () {
        if ($(this).prop("checked")) {
            $('#SendGetback').removeAttr('disabled');
            $('label', $('#SendGetback').parent()).css('color', '#000');
            $('#SendDesktopId').parents('.from-group').css('display', 'none');
            $('#SendGetback').parents('.from-group').css('display', 'none');

            $('#SendCopyId').parents('.from-group').css('display', 'none');
            $('#SendCopy').parents('.from-group').css('display', 'none');

            $('#CommentContent').parent().css('display', 'none');
        }
    });
    <?php endif;?>


    <?php if( isset($typeParapheur) && !empty($typeParapheur) ) :?>
        $('#SendSignature').parent().css('display', 'none');
        $('#SendSignature').parent().parent().css('display', 'none');
        $('#SendDesktopId').change( function() {
            if($('#SendDesktopId').val() == '-1' ) {
                $('#SendSignature').parent().css('display', 'block');
                $('#SendSignature').parent().parent().css('display', 'block');
                $('#SendSignature').select2();
            }
            else {
                $('#SendSignature').parent().css('display', 'none');
                $('#SendSignature').parent().parent().css('display', 'none');
            }
        });
    <?php endif;?>

	<?php if($endCircuit || $getback) :?>
		$('#SendDesktopId').change( function() {
			if($('#SendDesktopId').val() == '-1'  || $('#SendDesktopId').val() == '-3' ) {
				$('#SendGetback').prop('checked', true);
				$('#SendGetback').attr('disabled', 'disabled');
			}
			else {
				$('#SendGetback').prop('checked', false);
				$('#SendGetback').removeAttr('disabled');
			}

			$('#SendCopy').attr('disabled', 'disabled');
			if($('#SendDesktopId').val() == undefined || $('#SendDesktopId').val() == '' ) {
				$('#SendCopy').removeAttr('disabled');
			}
		});
	<?php endif;?>


	<?php if( !empty($hasPastellActif) && isset($typePastell) && !empty($typePastell) ) :?>
		$('#SendPastellTypeDocument').parent().css('display', 'none');
		$('#SendPastellTypeDocument').parent().parent().css('display', 'none');
		$('#SendPastellInforequiredTypeDocument').parent().css('display', 'none');
		$('#SendPastellInforequiredTypeDocument').parent().parent().css('display', 'none');
		$('#SendPastellDocIsNotValid').hide();
		$('#SendPastellContactEmail').hide();
		$('#SoustypeIpInSoustype').hide();

		$('#SendDesktopId').change( function() {
			if($('#SendDesktopId').val() == '-3' ) {
				$('#SendPastellTypeDocument').parent().css('display', 'block');
				$('#SendPastellTypeDocument').parent().parent().css('display', 'block');
				$('#SendPastellTypeDocument').select2();
			}
			else {
				$('#SendPastellTypeDocument').parent().css('display', 'none');
				$('#SendPastellTypeDocument').parent().parent().css('display', 'none');
				$('#SendPastellInforequiredTypeDocument').parent().css('display', 'none');
				$('#SendPastellInforequiredTypeDocument').parent().parent().css('display', 'none');
			}
		});

		$('#SendPastellTypeDocument').change( function() {

			if ($('#SendPastellTypeDocument').val() == 'document-a-signer'
				|| $('#SendPastellTypeDocument').val() == 'pdf-generique'
				|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>'
				|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-visa'
				|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-mailsec'
				|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-ged'
			) {
				if ($('#SendPastellTypeDocument').val() == 'document-a-signer'
					|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-visa'
				) {
					$('#SendPastellContactEmail').hide();
				}

				if ($('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>'
					|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-mailsec'
					|| $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-ged'
				) {
					if( '<?php echo $courrier['Soustype']['envoi_mailsec'];?>' == '' ) {
						$('#SendPastellContactEmail').hide();
					}
				}
				<?php if( $docIsValid  ) :?>
					$('#SendPastellInforequiredTypeDocument').parent().css('display', 'block');
					$('#SendPastellInforequiredTypeDocument').parent().parent().css('display', 'block');
					$('#SendPastellInforequiredTypeDocument').select2();


					if ($('#SendPastellTypeDocument').val() != 'document-a-signer'
						|| $('#SendPastellTypeDocument').val() != '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-visa'
					) {
						$('#SendPastellContactEmail').show();
					}

					if ( $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-visa'
					) {
						if( '<?php echo $courrier['Soustype']['envoi_mailsec'];?>' == '' ) {
							$('#SendPastellContactEmail').hide();
						}
					}

					<?php if( !empty($this->request->data['Send']['pastell_inforequired_type_document']) ) :?>
						$('#SendPastellInforequiredTypeDocument').parent().css('display', 'none');
						$('#SendPastellInforequiredTypeDocument').parent().parent().css('display', 'none');
						$('#SoustypeIpInSoustype').show();
					<?php endif;?>

					if ($('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>'  ) {
						if( '<?php echo $courrier['Soustype']['envoi_mailsec'];?>' == '' && '<?php echo $hasCheminement;?>') {
							$('#SendPastellContactEmail').hide();
						}
						if( '<?php echo $courrier['Soustype']['envoi_signature'];?>' == '' && '<?php echo $hasCheminement;?>') {
							$('#SoustypeIpInSoustype').hide();
						}
					}

			<?php else :?>
					$('#SendPastellDocIsNotValid').show();
					$('#SoustypeIpInSoustype').show();
					if ($('#SendPastellTypeDocument').val() != 'document-a-signer'
					|| $('#SendPastellTypeDocument').val() != '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-visa'
					) {
						$('#SendPastellContactEmail').show();
					}

					if ( $('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>-visa'
					) {
						if( '<?php echo $courrier['Soustype']['envoi_mailsec'];?>' == '' ) {
							$('#SendPastellContactEmail').hide();
						}
					}

					<?php if( !empty($this->request->data['Send']['pastell_inforequired_type_document']) ) :?>
						$('#SendPastellInforequiredTypeDocument').parent().css('display', 'none');
						$('#SendPastellInforequiredTypeDocument').parent().parent().css('display', 'none');
						$('#SoustypeIpInSoustype').show();
					<?php endif;?>

					if ($('#SendPastellTypeDocument').val() == '<?php echo Configure::read('Pastell.fluxStudioName'); ?>'  ) {
						if( '<?php echo $courrier['Soustype']['envoi_mailsec'];?>' == '' && '<?php echo $hasCheminement;?>') {
							$('#SendPastellContactEmail').hide();
						}
						if( '<?php echo $courrier['Soustype']['envoi_signature'];?>' == '' && '<?php echo $hasCheminement;?>') {
							$('#SoustypeIpInSoustype').hide();
						}
					}
				<?php endif;?>
			} else {
				$('#SendPastellDocIsNotValid').hide();
				$('#SendPastellContactEmail').hide();
				$('#SoustypeIpInSoustype').hide();
			}
		});
	<?php endif;?>
</script>
