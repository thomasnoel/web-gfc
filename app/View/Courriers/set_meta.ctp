<?php

/**
 *
 * Courriers/set_meta.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    var styles = {
        fontWeight: 'normal'
    };
</script>
<?php

    if( !empty($metas )){
        echo $this->Html->tag('div', __d('courrier', 'Courrier.metadonneeobligatoire'), array('class' => 'alert alert-warning'));
    }else {
        echo $this->Html->tag('div', __d('metadonnee', 'Metadonnee.void'), array('class' => 'alert alert-warning'));
    }
    $formCourrierMetadonnee = array(
        'name' => 'CourrierMetadonnee',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-7',
        'form_url' => array('controller' => 'courriers', 'action' => 'setMeta', $this->request->params['pass'][0]),
        'input' => array(
            'Courrier.id'=>array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' => $fluxId
                )
            )
        )
    );
    foreach ($metas as $i => $meta) {
        if (!empty($meta['CourrierMetadonnee'])) {
            $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.id"]=array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' => $meta['CourrierMetadonnee']['id']
                )
            );
        }
        $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.courrier_id"]=array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' =>  $this->request->params['pass'][0]
                )
            );
         $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.metadonnee_id"]=array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' =>  $meta['Metadonnee']['id']
                )
            );
         $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.obligatoire"]=array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' => @$meta['Metadonnee']['obligatoire']
                )
            );
        $required = ( @$meta['Metadonnee']['obligatoire'] == true ) ? true : false;

        // SI on est dans le cas de l'ajout, pas de champ obligatoire
        if( strpos( $_SERVER['HTTP_REFERER'], 'add') != false ) {
			$required = false;
		}
        $metaLabel = (isset($meta['Metadonnee']['obligatoire']) && $meta['Metadonnee']['obligatoire']) ? $meta['Metadonnee']['name'].'' : $meta['Metadonnee']['name'];

        // Ajout de la valeur Select dans le type de métadonnée
        $valOfSelect =  Configure::read('Selectvaluemetadonnee.id');
        if ($meta['Metadonnee']['typemetadonnee_id'] == 1) {//date
            $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.valeur.show"]=array(
                'labelText' =>$metaLabel,
                'inputType' => 'text',
                'dateInput' =>true,
                'items'=>array(
                    'type' => 'text',
//                    'readonly' => true,
                    'class' => 'datepicker',
                    'data-format'=>'dd/MM/yyyy',
                    'required'=>$required,
                    'value' =>!empty($meta['CourrierMetadonnee']['valeur']) ? ($meta['CourrierMetadonnee']['valeur']) : null,
                )
            );
            $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.valeur"]=array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' =>!empty($meta['CourrierMetadonnee']['valeur']) ? ($meta['CourrierMetadonnee']['valeur']) : null
                )
            );
        }else if ($meta['Metadonnee']['typemetadonnee_id'] == 3) {//boolean
            $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.valeur"]=array(
                'labelText' =>$metaLabel,
                'inputType' => 'radio',
                'items'=>array(
                    'legend' => false,
//                    'separator'=> '</div><div  class="radioLabel">',
                    'before' => '<div class="radioLabel">',
                    'after' => '</div>',
                    'label' => true,
                    'type'=>'radio',
                    'options' => array('Oui' => 'Oui', 'Non' => 'Non' ),
                    'value' => isset($meta['CourrierMetadonnee']['valeur']) ? $meta['CourrierMetadonnee']['valeur'] : null
                )
            );
        }else if ($meta['Metadonnee']['typemetadonnee_id'] == $valOfSelect ) {//select
            $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.valeur"]=array(
                'labelText' => $metaLabel,
                'inputType' => 'select',
                'items'=>array(
                    'type' => 'select',
                    'empty' => true,
                    'selected' => !empty($meta['CourrierMetadonnee']['valeur']) ? ($meta['CourrierMetadonnee']['valeur']) : null,
                    'required' =>$required,
                    'options' =>!empty( $meta['Selectvaluemetadonnee'] ) ? $meta['Selectvaluemetadonnee'] : null
                )
            );
        }else{
             $formCourrierMetadonnee['input']["CourrierMetadonnee.{$i}.valeur"]=array(
                'labelText' => $metaLabel,
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text',
                    'required' => $required,
                    'value' =>!empty($meta['CourrierMetadonnee']['valeur']) ? ($meta['CourrierMetadonnee']['valeur']) : null,
                )
            );
        }
?>
<script type="text/javascript">
    var styles = {
        fontWeight: 'bold'
    };
    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });
    });
</script>
<?php
    }
    echo $this->Formulaire->createForm($formCourrierMetadonnee);
    echo $this->Form->end();
?>
<div class="alert alert-info col-sm-11" id="autoSaveMeta" >
</div>
<script>

    <?php foreach ($metas as $i => $meta) :?>
      <?php if( in_array( $meta['Metadonnee']['typemetadonnee_id'], array( $valOfSelect , 3) ) ) :?>
            $("#CourrierMetadonnee<?php echo $i;?>Valeur").select2({allowClear: true});
        <?php endif;?>
        $('#CourrierMetadonnee<?php echo $i;?>ValeurShow').change(function () {
            $('#CourrierMetadonnee<?php echo $i;?>Valeur').val($('#CourrierMetadonnee<?php echo $i;?>ValeurShow').val());
        });

        <?php if( $meta['Metadonnee']['typemetadonnee_id'] == 3 ) :?>
            $("#CourrierMetadonnee<?php echo $i;?>ValeurOui").parent().children('label').css( 'cursor', 'pointer' );
            $("#CourrierMetadonnee<?php echo $i;?>ValeurNon").parent().children('label').css( 'cursor', 'pointer' );
        <?php endif;?>


		<?php if( $meta['Metadonnee']['typemetadonnee_id'] == 1 ) :?>
			$("#CourrierMetadonnee<?php echo $i;?>ValeurShow").removeAttr('readonly');
		<?php endif;?>

    <?php endforeach;?>
    $(document).ready(function () {
        $('#autoSaveMeta').hide();
        var timeoutId;
        $('#CourrierMetadonneeSetMetaForm input,#CourrierMetadonneeSetMetaForm  select,#CourrierMetadonneeSetMetaForm  textarea').on('input propertychange change', function () {
            clearTimeout(timeoutId);
            timeoutId = setTimeout(function () {
                // Runs 5 second (5000 ms) after the last change
            // Si on n'est pas dans le cas de la crétion d'un flux, on permet la sauvegarde automatique pour les méta-données
            var str = window.location.href;
            if (str.indexOf('add') == -1) {
				saveAuto();
            }
		}, <?php echo Configure::read('Saveauto.UpdateTimeout') ;?> );
        });

    });

    function saveAuto() {
        var url = $('#CourrierMetadonneeSetMetaForm').attr('action');
        gui.request({
            url: url,
            data: $('#CourrierMetadonneeSetMetaForm').serialize()
        }, function (data) {
            var d = new Date();
            $('#autoSaveMeta').html('Dernier enregistrement : ' + d.toLocaleTimeString());
            $('#autoSaveMeta').show();

			if( $('#flowControlsBis > a ').children('.fa-check').parent().hasClass('ui-state-disabled') ) {
				$('#flowControlsBis > a ').children('.fa-check').parent().removeClass('ui-state-disabled');
				$('#flowControls > a ').children('.fa-check').parent().removeClass('ui-state-disabled');
			}
        });
    }


	$('.saveEditBtn').show();
	$('.undoEditBtn').show();
</script>
