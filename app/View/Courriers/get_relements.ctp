<?php

/**
 *
 * Courriers/get_relements.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Form->create('ResponseDoc');
echo $this->Form->input('courrier_id', array('type' => 'hidden', 'value' => $fluxId));
?>

<?php if(Configure::read('Conf.Gabarit')):?>
<div class="rmodel_list">
    <fieldset><legend><?php echo __d('relement', 'Relement.Rmodels'); ?></legend>
            <?php
            if (!empty($rmodels)) {
                foreach ($rmodels as $rmodel) {
                    ?><input type="radio" name="data[ResponseDoc][gabarit_id]" value="<?php echo $rmodel['Gabaritdocument']['id']; ?>" /><?php
            echo $this->Element('ctxdoc', array('ctxdoc' => $rmodel['Gabaritdocument'], 'gfcDocType' => 'Gabaritdocument', 'cssClass' => 'ctxdoc'));
        }
    } else {
        echo $this->Html->tag('div', __d('courrier', 'Rmodel.void'), array('class' => 'alert alert-warning'));
    }
            ?>
    </fieldset>
</div>
<?php else:?>
<div class="rmodel_list">
    <fieldset><legend><?php echo __d('relement', 'Relement.Rmodels'); ?></legend>
            <?php
            if (!empty($rmodels)) {
                foreach ($rmodels as $rmodel) {
                    ?><input type="radio" name="data[ResponseDoc][rmodel_id]" value="<?php echo $rmodel['Rmodel']['id']; ?>" /><?php
            echo $this->Element('ctxdoc', array('ctxdoc' => $rmodel['Rmodel'], 'gfcDocType' => 'Rmodel', 'cssClass' => 'ctxdoc'));
        }
    } else {
        echo $this->Html->tag('div', __d('courrier', 'Rmodel.void'), array('class' => 'alert alert-warning'));
    }
            ?>
    </fieldset>
</div>
<?php endif;?>

<?php if( Configure::read('Affichage.Webdav') ) :?>
    <?php if( Configure::read('CD') != 81 ) :?>
    <div class="relement_list">
        <fieldset>
            <legend><?php echo __d('relement', 'Relement.documents') ?></legend>
            <div class="panel-body form-horizontal">
                <div class="relement_doc_list">
                <?php
                if (!empty($relements)) {
                    foreach ($relements as $relement) {?>
                        <?php
                        echo $this->Form->checkbox($relement['Relement']['id'], array('name' => 'data[ResponseDoc][Relement][' . $relement['Relement']['id'] . ']'));
                        echo $this->Element('ctxdoc', array('ctxdoc' => $relement['Relement'], 'gfcDocType' => 'Relement', 'cssClass' => 'ctxdoc', 'right_delete' => $right_delete,'hasParapheurActif' => $hasParapheurActif, 'hasEtapeParapheur' => $hasEtapeParapheur));
                    }
                } else {
                    echo $this->Html->tag('div', __d('courrier', 'Relement.void'), array('class' => 'alert alert-warning'));
                }
                ?>
                </div>
            </div>
        </fieldset>
    </div>
    <?php endif;?>
    <?php if( Configure::read('DOC_DEFAULT') && Configure::read( 'CD' ) != 81 ):?>
    <div class="relement_docs">
        <legend><?php echo __d('relement', 'Relement.modelesFournis') ?></legend>
        <div class="panel-body form-horizontal">
            <div class="relement_docs_list">
                <?php
                if (!empty($fichiers)) {
                    foreach ($fichiers as $i => $fichier) {
                        if(!empty($fichier['name'])){
                            echo $this->Element('ctxdoc', array('ctxdoc' => $fichier, 'gfcDocType' => 'Relement', 'cssClass' => 'ctxdoc', 'right_delete' => false));
                        }
						else {
							echo $this->Html->tag('div ', __d('courrier', 'Fichier.void'), array('class' => 'alert alert-warning'));
						}
                    }
                }
                else {
                    echo $this->Html->tag('div ', __d('courrier', 'Fichier.void'), array('class' => 'alert alert-warning'));
                }
                ?>
            </div>
        </div>
    </div>
    <?php endif;?>
<?php endif;?>

<span class="bttn_gen_doc btn btn-info-webgfc"><i class="fa fa-cog" title="<?php echo __d('relement', 'Relement.gendoc') ?>"></i> <?php echo __d('relement', 'Relement.gendoc') ?></span>
<?php echo $this->Form->end(); ?>

<script type="text/javascript">
    $('#upload_target').remove();
<?php
    if ($right_upload) {
?>
    var ulfile = gui.createupload({
        button: "<?php echo __d('default', 'Button.uploadFile'); ?>",
        label: "<?php echo __d('relement', 'Relement.add'); ?>",
        url: "/relements/add",
        fileInputName: "myfile",
        class: 'btn-upload',
        hiddenItem: {
            name: "fluxId",
            value: <?php echo $fluxId; ?>
        }
    });
    $('.relement_list fieldset').append(ulfile);
    // $('.relement_doc_list a.btn-default.btn-resize').css('margin', '0 0 0 -355px');
    // $('.relement_docs_list a.btn-default.btn-resize').css('margin', '0 0 0 -355px');
<?php
    }
?>


    function check_rmodels_select() {
        var valid = false;
        $('.rmodel_list input[type="radio"]').each(function () {
            if ($(this).prop("checked")) {
                valid = true;
            }
        });
        return valid;
    }

    function check_relements_select() {
        var valid = false;
        $('.relement_list input[type="checkbox"]').each(function () {
            if ($(this).prop("checked")) {
                valid = true;
            }
        });
        return valid;
    }


    function updateContextRight(newCourrierId) {
        var url = window.location.href;
        var urlCouper = url.split('/');
        var desktopId = urlCouper[urlCouper.length - 1];
        gui.request({
            url: '/courriers/updateContext/' + newCourrierId + '/' + desktopId
        }, function (data) {
            $('#flux_context').html(data);
        });

    }

//TODO: not work
    $('.bttn_gen_doc').button().click(function () {
        var form = $('#ResponseDocGetRelementsForm');
        var updateElement = $('.context_accordion').find('.ui-state-active');
        var documentTab = $('.context_accordion').find('.document_tab');

        var message = '';
        if (!check_rmodels_select()) {
            message = '<?php echo __d('relement', 'Relement.generate.rmodel_select'); ?>';
        }
        if (!check_relements_select()) {
            if (message != '') {
                message += "<br />";
            }
            message += '<?php echo __d('relement', 'Relement.generate.relement_select'); ?>';
        }
        var event = new $.Event();
        var loader_id = event.timeStamp;
        gui.loader({
            element: $('.context_accordion').find('.ui-state-active'),
            message: gui.loaderMessage,
            id: loader_id
        });

        gui.request({
            url: '/courriers/genResponse',
            data: $(form).serialize()
        }, function (data) {
            $("#loader_" + loader_id).remove();
            getJsonResponse(data);
            updateElement.removeAttr('loaded');
//
            updateContextRight("<?php echo $fluxId; ?>");
        });
    });

    $('.relement_list .ctxdocBttnWebdav').unbind('click').click(function () {
        window.location.href = "<?php echo WEBDAV_URL; ?>" + "/" + "<?php echo $conn; ?>" + "/" + "<?php echo $fluxId; ?>" + "/" + $(this).attr('itemName');
    });

    initContextDocumentBttnActions();

</script>
