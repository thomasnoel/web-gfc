<?php

/**
 *
 * Courriers/set_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    $context_params = array(
            'fluxId' => $fluxId,
            'rights' => $rights,
            'parent' => $parent,
            'sortant' => $sortant,
            'mainDoc' => $mainDoc
    );
//    debug($context_params);
    echo $this->Element('context', $context_params);
?>
