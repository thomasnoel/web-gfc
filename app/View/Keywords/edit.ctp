<?php

/**
 *
 * Keywords/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
if (empty($this->data['Keyword']['id'])) {
    $action = 'add';
    $titre = __('Nouveau mot-clé ', true) . ' : \'';
} else {
    $action = 'edit/' . $this->data['Keyword']['id'];
    $titre = __('Edition du mot-clé', true) . ' : \'' . $this->data['Keyword']['libelle'] . '\' ';
}
echo $html->tag('h1', $titre, array());
echo $form->create(null, array('action' => $action . '/' . $keywordlist_id));
echo $form->input('code', array(
                                            'size' => '100',
                                            'label' => __('Code', true),
                                            'div' => array('id' => 'divServiceNom', 'class' => 'input text required')
                                        ));
echo $form->input('libelle', array(
                                            'size' => '120',
                                            'label' => __('Libellé', true),
                                            'div' => array('id' => 'divServiceDescription')
                                        ));
if ($action == 'edit')
    echo $form->input('actif', array('label' => __('Actif', true)));

echo $form->hidden('id');
echo $form->hidden($this->data['Keyword']['keywordlist_id']);
echo $html->tag('div', null, array('class' => 'submit'));
echo $form->button('<i class="fa fa-times-circle-o" aria-hidden="true"></i> '.__('Annuler', true), array('onclick' => "location.href='" . $html->url(array('action' => 'index', $keywordlist_id)) . "'"));
echo $form->button('<i class="fa fa-floppy-o" aria-hidden="true"></i> '.__('Valider', true), array('type' => 'submit'));
echo $html->tag('/div', null);
echo $form->end();
?>
