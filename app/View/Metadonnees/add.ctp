<?php

/**
 *
 * Metadonnees/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formMetadonnee = array(
    'name' => 'Metadonnee',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'Metadonnee.name' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Metadonnee.typemetadonnee_id' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.typemetadonnee_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $typemetadonnees,
                'required'=>true,
                'empty' => true
            )
        ),
        'Soustype.Soustype' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.soustype_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'multiple' => true,
                'options' => $listeSoustypes
            )
        ),
        'Metadonnee.champfusion' => array(
            'labelText' =>__d('metadonnee', 'Metadonnee.champfusion'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
            )
        ),
		'Metadonnee.active' => array(
			'labelText' =>__d('metadonnee', 'Metadonnee.active'),
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden',
				'value' => true
			)
		),
		'Metadonnee.isrepercutable' => array(
			'labelText' => 'Métadonnée répercutable dans le flux réponse',
			'inputType' => 'checkbox',
			'items'=>array(
				'type'=>'checkbox',
				'checked'=>$this->request->data['Metadonnee']['isrepercutable']
			)
		)
    )
);
echo $this->Formulaire->createForm($formMetadonnee);
echo $this->Form->end();
?>
<?php echo $this->Html->tag('div', 'Veuillez enregistrer pour pouvoir saisir des valeurs pour cette métadonnée', array('id' => 'addValue','class'=>'alert alert-warning')); ?>
<script type="text/javascript">
    $('#addValue').css('display', 'none');
    <?php if(!empty(Configure::read('Selectvaluemetadonnee.id'))){ ?>
    var valOfSelect = <?php echo Configure::read('Selectvaluemetadonnee.id');?>;
    $(document).ready(function () {
        if ($('#MetadonneeTypemetadonneeId').val() == valOfSelect) {
            $('#addValue').css('display', 'block');
        } else {
            $('#addValue').css('display', 'none');
        }

        $('#MetadonneeTypemetadonneeId').change(function () {
            if ($('#MetadonneeTypemetadonneeId').val() == valOfSelect) {
                $('#addValue').css('display', 'block');
            } else {
                $('#addValue').css('display', 'none');
            }
        });
    });
    <?php } ?>
    $(document).ready(function () {
        $('#SoustypeSoustype').select2({allowClear: true, placeholder: "Sélectionner un soustype"});
        $('#MetadonneeTypemetadonneeId').select2();
    });
</script>
