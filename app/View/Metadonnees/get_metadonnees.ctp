<?php

/**
 *
 * Metadonnees/get_metadonnees.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php

if (!empty($metadonnees)) {
    $fields = array(
        'name',
        'typemetadonneename',
        'active'
    );
    $options = array();
    $data = $this->Liste->drawTbody($metadonnees, 'Metadonnee', $fields);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_metadonnees"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "true"
           >
    </table>
</div>
<script>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($metadonnees,$options); ?>');
    $('#table_metadonnees')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("metadonnee","Metadonnee.name"); ?>",
                        class: "name",
						rowspan_: 3
                    },
					{
						field: "typemetadonneename",
						title: "<?php echo __d("metadonnee","Metadonnee.typemetadonnee_id"); ?>",
						class: "typemetadonneename",
						rowspan_: 3
					},
                    {
                        field: "active",
                        title: "<?php echo __d('metadonnee', 'Metadonnee.active'); ?>",
                        class: "active_column"
                    },
					{
						field: "edit",
						title: "Modifier",
						class: "actions",
						width: "80px",
						align: "center"
					},
					{
						field: "delete",
						title: "Supprimer",
						class: "actions",
						width: "80px",
						align: "center"
					}
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            });
    $(document).ready(function () {
        addClassActive();
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
        $(".fixed-table-container").css('width', $(window).width() * 0.955);
    }

    function addClassActive() {
        $('#table_metadonnees .active_column').hide();
        $('#table_metadonnees .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }

</script>
<?php
	$actions = array(
			"edit" => array(
					"url" => Configure::read('BaseUrl') . '/metadonnees/edit',
					"updateElement" => "$('#infos .content')",
					"formMessage" => false
			),
			"delete" => array(
					"url" => Configure::read('BaseUrl') . '/metadonnees/delete',
					"updateElement" => "$('#infos .content')",
					"refreshAction" => "loadMeta();"
			)
	);
    echo $this->LIste->drawScript($metadonnees, 'Metadonnee', $fields, $actions);
    echo $this->Js->writeBuffer();
} else {
    echo $this->Html->div('alert alert-warning',__d('metadonnee', 'Metadonnee.void'));
}
?>
