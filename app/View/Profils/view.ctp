<?php

/**
 *
 * Profils/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="groups_tabs">
    <legend><?php echo $this->request->data['Profil']['name'] .':'.__d('profil', 'Profil.droitsFunc'); ?> </legend>
    <div id="habilitation_fonction">
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        gui.request({
            url: "<?php echo "/profils/getDroitsFunc/" . $id . "/func" ?>",
            updateElement: $('#groups_tabs #habilitation_fonction')
        });
    });
</script>
