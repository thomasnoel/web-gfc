<?php

/**
 *
 * Profils/set_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formProfil = array(
    'name' => 'Profil',
    'label_w' => 'col-sm-3',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Profil.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Profil.name' => array(
            'labelText' =>__d('profil', 'Profil.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        )
    )
);
echo $this->Formulaire->createForm($formProfil);
echo $this->Form->end();
?>
