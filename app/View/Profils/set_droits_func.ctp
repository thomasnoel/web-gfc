<?php
/**
 *
 * Profils/set_droits_func.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->tag('div', __d('profil', 'Profil.rightEdit.txt'), array('class' => 'flushRightsTab'));
?>
<?php
if ($mode == 'func') {
    if( Configure::read( 'debug' ) > 0 ) {
    }
    if ($rights['Right']['mode_classique']) {
        echo $this->Html->tag('p', __d('right', 'mode_classique.active') . __d('right', 'mode_classique.active.edit'), array('class' => 'alert alert-warning', 'style' => 'font-style: italic;padding: 2px;'));
    }
    echo $this->Element('rights', array('rights' => $rights, 'tabsForSets' => $tabsForSets, 'edit' => true, 'noSubmit' => true, 'url' => Router::url(array('controller' => 'profils', 'action' => 'setDroitsFunc'))));
} else if ($mode == 'func_classique') {
    echo $this->Html->tag('span', __d('right', 'mode_simplifie'), array('class' => 'modeSimplifieBttn'));
    $options = array(
        'no' => $this->Html->image('/DataAcl/img/test-fail-icon.png', array('style' => 'width: 16px;', 'alt' => 'no')),
        'yes' => $this->Html->image('/DataAcl/img/test-pass-icon.png', array('style' => 'width: 16px;', 'alt' => 'yes')),
        'parentNodeMarkClose' => '<i class="fa fa-minus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
        'parentNodeMarkOpen' => '<i class="fa fa-plus-square-o" aria-hidden="true" style="padding-right: 0.6em;"></i>',
        'edit' => true,
        'tableRootClass' => 'ui-widget ui-widget-content ui-corner-all',
        'legendClass' => 'ui-widget ui-widget-content ui-corner-all ui-state-focus',
        'fieldsetClass' => 'ui-widget ui-widget-content ui-corner-all',
        'fieldsetStyle' => 'margin-top: 15px;',
        'legendStyle' => 'padding-left: 55px;padding-right: 55px;padding-top: 3px;padding-bottom: 3px;',
        'requester' => array(
                'model' => 'Profil',
                'foreign_key' => $id
        ),
        'aclType' => 'Acl',
        'fields' => array(
                'ressource' => 'Fonctions',
                'read' => 'accès'
        ),
        'noSubmit' => true
    );
    $this->DataAcl->drawTable($rights, true, $options);
}
?>
<script type="text/javascript">
    initTables(true);
    <?php if ($mode == 'func') { ?>
        $('.modeClassiqueBttn').button().click(function(){
            var cpt = 0;
            $('#groups_tabs a').each(function() {
                if (cpt == 1){
                    var href = $.data(this, 'href.tabs');
                    href = href.replace('func', 'func_classique');
                    $('#groups_tabs').tabs('url', cpt, href);
                }
                cpt++;
            });
            $('#groups_tabs').tabs('load', 1);
        });
    <?php } else if ($mode == 'func_classique') { ?>
        $('.modeSimplifieBttn').button().click(function(){
            var cpt = 0;
            $('#groups_tabs a').each(function() {
                if (cpt == 1){
                    var href = $.data(this, 'href.tabs');
                    href = href.replace('func_classique', 'func');
                    $('#groups_tabs').tabs('url', cpt, href);
                }
                cpt++;
            });
            $('#groups_tabs').tabs('load', 1);
        });
    <?php } ?>
    $('.submit').each(function(){
        $('input[type=submit]', this).remove();
        var span = $('<span></span>').button().html("<?php echo __d('default', 'Button.submit'); ?>").css({
            'padding-left': '10px',
            'padding-right': '10px',
            'padding-bottom': '5px',
            'padding-top': '5px'
        }).click(function(){
            var form = $('form', $(this).parents('.ui-tabs-panel'));

            gui.request({
                    url: form.attr('action'),
                    data: form.serialize(),
                    loader: true,
                    loaderElement: form,
                    loaderMessage: gui.loaderMessage
            }, function(data){
                    getJsonResponse(data);
            });
        });
        $(this).append(span);
    });

</script>
