<?php

/**
 *
 * Profils/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div id="groups_tabs" >

    <div class="panel-body">
        <?php
        $formProfils = array(
            'name' => 'Profil',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Profil.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'Profil.name' => array(
                    'labelText' =>__d('profil', 'Profil.name'),
                    'inputType' => 'text',
                    'items'=>array(
                        'readonly' => true,
                        'required'=>true,
                        'type'=>'text'
                    )
                ),
                'Profil.active' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'Profil.readonly' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            )
        );

        if ( $isLdap ) {
            $formProfils['input']['Profil.ModelGroup'] = array(
                'labelText' =>__d('profil', 'Profil.ldapgroups'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'multiple' => true,
                    'options' => $groups,
                    'class'=>'ldapparam'
                )
            );
        }

        echo $this->Formulaire->createForm($formProfils);
        echo $this->Form->end();
        ?>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('#ProfilName').blur(function () {
        gui.request({
            url: "/profils/ajaxformvalid/name",
            data: $('#ProfilSetInfosForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        })
    });

    $('#ProfilActive').attr('disabled', 'disabled');
    $('#ProfilModelGroup').select2({allowClear: true, placeholder: "Sélectionner un groupe"});

    $('#ProfilName').css('cursor', 'not-allowed' );
</script>
