<?php

$data = array();
$fields = array(
    'filesystem',
    'size',
    'used',
    'avail',
    'use',
    'mounted_on'
);

for ($i = 1; $i < count($disks); $i++) {
    $data[] = array_combine($fields, preg_split('/\s+/', $disks[$i], 6));
}
?>

<table id="disks_table" data-toggle="table"></table>

<script type="text/javascript">
    
    $('#disks_table').bootstrapTable({
            data: <?php echo json_encode( $data );?>,
            width: "1200px",
            columns: [
                {
                    field: 'filesystem',
                    title: 'Espace disque',
                    class: 'name'
                },
                {
                    field: 'size',
                    title: 'Taille',
                    class: 'name'
                },
                {
                    field: 'used',
                    title: 'Utilisé',
                    class: 'name'
                },
                {
                    field: 'avail',
                    title: 'Disponible',
                    class: 'name'
                },
                {
                    field: 'use',
                    title: 'Pourcentage utilisé',
                    class: 'name'
                },
                {
                    field: 'mounted_on',
                    title: 'Point de montage',
                    class: 'name'
                }
            ]
        });
</script>