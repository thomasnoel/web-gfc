<div class="">
    <div id="backButton" style="margin-bottom: 10px;"></div>
    <div class="col-sm-6 ">
        <div class="table-list row superadmin-table-list">
            <h3><?php echo __d('menu', 'Console'); ?></h3>
            <div class="group-btn-admin">
                <a href='#' class="btn btn-success btn-block getLogs" role="button">Consulter les logs</a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 ">
        <div class="table-list row superadmin-table-list">
            <h3><?php echo __d('menu', 'Disque'); ?></h3>
            <div class="group-btn-admin">
                <a href='#' class="btn btn-success btn-block getDisk" role="button">Etat des disques</a>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.getLogs').button().click(function () {
        getLogs();
    });
    
    function getLogs() {
        var url = '/consoles/ajaxlogs/';
        gui.formMessage({
            url: url,
            loader: true,
            title: 'Logs',
            width: 470,
            loaderMessage: gui.loaderMessage,
            buttons: {
                "<i class='fa fa-times' aria-hidden='true'></i> Fermer": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/consoles/index"; ?>";
                }
            }
        });
    }
    $('.getDisk').button().click(function () {
        getDisk();
    });
    
    function getDisk() {
        var url = '/consoles/ajaxdisk/';
        gui.formMessage({
            url: url,
            loader: true,
            title: 'Espace disque actuel',
            width: 470,
            loaderMessage: gui.loaderMessage,
            buttons: {
                "<i class='fa fa-times' aria-hidden='true'></i> Fermer": function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }
            }
        });
    }
    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            }
        ]
    });
</script>