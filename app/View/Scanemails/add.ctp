<?php

/**
 *
 * Collectivites/scanemail_add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formScanemail = array(
        'name' => 'Scanemail',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Scanemail.scriptname' => array(
                'labelText' => __d('scanemail', 'Scanemail.scriptname'),
                'inputType' => 'text',
                'labelPlaceholder' =>__d('scanemail', 'Scanemail.placeholderScriptnameAdd'),
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Scanemail.name' => array(
                'labelText' =>__d('scanemail', 'Scanemail.name'),
                'inputType' => 'text',
                'labelPlaceholder' =>__d('scanemail', 'Scanemail.placeholderNameAdd'),
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Scanemail.mail' => array(
                'labelText' =>__d('scanemail', 'Scanemail.mail'),
               'inputType' => 'email',
                'items'=>array(
                    'type'=>'email',
                )
            ),
            'Scanemail.username' => array(
                'labelText' =>__d('scanemail', 'Scanemail.username'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Scanemail.password' => array(
                'labelText' =>__d('scanemail', 'Scanemail.password'),
                'inputType' => 'password',
                'items'=>array(
                    'type'=>'password',
                    'required'=>true
                )
            ),
            'Scanemail.hostname' => array(
                'labelText' =>__d('scanemail', 'Scanemail.hostname'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required'=>true
                )
            ),
            'Scanemail.configuration' => array(
                'labelText' => 'Options de connexion',
                'inputType' => 'text',
                'labelPlaceholder' => 'Par défaut mettre : /novalidate-cert',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Scanemail.port' => array(
                'labelText' =>__d('scanemail', 'Scanemail.port'),
                'inputType' => 'number',
                'items'=>array(
                    'type'=>'number',
                    'required'=>true,
                    'min'=>0
                )
            ),
            'Scanemail.desktopmanager_id' => array(
                'labelText' =>__d('scanemail', 'Scanemail.desktop_id'),
                'inputType' => 'select',
                'items'=>array(
                    'options' => $listDesktopsManagers,
                    'type'=>'select',
                    'empty' => true,
                    'required'=>true
                )
            ),
            'Scanemail.type_id' => array(
                'labelText' =>__d('scanemail', 'Scanemail.type_id'),
                'inputType' => 'select',
                'items'=>array(
//                    'options' => array(),
                    'options' => $types,
                    'type'=>'select',
                    'empty' => true,
                )
            ),
            'Scanemail.soustype_id' => array(
                'labelText' =>__d('scanemail', 'Scanemail.soustype_id'),
                'inputType' => 'select',
                'items'=>array(
                    'options' => array(),
                    'type'=>'select',
                    'empty' => true
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formScanemail);
    echo $this->Form->end();
?>

<script type="text/javascript">
//    $('#ScanemailDesktopId').select2();
    $('#ScanemailDesktopmanagerId').select2({allowClear: true, placeholder: "Sélectionner un bureau"});
    $("#ScanemailTypeId").select2({allowClear: true, placeholder: "Sélectionner un type"});
    $("#ScanemailSoustypeId").select2({allowClear: true, placeholder: "Sélectionner un sous-type"});

    //contruction du tableau de types / soustypes
    $('#ScanemailTypeId').change(function () {
        var typeChoix = new Array();
        typeChoix.push($('#ScanemailTypeId').val());
        chargeSoustypes(typeChoix);
    });
    function chargeSoustypes(types) {
        $.ajax({
            type: 'post',
            url: '/recherches/charge_soustypes',
            data: {TypeChoix: types},
            success: function (data) {
                $('#ScanemailSoustypeId').html(data);
            }
        });
    }

    $("<span class='formatNomScanemail alert alert-warning'><?php echo  __d('scanemail', 'Scanemail.formatScriptnameAdd'); ?></span>").insertAfter("#ScanemailScriptname");


    $('#ScanemailPassword').parent().append('<i class="fa fa-eye-slash unmaskscanemail" aria-hidden="true"></i>');

    $('.unmaskscanemail').on('click', function(){
        if($(this).parent().find('input').attr('type') == 'password') {
            changeType($(this).parent().find('input'), 'text');
            $(this).parents().find('#ScanemailAddForm i.unmaskscanemail').removeClass('fa-eye-slash').addClass('fa-eye');
        }
        else {
            changeType($(this).parent().find('input'), 'password');
            $(this).parents().find('#ScanemailAddForm i.unmaskscanemail').removeClass('fa-eye').addClass('fa-eye-slash');
        }
        return false;
    });
    /*
      function from : https://gist.github.com/3559343
      Thank you bminer!
    */
    // x = élément du DOM, type = nouveau type à attribuer
    function changeType(x, type) {
       if(x.prop('type') == type)
          return x; // ça serait facile.
       try {
          // Une sécurité d'IE empêche ceci
          return x.prop('type', type);
       }
       catch(e) {
          // On tente de recréer l'élément
          // En créant d'abord une div
          var html = $("<div>").append(x.clone()).html();
          var regex = /type=(\")?([^\"\s]+)(\")?/;
          // la regex trouve type=text ou type="text"
          // si on ne trouve rien, on ajoute le type à la fin, sinon on le remplace
          var tmp = $(html.match(regex) == null ?
             html.replace(">", ' type="' + type + '">') :
             html.replace(regex, 'type="' + type + '"') );

          // on rajoute les vieilles données de l'élément
          tmp.data('type', x.data('type') );
          var events = x.data('events');
          var cb = function(events) {
             return function() {
                //Bind all prior events
                for(i in events) {
                   var y = events[i];
                   for(j in y) tmp.bind(i, y[j].handler);
                }
             }
          }(events);
          x.replaceWith(tmp);
          setTimeout(cb, 10); // On attend un peu avant d'appeler la fonction
          return tmp;
       }
    }
</script>
