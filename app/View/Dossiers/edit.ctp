<?php

/**
 *
 * Dossiers/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formDossier = array(
    'name' => 'Dossier',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'name' => array(
            'labelText' =>__d('dossier', 'Dossier.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'comment' => array(
            'labelText' =>__d('dossier', 'Dossier.comment'),
            'inputType' => 'textarea',
            'items'=>array(
                'type'=>'textarea'
            )
        )
    )
);
echo $this->Formulaire->createForm($formDossier);
echo $this->Form->end();
?>
<script type="text/javascript">
    $('#DossierEditForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>
