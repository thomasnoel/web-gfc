
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));

$formConnecteurSms = array(
	'name' => 'Sms',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-7',
	'form_url' => array( 'controller' => 'sms', 'action' => 'sendSms'),
	'input' => array(
		'Sms.courrier_id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden',
				'value' => $fluxId
			)
		),
		'Sms.numero' => array(
			'labelText' => 'Numéro du destinataire',
			'inputType' => 'text',
			'placeHolder' => '06/07xxxxxxxx',
			'items'=>array(
				'type'=>'text',
				'required' => true,
				'max'=>"99999999",
				'min'=>"0",
				'data-minlength'=>"10"
			)
		),
		'Sms.message' => array(
			'labelText' => 'Message (< 160 carac.)',
			'inputType' => 'textarea',
			'items'=>array(
				'type'=>'textarea',
				'required' => true,
				'empty' => false
			)
		)
	)
);
echo $this->Formulaire->createForm($formConnecteurSms);
$this->Form->end();
?>
<div id="spnCharLeft"></div>
<script type="text/javascript">
	$('#SmsNumero').after($('<span></span>').attr('id', 'phone-check').css('margin-left', '5px'))
	var confirmOk = $('<span></span>').append('<div class="help-block with-errors"><ul class="list-unstyled"><li  style="color:#5c9ccc">Le numéro de téléphone est correct.</li></ul></div>');
	var confirmNok = $('<span></span>').append('<div class="help-block with-errors"><ul class="list-unstyled"><li style="color:red;">Veuillez saisir un numéro de téléphone valide commençant par 06 ou 07.</li></ul></div>');

	$('#SmsNumero').on('change', function () {
		var tel = $(this).val();

		if (tel === "") {
			$('#phone-check').empty()
			return;
		}

		tel = tel.replace(/\s+/g, '');
		$(this).val(tel);
		re = new RegExp(/^0(6|7)\d{8}$/);

		if(tel.match(re)){
			$('#phone-check').empty().append(confirmOk);
		}else {
			$('#phone-check').empty().append(confirmNok);
		}
	});

	$('#SmsMessage').after($('<span></span>').attr('id', 'spnCharLeft'));
	$('#spnCharLeft').css('display', 'none');
	var maxLimit = 160;

	$('#SmsMessage').keyup(function () {
		var lengthCount = this.value.length;
		if (lengthCount > maxLimit) {
			this.value = this.value.substring(0, maxLimit);
			var charactersLeft = maxLimit - lengthCount + 1;
		}
		else {
			var charactersLeft = maxLimit - lengthCount;
		}
		$('#spnCharLeft').css('display', 'block');
		$('#spnCharLeft').text(charactersLeft + ' caractères restants');
	});

</script>
