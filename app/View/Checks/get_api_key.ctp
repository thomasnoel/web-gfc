<?php

/**
 *
 * Authentifications/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="container">
	<div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
	<div id="liste" class="row">
	<div id="panelapiKey" class="table-list">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-key" aria-hidden="true"></i> Clé d'API</h3>
			</div>
			<div class="panel-body form-horizontal" >
				<?php
				$formUserApiKey = array(
					'name' => 'Environnement',
					'label_w' => 'col-sm-5',
					'input_w' => 'col-sm-5',
					'form_url' => array( 'controller' => 'checks', 'action' => 'getApiKey'),
					'input' => array(
						'User.apitoken' => array(
							'labelText' => "Clé d'API globale",
							'inputType' => 'text',
							'items'=>array(
								'type'=>'text',
								'value' => $user['User']['apitoken']
							)
						)
					)
				);
				echo $this->Formulaire->createForm($formUserApiKey);
				?>
			</div>

			<?php $apiTokenGen = '<i class="fa fa-refresh" id=apiTokenGen name=apiTokenGen title="Regénérer"></i>';?>
			<script type="text/javascript">
				var apiTokenGen = $('<?php echo $apiTokenGen; ?>').css({height: '16px', width: '16px', color: '#5397a7', float: 'right', margin: '-42px -50px auto auto', cursor: 'pointer'}).click(function () {
					swal({
						showCloseButton: true,
						title: "<?php echo __d('default', 'Confirmation de génération'); ?>",
						text: "<?php echo __d('default', "Êtes-vous sûr de vouloir modifier la clé d'API globale ? <br />  <br />Si oui, il vous faudra communiquer la nouvelle clé aux applications tierces <br />se connectant précédemment avec l'ancienne clé"); ?>",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#d33",
						cancelButtonColor: "#3085d6",
						confirmButtonText: '<i class="fa fa-refresh" aria-hidden="true"></i> Regénérer',
						cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
					}).then(function (data) {
						if (data) {
							gui.request({
								url: "/checks/genApiKey/" + <?php echo $user['User']['id'];?>,
								data: $(this).parents('form').serialize(),
								updateElement: $('#panelapiKey'),
								loader: true,
								loaderMessage: gui.loaderMessage
							}, function (data) {
								var s = getJsonResponse(data)['message'];
								var newapiValue = s.substring(  0, s.indexOf('<hr />') );
								$('#UserApitoken').val(newapiValue );
								getJsonResponse(data);
							});
						} else {
							swal({
								showCloseButton: true,
								title: "Annulé!",
								text: "Vous n\'avez pas modifié la clé.",
								type: "error",

							});
						}
					});
				}).appendTo($('#UserApitoken').parent());
			</script>
		</div>
	</div>
</div>


<script type="text/javascript">
	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
				}
			}
		]
	});
</script>
