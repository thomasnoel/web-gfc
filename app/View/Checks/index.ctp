<?php

/**
 *
 * Checks/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    //echo $this->Html->css(array('administration'), null, array('inline' => false));
    $yes = '<td class="icon-vert tdValue"><i class="fa fa-check" aria-hidden="true"></i></td>';
    $no = '<td class="icon-rouge tdValue"><i class="fa fa-times" aria-hidden="true"></i></td>';
    $unknow = '<td class="tdValue">--</i></td>';
?>
<div class="container">
    <div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
    <div class="row">
        <div class="table-list panel-infoBox" style="min-height:10px;">
            <h3 >Vérifications des pré-requis</h3
                <div class="panel-body">
                    <div class="col-sm-6">
                        <legend>Version des applications utilisées</legend>
                        <table class="table borderless">
                            <thead>
                                <tr>
                                    <th>Applications</th>
                                    <th class="tdValue">Version</th>
                                </tr>
                            </thead>
                            <tbody>
								<tr>
									<td><?php echo $isubuntu ? 'Ubuntu' : 'CentOS';?></td>
									<td class="tdValue"><?php echo $verif['ubuntults_version'] ?></td>
								</tr>
								<tr>
									<td></td>
									<td class="tdValue"><?php echo $verif['ubuntutotal_version'] ?></td>
								</tr>
                                <tr>
                                    <td>Apache</td>
                                    <td class="tdValue"><?php echo $verif['apache_version'] ?></td>
                                </tr>
                                <tr>
                                    <td>PHP</td>
                                    <td class="tdValue"><?php echo $verif['php_version'] ?></td>
                                </tr>
                                <tr>
                                    <td>PostgreSQL</td>
                                    <td class="tdValue"><?php echo $verif['postgresql_version'] ?></td>
                                </tr>
                                <tr>
                                    <td>CakePHP</td>
                                    <td class="tdValue"><?php echo $verif['cake_version'] ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <legend>Configuration du plugin DataAcl</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Présence des variables de configuration dans le fichier 'APP/Config/core.php'</td>
                                    <?php echo $verif['dataacl']['core'] ? $yes : $no; ?>
                                </tr>
                            </tbody>
                        </table>
                        <legend>Serveur de conversion documentaire cloudooo</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Présence des variables de configuration dans le fichier 'APP/Config/webgfc.inc'</td>
                                    <?php echo $verif['cloudooo']['config'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Serveur actif</td>
                                    <?php echo $verif['cloudooo']['up'] ? $yes : $no; ?>
                                </tr>
                            </tbody>
                        </table>
                        <legend>Session</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Délai de la session</td>
                                    <td class="tdValue"><?php echo $verif['delai_session']['value']; ?></td>
                                </tr>
                            </tbody>
                        </table>
                        <legend>Modules PHP (notamment pour le CAS)</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Mail.php</td>
                                    <?php echo $verif['modules']['Mail.php'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Mail/mime.php</td>
                                    <?php echo $verif['modules']['Mail/mime.php'] ? $yes : $no ; ?>
                                </tr>
                                <tr>
                                    <td>CAS.php</td>
                                    <?php echo $verif['modules']['CAS.php'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>XML/RPC2/Client.php</td>
                                    <?php echo $verif['modules']['XML/RPC2/Client.php'] ? $yes : $no; ?>
                                </tr>
                            </tbody>
                        </table>
						<legend>Répertoire de stockage des documents (par défaut : <?php echo WORKSPACE_PATH;?> dans core.php)</legend>
						<table class="table borderless">
							<tbody>
							<?php foreach($verif['data_workspace'] as $workspace => $value) :?>
								<tr>
									<td><?php echo $workspace;?></td>
									<?php echo $value ? $yes : $no; ?>
								</tr>
							<?php endforeach;?>
							</tbody>
						</table>
                    </div>
                    <div class="col-sm-6">
                        <legend>Configuration web-GFC</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Présence du fichier 'APP/Config/webgfc.inc'</td>
                                    <?php echo $verif['webgfc']['inc'] ? $yes : $no; ?>
                                </tr>
                                <tr>

                                    <td>Présence du fichier 'APP/webroot/files/wsdl/webgfc.wsdl'</td>
                                    <?php echo $verif['webgfc']['wsdl'] ? $yes : $no; ?>
                                </tr>
                            </tbody>
                        </table>
                        <legend>Serveur de fusion documentaire GED'OOo</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Présence des variables de configuration dans le fichier 'APP/Config/webgfc.inc'</td>
                                    <?php echo $verif['gedooo']['config'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Serveur actif</td>
                                    <?php echo $verif['gedooo']['up'] ? $yes : $no; ?>
                                </tr>
                            </tbody>
                        </table>

                        <legend>Extensions PHP</legend>
                        <table class="table borderless">
                            <tbody>
                                <tr>
                                    <td>Curl</td>
                                    <?php echo $verif['extensions']['curl'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Open SSL</td>
                                    <?php echo $verif['extensions']['openssl'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Imap</td>
                                    <?php echo $verif['extensions']['imap'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Apcu</td>
                                    <?php echo $verif['extensions']['apcu'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>SSH2</td>
                                    <?php echo $verif['extensions']['ssh2'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Zip</td>
                                    <?php echo $verif['extensions']['zip'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Phar</td>
                                    <?php echo $verif['extensions']['phar'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Ldap</td>
                                    <?php echo $verif['extensions']['ldap'] ? $yes : $no; ?>
                                </tr>
                                <tr>
                                    <td>Fileinfo</td>
                                    <?php echo $verif['extensions']['fileinfo'] ? $yes : $no; ?>
                                </tr>
                            </tbody>
                        </table>
						<legend>Outils pour le LAD/RAD</legend>
						<table class="table borderless">
							<tbody>
							<tr>
								<td>Tesseract</td>
								<?php echo $verif['ocr']['tesseract'] ? $yes : $no; ?>
							</tr>
							<tr>
								<td>Ghostscript</td>
								<?php echo $verif['ocr']['gs'] ? $yes : $no; ?>
							</tr>
							</tbody>
						</table>
						<legend>Outil pour la conversion de HTML vers PDF</legend>
						<table class="table borderless">
							<tbody>
							<tr>
								<td>Wkhtmltopdf</td>
								<?php echo $verif['htmltopdf']['wkhtmltopdf'] ? $yes : $no; ?>
							</tr>
							</tbody>
						</table>
						<legend>Outil pour la manipulation des PDFs</legend>
						<table class="table borderless">
							<tbody>
							<tr>
								<td>pdftk</td>
								<?php echo $verif['pdftk']['pdftk'] ? $yes : $no; ?>
							</tr>
							</tbody>
						</table>
                    </div>
                </div>
        </div>
    </div>
</div>

<script type='text/javascript'>

    gui.addbuttons({
        element: $('#backButton'),
        buttons: [
            {
                content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
                class: "btn-info-webgfc",
                action: function () {
                    window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
                }
            }
        ]
    });
</script>

