<?php

/**
 *
 * Configs/add_edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script type="text/javascript">
    function configCancel() {
<?php
$url = array('plugin' => null, 'controller' => 'configs', 'action' => 'add');
if ($this->action == 'edit') {
        $url = array('plugin' => null, 'controller' => 'configs', 'action' => 'edit', $this->data['Config']['id']);
}
echo $this->Js->request($url, array('update' => '#configsPanel', 'before' => '$("#busy-indicator").fadeIn();', 'complete' => '$("#busy-indicator").fadeOut();'));
?>
    }
</script>
<div class="insetShadow" style="margin: 25px;">
<?php
echo $this->Form->create('Config');
if ($this->action == 'edit') {
	echo $this->Form->input('Config.id');
}
echo $this->Form->input('Config.name', array('label' => __d('config', 'Config.name')));
echo $this->Form->input('Config.affichage_bannette', array('label' => __d('config', 'Config.affichage_bannette')));
echo $this->Form->input('Config.resolution', array('label' => __d('config', 'Config.resolution')));
?>
    <table>
        <tr>
            <td>
<?php echo $this->Js->submit(__d('default', 'Button.submit'), array('url' => array('plugin' => null, 'controller' => 'configs', 'action' => 'edit', $this->data['Config']['id']), 'update' => '#configsPanel', 'before' => '$("#busy-indicator").fadeIn();', 'complete' => '$("#busy-indicator").fadeOut();')); ?>
            <td>
                <span id="cancelBttn" onclick="configCancel();" class="bttn"><?php echo __d('default', 'Button.cancel'); ?></span>
            </td>
        </tr>
    </table>
<?php echo $this->Form->end(); ?>
</div>
	<?php echo $this->Js->writeBuffer(); ?>

