<?php

/**
 *
 * Metadonnees/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formIntituleagent = array(
    'name' => 'Intituleagent',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Intituleagent.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Intituleagent.name' => array(
            'labelText' =>__d('intituleagent', 'Intituleagent.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Intituleagent.description' => array(
            'labelText' => __d('intituleagent', 'Intituleagent.description'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'Desktopmanager.Desktopmanager' => array(
            'labelText' => __d('intituleagent', 'Intituleagent.desktopmanager_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'empty' => true,
                'multiple' => true,
                'class' => 'JSelectMultiplel',
                'options' => $desktopsmanagers
            )
        ),
        'Intituleagent.active' => array(
            'labelText' =>__d('intituleagent', 'Intituleagent.active'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>$this->request->data['Intituleagent']['active']
            )
        )
    )
);
echo $this->Formulaire->createForm($formIntituleagent);
echo $this->Form->end();
?>

<script type="text/javascript">

    $('#DesktopmanagerDesktopmanager').select2({allowClear: true, placeholder: "Sélectionner un ou plusieurs bureaux"});
</script>

