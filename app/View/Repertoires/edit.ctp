<?php

/**
 *
 * Repertoires/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formRepertoire= array(
    'name' => 'Repertoire',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'input' => array(
        'Repertoire.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Repertoire.name' => array(
            'labelText' =>__d('repertoire', 'Repertoire.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Repertoire.parent_id' => array(
            'labelText' =>__d('repertoire', 'Repertoire.parent_id'),
            'inputType' => 'select',
            'items'=>array(
                'options' => $repertoires,
                'empty' => true,
                'type' => 'select',
                'escape' => false
            )
        )
    )
);
echo $this->Formulaire->createForm($formRepertoire);
echo $this->Form->end();
?>
<script type="text/javascript">

</script>
