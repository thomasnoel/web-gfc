<?php

/**
 *
 * Repertoires/get_flux.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php
if (!empty($flux)) {

//        $this->Bannette->draw($flux, "Total ", array('view' => $viewAction), true);

         $nbItem = Hash::get($this->request->params, 'paging.Courrier.count');
?>
<div class="panel">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title">Total: <?php echo $nbItem; ?></h4>
        </div>
        <div class="bannette_panel panel-body">
            <table class="bannette-table"
                   data-toggle="table"
                   data-height="499"
                   data-show-refresh="false"
                   data-show-toggle="false"
                   data-show-columns="true"
                   data-search="true"
                   data-locale = "fr-CA"
                   data-select-item-name="toolbar1"
                   id="table_res">
                <thead>
                    <tr>
                        <?php
                            echo $this->Bannette->drawTableHead($flux, array('view' => $viewAction),true);
                        ?>
                    </tr>
                </thead>
            </table>
             <?php
            echo $this->Bannette->drawPaginator($flux,array('view' => 'historiqueGlobal' , 'delete' => 'delete'));
            ?>
        </div>
    </div>
</div>



<?php
echo $this->Js->writeBuffer();
        echo $this->Form->input('repertoireSelected', array('type' => 'hidden', 'id' => 'repertoireSelected', 'value' => $repertoire_id));
} else {
        echo $this->Html->div('alert alert-warning',__d('repertoire', 'Element.void'));
//	echo __d('repertoire', 'Element.void');
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        var fluxChoix = [];
        <?php $data =!empty($this->Bannette->drawTableTd($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete')))?  $this->Bannette->drawTableTd($flux, array('view' => 'historiqueGlobal' , 'delete' => 'delete')) : '[]' ?>;
        data = <?php echo $data; ?>;
        $('.bannette-table').bootstrapTable();
        $("#table_res").bootstrapTable('load', data);
        $("#table_res")
                .on("click-row.bs.table", function (e, row, element) {
                    if (!$(element.context).hasClass("action")) {
                        var href = element.find('.thView').find('a').attr('href');
                        $(location).attr("href", href);
                    } else if ($(element.context).hasClass("thView")) {
                    }
                })
                .on('check.bs.table', function (e, row) {
                    var fluxId = row['id'];
                    fluxChoix.push(fluxId);
                })
                .on('uncheck.bs.table', function (e, row) {
                    var fluxId = row['id'];
                    fluxChoix = jQuery.grep(fluxChoix, function (value) {
                        return value != fluxId;
                    });
                })
                .on('check-all.bs.table', function (e, rows) {
                    $.each(rows, function (i, row) {

                        var fluxId = row['id'];
                        if (jQuery.inArray(fluxId, fluxChoix)) {
                            fluxChoix.push(fluxId);
                        }
                    });
                })
                .on('uncheck-all.bs.table', function (e, rows) {
                    $.each(rows, function (i, row) {
                        var fluxId = row['id'];
                        fluxChoix = jQuery.grep(fluxChoix, function (value) {
                            return value != fluxId;
                        });
                    });
                });

        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
    });



    $('#repertoires_skel_liste .content .treeTr').removeClass('ui-state-focus');
    $('#repertoires_skel_liste .content .treeTr').each(function () {
        if ($(this).attr('itemId') == '<?php echo $repertoire_id; ?>') {
            $(this).addClass('ui-state-focus');
        }
    });
<?php if(!empty($flux)){ ?>
    $('.alert-warning').remove();
<?php } ?>
    gui.removebutton({
        element: $('#repertoires_skel_infos .controls'),
        button: 1
    });

    gui.addbutton({
        element: $('#repertoires_skel_infos .controls'),
        button: {
            content: '<i class="fa fa-trash" aria-hidden="true"></i>',
            action: function () {
                var tabChecked = new Array();
                $('.selected .checkItem').each(function () {
                    tabChecked.push($(this).attr('itemId'));
                });
                if (tabChecked.length == 0) {
                    swal({
                    showCloseButton: true,
                        title: 'Enlever les flux',
                        text: 'Veuillez choisir au moins un flux',

                    });
                } else {
                    var form = "<form id='formChecked'>";
                    for (i in tabChecked) {
                        form += '<input type="hidden" name="data[itemToUnlink][]" value="' + tabChecked[i] + '">';
                    }
                    form += "</form>";
                    swal({
                    showCloseButton: true,
                        title: 'Enlever des flux',
                        text: 'Voulez-vous enlever ces flux du repertoire ?' + form,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#d33",
                        cancelButtonColor: "#3085d6",
                        confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
                        cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
                    }).then(function (data) {
                        if (data) {
                            gui.request({
                                url: '<?php echo Configure::read('BaseUrl') . "/repertoires/unlinkProfil/"; ?>' + $('#repertoireSelected').val(),
                                data: $('#formChecked').serialize()
                            }, function (data) {
                                gui.request({
                                    url: '<?php echo Configure::read('BaseUrl') . '/repertoires/getFlux/' . $repertoire_id; ?>',
                                    loader: true,
                                    updateElement: $('#repertoires_skel_infos .content'),
                                    loaderMessage: gui.loaderMessage
                                });
                                $('#formChecked').remove();
                                loadRepertoires();
                            });
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Annulé!",
                                text: "Vous n\'avez pas supprimé.",
                                type: "error",

                            });
                        }
                    });
                    $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
                }
            }
        }
    });

</script>
