<?php

/**
 *
 * Types/edit.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
    $formType = array(
        'name' => 'Type',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-5',
        'input' => array(
            'Type.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'Type.name' => array(
                'labelText' => __d('type', 'Type.name'),
                'inputType' => 'text',
                'items'=>array(
                    'required'=>true,
                    'type'=>'text'
                )
            ),
            'Type.active' => array(
                'labelText' => __d('type', 'Type.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked'=>$this->request->data['Type']['active']
                )
            )
        )
    );
    if( Configure::read('Webservice.GRC')) {
        $formType['input']['Type.grc_servid'] = array(
            'labelText' => __d('type', 'Type.grc_servid'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        );
    }

echo $this->Formulaire->createForm($formType);
echo $this->Form->end();
?>


<script type="text/javascript">
    $('#TypeEditForm').on('keyup keypress', function (e) {
        var keyCode = e.keyCode || e.which;
        if (keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });
</script>
