<?php

/**
 *
 * Referentielfantoir/import.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Form->create('Import', array(
            'class' => 'form-horizontal ',
            'enctype' => 'multipart/form-data',
            'inputDefaults' => array(
                'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                'div' => array('class' => 'form-group '),
                'label' => array('class' => 'control-label col-sm-5'),
                'between' => '<div class="controls   col-sm-5 ">',
                'after' => '<div class="help-block with-errors"></div></div>',
                'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                'data-toggle' => "validator"
        )));
echo $this->Form->input('Import.departement_id', array('type' => 'select', 'options' => $departements, 'empty' => true, 'label' => array('text'=> __d('referentielfantoir', 'Referetielfantoir.region_id'),'class'=>'control-label  col-sm-5'),'class'=>'form-control'));
echo $this->Form->input('Import.file', array('type' => 'file', 'label' => array('text'=>__d('referentielfantoir', 'Referentielfantoir.File'),'class'=>'control-label  col-sm-5'),'class'=>'form-control'));
echo $this->Form->end();
?>
