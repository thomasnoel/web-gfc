<?php

/**
 *
 * Dossiers/get_dossiers.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//debug($referentielsfantoir);
if (!empty($referentielsfantoir)) {
    $options = array(
        'nodes' => array(
            array(
                'model' => 'Referentielfantoir',
                'field' => 'name',
                'actions' => array(
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => '/referentielsfantoir/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/referentielsfantoir/delete',
                        "updateElement" => "$('#infos .content')",
                        "refreshAction" => "loadStructure();",
//                        "icoColor" => "#FF0000"
                    )
                )
            ),
            array(
                'model' => 'Referentielfantoirdir',
                'field' => 'name',
                'actions' => array(
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => '/referentielsfantoirdir/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/referentielsfantoirdir/delete',
                        "updateElement" => "$('#infos .content')",
                        "refreshAction" => "loadStructure();",
//                        "icoColor" => "#FF0000"
                    )
                )
            ),
            array(
                'model' => 'Referentielfantoircom',
                'field' => 'name',
                'actions' => array(
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => '/referentielsfantoircom/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/referentielsfantoircom/delete',
                        "updateElement" => "$('#infos .content')",
                        "refreshAction" => "loadStructure();",
//                        "icoColor" => "#FF0000"
                    )
                )
            ),
            array(
                'model' => 'Referentielfantoirvoie',
                'field' => 'name',
                'actions' => array(
                    "edit" => array(
                        "ico" => 'fa fa-pencil',
                        "url" => '/referentielsfantoirvoie/edit',
                        "updateElement" => "$('#infos .content')",
                        "formMessage" => false,
//                        "icoColor" => "#5397a7"
                    ),
                    "delete" => array(
                        "ico" => 'fa fa-trash',
                        "url" => '/referentielsfantoirvoie/delete',
                        "updateElement" => "$('#infos .content')",
                        "refreshAction" => "loadStructure();",
//                        "icoColor" => "#FF0000"
                    )
                )
            )
        ),
        'listId' => 'browser',
        'listClass' => 'filetree'
    );
    echo $this->Liste->jqueryTreeView($referentielsfantoir, $options);
} else {
    echo $this->Html->div('alert alert-warning',__d('referentielfantoir', 'Referentielfantoir.void'));
}
?>

<script type="text/javascript">
    $("#browser").treeview({collapsed: true});

    $('.treeviewBttn i').each(function () {
        var parentSpan = $(this).parent().parent();
        var parentLink = $(this).parent();
        var href = $(this).parent().attr('href');
        var img = $(this).detach();
        img.appendTo(parentSpan).click(function () {

            if ($(this).hasClass('editBttn')) {
                gui.request({
                    url: href,
                    updateElement: $('#infos .content'),
                    loader: true,
                    loaderMessage: gui.loaderMessage
                });
            }

            if ($(this).hasClass('deleteBttn')) {
                swal({
                    showCloseButton: true,
                    title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
                    text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#d33',
                    cancelButtonColor: '#3085d6',
                    confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
                    cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
                }).then(function (data) {
//                    $(this).parents(".modal").modal('hide');
                    if (data) {
                        gui.request({
                            url: href,
                            updateElement: $('#webgfc_content'),
                            loader: true,
                            loaderMessage: gui.loaderMessage
                        }, function (data) {
                            getJsonResponse(data);
                            loadReferentielsfantoir();
                        });
                    } else {
                        swal({
                    showCloseButton: true,
                            title: "Annulé!",
                            text: "Vous n'avez pas supprimé, ;) .",
                            type: "error",

                        });
                    }
                });
                $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
            }
        });
        parentLink.remove();
    });
</script>
