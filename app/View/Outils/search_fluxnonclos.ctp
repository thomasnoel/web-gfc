<?php

/**
 *
 * Recherches/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>

<div class="container">
	<div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
	<div class="row">
		<div class="table-list rechercheCondition" id="liste">
			<h3>Conditions</h3>
			<div class="content">
				<div  class="table-list">
					<div class="panel-body form-horizontal">
						<?php echo $this->Html->tag('div', "Ce menu de recherche retournera les flux <b>en cours de traitement</b> qui sont encore présents chez des agents <b>non actifs</b>.", array('class' => 'alert alert-info', 'style'=>'margin-bottom:0px;'));?>
					</div>

					<div class="content" style="padding-top: 30px;">
						<?php

						$formOutil = array(
							'name' => 'Outils',
							'label_w' => 'col-sm-2',
							'input_w' => 'col-sm-4',
							'input' => array(
								'Bancontenu.user_id' => array(
									'inputType' => 'select',
									'labelText' => 'Agent non actif',
									'items'=>array(
										'type' => 'select',
//										'multiple' => true,
										'options' => $users,
										'empty' => true
									)
								),
								'Courrier.reference' => array(
									'inputType' => 'text',
									'labelText' => 'N° de référence',
									'items'=>array(
										'type' => 'text',
										'empty' => true
									)
								)
							)
						);
						echo $this->Formulaire->createForm($formOutil);
						echo $this->Form->end();
						?>
					</div>
				</div>
			</div>
			<div class="controls panel-footer " role="group"></div>
		</div>
		<div class="row">
			<div class="table-list rechercheCondition" id="infos" style="padding-bottom: 40px;overflow: hidden;">
				<h3>Résultats</h3>
				<div class="content"></div>
				<div class="controls panel-footer  " role="group"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	'use strict';

	$("#BancontenuUserId").select2({allowClear: true, placeholder: "Sélectionner un agent non actif"});

	// Ajout de l'action de recherche via la touche Entrée
	$('#OutilsSearchFluxnonclosForm').keypress(function (e) {
		if (e.keyCode == 13) {
			$('#liste .controls .searchBtn').trigger('click');
			return false;
		}
	});

	gui.buttonbox({
		element: $('#liste .controls'),
		align: 'center'
	});

	// Bouton  pour lancer la recherche
	gui.addbuttons({
		element: $('#liste .controls'),
		buttons: [
			{
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc searchBtn',
				action: function () {
					gui.request({
						url: "<?php echo Configure::read('BaseUrl') . "/outils/fluxNonClos/"; ?>",
						data: $('#OutilsSearchFluxnonclosForm').serialize(),
						loader: true,
						updateElement: $('#infos .content'),
						loaderMessage: gui.loaderMessage
					});
					$('#liste').hide();
					$('#infos').show();
					$('#circuit').show();
				}
			}
		]
	});

	$('#infos').hide();
	$('#circuit').hide();
	$('.hide-search-formulaire').hide();


	// Si les résultats sont affichés, on affiche les actions possibles
	if ($('#infos .controls .btn').length == 0) {
		gui.buttonbox({
			element: $('#infos .controls'),
			align: "center"
		});

		// permet de masquer le formulaire de recherche, lorsqu'on le réouvre
		$('#liste h3').css('cursor', 'pointer');
		$('#liste h3').click(function () {
			$('#liste').hide();
		});

		// Bouton pour réouvrir le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc',
				action: function () {
					$('#liste').show();
					$('.hide-search-formulaire').show();
				}
			}
		});

		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: "<i class='fa fa-unlock-alt' ></i>  Détacher le(s) flux",
				title: "<?php echo __d('courrier', 'Button.detachablelot'); ?>",
				class: 'btn-info-webgfc',
				action: function () {
					var tabChecked = new Array();
					var desktopChecked = new Array();

					$('.selected').each(function () {
						tabChecked.push($(this).find('.checkItem').attr('itemid'));
						desktopChecked.push($(this).find('.checkDesktop').attr('desktopid'));
					});
					$('.checkItem').each(function () {
						if ($(this).prop('checked')) {
							tabChecked.push($(this).attr('itemid'));
							desktopChecked.push($(this).attr('desktopid'));
						}
					});
					if (tabChecked.length == 0) {
						swal({
							showCloseButton: true,
							type: 'warning',
							title: 'Détachement de flux',
							text: 'Veuillez choisir au moins un flux',

						});
					} else {
						var form = "<form id='formChecked'>";
						for (var i in tabChecked) {
							form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
							form += '<input type="hidden" name="data[checkDesktop][]" value="' + desktopChecked[i] + '">';
						}
						form += "</form>";

						swal({
							showCloseButton: true,
							title: "<?php echo __d('default', 'Confirmation de détachement'); ?>",
							text: "<?php echo __d('default', 'Voulez-vous détacher ces éléments de votre bannette ?'); ?>",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#d33',
							cancelButtonColor: '#3085d6',
							confirmButtonText: "<?php echo __d('default', 'Button.valid'); ?>",
							cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
						}).then(function (data) {
							if (data) {
								gui.request({
									url: "/outils/passageenlulot",
									data: $(form).serialize(),
									loader: true,
									loaderMessage: gui.loaderMessage
								}, function (data) {
									$(this).parents(".modal").modal('hide');
									$(this).parents(".modal").empty();
									gui.request({
										url: "<?php echo Configure::read('BaseUrl') . "/outils/fluxNonClos/"; ?>",
										data: $('#OutilsSearchFluxnonclosForm').serialize(),
										loader: true,
										updateElement: $('#infos .content'),
										loaderMessage: gui.loaderMessage
									});
								});
							} else {
								swal({
									showCloseButton: true,
									title: "Annulé!",
									text: "Vous n'avez pas détaché, ;) .",
									type: "error",

								});
							}
						});
					}
				}
			}
		});

	}

	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/outils"; ?>";
				}
			}
		]
	});
</script>

