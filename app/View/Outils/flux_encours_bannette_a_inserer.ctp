<?php

/**
 *
 * Recherches/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>
<?php
if (empty($courriers)) {
	echo $this->Html->tag('div', "Aucun flux trouvé", array('class' => 'alert alert-warning'));
}else{
	?>
	<div class="bannette_panel panel-body">
		<script type="text/javascript">
			var screenHeight = $(window).height() * 0.5;
		</script>
		<!--  Tableau de résultat affichant les informations du flux -->
		<table class="-table bannette-table"
			   data-toggle="table"
			   data-height=screenHeight
			   data-show-refresh="false"
			   data-show-toggle="false"
			   data-show-columns="false"
			   data-search="false"
			   data-locale = "fr-CA"
			   data-select-item-name="toolbar1"
			   id="table_res">
			<thead>
			<tr>
				<?php
				$ths = "";
				$ths .= $this->Html->tag('th', '', array('class' => 'bs-checkbox', 'data-field' => 'state', 'data-checkbox' => 'true'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.reference'), array('class' => 'reference tri', 'data-sortable' => 'true', 'data-field' => 'reference'));
				$ths .= $this->Html->tag('th', __d('bannette', 'Courrier.etat'), array('class' => 'etat tri', 'data-sortable' => 'true', 'data-field' => 'etat'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.intitule'), array('class' => 'nom tri', 'data-sortable' => 'true', 'data-field' => 'nom'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.objet'), array('class' => 'objet tri', 'data-sortable' => 'true', 'data-field' => 'objet'));
				$ths .= $this->Html->tag('th', 'Agent possédant le flux actuellement', array('class' => 'tri', 'data-sortable' => 'true', 'data-field' => 'agentstraitants'));
				$ths .= $this->Html->tag('th', __d('recherche', 'Courrier.datereception'), array('class' => 'datereception tri', 'data-sortable' => 'true', 'data-field' => 'datereception'));
				$ths .= $this->Html->tag('th', __d('courrier', 'Courrier.retard'), array('class' => 'retard tri', /* 'data-sortable' => 'true',  */ 'data-field' => 'retard', 'data-align' => 'center', 'data-width' => '80px'));
				$ths .= $this->Html->tag('th', 'Actions', array('class' => 'actions thView', 'data-field' => 'view', 'data-align' => 'center', 'data-width' => '80px'));
				echo $ths;
				?>
			</tr>
			</thead>
		</table>

		<?php
		$drawTds = array();
		foreach($courriers as $c => $courrier) {
			// traduction des données du flux
			$url = array('plugin' => null, 'controller' => 'courriers', 'action' => 'historiqueGlobal', $courrier['Courrier']['id']);
			$view = $this->Html->link($this->Html->tag('i', '', array('alt' => __d('default', 'Button.view'), 'title' => __d('default', 'Button.view'), 'class' => 'fa fa-eye')), $url, array('target' => '_blank', 'escape' => false));
			if ($courrier['Courrier']['mail_retard_envoye']) {
				$retardDisplay = $this->Html->image("/img/retard_16.png", array('height' => '16px;', 'style' => 'vertical-align: middle;', 'alt' => __d('courrier', 'Courrier.alertRetard'), 'title' => __d('courrier', 'Courrier.alertRetard')));
			}
			$result = array(
				"id" => $courrier['Courrier']['id'],
				'reference' => $courrier['Courrier']["reference"] . $this->Form->input('checkDesktop_' . $courrier['Bancontenu']['desktop_id'], array('type' => 'hidden', 'class' => 'checkDesktop', 'desktopid' => $courrier['Bancontenu']['desktop_id'])),
				'etat' => $this->Html->image('/img/flux_a_traite.png', array('alt' => 'traitement', 'title' => __d('courrier', 'Courrier.courrier_a_traite'))),
				'nom' => $courrier['Courrier']['name'] . $this->Form->input('checkItem_' . $courrier['Courrier']['id'], array('type' => 'hidden', 'class' => 'checkItem', 'itemid' => $courrier['Courrier']['id'])),
				'objet' => preg_replace("/[\n\r]/", " ", $this->String->troncateStr($courrier['Courrier']["objet"], 90)),
				'agentstraitants' => nl2br( $courrier['Courrier']['agentstraitants'] ),
				'datereception' => strftime("%d/%m/%Y %T", strtotime($courrier['Courrier']["datereception"])),
				'retard' => @$retardDisplay,
				'view' => $view
			);
			array_push($drawTds, $result);
			$data = json_encode($drawTds);
		}
		?>
	</div>

	<script type="text/javascript">
		$(document).ready(function () {
			var fluxChoix = [];
			var desktopChoix = [];
			if ($('.table-list h3 span').length < 1) {
				<?php if(isset($nbCourriers) && !empty($nbCourriers)): ?>
				$('.table-list h3').append(' <span> - total: <?php echo $nbCourriers;?></span>');
				<?php endif; ?>
			} else {
				$('.table-list h3 span').remove();
				<?php if(isset($nbCourriers) && !empty($nbCourriers)): ?>
				$('.table-list h3').append(' <span> - total: <?php echo $nbCourriers;?></span>');
				<?php endif; ?>
			}

			// affichage des données du flux
			var data = <?php echo $data; ?>;
			$('.bannette-table').bootstrapTable();
			$("#table_res").bootstrapTable('load', data);
			$("#table_res")
				.on("click-row.bs.table", function (e, row, element) {
					if (!$(element.context).hasClass("action")) {
						var itemId = element.find('.checkItem').attr('itemId');
						window.open('/courriers/historiqueGlobal/' + itemId);
					}
				})
				.on('check.bs.table', function (e, row) {
					var fluxId = row['id'];
					fluxChoix.push(fluxId);

					var desktopId = row['desktopId'];
					desktopChoix.push(desktopId);
				})
				.on('uncheck.bs.table', function (e, row) {
					var fluxId = row['id'];
					fluxChoix = jQuery.grep(fluxChoix, function (value) {
						return value != fluxId;
					});
					var desktopId = row['desktopId'];
					desktopChoix  = jQuery.grep(desktopChoix, function (valueDesktop) {
						return valueDesktop != desktopId;
					});
				})
				.on('check-all.bs.table', function (e, rows) {
					$.each(rows, function (i, row) {

						var fluxId = row['id'];
						if (jQuery.inArray(fluxId, fluxChoix)) {
							fluxChoix.push(fluxId);
						}
						var desktopId = row['desktopId'];
						if (jQuery.inArray(desktopId, desktopChoix)) {
							desktopChoix.push(desktopId);
						}
					});
				})
				.on('uncheck-all.bs.table', function (e, rows) {
					$.each(rows, function (i, row) {
						var fluxId = row['id'];
						fluxChoix = jQuery.grep(fluxChoix, function (value) {
							return value != fluxId;
						});

						var desktopId = row['desktopId'];
						desktopChoix = jQuery.grep(desktopChoix, function (valueDesktop) {
							return valueDesktop != desktopId;
						});
					});
				});

		});
	</script>
<?php }?>
