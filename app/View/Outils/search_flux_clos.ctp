<?php

/**
 *
 * Recherches/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('zone/Recherches/recherchesFunction.js', array('inline' => true));
echo $this->Html->script('appAttendable.js', array('inline' => true));
?>

<div class="container">
	<div id="backButton" style="margin-left: -15px; margin-bottom: 10px;"></div>
	<div class="row">
		<div class="table-list rechercheCondition" id="liste">
			<h3>Conditions</h3>
			<div class="content">
				<div  class="table-list">
					<div class="panel-body form-horizontal">
						<?php echo $this->Html->tag('div', "Ce menu de recherche retournera tous les flux <b>clos</b>.", array('class' => 'alert alert-info', 'style'=>'margin-bottom:0px;'));?>
					</div>

					<div class="content" style="padding-top: 30px;">
						<?php

						$formOutil = array(
							'name' => 'Outils',
							'label_w' => 'col-sm-2',
							'input_w' => 'col-sm-4',
							'input' => array(
								'Courrier.reference' => array(
									'inputType' => 'text',
									'labelText' => 'N° de référence',
									'items'=>array(
										'type' => 'text',
										'empty' => true
									)
								)
							)
						);
						echo $this->Formulaire->createForm($formOutil);
						?>
						<br />
						<div class="form-group "  id="SendPastellDocIsNotValid">
							<label for="SendPastellInforequiredTypeDocument" class="control-label col-sm-2"><?php echo __d('recherche', 'Recherche.cdatereception'); ?></label>
							<div class=" controls-input col-sm-2 ">
								<b style='color:red;'></b>
							</div>
						</div>
<!--						<legend style="margin-left:190px;border-bottom:0px;margin-top:5px;margin-bottom:4px;"><b>--><?php //echo __d('recherche', 'Recherche.cdatereception'); ?><!--</b></legend>-->

						<?php
							$formOutil2 = array(
								'name' => 'Outils',
								'label_w' => 'col-sm-2',
								'input_w' => 'col-sm-4',
								'title' => 'Test',
								'input' => array(
									'Courrier.datereceptiondebut' => array(
										'labelText' =>'Début',
										'inputType' => 'text',
										'dateInput' =>true,
										'items'=>array(
											'type' => 'text',
											'readonly' => true,
											'class' => 'datepicker',
											'data-format'=>'dd/MM/yyyy',
										)
									),
									'Courrier.datereceptionfin' => array(
										'labelText' =>'Fin',
										'inputType' => 'text',
										'dateInput' =>true,
										'items'=>array(
											'type' => 'text',
											'readonly' => true,
											'class' => 'datepicker',
											'data-format'=>'dd/MM/yyyy',
										)
									)
								)
							);

							echo $this->Formulaire->createForm($formOutil2);
							echo $this->Form->end();
						?>
					</div>
				</div>
			</div>
			<div class="controls panel-footer " role="group"></div>
		</div>
		<div class="row">
			<div class="table-list rechercheCondition" id="infos" style="padding-bottom: 40px;overflow: hidden;">
				<h3>Résultats</h3>
				<div class="content"></div>
				<div class="controls panel-footer  " role="group"></div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	'use strict';

	$(document).ready(function () {
		$('.form_datetime input').datepicker({
			language: 'fr-FR',
			format: "dd/mm/yyyy",
			weekStart: 1,
			autoclose: true,
			todayBtn: 'linked'
		});
	});
	// Ajout de l'action de recherche via la touche Entrée
	$('#OutilsSearchFluxClosForm').keypress(function (e) {
		if (e.keyCode == 13) {
			$('#liste .controls .searchBtn').trigger('click');
			return false;
		}
	});

	gui.buttonbox({
		element: $('#liste .controls'),
		align: 'center'
	});

	// Bouton  pour lancer la recherche
	gui.addbuttons({
		element: $('#liste .controls.panel-footer'),
		buttons: [
			{
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc searchBtn',
				action: function () {
					gui.request({
						url: "<?php echo Configure::read('BaseUrl') . "/outils/fluxClosASupprimer/"; ?>",
						data: $('#OutilsSearchFluxClosForm').serialize(),
						loader: true,
						updateElement: $('#infos .content'),
						loaderMessage: gui.loaderMessage
					});
					$('#liste').hide();
					$('#infos').show();
					$('#circuit').show();
				}
			}
		]
	});

	$('#infos').hide();
	$('#circuit').hide();
	$('.hide-search-formulaire').hide();


	// Si les résultats sont affichés, on affiche les actions possibles
	if ($('#infos .controls .btn').length == 0) {
		gui.buttonbox({
			element: $('#infos .controls'),
			align: "center"
		});

		// permet de masquer le formulaire de recherche, lorsqu'on le réouvre
		$('#liste h3').css('cursor', 'pointer');
		$('#liste h3').click(function () {
			$('#liste').hide();
		});

		// Bouton pour réouvrir le formulaire de recherche
		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
				title: "<?php echo __d('default', 'Button.search'); ?>",
				class: 'btn-info-webgfc',
				action: function () {
					$('#liste').show();
					$('.hide-search-formulaire').show();
				}
			}
		});

		gui.addbutton({
			element: $('#infos .controls'),
			button: {
				content: "<i class='fa fa-trash' ></i>  Supprimer le(s) flux",
				title: "<?php echo __d('courrier', 'Button.delete'); ?>",
				class: 'btn  btn-danger',
				action: function () {
					var tabChecked = new Array();

					$('.selected').each(function () {
						tabChecked.push($(this).find('.checkItem').attr('itemId'));
					});
					$('.checkItem').each(function () {
						if ($(this).prop('checked')) {
							tabChecked.push($(this).attr('itemId'));
						}
					});

					if (tabChecked.length == 0) {
						swal({
							showCloseButton: true,
							type: 'warning',
							title: 'Suppression de flux',
							text: 'Veuillez choisir au moins un flux',

						});
					} else {
						var form = "<form id='formChecked'>";
						for (var i in tabChecked) {
							form += '<input type="hidden" name="data[checkItem][]" value="' + tabChecked[i] + '">';
						}
						form += "</form>";

						swal({
							showCloseButton: true,
							title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
							text: "<?php echo __d('default', 'Voulez-vous supprimer ces éléments ?'); ?>",
							type: 'warning',
							showCancelButton: true,
							confirmButtonColor: '#d33',
							cancelButtonColor: '#3085d6',
							confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
							cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
						}).then(function (data) {
							if (data) {
								gui.request({
									url: "/outils/deleteenlot",
									data: $(form).serialize(),
									loader: true,
									loaderMessage: gui.loaderMessage
								}, function (data) {
									getJsonResponse(data);
									$(this).parents(".modal").modal('hide');
									$(this).parents(".modal").empty();
									gui.request({
										url: "<?php echo Configure::read('BaseUrl') . "/outils/fluxClosASupprimer/"; ?>",
										data: $('#OutilsSearchFluxClosForm').serialize(),
										loader: true,
										updateElement: $('#infos .content'),
										loaderMessage: gui.loaderMessage
									});

									processJsonFormCheck(data);
								});
							} else {
								swal({
									showCloseButton: true,
									title: "Annulé!",
									text: "Vous n'avez pas supprimé, ;) .",
									type: "error",

								});
							}
						});
						$('.swal2-cancel').css('margin-left', '-320px');
						$('.swal2-cancel').css('background-color', 'transparent');
						$('.swal2-cancel').css('color', '#5397a7');
						$('.swal2-cancel').css('border-color', '#3C7582');
						$('.swal2-cancel').css('border', 'solid 1px');

						$('.swal2-cancel').hover(function () {
							$('.swal2-cancel').css('background-color', '#5397a7');
							$('.swal2-cancel').css('color', 'white');
							$('.swal2-cancel').css('border-color', '#5397a7');
						}, function () {
							$('.swal2-cancel').css('background-color', 'transparent');
							$('.swal2-cancel').css('color', '#5397a7');
							$('.swal2-cancel').css('border-color', '#3C7582');
						});
					}
				}
			}
		});

	}

	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/outils"; ?>";
				}
			}
		]
	});

</script>

