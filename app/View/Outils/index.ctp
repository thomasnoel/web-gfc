
<div id="backButton" style="margin-bottom: 10px;"></div>
<div class="col-sm-6 ">
	<div class="table-list row superadmin-table-list">
		<h3><?php echo __d('menu', "Outils pour l'administrateur"); ?></h3>
		<div class="group-btn-admin">
			<?php if($hasParapheur) :?>
				<a href="/outils/searchParapheur" class="btn btn-success btn-block" role="button"><?php echo __d('menu', " Rechercher un flux lié au i-Parapheur"); ?></a>
			<?php endif;?>
			<a href="/outils/searchFluxnonclos" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Flux non clos des agents non actifs"); ?></a>
			<a href="/outils/searchFluxEncoursBannetteAInserer" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Flux toujours dans les bannettes à insérer alors qu'ils sont insérés dans un circuit"); ?></a>
			<a href="/outils/searchFluxClos" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Suppression de flux clos"); ?></a>
		</div>
	</div>
</div>

<script type="text/javascript">
	gui.addbuttons({
		element: $('#backButton'),
		buttons: [
			{
				content: "<i class='fa fa-arrow-left' aria-hidden='true'></i> <?php echo __d('default', 'Button.back'); ?>",
				class: "btn-info-webgfc",
				action: function () {
					window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
				}
			}
		]
	});
</script>
