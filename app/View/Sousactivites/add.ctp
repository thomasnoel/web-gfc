<?php

/**
 *
 * Sousactivites/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="col-sm-12 zone-form">
    <legend class="panel-title"><?php echo __d('sousactivite', 'Sousactivite.add'); ?></legend>
    <div class="panel-body">
            <?php
            $formSousactivite = array(
                'name' => 'Sousactivite',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-6',
                'input' => array(
                    'Sousactivite.name' => array(
                        'labelText' =>__d('sousactivite', 'Sousactivite.name'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Sousactivite.description' => array(
                        'labelText' =>__d('sousactivite', 'Sousactivite.description'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    )
                )
            );
            echo $this->Formulaire->createForm($formSousactivite);
            echo $this->Form->end();
            ?>
    </div>
    <div class="controls text-center" role="group">
        <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">
	$('div.panel-footer').hide();
    $('#valid').click(function () {
        var url = "<?php echo Router::url(array('controller' => 'sousactivites', 'action' => 'add', $activiteId)); ?>";
        if (form_validate($('#SousactiviteAddForm'))) {
            gui.request({
                url: url,
                data: $('#SousactiviteAddForm').serialize(),
                loaderElement: $('#infos .content'),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                loadValues();
                loadActivite();
            });
			$('div.panel-footer').show();
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });
    $('#cancel').click(function () {
        loadValues();
        loadActivite();
    });
</script>
