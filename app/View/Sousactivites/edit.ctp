<?php

/**
 *
 * Sousactivites/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="col-sm-12 zone-form">
    <legend class="panel-title"><?php echo __d('sousactivite', 'Sousactivite.edit'); ?></legend>
    <div class="panel-body">
            <?php
            $formSousactivite = array(
                'name' => 'Sousactivite',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-6',
                'input' => array(
                    'Sousactivite.id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden'
                        )
                    ),
                    'Sousactivite.name' => array(
                        'labelText' =>__d('sousactivite', 'Sousactivite.name'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Sousactivite.description' => array(
                        'labelText' =>__d('sousactivite', 'Sousactivite.description'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    )
                )
            );
            echo $this->Formulaire->createForm($formSousactivite);
            echo $this->Form->end();
            ?>
    </div>
    <div class="controls text-center" role="group">
        <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">

    $('#valid').click(function () {
        var form = $(this).parents('.modal').find('form');
        if (form_validate(form)) {
            gui.request({
                url: form.attr('action'),
                data: form.serialize()
            }, function () {
                loadValues();
                loadActivite();
            });
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });
    $('#cancel').click(function () {
        loadValues();
        loadActivite();
    });
</script>
