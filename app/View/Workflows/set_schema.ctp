<?php

/**
 *
 * Workflows/set_schema.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script>
    function set_comment_description_height(etapid, heightComment) {
        $("#comment_description_" + etapid).css('height', heightComment);
        $("#test_" + etapid).css('height', heightComment);
    }
</script>
<?php
    echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
if (empty($etapes)) {
    echo '<div class="alert alert-warning">'.__d('circuit', 'Etape.voidDefinie').'</div>';
} else {
    $typesEtape = array(
        ETAPESIMPLE => 'Simple',
        ETAPEET => 'Collaborative',
        ETAPEOU => 'Concurrente'
    );
?>
<legend><?php echo $circuit['Circuit']['nom']; ?></legend>
<a href="#" class="addEtape btn btn-info-webgfc"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d('circuit', 'New Etape'); ?></a>
<table class="show-circuit">
    <thead ><th class="upDown"></th><th class="etapeInfo etapeInfoPourSetSchema"></th><th class="arrow arrowPourSetSchema"></th><th class="etapeDescription etapeDescriptionPourSetSchema"></th><th class="actions"></th></thead>
<tbody>
    <?php

        $editCompo = '<i class="fa fa-pencil table" id=editCompo name=editCompo title="Modifier la composition"></i>';
//debug($editCompo);
    foreach ($etapes as $etape) {
        $ulCompoContent = array();
        foreach ($etape['Composition'] as $composition) {
            $delCompo = $this->Html->image('/img/trash.png', array('title' => __d('circuit', 'Delete Compo'), 'class' => 'removeCompo bttn', 'itemId' => $composition['id']));

            $editCompo = '<i class="fa fa-pencil table" id=editCompo name=editCompo title="Modifier la composition"></i>';
            $compositionName = '';
            if( $composition['trigger_id'] == '-1') {
                if( !empty($soustypes) ) {
                    $sousTypeSelectionne = $soustypes[$composition['soustype']];
                    if( !empty( $sousTypeSelectionne )) {
                        $compositionName = 'Etape Parapheur : '.$sousTypeSelectionne;
                    }
                    else {
						$compositionName = "<i style='color:red;'>Etape Parapheur : La valeur du sous-type sélectionné n'existe pas dans le Parapheur cible</i>";
                    }
                }
                else {
                    $compositionName = '<i style="color:red;">Etape Parapheur : Connecteur Parapheur en erreur</i>';
                }
            }
            else if( $composition['trigger_id'] == '-3') {
				if( !empty($documents) ) {
					$listeDocumentSelectionne = $documents[$composition['type_document']];
					if( !empty( $listeDocumentSelectionne )) {
						$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne;
						if( $composition['type_document'] == 'document-a-signer' ) {
							$typeSelectionne = $composition['inforequired_type_document'];
							$compositionName = 'Etape Pastell : '.$listeDocumentSelectionne.' : '.$typeSelectionne;
						}
					}
					else {
						$compositionName = 'Etape Pastell';
					}
				}
				else {
					$compositionName = '<i style="color:red;">Etape Pastell : Connecteur Pastell en erreur</i>';
				}
			}
            else {
                $compositionName = $composition[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS];
            }
        ?>

        <?php
            $ulCompoContent[] = $delCompo . $this->Html->tag('div', $compositionName, array('class' => 'desktop_name'));
        }

        $userEnough=array();
        $userEnough=$this->requestAction('Workflows/userEnough/'.$etape['Etape']['id']);
        if($userEnough[0]!=true){
            foreach($userEnough[1] as $falseEtapeId){
                if($falseEtapeId==$etape['Etape']['id']){
                    $userNotEnought =true;
                    $panelClassEtape = ' panel-danger';
                    $descriptionEtapeTitle =  __d('circuit', 'Not Enough User').$this->Html->tag('input','',array('type' => 'hidden','id'=>'noenoughUser','value'=>'noenoughUser'));
                    $arrowDirectionColor = '#A94464';
                }
            }
        }else{
            $userNotEnought =false;
            $panelClassEtape = ' panel-default';
            $descriptionEtapeTitle = '';
            $arrowDirectionColor = '#b2b2b2';
        }

    ?>
    <tr class="etape">
        <!--bouton up & down-->
        <td class="upDown">
            <?php if ($etape != $etapes[0]): ?>
            <i class="fa fa-arrow-up move_up bttn" itemId="<?php echo $etape['Etape']['id'];?>"></i>
            <?php endif; ?>
            <br />
            <?php if ($etape != $etapes[count($etapes) - 1]): ?>
            <i class="fa fa-arrow-down move_down bttn" itemId="<?php echo $etape['Etape']['id'];?>"></i>
            <?php endif; ?>
        </td>
        <!--etape-->
        <td  class="etapeInfo">
            <div class="panel <?php echo $panelClassEtape; ?>" id="comment_<?php echo $etape['Etape']['id']; ?>">
                <div class="panel-heading">
                    <h4 class="panel-title"><?php echo $etape['Etape']['ordre']; ?><span ><?php echo $typesEtape[$etape['Etape']['type']]; ?></span></h4>
                </div>
                <div class="panel-body">
                    <legend><?php echo $etape['Etape']['nom']; ?></legend>
                    <ul>
                        <?php foreach ($ulCompoContent as $composition): ?>
                        <li><?php echo $composition; ?></li>
                        <?php endforeach;?>
                        <?php if($userNotEnought): ?>
                        <li class="alert alert-danger"><?php echo $descriptionEtapeTitle; ?></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </td>
        <!--arrow-down-->
        <td class="arrow" style="color:<?php echo $arrowDirectionColor; ?>;"><div><div style=" background:<?php echo $arrowDirectionColor; ?>;" id="test_<?php echo $etape['Etape']['id']; ?>"></div><i class="fa fa-long-arrow-down" aria-hidden="true" ></i></div></td>
        <!--description du comment-->
        <td class="etapeDescription">
            <div class="panel panel-default " style="border-color:<?php echo $arrowDirectionColor; ?>;" id="comment_description_<?php echo $etape['Etape']['id']; ?>">
                <div class="panel-body">
                    <legend>Description: </legend>
                    <p><?php echo $etape['Etape']['description']; ?></p>
                </div>
            </div>
        </td>
        <!--actions -->
        <td class="actions">
            <?php if ($etape['canAdd']): ?>
            <i class="addCompoBtn bttn fa fa-user-plus" itemId="<?php echo $etape['Etape']['id']; ?>" title="<?php echo  __d('circuit', 'New Compo'); ?>"></i>
            <?php endif; ?>
            <i class="editEtapeBtn bttn fa fa-pencil" itemId="<?php echo $etape['Etape']['id']; ?>" title="<?php echo  __d('circuit', 'Edit Etape'); ?>"></i>
            <?php if($etape['isDeletable'] === true): ?>
            <i class="deleteEtapeBtn bttn fa fa-trash" itemId="<?php echo $etape['Etape']['id']; ?>" title="<?php echo  __d('circuit', 'Delete Etape'); ?>"></i>
            <?php endif; ?>
        </td>
    </tr>
<script>


    $('<?php echo $editCompo; ?>').css({height: '20px', width: '20px', color:'#5397a7'}).appendTo($('.desktop_name').parent()).css("cursor", "pointer").css("margin", "-22px -22px").css("float", "right").click(function () {
        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            var itemId = $(this).parent().children().attr('itemid');
            gui.request({
                url: "/cakeflow/Compositions/edit/" + itemId,
                updateElement: $('#circuit_tabs_3'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        }
    });
    var etapid = "<?php echo $etape['Etape']['id']; ?>";
    $(window).resize(function () {
        var heightComment = $("#comment_" + etapid).height();
        set_comment_description_height(etapid, heightComment);
    });

    var heightComment = $("#comment_" + etapid).height();
    set_comment_description_height(etapid, heightComment);

</script>

    <?php
    }//endforeach
    ?>
</tbody>
</table>
<?php
    }
?>
<a href="#" class="addEtape btn btn-info-webgfc"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?php echo __d('circuit', 'New Etape'); ?></a>
<script type="text/javascript">

<?php if( !Configure::read('Conf.SAERP') ):?>
    var noenoughUser = $('#noenoughUser').val();
    if (noenoughUser == 'noenoughUser') {
        gui.disablebutton({
            element: $('#infos .controls'),
            button: "<?php echo __d('default', 'Button.submit'); ?>"
        });
    } else {
        gui.enablebutton({
            element: $('#infos .controls'),
            button: "<?php echo __d('default', 'Button.submit'); ?>"
        });
    }
<?php else:?>
    gui.enablebutton({
        element: $('#infos .controls'),
        button: "<?php echo __d('default', 'Button.submit'); ?>"
    });
<?php endif;?>

    function refreshSchema() {
        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: "<?php echo "/workflows/setSchema/" . $circuit['Circuit']['id']; ?>",
                updateElement: $('#circuit_tabs_3'),
            });
        }
    }

    $('.move_up').button().click(function () {
        var itemId = $(this).attr('itemId');
        gui.request({
            url: "/workflows/moveEtape/" + itemId + "/up",
            loader: true,
            updateElement: $('#circuit_tabs')
        }, function () {
            refreshSchema();
        });
    });

    $('.move_down').button().click(function () {
        var itemId = $(this).attr('itemId');
        gui.request({
            url: "/workflows/moveEtape/" + itemId + "/down",
            loader: true,
            updateElement: $('#circuit_tabs')
        }, function () {
            refreshSchema();
        });
    });

    $('.addEtape').button().click(function () {
        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: '/cakeflow/Etapes/add/' + <?php echo $circuit['Circuit']['id']; ?>,
                updateElement: $('#circuit_tabs_3'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        }
    });

    $('.editEtapeBtn').button().click(function () {
        var itemId = $(this).attr('itemId');

        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: "/cakeflow/Etapes/edit/" + itemId,
                updateElement: $('#circuit_tabs_3'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        }
    });


    $('.deleteEtapeBtn').button().click(function () {
        var itemId = $(this).attr('itemId');

        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/cakeflow/Etapes/delete/" + itemId
                }, function () {
                    refreshSchema();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });


    });

    $('.addCompoBtn').button().click(function () {
        var itemId = $(this).attr('itemId');

        if ($('#circuit_tabs_3').length > 0 && $('#circuit_tabs_3').is(':visible')) {
            gui.request({
                url: "/cakeflow/Compositions/add/" + itemId,
                updateElement: $('#circuit_tabs_3'),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
        }
    });

    $('.removeCompo').button().click(function () {
        var itemId = $(this).attr('itemId');
        swal({
                    showCloseButton: true,
            title: "<?php echo __d('default', 'Modal.suppressionTitle'); ?>",
            text: "<?php echo __d('default', 'Modal.suppressionContents'); ?>",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: "<?php echo __d('default', 'Button.delete'); ?>",
            cancelButtonText: "<?php echo __d('default', 'Button.cancel'); ?>",
        }).then(function (data) {
            if (data) {
                gui.request({
                    url: "/cakeflow/Compositions/delete/" + itemId
                }, function () {
                    refreshSchema();
                });
            } else {
                swal({
                    showCloseButton: true,
                    title: "Annulé!",
                    text: "Vous n'avez pas supprimé, ;) .",
                    type: "error",

                });
            }
        });
        $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });

    });


</script>
