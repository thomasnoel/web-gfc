<?php

/**
 *
 * Users/set_infos_persos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));


    // Bloc pour l'identité de l'utilisateur
    $formUser = array(
        'name' => 'User',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-7',
        'input' => array(
            'User.id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            ),
            'User.username' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' => $username
                )
            ),
            'User.nom' => array(
                'labelText' =>__d('user', 'User.nom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => true
                )
            ),
            'User.prenom' => array(
                'labelText' =>__d('user', 'User.prenom'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text',
                    'required' => true
                )
            ),
            'User.mail' => array(
                'labelText' =>'<i class="fa fa-envelope" aria-hidden="true"></i>',
                'inputType' => 'email',
                'items'=>array(
                    'type'=>'email',
                    'required' => true
                )
            ),
            'User.numtel' => array(
                'labelText' =>'<i class="fa fa-phone" aria-hidden="true"></i>',
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );

    // Bloc pour le changement de mot de passe
    $formUserMdp = array(
        'name' => 'UserSetMdp',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-7',
        'form_url' => array('controller' => 'users', 'action' => 'setInfosPersos', $this->data['User']['id']),
        'input' => array(
            'User.oldpassword' => array(
                'labelText' =>__d('user', 'User.oldpassword'),
                'inputType' => 'password',
                'items'=>array(
                    'required'=>true,
                    'type'=>'password'
                )
            ),
            'User.password' => array(
                'labelText' =>__d('user', 'User.password'),
                'inputType' => 'password',
                'items'=>array(
                    'required'=>true,
                    'type'=>'password'
                )
            ),
            'User.password2' => array(
                'labelText' =>__d('user', 'User.password_confirm'),
                'inputType' => 'password',
                'items'=>array(
                    'type'=>'password'
                )
            ),
            'User.pwdmodified' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden',
                    'value' => 'false'
                )
            )
        )
    );
    if($this->data['User']['desktop_id'] != null) {
    //    if(isset($this->request->data['User']['accept_notif'])){
        // Blocs pour l'abonnement aux notifications
        $formUserNotification = array(
            'name' => 'UserSetNotif',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-1',
            'form_url' => array('controller' => 'users', 'action' => 'setInfosPersos', $this->data['User']['id']),
            'input' => array(
                'User.accept_notif' => array(
                    'labelText' =>__d('user', 'User.accept_notif'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['accept_notif']
                    )
                )
            )
        );
        $formUserNotificationDetail = array(
            'name' => 'UserSetNotif',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-1',
            'input' => array(
                'User.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'User.mail_insertion' => array(
                    'labelText' =>__d('user', 'User.mail_insertion'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_insertion']
                    )
                ),
                'User.mail_traitement' => array(
                    'labelText' =>__d('user', 'User.mail_traitement'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_traitement']
                    )
                ),
                'User.mail_refus' => array(
                    'labelText' =>__d('user', 'User.mail_refus'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_refus']
                    )
                ),
                'User.mail_retard_validation' => array(
                    'labelText' =>__d('user', 'User.mail_retard_validation'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_retard_validation']
                    )
                ),
                'User.mail_copy' => array(
                    'labelText' =>__d('user', 'User.mail_copy'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_copy']
                    )
                ),
                'User.mail_insertion_reponse' => array(
                    'labelText' =>__d('user', 'User.mail_insertion_reponse'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_insertion_reponse']
                    )
                ),
                'User.mail_aiguillage' => array(
                    'labelText' =>__d('user', 'User.mail_aiguillage'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_aiguillage']
                    )
                ),
                'User.mail_delegation' => array(
                    'labelText' =>__d('user', 'User.mail_delegation'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['User']['mail_delegation']
                    )
                ),
                'User.mail_clos' => array(
                    'labelText' =>__d('user', 'User.mail_clos'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>!empty($this->request->data['User']['mail_clos'])? $this->request->data['User']['mail_clos']:false
                    )
                ),
                'User.typeabonnement' => array(
                    'labelText' =>__d('user', 'User.typeabonnement'),
                    'inputType' => 'select',
                    'changeWidth' =>true,
                    'items'=>array(
                        'type'=>'select',
                        'options'=> $typeabonnement,
                        'empty' => false,
                        'new_label_w' => 'col-sm-6',
                        'new_input_w' => 'col-sm-2',
                    )
                )
            )
        );
    }
?>
<div class="container">
    <div class="row">
        <div class="container-formulaire">
            <h3><?php echo __d('menu', 'Informations personnelles'); ?></h3>
            <div class="row">
                <div class="panel col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo __d('user','User.user'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal">
                        <?php
                            echo $this->Formulaire->createForm($formUser);
                            echo $this->Form->end();
                        ?>
                        </div>
                        <div class="panel-footer " role="group" id="chInfo">
                        </div>
                    </div>
                </div>
                <div class="panel col-sm-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title"><?php echo __d('menu', 'Mot de passe'); ?></h4>
                        </div>
                        <div class="panel-body form-horizontal"  style="height:230px">
                        <?php

                            echo $this->Formulaire->createForm($formUserMdp);
                            echo $this->Form->end();

                        ?>
                        </div>
                        <div class="panel-footer " role="group"  id='chMdp'>
                        </div>
                    </div>
                </div>
                <?php if($this->data['User']['desktop_id'] != null ) :?>
                    <div class="panel col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title"><?php echo __d('user', 'Notification'); ?></h4>
                            </div>
                            <div class="panel-body form-horizontal">
                            <?php
                                echo $this->Formulaire->createForm($formUserNotification);
                            ?>

                                <div id="UserOptionNotification" class="notification">
                                <?php

                                echo $this->Formulaire->createForm($formUserNotificationDetail);
                                ?>
                                </div>
                            <?php
                                echo $this->Form->end();
                            ?>
                            </div>
                            <div class="panel-footer " role="group" id="chNotif">
                            </div>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    // Valider / Annuler pour les info perso
    gui.buttonbox({
        element: $('#chInfo')
    });

    gui.addbuttons({
        element: $('#chInfo'),
        buttons: [
            {
                content: '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                title: 'Annuler',
                class: 'btn-danger-webgfc btn-inverse ',
                action: function () {
                    window.location.reload();
                }
            },
            {
                content: '<i class="fa fa-floppy-o" aria-hidden="true"></i>',
                title: 'Enregistrer',
                class: 'btn-success ',
                action: function () {
                    $('#UserSetInfosPersosForm').submit();
                }
            }
        ]
    });

    // Valider / Annuler pour le mot de passe
    gui.buttonbox({
        element: $('#chMdp')
    });
    gui.addbuttons({
        element: $('#chMdp'),
        buttons: [
            {
                content: '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                title: 'Annuler',
                class: 'btn-danger-webgfc btn-inverse ',
                action: function () {
                    window.location.reload();
                }
            },
            {
                content: '<i class="fa fa-floppy-o" aria-hidden="true"></i>',
                title: 'Enregistrer',
                class: 'btn-success ',
                action: function () {
                    $('#UserSetMdpSetInfosPersosForm').submit();
                }
            }
        ]
    });

    // Valider / Annuler pour les notifications
    gui.buttonbox({
        element: $('#chNotif')
    });

    gui.addbuttons({
        element: $('#chNotif'),
        buttons: [
            {
                content: '<i class="fa fa-times-circle-o" aria-hidden="true"></i>',
                title: 'Annuler',
                class: 'btn-danger-webgfc btn-inverse ',
                action: function () {
                    window.location.reload();
                }
            },
            {
                content: '<i class="fa fa-floppy-o" aria-hidden="true"></i>',
                title: 'Enregistrer',
                class: 'btn-success ',
                action: function () {
                    $('#UserSetNotifSetInfosPersosForm').submit();
                }
            }
        ]
    });


    $(document).ready(function () {

        $('#UserMail').blur(function () {
            gui.request({
                url: "/users/ajaxformvalid/mail",
                data: $('#UserSetInfosPersosForm').serialize()
            }, function (data) {
                processJsonFormCheck(data);
            })
        });


        // Affichage des notifications
        $('#UserAcceptNotif').change(function () {
            if ($('#UserAcceptNotif').prop('checked')) {
                $('#UserOptionNotification').show();
            }
            else {
                $('#UserOptionNotification').hide();
//                $('#UserOptionNotification input').prop("checked", false);
            }
        });
        if ($('#UserAcceptNotif').prop('checked')) {
            $('#UserOptionNotification').show();
        }
        else {
            $('#UserOptionNotification').hide();
        }
    });
</script>
