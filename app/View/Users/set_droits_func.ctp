<?php

/**
 *
 * Users/set_droits_func.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$this->Droits->printTabAcces($droitsfunc, 'Liste des fonctionnalités');
$formUserFonctionRights = array(
    'name' => 'UserFonctionRights',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'form_url'=>array('controller' => 'users', 'action' => 'setDroitsMeta', $id),
    'input' => array(
        'User.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'User.rights' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden',
                'value'=>'action'
            )
        )
    )
);
echo $this->Formulaire->createForm($formUserFonctionRights);
echo $this->Form->end();
?>
