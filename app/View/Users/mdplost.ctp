<?php

/**
 *
 * Users/mdplost.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>

<div class="col-xs-10
     col-sm-7
     col-md-6
     col-lg-5
     center-block login-block">

    <div class="form-block">

        <img class="row center-block main-logo"
             src="/img/web-gfc_color_h48px.png">

        <div class="row buffer-top text-center">
            <?php echo __d( 'mdplost', 'Mdplost.lost'); ?>
        </div>

        <form class="row double-buffer-top" role="form" action="" method="post" class="login-form" id="UserMdplostForm" data-toggle="validator">
            <div class="form-horizontal form-group">

                <label for="login"
                       class="col-md-4
                       control-label normal-left"><?php echo __d('mdplost', 'User.username'); ?> *</label>  <!-- font-family: Arial, serif; -->

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-login">
                        <i class="fa fa-user fa-fw"
                           aria-hidden="true"></i>
                    </span>
                    <input type="text"
                           class="form-control"
                           name="data[User][username]"
                           id="UserUsername"
                           required="required"
                           aria-describedby="sizing-addon-login">
                </div>
            </div>

            <div class="form-horizontal form-group" id="form_group_password">
                <label for="password"
                       class="col-md-4 control-label normal-left"><?php echo __d('mdplost', 'User.mail'); ?> *</label>  <!-- font-family: Arial, serif; -->

                <div class="col-md-8 input-group">
                    <span class="input-group-addon color-inverse"
                          id="sizing-addon-password">
                        <i class="fa fa-envelope fa-fw"
                           aria-hidden="true"></i>
                    </span>
                    <input type="email"
                           class="form-control"
                           name="data[User][mail]"
                           id="UserMail"
                           required="required"
                           aria-describedby="sizing-addon-password">
                </div>
            </div>
        </form>

        <div class="row form-horizontal form-group buffer-top" id="mdplostButtonBox">
        </div>

        </form>

        <div class="row buffer-top alert-info">
            <?php echo __d('mdplost', 'Mdplost.beforesave'); ?>
            <!--<a href="#">Mot de passe oublié ?</a>-->
        </div>

    </div>
</div>

<script type="text/javascript">

//    $('#UserMdplostForm').validate({
//        'rules': {
//            'data[User][username]': 'required',
//            'data[User][mail]': 'required'
//        }
//    });

    gui.buttonbox({
        element: $('#mdplostButtonBox')
    });
    gui.addbutton({
        element: $('#mdplostButtonBox'),
        button: {
            content: '<i class="fa fa-times-circle" aria-hidden="true"></i> Annuler',
            class: "pull-right btn btn-danger",
            action: function () {
                window.location.href = "/users/login";
            }
        }
    });


    gui.addbutton({
        element: $('#mdplostButtonBox'),
        button:
                {
                    content: '<i class="fa fa-paper-plane" aria-hidden="true"></i> <?php echo __d('default', 'Button.mdplost'); ?>',
                    class: "pull-right btn btn-primary",
                    action: function () {
                        if (form_validate($("#UserMdplostForm"))) {
                            $('#UserMdplostForm').submit();
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }
                    }
                }
    });

    $('#UserMdplostForm').keyup(function (e) {
        if (e.keyCode == 13) {
            if (form_validate($("#UserMdplostForm"))) {
                $('#UserMdplostForm').submit();
            } else {
                swal({
                    showCloseButton: true,
                    title: "Oops...",
                    text: "Veuillez vérifier votre formulaire!",
                    type: "error",

                });
            }
        }
    });
    $("#button_cancelEnvoyerMail").click(function () {
        window.location.href = "/users/login";
    });

</script>
