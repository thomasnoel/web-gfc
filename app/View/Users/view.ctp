<?php
/**
 *
 * Users/view.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->css(array('cake.generic'), null, array('inline' => false));
?>
<div id="user_form_id" class="elementBordered">
	<h2><?php __('User'); ?></h2>
	<ul class="actions">
		<li><?php echo $this->Html->link(__('Edit User', true), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('Delete User', true), array('action' => 'delete', $user['User']['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('New User', true), array('action' => 'add')); ?> </li>
	</ul>

	<dl><?php $i = 0;
$class = ' class="altrow"'; ?>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'User.id'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['id']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'Login'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['username']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'User.password'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['password']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'Nom'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['nom']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'Prenom'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['prenom']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'Profile'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $this->Html->link($user['Profil']['name'], array('controller' => 'profils', 'action' => 'view', $user['Profil']['id'])); ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'Date de Création'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['created']; ?>
			&nbsp;
		</dd>
		<dt<?php if ($i % 2 == 0) echo $class; ?>><?php echo __d('user', 'Date de Modification'); ?></dt>
		<dd<?php if ($i++ % 2 == 0) echo $class; ?>>
			<?php echo $user['User']['modified']; ?>
			&nbsp;
		</dd>
	</dl>

	<div class="related">
		<h3><?php __('Related Services'); ?></h3>
		<?php if (!empty($user['Service'])): ?>
			<table cellpadding = "0" cellspacing = "0">
				<tr>
					<th><?php __('Id'); ?></th>
					<th><?php __('Name'); ?></th>
					<th><?php __('Collectivite Id'); ?></th>
					<th><?php __('Created'); ?></th>
					<th class="actions"><?php echo __d('default', 'Actions'); ?></th>
				</tr>
				<?php
				$i = 0;
				foreach ($user['Service'] as $service):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class; ?>>
						<td><?php echo $service['id']; ?></td>
						<td><?php echo $service['name']; ?></td>
						<td><?php echo $service['created']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('View', true), array('controller' => 'services', 'action' => 'view', $service['id'])); ?>
							<?php echo $this->Html->link(__('Edit', true), array('controller' => 'services', 'action' => 'edit', $service['id'])); ?>
							<?php echo $this->Html->link(__('Delete', true), array('controller' => 'services', 'action' => 'delete', $service['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $service['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>

		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Service', true), array('controller' => 'services', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>

	<div class="related">
		<h3><?php __('Related Circuits'); ?></h3>
		<?php if (!empty($user['Circuit'])): ?>
			<table cellpadding = "0" cellspacing = "0">
				<tr>
					<th><?php __('Id'); ?></th>
					<th><?php __('Soustype Id'); ?></th>
					<th><?php __('Created'); ?></th>
					<th><?php __('Modified'); ?></th>
					<th class="actions"><?php echo __d('default', 'Actions'); ?></th>
				</tr>
				<?php
				$i = 0;
				foreach ($user['Circuit'] as $circuit):
					$class = null;
					if ($i++ % 2 == 0) {
						$class = ' class="altrow"';
					}
					?>
					<tr<?php echo $class; ?>>
						<td><?php echo $circuit['id']; ?></td>
						<td><?php echo $circuit['soustype_id']; ?></td>
						<td><?php echo $circuit['created']; ?></td>
						<td><?php echo $circuit['modified']; ?></td>
						<td class="actions">
							<?php echo $this->Html->link(__('View', true), array('controller' => 'circuits', 'action' => 'view', $circuit['id'])); ?>
							<?php echo $this->Html->link(__('Edit', true), array('controller' => 'circuits', 'action' => 'edit', $circuit['id'])); ?>
							<?php echo $this->Html->link(__('Delete', true), array('controller' => 'circuits', 'action' => 'delete', $circuit['id']), null, sprintf(__('Are you sure you want to delete # %s?', true), $circuit['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
			</table>
		<?php endif; ?>

		<div class="actions">
			<ul>
				<li><?php echo $this->Html->link(__('New Circuit', true), array('controller' => 'circuits', 'action' => 'add')); ?> </li>
			</ul>
		</div>
	</div>
</div>
