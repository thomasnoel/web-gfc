<?php

/**
 *
 * Users/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formUserAdd = array(
    'name' => 'User',
    'div_w' => 'col-sm-6',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array()
);

$formUserUserContent = array(
    'name' => 'User',
    'div_w' => 'col-sm-6',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'User.username' => array(
            'labelText' =>__d('user', 'User.username'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'User.password' => array(
            'labelText' =>__d('user', 'User.password'),
            'inputType' => 'password',
            'items'=>array(
                'required'=>true,
                'type'=>'password'
            )
        ),
        'User.nom' => array(
            'labelText' =>__d('user', 'User.nom'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'User.prenom' => array(
            'labelText' =>__d('user', 'User.prenom'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'User.mail' => array(
            'labelText' =>__d('user', 'User.mail'),
            'inputType' => 'email',
            'items'=>array(
                'required'=>true,
                'type'=>'email'
            )
        ),
        'User.numtel' => array(
            'labelText' =>__d('user', 'User.numtel'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'User.addressbook_private' => array(
            'labelText' =>__d('user', 'User.addressbook_private'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox'
            )
        )
    )
);

$formUserDesktopContent = array(
    'name' => 'User',
    'div_w' => 'col-sm-6',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-7',
    'input' => array(
        'Desktop.profil_id' => array(
            'labelText' => 'Profil',
            'inputType' => 'select',
            'items'=>array(
                'required'=>true,
                'options'=> $groups,
                'type'=>'select',
                'escape'=>false,
                'empty' => true
            )
        ),
        'Desktop.name' => array(
            'labelText' =>__d('desktop', 'Desktop.name'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text',
                'required'=>true
            )
        )
	)
);

$formUserDesktopServiceContent = array(
	'name' => 'User',
	'div_w' => 'col-sm-6',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-7',
	'input' => array(
		'Desktop.Service.Service' => array(
			'labelText' =>'Service(s)',
			'inputType' => 'select',
			'items'=>array(
				'type' => 'select',
				'options' => $services,
//                'escape' => false,
				'multiple' => true,
				'required'=>true
			)
		)
	)
);

$formUserNotificationContent = array(
    'name' => 'User',
    'div_w' => 'col-sm-12',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-3',
    'input' =>array(
        'User.accept_notif' => array(
            'labelText' =>__d('user', 'User.accept_notif'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        )
    )
);

$formUserOptionNotificationContent = array(
    'name' => 'User',
    'div_w' => 'col-sm-6',
    'label_w' => 'col-sm-7',
    'input_w' => 'col-sm-5',
    'input' =>array(
        'User.mail_insertion' => array(
            'labelText' =>__d('user', 'User.mail_insertion'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_traitement' => array(
            'labelText' =>__d('user', 'User.mail_traitement'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_refus' => array(
            'labelText' =>__d('user', 'User.mail_refus'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_retard_validation' => array(
            'labelText' =>__d('user', 'User.mail_retard_validation'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_copy' => array(
            'labelText' =>__d('user', 'User.mail_copy'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_insertion_reponse' => array(
            'labelText' =>__d('user', 'User.mail_insertion_reponse'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_aiguillage' => array(
            'labelText' =>__d('user', 'User.mail_aiguillage'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_delegation' => array(
            'labelText' =>__d('user', 'User.mail_delegation'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
            )
        ),
        'User.mail_clos' => array(
            'labelText' =>__d('user', 'User.mail_clos'),
            'inputType' => 'checkbox',
            'items'=>array(
                'type'=>'checkbox',
                'checked'=>!empty($this->request->data['User']['mail_clos'])? $this->request->data['User']['mail_clos']:false
            )
        )
    )
);
$formUserAbonnement = array(
    'name' => 'User',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-4',
    'input' => array(
        'User.typeabonnement' => array(
            'labelText' =>__d('user', 'User.typeabonnement'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options'=> $typeabonnement,
                'empty' => false
            )
        )
    )
);


?>
<fieldset>
    <?php echo $this->Formulaire->createForm($formUserAdd); ?>
    <fieldset class="form-group col-sm-12">
        <legend>Utilisateur</legend>
        <?php echo $this->Formulaire->createForm($formUserUserContent); ?>
    </fieldset>
    <fieldset class="form-group col-sm-12">
        <legend><?php echo __d('desktop', 'Desktop.priDesktop'); ?></legend>
        <?php echo $this->Formulaire->createForm($formUserDesktopContent); ?>
    </fieldset >
	<fieldset class="form-group col-sm-12">
        <?php echo $this->Formulaire->createForm($formUserDesktopServiceContent); ?>
    </fieldset >
    <fieldset class="form-group col-sm-12">
        <legend><?php echo __d('user', 'User.notification'); ?></legend>
        <?php echo $this->Formulaire->createForm($formUserNotificationContent); ?>
    </fieldset>
    <fieldset id="UserOptionNotification" class="notification form-group col-sm-12">
        <legend><?php echo __d('user', 'User.optionnotif'); ?></legend>
        <?php echo $this->Formulaire->createForm($formUserOptionNotificationContent); ?>
        <hr class="col-sm-12">
        <?php echo $this->Formulaire->createForm($formUserAbonnement); ?>
    </fieldset>
</fieldset>


<?php
echo $this->Form->end();
?>


<script type="text/javascript">

    function checkDesktopNameUnique() {
        gui.request({
            url: "/users/ajaxformvalid/desktopname",
            data: $('#UserAddForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    }
    $('#DesktopProfilId').change(function () {
        var str = "";
        $("#DesktopProfilId option:selected").each(function () {
            str += $(this).text() + " ";
        });
        var nom = $('#UserAddForm #UserNom').val();
        var prenom = $('#UserAddForm #UserPrenom').val();
        $('#DesktopName').val(str + prenom + ' ' + nom);
    });

    $('#DesktopProfilId').blur(function () {
        if ($('#DesktopName').val() == '' && $('#UserNom').val() != '' && $('#UserPrenom').val() != '' && $('#DesktopProfilId').val() != '') {
            $('#DesktopName').val('<?php echo __d('desktop', 'Desktop.Desktop'); ?> ' + $('#DesktopProfilId option:selected').html() + ' ' + $('#UserNom').val() + ' ' + $('#UserPrenom').val());
            checkDesktopNameUnique();
        }
    });

    $("input[name='data[User][username]']").blur(function () {
        gui.request({
            url: "/users/ajaxformvalid/username",
            data: $('#UserAddForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    });

    $(document).ready(function () {
        $('#DesktopServiceService').select2({allowClear: true, placeholder: "<?php echo __d('user', 'User.Service.Service');?>"});
    });

    // Affichage des notifications
    $('#UserOptionNotification').hide();
    $('#UserAcceptNotif').change(function () {
        if ($('#UserAcceptNotif').prop("checked")) {
            $('#UserOptionNotification').show();
        }
        else {
            $('#UserOptionNotification').hide();
        }
    });
    $('#DesktopProfilId').select2({allowClear: true, placeholder: "Sélectionner un groupe"});
</script>
