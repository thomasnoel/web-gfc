<?php

/**
 *
 * Users/changemdp.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="panel panel-default" id="infosPass">
    <div class="panel-heading">
        <h4 class="panel-title"><?php echo __d('user', 'Change password'); ?></h4>
    </div>
    <div class="panel-body">
        <?php
        if ($this->Session->check( 'Message.flash' ) ) {
            echo $this->Session->flash();
        }
        if ($this->Session->check( 'Message.auth' ) ) {
            echo $this->Session->flash( 'auth' );
        }
        $formUser = array(
            'name' => 'User',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'User.id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            )
        );

        $formUserInfo = array(
            'name' => 'User',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'User.username' => array(
                    'labelText' =>__d('user', 'User.username'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'User.nom' => array(
                    'labelText' =>__d('user', 'User.nom'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'User.prenom' => array(
                    'labelText' => __d('user', 'User.prenom'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'User.mail' => array(
                    'labelText' => __d('user', 'User.mail'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                )
            )
        );

        $formUserMdp = array(
            'name' => 'User',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'User.pwdmodified' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden',
                        'value'=>true
                    )
                ),
                'User.password' => array(
                    'labelText' =>__d('user', 'User.password'),
                    'inputType' => 'password',
                    'items'=>array(
                        'type'=>'password',
                        'required'=>true
                    )
                ),
                'User.password2' => array(
                    'labelText' =>__d('user', 'User.password_confirm'),
                    'inputType' => 'password',
                    'items'=>array(
                        'type'=>'password',
                        'required'=>true
                    )
                )
            )
        );
        echo $this->Formulaire->createForm($formUser);
        $fieldsetUserContent = "<div class='col-sm-6'>".$this->Formulaire->createForm($formUserInfo)."</div>";
        $fieldsetUserContent .= "<div class='col-sm-6'>".$this->Formulaire->createForm($formUserMdp)."</div>";
        echo $this->Html->tag('fieldset', $fieldsetUserContent);

        echo $this->Form->end();

        ?>
    </div>
    <div class="panel-footer " role="group">
        <a class="btn btn-danger-webgfc btn-inverse " id="cancel"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
        <a class="btn btn-success " id="saveNewMDP"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
    </div>
</div>
<script type="text/javascript">
    $('#saveNewMDP').click(function () {

        if (form_validate($('#UserChangemdpForm'))) {
            var id = $('#UserId').val();
            gui.request({
                url: '/users/changemdp/' + id,
                data: $('#UserChangemdpForm').serialize(),
                loaderElement: $(".infoZone"),
                loaderMessage: gui.loaderMessage
            }, function (data) {
                getJsonResponse(data);
                gui.request({
                    url: '/users/edit/' + id,
                    loader: false
                }, function (data) {
                    $('.ui-dialog-content').html(data);
                    $('.loader').remove();
                });
            });
            $("#infosPass").remove();
            $("#infoPerson").show();
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });
    $('#cancel').click(function () {
        $("#infosPass").remove();
        $("#infoPerson").show();
    });


    // Affichage des notifications
    $('#UserOptionNotification').hide();
    $('#UserAcceptNotif').change(function () {
        if ($('#UserAcceptNotif').prop("checked")) {
            $('#UserOptionNotification').show();
        }
        else {
            $('#UserOptionNotification').hide();
        }
    });
</script>


