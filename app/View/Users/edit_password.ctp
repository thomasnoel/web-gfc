<?php

/**
 *
 * Users/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formUserEditPassword = array(
	'name' => 'User',
	'div_w' => 'col-sm-12',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-7',
	'input' => array()
);
	$formUserUserContent = array(
		'name' => 'User',
		'div_w' => 'col-sm-12',
		'label_w' => 'col-sm-5',
		'input_w' => 'col-sm-7',
		'input' => array(
			'User.id' => array(
				'inputType' => 'hidden',
				'items'=>array(
					'type'=>'hidden'
				)
			)
		)
	);
if( $oldPasswordProvided ) {

	$formUserUserContentOldPassword = array(
			'name' => 'User',
			'div_w' => 'col-sm-12',
			'label_w' => 'col-sm-5',
			'input_w' => 'col-sm-7',
			'input' => array(
					'User.oldpassword' => array(
							'labelText' => __d('user', 'User.oldpassword'),
							'inputType' => 'text',
							'items' => array(
									'required' => true,
									'type' => 'password'
							)
					)
			)
	);
}

$formUserUserContent2 = array(
	'name' => 'User',
	'div_w' => 'col-sm-12',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-7',
	'input' => array(
		'User.password' => array(
			'labelText' =>__d('user', 'User.password'),
			'inputType' => 'password',
			'items'=>array(
				'required'=>true,
				'type'=>'password'
			)
		),
		'User.password2' => array(
			'labelText' =>__d('user', 'User.password'),
			'inputType' => 'password',
			'items'=>array(
				'required'=>true,
				'type'=>'password'
			)
		)
	)
);



?>
<fieldset>
	<?php echo $this->Formulaire->createForm($formUserEditPassword); ?>
	<fieldset class="form-group col-sm-12">
		<legend>Modification du mot de passe</legend>
		<?php
			echo $this->Formulaire->createForm($formUserUserContent);
			if($oldPasswordProvided) {
				echo $this->Formulaire->createForm($formUserUserContentOldPassword);
			}
			echo $this->Formulaire->createForm($formUserUserContent2);
		?>
	</fieldset>
</fieldset>


<?php
echo $this->Form->end();
?>
