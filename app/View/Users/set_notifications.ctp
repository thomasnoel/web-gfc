<?php

/**
 *
 * Users/set_notifications.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php //echo $this->Html->css(array('recherche'), null, array('inline' => false));  ?>

<div id="Tabsearch">
    <ul>
        <li><h3><span><?php echo __d('notification', 'Notification.Gestion'); ?></span></h3></li>
    </ul>
    <div id="notifif_conf">
        <h4><?php echo __d('notification', 'Notification.AutorisationEmail'); ?></h4>
            <?php echo $this->Form->create('User'); ?>
            <?php
            echo $this->Form->input('User.id');
            echo $this->Form->input('MailNotification.MailNotification', array('label' => false, 'type' => 'select', 'multiple' => 'checkbox', 'style' => 'height: 250px;width: 450px;', 'options' => $mailNotif, 'default' => false, 'escape' => false), false);
            echo $this->Form->end();
            ?>
    </div>
</div>
<div id="Valide">

</div>
<script type="text/javascript">
    //activation des onglets
    gui.tabs({element: $('#Tabsearch')});
    gui.buttonbox({element: $('#Valide')});
    gui.addbutton({element: $('#Valide'),
        button: {
            content: '<i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer',
            action: function () {
                $('#UserSetNotificationsForm').submit();
            }
        }});
    gui.addbutton({element: $('#Valide'),
        button: {
            content: '<i class="fa fa-floppy-o" aria-hidden="true"></i> Annuler',
            action: function () {
                $('#NotificationAddForm').submit();
            }
        }
    });
</script>
