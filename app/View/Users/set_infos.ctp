<?php

/**
 *
 * Users/set_infos.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$formUser = array(
    'name' => 'User',
    'label_w' => 'col-sm-6',
    'input_w' => 'col-sm-6',
    'input' => array(
        'User.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'User.username' => array(
            'labelText' =>__d('user', 'User.username'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'User.nom' => array(
            'labelText' =>__d('user', 'User.nom'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'User.prenom' => array(
            'labelText' =>__d('user', 'User.prenom'),
            'inputType' => 'text',
            'items'=>array(
                'type'=>'text'
            )
        ),
        'User.profil_id' => array(
            'labelText' =>__d('user', 'User.profil_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $groups,
                'empty' => true
            )
        ),
        'Service.Service' => array(
            'labelText' =>__d('user', 'User.username'),
            'inputType' => 'select',
            'items'=>array(
                 'options' => $services,
                'default' => false,
                'multiple' => 'multiple',
                'type'=>'select',
                'class' => 'selectMultiple',
                'escape' => false
            )
        )
    )
);
echo $this->Formulaire->createForm($formUser);
echo $this->Form->end();
?>
