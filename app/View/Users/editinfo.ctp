<?php

/**
 *
 * Users/editinfo.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php // echo $this->Html->css(array('zones/user/infospersos'), null, array('inline' => false)); ?>

<table id="tabinfospersos">
    <tr>
        <td>
            <h3 class="ui-widget-header-webgfc ui-state-default ui-corner-top"><?php echo __d('menu', 'Informations personnelles'); ?></h3>
            <div class="ui-widget ui-widget-content ui-corner-bottom ui-widget-webgfc" id="infospersos">
                <table class="ui-widget-webgfc-viewtab">
                    <tr>
                        <td class="label"><?php __('Nom'); ?></td>
                        <td class="value"><?php echo $user['User']['nom']; ?></td>
                    </tr>
                    <tr>
                        <td class="label"><?php __('Prenom'); ?></td>
                        <td class="value"><?php echo $user['User']['prenom']; ?></td>
                    </tr>
                    <tr>
                        <td class="label"><?php __('Email'); ?></td>
                        <td class="value"><?php echo $user['User']['mail']; ?></td>
                    </tr>
                    <tr>
                        <td class="label"><?php __('Numtel'); ?></td>
                        <td class="value"><?php echo $user['User']['numtel']; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2" id="chinfo"><span>Modifier les informations</span></td>
                    </tr>
                </table>
            </div>
        </td>
        <td>
            <h3 class="ui-widget-header-webgfc ui-state-default ui-corner-top"><?php echo __d('menu', 'Mon Compte'); ?></h3>
            <div class="ui-widget ui-widget-content ui-corner-bottom ui-widget-webgfc" id="infoscompte">
                <table class="ui-widget-webgfc-viewtab">
                    <tr>
                        <td class="label"><?php __('Identifiant'); ?></td>
                        <td class="value"><?php echo $user['User']['username']; ?></td>
                    </tr>
                    <tr>
                        <td class="label"><?php __('Collectivite'); ?></td>
                        <td class="value"><?php echo $collectivite['Collectivite']['name']; ?></td>
                    </tr>
                    <tr>
                        <td class="label"><?php __('Profil'); ?></td>
                        <td class="value"><?php echo $user['Profil']['name']; ?></td>
                    </tr>
                    <tr>
                        <td class="label"><?php __('Services'); ?></td>
                        <td class="value">
                            <ul>
								<?php
								foreach ($user['Service'] as $service) {
									echo "<li>" . $service['name'] . "</li>";
								}
								?>
                            </ul>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" id="chmdp"><span>Changer de mot de passe</span></td>
                    </tr>
                    <tr>
                        <td class="label">Statut</td>
                        <td class="value">
							<?php
							if ($isAway) {
								echo "Absent(e)";
							} else {
								echo "Présent(e)";
							}
							?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Délégué</td>
                        <td class="value">
							<?php
							if ($isAway) {
								echo $delegue['User']['prenom'] . " " . $delegue['User']['nom'];
							}
							?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Du</td>
                        <td class="value">
							<?php
							if ($isAway) {
								$this->Html2->ukToFrenchDateWithSlashes($user['User']['date_dep'] . "000000");
							}
							?>
                        </td>
                    </tr>
                    <tr>
                        <td class="label">Au</td>
                        <td class="value">
							<?php
							if ($isAway) {
								$this->Html2->ukToFrenchDateWithSlashes($user['User']['date_ret'] . "000000");
							}
							?>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>

        <td>
            <h3 class="ui-widget-header-webgfc ui-state-default ui-corner-top"><?php echo __d('menu', 'Gestion des Absences'); ?></h3>
            <div class="ui-widget ui-widget-content ui-corner-bottom ui-widget-webgfc" id="gestionAbsences">
                <h4>Gestion manuelle</h4>
                Délégue : <select>
					<?php foreach ($users as $key => $user) { ?>
                    <option value="<?php echo $key; ?>"><?php echo $user; ?></option>
					<?php } ?>
                </select>
                <br />
                <br />
                <span id="awayBttn">Départ</span> <span id="notawayBttn">Retour</span>
                <h4>Planification</h4>
                Délégue : <select>
					<?php foreach ($users as $key => $user) { ?>
                    <option value="<?php echo $key; ?>"><?php echo $user; ?></option>
					<?php } ?>
                </select>
                <br />
                <br />
                Du : <input type="text" id="dateDep" id="dateDep"><br />
                Au : <input type="text" id="dateRet" id="dateRet"><br />
                <br />
                <div id="planification"></div>
            </div>
        </td>
    </tr>
</table>

<script type="text/javascript">
    $('#chinfo span').button();
    $('#chmdp span').button();

    $('#awayBttn').button();
    $('#notawayBttn').button();

    gui.buttonbox({
        element: $('#planification')
    });

    gui.addbuttons({
        element: $('#planification'),
        buttons: [
            {
                content: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
                class: "btn-danger-webgfc btn-inverse ",
                action: function () {

                }
            },
            {
                content: '<i class="fa fa-check" aria-hidden="true"></i> Valider',
                action: function () {

                }
            }
        ]
    });

    gui.datepicker({
        element: $('#dateDep')
    });
    gui.datepicker({
        element: $('#dateRet')
    });

</script>
