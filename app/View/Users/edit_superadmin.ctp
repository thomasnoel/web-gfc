<?php

/**
 *
 * Users/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formUserEditsuperadmin = array(
	'name' => 'User',
	'div_w' => 'col-sm-12',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-7',
	'input' => array()
);

$formUserUserContent = array(
	'name' => 'User',
	'div_w' => 'col-sm-12',
	'label_w' => 'col-sm-5',
	'input_w' => 'col-sm-7',
	'input' => array(
		'User.id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden'
			)
		),
		'User.username' => array(
			'labelText' =>__d('user', 'User.username'),
			'inputType' => 'text',
			'items'=>array(
				'required'=>true,
				'type'=>'text'
			)
		),
//		'User.password' => array(
//			'labelText' =>__d('user', 'User.password'),
//			'inputType' => 'password',
//			'items'=>array(
//				'required'=>true,
//				'type'=>'password'
//			)
//		),
		'User.nom' => array(
			'labelText' =>__d('user', 'User.nom'),
			'inputType' => 'text',
			'items'=>array(
				'required'=>true,
				'type'=>'text'
			)
		),
		'User.prenom' => array(
			'labelText' =>__d('user', 'User.prenom'),
			'inputType' => 'text',
			'items'=>array(
				'required'=>true,
				'type'=>'text'
			)
		),
		'User.mail' => array(
			'labelText' =>__d('user', 'User.mail'),
			'inputType' => 'email',
			'items'=>array(
				'required'=>true,
				'type'=>'email'
			)
		),
		'User.active' => array(
			'labelText' =>__d('user', 'User.active'),
			'inputType' => 'checkbox',
			'items'=>array(
				'type'=>'checkbox',
				'checked'=>$this->request->data['User']['active']
			)
		)
	)
);



?>
<fieldset>
	<?php echo $this->Formulaire->createForm($formUserEditsuperadmin); ?>
	<fieldset class="form-group col-sm-12">
		<legend>Utilisateur</legend>
		<?php echo $this->Formulaire->createForm($formUserUserContent); ?>
	</fieldset>
</fieldset>


<?php
echo $this->Form->end();
?>


<script type="text/javascript">
	$("input[name='data[User][username]']").blur(function () {
		gui.request({
			url: "/users/ajaxformvalid/username",
			data: $('#UserEditSuperadminForm').serialize()
		}, function (data) {
			processJsonFormCheck(data);
		});
	});
</script>
