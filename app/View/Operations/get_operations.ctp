<?php

/**
 *
 * Operations/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="bouton-search btn-group"  role="group" id="searchControls"">
    <?php
/*  Formulaire de recherche d'operations */
echo $this->Html->link(
        '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
        '#',
        array( 'escape' => false,
                'title' => 'Accès à la recherche',
                'onclick' => "$( '#zoneOperationGetOperationsForm' ).toggle(); return false;",
                'id' => 'searchOperationGetOperationsForm',
                'class' => 'btn btn-primary '
        )
    );
    echo $this->Html->link(
            '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
            '#',
            array( 'escape' => false,
                      'title' => 'Réinitialiser la recherche',
                      'onclick' => "loadOperation(); return false;",
                      'class' => 'btn btn-primary '
            )
        );
    ?>
</div>
    <?php
$formOperation = array(
        'name' => 'Operation',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-4',
        'input_class'=>( ( is_array( $this->request->data )  ) ? 'search folded' : 'search unfolded' ),
        'input_url' => 'getOperations',
        'input' => array(
            'Operation.id' => array(
                'labelText' =>__d('operation', 'Operation.name'),
                'inputType' => 'select',
                'labelPlaceholder' =>__d('operation', 'Operation.placeholderNameAdd'),
                'items'=>array(
                    'type'=>'select',
                    'options' => $listoperations,
                    'empty' => true
                )
            ),
            'Operation.active' => array(
                'labelText' =>'Opération active ?',
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked'=>true
                )
            )
        )
    );
?>
<fieldset class="zone-form" id="zoneOperationGetOperationsForm">
    <legend>Recherche de bureaux</legend>
    <?php
        echo $this->Formulaire->createForm($formOperation);
    ?>
    <div class="controls text-center" role="group" >
        <span id="reset" class="aere btn btn-info-webgfc btn-inverse"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</span>
        <span id="searchButton" class="aere btn btn-success "><i class="fa fa-search" aria-hidden="true"></i> Rechercher</span>
    </div>
    <?php echo $this->Form->end();?>
</fieldset>

<script type="text/javascript">
    $('#formulaireButton').button();
    $('#searchButton').button();
    $('#reset').button();


    $('#searchButton').click(function () {
        gui.request({
            url: $("#OperationGetOperationsForm").attr('action'),
            data: $("#OperationGetOperationsForm").serialize(),
            loader: true,
            updateElement: $('#liste .content'),
            loaderMessage: gui.loaderMessage,
        }, function (data) {
            $('#liste .content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $(function () {
        $('#OperationGetOperationsForm').keypress(function (event) {
            if (event.which == 13) {
                gui.request({
                    url: $("#OperationGetOperationsForm").attr('action'),
                    data: $("#OperationGetOperationsForm").serialize(),
                    loader: true,
                    updateElement: $('#liste .content'),
                    loaderMessage: gui.loaderMessage,
                }, function (data) {
                    $('#liste .content').html(data);
                }
                );
                return false;
            }
        });

    });

    $('#reset').click(function () {
        resetJSelect('#OperationGetOperationsForm');
        $('#zoneOperationGetOperationsForm').hide();
    });

    $('#zoneOperationGetOperationsForm').hide();

    $('#OperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});
</script>

<?php
$this->Paginator->options(array(
    'update' => '#liste .content',
    'evalScripts' => true,
    'before' => 'gui.loader({ element: $("#liste .content"), message: gui.loaderMessage });',
    'complete' => '$(".loader").remove()',
));

//$pagination = '';
//if (Set::classicExtract($this->params, 'paging.Operation.pageCount') > 1) {
//    $pagination = '<div class="pagination" style="margin: auto auto auto 80%;">
//                                ' . implode(
//                                    '', Set::filter(
//                                        array(
//                                            $this->Html->tag('li',$this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
//                                            $this->Html->tag('li',$this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
//                                            $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
//                                            $this->Html->tag('li',$this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
//                                            $this->Html->tag('li',$this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
//                                        )
//                                    )
//                                ) . '
//                        </div>';
//}


?>

<?php
if( !empty($operations) ) {
    $fields = array(
        'name',
        'nomoperation',
        'ville'
    );
    $actions = array(
         "add" => array(
            "url" => Configure::read('BaseUrl') . '/operations/',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false,
             "refreshAction" => "loadOperation();"
        ),
        "edit" => array(
            "url" => Configure::read('BaseUrl') . '/operations/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false,
            "refreshAction" => "loadOperation();"
        ),
        "delete" => array(
            "url" => Configure::read('BaseUrl') . '/operations/delete/',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadOperation();"
        )
    );
    $options = array();

    $data_table = $this->Liste->drawTbody($operations, 'Operation', $fields, $actions, $options);
    $data = (json_decode($data_table,true));
    for($i=0; $i<count($operations);$i++){
        if(isset($operations[$i]['Operation']['sousope'])){
            $sousoperations = $operations[$i]['Operation']['sousope'];
            $data[$i]['sousoperation']="<ul>";
            foreach ($sousoperations as $sousoperation) {
                $data[$i]['sousoperation'].="<li>".$sousoperation."</li>";
            }
            $data[$i]['sousoperation'].="</ul>";
        }
    }

    $data = json_encode($data);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_operations"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-pagination = "false"
           >
    </table>
</div>

<?php // echo $pagination; ?>
<script type="text/javascript">
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($operations,$options); ?>');
    $('#table_operations')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d('operation', 'Operation.name'); ?>",
                        class: "name"
                    },
                    {
                        field: "nomoperation",
                        title: "<?php echo __d('operation', 'Operation.nomoperation'); ?>",
                        class: "nomoperation"
                    },
                    {
                        field: "ville",
                        title: "<?php echo __d('operation', 'Operation.ville'); ?>",
                        class: "ville"
                    },
                    {
                        field: "sousoperation",
                        title: "<?php echo __d('operation', 'Operation.getSousoperations'); ?>",
                        class: "sousoperation"
                    },
                    {
                        field: "active",
                        title: "<?php echo 'Active'; ?>",
                        class: "active_column"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('page-change.bs.table', function (number, size) {
                addClassActive();
            });
    $(document).ready(function () {
        addClassActive();
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }
    function addClassActive() {
        $('#table_operations .active_column').hide();
        $('#table_operations .active_column').each(function () {
            if ($(this).html() == 'false') {
                $(this).parents('tr').addClass('inactive');
            }
        });
    }
</script>
<?php
    echo $this->LIste->drawScript($operations, 'Operation', $fields, $actions, $options);
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('operation', 'Operation.void'), array('class' => 'alert alert-warning'));
?>
<script>
    gui.disablebutton({
        element: $('#liste .controls'),
        content: '<i class="fa fa-download" aria-hidden="true"></i>'
    });
</script>
<?php
}
?>
