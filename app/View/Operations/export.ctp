<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Informations sur les OP' ));
    
    $this->Csv->addRow(
        array(
            'N° de l\'OP',
            'Intitulé de l\'OP',
            'Ville concernée',
            'N° de convention',
            'Nature de l\'opération',
            'Adresse',
            'RO',
            'AO',
            'DIR',
            'DG',
            'DO',
            'JUR',
            'Gestion'
        )
	);

    $rowIntituleagent = array();
    $rowSousop = array();
    $rowOperation = array();
	foreach( $results as $m => $operation ){

        $numeroOperation = $operation['Operation']['name'];
        $nomOperation = $operation['Operation']['nomoperation'];
        $ville = $operation['Operation']['ville'];
        $numconvention = $operation['Operation']['numconvention'];
        $nature = $operation['Operation']['nature'];
        $adresse = $operation['Operation']['numvoie'].' '.$operation['Operation']['nomvoie'].' '.$operation['Operation']['compl'].' '.$operation['Operation']['codepostal'].' '.$operation['Operation']['commune'];
        
        $ro = isset( $operation['Operation']['Intituleagent']['RO'] ) ? $operation['Operation']['Intituleagent']['RO'] : '';
        $ao = isset( $operation['Operation']['Intituleagent']['AO'] ) ? $operation['Operation']['Intituleagent']['AO'] : '';
        $dg = isset( $operation['Operation']['Intituleagent']['DG'] ) ? $operation['Operation']['Intituleagent']['DG'] : '';
        $dir = isset( $operation['Operation']['Intituleagent']['DIR'] ) ? $operation['Operation']['Intituleagent']['DIR'] : '';
        $do = isset( $operation['Operation']['Intituleagent']['DO'] ) ? $operation['Operation']['Intituleagent']['DO'] : '';
        $gest = isset( $operation['Operation']['Intituleagent']['GEST'] ) ? $operation['Operation']['Intituleagent']['GEST'] : '';
        $jur = isset( $operation['Operation']['Intituleagent']['JUR'] ) ? $operation['Operation']['Intituleagent']['JUR'] : '';

        $rowOperation[$m] = array_merge(
            array($numeroOperation),
            array($nomOperation),
            array($ville),
            array($numconvention),
            array($nature),
            array($adresse),
            array($ro),
            array($ao),
            array($dir),
            array($dg),
            array($do),
            array($jur),
            array($gest)
        );
        $this->Csv->addRow($rowOperation[$m]);
        
    }


    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>
