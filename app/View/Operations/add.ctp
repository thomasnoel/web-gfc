<?php

/**
 *
 * Operations/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="row">
<?php
    $formOperation = array(
        'name' => 'Operation',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Operation.name' => array(
                'labelText' =>__d('operation', 'Operation.name'),
                'inputType' => 'text',
                'labelPlaceholder' =>__d('operation', 'Operation.placeholderNameAdd'),
                'items'=>array(
                    'required'=>true,
                    'type'=>'text'
                )
            ),
            'Operation.nomoperation' => array(
                'labelText' =>__d('operation', 'Operation.nomoperation'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.ville' => array(
                'labelText' =>__d('operation', 'Operation.ville'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.numconvention' => array(
                'labelText' =>__d('operation', 'Operation.numconvention'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.nature' => array(
                'labelText' =>__d('operation', 'Operation.nature'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.lieu' => array(
                'labelText' =>__d('operation', 'Operation.lieu'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formOperation);
    echo $this->Html->tag('legend', __d('operation', 'Operation.adresses'));
    $formOperationAdd = array(
        'name' => 'Operation',
        'label_w' => 'col-sm-5',
        'input_w' => 'col-sm-6',
        'input' => array(
            'Operation.numvoie' => array(
                'labelText' =>__d('operation', 'Operation.numvoie'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.nomvoie' => array(
                'labelText' =>__d('operation', 'Operation.nomvoie'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.compl' => array(
                'labelText' =>__d('operation', 'Operation.compl'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.codepostal' => array(
                'labelText' =>__d('event', 'Event.codepostal'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.commune' => array(
                'labelText' =>__d('event', 'Event.commune'),
                'inputType' => 'text',
                'items'=>array(
                    'type'=>'text'
                )
            ),
            'Operation.active' => array(
                'labelText' =>__d('operation', 'Operation.active'),
                'inputType' => 'checkbox',
                'items'=>array(
                    'type'=>'checkbox',
                    'checked'=>true,
                )
            ),
            'Operation.type_id' => array(
                'inputType' => 'hidden',
                'items'=>array(
                    'type'=>'hidden'
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formOperationAdd);
    echo $this->Form->end();
?>
</div>

