<?php
	$this->Csv->preserveLeadingZerosInExcel = true;

    $this->Csv->addRow( array( 'Informations sur les marchés' ));
    
    $this->Csv->addRow(
        array(
            'N° du marché',
            'N° de l\'OP',
            'Titulaire',
            'Objet du marché',
            'Type de contractant',
            'Date notification',
            'Mois M0',
            'RIF ou autres collect.'
        )
	);

	foreach( $results as $m => $marche ){

        $numeroMarche = $marche['Marche']['numero'];
        $numeroOP = $marche['Operation']['name'];
        $typecontractant = $marche['Contractant']['name'];
        $uniteRIF = $marche['Uniterif']['name'];
        $titulaireMarche = $marche['Marche']['titulaire'];
        $objetMarche = $marche['Marche']['objet'];
        $datenotification = $this->Html2->ukToFrenchDateWithSlashes($marche['Marche']['datenotification']);
        $moism0 = $marche['Marche']['premiermois'];

        $rowMarche[$m] = array_merge(
            array($numeroMarche),
            array($numeroOP),
            array($titulaireMarche),
            array($objetMarche),
            array($typecontractant),
            array($datenotification),
            array($moism0),
            array($uniteRIF)
        );
        
        $this->Csv->addRow($rowMarche[$m]);
    }


    Configure::write( 'debug', 0 );
	echo $this->Csv->render( "{$this->request->params['controller']}_{$this->request->params['action']}_".date( 'Ymd-His' ).'.csv' );
?>