<?php

/**
 *
 * Intitulesagents/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="bouton-search btn-group"  role="group" id="searchControls">
    <?php
if( !empty($intituleagentbyoperations) ) {
/*  Formulaire de recherche d'operations */
echo $this->Html->link(
        '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
        '#',
        array( 'escape' => false,
                  'title' => 'Accès à la recherche',
                  'onclick' => "$( '#zone_recherche_intituleagentbyoperations' ).toggle(); return false;",
                  'id' => 'formulaireButton',
                  'class' => 'btn btn-primary '
        )
    );
}
    echo $this->Html->link(
            '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
            '#',
            array( 'escape' => false,
                      'title' => 'Réinitialiser la recherche',
                      'onclick' => "loadIntituleagentbyoperation(); return false;",
                      'class' => 'btn btn-primary '
            )
        );
    ?>
</div>
<fieldset class="zone-form" id="zone_recherche_intituleagentbyoperations">
    <legend>Recherche de bureaux</legend>
    <?php
    $formIntituleagentbyoperation = array(
        'name' => 'Intituleagentbyoperation',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-3',
        'form_url' => 'getIntituleagentbyoperations',
        'input' => array(
            'Intituleagentbyoperation.coperation_id' => array(
                'labelText' =>__d('operation', 'Operation.name'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'options' => $operations,
                    'empty' => true
                )
            )
        )
    );
    echo $this->Formulaire->createForm($formIntituleagentbyoperation);
    ?>
    <div class="controls text-center" role="group" >
        <span id="reset" class="aere btn btn-info-webgfc btn-inverse "><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</span>
        <span id="searchButton" class="aere btn btn-success "><i class="fa fa-search" aria-hidden="true"></i> Rechercher</span>
    </div>
</fieldset>

<?php echo $this->Form->end();?>


<script type="text/javascript">
    $('#zone_recherche_intituleagentbyoperations').hide();
    $('#zone_recherche_intituleagentbyoperations div.required').removeClass('required');

    $('#formulaireButton').button();
    $('#searchButton').button();
    $('#reset').button();



    $('#searchButton').click(function () {
        gui.request({
            url: $("#IntituleagentbyoperationGetIntituleagentbyoperationsForm").attr('action'),
            data: $("#IntituleagentbyoperationGetIntituleagentbyoperationsForm").serialize(),
            loader: true,
            updateElement: $('#liste .content'),
            loaderMessage: gui.loaderMessage,
        }, function (data) {
            $('#liste .content').html(data);
        });
    });

    // Ajout de l'action de recherche via la touche Entrée
    $(function () {
        $('#IntituleagentbyoperationGetIntituleagetnbyoperationsForm').keypress(function (event) {
            if (event.which == 13) {
                gui.request({
                    url: $("#IntituleagentbyoperationGetIntituleagetnbyoperationsForm").attr('action'),
                    data: $("#IntituleagentbyoperationGetIntituleagetnbyoperationsForm").serialize(),
                    loader: true,
                    updateElement: $('#liste .content'),
                    loaderMessage: gui.loaderMessage,
                }, function (data) {
                    $('#liste .content').html(data);
                }
                );
                return false;
            }
        });

    });

    $('#reset').click(function () {
        resetJSelect('#IntituleagentbyoperationGetIntituleagentbyoperationsForm');
        $('#zone_recherche_intituleagentbyoperations').hide();
    });

    $('#IntituleagentbyoperationGetIntituleagetnbyoperationsForm.folded').hide();

    $('#IntituleagentbyoperationCoperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});
</script>
<?php
//$this->Paginator->options(array(
//    'update' => '#liste .content',
//    'evalScripts' => true,
//    'before' => 'gui.loader({ element: $("#liste .content"), message: gui.loaderMessage });',
//    'complete' => '$(".loader").remove()',
//));
//
//$pagination = '';
//if (Set::classicExtract($this->params, 'paging.Intituleagentbyoperation.pageCount') > 1) {
//	$pagination = '<div class="paging">
//                                            ' . implode(
//                                                '', Set::filter(
//                                                    array(
//                                                        $this->Html->tag('li',$this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
//                                                        $this->Html->tag('li',$this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
//                                                        $this->Html->tag('li',$this->Paginator->numbers(array('separator'=>false))),
//                                                        $this->Html->tag('li',$this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
//                                                        $this->Html->tag('li',$this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
//                                                    )
//                                                )
//                                            ) . '
//                                        </div>';
//}
?>
<?php if( !empty($intituleagentbyoperations) ) {
    $i = 0;
    foreach ($intituleagentbyoperations as $idOp => $intituleagentbyoperation){
        if(isset($operations[$idOp])){
            $data[$i]['name'] = $operations[$idOp];
        }

        if( !empty($intituleagentbyoperation['Intituleagentbyoperation'] ) && isset($intituleagentbyoperation['Intituleagentbyoperation']) ) {
            sort( $intituleagentbyoperation['Intituleagentbyoperation'] );
            $data[$i]['desktopmanager_id'] = '<ul>';
            foreach( $intituleagentbyoperation['Intituleagentbyoperation'] as $k => $intitule ) {
                $data[$i]['desktopmanager_id'].="<li>".$intituleagents[$intitule['intituleagent_id']]. ' = '.$desktopsmanagers[$intitule['desktopmanager_id']]."</li>";
            }
            $data[$i]['desktopmanager_id'] .= '</ul>';
        }

        $data[$i]['edit'] = $this->Html->tag('i', '', array(
                    'title' => __d('default', 'Button.edit'),
                    'alt' => __d('default', 'Button.edit'),
                    'class' => "fa fa-pencil itemEdit",
                    'itemId' => $idOp,
//                    'style' => 'color: #5397a7;cursor:pointer'
                        )
                );

        $data[$i]['delete'] = $this->Html->tag('i', '', array(
                        'title' => __d('default', 'Button.delete'),
                        'alt' => __d('default', 'Button.delete'),
                        'class' => "fa fa-trash itemDelete",
                        'itemId' => $idOp,
//                        'style' => 'color: #FF0000;cursor:pointer'
                            )
                    );
        $i++;
    }
    $data = json_encode($data);
    ?>
<div  class="bannette_panel panel-body">
    <table id="table_intituleagentbyoperations"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height="400"
           data-pagination = "false"
           >
    </table>
    <!--<p><?php // echo $pagination; ?></p>-->
</div>

<script type="text/javascript">
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($intituleagentbyoperations,array()); ?>');
    $('#table_intituleagentbyoperations')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d('operation', 'Operation.name'); ?>",
                        class: "name"
                    },
                    {
                        field: "desktopmanager_id",
                        title: "<?php echo __d('intituleagentbyoperation', 'Intituleagentbyoperation.desktopmanager_id'); ?>",
                        class: "desktopmanager_id"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            })
            .on('search.bs.table', function (e, text) {
                itemEdit();
                itemDelete();
            });


    $(document).ready(function () {
        $('.pagination li span').click(function () {
            $(this).find('a').click();
        });
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.5);
    }

    function itemEdit() {
        $('i.itemEdit').click(function () {
            var itemId = $(this).attr("itemId");
            var url = "intituleagentbyoperations/edit/" + itemId;
            gui.formMessage({
                updateElement: $('#infos'),
                loader: true,
                width: '500px',
                loaderMessage: gui.loaderMessage,
                title: "<?php echo __d('intituleagentbyoperation', 'Intituleagentbyoperation.editIntituleagentbyoperation'); ?>",
                data: $(this).serialize(),
                url: url,
                buttons: {
                    "<?php echo __d('default', 'Button.cancel'); ?>": function () {
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                        loadIntituleagentbyoperation();
                    },
                    "<?php echo __d('default', 'Button.submit'); ?>": function () {
                        if (form_validate($('#SousoperationEditForm'))) {
                            var selectPass = false;
                            $('select[id*="DesktopmanagerId"]').each(function () {
                                if ($(this).val() != "") {
                                    selectPass = true;
                                }
                            });
                            if (selectPass) {
                                gui.request({
                                    url: url,
                                    data: $('#IntituleagentbyoperationEditForm').serialize(),
                                    loaderElement: $('#infos .content'),
                                    loaderMessage: gui.loaderMessage
                                }, function (data) {
                                    gui.request({
                                        loader: true,
                                        updateElement: $('#infos .content #ui-tabs-3'),
                                    }, function (data) {
                                        $('.ui-dialog-content').html(data);

                                    });
                                });
                                $(this).parents('.modal').modal('hide');
                                $(this).parents('.modal').empty();
                                loadIntituleagentbyoperation();
                            } else {
                                swal({
                    showCloseButton: true,
                                    title: "Oops...",
                                    text: "Veuillez choisir au moins un bureau!",
                                    type: "error",

                                });
                            }
                        } else {
                            swal({
                    showCloseButton: true,
                                title: "Oops...",
                                text: "Veuillez vérifier votre formulaire!",
                                type: "error",

                            });
                        }

                    }
                }
            });

        });
    }
    itemEdit();

    function itemDelete() {
        $('i.itemDelete').click(function () {

            var itemId = $(this).attr("itemId");
            swal({
                    showCloseButton: true,
                title: "<?php echo __d('default', 'Confirmation de suppression'); ?>",
                text: "<?php echo __d('default', 'Voulez-vous supprimer cet élément ?'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#d33",
                cancelButtonColor: "#3085d6",
                confirmButtonText: '<i class="fa fa-trash" aria-hidden="true"></i> Supprimer',
                cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
            }).then(function (data) {
                if (data) {
                    gui.request({
                        url: "/intituleagentbyoperations/delete/" + itemId,
                        loader: true,
                        loaderMessage: gui.loaderMessage,
                        updateElement: $('#infos .content')
                    }, function (data) {
                        getJsonResponse(data);
                        $(this).parents('.modal').modal('hide');
                        $(this).parents('.modal').empty();
                        loadIntituleagentbyoperation();
                    });
                } else {
                    swal({
                    showCloseButton: true,
                        title: "Annulé!",
                        text: "Vous n\'avez pas supprimé.",
                        type: "error",

                    });
                }
            });
            $('.swal2-cancel').css('margin-left', '-320px');
        $('.swal2-cancel').css('background-color', 'transparent');
        $('.swal2-cancel').css('color', '#5397a7');
        $('.swal2-cancel').css('border-color', '#3C7582');
        $('.swal2-cancel').css('border', 'solid 1px');

        $('.swal2-cancel').hover(function () {
            $('.swal2-cancel').css('background-color', '#5397a7');
            $('.swal2-cancel').css('color', 'white');
            $('.swal2-cancel').css('border-color', '#5397a7');
        }, function () {
            $('.swal2-cancel').css('background-color', 'transparent');
            $('.swal2-cancel').css('color', '#5397a7');
            $('.swal2-cancel').css('border-color', '#3C7582');
        });
        });
    }
    itemDelete();

</script>
<?php
    echo $this->Js->writeBuffer();
}else{
    echo $this->Html->tag('div', __d('Intituleagentbyoperation', 'Intituleagentbyoperation.void'), array('class' => 'alert alert-warning'));
}
?>

