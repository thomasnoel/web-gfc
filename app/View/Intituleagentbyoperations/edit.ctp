<?php

/**
 *
 * Metadonnees/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php foreach( $intitulesagents as $b => $intituleName ) :?>
<script type="text/javascript">

    //contruction du tableau de intitulesagents / bureaux
    var intitulesagents = [];
    <?php /*foreach ($intitulesagents as $intitule) {*/ ?>
    var intitule = {
        id: "<?php echo $intituleName['Intituleagent']['id']; ?>",
        name: "<?php echo addslashes( $intituleName['Intituleagent']['name'] ); ?>",
        desktopsmanagers: []
    };
        <?php foreach ($intituleName['Desktopmanager'] as $desktopmanager) { ?>
    var desktopmanager = {
        id: "<?php echo $desktopmanager['id']; ?>",
        name: "<?php echo addslashes( $desktopmanager['name'] ); ?>"
    };
    intitule.desktopsmanagers.push(desktopmanager);
        <?php } ?>
    intitulesagents.push(intitule);
    <?php /*}*/ ?>

    //remplissage de la liste des intitulesagents
    for (i in intitulesagents) {
        fillDesktopsmanagers(intitulesagents[i].id);
    }

    function fillDesktopsmanagers(intituleagent_id, desktomanager_id) {
        for (i in intitulesagents) {
            if (intitulesagents[i].id == intituleagent_id) {
                for (j in intitulesagents[i].desktopsmanagers) {
                    if (intitulesagents[i].desktopsmanagers[j].id == desktomanager_id) {

                        $("#<?php echo $intituleName['Intituleagent']['id'];?>IntituleagentbyoperationDesktopmanagerId").append($("<option value='" + intitulesagents[i].desktopsmanagers[j].id + "' selected='selected'>" + intitulesagents[i].desktopsmanagers[j].name + "</option>"));
                    } else {
                        $("#<?php echo $intituleName['Intituleagent']['id'];?>IntituleagentbyoperationDesktopmanagerId").append($("<option value='" + intitulesagents[i].desktopsmanagers[j].id + "'>" + intitulesagents[i].desktopsmanagers[j].name + "</option>"));
                    }
                }
            }
        }
    }


    <?php if( !empty( $intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['intituleagent_id'] ) ) :?>
    $("#<?php echo $intituleName['Intituleagent']['id'];?>IntituleagentbyoperationIntituleagentId").val("<?php echo $intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['intituleagent_id'];?>").change();
    <?php endif;?>
    <?php if( !empty( $intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['desktopmanager_id'] ) ) :?>
    $("#<?php echo $intituleName['Intituleagent']['id'];?>IntituleagentbyoperationDesktopmanagerId").val("<?php echo $intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['desktopmanager_id'];?>").change();
    <?php endif;?>

    <?php if( !empty( $intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['operation_id'] ) ) :?>
    $("#IntituleagentbyoperationOperationId").val("<?php echo $intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['operation_id'];?>").change();
    <?php endif;?>
</script>
<?php endforeach;?>
<div class="alert alert-info">Veuillez choisir au moins un bureau.</div>
<?php
$formIntituleagentbyoperation = array(
    'name' => 'Intituleagentbyoperation',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-5',
    'form_type'=>'post',
    'form_url'=>'/intituleagentbyoperations/edit/'.$this->request->data[0]['Intituleagentbyoperation']['operation_id'],
    'input' => array(
        'Intituleagentbyoperation.operation_id' => array(
            'labelText' =>__d('intituleagentbyoperation', 'Intituleagentbyoperation.operation_id'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => $operations,
                'empty' => true,
                'selected' => $id
            )
        )
    )
);
foreach( $intitulesagents as $b => $intituleName ) {
    if( !empty($intitulesagentsbyoperations[$intituleName['Intituleagent']['id']]['Intituleagentbyoperation']['operation_id']) ) {
        $formIntituleagentbyoperation["input"][$intituleName['Intituleagent']['id'].".Intituleagentbyoperation.id"] = array(
            'inputType' => 'hidden',
            'items'=>array(
            'type'=>'hidden',
            )
        );
    }
    $formIntituleagentbyoperation["input"][$intituleName['Intituleagent']['id'].".Intituleagentbyoperation.intituleagent_id"] = array(
        'inputType' => 'hidden',
        'items'=>array(
            'type'=>'hidden',
            'value'=>$intituleName['Intituleagent']['id']
        )
    );
    $formIntituleagentbyoperation["input"][$intituleName['Intituleagent']['id'].".Intituleagentbyoperation.desktopmanager_id"] =array(
        'labelText' =>$intituleName['Intituleagent']['name'],
        'inputType' => 'select',
        'items'=>array(
           'type'=>'select',
            'options' => array(),
            'empty' => true
        )
    );
}
echo $this->Formulaire->createForm($formIntituleagentbyoperation);
echo $this->Form->end();
?>

<script type="text/javascript">
    $('#IntituleagentbyoperationOperationId').select2({allowClear: true, placeholder: "Sélectionner une OP"});

<?php foreach( $intitulesagents as $b => $intituleName ) :?>
    $('#<?php echo $intituleName['Intituleagent']['id'];?>IntituleagentbyoperationDesktopmanagerId').select2({allowClear: true, placeholder: "Sélectionner un bureau"});
<?php endforeach;?>
</script>
