<?php

/**
 *
 * Users/login.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php

    $cssFiles = array(
        'select2'
    );
    echo $this->Html->css($cssFiles);


    $jsFiles = array(
        'select2' //Arnaud
    );
    if (Configure::read('debug') > 0) {
        $jsFiles[] = 'debugTools';
    }
    echo $this->Html->script($jsFiles);
?>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
		<?php echo $this->Html->charset(); ?>
        <title>
			<?php echo __d('default', 'titre'); ?>
        </title>
		<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');
        $cssFiles = array(
            'select2'
        );
        echo $this->Html->css($cssFiles);

		echo $this->Html->css(array(JQUERYCSS . 'jquery-ui-' . JQUERYUIVERSION . '.custom', JQUERYCSSBASE . 'jquery.jgrowl', 'webgfc', 'login'));

		echo $this->fetch('script');
		echo $this->Html->script(array(JQUERYJSBASE . 'jquery-' . JQUERYVERSION, JQUERYJSBASE . 'jquery-ui-' . JQUERYUIVERSION . '.custom.min', JQUERYJSBASE . 'jquery.ui.datepicker-fr', JQUERYJSBASE . 'jquery.jgrowl', JQUERYJSBASE . 'jquery.validate', JQUERYJSBASE . 'jquery.validate.messages_fr', JQUERYJSBASE . 'jquery.simpleclass', 'gui', 'default'));
		?>
    </head>
    <body>
		<?php echo $this->Html->tag('div', '&nbsp;', array('class' => 'logo_titre')); ?>
        <div id="container">
            <div id="header"></div>
            <div id="content">
                <div id="webgfc_content" class="shadowBox">
                    <?php echo $this->fetch('content'); ?>
                </div>
            </div>
            <footer>
                <div class="navbar navbar-inverse navbar-fixed-bottom">
                    <div class="container-fluid">
                        <span class="hidden-xs
                              hidden-sm
                              col-md-4
                              col-lg-4
                              navbar-left navbar-icons-buffer-top"
                              style="padding-left: 15px;">
                        </span>

                        <span class="hidden-xs
                              col-sm-4
                              col-md-4
                              col-lg-4
                              text-center h5 text-muted"
                              style="padding-top: 6px;"> <!-- font-family: 'Ubuntu Light', serif -->
                            web-GFC v<span class="bold"><?php echo WEBGFCVERSION; ?></span> © Libriciel SCOP 2006-<?php echo date('Y') ;?>
                        </span>

                        <span class="col-xs-12
                              col-sm-4
                              col-md-4
                              col-lg-4
                              navbar-right navbar-icons-buffer-top"
                              style="padding-right: 30px;">
                            <a href="https://www.libriciel.fr" class="pull-right" target="_external">
                                <img src="/img/Libriciel_white_h24px.png">
                            </a>
                        </span>
                    </div>
                </div>
            </footer>
        </div>

    </body>
</html>

<script type="text/javascript">
    $('#UserCollectivite').select2({allowClear: true, placeholder: "Sélectionner une collectivité"});

</script>
