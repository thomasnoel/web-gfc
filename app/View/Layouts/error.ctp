<?php

/**
 *
 * Layouts/login.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo __d('default', 'titre'); ?>
        </title>
        <?php
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->Html->css(array(JQUERYCSS . 'jquery-ui-' . JQUERYUIVERSION . '.custom', JQUERYCSSBASE . 'jquery.jgrowl', 'webgfc'/*,'bootstrap/bootstrap.min'/*, 'error'*/));
        echo $this->fetch('script');
        echo $this->Html->script(array('ls-elements.js'));
        ?>
    </head>
    <body>
        <?php echo $this->Html->tag('div', '&nbsp;', array('class' => 'logo_titre')); ?>
        <div id="container">
            <div id="header"></div>
            <div id="content">
                <div id="webgfc_content">
                    <div id="webgfc_error_container" class="container">
                        <div class="row">
                            <?php
                            echo $this->Session->flash();
                            echo $this->fetch('content');
                            echo $this->Html->tag('div', __d('error', 'page.getback'), array('class' => 'infomsg'));
                            $divErrorContent =  $this->Html->link(__d('error', 'environnement.getback'), array('controller' => 'environnement'),array('class'=>'btn btn-primary'));
                            $divErrorContent .= $this->Html->link(__d('error', 'environnement.logout'), array('controller' => 'users', 'action' => 'logout', 'plugin' => null),array('class'=>'btn btn-primary'));
                            echo $this->Html->tag('div', $divErrorContent, array('class' => 'bttnbox '));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer Libriciel SCOP -->
        <ls-footer class="ls-login-footer" active="webgfc" application_name="web-GFC v(<?php echo WEBGFCVERSION;?>)"></ls-footer>
    </body>
</html>
