<?php

/**
 *
 * Layouts/login.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
/**
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.cake.libs.view.templates.layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" style="font-size:16px;">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo __d('default', 'titre'); ?>
        </title>
            <?php
            echo $this->Html->meta('icon');
            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->Html->css(array(
                    'font-awesome/css/font-awesome.min.css',
                    'ls-bootstrap-4.css' // uniquement pour la page de login (2019-09-23)
            ));
            $jsFiles = array(
                 JQUERYJSBASE . 'jquery-' . JQUERYVERSION,
                JQUERYJSBASE . 'jquery-ui-' . JQUERYUIVERSION . '.custom.min',
                JQUERYJSBASE . 'jquery.validate',
                JQUERYJSBASE . 'jquery.validate.messages_fr',
                JQUERYJSBASE . 'jquery.simpleclass',
                JQUERYJSBASE . 'jquery.treeview',
                JQUERYJSBASE . 'jquery.tablesorter.min',
                JQUERYJSBASE . 'jquery-migrate-1.2.1.min',
                JQUERYJSBASE . 'jquery.caret.1.02.js',
                'bootstrap-table.min.js',
                'gui',
                'flexpaper_flash',
                'default',
                '/data_acl/js/jquery.acl',
                'select2', //Arnaud
                'bootstrap.min.js',
                'bootstrap-datepicker.min.js',
                'bootstrap-datepicker.fr.min.js',
                'moment.min.js',
                'layer/layer.js',
                'compatibility.js',
                'validator.min.js',
                'sweetalert2.min.js',
                'bootstrap-table-fr-FR.min'
            );
            echo $this->Html->script($jsFiles);
            echo $this->fetch('script');
            ?>
    </head>
    <body>


        <ls-lib-login logo="/img/logo-webgfc-500px.png"
                      visual-configuration='<?php echo $config['Configuration']['config'];?>'
                      login-username-input-name="data[User][username]"
                      login-password-input-name="data[User][password]"
                      forgot-is-form-ajax="true"
                      forgot-username-input-name="data[User][username]"
                      forgot-email-input-name="data[User][mail]"
                      login-show-error="<?php echo $error;?>" >

            <div class='login-form-addons'>

				<?php if( Configure::read('Mode.Multicoll') && Configure::read('Affichage.Selectcoll') ):?>
					<?php
						echo $this->Form->input(
							'User.coll',
							array(
								'type' => 'select',
								'empty' => true,
								'options' => $conns,
								'label' => array(
									'text'=> 'Choix de la collectivité',
									'class'=> 'control-label',
								),
								'class' => 'form-control',
							)
						);
					?>
					<span id="SelectColl">
						<i class="fa fa-building"></i>
					</span>
					<script type="text/javascript">
						$('#UserColl').css('display', 'inline');
						$('#UserColl').css('width', '64.7%');
						$('#UserColl').css('margin-left', '17px');
						$('#UserColl').css('padding-left', '45px');
						$('#UserColl').parent('div').addClass('input-group-front-icon ');
						$('#UserColl').parent('div').addClass('form-group');
						$('#UserColl').parent('div').prepend( $('#SelectColl') );

						$('#SelectColl').css('margin-left', '35.2%');
					</script>
				<?php endif;?>

				<!-- Message affiché lors de la connexion -->
                <div style="display:none;" class='alert float-right' id='waiter'>
                    <div style="display:inline-block;">
                        <center>
                            <p class="waiter-comment alert alert-info" style="width: 105%;padding:0.5rem;">
                                <?php echo $this->Html->image('/img/ajax-loader.gif', array( 'style' => 'float:left;padding-top:15px;padding-right:10px;', 'alt' => __('Loading'), 'id' => 'waiter-image'));?>
                                Afin d'améliorer les temps de traitement, différentes informations sont enregistrées en session lors de la connexion.<br /> Ceci peut prendre de quelques secondes à 1 minute.
                            </p>
                        </center>
                    </div>

                </div>


                 <!-- Bouton Afficher les comptes de démonstration -->
                <?php if (SHOW_LOGIN_TABLE) { ?>
					<div class="show_login_table">

						<div id="comptesdemoDiv"  style="display:none;">
							<div class="ui-widget ui-widget-content ui-corner-bottom ui-widget-webgfc">
								<table id="comptesdemo" style="border:1px solid grey;">
									<thead>
									<th class="text-center" style="width:33%;">Login</th>
									<th class="text-center" style="width:33%;">Mot de passe</th>
									<th class="text-center" style="width:33%;">Profil</th>
									</thead>
									<tbody>
										<tr>
											<td>PEVIVER@demo</td>
											<td class="text-center">webgfc</td>
											<td>Aiguilleur</td>
										</tr>
										<tr>
											<td>MJEAN@demo</td>
											<td class="text-center">webgfc</td>
											<td>Initiateur</td>
										</tr>
										<tr>
											<td>LREZEAU@demo</td>
											<td class="text-center">webgfc</td>
											<td>Valideur-Editeur</td>
										</tr>
										<tr>
											<td>MJONAS@demo</td>
											<td class="text-center">webgfc</td>
											<td>Valideur</td>
										</tr>

										<tr>
											<td>administrateur@demo</td>
											<td class="text-center">admin</td>
											<td>Admin</td>
										</tr>
										<tr>
											<td>admin@admin</td>
											<td class="text-center">admin</td>
											<td>Superadmin</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				<?php } ?>
            </div>
        </ls-lib-login>


        <!-- Footer Libriciel SCOP -->
        <ls-lib-footer class="ls-login-footer" active="webgfc" application_name="web-GFC v(<?php echo WEBGFCVERSION;?>)"></ls-lib-footer>
        <?php
            echo $this->Html->script(array(
                    'ls-elements.js'
            ));
        ?>

        <script>
            // Pour le mot de passe oublié, on bascule vers la page confirmant l'envoi du mail
            var loginEl = document.getElementsByTagName('ls-lib-login')[0];
            loginEl.addEventListener('forgotSubmit', function(e) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function() {
                    if (xhr.readyState === 4){
                        loginEl.setAttribute('current-page', 'forgot-success');
                    }
                };
                xhr.open('POST', '/users/mdplost');
                xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                xhr.send();
            });

            // Pour afficher le message d'attente lorsque l'on se connecte
            document.getElementById('ls-login-form').addEventListener('submit', function(e) {
                document.getElementById('waiter').style.display = 'block';

                $('#comptesdemoDiv').hide();
                $('.btn.waiter').html('Afficher les comptes de démonstration');
            });

            // Pour le tableau des logins de démonstration
            <?php if (SHOW_LOGIN_TABLE) { ?>
                gui.addbutton({
                    element: $('.login-form-addons .show_login_table'),
                    button: {
                        class: "btn btn-primary btn-inverse waiter",
                        content: "Afficher les comptes de démonstration",
                        action: function () {
                            $('.show_login_table').append( $('#comptesdemoDiv'));
                            $('#comptesdemoDiv').toggle();
                        }
                    }
                });

                $('.btn.waiter').css('cursor', 'pointer');
                $('.btn.waiter').css('background-color', 'transparent');
				$('.btn.waiter').css('color', '#5397a7');
				$('.btn.waiter').css('border-color', '#3C7582');
                $('.btn.waiter').css('text-shadow', 'none');
                $('.btn.waiter').css('margin-bottom', '10px');
                $('.btn.waiter').addClass('float-right');
                $('.btn.waiter').hover(function () {
					$('.btn.waiter').css('background-color', '#5397a7');
					$('.btn.waiter').css('color', 'white');
					$('.btn.waiter').css('border-color', '#5397a7');
                }, function () {
					$('.btn.waiter').css('background-color', 'transparent');
					$('.btn.waiter').css('color', '#5397a7');
					$('.btn.waiter').css('border-color', '#3C7582');
                });

                // On ren omme l'action pour le bouton
                $('.btn.waiter').click(function () {
                    $('.btn.waiter').html('Masquer les comptes de démonstration');
                    if ($('#comptesdemoDiv').css('display') == 'block') {
                        $('.btn.waiter').html('Masquer les comptes de démonstration');
                    } else {
                        $('.btn.waiter').html('Afficher les comptes de démonstration');
                    }
                });


                // On sélectionne une ligne et la valeur remplit les champs
                $('#comptesdemo tr').css('cursor', 'pointer').hover(function () {
                    $(this).addClass(' alert-info');
                }, function () {
                    $(this).removeClass(' alert-info');
                }).click(function () {
                    $('input[name="data[User][username]"]').val($('td:nth-child(1)', this).html());
                    $('input[name="data[User][password]"]').val($('td:nth-child(2)', this).html());
                });
            <?php } ?>
		</script>
    </body>
</html>
