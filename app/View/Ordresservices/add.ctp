<?php

/**
 *
 * Ordreservices/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arn aud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
//echo  $this->Html->css(array('administration'), null, array('inline' => false));
?>
<div class="row">
    <div class="panel panel-default">
        <div class="panel-heading"> <button data-dismiss="modal" class="close">×</button>
            <h4 class="panel-title"><?php echo __d('sousactivite', 'Sousactivite.edit'); ?></h4>
        </div>
        <div class="panel-body">
        <?php

        $formOrdresservices = array(
                'name' => 'Ordreservice',
                'label_w' => 'col-sm-5',
                'input_w' => 'col-sm-5',
                'form_url' =>array('controller' => 'marches', 'action' => 'add'),
                'input' => array(
                    'Ordreservice.numero' => array(
                        'labelText' =>__d('ordreservice', 'Ordreservice.numero'),
                        'inputType' => 'text',
                        'items'=>array(
                            'required'=>true,
                            'type'=>'text',
                            'value' => $numeroOS
                        )
                    ),
                    'Ordreservice.organisme_id' => array(
                        'labelText' =>__d('ordreservice', 'Ordreservice.organisme_id'),
                        'inputType' => 'select',
                        'items'=>array(
                            'type' => 'select',
                            'options' => $orgs,
                            'empty' => true
                        )
                    ),
                    'Ordreservice.objet' => array(
                        'labelText' =>__d('ordreservice', 'Ordreservice.objet'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Ordreservice.montant' => array(
                        'labelText' =>__d('ordreservice', 'Ordreservice.montant'),
                        'inputType' => 'text',
                        'items'=>array(
                            'type'=>'text'
                        )
                    ),
                    'Ordreservice.marche_id' => array(
                        'inputType' => 'hidden',
                        'items'=>array(
                            'type'=>'hidden',
                            'value'=>$this->request->params['pass'][0]
                        )
                    )
                )
            );
            echo $this->Formulaire->createForm($formOrdresservices);
            echo $this->Form->end();
        ?>
        </div>
        <div class="panel-footer controler " role="group">
            <a id="cancel" class="btn btn-danger-webgfc btn-inverse "><i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler</a>
            <a id="valid" class="btn btn-success "><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</a>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('#OrdreserviceOrganismeId').select2({allowClear: true, placeholder: "Sélectionner un organisme"});

    $('#OrdreserviceNumero').change(function () {
        var marcheid = "<?php echo $this->request->params['pass'][0];?>";
        gui.request({
            url: "/ordresservices/ajaxformdata/numero/" + marcheid,
            data: $('#OrdreserviceAddForm').serialize()
        }, function (data) {
            processJsonFormCheck(data);
        });
    });
    $('#valid').click(function () {
        var url = "<?php echo Router::url(array('controller' => 'ordresservices', 'action' => 'add', $marcheId)); ?>";
        var form = $(this).parents('.modal').find('form');
        if (form_validate(form)) {
            gui.request({
                url: url,
                data: form.serialize()
            }, function () {
                loadValues();
            });
            affichageTab();
        } else {
            swal({
                    showCloseButton: true,
                title: "Oops...",
                text: "Veuillez vérifier votre formulaire!",
                type: "error",

            });
        }

    });
    $('#cancel').click(function () {
        loadValues();
        affichageTab();
    });

</script>



