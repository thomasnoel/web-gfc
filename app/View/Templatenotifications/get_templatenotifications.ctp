<?php

/**
 *
 * Templatenotifications/index.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>

<?php if( !empty($templatenotifications) ) {
    $fields = array(
        'name',
        'subject',
        'object'
    );


    $actions = array(
        'edit' => array(
            "url" => Configure::read('BaseUrl') . '/templatenotifications/edit',
            "updateElement" => "$('#infos .content')",
            "formMessage" => false
        ),
        'delete' => array(
            "url" => Configure::read('BaseUrl') . '/templatenotifications/delete',
            "updateElement" => "$('#infos .content')",
            "refreshAction" => "loadTemplatenotification();"
        )
    );

    $options = array();
    $data = $this->Liste->drawTbody($templatenotifications, 'Templatenotification', $fields, $actions, $options);
?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>
<div  class="bannette_panel panel-body">
    <table id="table_templatemails"
           data-toggle="table"
           data-search="true"
           data-locale = "fr-CA"
           data-height=screenHeight
           data-show-refresh="false"
           data-show-toggle="false"
           data-show-columns="true"
           data-toolbar="#toolbar"
           data-pagination = "true"
           >
    </table>
</div>

<script>
    //title legend (nombre de données)
    if (!$('.table-list h3 span').length < 1) {
        $('.table-list h3 span').remove();
    }
    $('.table-list h3').append('<?php echo $this->Liste->drawPanelHeading($templatenotifications,$options); ?>');
    $('#table_templatemails')
            .bootstrapTable({
                data:<?php echo $data;?>,
                columns: [
                    {
                        field: "name",
                        title: "<?php echo __d("templatenotification","Templatenotification.name"); ?>",
                        class: "name"
                    },
                    {
                        field: "subject",
                        title: "<?php echo __d("templatenotification","Templatenotification.subject"); ?>",
                        class: "subject"
                    },
                    {
                        field: "object",
                        title: "<?php echo __d("templatenotification","Templatenotification.object"); ?>",
                        class: "object"
                    },
                    {
                        field: "edit",
                        title: "Modifier",
                        class: "actions thEdit",
                        width: "80px",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "Supprimer",
                        class: "actions thDelete",
                        width: "80px",
                        align: "center"
                    }
                ]
            });

    $(document).ready(function () {
        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });
    });
    function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.49);
    }

</script>
<?php
        echo $this->LIste->drawScript($templatenotifications, 'Templatenotifications', $fields, $actions, $options);
    }else{
        echo $this->Html->tag('div', __d('templatenotification', 'Templatenotification.void'), array('class' => 'alert alert-warning'));
    }
?>
