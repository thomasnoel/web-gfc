<?php

/**
 *
 * Environnement/superadminenv.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="">
    <div class="col-sm-6 ">
        <div class="table-list row superadmin-table-list">
            <h3><?php echo __d('menu', 'Collectivites'); ?></h3>
            <div class="group-btn-admin">
                <a href="/collectivites" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionCollectivites'); ?></a>
                <a href="/authentifications" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'Authentification'); ?></a>
                <a href="/checks/testgedooo" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Test d'impression"); ?></a>
                <a href="/checks/getApiKey" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Clé d'API globale"); ?></a>
            </div>
        </div>
    </div>
    <div class="col-sm-6 ">
        <div class="table-list row superadmin-table-list">
            <h3><?php echo __d('menu', 'Outils'); ?></h3>
            <div class="group-btn-admin">
                <a href="/checks/index" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'checkInstall'); ?></a>
                <a href="/checks/get_test_mail" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Test d'envoi de mail"); ?></a>
                <a href="/consoles/index" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Console d'administration"); ?></a>
                <a href="/configurations/config" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Configuration de la page d'accueil"); ?></a>
				<a href="/users/index_superadmin" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Gestion des super-administrateurs"); ?></a>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $('#adminEnv td span.bttn').button();
</script>
