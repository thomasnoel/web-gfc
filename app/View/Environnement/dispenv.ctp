<?php
/**
 *
 * Environnement/dispenv.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<script>
	var supModalTitle = "<?php echo __d('default', 'Modal.suppressionTitle');?>";
	var supModalContent = "<?php echo __d('default', 'Modal.suppressionContents'); ?>";
	var btnConfirm = "<?php  echo __d('default', 'Button.valid');?>";
	var btnCancel = "<?php  echo __d('default', 'Button.cancel');?>";

	$('.swal2-cancel').css('margin-left', '-320px');
	$('.swal2-cancel').css('background-color', 'transparent');
	$('.swal2-cancel').css('color', '#5397a7');
	$('.swal2-cancel').css('border-color', '#3C7582');
	$('.swal2-cancel').css('border', 'solid 1px');

	$('.swal2-cancel').hover(function () {
		$('.swal2-cancel').css('background-color', '#5397a7');
		$('.swal2-cancel').css('color', 'white');
		$('.swal2-cancel').css('border-color', '#5397a7');
	}, function () {
		$('.swal2-cancel').css('background-color', 'transparent');
		$('.swal2-cancel').css('color', '#5397a7');
		$('.swal2-cancel').css('border-color', '#3C7582');
	});
	function messageHideShow() {
		$('#message_compos').toggle("slow");
	}
</script>
<?php
echo $this->Bannette->importScript();
?>
<?php
echo $this->Form->create('DispForm');
?>
<input type="hidden" name="data[User][id]" value="<?php echo $userId; ?>" />
<div class="" id="dispenv_tabs">
	<div class="row bannette-content">
		<div class="table-list">
			<div class="infoParentDiv"></div>
			<ul class="nav nav-tabs titre" role="tablist">
				<?php
				$j = 1;
				foreach ($bannettes as $bannetteName => $bannetteContent) {
					$alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));
					$path = "paging.{$alias}.count";
					$countFlux = Hash::get($this->request->params, $path);
					if($tab == null && $j==1){
						echo ' <li class="active">'
								. '<a href="#'.$bannetteName.'" role="tab" data-toggle="tab">'
								.__d('bannette', 'Bannette.' . $bannetteName).'   ' .$this->Html->tag('span', ' : ' . $countFlux)
								. '</a>'
								.'</li>';
					}else if($tab!=null && $bannetteName == $tab){
						echo ' <li class="active">'
								. '<a href="#'.$bannetteName.'" role="tab" data-toggle="tab">'
								.__d('bannette', 'Bannette.' . $bannetteName).'   ' .$this->Html->tag('span', ' : ' . $countFlux)
								. '</a>'
								.'</li>';
					}else{
						echo ' <li class="">'
								. '<a href="#'.$bannetteName.'" role="tab" data-toggle="tab">'
								.__d('bannette', 'Bannette.' . $bannetteName).'   ' .$this->Html->tag('span', ' : ' . $countFlux)
								. '</a>'
								.'</li>';
					}
					$j++;
				}
				?>
			</ul>
			<div class="tab-content container">
				<?php
				$i = 1;
				foreach ($bannettes as $bannetteName => $bannetteContent) {
				$bannetteOptions = array(
						'sortable' => false,
						'checkable' => true,
						'filter' => true,
						'translateBannetteName' => true,
						'date' => 'created',
						'disp' => false,
						'unwantedFields' => array(
								'retard',
								'direction',
								'creatorId',
								'userconnectedId',
								'objet',
								'contact',
								'typesoustype',
								'modified', //Arnaud
								'mailRetardEnvoye' //Arnaud
						)
				);
				if ($bannetteName == 'aiguillage') {
					$bannetteOptions['disp'] = true;
				} else {
					$bannetteOptions['checkable'] = true;
				}

				if(!$tab==null && $tab==$bannetteName){
				?>
				<div class="tab-pane fade active in" id="<?php echo $bannetteName; ?>" >
					<?php
					echo $this->Bannette->bannettePaginator($bannetteName);
					}else if($i==1 && $tab==null){
					?>
					<div class="tab-pane fade active in" id="<?php echo $bannetteName; ?>" >
						<?php
						echo $this->Bannette->bannettePaginator($bannetteName);
						}else{
						echo $this->Bannette->bannettePaginator($bannetteName);
						?>
						<div class="tab-pane fade" id="<?php echo $bannetteName; ?>" >
							<?php
							}
							?>

							<script type="text/javascript">
								var screenHeight = $(window).height() * 0.5;
							</script>
							<table id="Table_<?php echo $bannetteName; ?>"
								   data-toggle="table"
								   data-height=screenHeight
								   data-search="true"
								   data-locale = "fr-CA"
								   data-show-refresh="false"
								   data-show-toggle="false"
								   data-show-columns="true"
								   data-toolbar="#toolbar">
								<thead>
								<tr>
									<?php
									echo $this->Bannette->bannetteHead($bannetteContent, $bannetteName, $bannetteOptions);
									?>
								</tr>
								</thead>
							</table>
						</div>
						<?php
						$i++;
						}
						?>
					</div>
				</div>
				<div id="controls" class="controls " role="group">

				</div>
			</div>
		</div>
		<div id="flux_context" class="container" >

		</div>
		<?php echo $this->Form->end(); ?>
		<script type="text/javascript">


			$(document).ready(function () {
				var fluxChoix = [];
				var desktopChoix = [];
				<?php
				foreach ($bannettes as $bannetteName => $bannetteContent) {
				$bannetteOptions = array(
						'sortable' => false,
						'checkable' => true,
						'filter' => true,
						'translateBannetteName' => true,
						'date' => 'created',
						'disp' => false,
						'unwantedFields' => array(
								'retard',
								'direction',
								'creatorId',
								'userconnectedId',
								'objet',
								'contact',
								'typesoustype',
								'modified', //Arnaud
								'mailRetardEnvoye' //Arnaud
						)
				);
				if(!empty($bannetteContent)){
				?>
				var data = <?php echo $this->Bannette->bannetteTableTd($bannetteContent, $bannetteName, $bannetteOptions); ?>;
				$("#Table_<?php echo $bannetteName; ?>")
					.bootstrapTable('load', data)
					.on('all.bs.table', function (e, data) {
						$('.unread').each(function () {
							if ($(this).val() == '-1') {
								$(this).parents('tr').addClass('unread');
							}
						});
						$('.thName').find('i').each(function () {
							var fluxId = $(this).attr('id').substring(5);
							$(this).hover(function () {
								$('#flux_' + fluxId).show();
							}, function () {
								$('#flux_' + fluxId).hide();
							});
						});
					})
					.on('click-row.bs.table', function (e, row, element) {
						if (!$(element.context).hasClass('thActions') && !$(element.context).hasClass('bs-checkbox')) {
							var href = element.find('.thEdit').find('a').attr('href');
							if( href == undefined ) {
								var href = element.find('.thView').find('a').attr('href');
							}
							$(location).attr('href', href);
						}
					})
					.on('check.bs.table', function (e, row) {
						var fluxId = row['id'];
						fluxChoix.push(fluxId);
						var fluxChoixNew = fluxChoix;
						isCopy(fluxChoixNew);
						var desktopId = row['desktopId'];
						desktopChoix.push(desktopId);
					})
					.on('uncheck.bs.table', function (e, row) {
						var fluxId = row['id'];
						fluxChoix = jQuery.grep(fluxChoix, function (value) {
							return value != fluxId;
						});
						var fluxChoixNew = fluxChoix;

						isCopy(fluxChoixNew);
						var desktopId = row['desktopId'];
						desktopChoix = jQuery.grep(desktopChoix, function (valueDesktop) {
							return valueDesktop != desktopId;
						});
					})
					.on('check-all.bs.table', function (e, rows) {
						$.each(rows, function (i, row) {

							var fluxId = row['id'];
							if (jQuery.inArray(fluxId, fluxChoix)) {
								fluxChoixNew = fluxChoix.push(fluxId);
							}

							isCopy(fluxChoixNew);
							var desktopId = row['desktopId'];
							if (jQuery.inArray(desktopId, desktopChoix)) {
								desktopChoixNew = desktopChoix.push(desktopId);
							}
						});
					})
					.on('uncheck-all.bs.table', function (e, rows) {
						$.each(rows, function (i, row) {
							var fluxId = row['id'];
							fluxChoix = jQuery.grep(fluxChoix, function (value) {
								return value != fluxId;
							});

							isCopy(fluxChoixNew);
							var desktopId = row['desktopId'];
							desktopChoix = jQuery.grep(desktopChoix, function (valueDesktop) {
								return valueDesktop != desktopId;
							});
						});
						var fluxChoixNew = fluxChoix;
					})
					.on('sort.bs.table', function (e, name, order) {
						var bannetteName = $(this).parents('.tab-pane.fade.active.in').attr('id');
						var nb = parseInt($('.nav.nav-tabs.titre li.active a span').html().substr(3));
						var colonne;
						switch (name) {
							case 'priorite':
								colonne = 'Courrier.priorite';
								break;
							case 'retard':
								colonne = 'Courrier.mail_retard_envoye';
								break;
							case 'reference':
								colonne = 'Courrier.reference';
								break;
							case 'contact':
								colonne = 'Organisme.name';
								break;
							case 'date':
								colonne = 'Courrier.datereception';
								break;
							case 'typesoustype':
								colonne = 'Type.name';
								break;
							case 'bureau':
								colonne = 'Desktop.name';
								break;
							case 'commentaire':
								colonne = 'Courrier.commentaire';
								break;
							case 'modified':
								colonne = 'Courrier.modified';
								break;
						}
						if (nb > 20) {
							gui.request({
								url: "/environnement/setSessionPagination/" + bannetteName + '/' + colonne,
								loader: true,
								loaderMessage: gui.loaderMessage,
								updateElement: $('.tab-content ')
							}, function (data) {
							});
						}
					});


				$(window).resize(function () {
					$("#Table_<?php echo $bannetteName; ?>").bootstrapTable( 'resetView' , {height: screenHeight} );
				});
				<?php
				};
				};
				?>
				$('.infoParentDiv').hide();
				$('.pagination').each(function () {
					var tab = $(this).attr('class').split('_')[0];
					$(this).insertAfter("#" + tab + " .bootstrap-table").show();
				});

				$('.pagination li').click(function (e) {
					if (!$(e.target).is('a')) {
						window.location.href = $('a', this).attr('href');
					}
				});

				//actions des boutons de #controls
				gui.buttonbox({element: $("#controls")});

				//bouton pour consulter les fichier scané
				<?php if ($isAuthFusionScan) { ?>
				gui.addbutton({
					element: $('#controls'),
					button: {
//                        content: "<i class='fa fa-folder-open'></i>",
						content: "<i class='fa fa-list'></i>",
						title: '<?php echo __d('courrier', 'Button.fichierScane'); ?>',
						action: function () {
							gui.request({
								url: "<?php echo Configure::read('BaseUrl') . "/documents/getFichierScane"; ?>",
								updateElement: $('#dispenv_tabs'),
								loader: true,
								loaderMessage: gui.loaderMessage
							});
						}
					}
				});
				<?php }?>

				gui.addbutton({
					element: $('#controls'),
					button: {
						content: "<i class='fa fa-paper-plane'></i>",
						title: '<?php echo __d('courrier', 'Button.sendMultiple'); ?>',
						action: function () {
							if (fluxChoix.length == 0) {
								swal({
									showCloseButton: true,
									title: "Oops...",
									text: "Veuillez choisir au moins un flux",
									type: "error",

								});
							} else if( $('#controls a fa fa-unlock-alt').hasClass('ui-state-disabled') ) {
								swal({
									showCloseButton: true,
									title: "Oops...",
									text: "Ce (ou ces) flux ne peu(ven)t être déplacés",
									type: "error",

								});
							} else {
								var form = "<form id='formChecked'>";
								for (i in fluxChoix) {
									form += '<input type="hidden" name="data[DispForm][itemToLink][]" value="' + fluxChoix[i] + '">';
								}
								form += "</form>";
								gui.formMessage({
									updateElement: $('#DispFormDispenvForm'),
									loader: true,
									width: '550px',
									loaderMessage: gui.loaderMessage,
									title: '<?php echo __d('courrier', 'Courrier.dispatching'); ?>',
									data: $(form).serialize(),
									url: '/courriers/dispmultiplesend/',
									buttons: {
										"<?php echo __d('default', 'Button.cancel'); ?>": function () {
											$(this).parents('.modal').modal('hide');
											$(this).parents('.modal').empty();
										},
										"<?php echo __d('default', 'Button.submit'); ?>": function () {
											var formDate = $(this).parents('.modal').find('form');
											if (form_validate(formDate)) {
												gui.request({
													url: '/courriers/dispmultiplesend/',
													data: $('#CourrierDispmultiplesendForm').serialize(),
													loaderElement: $('#DispFormDispenvForm'),
													loaderMessage: gui.loaderMessage
												}, function (data) {
													gui.request({
														url: '/environnement/index/0/disp/',
														loader: true,
													}, function (data) {
														layer.msg('Les informations ont été enregistrées', {});
														window.location.reload(); // arnaud
													});
												});
												$(this).parents('.modal').modal('hide');
												$(this).parents('.modal').empty();
											} else {
												swal({
													showCloseButton: true,
													title: "Oops...",
													text: "Veuillez vérifier votre formulaire!",
													type: "error",

												});
											}
										}
									}
								});
							}

						}
					}
				});

				gui.addbutton({
					element: $('#controls'),
					button: {
						content: "<i class='fa fa-unlock-alt'></i>",
						title: '<?php echo __d('courrier', 'Button.detachablelot'); ?>',
						action: function () {
							if (fluxChoix.length == 0) {
								swal({
									showCloseButton: true,
									title: "Oops...",
									text: "Veuillez choisir au moins un flux",
									type: "error",

								});
							} else {
								$.ajax({
									type: 'post',
									dataType: 'json',
									url: '/Courriers/isInCopy',
									data: {fluxChoix: fluxChoix},
									success: function (data) {
										if (data['incopy'] == false) {
											swal({
												showCloseButton: true,
												title: "Oops...",
												text: 'Vous avez au moins un flux qui n\'est pas en copie,<br/> Veuillez vérifier!',
												type: "error",

											});
										} else {
											var form = "<form id='formChecked'>";
											for (i in fluxChoix) {
												form += '<input type="hidden" name="data[DispForm][itemToLink][]" value="' + fluxChoix[i] + '">';
												form += '<input type="hidden" name="data[DispForm][checkDesktop][]" value="' + desktopChoix[i] + '">';
											}
											form += "</form>";
											swal({
												showCloseButton: true,
												title: "<?php echo __d('default', 'Confirmation de détachement'); ?>",
												text: "<?php echo __d('default', 'Voulez-vous détacher ces éléments de votre bannette ?'); ?>",
												type: "warning",
												showCancelButton: true,
												confirmButtonColor: "#d33",
												cancelButtonColor: "#3085d6",
												confirmButtonText: '<i class="fa fa-check" aria-hidden="true"></i> Valider',
												cancelButtonText: '<i class="fa fa-times-circle-o" aria-hidden="true"></i> Annuler',
											}).then(function (data) {
												if (data) {
													gui.request({
														url: "/courriers/detachelot",
														data: $(form).serialize(),
														loader: true,
														loaderMessage: gui.loaderMessage
													}, function (data) {
														window.location.href = "/environnement/index/0/disp";
													});
												} else {
													swal({
														showCloseButton: true,
														title: "Annulé!",
														text: "Vous n\'avez pas détaché.",
														type: "error",

													});
												}
											});
											$('.swal2-cancel').css('margin-left', '-320px');
											$('.swal2-cancel').css('background-color', 'transparent');
											$('.swal2-cancel').css('color', '#5397a7');
											$('.swal2-cancel').css('border-color', '#3C7582');
											$('.swal2-cancel').css('border', 'solid 1px');

											$('.swal2-cancel').hover(function () {
												$('.swal2-cancel').css('background-color', '#5397a7');
												$('.swal2-cancel').css('color', 'white');
												$('.swal2-cancel').css('border-color', '#5397a7');
											}, function () {
												$('.swal2-cancel').css('background-color', 'transparent');
												$('.swal2-cancel').css('color', '#5397a7');
												$('.swal2-cancel').css('border-color', '#3C7582');
											});
										}
									}
								});
							}
						}
					}
				});

				changeTableBannetteHeight();
				$(window).resize(function () {
					changeTableBannetteHeight();
				});

				// On clique sur la colonne Référence pour trier et réorganiser les colonnes
				$("th.thRef div").click();
			});

			function isCopy(fluxChoix) {
				var form = "<form id='formChecked'>";
				for (i in fluxChoix) {
					form += '<input type="hidden" name="data[InCopy][]" value="' + fluxChoix[i] + '">';
				}
				form += "</form>";
				gui.request({
					url: '/Courriers/isInCopy',
					data: $(form).serialize(),
					loader: false,
				}, function (data) {
					if (data == '{"incopy":false}') {
						gui.disablebutton({
							element: $('#controls'),
							button: '<i class="fa fa-unlock-alt"></i>'
						});
					}
					else {
						gui.enablebutton({
							element: $('#controls'),
							button: '<i class="fa fa-unlock-alt"></i>'
						});
					}
				});
			}

			function changeTableBannetteHeight() {
				$(".fixed-table-container").css('height', $(window).height() * 0.59);
				$(".fixed-table-container").css('overflow', 'hidden');
			}
		</script>
