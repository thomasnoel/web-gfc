<?php

/**
 *
 * Environnement/historique.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
    echo $this->Bannette->importScript();
?>
<div class="">
    <div class="row">
        <div class="table-list">
            <div class="infoParentDiv"></div>
            <ul class="nav nav-tabs titre" role="tablist">
                <?php
                    $j = 1;
                    foreach ($historique as $bannetteName => $bannetteContent) {

                        $bannetteNameAffichie = $bannetteName;
                        $alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));
//                        $path = "paging.{$alias}.count";
//                        $countFlux = Hash::get($this->request->params, $path);
                        $countFlux = $number[$bannetteName]; //TODO

                        $bannetteName=str_replace(" ","",ucwords($bannetteName));
                        $bannetteName=preg_replace("/[[:punct:]]/i","",$bannetteName);
                        if(($tabName == null && $j==1) ||($tabName!=null && $bannetteName == $tabName)){
                            echo ' <li class="active">'
                                    . '<a href="#'.$bannetteName.'" role="tab" data-toggle="tab">'
                                    .$bannetteNameAffichie.'   ' .$this->Html->tag('span', ' : ' . $countFlux)
                                    . '</a>'
                                .'</li>';
                        }else{
                            echo ' <li class="">'
                                    . '<a href="#'.$bannetteName.'" role="tab" data-toggle="tab">'
                                    .$bannetteNameAffichie.'   ' .$this->Html->tag('span', ' : ' . $countFlux)
                                    . '</a>'
                                .'</li>';
                        }
                        $j++;
                    }
                ?>

            </ul>

			<div class="alert alert-info" style="margin-top:5px;margin-bottom:-15px;"><b>Attention : l'historique ne propose que les flux créés depuis <?php echo substr( Configure::read('Bannettehistorique.Restrictionmax'), 0, 3 );?> <?php echo $translationType;?>. Pour consulter les flux antérieurs, veuillez passer par la recherche</b></div>

			<div class="tab-content container" style="padding:20px;">
                <?php
                    $i = 1;
                    foreach ($historique as $bannetteName => $bannetteContent) {

                            $bannetteNameSepare = $bannetteName;
                            $bannetteName=str_replace(" ","",ucwords($bannetteName));
                            $bannetteName=preg_replace("/[[:punct:]]/i","",$bannetteName);
                            $bannetteOptions = array(
                                    'sortable' => false,
                                    'checkable' => false,
                                    'filter' => true,
                                    'translateBannetteName' => false,
                                    'date' => 'datereception'
                            );
                           if(!$tabName==null && $tabName==$bannetteName){
                    ?>
                <div class="tab-pane fade active in" id="<?php echo $bannetteName; ?>" >
                    <?php
                                if( !empty( $this->request->data['Courrier'] ) ) {
                                    if( empty( $bannetteContent[0]['id'] ) ) {
                                        echo $this->Html->tag('div', __d('courrier', 'Courrier.Search.void'), array('class' => 'alert alert-warning'));
                                    }
                                }
                                echo $this->Bannette->bannettePaginator($bannetteName);
                            }else if($i==1 && $tabName==null){
                    ?>
                    <div class="tab-pane fade active in" id="<?php echo $bannetteName; ?>" >
                    <?php

                                if( !empty( $this->request->data['Courrier'] ) ) {
                                    if( empty( $bannetteContent[0]['id'] ) ) {
                                       echo $this->Html->tag('div', __d('courrier', 'Courrier.Search.void'), array('class' => 'alert alert-warning'));
                                    }
                                }
                                echo $this->Bannette->bannettePaginator($bannetteName);
                            }else{
                    ?>
                        <div class="tab-pane fade" id="<?php echo $bannetteName; ?>" >
                    <?php
                                if( !empty( $this->request->data['Courrier'] ) ) {
                                    echo $this->Html->tag('div', __d('courrier', 'Courrier.Search.void'), array('class' => 'alert alert-warning'));
                                }
                                echo $this->Bannette->bannettePaginator($bannetteName);

                            }
                    ?>

<script type="text/javascript">
    var screenHeight = $(window).height() * 0.5;
</script>

                    <div  class="bannette_panel panel-body" >
                            <span class="bouton-search action-title pull-right"  role="group" style="margin-top: 10px; margin-left: 5px;">
                                <a href="#" class="btn btn-info-webgfc btn-inverse" data-target="#EnvironnementGetFluxFormModal" data-toggle="modal" title="Rechercher"> <i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
                                <a href="#" class="btn btn-info-webgfc btn-inverse" id="searchCancel" title="Réinitialiser" style="margin-left: 15px;"><i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser</a>
                            </span>
                            <table id="Table_<?php echo $bannetteName; ?>"
                                   data-toggle="table"
                                   data-height=screenHeight
                                   data-search="true"
                                   data-locale = "fr-CA"
                                   data-show-refresh="false"
                                   data-show-toggle="false"
                                   data-show-columns="true"
                                   data-toolbar="#toolbar">
                                <thead>
                                    <tr>
                                            <?php
                                            echo $this->Bannette->bannetteHead($bannetteContent, $bannetteName, $bannetteOptions);
                                            ?>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                   <?php
                        $i++;
                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.keep-open btn-group.dropdown-toggle').css( 'margin-right', '-600px!' );
    $('.keep-open btn-group.dropdown-toggle').css( 'margin-left', '200px' );
    $('.fixed-table-toolbar .dropdown-toggle').css( 'color', 'red' );
    $(document).ready(function () {
        //charger les contenus de tableaux
        var fluxChoix = [];
        <?php
        foreach ($historique as $bannetteName => $bannetteContent) {
            $bannetteOptions = array(
                'sortable' => false,
                'checkable' => true,
                'filter' => true,
                'translateBannetteName' => true,
                'date' => 'datereception'
           );
           if(!empty($bannetteContent)){
               $bannetteName=str_replace(" ","",ucwords($bannetteName));
               $bannetteName=preg_replace("/[[:punct:]]/i","",$bannetteName);
       ?>
        var data = <?php echo $this->Bannette->bannetteTableTd($bannetteContent, $bannetteName, $bannetteOptions); ?>;
        $('.tab-pane.active.in').find('#Table_<?php echo $bannetteName; ?>').bootstrapTable();
        $("#Table_<?php echo $bannetteName; ?>")
                .bootstrapTable('load', data)
                .on('all.bs.table', function (e, data) {
                    $('.unread').each(function () {
                        if ($(this).val() == '-1') {
                            $(this).parents('tr').addClass('unread');
                        }
                    });
                    $('.thName').find('i').each(function () {
                        var fluxId = $(this).attr('id').substring(5);
                        $(this).hover(function () {
                            $('#flux_' + fluxId).show();
                        }, function () {
                            $('#flux_' + fluxId).hide();
                        });
                    });
                })
                .on('click-row.bs.table', function (e, row, element) {
                    if (!$(element.context).hasClass('thActions') && !$(element.context).hasClass('bs-checkbox')) {
                        var href = element.find('.thView').find('a').attr('href');
                        window.open(href);
                    }
                })
                .on('sort.bs.table', function (e, name, order) {
                    var bannetteName = $(this).parents('.tab-pane.fade.active.in').attr('id');
                    var nb = parseInt($('.nav.nav-tabs.titre li.active a span').html().substr(3));
                    var colonne;
                    switch (name) {
                        case 'priorite':
                            colonne = 'Courrier.priorite';
                            break;
                        case 'retard':
                            colonne = 'Courrier.mail_retard_envoye';
                            break;
                        case 'reference':
                            colonne = 'Courrier.reference';
                            break;
                        case 'contact':
                            colonne = 'Organisme.name';
                            break;
                        case 'date':
                            colonne = 'Courrier.datereception';
                            break;
                        case 'typesoustype':
                            colonne = 'Type.name';
                            break;
                        case 'bureau':
                            colonne = 'Desktop.name';
                            break;
                        case 'commentaire':
                            colonne = 'Courrier.commentaire';
                            break;
                        case 'modified':
                            colonne = 'Courrier.modified';
                            break;
                    }
                    if (nb > 20) {
                        //a vérifier
                        gui.request({
                            url: "/environnement/setSessionPagination/" + bannetteName + '/' + colonne,
                            loader: true,
                            loaderMessage: gui.loaderMessage,
                            updateElement: $('.tab-content ')
                        }, function (data) {
                            /*if (data) {
                                window.location.href = window.location.href;
                            }*/
                        });
                    }
                });

                $(window).resize(function () {
                    $("#Table_<?php echo $bannetteName; ?>").bootstrapTable( 'resetView' , {height: screenHeight} );
                });
    <?php
            };
        };
    ?>

        $('.infoParentDiv').hide();

        $('#TypeType').select2();
        $('#SoustypeSoustype').select2();
        $('#CourrierEtat').select2({allowClear: true, placeholder: "Sélectionner un statut"});
        $('#searchCancel').click(function () {
            location.reload();
        });
        $('.table-list ul.nav-tabs .retour').css('float', 'right');
        $('.table-list ul.nav-tabs .retour').attr('title', 'Tous les flux dans ce bannette');

        $('.nav-tabs a').click(function () {
            if (parseInt($(this).find('span').text().replace(':', '')) > 20) {
                var bannette = $(this).attr('href').replace('#', '');
                $(this).removeAttr("href");
                window.location.href = "<?php echo Configure::read('BaseUrl') . "/environnement/historique/"; ?>" + bannette;
            }
        });

        $('.pagination').each(function () {
            var tab = $(this).attr('class').split('_')[0];
            $(this).insertAfter("#" + tab + " .bootstrap-table").show();
        });

        $('.pagination li').click(function (e) {
            if (!$(e.target).is('a')) {
                window.location.href = $('a', this).attr('href');
            }
        });

        changeTableBannetteHeight();
        $(window).resize(function () {
            changeTableBannetteHeight();
        });


        // On clique sur la colonne Référence pour trier et réorganiser les colonnes
        $("th.thRef span").click();
    });

	function changeTableBannetteHeight() {
        $(".fixed-table-container").css('height', $(window).height() * 0.65);
        $(".fixed-table-container").css('overflow', 'hidden');
    }
    /*
     function iconInfoParentMouseOver(fluxId) {
     $('.infoParentDiv').show();
     $('.infoParentDiv').append($('#flux_' + fluxId).html());
     $('#hand_' + fluxId).offset();
     $('.infoParentDiv').css({
     position: 'absolute',
     left: $('#hand_' + fluxId).offset().left + 15,
     top: $('#hand_' + fluxId).offset().top + 15
     })
     }
     function iconInfoParentMouseOut(fluxId) {
     $('.infoParentDiv').empty();
     $('.infoParentDiv').hide();
     }
     */
    $(document).ready(function () {
        $('.form_datetime input').datepicker({
            language: 'fr-FR',
            format: "dd/mm/yyyy",
            weekStart: 1,
            autoclose: true,
            todayBtn: 'linked'
        });

        gui.addbutton({
            element: $('#EnvironnementGetFluxFormButton'),
            button: {
                content: '<i class="fa fa-undo" aria-hidden="true"></i> Réinitialiser',
                class: 'btn btn-info-webgfc btn-inverse cancelSearch',
                action: function () {
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();
                }

            }
        });

        gui.addbutton({
            element: $('#EnvironnementGetFluxFormButton'),
            button: {
                content: '<i class="fa fa-search" aria-hidden="true"></i> Rechercher',
                class: 'btn btn-success searchBtn',
                action: function () {
                    var form = $(this).parents('.modal').find('form');
                    gui.request({
                        url: form.attr('action'),
                        data: form.serialize(),
                        loader: true,
                        updateElement: $('#webgfc_content'),
                        loaderMessage: gui.loaderMessage
                    }, function (data) {
                        $('#webgfc_content').empty();
                        $('#webgfc_content').html(data);
                    });
                    $(this).parents('.modal').modal('hide');
                    $(this).parents('.modal').empty();

                }

            }
        });

        $('.form-control').css('height', '34px');



		// On met en rouge les retards
		$("th.thRetard.thState").css('color', 'black');
		$("td.thRetard.thState").css('color', 'blue');
		var hasimg = $("td.thRetard.thState").find('img');
		if( hasimg ) {
			$("td.thRetard.thState").find('img').parent('td').css('color', 'red');
		}

	});
</script>
<div id="EnvironnementGetFluxFormModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content zone-form">
            <div class="modal-header">
                <button data-dismiss="modal" class="close">×</button>
                <h4 class="modal-title">Recherche de flux dans l'historique</h4>
            </div>
            <div class="modal-body">
                            <?php
                        $formEnvironnementSearch = array(
                            'name' => 'Environnement',
                            'label_w' => 'col-sm-5',
                            'input_w' => 'col-sm-6',
							'form_url' => array( 'controller' => 'environnement', 'action' => 'historique' ),
                            'input' => array(
                                'Courrier.reference' => array(
                                    'labelText' =>__d('courrier', 'Courrier.reference'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type' => 'text'
                                    )
                                ),
                                'Courrier.name' => array(
                                    'labelText' =>__d('courrier', 'Courrier.nomrecherche'),
                                    'inputType' => 'text',
                                    'items'=>array(
                                        'type'=>'text'
                                    )
                                ),
                                'Courrier.etat' => array(
                                    'labelText' =>__d('courrier', 'Courrier.etat'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type'=>'select',
                                        'multiple' => true,
//                                        'class' => 'JSelectMultiple',
                                        'options' => $etat,
                                        'empty' => true
                                    )
                                ),
                                'Courrier.datereception' => array(
                                    'labelText' =>__d('courrier', 'Courrier.datereception'),
                                    'inputType' => 'text',
                                    'dateInput' =>true,
                                    'items'=>array(
                                        'type'=>'text',
                                        'readonly' => true,
                                        'class' => 'datepicker',
                                        'data-format'=>'dd/MM/yyyy'
                                    )
                                ),
                                'Courrier.datereceptionfin' => array(
                                    'labelText' =>__d('courrier', 'Courrier.cdatereceptionfin'),
                                    'inputType' => 'text',
                                    'dateInput' =>true,
                                    'items'=>array(
                                        'type'=>'text',
                                        'readonly' => true,
                                        'class' => 'datepicker',
                                        'data-format'=>'dd/MM/yyyy'
                                    )
                                ),
                                'Type.Type' => array(
                                    'labelText' => __d('recherche', 'Recherche.Type'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type' => 'select',
                                        'empty' => true,
//                                        'options' =>   $types,
                                        "options" =>$typeOptions,
                                        'multiple' => true
//                                        'class' => 'JSelectMultiple'
                                    )
                                ),
                                'Soustype.Soustype' => array(
                                    'labelText' => __d('recherche', 'Recherche.Soustype'),
                                    'inputType' => 'select',
                                    'items'=>array(
                                        'type' => 'select',
                                        'empty' => true,
                                        'options' => array(),
                                        'multiple' => true
//                                        'class' => 'JSelectMultiple'
                                    )
                                )
                            )
                        );
                        echo $this->Formulaire->createForm($formEnvironnementSearch);
                        echo $this->Form->end();
                        ?>
            </div>
            <div id="EnvironnementGetFluxFormButton" class="modal-footer controls " role="group">

            </div>
                        <?php echo $this->Form->end();?>
        </div>
    </div>
</div>
<script>
    $('#TypeType').change(function () {
        var type_id = $(this).val();
        var soustypeLists = <?php echo json_encode($soustypeOptions); ?>;
        if ($.isArray(type_id)) {
            $('#SoustypeSoustype option').remove();
            $.each(type_id, function (index, val) {
                $.each(soustypeLists[val], function (index, value) {
                    $('#SoustypeSoustype').append($("<option value='" + index + "'>" + value + "</option>"));
                });
            });
        }
    });
    $('#EnvironnementGetFluxFormModal').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#EnvironnementGetFluxFormButton .searchBtn').trigger('click');
            return false;
        }
        if (e.keyCode === 27) {
            $('#EnvironnementGetFluxFormButton .cancelSearch').trigger('click');
            return false;
        }
    });

    // Ajout de l'action de recherche via la touche Entrée
//    $('#EnvironnementHistoriqueForm').keypress(function (e) {
//        if (e.keyCode == 13) {
//            gui.request({
//                url: $("#EnvironnementHistoriqueForm").attr('action'),
//                data: $("#EnvironnementHistoriqueForm").serialize(),
//                loader: true,
//                updateElement: $('#webgfc_content'),
//                loaderMessage: gui.loaderMessage
//            }, function (data) {
//                $('#webgfc_content').empty();
//                $('#webgfc_content').html(data);
//            });
//            $(this).parents('.modal').modal('hide');
//            $(this).parents('.modal').empty();
//        }
//    });
</script>
