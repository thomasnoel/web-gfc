<?php

/**
 *
 * Environnement/adminenv.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<div class="">
    <div class="row">
        <div class="table-list">
            <h3>Administration</h3>
            <div>
                <ul class="nav nav-tabs titre " role="tablist">
                    <li class="active"><a href="#infos" data-toggle="tab">Gestion de la collectivité</a></li>
                    <li><a href="#donnes" data-toggle="tab">Gestion des données de la collectivité</a></li>
                    <li><a href="#outils" data-toggle="tab">Outils</a></li>
                    <?php if( Configure::read('Conf.SAERP')):?>
                        <li><a href="#operation" data-toggle="tab"><?php echo __d('menu', 'gestionSAERP'); ?></a></li>
                        <li><a href="#events" data-toggle="tab"><?php echo __d('menu', 'contactSAERP'); ?></a></li>
                    <?php endif; ?>
                </ul>

                <div class="panel-body tab-content">
                    <div id="infos" class="tab-pane fade in active">
                        <div class="group-btn-admin">
                            <a href="/collectivites/admin" class="btn btn-success btn-block" role="button"><?php echo __d('default', 'Informations'); ?></a>
                            <a href="/users" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'structureCollectivite'); ?>&nbsp;&nbsp;&nbsp;&nbsp;(<?php echo __d('collectivite', 'Collectivite.nbServices'); ?> : <?php echo $nbServices; ?> - <?php echo __d('collectivite', 'Collectivite.nbUsers'); ?> : <?php echo $nbUsers; ?> - <?php echo __d('collectivite', 'Collectivite.nbProfils'); ?> : <?php echo $nbProfils; ?>)</a>
                            <a href="/connecteurs" class="btn btn-success btn-block" role="button"><?php echo __d('default', 'Connecteurs'); ?></a>
                            <a href="/scanemails" class="btn btn-success btn-block" role="button"><?php echo __d('default', 'Emails'); ?></a>
                        </div>
                    </div>
                    <div id="donnes" class="tab-pane fade">
                        <div class="group-btn-admin">
                            <a href="/originesflux" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionOriginesflux'); ?></a>
                            <a href="/soustypes" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionTypesSoustypes'); ?>&nbsp;&nbsp;&nbsp;&nbsp;(<?php echo __d('collectivite', 'Collectivite.nbTypes'); ?> : <?php echo $nbTypes; ?> - <?php echo __d('collectivite', 'Collectivite.nbSoustypes'); ?> : <?php echo $nbSoustypes; ?>)</a>
                            <a href="/desktopsmanagers" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionBureaux'); ?>&nbsp;&nbsp;&nbsp;&nbsp;(<?php echo __d('collectivite', 'Collectivite.nbBureaux'); ?> : <?php echo $nbBureaux; ?>)</a>
                            <a href="/workflows" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionCircuits'); ?>&nbsp;&nbsp;&nbsp;&nbsp;(<?php echo __d('collectivite', 'Collectivite.nbCircuit'); ?> : <?php echo $nbCircuits; ?>)</a>
                            <a href="/metadonnees" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionMeta'); ?>&nbsp;&nbsp;&nbsp;&nbsp;(<?php echo __d('collectivite', 'Collectivite.nbMeta'); ?> : <?php echo $nbMeta; ?>)</a>
                            <a href="/dossiers" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionDossiersAffaires'); ?>&nbsp;&nbsp;&nbsp;&nbsp;(<?php echo __d('collectivite', 'Collectivite.nbAffaires'); ?> : <?php echo $nbAffaires; ?> - <?php echo __d('collectivite', 'Collectivite.nbDossiers'); ?> : <?php echo $nbDossiers; ?>)</a>
                        </div>
                    </div>
                    <div id="outils" class="tab-pane fade">
                        <div class="group-btn-admin">
                            <!-- <a href="/keywordlists" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionThesaurus'); ?></a>-->
                            <a href="/titres" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionTitre'); ?></a>
                            <a href="/addressbooks" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionAddressbooks'); ?></a>
                            <a href="/activites" class="btn btn-success btn-block" role="button"><?php echo  __d('menu', 'Activites'); ?></a>
                            <a href="/bans" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'gestionBans'); ?></a>
                            <a href="/templatemails" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'templateMails'); ?></a>
                            <a href="/templatenotifications" class="btn btn-success btn-block" role="button"><?php echo __d('menu', 'templateNotifications'); ?></a>
                            <a href="/outils" class="btn btn-success btn-block" role="button"><?php echo __d('menu', "Outils pour l'administrateur"); ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('a.bttn').button();

    $('.disabled').addClass('ui-state-disabled').attr('title', 'En développement ...').click(function () {
        swal(
                'Oops...',
                "En développement ...<br>Cette fonctionnalité n\'est pas encore disponible.",
                'warning'
                );
    });
</script>
