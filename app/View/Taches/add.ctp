<?php

/**
 *
 * Taches/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<?php
$formTache= array(
    'name' => 'Tache',
    'label_w' => 'col-sm-5',
    'input_w' => 'col-sm-5',
    'input' => array(
        'Tache.name' => array(
            'labelText' =>__d('tache', 'Tache.name'),
            'inputType' => 'text',
            'items'=>array(
                'required'=>true,
                'type'=>'text'
            )
        ),
        'Tache.courrier_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
		'Tache.creator_desktop_id' => array(
			'inputType' => 'hidden',
			'items'=>array(
				'type'=>'hidden'
			)
		),
        'Desktop.Desktop' => array(
			'labelText' => 'Utilisateur(s) affecté(s) à cette tâche',
            'inputType' => 'select',
            'items'=>array(
                'required' =>true,
                'type'=>'select',
                'options' => $users,
                'empty' => true,
                'multiple' => true
            )
        ),
        'Tache.delai_nb' => array(
            'labelText' =>__d('tache', 'Tache.delai_nb'),
            'inputType' => 'number',
            'items'=>array(
                'type'=>'number'
            )
        ),
        'Tache.delai_unite' => array(
            'labelText' => __d('tache', 'Tache.delai_unite'),
            'inputType' => 'select',
            'items'=>array(
                'type'=>'select',
                'options' => array('0' => 'jour', '1' => 'semaine', '2' => 'mois'),
                'empty'=>true
            )
        )
    )
);
echo $this->Formulaire->createForm($formTache);
echo $this->Form->end();
?>
<script type="text/javascript">
    $('#DesktopDesktop').select2();
    $('#TacheDelaiUnite').select2();
    var border_legend = $("#TacheDelaiNb").parents('.form-group');
    $("<legend><?php echo __d('tache', 'Tache.delai'); ?></legend>").insertBefore(border_legend);
</script>
