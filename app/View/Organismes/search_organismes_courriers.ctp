<?php
echo $this->Html->script('contactinfo.js', array('inline' => true));
    $formSearch = array(
        'name' => 'searchOrganismesCourriers',
        'label_w' => 'col-sm-4',
        'input_w' => 'col-sm-3',
        'form_url' =>array('controller' => 'organismes', 'action' => 'search_organismes_courriers'),
//        'form_class'=>'pull-right',
        'input' => array(
            'Organisme.name' => array(
                'labelText' =>__d('organisme', 'Organisme.name'),
                'inputType' => 'text',
                'items'=>array(
                    'type' => 'text',
                    'required' => false
                )
            )
        )
    );
?>
<div  class="zone-form">
    <legend>Recherche d'organismes</legend>
        <?php
            echo $this->Formulaire->createForm($formSearch);
        ?>
    <div class="controls  text-center" role="group" >
        <a id="searchButton" class="aere btn btn-success " ><i class="fa fa-search" aria-hidden="true"></i> Rechercher</a>
		<a id="closeButton" class="aere btn btn-info-webgfc " ><i class="fa fa-times" aria-hidden="true"></i> Fermer</a>
    </div>
    <?php echo $this->Form->end(); ?>
</div>

    <?php
        ?>
        <div id="organismesvalues" >
            <legend class="bannette">Liste des organismes</legend>
            <div  class="bannette_panel panel-body">
                <table id="table_organismes_edit"
                       data-toggle="table"
                       data-locale = "fr-CA"
                       data-pagination="true"
                       data-page-size =" 100"
                       >
                </table>
            </div>
        </div>
    <script type="text/javascript">
        //title legend (nombre de données)
        if ($('#organismesvalues .bannette span').length < 1) {
            $('#organismesvalues .bannette ').append('<span> - total : <?php echo count($organismes);?></span>');
        }
        $('#table_organismes_edit')
            .bootstrapTable({
                data:<?php echo json_encode($organismes);?>,
                columns: [
                    {
                        field: 'id',
                        title: "Identifiant",
                        class: 'hidden'
                    },
                    {
                        field: 'name',
                        title: "<?php echo __d('organisme','Organisme.name'); ?>",
                        class: 'name'
                    },
                    {
                        field: 'ville',
                        title: "<?php echo __d('organisme','Organisme.ville'); ?>",
                        class: 'ville'
                    }
                ]
            });
    </script>
<?php
    echo $this->Js->writeBuffer();
?>


<script type="text/javascript">
    $('#searchButton').button();
	$('#closeButton').button();
    $('.zone-form').keypress(function (e) {
        if (e.keyCode == 13) {
            $('#searchButton').trigger('click');
            return false;
        }
    });
    $('#searchButton').click(function () {
        gui.request({
            url: '/organismes/search_organismes_courriers/',
            data: $("#searchOrganismesCourriersSearchOrganismesCourriersForm").serialize(),
            loader: true,
            updateElement: $('#table_organismes_edit'),
            loaderMessage: gui.loaderMessage,
        }, function (data) {
            $('.zone-form').remove();
            $('#organismesvalues').html(data);
        });
    });


		$('#closeButton').click(function () {
			$(this).parents(".modal").modal('hide');
			$(this).parents(".modal").empty();
		});

    $('#table_organismes_edit tbody tr').css('cursor', 'pointer');
    $('#table_organismes_edit tbody tr').click(function () {
        var orgId = $(this).children('.hidden').text();
        var orgName = $(this).children('.name').text();
        $(this).css('background', 'grey');
        $(".modal").modal('hide');
        $(".modal").empty();
        refreshOrgvalue(orgId, orgName);
    });


    function refreshOrgvalue(orgId, orgName) {
        $("#CourrierOrganismeId").append($("<option value='" + orgId + "' selected='selected'>" + orgName + "</option>"));
        $('#CourrierOrganismeId').val(orgId).change();
        $('#HiddenCourrierOrganismeId').val(orgId);
        listContact(orgId);
    }

    function listContact(orgId) {
        gui.request({
            url: '/contacts/searchSansContact/' + orgId
        }, function (data) {
            var json = jQuery.parseJSON(data);
            refreshContactInfos(json['Contact']['id']);
            $('#HiddenCourrierContactId').val(json['Contact']['id']);
        });


    }
</script>
