<?php

/**
 *
 * Addressbooks/get_addressbooks.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author yjin
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$this->Paginator->options(array(
   'update' => '#zone_organisme .panel-body',
   'evalScripts' => true,
   'before' => 'gui.loader({ element: $("#zone_organisme .panel-body"), message: gui.loaderMessage });',
   'complete' => '$(".loader").remove()'
   ));
$pagination = '';
if (Set::classicExtract($this->params, 'paging.Organisme.pageCount') > 1) {
   $pagination = '<div class="pagination">
   ' . implode(
     '', Set::filter(
       array(
          $this->Html->tag('li', $this->Paginator->first('<<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->prev('<', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->numbers(array('separator'=>false))),
          $this->Html->tag('li', $this->Paginator->next('>', array('separator'=>false), null, array('class' => 'disabled'))),
          $this->Html->tag('li', $this->Paginator->last('>>', array('separator'=>false), null, array('class' => 'disabled'))),
          )
       )
     ) . '
   </div>';
}
?>

<table id="table_organisme"
       data-toggle="table"
       data-locale = "fr-CA"
       data-height="499"
       >
</table>
<div>
<?php echo $pagination; ?>
    <?php if(!empty($organismes)):
        $formSearch = array(
            'name' => 'exportOrganisme',
            'label_w' => 'col-sm-4',
            'input_w' => 'col-sm-6',
            'form_url' =>array('controller' => 'organismes', 'action' => 'export'),
            'form_class'=>'pull-right',
            'input' => array('Organisme.addressbook_id' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'Organisme.name' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                ),
                'Organisme.active' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            )
        );
    if( Configure::read('Conf.SAERP')) {
         $formSearch['input']['Organisme.Operation.id']=array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
         );
         /*$formSearch['input']['Organisme.Fonction.id']=array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
         );
         $formSearch['input']['Organisme.Event.id']=array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
         );*/
         $formSearch['input']['Organisme.Activite.id']=array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
         );
    }
    echo $this->Formulaire->createForm($formSearch);
    ?>
    <button type="submit" class="btn btn-info-webgfc pull-right" id="exportResultat" style="margin:20px auto;"><i class="fa fa-download" aria-hidden="true"></i></button>
    <?php
    $this->Form->end();
    endif;
    ?>
</div>
<script>
    $('#table_organisme')
            .bootstrapTable({
                data: <?php echo json_encode($organismes); ?>,
                columns: [
                    {
                        field: "name",
                        title: "Nom",
                        class: "name"
                    },
                    {
                        field: "view",
                        title: "",
                        align: "center"

                    },
                    {
                        field: "edit",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "delete",
                        title: "",
                        align: "center"
                    },
                    {
                        field: "exportcsv",
                        title: "",
                        align: "center"
                    }
                ]
            }).on('search.bs.table', function (e, text) {
        chargerFunctionJquery();
    });
    $(document).ready(function () {
        chargerFunctionJquery();
        $('#exportOrganismeExportForm input').each(function () {
            var idInput = $(this).attr('id').replace('Organisme', 'SearchOrganisme');
            if ($('#searchModal form #' + idInput).val().length != 0) {
                $(this).val($('#searchModal form #' + idInput).val());
            }
        });
    });
    function chargerFunctionJquery() {
        $.getScript("/js/addressbook.js");

       $('.viewOrganisme').click(function () {
            var id = $(this).attr('id').substring(10);
            var href = '/organismes/get_contact/' + id;
            var chargeZone = '#zone_contact .panel-body';
            $("#zone_carnet").removeAttr('class').addClass("col-sm-2 col-sm-2");
            $("#zone_organisme").removeAttr('class').addClass("col-sm-2 col-sm-2");
            $("#zone_contact").removeAttr('class').addClass("col-sm-8 col-sm-8");
            viewItem(href, chargeZone);
        });
        function viewItem(href, chargeZone) {
            gui.request({
                url: href,
                updateElement: $(chargeZone),
                loader: true,
                loaderMessage: gui.loaderMessage
            });
            if (chargeZone == "#infos .content") {
                $('#infos .panel-title').empty();
                $('#infos').modal('show');
            }
        }

        // édition des organismes
        $('.editOrganisme').click(function () {
            var id = $(this).attr('id').substring(10);
            var href = '/organismes/edit/' + id;
            var isSansOrganisme = $(this).parent().parent().text();
            isSansOrganisme.includes("Sans organisme");
            if( !isSansOrganisme.includes("Sans organisme") ) {
                editItem(href);
            }
            else {
                var title = "Modifier l'organisme";
                var text = "Cet organisme ne peut être modifié";
                swal(
                    'Oops...',
                    title + "<br>" + text,
                    'warning'
                );
            }
//            editItem(href);
        });
        // visualisation des organismes
        function editItem(href) {
            $('#infos').modal('show');
            gui.request({
                url: href,
                updateElement: $('#infos .content'),
                loader: true,
                loaderShowDiv: $('#liste .content'),
                loaderMessage: gui.loaderMessage
            });
        }

        // export des organismes
        $('.exportOrganisme').click(function () {
            var id = $(this).attr('id').substring(10);
            var href = '/organismes/export/' + id;
            exportItem(href);
        });
        function exportItem(href) {
            window.location.href = href;
        }
    }
</script>
<?php echo $this->Js->writeBuffer();?>
