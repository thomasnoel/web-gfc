<?php

/**
 *
 * Organismes/add.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
echo $this->Html->script('jquery/jquery.ui.autocomplete.html.js', array('inline' => true));
echo $this->Html->script('formValidate.js', array('inline' => true));
?>
<div class="row">
    <?php
    $formOrganisme = array(
    'name' => 'Organisme',
    'label_w' => 'col-sm-4',
    'input_w' => 'col-sm-8',
    'input' => array(
        'Organisme.id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        ),
        'Organisme.addressbook_id' => array(
            'inputType' => 'hidden',
            'items'=>array(
                'type'=>'hidden'
            )
        )
    )
);
echo $this->Formulaire->createForm($formOrganisme);
?>
    <div class="OrganismeInfo col-sm-6">
 <?php
        $valueDept = isset($this->request->data['Organisme']['ban_id']) ? $this->request->data['Organisme']['ban_id'] : null;
        $valueCommune = isset($this->request->data['Organisme']['bancommune_id']) ? $this->request->data['Organisme']['bancommune_id'] : null;
        $formOrganismeInfos = array(
            'name' => 'Organisme',
            'label_w' => 'col-sm-5',
            'input_w' => 'col-sm-7',
            'input' => array(
                'Organisme.name' => array(
                    'labelText' =>__d('organisme', 'Organisme.name'),
                    'inputType' => 'text',
                    'items'=>array(
                        'required'=>true,
                        'type'=>'text'
                    )
                ),
                'Organisme.email' => array(
                    'labelText' =>__d('organisme', 'Organisme.email'),
                    'inputType' => 'email',
                    'items'=>array(
                        'type'=>'email'
                    )
                ),
                'Organisme.tel' => array(
                    'labelText' =>__d('organisme', 'Organisme.tel'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.portable' => array(
                    'labelText' =>__d('organisme', 'Organisme.portable'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.fax' => array(
                    'labelText' =>__d('organisme', 'Organisme.fax'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.antenne' => array(
                    'labelText' =>__d('organisme', 'Organisme.antenne'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.siret' => array(
                    'labelText' =>__d('organisme', 'Organisme.siret'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Activite.Activite' => array(
                    'labelText' =>__d('organisme', 'Organisme.activite_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'multiple' => true,
                        'options' => $activites
                    )
                )
            )
        );
        if( Configure::read( 'Conf.SAERP') ) {
//            $formOrganismeInfos['input']['Activite.Activite'] = array(
//                'labelText' =>__d('organisme', 'Organisme.activite_id'),
//                'inputType' => 'select',
//                'items'=>array(
//                    'type'=>'select',
//                    'empty' => true,
//                    'multiple' => true,
//                    'options' => $activites
//                )
//            );
            $formOrganismeInfos['input']['Operation.Operation'] = array(
                'labelText' =>__d('organisme', 'Organisme.operation_id'),
                'inputType' => 'select',
                'items'=>array(
                    'type'=>'select',
                    'empty' => true,
                    'multiple' => true,
                    'options' => $operations
                )
            );
        }
        echo $this->Formulaire->createForm($formOrganismeInfos);
    ?>
    </div>
    <div class="OrganismeAdresse col-sm-6">
    <?php
        $formCourrierOrganismeAdd = array(
            'name' => 'Courrier',
            'label_w' => 'col-sm-6',
            'input_w' => 'col-sm-6',
            'input' => array(
                'Organisme.adressecomplete' => array(
                    'labelText' =>__d('contact', 'Contact.adressecompleteLable'),
                    'inputType' => 'text',
                    'items'=>array(
                        'title'=>__d('contact', 'Contact.adressecompleteTitle'),
                        'type'=>'text'
                    )
                ),
                'Organisme.ban_id' => array(
                    'labelText' =>__d('organisme', 'Organisme.ban_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Organisme.bancommune_id' => array(
                    'labelText' =>__d('organisme', 'Organisme.bancommune_id'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Organisme.banadresse' => array(
                    'labelText' =>__d('organisme', 'Organisme.banadresse'),
                    'inputType' => 'select',
                    'items'=>array(
                        'type'=>'select',
                        'empty' => true,
                        'options' => array()
                    )
                ),
                'Organisme.numvoie' => array(
                    'labelText' =>__d('organisme', 'Organisme.numvoie'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.nomvoie' => array(
                    'labelText' =>__d('organisme', 'Organisme.nomvoie'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.compl' => array(
                    'labelText' =>__d('organisme', 'Organisme.compl'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.cp' => array(
                    'labelText' =>__d('organisme', 'Organisme.cp'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.ville' => array(
                    'labelText' =>__d('organisme', 'Organisme.ville'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.pays' => array(
                    'labelText' =>__d('organisme', 'Organisme.pays'),
                    'inputType' => 'text',
                    'items'=>array(
                        'type'=>'text'
                    )
                ),
                'Organisme.active' => array(
                    'labelText' =>__d('organisme', 'Organisme.active'),
                    'inputType' => 'checkbox',
                    'items'=>array(
                        'type'=>'checkbox',
                        'checked'=>$this->request->data['Organisme']['active']
                    )
                ),
                'Organisme.diffuse_contact' => array(
                    'inputType' => 'hidden',
                    'items'=>array(
                        'type'=>'hidden'
                    )
                )
            )
        );
        if( Configure::read('CD') == 81 ) {
            $formCourrierOrganismeAdd['input']['Organisme.canton'] = array(
                'labelText' =>__d('organisme', 'Organisme.canton'),
                'inputType' => 'text',
                'items'=>array(
                    'title'=>__d('organisme', 'Organisme.canton'),
                    'type'=>'text'
                )
            );
        }
        echo $this->Formulaire->createForm($formCourrierOrganismeAdd);
    ?>
    </div>
<?php
    if( Configure::read( 'Conf.SAERP') ) {
?>
    <div class="col-sm-8"></div>
    <div class="contact-creation-infos col-sm-4 information_update">
        <p><b>Fiche créée le : </b><?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($this->request->data['Organisme']['created']); ?></br>
            <b>Dernière modification le : </b> <?php echo $this->Html2->ukToFrenchDateWithHourAndSlashes($this->request->data['Organisme']['modified']); ?></p>
    </div>
<?php
    }
    echo $this->Form->end();
?>
</div>
<script type="text/javascript">
    // select2 sur les listes déroulantes
    $('#OrganismeBanId').select2({allowClear: true, placeholder: "Sélectionner un département"});
    $('#OrganismeBancommuneId').select2({allowClear: true, placeholder: "Sélectionner une commune"});
    $('#OrganismeBanadresse').select2({allowClear: true, placeholder: "Sélectionner une adresse"});

    $.widget("custom.catcomplete", $.ui.autocomplete, {
        _renderMenu: function (ul, items) {
            var self = this,
                    currentCategory = "";

            $.each(items, function (index, item) {
                if (item.category != currentCategory) {
                    ul.append("<li class='ui-autocomplete-category'>" + item.category + "</li>");
                    currentCategory = item.category;
                }
                self._renderItem(ul, item);
            });

        }

    });

    //contruction du tableau de types / soustypes
    var bans = [];
    <?php foreach ($bans as $ban) { ?>
    var ban = {
        id: "<?php echo $ban['Ban']['id']; ?>",
        name: "<?php echo $ban['Ban']['name']; ?>",
        banscommunes: []
    };
        <?php foreach ($ban['Bancommune'] as $communeB) { ?>
    var bancommune = {
        id: "<?php echo $communeB['id']; ?>",
        name: "<?php echo $communeB['name']; ?>"
    };
    ban.banscommunes.push(bancommune);
        <?php } ?>
    bans.push(ban);
    <?php } ?>

    function fillBanscommunes(ban_id, bancommune_id) {
        $("#OrganismeBancommuneId").empty();
        $("#OrganismeBancommuneId").append($("<option value=''> --- </option>"));

        for (i in bans) {
            if (bans[i].id == ban_id) {
                for (j in bans[i].banscommunes) {
                    if (bans[i].banscommunes[j].id == bancommune_id) {
                        $("#OrganismeBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "' selected='selected'>" + bans[i].banscommunes[j].name + "</option>"));
                    } else {
                        $("#OrganismeBancommuneId").append($("<option value='" + bans[i].banscommunes[j].id + "'>" + bans[i].banscommunes[j].name + "</option>"));
                    }
                }
            }
        }
    }


    $("#OrganismeBanId").append($("<option value=''> --- </option>"));
    //remplissage de la liste des types
    for (i in bans) {
        $("#OrganismeBanId").append($("<option value='" + bans[i].id + "'>" + bans[i].name + "</option>"));
    }
    //definition de l action sur type
    $("#OrganismeBanId").bind('change', function () {
        var ban_id = $('option:selected', this).attr('value');
        fillBanscommunes(ban_id);
    });

    var checkAdresses = function (bancommune_id) {
        $("#OrganismeBanadresse").empty();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getAdresses/' + bancommune_id,
            data: $('#OrganismeEditForm').serialize(),
            success: function (data) {
                if (data.length > 0) {
                    $("#OrganismeBanadresse").append($("<option value=''> --- </option>"));
                    // remplissage de la partie adresse
                    for (var i in data) {
                        $("#OrganismeBanadresse").append($("<option value='" + data[i].Banadresse.nom_afnor + "'>" + data[i].Banadresse.nom_afnor + "</option>"));
                    }
                }
            }
        });
    }

    $("#OrganismeBancommuneId").bind('change', function () {
        var bancommune_id = $('option:selected', this).attr('value');
        checkAdresses(bancommune_id);
    });

    <?php if( !empty($this->request->data['Organisme']['ban_id'])):?>
    $("#OrganismeBanId").select2('val', "<?php echo $this->request->data['Organisme']['ban_id']; ?>");
    $("#OrganismeBanId").trigger('change');
    <?php else:?>
    <?php if( !Configure::read('Conf.SAERP')) : ?>
    $("#OrganismeBanId").select2('val', "<?php echo isset( $bans[0]['Ban']['id'] ) ? $bans[0]['Ban']['id'] : ''; ?>");
    $("#OrganismeBanId").trigger('change');
    <?php endif;?>
    <?php endif;?>

    <?php if( !empty( $this->request->data['Organisme']['bancommune_id'] )):?>
    $("#OrganismeBancommuneId").select2('val', "<?php echo $this->request->data['Organisme']['bancommune_id']; ?>");
    $("#OrganismeBancommuneId").trigger('change');


    var adresse = {text: "<?php echo $this->request->data['Organisme']['banadresse']; ?>"};
    $("#OrganismeBanadresse").select2('data', adresse, true);
    $('#OrganismeBanadresse').append('<option value="' + adresse.text + '">' + adresse.text + '</option>');

    <?php endif;?>

    <?php if( empty( $this->request->data['Organisme']['ban_id'] )):?>
    $("#OrganismeBanId").val("---").change();
    $("#ContactBanId").val("---").change();
    <?php endif;?>

    $('#editValue').addClass('selectmetadonnees');
    $('#editValue').button().click(function () {


        var url = "<?php echo Router::url(array('controller' => 'contacts', 'action' => 'add', $id )); ?>";
        gui.request({
            url: url,
            loader: true,
            updateElement: $('body'),
            loaderMessage: gui.loaderMessage
        }, function (data) {
            $('.modal-body').html(data);
        });
    });



    // Using jQuery UI's autocomplete widget:
    $('#OrganismeAdressecomplete').bind("keydown", function (event) {
        if (event.keyCode != 16 &&
                event.keyCode != 17 &&
                event.keyCode != 18) {
            $('#OrganismeBanId').val(0);
            $('#OrganismeBancommuneId').html('');
        }
    }).catcomplete({
        minLength: 3,
        source: '/contacts/searchAdresse',
        select: function (event, ui) {
            gui.request({
                url: '/contacts/searchAdresseSpecifique/' + ui.item.id
            }, function (data) {
                $('#OrganismeBancommuneId').val(ui.item.id);
                var json = jQuery.parseJSON(data);
                var banadresse = json['Banadresse'];
                var ban = json['Ban'];
                var banville = json['Bancommune'];
                var num = banadresse['numero'];
                if (typeof banadresse['rep'] !== 'undefined') {
                    num = banadresse['numero'] + " " + banadresse['rep'];
                }
                if (typeof banadresse['id'] !== 'undefined') {
                    $('#OrganismeBanId').val(ban['id']).change();
                    $('#OrganismeBancommuneId').val(banville['id']).change();
                    $("#OrganismeBanadresse").append($("<option value='" + banadresse['nom_afnor'] + "' selected='selected'>" + banadresse['nom_afnor'] + "</option>"));
                    $("#OrganismeBanadresse").val(banadresse['nom_afnor']).change();
                    $('#OrganismeNumvoie').val(num);
                    $('#OrganismeNomvoie').val(banadresse['nom_afnor']);
                    $('#OrganismeCp').val(banadresse['code_post']);
                    $('#OrganismeVille').val(banville['name']);
                    <?php if( Configure::read('CD') == 81 ):?>
                        $('#OrganismeCanton').val(banadresse['canton']);
                    <?php endif;?>

                }
            });
        }
    });

    // Partie Organisme
    // Partie pour mettre à jour le code postal selon le nom de voie sélectionné
    $('#OrganismeBanadresse').change(function () {
        refreshCodepostalOrganisme($("#OrganismeBancommuneId").val(), $("#OrganismeBanadresse").val());
    });
    var refreshCodepostalOrganisme = function (bancommuneId, banadresseName) {
        var params = '';
        if (banadresseName != '') {
            var params = bancommuneId + '/' + banadresseName;
        }
        else {
            params = bancommuneId;
        }
        gui.request({
            url: '/bansadresses/getCodepostal/' + params
        }, function (data) {
            var json = jQuery.parseJSON(data);
            var codepostal = json;
            $('#OrganismeNomvoie').val(codepostal['nom_afnor']);
            $('#OrganismeCp').val(codepostal['codepostal']);
            $('#OrganismeVille').val(codepostal['ville']);
            <?php if( Configure::read('CD') == 81 ):?>
                $('#OrganismeCanton').val(codepostal['canton']);
            <?php endif;?>
        });
    }


    // Pour les organismes
    var checkHomonymsOrgs = function () {
        $('.homonymsorgs').remove();
        $.ajax({
            type: 'post',
            dataType: 'json',
            url: '/Organismes/getHomonymsOrgs',
            data: $('#OrganismeEditForm').serialize(),
            success: function (data) {

                if (data.length > 0) {
                    var homonymsorgs = $('<div></div>').html('<?php echo __d('organisme', 'Organisme.HomonymsListTitleEdit'); ?>').addClass('homonymsorgs').addClass('alert alert-warning');
                    var homonymorgList = $('<ul></ul>');

                    for (var i in data) {
                        homonymorgList.append($('<li></li>').html(data[i].Organisme.name + ' (' + data[i].Addressbook.name + ')'));
                    }
                    homonymsorgs.append(homonymorgList);

                    $('#OrganismeName').parent().after(homonymsorgs);

                }
            }
        });

    }

    $('#OrganismeName').blur(function () {
        checkHomonymsOrgs();
    });
    $('#OrganismeEditForm').addClass('checkHomonyms');

    $('#ActiviteActivite').select2({allowClear: true, placeholder: "Sélectionner une activité"});

     <?php if( Configure::read('Conf.SAERP') ) :?>
    $('#OperationOperation').select2({allowClear: true, placeholder: "Sélectionner une OP"});
    <?php endif;?>

    $('#infos .panel-title').empty();
    $('#infos .panel-title').append('Modification : <?php echo $this->request->data['Organisme']['name']; ?>');
</script>
