<div>
    <span>
        <?php
            foreach($mailsSent as $mail) {

				if( $mail['Desktop']['id'] == '-3' || empty($mail['Desktop']['id']) ) {
					echo '<dl>'
							. '<dt>Mail sécurisé adressé à : </dt><dd>'.$mail['Sendmail']['email']. '</dd>'
							. '<dt>Envoyé le : </dt><dd>'.  $this->Html2->ukToFrenchDateWithHourAndSlashes( $mail['Sendmail']['created'] ). '</dd>'
							. '<dt>Informations : </dt><dd>'.  $mail['Sendmail']['message']. '</dd>'
							. '</dl>';
				}
				else {
					echo '<dl>'
							. '<dt>Mail envoyé par : </dt><dd>' . $mail['Desktop']['name'] . '</dd>'
							. '<dt>Adressé à : </dt><dd>' . $mail['Sendmail']['email'] . '</dd>'
							. '<dt>Concernant le document : </dt><dd>' . $mail['Document']['name'] . '</dd>'
							. '<dt>Envoyé le : </dt><dd>' . $this->Html2->ukToFrenchDateWithHourAndSlashes($mail['Sendmail']['created']) . '</dd>'
							. '</dl>';
				}

            }
        ?>
    </span>
</div>
