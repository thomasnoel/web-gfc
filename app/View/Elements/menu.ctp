<?php

/**
 *
 * Elements/menu.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<?php
$sessionMenu = $this->Session->read('Auth.User.menu');
$collectiviteName = $this->Session->read('Auth.User.collectivite_name');
?>
<nav class="navbar navbar-inverse navbar-fixed-top" id="webgfc_menu">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="nav navbar-nav navbar-left nav-pills">
            <li><a class="logo-gfc" href="/environnement" title="Accueil"><span class="sr-only"></span></a></li>
            <li id="user_collectivite" class="hidden-sm hidden-xs ">
                <a href="/environnement">
                    <span class="collectivite_text_menu" title="Accueil"><?php echo $collectiviteName; ?></span>
                </a>
            </li>

<?php
if (!empty($sessionMenu)) {
    foreach ($sessionMenu as $menu) {
        if(!empty($menu)){
            if($menu['txt']!='Mon compte' && $menu['txt']!='Rechercher' && $menu['txt']!="Carnet d'adresses" && $menu['title']!="Créer un nouveau flux"){
		       if (!empty($menu['items'])) {
					?>
					<li class="dropdown">
						<a href="#"
						   class="dropdown-toggle"
						   data-toggle="dropdown"
						   role="button"
						   aria-expanded="false"
						   id="scale_down_pusher"
						   title="<?php echo isset($menu['title'])? $menu['title'] : ''; ?>">
							   <?php echo $menu['txt']; ?> <i class="fa fa-chevron-down" aria-hidden="true"></i>
							<span class="sr-only"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
								<?php
							foreach ($menu['items'] as $item) {
								if($item['type']!='separator'){
									if(isset($item['escape']) && $item['escape'] == true) {
										echo $this->Html->tag('li',$this->Html->link($item['txt'],$item['url'],array('escape'=>true, 'target' => '_external')));
									}else {
										echo $this->Html->tag('li',$this->Html->link($item['txt'],$item['url']),array('escape'=>false));
									}
								}else{
									echo $this->Html->tag('li',"",array('class'=>'divider'));
								}

							}
							?>
						</ul>
					</li>
					<?php
        		}
            }else{
                if($menu['txt']=='Rechercher' ){
					?>
					<li>
						<a href="/recherches/index"
						   title="Rechercher un flux">
							<i class="fa fa-search" aria-hidden="true"></i>&nbsp;
							<span class="text_menu hidden-md hidden-sm hidden-xs "> Rechercher </span>
							<span class="sr-only"></span></a>
					</li>
				<?php
                }
                else if($menu['txt']=="Carnet d'adresses" ){
				?>
					<li>
						<a href="/addressbooks/index"
						   title="<?php echo $menu["title"]; ?>">
							<i class="fa fa-book" aria-hidden="true"></i>&nbsp;
							<span class="text_menu hidden-md hidden-sm hidden-xs "> <?php echo $menu["title"]; ?> </span>
							<span class="sr-only"></span></a>
					</li>
				<?php
                }
                else if($menu['txt']=="Marchés" ){
				?>
					<li>
						<a href="/marches/index"
						   title="<?php echo $menu["title"]; ?>">
							<i class="fa fa-shopping-cart" aria-hidden="true"></i>&nbsp;
							<span class="text_menu hidden-md hidden-sm hidden-xs "> <?php echo $menu["title"]; ?> </span>
							<span class="sr-only"></span></a>
					</li>
				<?php
                }
                else if($menu['txt']=="Créer un nouveau flux" ){
				?>
					<?php if( $displayForm ) :?>
						<li>
							<a href="#"
							   title="<?php echo $menu["title"]; ?>"
							   data-toggle = "modal"
							   data-target = "#myModal">
								<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
								<span class="text_menu hidden-md hidden-sm hidden-xs "> <?php echo $menu["title"]; ?> </span>
								<span class="sr-only"></span></a>
						</li>
					<?php else :?>
						<li>
							<a href="/courriers/add/<?php echo $desktopId;?>/<?php echo $serviceId;?>"
							   title="Créer un nouveau flux">
								<i class="fa fa-plus" aria-hidden="true"></i>&nbsp;
								<span class="text_menu hidden-md hidden-sm hidden-xs "> Créer un nouveau flux</span>
								<span class="sr-only"></span></a>
						</li>
					<?php endif;?>

					<?php
				} else if( isset($menu['title']) && $menu["title"]=="Créer un nouveau flux") {
				?>
					<li class="dropdown">
						<a href="#"
						   class="dropdown-toggle"
						   data-toggle="dropdown"
						   role="button"
						   aria-expanded="false"
						   id="scale_down_pusher"
						   title="<?php echo isset($menu['title']) ? $menu['title'] : ''; ?>">
							<?php echo $menu['txt']; ?> <i class="fa fa-chevron-down" aria-hidden="true"></i>
							<span class="sr-only"></span>
						</a>
						<ul class="dropdown-menu" role="menu">
							<?php
							foreach ($menu['items'] as $item) {

								if( !empty( $this->Session->read('Auth.DesktopmanagerInit') ) ) {
									// ajout pour Démarches-Simplifiées
									if( Configure::read('Webservice.DS') ) {
											if (isset($item['url']['controller']) && $item['url']['controller'] == 'demarchessimplifiees') {
												echo $this->Html->tag('li', $this->Html->link($item['txt'], $item['url']), array('escape' => false));
											}

									}

									// ajout pour Direct-Mairie
									if( Configure::read('Webservice.Directmairie') ) {
										if (isset($item['url']['controller']) && $item['url']['controller'] == 'directmairies') {
											echo $this->Html->tag('li', $this->Html->link($item['txt'], $item['url']), array('escape' => false));
										}
									}

									if (!isset($item['url']['controller'] ) && isset($item['action']['data-toggle']) ) {
										if (!$displayForm) {
											$url = array('controller' => 'courriers', 'action' => 'add', $desktopId, $serviceId);
											echo $this->Html->tag('li', $this->Html->link($item['txt'], $url, array('escape' => false)));
										} else {
											?>
											<li>
												<a href="#"
												   title="<?php echo $menu["title"]; ?>"
												   data-toggle="modal"
												   data-target="#myModal">
													<span class="text_menu hidden-md hidden-sm hidden-xs "> <?php echo $menu["title"]; ?> </span>
													<span class="sr-only"></span></a>
											</li>
											<?php
										}
									}
								}else {
									echo $this->Html->tag('li', $this->Html->link($item['txt'], '#'), array( 'class' => 'popupalert', 'escape' => false));
									?>
										<script type="text/javascript">
										$('.popupalert').click(function () {
											swal({
												showCloseButton: true,
												title: 'Ajout impossible',
												text: 'Veuillez contacter votre administrateur, <br />aucun bureau initiateur trouvé',
												type: 'error'
											});
										});
									</script>
								<?php
								}
							}
							?>
						</ul>
					</li>
				<?php
				}
            }
        }
    }
}
?>

        </ul>
        <ul class="nav navbar-nav navbar-right nav-pills">
            <!--<li id="user_connected"><a href="/users/getInfosCompte"><span class="text_menu hidden-md hidden-sm hidden-xs"><?php /*echo $this->Session->read('Auth.User.prenom') . ' ' . $this->Session->read('Auth.User.nom') .' ('.$this->Session->read('Auth.User.username').')'; */?></span></a></li>-->
            <?php if ($this->Session->read('Auth.User.Env.main') === 'superadmin' ) :?>
                <li id="user_connected"><a href="/users/setInfosPersos"><span class="text_menu hidden-md hidden-sm hidden-xs"><?php echo $this->Session->read('Auth.User.prenom') . ' ' . $this->Session->read('Auth.User.nom'); ?></span></a></li>
            <?php else :?>
                <li id="user_connected"><a href="/users/getInfosCompte"><span class="text_menu hidden-md hidden-sm hidden-xs"><?php echo $this->Session->read('Auth.User.prenom') . ' ' . $this->Session->read('Auth.User.nom'); ?></span></a></li>
            <?php endif;?>
            <li id="nbNotifications" title="Notifications"><a class="notification_fullwrapper">
                    <i class="fa fa-bell" aria-hidden="true"></i></a>
            </li>
            <li id="nbTaches" title="Taches en attente"><a class="notification_fullwrapper">
                    <i class="fa fa-tasks" aria-hidden="true"></i></a>
            </li>
            <li id="absences" title="Délégations"><a><i class="fa fa-user-times" aria-hidden="true"></i></a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" id="scale_down_pusher" style="height: 50px!important;"><i class="fa fa-bars" aria-hidden="true"></i></a>
                <ul class="dropdown-menu" role="menu" id="menuRightItem" >
                    <?php
                        foreach ($sessionMenu['systeme']['items'] as $item) {
                             if($item['type']!='separator'){
                                echo $this->Html->tag('li',$this->Html->link($item['txt'],$item['url'],array('escape'=>false)),array('escape'=>false));
                             }
                             else {
                                 echo $this->Html->tag('li',"",array('class'=>'divider'));
                             }
                        }
                    ?>
                </ul>
            </li>
        </ul>
    </div>
</nav>


<script type="text/javascript">
    function selectService(parentSelect) {
        var desktopId = $('option:selected', $(parentSelect)).val();
        $('#serviceCreateFlux option').css('display', 'none');
        $('#serviceCreateFlux option.void').css('display', 'block');
        $('#serviceCreateFlux option.desktop' + desktopId).css('display', 'block');
    }

    function getProflUser() {
        var userMainProfil = '<?php echo $this->Session->read('Auth.User.Env.main'); ?>';
        return userMainProfil;
    }


    function gestionWidthMenu() {
        var userMainProfil = getProflUser();
        if (userMainProfil != 'admin') {
            if ($(window).width() >= 1200) {
                if ($(window).width() < 1600) {
                    $('.navbar-left').find('li').each(function () {
                        $(this).find('.text_menu').hide();
                    });
                } else {
                    $('.navbar-nav').find('li').each(function () {
                        $(this).find('.text_menu').show();
                    });
                }
            }
        } else {
            if ($(window).width() >= 1200) {
                if ($(window).width() < 1600) {
                    $('.navbar-left').find('li').each(function () {
                        $(this).find('.text_menu').hide();
                    });
                } else {
                    $('.navbar-nav').find('li').each(function () {
                        $(this).find('.text_menu').show();
                    });
                }
            }
        }
    }
    $(window).resize(function () {
        gestionWidthMenu();
    });

    $(document).ready(function () {
        gestionWidthMenu();
        //enlever menu "nouveau flux" pour qui est admin mais n'est pas un initiateur.(pas ideal pour résoudre ce problème)
        if ($('#myModal').length != 1) {
            $('#navbar li .dropdown-menu li a[data-target="#myModal"]').parent().remove();
        }
    });


</script>
<?php

if( !empty( $this->Session->read('Auth.DesktopmanagerInit') ) ) {
	if($displayForm) {
		if (!empty($add) && !empty($addservice)) {
			?>
		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<form id="desktopCreateForm" name="desktopCreateForm" action="#" class="panel-body form-horizontal">
						<div class="modal-header">
							<button data-dismiss="modal" class="close">×</button>
							<h4 class="modal-title"><?php echo __d('courrier', 'New Courrier'); ?></h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<label class="control-label col-sm-6" for="desktopCreateFlux"><?php echo __d('desktop', 'Desktop.choice') ?></label>
								<div class="controls col-sm-5">
									<select class="selectpicker form-control" type="text" id="desktopCreateFlux" name="desktopCreateFlux" onchange="selectService(this)">
										<option class="void" value=""> </option>
										<?php
										$addSelectRole ='';
										foreach ($add as $desktopId => $desktopName) {
											$addSelectRole .= '<option value="' . $desktopId . '">' . $desktopName . '</option>';
										}
										echo $addSelectRole;
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-6 " for="inputEmail">Veuillez sélectionner un service</label>
									<?php
										$formSend = array(
											'name' => 'Courrier',
											'label_w' => 'col-sm-6',
											'input_w' => 'col-sm-5',
											'input' => array(
												'serviceCreateFlux' => array(
		//                                            'labelText' =>__d('service', 'Soustype'),
													'inputType' => 'select',
													'items'=>array(
														'type' => 'select',
														"options" =>array(),
														'empty' => true
													)
												)
											)
										);
										echo $this->Formulaire->createForm($formSend);
									?>
							</div>
						</div>
						<div id="newFluxButton" class="modal-footer " role="group" style="width: 100%">
						</div>
					</form>
				</div>

			</div>
		</div>
		<script type="text/javascript">
			$('#desktopCreateFlux' ).select2({allowClear: true, placeholder: "Sélectionner un profil"});
			$('#CourrierServiceCreateFlux' ).select2({allowClear: true, placeholder: "Sélectionner un service"});
			gui.addbuttons({
				element: $('#newFluxButton'),
				buttons: [
					{
						content: "<?php echo __d('default', 'Button.cancel'); ?>",
						class: 'btn btn-danger-webgfc btn-inverse ',
						data_dismiss: 'modal'
					},
					{
						content: "<?php echo __d('default', 'Button.submit'); ?>",
						class: 'btn btn-success btn-sencondary',
						action: function () {
							if ($('#desktopCreateFlux option:selected').val() != '' && $('#serviceCreateFlux option:selected').val() != '') {
								window.location.href = '<?php echo $this->Html->url(array('controller' => 'courriers', 'action' => 'add')); ?>/' + $('#desktopCreateFlux option:selected').val() + '/' + $('#CourrierServiceCreateFlux option:selected').val();
							}
							else {
								$("#desktopCreateForm label.ui-state-error").hide();
								$("#desktopCreateForm select").removeClass('alert alert-warning');
								// Pour la liste des bureaux
								if ($('#desktopCreateFlux option:selected').val() == '') {
									$("#desktopCreateFlux").addClass('alert alert-warning');
									$("#desktopCreateFlux").parent('div.input').find('label.ui-state-error').show();
								}

								// Pour la liste des services
								if ($('#CourrierServiceCreateFlux option:selected').val() == '') {
									$("#CourrierServiceCreateFlux").addClass('alert alert-warning');
									$("#CourrierServiceCreateFlux").parent('div.input').find('label.ui-state-error').show();
								}
							}
						}
					}
				]
			}
			);



			$('#desktopCreateFlux').change(function () {
				var desktop_id = $(this).val();
				var serviceLists = <?php echo json_encode($addservice); ?>;
				$('#CourrierServiceCreateFlux option').remove();

				// On est obligés de passer par un array car on ne peut trier les clés d'un objet
				var options = [];
				for (var value in serviceLists[desktop_id]) {
					options.push([value, serviceLists[desktop_id][value]]);
				}
				options.sort(function(a, b) {
					if(a[1] < b[1]) return -1;
					else if(a[1] > b[1]) return 1;
					return 0;
				});
				$.each(options, function (index, value) {
					$('#CourrierServiceCreateFlux').append($("<option value='" + value[0] + "'>" + value[1] + "</option>"));
				});
			});
		<?php
		}
	}
}
else {
?>
	<script type="text/javascript">
		console.log('coucou');
	</script>
	<?php
}
?>

</script>
