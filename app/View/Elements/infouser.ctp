<?php

/**
 *
 * Elements/infouser.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
$userinfo = array(
	array(
		'<span class="bold">' . __d('user', 'Utilisateur') . '</span> ',
		$this->Session->read('Auth.User.username') . '@' . $this->Session->read('Auth.User.login_suffix') . ' (' . $this->Session->read('Auth.User.prenom') . ' ' . $this->Session->read('Auth.User.nom') . ')'
	)
);
echo $this->Html->tag('table', $this->Html->tableCells($userinfo), array('class' => 'userinfoTable'));
?>
