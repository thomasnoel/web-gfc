<?php

/**
 *
 * Elements/context.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * Dessine la panneau contextuel lors de la visualisation ou l'édition d'un flux
 *
 *
 *
 * @param array $documentList Liste des documents attachés au flux
 * @param boolean $response contexte de réponse
 * @params integer $fluxId identifiant du flux
 *
 */
?>
<?php
echo $this->Html->script('contextFunction.js', array('inline' => true));
if (!isset($sortant)) {
    $sortant = false;
}
if (!empty($parent)) {
}
if (!empty($fromDispEnv) && $fromDispEnv) {
}

?>
<script type="text/javascript">
    //Variable pour contextFunction.js
    var fluxId = <?php echo $fluxId;?>;
    var buttonSubmit = "<?php echo __d('default', 'Button.valid'); ?>";
    var windowLocationHref = "<?php echo Configure::read('BaseUrl') . "/environnement"; ?>";
    var buttonCancel = "<?php echo __d('default', 'Button.cancel'); ?>";
    var titleModalSupp = "<?php echo __d('default', 'Modal.suppressionTitle'); ?>";
    var msgModalSupp = "<?php echo __d('default', 'Modal.suppressionContents'); ?>";
    var titleModalSendmail = "<?php echo __d('default', 'Confirm.sendmail'); ?>";
    var titleModalGED = "<?php echo __d('default', 'Versement en GED'); ?>";
    var msgModalGED = "<?php echo __d('default', 'Etes-vous sûr de vouloir verser ce flux en GED ?'); ?>";
    var username = "<?php echo $this->Session->read('Auth.User.username'); ?>";
    var mainDoc = null;
    var mainDocId = null;
    <?php if (!empty($mainDoc) && is_array($mainDoc)) { ?>
    mainDoc = <?php echo json_encode($mainDoc);?>;
    mainDocId = <?php echo $mainDoc['Document']['id']; ?>;
    <?php } ?>
    <?php if (!empty($parent)) { ?>
    var parentFluxId = <?php echo $parent['Courrier']['id']; ?>;
    <?php }?>
    <?php if (!empty($fluxRefus)) { ?>
    var refusFluxId = <?php echo $fluxRefus['Courrier']['id']; ?>;
    <?php }?>
</script>

<?php
if (!isset($fromDispEnv)) {
        $fromDispEnv = false;
}

if (!isset($fluxId)) {
        $fluxId = '';
}

if (empty($documentList)) {
    if ($fromDispEnv) {
        $documentList = array(array('name' => __d('document', 'Document.list.undefined')));
    } else {
        $documentList = array(array('name' => __d('document', 'Document.list.void')));
    }
}

if (empty($rights) || (isset($rights) && !is_array($rights))) {
    $rights = array(
        'getDocuments' => false,
        'getRelements' => false,
        'getArs' => false,
        'getCircuit' => false,
        'getSms' => false
    );
} else {
    if (empty($rights['getDocuments'])) {
        $rights['getDocuments'] = false;
    }
    if (empty($rights['getRelements'])) {
        $rights['getRelements'] = false;
    }
    if (empty($rights['getArs'])) {
        $rights['getArs'] = false;
    }
    if (empty($rights['getCircuit'])) {
        $rights['getCircuit'] = false;
    }
    if (empty($rights['getSms'])) {
        $rights['getSms'] = false;
    }
}
?>
<ul class="nav nav-tabs titre" role="tablist">
    <?php
        // Bouton pour agrandir la consultation de la PJ
        echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-plus-square-o"  title="Augmenter" aria-hidden="true"></i>', '#grow', array('escape' => false,"data-toggle"=>"tab")));

        echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-arrow-right"  title="Masquer" aria-hidden="true"></i>', '#', array('escape' => false,"data-toggle"=>"tab","id"=>"cacheLateral")));
        echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-camera-retro" title="'.__d('courrier', 'Context.preview').'" aria-hidden="true"></i>', '#preview', array('escape' => false,"data-toggle"=>"tab")),array('class'=>'active'));
        if ($rights['getDocuments']) {
            echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-file" title="'.__d('courrier', 'Documents').'" aria-hidden="true"></i>', '#documents', array('escape' => false,"data-toggle"=>"tab")));
        }
        if (!$fromDispEnv) {
            if ($rights['getRelements'] && (!empty($parent) || $sortant || Configure::read('Flux.interne') ) && Configure::read('DOC_FROM_GFC') || Configure::read( 'Conf.SAERP') || Configure::read( 'Conf.Gabarit') || Configure::read( 'Conf.CD')) {
                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-file-text" title="'.__d('courrier', 'Relements.noresp').'" aria-hidden="true"></i>', '#relements', array('escape' => false,"data-toggle"=>"tab")));
            }
            if ($rights['getArs'] /*&& empty($parent) && !$sortant */&& Configure::read('DOC_FROM_GFC')) {
                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-envelope" title="'.__d('courrier', 'Ars').'" aria-hidden="true"></i>', '#ars', array('escape' => false,"data-toggle"=>"tab")));
            }
            if ($rights['getCircuit']) {
                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-road" title="'.__d('courrier', 'Circuit').'" aria-hidden="true"></i>', '#circuit', array('escape' => false,"data-toggle"=>"tab")));
            }
			if ($rights['getSms'] && $connecteurSMSActif) {
				echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-mobile" style="font-size:16px;padding-left:3px;" title="'.__d('courrier', 'SMS').'" aria-hidden="true"></i>', '#sms', array('escape' => false,"data-toggle"=>"tab")));
			}
            if (!empty($parent)) {
                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-newspaper-o" title="'.__d('courrier', 'Parent').'" aria-hidden="true"></i>', '#parent', array('escape' => false,"data-toggle"=>"tab")));
            }
            if (!empty($fluxRefus)) {
                echo $this->Html->tag('li', $this->Html->link('<i class="fa fa-times" title="'.__d('courrier', 'FluxRefus').'" aria-hidden="true"></i><i class="fa fa-newspaper-o" title="'.__d('courrier', 'FluxRefus').'" aria-hidden="true"></i>', '#fluxRefus', array('escape' => false,"data-toggle"=>"tab")));
            }
        }
    ?>
</ul>
<!--<div class="tab-content row" style="margin-left: 21%;min-height: 350px;max-width: 78%;">-->
<div class="tab-content " style="margin-left: 65px;">
    <!--<div class="tab-content row" style="margin-left: 19%;min-height: 450px;max-width: 88%;">-->
    <div  class="tab-pane fade active in"  id="preview">
        <div class="document_preview">
            <h4 class="header"><?php echo __d('document', 'Document.preview'); ?> : <span class="selected_document"></span>
                    <?php if (!empty($mainDoc)) { ?>
                <a  href="/Documents/getPreview/<?php echo $mainDoc['Document']['id']; ?>/true" target="_blank" >
					<i class="fa fa-compress" aria-hidden="true" title="Plein écran"></i>
                </a>
                    <?php } ?>
            </h4>
            <div class="content"><div class="alert alert-warning"><?php echo __d('document', 'Document.preview.disable'); ?></div></div>
        </div>
    </div>
<?php
    if ($rights['getDocuments']) {
        echo '
        <div  class="tab-pane fade" id="documents">';
            if (empty($fluxId)) {
                echo '<div class="alert alert-warning">
                    '.__d('courrier', 'Document.void').'<br />
                    <br />
                    <br />
                    <br />'.
                    __d('courrier', 'Document.void.upload')
                    .'<br />
                    <br />
                    <br />
                    <br />
                </div>';
            }
        echo'</div>';
    }
    if (!$fromDispEnv) {
        if ($rights['getRelements'] && (!empty($parent) || $sortant || Configure::read('Flux.interne') ) && Configure::read('DOC_FROM_GFC') || Configure::read( 'Conf.SAERP') || Configure::read( 'Conf.Gabarit')) {
            echo '
            <div  class="tab-pane fade" id="relements">
            </div>';
        }
        if ($rights['getArs'] && Configure::read('DOC_FROM_GFC')) {
            echo '
            <div  class="tab-pane fade" id="ars">
            </div>';
        }
        if ($rights['getCircuit']) {
            echo '
            <div  class="tab-pane fade" id="circuit">
            </div>';
        }
        if ($rights['getSms']) {
            echo '
            <div  class="tab-pane fade" id="sms">
            </div>';
        }
        if (!empty($parent)) {
            echo '
            <div  class="tab-pane fade" id="parent">
            </div>';
        }
        if (!empty($fluxRefus)) {
            echo '
            <div  class="tab-pane fade" id="fluxRefus">
            </div>';
        }
    }
?>
</div>

<div style="clear: both;"></div>

<div style="display: none" id="mainDocTmp">
    <?php
    if (!empty($mainDoc)) {
    ?>
    <li class="ctxdoc">
            <?php
            echo $this->Element('ctxdoc', array(
                    'ctxdoc' => $mainDoc['Document'],
                    'gfcDocType' => 'Document',
                    'cssClass' => '',
                    'rightToDelete' => !empty($mainDoc['Document']['desktop_creator_id']) ? true : false,
                    'isGeneratedFile' => !empty($mainDoc['Document']['isgenerated']) ? true : false
            ));
            ?>
    </li>
    <?php
    }
    ?>
</div>

<script type="text/javascript">

    affichageTabContext(fluxId);
    // Change hash for page-reload
    $('#flux_context .nav-tabs a').on('shown.bs.tab', function (e) {
        affichageTabContext(fluxId);
    });
    $('#flux_infos >.panel').css('margin-right', '0px');
    $('#cacheLateral').toggle(function () {
        $('.infos').removeClass('col-sm-6').addClass('col-sm-10');
        $('#flux_context .tab-content').hide();
        $('#flux_context').removeClass('col-sm-6').addClass('col-sm-1').css('margin-right', '0px');
        $('#flux_context').removeClass('col-sm-6');
        $('#cacheLateral').html('<i class="fa fa-arrow-left" aria-hidden="true" title="Afficher"></i>');
        $('.tabbable-text').show();
        $('#flux_infos .tab-content').css('margin-left', '24px');

		$('#flux_context').css('position', 'inherit');
		$('#flux_context').css('left', '0%');
    }, function () {
        $('.infos').removeClass('col-sm-10').addClass('col-sm-6');
        $('#flux_context .tab-content').show();
//        $('#flux_context').addClass('col-sm-6').removeClass('col-sm-1').css('margin-right', '0px');
        $('#flux_context').addClass('col-sm-6').removeClass('col-sm-1').css('margin-right', '0px');
        $('#cacheLateral').html('<i class="fa fa-arrow-right" aria-hidden="true" title="Masquer"></i>');
        if ($(window).width() < 1280) {
            $('.tabbable-text').hide();
//            $('#flux_infos .tab-content').css('margin-left', '24px');
            $('#flux_infos .tab-content').css('margin-left', '47px');
        }

		$('#flux_context').css('position', 'fixed');
		$('#flux_context').css('left', '50%');
		// $('#flux_context').css('background', 'red');
    });


    $( '.fa-plus-square-o' ).toggle(function () {
        $('#flux_infos').hide();
        $('#flux_context').removeClass('col-sm-6').addClass('col-sm-10');
		$('#flux_context').css('left', '1%');
		$('#flux_context').css('margin-top', '50px');
        $('#viewerContainer').css('width', '100%');
        $('#viewerContainer').css('height', '80%');
        $('#controls').addClass('col-sm-10');
        $('.infos').addClass('col-sm-10');
        var previewWidth = $('.document_preview').width();
		$('.page').css('width', previewWidth);
		$('.page').css('height', '1403px');
		var container = document.getElementById('viewerContainer');
		if(container != null ) {
			var pdfViewer = new PDFJS.PDFViewer({
				container: container,
			});
			container.addEventListener('pagesinit', function () {
				// We can use pdfViewer now, e.g. let's change default scale.
				pdfViewer.currentScaleValue = 'auto';
			});
			PDFJS.getDocument(DEFAULT_URL).then(function (pdfDocument) {
				// Document loaded, specifying document for the viewer
				pdfViewer.setDocument(pdfDocument);
			});
		}
		$('.fa-plus-square-o').removeClass('fa-plus-square-o').addClass('fa-minus-square-o');
		$('.fa-minus-square-o').attr('title', 'Réduire');
        $('.page').css( 'margin-left', '0');
    }, function () {
        $('#flux_infos').show();
        $('#flux_context').addClass('col-sm-6').removeClass('col-sm-10');
		$('#flux_context').css('left', '50%');
		$('#flux_context').css('margin-top', '1px');
        $('#controls').addClass('col-sm-6');
        $('.page').css('margin', '1px auto -8px auto');

		$('.fa-minus-square-o').removeClass('fa-minus-square-o').addClass('fa-plus-square-o');
		$('.fa-plus-square-o').attr('title', 'Augmenter');
        $('.infos').removeClass('col-sm-10').addClass('col-sm-6');
    });
	$('#flux_context').css('height', '70%');
</script>
