<?php
/**
 *
 * Elements/flexPaper.ctp View File
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */
?>
<a id="viewerPlaceHolder" style="width:<?php echo $width; ?>;height:<?php echo $height ?>;display:block;margin-left: auto; margin-right: auto;">
	<?php echo __d('default', 'Your browser does not support flash.'); ?>
</a>
<script type="text/javascript">

	var h = parseInt($('.ui-accordion-content:first').css('height'));
	$('#viewerPlaceHolder').css('height', (h - 34) + 'px');


	var fp = new FlexPaperViewer(
	'/files/flexpaper/FlexPaperViewer',
	'viewerPlaceHolder', { config : {
			SwfFile : "<?php echo $filename; ?>",
			Scale : 0.1,
			ZoomTransition : 'easeOut',
			ZoomTime : 0.5,
			ZoomInterval : 0.2,
			FitPageOnLoad : false,
			FitWidthOnLoad : true,
			PrintEnabled : true,
			FullScreenAsMaxWindow : true,
			ProgressiveLoading : false,
			MinZoomSize : 0.2,
			MaxZoomSize : 5,
			SearchMatchAll : true,
			InitViewMode : 'Portrait',

			ViewModeToolsVisible : true,
			ZoomToolsVisible : true,
			NavToolsVisible : true,
			CursorToolsVisible : false,
			SearchToolsVisible : false,

			WMODE: 'Transparent',

			localeChain: 'fr_FR'
		}});

</script>
