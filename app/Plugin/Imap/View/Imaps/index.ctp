<?php echo $this->Html->css('imap'); ?>

<script type="text/javascript">
	/**
	 *
	 * @param {type} mailHeader
	 */
	function mkMail(mailHeader) {
		var retval = $('<div></div>').addClass('mail').attr('data-msgno', mailHeader.msgno);
		var subject = $('<span></span>').addClass('subject');
		var date = $('<span></span>').addClass('date').addClass('label').addClass('label-info').addClass('pull-right');
		var exp = $('<span></span>').addClass('date').addClass('label').addClass('label-inverse').addClass('pull-left');

		subject.html('<br />' + mailHeader.subject);
		exp.html(mailHeader.from);

		var newDate = new Date(mailHeader.date);
		var datefr = ((newDate.getDate() > 9) ? '' : '0') + newDate.getDate() + '/' + ((newDate.getMonth() > 8) ? '' : '0') + (newDate.getMonth() + 1) + '/' + newDate.getFullYear() + " - " + ((newDate.getHours() > 9) ? '' : '0') + newDate.getHours() + ":" + ((newDate.getMinutes() > 9) ? '' : '0') + newDate.getMinutes() + ':' + ((newDate.getSeconds() > 9) ? '' : '0') + newDate.getSeconds();
		date.html(datefr);

		retval.append(exp).append(date).append(subject);
		return retval;
	}

	/**
	 *
	 * @param {type} mailContent
	 * @param {type} msgno
	 */
	function mkMailContent(mailContent, msgno) {
		var retval = $('<div></div>').addClass('mailContent');
		var mailBody = $('<div><div>').addClass('mailBody');
		var attachements = $('<div></div>').addClass('mailAttachment');
		for (var partno in mailContent.parts) {
			var fileicon = $('<i></i>').addClass('icon-file');
			var file = $('<div></div>').addClass('file').html(fileicon).attr('data-parts', partno);
			if (mailContent.parts[partno].ifdparameters === 1 && mailContent.parts[partno].dparameters[0].attribute.toLowerCase() === 'filename') {
				file.append(' ' + mailContent.parts[partno].dparameters[0].value);
				attachements.append(file);
			} else if (mailContent.parts[partno].ifparameters === 1 && mailContent.parts[partno].parameters[0].attribute.toLowerCase() === 'name') {
				file.append(' ' + mailContent.parts[partno].parameters[0].value);
				attachements.append(file);
			} else if (mailContent.parts[partno].ifparameters === 1 && mailContent.parts[partno].parameters[0].attribute.toLowerCase() === 'charset') {
				mailBody.html('<div class="ajax-loader-div"><?php echo $this->Html->image('/img/ajax-loader.gif'); ?></div>');
				$.ajax({
					type: 'post',
					url: '/imap/imaps/getMailPart.json',
					data: {'msgno': msgno, 'part': partno},
					success: function(data) {
						mailBody.html(decodeURIComponent(data.return.part.message));
					},
					error: function(data) {
						processAjaxError(mailBody, data)
					}
				});
			}
			var dlIco = $('<i></i>').addClass('icon-download');
			var btnDl = $('<a></a>').addClass('btn').addClass('btn-small').addClass('btn-download').append(dlIco).attr('href', '/imap/imaps/getMailPart/' + msgno + '/' + partno);
			file.append(' ').append(btnDl);
		}
		retval.append(attachements).append(mailBody);
		return retval;
	}

	/**
	 *

	 * @param {type} element
	 * @param {type} data
	 * @returns {undefined} */
	function processAjaxError(element, data) {
		element.html('Error fetching data ...');
		element.append('<br />response status : ' + data.status);
		element.append('<br />response statusText : ' + data.statusText);
		element.append('<hr />');
		element.append('<div style="text-align: left;">' + data.responseText + '</div>');
	}


	/**
	 *
	 * @param {type} msgno
	 * @returns {undefined}
	 */
	function getMailInfo(msgno) {
		$('#mailInfo').html('<div class="ajax-loader-div"><?php echo $this->Html->image('/img/ajax-loader.gif'); ?></div>');
		$('#requestStatusMail').removeClass('btn-danger');
		$('#requestStatusMail').removeClass('btn-success');
		$('#requestStatusMail').html('loading ...');
		$.ajax({
			type: 'post',
			url: '/imap/imaps/getMail.json',
			data: {msgno: msgno},
			success: function(data) {
				$('#mailInfo').empty();
				$('#requestStatusMail').html(data.message);
				$('#requestStatusMail').addClass('btn-success');

				$('#mailInfo').append(mkMail(data.return.mail.header));
				$('#mailInfo').append(mkMailContent(data.return.mail.structure, msgno));

			},
			error: function(data) {
				console.log(data);
				$('#requestStatusMail').html(data.statusText);
				$('#requestStatusMail').addClass('btn-danger');
				processAjaxError($('#mailInfo'), data);
			}
		});
	}

	/**
	 *
	 * @returns {undefined}
	 */
	function getImapInfo() {
		$('#mailList').html('<div class="ajax-loader-div"><?php echo $this->Html->image('/img/ajax-loader.gif'); ?></div>');
		$('#mailInfo').html('<div class="alert alert-info">Aucun mail selectionné</div>');
		$('#requestStatusMail').html(' ... ').removeClass('btn-success');

		$('#requestStatus').removeClass('btn-danger');
		$('#requestStatus').removeClass('btn-success');
		$('#requestStatus').html('loading ...');
		$('#nbmail .badge').html('0');
		$.ajax({
			url: '/imap/imaps/retrieve.json',
			success: function(data) {
				$('#mailList').empty();
				$('#requestStatus').html(data.message);
				$('#requestStatus').addClass('btn-success');
				$('#nbmail .badge').html(data.return.mails.length);
				for (var i in data.return.mails) {
					$('#mailList').append(mkMail(data.return.mails[i]));
				}
				$('#mailList .mail').click(function() {
					$('#mailList .mail').removeClass('selected');
					$(this).addClass('selected');
					getMailInfo($(this).attr('data-msgno'));
				});
			},
			error: function(data) {
				console.log(data);
				$('#requestStatus').html(data.statusText);
				$('#requestStatus').addClass('btn-danger');
				processAjaxError($('#mailList'), data);
			}
		});
	}
</script>

<h2>IMAP</h2>
<div id="mailConfig">
	<h3 class="btn">Configuration <i class="icon-circle-arrow-down"></i></h3>
	<dl>
		<dt>Compte</dt><dd><?php echo $conn->configKeyName; ?></dd>
		<dt>Serveur</dt><dd><?php echo $conn->config['server']; ?></dd>
		<dt>Port</dt><dd><?php echo $conn->config['port']; ?></dd>
		<dt>Login</dt><dd><?php echo $conn->config['login']; ?></dd>
		<dt>Password</dt><dd> ************ </dd>
		<dt>Encryption</dt><dd><?php echo (!empty($conn->config['encryption']) ? $conn->config['encryption'] : '&nbsp;'); ?></dd>
		<dt>Login Encryption</dt><dd><?php echo (!empty($conn->config['login_encryption']) ? $conn->config['login_encryption'] : '&nbsp;'); ?></dd>
	</dl>
</div>

<h3>
	Lecture du compte (Bo&icirc;te de r&eacute;ception)
	<span id="btnRefresh" class="btn btn-primary pull-right"><i class="icon-white icon-refresh"></i> refresh</span>
	<span id ="nbmail" class="btn pull-right">Nb : <span class="badge badge-important">0</span></span>
	<span id="requestStatus" class="btn pull-right"></span>
</h3>
<div id="mailList">
	<div class="ajax-loader-div"><?php echo $this->Html->image('/img/ajax-loader.gif'); ?></div>
</div>

<h3>
	Lecture du mail
	<span id="requestStatusMail" class="btn pull-right">...</span>
</h3>
<div id="mailInfo">
	<div class="alert alert-info">Aucun mail selectionné</div>
</div>

<div id="cakePHPconsole">
	<h3 class="btn">CakePHP console <i class="icon-circle-arrow-down"></i></h3>
	<pre>
		<?php debug($conn); ?>
	</pre>
</div>

<script type="text/javascript">
	$('#btnRefresh').click(function() {
		getImapInfo();
	});

	$(document).ready(function() {
		$('#btnRefresh').click();
	});

	$('#mailConfig dl').hide();
	$('#mailConfig h3').click(function() {
		$('#mailConfig dl').slideToggle('fast');
	});

	$('#cakePHPconsole pre').hide();
	$('#cakePHPconsole h3').click(function() {
		$('#cakePHPconsole pre').slideToggle('fast');
	});
</script>
