<?php

App::uses('AppController', 'Controller');

/**
 * CakePHP Imap plugin
 * Imap controller app class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		Imap
 * @subpackage		Imap.Controller
 */
class ImapAppController extends AppController {



}

