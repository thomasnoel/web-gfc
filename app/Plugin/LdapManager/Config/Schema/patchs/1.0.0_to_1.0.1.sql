ALTER TABLE IF EXISTS ldapm_groups ALTER COLUMN "name" TYPE character varying(500);
ALTER TABLE IF EXISTS ldapm_groups ALTER COLUMN dn TYPE text;