<?php

class ModelGroup extends LdapManagerAppModel
{
    public $tablePrefix = 'ldapm_';
    
    public $useTable = 'models_groups';
    
    public $belongsTo = array(
        'Group' => array(
            'className' => 'Group',
            'foreignKey' => 'ldapm_groups_id',
            'type' => 'INNER',
//            'conditions' => null,
//            'fields' => null,
//            'order' => null
        )
    );
//    public $hasOne = array(
//        'Group' => array(
//            'className' => 'Group',
//            'foreignKey' => false,
//            'conditions' => array(
//                'Group.id = ModelGroup.ldapm_groups_id'
//            ),
//            'dependent' => false
//        )
//    );
    
}