<?php

class UserTask extends Shell {
    
    public $uses = array('User','LdapManager.LdapUser', 'LdapManager.ModelGroup', 'LdapManager.Group', 'Connecteur');

    public function execute() {
    }
    
    public function update() {
        
        if (!method_exists($this->User, 'saveLdapManagerLdap')) {
            Shell::error(__d('cake_dev', 'Callback %s not defined in %s', 'saveLdapManagerLdap()', $this->User->alias));
        }
        
        $dataSource = $this->User->getDataSource();
        try
        {
            $dataSource->begin();
            $fields_ldap=$this->LdapUser->getFieldsUserMap();
 
            //Liste groups ldap
            $this->ModelGroup->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->Group->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $model_groups=$this->ModelGroup->find('all');

            foreach($model_groups as $group)
            {
                Shell::out(__('Recherche d\'utilisateurs dans le groupe: ').$group['Group']['name']);
                $usersLdap = $this->LdapUser->getActifMemberOf($group['Group'][$this->LdapUser->getKeyWord('dn')]);
                if(!empty($usersLdap)){
                    foreach($usersLdap as $user_ldap_key=>$user_ldap)
                    {
                        if( !empty($user_ldap)) {
                            $this->User->setDataSource(Configure::read('LdapManager.Ldap.conn'));
                            $this->User->recursive=-1;
                            $UserId=$this->User->{'findBy'.$fields_ldap[$this->LdapUser->getKeyWord('uid')]}( $user_ldap['User'][$this->LdapUser->getKeyWord('uid')], array('id') );

                            // S'il n'existe pas on le crée
                            // Désormais on crée ET on met à jour
//                            if(empty($UserId)){
                                $data = array();
                                foreach ($fields_ldap as $key_field => $field) {
                                    if (!empty($user_ldap['User'][$key_field])) {
                                        $data['User'][$field] = $user_ldap['User'][$key_field];
                                    }
                                }
                                $data['User'][strtolower($group['ModelGroup']['model'].'_id')] = $group['ModelGroup']['foreign_key'];
                                
                                // Open LDAP
                                $this->Connecteur->setDataSource(Configure::read('LdapManager.Ldap.conn'));
                                $ldap = $this->Connecteur->find(
                                    'first',
                                    array(
                                        'conditions' => array(
                                            'Connecteur.name ILIKE' => '%LDAP%'
                                        ),
                                        'contain' => false
                                    )
                                );

                                $user_id=$this->User->saveLdapManagerLdap($data);
                                if(!$user_id){
                                    if( is_array($data['User']['username']) && isset( $data['User']['username'][0]) ) {
                                        Shell::out('L\'utilisateur '. $data['User']['username'][0] . ' existe déjà.');
                                    }
                                    else {
                                        Shell::out('L\'utilisateur '. $data['User']['username'] . ' est déja présent');
                                    }
                                    
                                }
                                else {
                                    if( is_array($data['User']['username']) && isset( $data['User']['username'][0]) ) {
                                        Shell::out('L\'utilisateur '. $data['User']['username'][0] .' a été créé');
                                    }
                                    else {
                                        Shell::out('L\'utilisateur '. $data['User']['username'] . ' a été mis à jour');
                                    }
                                }
//                            }
//                            else {
//                                $user = $this->User->find(
//                                    'first',
//                                    array(
//                                        'conditions' => array(
//                                            'User.id' => $UserId['User']['id']
//                                        ),
//                                        'contain' => false
//                                    )
//                                );
////                                $this->log( $user );
//                                Shell::out('L\'utilisateur '. $user['User']['username'] . ' existe déjà.');
//                            }
                        }
                    }
                }
                Shell::hr();
            }
            
            $dataSource->commit();
            return true;
        }
        catch (ErrorException $e)
        {
            $dataSource->rollback();
            Shell::error($e->getMessage());
        }
        
        return false;
    }
    
}