<?php

App::uses('Folder', 'Utility');
class CompositionsController extends CakeflowAppController {

    public $name = 'Compositions';
    public $helpers = array('Text', 'Cakeflow.Myhtml');
    public $components = array('Paginator', 'Cakeflow.VueDetaillee', 'DataAuthorized', 'NewPastell');

	/**
	 * @var repository utilisée pour la récupération de la liste des sous-types IP
	 */
	public $repository;
// Gestion des droits
//  public $commeDroit = array(
//      'index' => 'Circuits:index',
//      'edit' => 'Circuits:index',
//      'view' => 'Circuits:index',
//      'add' => 'Circuits:index',
//      'delete' => 'Circuits:index');
    //webgfc edit
//  public $components = array('DataAuthorized');

    public function index($etape_id = null) { // FIXME: Composition.etape_id
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $parapheur = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%Parapheur%',
				'Connecteur.use_signature'=> true
            ),
            'contain' => false
                )
        );
        // lecture de l'étape
        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Etapes', 'Compositions'));
        $this->{$this->modelClass}->Etape->Behaviors->attach('Containable');
        $etape = $this->Composition->Etape->find('first', array(
            'fields' => array('id', 'nom'),
            'contain' => array('Circuit.nom', 'Circuit.id'),
            'conditions' => array('Etape.id' => $etape_id)));

//        debug($etape);
        //webgfc edit
        $this->set('circuit_id', $etape['Circuit']['id']);

        if (!empty($etape)) {
            $this->paginate = array(
                'recursive' => -1,
                'conditions' => array('Composition.etape_id' => $etape_id));
            $compositions = $this->Paginator->paginate('Composition');

            //Si le circuit est vide, rediriger vers la vue d'ajout d'étape
            if (empty($compositions) && stripos($this->previous, 'compositions/add') === false)
                $this->redirect(array('action' => 'add', $etape_id));


            // mise en forme pour la vue
            foreach ($compositions as $i => $data) {
                $compositions[$i][$this->modelClass]['typeValidationLibelle'] = $this->{$this->modelClass}->libelleTypeValidation($compositions[$i][$this->modelClass]['type_validation']);
                if ($compositions[$i][$this->modelClass]['trigger_id'] == -1) { //Cas d'une compo délégation parapheur
                    $compositions[$i][$this->modelClass]['typeValidationLibelle'] .= " (" . $parapheur['Connecteur']['type'] . " / " . $compositions[$i][$this->modelClass]['type_document'] . ")";
                }
                $compositions[$i][$this->modelClass]['triggerLibelle'] = $this->formatLinkedModel('Trigger', $compositions[$i][$this->modelClass]['trigger_id']);
            }

            // Peut-on ajouter une composition à cette étape ?
            $canAdd = $this->Composition->Etape->canAdd($etape_id);
            $this->set(compact('compositions', 'etape', 'canAdd'));
        } else {
            $this->Session->setFlash('Etape introuvable', 'growl');
            $this->redirect($this->referer());
        }
    }

    /**
     * Vue détaillée des compositions d'une étape
     */
    public function view($id = null) {
        // Données du connecteur
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $parapheur = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%Parapheur%',
				'Connecteur.use_signature'=> true
            ),
            'contain' => false
                )
        );

        $this->set('ariane', array('Administration', 'Gestion des circuits', 'Etapes', 'Compositions', 'Vue détaillée d\'une composition'));
        $this->{$this->modelClass}->Behaviors->attach('Containable');
        $this->request->data = $this->{$this->modelClass}->find('first', array(
            'contain' => array(
                'Etape.id', 'Etape.nom', 'Etape.Circuit.nom'),
            'conditions' => array('Composition.id' => $id)));
        if (empty($this->request->data)) {
            $this->Session->setFlash(__('Invalide id pour la', true) . ' ' . __('composition', true) . ' : ' . __('affichage de la vue impossible.', true), 'growl', array('type' => 'important'));
            $this->redirect(array('action' => 'index'));
        } else {
            $this->pageTitle = Configure::read('appName') . ' : ' . __('Composition des Circuits de traitement', true) . ' : ' . __('vue d&eacute;taill&eacute;e', true);

            // préparation des informations à afficher dans la vue détaillée
            $maVue = new $this->VueDetaillee(
                    'Vue détaillée de la composition de l\'étape \'' . $this->request->data['Etape']['nom'] . '\' du circuit \'' . $this->request->data['Etape']['Circuit']['nom'] . '\'', __('Retour à la liste des compositions', true), array('action' => 'index', $this->request->data['Etape']['id']));
            $maVue->ajouteSection(__('Informations principales', true));
            $maVue->ajouteLigne(__('Identifiant interne (id)', true), $this->request->data[$this->modelClass]['id']);
            if ($this->request->data[$this->modelClass]['trigger_id'] == -1) {
                $maVue->ajouteLigne("Déclencheur", "Parapheur électronique");
            } else {
                $maVue->ajouteLigne(CAKEFLOW_TRIGGER_TITLE, $this->formatLinkedModel('Trigger', $this->request->data[$this->modelClass]['trigger_id']));
            }
            $maVue->ajouteLigne(__('Type', true), $this->{$this->modelClass}->libelleTypeValidation($this->request->data[$this->modelClass]['type_validation']));
            if ($this->request->data[$this->modelClass]['soustype'] != '')
                $maVue->ajouteLigne(__('Sous-Type', true), $this->request->data[$this->modelClass]['type_document']);
//        $maVue->ajouteLigne(CAKEFLOW_TRIGGER_TITLE, $this->formatLinkedModel('Trigger', $this->request->data[$this->modelClass]['trigger_id']));
//        $maVue->ajouteLigne(__('Type', true), $this->{$this->modelClass}->libelleTypeValidation($this->request->data[$this->modelClass]['type_validation']));
            $maVue->ajouteSection(__('Création / Modification', true));
            $maVue->ajouteLigne(__('Date de cr&eacute;ation', true), $this->request->data[$this->modelClass]['created']);
            $maVue->ajouteElement(__('Par', true), $this->formatUser($this->request->data[$this->modelClass]['created_user_id']));
            $maVue->ajouteLigne(__('Date de dernière modification', true), $this->request->data[$this->modelClass]['modified']);
            $maVue->ajouteElement(__('Par', true), $this->formatUser($this->request->data[$this->modelClass]['modified_user_id']));

            $this->set('contenuVue', $maVue->getContenuVue());
        }
    }

    /**
     *
     */
    /* public function add($id) {
      if (!empty($this->request->data)) {
      $this->setCreatedModifiedUser($this->request->data);
      $this->Composition->create($this->request->data);
      if ($this->Composition->validates($this->request->data)) {
      if ($this->Composition->save()) {
      $this->Session->setFlash(__('Enregistrement effectué.', true), 'growl');
      } else {
      $this->Session->setFlash('Erreur lors de l\'enregistrement.', 'growl');
      }
      }
      $etape_id = $this->request->data['Composition']['etape_id'];
      } else {
      $canAdd = $this->Composition->Etape->canAdd($id);
      if (!$canAdd) {
      $this->Session->setFlash('Impossible d\'ajouter une composition à cette étape', 'growl');
      $this->redirect($this->referer());
      }
      $etape_id = $this->request->data['Composition']['etape_id'] = $id;
      }

      // lecture de la liste des déclencheurs
      //        $this->set('triggers', $this->listLinkedModel('Trigger'));

      //  WebGFC Edit
      $model = CAKEFLOW_TRIGGER_MODEL;
      $this->loadModel($model);
      $desktops = $this->DataAuthorized->getAuthDesktops(array('etapeId' => $id));
      $this->set('triggers', $desktops);
      // Récupération de la liste des services par bureau d'utilisateur sélectionné
      $this->loadModel('User');
      $listServices = array();
      foreach( $desktops as $key => $name ){
      $listServices[$key] = $this->User->Desktop->getServicesList($key);
      }
      $this->set('triggerServices', $listServices);
      // Fin WebGFC edit

      $this->set('typeValidations', $this->Composition->listeTypeValidation());
      $this->set('canAdd', $this->Composition->Etape->canAdd($etape_id));
      } */

    /**
     *
     */
    /* public function edit($id = null) {
      if (!empty($this->request->data)) {
      $this->setCreatedModifiedUser($this->request->data);
      $this->Composition->create($this->request->data);
      if ($this->Composition->validates($this->request->data)) {
      if ($this->Composition->save()) {
      $this->Session->setFlash(__('Enregistrement effectué.', true), 'growl');
      //                    $this->redirect(array('action' => 'index', $this->request->data['Composition']['etape_id']));
      } else {
      $this->Session->setFlash('Erreur lors de l\'enregistrement.', 'growl');
      }
      }
      $etape_id = $this->request->data['Composition']['etape_id'];
      } else {
      $this->request->data = $this->Composition->read(null, $id);
      $etape_id = $this->request->data['Composition']['etape_id'];
      }
      // lecture de la liste des déclencheurs
      //        $this->set('triggers', $this->listLinkedModel('Trigger'));
      //WebGFC Edit
      $triggers = array();
      $users = $this->User->find('all');
      foreach ($users as $user) {
      if ($user['User']['profil_id'] > 2 && $this->User->getCollectiviteId($user['User']['id']) == $this->Session->read('Auth.User.collectivite_id')) {
      $triggers[$user['User']['id']] = $user['User']['username'];
      }
      }
      $this->set('triggers', $triggers);
      $this->set('typeValidations', $this->Composition->listeTypeValidation());
      $this->set('canAdd', $this->Composition->Etape->canAdd($etape_id));
      } */

    /**
     *
     */
    function add($etape_id) {
        $this->set('ariane', array('Administration', 'Composition des circuits', 'Ajout de circuit'));
        $this->_add_edit($etape_id);
    }

    /**
     *
     */
    function edit($id = null) {
        $this->set('ariane', array('Administration', 'Composition des circuits', 'Edition du circuit'));
        $this->_add_edit($id);
    }

    /**
     *
     */
    public function _add_edit($id = null) {

        $this->Connecteur = ClassRegistry::init('Connecteur');
        // Partie i-PArpaheur
        $hasParapheurActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true
                ),
                'contain' => false
            )
        );
        $this->set('hasParapheurActif', $hasParapheurActif);

		// Partie PASTELL
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->set('hasPastellActif', $hasPastellActif);

        if (!empty($this->data)) {
            $this->setCreatedModifiedUser($this->request->data);
            if ($this->request->data['Composition']['type_composition'] == 'PARAPHEUR') {
                $this->Composition->Etape->id = $this->request->data['Composition']['etape_id'];
                $this->Composition->Etape->saveField('soustype', $this->request->data['Composition']['soustype']);
            } else if ($this->request->data['Composition']['type_composition'] == 'PASTELL') {
				$this->Composition->Etape->id = $this->request->data['Composition']['etape_id'];
				$this->Composition->Etape->saveField('type_document', $this->request->data['Composition']['type_document']);
				$this->Composition->Etape->saveField('inforequired_type_document', $this->request->data['Composition']['inforequired_type_document']);
			}
            else
                $this->request->data['Composition']['soustype'] = NULL;

            $this->Composition->create($this->request->data);
            if ($this->Composition->validates($this->request->data)) {
                if ($this->Composition->save()) {
                    $this->Session->setFlash('Enregistrement effectué.', 'growl');
                    return $this->redirect(array('action' => 'index', $this->data['Composition']['etape_id']));
                } else {
                    $this->Session->setFlash('Erreur lors de l\'enregistrement.', 'growl');
                }
            }
            $etape_id = $this->data['Composition']['etape_id'];
        } else if ($this->action == 'edit') {

//            $this->request->data = $this->Composition->read(null, $id);
            $this->request->data = $this->Composition->find(
                'first',
                array(
                    'conditions' => array(
                        'Composition.id' => $id
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );

            $etape_id = $this->request->data['Composition']['etape_id'];
            $this->set('canAddParapheur', ($hasParapheurActif['Connecteur']['use_signature'] && !$this->Composition->hasAny(array('etape_id' => $etape_id, 'trigger_id' => -1, 'id !=' => $id))));

			$canAddPastell = false;
			if( isset($hasPastellActif) && !empty($hasPastellActif) ) {
				$canAddPastell = ($hasPastellActif['Connecteur']['use_pastell'] && !$this->Composition->hasAny(array('etape_id' => $etape_id, 'trigger_id' => -3, 'id !=' => $id)));
			}
			$this->set('canAddPastell', $canAddPastell );

            //WebGFC Edit
            //            //  WebGFC Edit
            $model = CAKEFLOW_TRIGGER_MODEL;
            $this->loadModel($model);
            $desktops = $this->DataAuthorized->getAuthDesktops(array('etapeId' => $etape_id));
            $this->set('triggers', $desktops);
            // Récupération de la liste des services par bureau d'utilisateur sélectionné
            $this->loadModel('User');
            $listServices = array();
            foreach ($desktops as $key => $name) {
                $listServices[$key] = $this->User->Desktop->getServicesList($key);
            }
            $this->set('triggerServices', $listServices);
            // Fin WebGFC edit
             $etape = $this->Composition->Etape->find('first', array(
                'fields' => array('circuit_id'),
                'conditions' => array('Etape.id' => $etape_id),
                'recursive' => -1));
            $this->set('circuit_id', $etape['Etape']['circuit_id']);
        } else if ($this->action == 'add') {
            $canAdd = $this->Composition->Etape->canAdd($id);
            if (!$canAdd) {
                $this->Session->setFlash('Impossible d\'ajouter une composition à cette étape', 'growl');
                return $this->redirect($this->referer());
            }
            $this->request->data['Composition']['etape_id'] = $id;
            $etape_id = $id;


            // Exsite-t-il un connecteur de type aprapheur
//            $this->set('canAddParapheur', (Configure::read('USE_PARAPHEUR') && !$this->Composition->hasAny(array('etape_id' => $id, 'trigger_id' => -1))));
            $this->set('canAddParapheur', ($hasParapheurActif['Connecteur']['use_signature'] && !$this->Composition->hasAny(array('etape_id' => $id, 'trigger_id' => -1))));
			$this->set('canAddPastell', ($hasPastellActif['Connecteur']['use_pastell'] && !$this->Composition->hasAny(array('etape_id' => $id, 'trigger_id' => -3))));
            //  WebGFC Edit
            $model = CAKEFLOW_TRIGGER_MODEL;
            $this->loadModel($model);
            $desktops = $this->DataAuthorized->getAuthDesktops(array('etapeId' => $id));
            $this->set('triggers', $desktops);
            // Récupération de la liste des services par bureau d'utilisateur sélectionné
            $this->loadModel('User');
            $listServices = array();
            foreach ($desktops as $key => $name) {
                $listServices[$key] = $this->User->Desktop->getServicesList($key);
            }
            $this->set('triggerServices', $listServices);
            // Fin WebGFC edit

            $etape = $this->Composition->Etape->find('first', array(
                'fields' => array('circuit_id'),
                'conditions' => array('Etape.id' => $id),
                'recursive' => -1));
            $this->set('circuit_id', $etape['Etape']['circuit_id']);
        }

        $this->set('typeCompositions', $this->Composition->listeTypes());

        // lecture de la liste des déclencheurs
//        $triggers = $this->listLinkedModel('Trigger');
//        $this->set('triggers', $triggers);
        $this->set('typeValidations', $this->Composition->listeTypeValidation());
        $this->set('canAdd', $this->Composition->Etape->canAdd($etape_id));

		$this->set('soustypes', array());
		$this->Session->write('Iparapheur.listSoustype', null);
        if ($hasParapheurActif['Connecteur']['use_signature'] && $hasParapheurActif['Connecteur']['signature_protocol'] == 'IPARAPHEUR') {
			if (empty($this->Session->read('Iparapheur.listSoustype'))) {
				$soustypes = $this->Composition->Etape->listeSousTypesParapheur($hasParapheurActif);
				$soustypes = $soustypes['soustype'];
			}
			else {
				$soustypes = $this->Session->read( 'Iparapheur.listSoustype' );
			}
            if (!empty($soustypes)) {
                $this->set('soustypes', $soustypes);
            } else {
                $this->Session->setFlash('Connexion au parapheur impossible', 'growl');
                $this->set('canAddParapheur', false);
            }
        }

		$documents = [];
		$listSoustypeIp = [];
		if (isset($hasPastellActif) && !empty($hasPastellActif) ) {
			if ($hasPastellActif['Connecteur']['use_pastell']) {
				if (empty($this->Session->read('Pastell.listSoustypeIp'))) {
					$id_entity = $hasPastellActif['Connecteur']['id_entity'];
					// Récup des sous-types dispos
					$PastellComponent = new NewPastellComponent();
					$listSoustypeIp = $PastellComponent->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
					$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
				}
				$documents = $this->Composition->Etape->Circuit->getDocNamePastell($etape['Etape']['circuit_id']);
				$listSoustypeIp = $this->Session->read('Pastell.listSoustypeIp');
			}
		}

		$this->set('listSoustypeIp', $listSoustypeIp);
		$this->set('documents', $documents);
        $this->render('add_edit');
    }

    /**
     *
     */
    public function delete($id = null) {
        $this->loadModel('Desktopmanager');
        $desktopsmanagers = $this->Desktopmanager->find('list');
        $compo = $this->Composition->find(
            'first',
            array(
                'conditions' => array('Composition.id' => $id),
                'recursive' => -1,
                'contain' => false
            )
        );
        if ($this->Composition->delete($id)) {
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le bureau ".$desktopsmanagers[$compo['Composition']['trigger_id']]. " de l'étape ". $compo['Composition']['etape_id'] ." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', array());

            $this->Session->setFlash(__('Suppression effectuée', true), 'growl');
        } else {
            $this->Session->setFlash('Erreur lors de la suppression', 'growl');
        }
        $this->autoRender = false;
//        $this->redirect($this->referer());
    }

}

?>
