-- --------------------------------------------------------
--
-- Structure de la table `wkf_compositions`
--
ALTER TABLE `wkf_compositions` CHANGE `user_id` `trigger_id` INT( 10 ) NULL DEFAULT NULL;

ALTER TABLE `wkf_compositions`
ADD `created_user_id` INT( 11 ) NULL DEFAULT NULL ,
ADD `modified_user_id` INT( 11 ) NULL DEFAULT NULL ,
ADD `created` DATETIME NULL DEFAULT NULL ,
ADD `modified` DATETIME NULL DEFAULT NULL;


-- --------------------------------------------------------
--
-- Structure de la table `wkf_traitements`
--
ALTER TABLE `wkf_traitements` DROP `user_id`;

ALTER TABLE `wkf_traitements` DROP `etape_id`;

ALTER TABLE `wkf_traitements` CHANGE `archive_id` `target_id` INT( 11 ) NOT NULL;

ALTER TABLE `wkf_traitements` ADD `treated` TINYINT NOT NULL DEFAULT '0' AFTER `numero_traitement`;

ALTER TABLE `wkf_traitements` DROP `created`;

ALTER TABLE `wkf_traitements`
ADD `created_user_id` INT( 11 ) NULL DEFAULT NULL ,
ADD `modified_user_id` INT( 11 ) NULL DEFAULT NULL ,
ADD `created` DATETIME NULL DEFAULT NULL ,
ADD `modified` DATETIME NULL DEFAULT NULL;


-- --------------------------------------------------------
--
-- Structure de la table `wkf_visas`
--
ALTER TABLE `wkf_visas` CHANGE `user_id` `trigger_id` INT( 11 ) NOT NULL;
ALTER TABLE `wkf_visas` CHANGE `commentaire` `commentaire` VARCHAR( 500 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL;
ALTER TABLE `wkf_visas` CHANGE `signature_id` `signature_id` INT( 11 ) NULL;

ALTER TABLE `wkf_visas` DROP `composition_id`;
ALTER TABLE `wkf_visas` DROP `etape_id`;
ALTER TABLE `wkf_visas` DROP `created`;

ALTER TABLE `wkf_visas`
ADD `etape_nom` VARCHAR( 250 ) NULL AFTER `signature_id`,
ADD `etape_type` INT( 10 ) NOT NULL AFTER `etape_nom`,
ADD `date` DATETIME NULL AFTER `commentaire`,
ADD `type_validation` VARCHAR( 1 ) NOT NULL;
