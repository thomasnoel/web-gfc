-- phpMyAdmin SQL Dump
-- version 2.11.8.1deb5+lenny4
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Jeu 02 Septembre 2010 à 16:35
-- Version du serveur: 5.0.51
-- Version de PHP: 5.2.6-1+lenny9



--
-- Base de données: `webdelib`
--

-- --------------------------------------------------------

--
-- Structure de la table `wkf_circuits`
--

CREATE TABLE wkf_circuits (
  id serial not null,
  nom character varying(250) NOT NULL,
  description bytea,
  actif integer NOT NULL default '1',
  defaut integer NOT NULL default '0',
  created_user_id integer NOT NULL,
  modified_user_id integer default NULL,
  created timestamp without time zone,
  modified timestamp without time zone
);
ALTER TABLE wkf_circuits ADD CONSTRAINT wkf_circuits_pkey PRIMARY KEY (id);

--
-- Contenu de la table `wkf_circuits`
--

--INSERT INTO `wkf_circuits` (`id`, `nom`, `description`, `actif`, `defaut`, `created_user_id`, `modified_user_id`, `created`, `modified`) VALUES
--(1, 'Validation', '', 1, 0, 0, NULL, '2010-08-18 10:15:09', '2010-08-18 10:15:09'),
--(2, 'Collaboratif', '', 1, 0, 0, NULL, '2010-08-19 16:11:03', '2010-08-19 16:11:03'),
--(3, 'Circuit 1', 'Ce circuit est destiné au service Transport', 1, 1, 0, NULL, '2010-08-31 16:00:01', '2010-08-31 16:00:01');

-- --------------------------------------------------------

--
-- Structure de la table `wkf_compositions`
--

CREATE TABLE wkf_compositions (
  id serial not null,
  etape_id integer NOT NULL,
  type_validation character varying(1) NOT NULL,
  created_user_id integer DEFAULT NULL,
  modified_user_id integer DEFAULT NULL,
  created timestamp without time zone,
  modified timestamp without time zone,
  trigger_id integer default NULL
);
ALTER TABLE wkf_compositions ADD CONSTRAINT wkf_compositions_pkey PRIMARY KEY (id);

--
-- Contenu de la table `wkf_compositions`
--

--INSERT INTO `wkf_compositions` (`id`, `etape_id`, `type_validation`, `user_id`) VALUES
--(1, 1, 'V', 2),
--(2, 2, 'V', 3),
--(3, 2, 'V', 4),
--(4, 3, 'V', 5),
--(5, 3, 'V', 6),
--(6, 4, 'V', 7),
--(7, 3, 'V', 9),
--(9, 5, 'V', 5),
--(10, 6, 'V', 7),
--(11, 5, 'V', 6),
--(12, 7, 'V', 8),
--(13, 8, 'V', 3),
--(14, 9, 'V', 4),
--(15, 10, 'V', 6),
--(16, 10, 'V', 7),
--(17, 11, 'V', 10),
--(18, 11, 'V', 11);

-- --------------------------------------------------------

--
-- Structure de la table `wkf_etapes`
--

CREATE TABLE wkf_etapes (
  id serial not null,
  circuit_id integer NOT NULL,
  nom character varying(250) NOT NULL,
  description bytea,
  type integer NOT NULL,
  ordre integer NOT NULL,
  created_user_id integer NOT NULL,
  modified_user_id integer default NULL,
  created timestamp without time zone,
  modified timestamp without time zone
--  KEY `nom` (`nom`)
);
ALTER TABLE wkf_etapes ADD CONSTRAINT wkf_etapes_pkey PRIMARY KEY (id);

--
-- Contenu de la table `wkf_etapes`
--

--INSERT INTO `wkf_etapes` (`id`, `circuit_id`, `nom`, `description`, `type`, `ordre`, `created_user_id`, `modified_user_id`, `created`, `modified`) VALUES
--(1, 1, 'Première étape', '', 1, 1, 0, NULL, '2010-08-18 10:15:21', '2010-08-18 10:15:21'),
--(2, 1, 'Deuxième étape', '', 2, 2, 0, NULL, '2010-08-18 10:15:31', '2010-08-18 10:15:58'),
--(3, 1, 'Troisième étape', '', 3, 3, 0, NULL, '2010-08-18 10:15:46', '2010-08-18 10:16:05'),
--(4, 1, 'Quatrième et fin', '', 1, 4, 0, NULL, '2010-08-18 10:16:19', '2010-08-18 10:16:19'),
--(5, 2, 'fisrt', '', 3, 1, 0, NULL, '2010-08-19 16:11:18', '2010-08-19 16:11:18'),
--(6, 2, 'last', '', 1, 2, 0, NULL, '2010-08-19 16:11:27', '2010-08-19 16:11:27'),
--(8, 3, 'nom de l''étape 1 du circuit 1', '', 1, 1, 0, NULL, '2010-08-31 16:06:55', '2010-08-31 16:06:55'),
--(9, 3, 'Etape 2 du circuit 1', '', 1, 2, 0, NULL, '2010-09-01 17:33:07', '2010-09-01 17:33:07'),
--(10, 3, 'Etape 3 du circuit 1', '', 2, 3, 0, NULL, '2010-09-01 17:34:40', '2010-09-01 17:34:40'),
--(11, 3, 'Etape 4 du circuit 1', '', 3, 4, 0, NULL, '2010-09-01 17:36:31', '2010-09-01 17:36:31');

-- --------------------------------------------------------

--
-- Structure de la table `wkf_signatures`
--

CREATE TABLE wkf_signatures(
  id serial not null,
  type_signature character varying (100) NOT NULL,
  signature bytea NOT NULL

);
ALTER TABLE wkf_signatures ADD CONSTRAINT wkf_signatures_pkey PRIMARY KEY (id);

--
-- Contenu de la table `wkf_signatures`
--


-- --------------------------------------------------------

--
-- Structure de la table `wkf_traitements`
--

CREATE TABLE wkf_traitements(
  id serial not null,
  circuit_id integer NOT NULL,
  target_id integer NOT NULL,
  numero_traitement integer NOT NULL default '1',
  treated boolean NOT NULL,
  created_user_id integer DEFAULT NULL,
  modified_user_id integer DEFAULT NULL,
  created timestamp without time zone,
  modified timestamp without time zone

);
ALTER TABLE wkf_traitements ADD CONSTRAINT wkf_traitements_pkey PRIMARY KEY (id);


--
-- Contenu de la table `wkf_traitements`
--


-- --------------------------------------------------------

--
-- Structure de la table `wkf_visas`
--

CREATE TABLE wkf_visas(
  id serial not null,
  traitement_id integer NOT NULL,
  trigger_id integer NOT NULL,
  signature_id integer NULL,
  etape_nom character varying(250),
  etape_type integer NOT NULL,
  action character varying(2) NOT NULL,
  commentaire character varying(500) ,
  date timestamp without time zone,
  type_validation character varying(1) NOT NULL,
  numero_traitement integer NOT NULL
);
ALTER TABLE wkf_visas ADD CONSTRAINT wkf_visas_pkey PRIMARY KEY (id);

--
-- Contenu de la table `wkf_visas`
--


--
-- Indexes
--

CREATE UNIQUE INDEX wkf_cicruits_unique_nom ON wkf_circuits (nom);
CREATE INDEX wkf_traitements_target_id_key ON wkf_traitements (target_id);
CREATE INDEX wkf_visas_traitement_id_key ON wkf_visas (traitement_id);
CREATE INDEX wkf_visas_trigger_id_key ON wkf_visas (trigger_id);

--
-- Relations
--
ALTER TABLE ONLY wkf_circuits ADD CONSTRAINT users_wkf_circuits_fkey FOREIGN KEY (created_user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY wkf_circuits ADD CONSTRAINT users_wkf_circuits_fkey2 FOREIGN KEY (modified_user_id) REFERENCES users(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY wkf_compositions ADD CONSTRAINT wkf_compositions_wkf_etapes_fkey FOREIGN KEY (etape_id) REFERENCES wkf_etapes(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE ONLY wkf_etapes ADD CONSTRAINT wkf_circuits_wkf_compositions_fkey FOREIGN KEY (circuit_id) REFERENCES wkf_circuits(id) ON UPDATE CASCADE ON DELETE CASCADE;




