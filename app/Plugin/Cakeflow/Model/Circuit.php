<?php

/**
 * modele des circuits
 */
App::uses('CakeflowAppModel', 'Cakeflow.Model');

class Circuit extends CakeflowAppModel {

    public $name = 'Circuit';
    public $tablePrefix = 'wkf_';
    public $displayName = 'nom';
    public $displayField = 'nom';

    /**     * *******************************************************************
     * 	Associations
     * ** ****************************************************************** */
    public $belongsTo = array(
        'CreatedUser' => array(
            'className' => CAKEFLOW_USER_MODEL,
            'foreignKey' => 'created_user_id'
        ),
        'ModifiedUser' => array(
            'className' => CAKEFLOW_USER_MODEL,
            'foreignKey' => 'modified_user_id'
        )
    );
    public $hasMany = array(
        'Cakeflow.Etape',
        'Cakeflow.Traitement',
        //WebGFC edit
        'Soustype'
    );
    //WebGFC edit
    /**
     *
     * @var type
     */
    public $hasAndBelongsToMany = array(
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_metadonnees',
            'foreignKey' => 'metadonnee_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheMetadonnee'
        )
    );

    /**     * *******************************************************************
     * 	Règles de validation
     * ** ****************************************************************** */
    public $validate = array(
        'nom' => array(
            array(
                'rule' => array('maxLength', '250'),
                'message' => 'Maximum 250 caractères'
            ),
            //Webgfc edit
            //TODO: remettre les regles d unicite
            array(
                'rule' => 'isUnique',
                'message' => 'Valeur déjà utilisée'
            ),
            array(
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Champ obligatoire'
            )
        )
    );

    /*
     * les regles de controle et les messages d'erreurs de la publiciable de classe 'validate'
     * sont initialisées ici car on utilise les fonctions d'internationalisation __()
     * que l'on ne peut pas utiliser lors de la déclaration de la publiciable
     */

    public function beforeValidate($options = Array()) {
        // nom : unique et obligatoire
        $this->validate['nom']['obligatoire'] = array(
            'rule' => 'notBlank',
            'required' => true,
            'message' => __('Information obligatoire.', true) . ' ' . __('Veuillez saisir un nom.', true)
        );
//     $this->validate['nom']['unique'] = array(
//         'rule' => 'isUnique',
//         'message' => __('Ce nom est deja utilise.', true) . ' ' . __('Veuillez saisir un autre nom.', true)
//     );
        // actif : au moins un actif
        $this->validate['actif']['auMoinsUnActif'] = array(
            'rule' => 'verifActif',
            'message' => __('Il faut au moins un circuit de traitement actif.', true) . ' ' . __('Veuillez cocher ce circuit actif.', true)
        );


        return true;
    }

    /**
     * Vérifie qu'au moins un circuit est actif
     */
    public function verifActif() {
        return (
                $this->data[$this->name]['actif'] ||
                $this->find('count', array(
                    'conditions' => array(
                        'Circuit.id <>' => $this->data[$this->name]['id'],
                        'Circuit.actif' => true),
                    'recursive' => -1)));
    }

    /**     * *******************************************************************
     * 	INFO: ne servait qu'avec UserDelegue ? à vérifier
     * ** ****************************************************************** */
    /* function afterFind( $results, $primary = false ) {
      $compositions = Set::extract( $results, '/Etape/Composition' );
      if( $primary && !empty( $compositions ) ) {
      foreach( $results as $i => $result ) {
      foreach( $result['Etape'] as $j => $etape ) {
      foreach( $etape['Composition'] as $k => $composition ) {
      $user = $this->Etape->Composition->User->find(
      'first',
      array(
      'fields' => array( 'User.nom', 'User.prenom' ),
      'conditions' => array( 'User.id' => $composition['trigger_id'] ),
      'recursive' => -1
      )
      );
      $results[$i]['Etape'][$j]['Composition'][$k] = Set::merge( $results[$i]['Etape'][$j]['Composition'][$k], $user );

      $userDelegue = $this->Etape->Composition->UserDelegue->find(
      'first',
      array(
      'fields' => array( 'UserDelegue.nom', 'UserDelegue.prenom' ),
      'conditions' => array( 'UserDelegue.id' => $composition['user_delegue_id'] ),
      'recursive' => -1
      )
      );
      $results[$i]['Etape'][$j]['Composition'][$k] = Set::merge( $results[$i]['Etape'][$j]['Composition'][$k], $userDelegue );
      }
      }
      }
      }
      return $results;
      } */

    /**
     * Détermine si une instance peut être supprimée tout en respectant l'intégrité référentielle
     * Paramètre : id
     */
    public function isDeletable($id) {
        // Existence de l'instance en base
        if (!$this->find('count', array('recursive' => -1, 'conditions' => array('id' => $id))))
            return false;

        // Existence d'une étape liée
        /*
          if ($this->Etape->find('count', array('recursive' => -1, 'conditions' => array('circuit_id' => $id))))
          return false;
         *
         */

        return true;
    }

    /**
     * Mise à jour du champ défaut
     */
    public function afterSave($created, $options = Array()) {
        if (CAKEFLOW_GERE_DEFAUT && $this->data[$this->name]['defaut'])
            $this->updateAll(
                    array('Circuit.defaut' => 'false'), array('Circuit.id <>' => $this->id));
    }

    /**
     * Retourne l'id du circuit actif par défaut
     * @return integer id du circuit actif par défaut
     */
    public function getDefautId() {
        $data = $this->find('first', array(
            'conditions' => array('actif' => true, 'defaut' => true),
            'fields' => array('id'),
            'recursive' => -1));
        if (empty($data))
            return null;
        else
            return $data['Circuit']['id'];
    }

    /**
     * Retourne les id des circuits actifs et ayant au moins une étape
     * @return array liste des id
     */
    public function actifsAuMoinsUneEtape() {
        // initialisations
        $ret = null;
        // lecture de tous les circuits actifs
        $circuits = $this->find('all', array(
            'conditions' => array('actif' => true),
            'fields' => array('id'),
            'recursive' => -1));
        foreach ($circuits as $circuit) {
            if ($this->Etape->hasAny(array('circuit_id' => $circuit['Circuit']['id'])))
                $ret[] = $circuit['Circuit']['id'];
        }

        return $ret;
    }

    public function getList() {
        return $this->find('list', array('conditions' => array('actif' => 1), 'order' => array('nom')));
    }

    public function listeCircuitsParUtilisateur($user_id) {
        $circuits = array();
        $compositions = $this->Etape->Composition->find('all', array('conditions' => array('Composition.trigger_id' => $user_id), 'contain' => array('Etape')));
        foreach ($compositions as $composition) {
            if (!empty($composition['Etape']['circuit_id']))
                array_push($circuits, $composition['Etape']['circuit_id']);
        }
        return implode($circuits, ',');
    }

    public function listeCircuitsParDesktops($desktop_id) {
        $circuits = array();
        $compositions = $this->Etape->Composition->find('all', array('conditions' => array('Composition.trigger_id' => $desktop_id), 'contain' => array('Etape')));
        foreach ($compositions as $composition) {
            if (!empty($composition['Etape']['circuit_id']))
                array_push($circuits, $composition['Etape']['circuit_id']);
        }
        return $circuits;
    }

    public function listeCircuitsParDesktopsmanagers($desktopmanager_id) {
        $circuits = array();
        $compositions = $this->Etape->Composition->find('all', array('conditions' => array('Composition.trigger_id' => $desktopmanager_id), 'contain' => array('Etape')));
        foreach ($compositions as $composition) {
            if (!empty($composition['Etape']['circuit_id']))
                array_push($circuits, $composition['Etape']['circuit_id']);
        }

        return $circuits;
    }

    public function getLibelle($circuit_id = null) {
        if ($circuit_id == null)
            return '';
        $circuit = $this->find('first', array('conditions' => "Circuit.id = $circuit_id",
            'recursive' => '-1',
            'field' => 'nom'));
        return $circuit['Circuit']['nom'];
    }

    public function getAllMembers($circuit_id, $options = array()) {
        $members = array();
        $etapes = $this->Etape->find(
        	'all',
			array(
				'fields' => array(
					'Etape.id',
					'Etape.ordre'
				),
				'conditions' => array(
					'Etape.circuit_id' => $circuit_id
				),
				'recursive' => -1,
				'order' => 'Etape.ordre ASC'
			)
		);

        foreach ($etapes as $etape) {
            $compositions = $this->Etape->Composition->find(
            	'all',
				array(
					'recursive' => -1,
                	'conditions' => array(
                		'Composition.etape_id' => $etape['Etape']['id']
					),
					'fields' => 'Composition.trigger_id'
				)
			);
            foreach ($compositions as $composition) {
                $members[$etape['Etape']['ordre']][] = $composition['Composition']['trigger_id'];
            }
        }
        return $members;
    }

    /* public function userInCircuit($user_id, $circuit_id) {
      $etapes = $this->Etape->find(
      'all',
      array(
      'conditions' => array('Etape.circuit_id' => $circuit_id),
      'order' => array('Etape.ordre ASC')
      )
      );

      $traitement_id = Hash::extract($circuit, 'Traitement.{n}');

      foreach ($etapes as $etape) {
      foreach ($etape['Composition'] as $composition) {
      if ($composition['trigger_id'] == $user_id)
      return true;
      }
      }
      return false;
      } */

    public function userInCircuit($user_id, $circuit_id) {
        $circuitInfo = $this->find(
                'first', array(
            'conditions' => array(
                'Circuit.id' => $circuit_id
            ),
            'contain' => array(
                'Etape' => array(
                    'Composition'
                )
            )
                )
        );
        $visa = false;
        foreach ($circuitInfo['Etape'] as $etape) {
            foreach ($etape['Composition'] as $composition) {
                if ($composition['trigger_id'] == $user_id)
                    $visa = true;
            }
        }
        return !empty($visa);
    }

    /**
     * Insert une cible ($targetId) dans le circuit de traitement $circuitId
     * @param integer $circuitId identifiant du circuit
     * @param integer $targetId identifiant de la cible (objet à faire traiter dans le circuit)
     * @param integer $createdUserId idenifiant de l'utilisateur connecté a l'origine de la création
     */
    public function insertDansCircuit($circuitId, $targetId, $createdUserId = null) {
        // on vérifie l'existence du circuit
        if (!$this->find('count', array('recursive' => -1, 'conditions' => array('id' => $circuitId))))
            return false;
        // on vérifie l'existence de la cible
        if (!$this->Traitement->{CAKEFLOW_TARGET_MODEL}->find('count', array('recursive' => -1, 'conditions' => array('id' => $targetId))))
            return false;
        // on vérifie que le traitement n'existe pas déjà en base
        if ($this->Traitement->find('count', array('recursive' => -1, 'conditions' => array('target_id' => $targetId))))
            return false;


        // ajout d'une occurence dans la table traitement
        $traitement = $this->Traitement->create();
        $traitement['Traitement']['circuit_id'] = $circuitId;
        $traitement['Traitement']['target_id'] = $targetId;
        $traitement['Traitement']['numero_traitement'] = 1;
//        $traitement['Traitement']['treated'] = false;
        $traitement['Traitement']['treated'] = 0;
        $traitement['Traitement']['created_user_id'] = $createdUserId;
        $traitement['Traitement']['modified_user_id'] = $createdUserId;
        $this->Traitement->save($traitement);
        $traitement['Traitement']['id'] = $this->Traitement->id;

        // ajout des occurences dans la table visa
        $this->Etape->Behaviors->attach('Containable');
        $etapes = $this->Etape->find('all', array(
            'fields' => array('Etape.id', 'Etape.nom', 'Etape.type', 'Etape.soustype', 'Etape.ordre', 'Etape.cpt_retard', 'Etape.description', 'Etape.type_document', 'Etape.inforequired_type_document'),
            'contain' => array('Composition.trigger_id', 'Composition.type_validation'),
            'conditions' => array('Etape.circuit_id' => $circuitId),
            'order' => array('Etape.ordre ASC')
        ));


//debug($etapes);
        foreach ($etapes as $etape) {
            foreach ($etape['Composition'] as $composition) {

//        foreach($desktopsIds as $i => $desktopId ) {
                $visa = $this->Traitement->Visa->create();
                $visa['Visa']['traitement_id'] = $traitement['Traitement']['id'];
                $visa['Visa']['trigger_id'] = $composition['trigger_id'];
//            $visa['Visa']['trigger_id'] = $desktopId;
                $visa['Visa']['signature_id'] = 0;
                $visa['Visa']['etape_nom'] = $etape['Etape']['nom'];
                $visa['Visa']['etape_id'] = $etape['Etape']['id'];
                $visa['Visa']['etape_type'] = $etape['Etape']['type'];
                //TODO computeDateRetard not work (getSeanceDeliberanteId, can't find function)
//                $visa['Visa']['date_retard'] = $this->Etape->computeDateRetard($etape['Etape']['cpt_retard'], $targetId);
                $visa['Visa']['action'] = 'RI';
                $visa['Visa']['commentaire'] = $etape['Etape']['description'];
                $visa['Visa']['date'] = null;
                $visa['Visa']['numero_traitement'] = $etape['Etape']['ordre'];
                $visa['Visa']['type_validation'] = $composition['type_validation'];
                $visa['Visa']['soustype'] = $etape['Etape']['soustype'];
                $visa['Visa']['type_document'] = $etape['Etape']['type_document'];
                $visa['Visa']['inforequired_type_document'] = $etape['Etape']['inforequired_type_document'];
                $this->Traitement->Visa->save($visa);
//        }
            }
        }

        return true;
    }

    /**
     * Ajoute au traitement terminé de la cible $targetId les étapes du circuit $circuitId
     * @param integer $circuitId identifiant du circuit
     * @param integer $targetId identifiant de la cible (objet à faire traiter dans le circuit)
     * @param integer $createdUserId idenifiant de l'utilisateur connecté a l'origine de la création
     */
    public function ajouteCircuit($circuitId, $targetId, $createdUserId = null) {
        // on vérifie l'existence du circuit
        if (!$this->find('count', array('recursive' => -1, 'conditions' => array('id' => $circuitId))))
            return false;
        // on vérifie l'existence de la cible
        if (!$this->Traitement->{CAKEFLOW_TARGET_MODEL}->find('count', array('recursive' => -1, 'conditions' => array('id' => $targetId))))
            return false;
        // on vérifie que le traitement existe
        if (!$this->Traitement->find('count', array('recursive' => -1, 'conditions' => array('target_id' => $targetId))))
            return false;

        // lecture du traitement
        $traitement = $this->Traitement->find('first', array(
            'recursive' => -1,
            'fields' => array('id', 'numero_traitement'),
            'conditions' => array(
                'target_id' => $targetId,
                'treated' => true)));
        if (empty($traitement))
            return false;

        // ajout des occurences dans la table visa
        $this->Etape->Behaviors->attach('Containable');
        $etapes = $this->Etape->find('all', array(
            'fields' => array('Etape.id', 'Etape.nom', 'Etape.type', 'Etape.ordre', 'Etape.cpt_retard', 'Etape.soustype', 'Etape.type_document', 'Etape.inforequired_type_document'),
            'contain' => array('Composition.trigger_id', 'Composition.type_validation'),
            'conditions' => array('Etape.circuit_id' => $circuitId),
            'order' => array('Etape.ordre ASC')
        ));
        foreach ($etapes as $etape) {
            foreach ($etape['Composition'] as $composition) {
                $visa = $this->Traitement->Visa->create();
                $visa['Visa']['traitement_id'] = $traitement['Traitement']['id'];
                $visa['Visa']['trigger_id'] = $composition['trigger_id'];
                $visa['Visa']['signature_id'] = 0;
                $visa['Visa']['etape_nom'] = $etape['Etape']['nom'];
                $visa['Visa']['etape_id'] = $etape['Etape']['id'];
                $visa['Visa']['etape_type'] = $etape['Etape']['type'];
                $visa['Visa']['date_retard'] = $this->Etape->computeDateRetard($etape['Etape']['cpt_retard'], $targetId);
                $visa['Visa']['action'] = 'RI';
                $visa['Visa']['commentaire'] = '';
                $visa['Visa']['date'] = null;
                $visa['Visa']['numero_traitement'] = $traitement['Traitement']['numero_traitement'] + $etape['Etape']['ordre'];
                $visa['Visa']['type_validation'] = $composition['type_validation'];
				$visa['Visa']['soustype'] = $etape['soustype'];
				$visa['Visa']['type_document'] = $etape['type_document'];
				$visa['Visa']['inforequired_type_document'] = $etape['inforequired_type_document'];
                $this->Traitement->Visa->save($visa);
            }
        }

        // mise à jour du traitement
        $traitement['Traitement']['circuit_id'] = $circuitId;
        $traitement['Traitement']['modified_user_id'] = $createdUserId;
        $traitement['Traitement']['treated'] = false;
        $traitement['Traitement']['numero_traitement'] ++;
        $this->Traitement->save($traitement);

        return true;
    }

    public function hasEtapeDelegation($circuit_id) {
        $this->Etape->Behaviors->attach('Containable');
        $etapes = $this->Etape->find('all', array(
            'conditions' => array(
                'Etape.circuit_id' => $circuit_id
            ),
            'contain' => array('Composition.type_validation'),
        ));
        $compositions = Hash::extract($etapes, '{n}.Composition.{n}.type_validation');
        return in_array('D', $compositions);
    }

    public function desktopInCircuit($circuit_id) {
        $listOfDesktops = array();
        $circuitInfo = $this->find('first', array(
            'conditions' => array(
                'Circuit.id' => $circuit_id
            ),
            'contain' => array(
                'Etape' => array(
                    'Composition'
                )
            )
        ));

        $visa = false;
        if(!empty($circuitInfo)) {
            foreach ($circuitInfo['Etape'] as $etape) {
                foreach ($etape['Composition'] as $composition) {
                    $listOfDesktops[] = $composition['trigger_id'];
                }
            }
        }
        return $listOfDesktops;
    }


	/**
	 * Méthode pour retourner si le circuit est actif ou non
	 * @param $circuit_id
	 * @return bool
	 */
	public function isActif($circuit_id) {
		$circuit = $this->find('first', array(
			'conditions' => array(
				'Circuit.id' => $circuit_id,
				'Circuit.actif' => true
			),
			'contain' => false
		));
		$isActif = false;
		if( !empty($circuit) ) {
			$isActif = true;
		}
		return $isActif;
	}


	/**
	 *
	 * @param type $params
	 */
	public function search($params) {
		if (empty($params)) {
			$conditions = array(
				'Circuit.actif' => true
			);
		}

		if (!empty($params)) {
			if (isset($params['Circuit']['nom']) && !empty($params['Circuit']['nom'])) {
				$conditions[] = 'Circuit.nom ILIKE \'' . $this->wildcard('%' . $params['Circuit']['nom'] . '%') . '\'';
			}
			if (isset($params['Circuit']['actif'])) {
				$conditions[] = array('Circuit.actif' => $params['Circuit']['actif']);
			}
		}

		$querydata = array(
			'conditions' => array(
				$conditions
			),
			'order' => 'Circuit.nom',
			'contain' => false
		);

		return $querydata;
	}

	/**
	 * Focntion permettant de remonter la liste des documents autorisés pour Pastell
	 *  ainsi que le cheminement visé
	 * @param $id
	 */
	public function getDocNamePastell($id) {
		$typePastell = [
			Configure::read('Pastell.fluxStudioName') => 'Cheminement défini dans le sous-type'
		];

		$documents = array_merge(
			Configure::read('Pastell.typesdoccircuit'),
			$typePastell
		);
		return $documents;
	}
}

?>
