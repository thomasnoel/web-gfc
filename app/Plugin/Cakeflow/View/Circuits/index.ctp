<?php $this->Html->css(array('cake.generic'));?>
<div class="elementBordered2">
<?php
//echo $this->Html->css( 'Cakeflow.design.css'); //FIXME: à voir

echo $this->Html->tag('h2', __('Liste des circuits de traitement', true));

echo $this->element('indexPageCourante');
echo $this->Html->tag('table', null, array('cellpadding' => '0', 'cellspacing' => '0'));
// initialisation de l'entete du tableau
$tableHeaders = array(
    $this->Paginator->sort(__('nom', true), 'Nom'),
    __('Description', true),
    __('Etapes', true),
    $this->Paginator->sort(__('actif', true), 'Actif'));

if (CAKEFLOW_GERE_DEFAUT)
    $tableHeaders[] = __('Défaut', true);
$tableHeaders[] = __('Actions', true);
echo $this->Html->tag('thead', $this->Html->tableHeaders($tableHeaders));

//echo $this->Html->tableHeaders($tableHeaders);
//debug($tableHeaders);
foreach ($this->data as $rownum => $rowElement) {
    
//    $attributTr = ($rownum & 1) ? array() : array('class' => 'altrow');
//    echo $this->Html->tag('tr', null, $attributTr);
     echo $this->Html->tag('tr', null);

    echo $this->Html->tag('td', $rowElement['Circuit']['nom']);
    echo $this->Html->tag('td', $rowElement['Circuit']['description']);
    echo $this->Html->tag('td');
    foreach ($rowElement['Etape'] as $etape) {
        echo $etape['Etape']['nom'] . ' (' . $listeType[$etape['Etape']['type']] . ')' . '<br/>';
    }
    echo $this->Html->tag('/td');
    echo $this->Html->tag('td', $rowElement['Circuit']['actifLibelle']);
    if (CAKEFLOW_GERE_DEFAUT)
        echo $this->Html->tag('td', $rowElement['Circuit']['defautLibelle']);
     echo $this->Html->tag('td', null, array('style' => 'text-align:center'));

    echo $this->Html->tag('td', null, array('class' => 'actions'));
    echo $this->element('boutonAction', array(
        'class' => 'link_wkf_etapes',
        'plugin' => 'cakeflow',
        'url' => array('controller' => 'etapes', 'action' => 'index', $rowElement['Circuit']['id']),
        'title' => __('Étapes', true),
        'iconPath' => '/img/icons/etape.png'));
    if ($rowElement['ListeActions']['view'])
        echo $this->element('boutonAction', array('plugin' => 'cakeflow', 'url' => array('action' => 'view', $rowElement['Circuit']['id'])));
    if ($rowElement['ListeActions']['edit'])
        echo $this->element('boutonAction', array('plugin' => 'cakeflow', 'url' => array('action' => 'edit', $rowElement['Circuit']['id'])));
    if ($rowElement['ListeActions']['delete'])
        echo $this->element('boutonAction', array('plugin' => 'cakeflow', 'url' => array('action' => 'delete', $rowElement['Circuit']['id']),
            'confirmMessage' => __('Voulez-vous supprimer le circuit de traitement : ', true) . $rowElement['Circuit']['nom']));
    if ($rowElement['ListeActions']['visuCircuit'])
        echo $this->element('boutonAction', array(
            'plugin' => 'cakeflow',
            'url' => array('action' => 'visuCircuit', $rowElement['Circuit']['id']),
            'title' => __('Visionner', true),
            'iconPath' => '/img/icons/visionner.png'));
    echo $this->Html->tag('/td', null);
    echo $this->Html->tag('/tr', null);
}
echo $this->Html->tag('/table', null);
echo $this->element('indexPageNavigation');
?>

<div class="actions">
    <ul>
        <li><?php echo $this->Html->link(__('Ajouter un circuit', true), array('action' => 'add'), array(), false, false); ?></li>
    </ul>
</div>
</div>