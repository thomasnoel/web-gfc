<script type="text/javascript">
<?php if (isset($type) && $type == 'important') : ?>
	layer.msg("<?php echo '<div class=\'important\'>'.$content_for_layout.'</div>'; ?>", {offset: '70px'});
<?php elseif (isset($type) && $type == 'erreur') : ?>
	layer.msg("<?php echo '<div class=\'erreur\'>'.$content_for_layout.'</div>'; ?>", {offset: '70px'});
<?php else : ?>
	layer.msg("<?php echo $content_for_layout; ?>", {});
<?php endif; ?>
</script>
