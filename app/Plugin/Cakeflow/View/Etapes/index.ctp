<div class="elementBordered2">
<?php

//$this->pageTitle = sprintf(__("Étapes du circuit '%s'", true), $circuit['Circuit']['nom']);
//echo $this->Html->tag('h2', $this->pageTitle);

if (!empty($etapes)) {
    $cells = '';

    foreach ($etapes as $rownum => $etape) {
        $row = Set::extract($etape, 'Etape');

        // Liens pour changer la position de l'étape
        $ordre = $etape['Etape']['ordre'];
        $moveUp = ( ( $ordre > 1 ) ? html_entity_decode($this->Html->link('&#9650;', array('action' => 'moveUp', $row['id']), array(), false)) : '&#9650;' );
        $moveDown = ( ( $ordre < $nbrEtapes ) ? html_entity_decode($this->Html->link('&#9660;', array('action' => 'moveDown', $row['id']), array(), false)) : '&#9660;' );

        // Mise en forme de la liste des déclencheurs
        $triggers = array();
        foreach ($etape['Composition'] as $composition) {
            $triggers[] = $composition['libelleTrigger'];
        }

        $cells .= '<tr ' . ( ($rownum & 1) ? '' : ' class="altrow"' ) . '>
					<td>' . "$moveUp $ordre $moveDown" . '</td>
					<td>' . h($this->Text->truncate($row['nom'], 30, array('ending' => '...', 'exact' => true, 'html' => true))) . '</td>
					<td>' . $this->Text->truncate($row['description'], 30, array('ending' => '...', 'exact' => true, 'html' => true)) . '</td>
					<td>' . h($row['libelleType']) . '</td>
					<td>' . implode(', ', $triggers) . '</td>';
         if (!empty($row['cpt_retard']))
			$cells .= '<td><i class="fa fa-clock-o"  aria-hidden="true"></i> ' . $row['cpt_retard'] . ' jours avant la séance</td>';
        elseif (!isset($row['cpt_retard']))
            $cells .= '<td><i class="fa fa-ban"  aria-hidden="true"></i> <em>Pas d\'alerte de retard programmée<em></td>';
        else
            $cells .= '<td><i class="fa fa-clock-o"  aria-hidden="true"></i> Le jour de la séance</td>';

        $cells .= '<td style="text-align:center">';
        $cells .= $this->Myhtml->bouton(array('controller' => 'compositions', 'action' => 'index', $etape['Etape']['id']), __("Composition de l'étape", true), false, '/cakeflow/img/icons/composition.png');
        $cells .= $this->Myhtml->bouton(array('action' => 'view', $etape['Etape']['id']), 'Voir') . '
					' . $this->Myhtml->bouton(array('action' => 'edit', $etape['Etape']['id']), 'Modifier') . '
					' . (($etape['ListeActions']['delete']) ? $this->Myhtml->bouton(array('action' => 'delete', $etape['Etape']['id']), 'Supprimer', 'Voulez vous réellement supprimer l&acute;étape '.$etape['Etape']['nom'].' ?') : '') . '
				</td>
			</tr>';
    }

//$cells .= html_entity_decode($this->Myhtml->bouton(array('action' => 'edit', $etape['Etape']['id']), __('Modifier l\'étape', true), false, '/img/icons/modifier.png'));
    $headers = $this->Html->tableHeaders(
                    array(
                        __('Ordre', true),
                        __('Nom', true),
                        __('Description', true),
                        __('Type', true),
                        __('Utilisateur(s)', true),
                        __('Délais avant retard', true),
                        __('Actions', true)
                    )
    );

    echo $this->element('indexPageCourante');
    echo $this->Html->tag('table', $this->Html->tag('thead', $headers) . $this->Html->tag('tbody', $cells), array('class' => 'table table-striped'));
//    echo $this->Html->tag( 'table', $this->Html->tag('thead', $headers) . $this->Html->tag('tbody', $cells), array('cellpadding' => 0, 'cellspacing' => 0));
    echo $this->element('indexPageNavigation');
}
?>

    <div class="actions">
        <ul>
        <?php echo $this->Html->tag('li', $this->Html->link(__('Ajouter une étape', true), array('action' => 'add', $this->params['pass'][0]))); ?>
        <?php echo $this->Html->tag('li', $this->Html->link(__('Retour à la liste des circuits', true), array('controller' => 'circuits', 'action' => 'index'))); ?>
        </ul>
    </div>
</div>