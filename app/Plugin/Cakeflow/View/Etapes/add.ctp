<?php

echo $this->Html->script('formValidate.js', array('inline' => true)); ?>

<!--<div class="panel modal-dialog modal-lg" id="addContactModal">
    <div class="panel-default">
            <div class="panel-heading">
                    <h4 class="panel-title"><?php echo __d('contact', 'Contactinfo.add') ?></h4>
            </div>
            <div class="panel-body">-->
<?php
echo $this->Form->create('Etape',array(
                                        'class' => 'form-horizontal',
                                        'inputDefaults' => array(
                                          'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                          'div' => array('class' => 'form-group'),
                                          'label' => array('class' => 'control-label  col-sm-3'),
                                          'between' => '<div class="controls  col-sm-5">',
                                          'after' => '<div class="help-block with-errors"></div></div>',
                                          'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                          'data-toggle'=>"validator"
                                      )));  
echo $this->Form->input('Etape.circuit_id', array('type' => 'hidden'));
echo $this->Form->input('Etape.nom', array('label' => array('text'=> __d('circuit', 'Etape.nom', true),'class'=>'control-label  col-sm-3'),'class' => 'form-control','required'));
echo $this->Form->input('Etape.description', array('label' => array('text'=> __d('circuit', 'Etape.description', true),'class'=>'control-label  col-sm-3'),'class' => 'form-control'));
echo $this->Form->input('Etape.type', array('options' => $types, 'empty' => true, 'label' => array('text'=> __('Type', true),'class'=>'control-label  col-sm-3'),'class' => 'form-control','required'));
?>
</div>
<script>
    console.log('ciici');
//    $('.select2-choice').css('width', '348px');
    $('.control-group + p').css('color', 'red');
</script>
<!--            <div class="panel-footer addContact-controls" >

            </div>-->
<!--    </div>
</div>-->
