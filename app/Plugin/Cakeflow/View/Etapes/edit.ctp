<?php

echo $this->Form->create('Etape');
echo $this->Form->input('Etape.circuit_id', array('type' => 'hidden'));
echo $this->Form->input('Etape.id');
echo $this->Form->input('Etape.ordre', array('type' => 'hidden'));
echo $this->Form->input('Etape.nom', array('label' => __d('circuit', 'Etape.nom', true)));
echo $this->Form->input('Etape.description', array('label' => __d('circuit', 'Etape.description', true)));
echo $this->Form->input('Etape.type', array('options' => $types, 'empty' => true, 'label' => __('Type', true)));
?>
