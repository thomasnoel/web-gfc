<?php
debug('heree');
echo $this->Form->create('Composition',array(
                                        'class' => 'form-horizontal',
                                        'url' => Router::url(null, true),
                                        'inputDefaults' => array(
                                          'format' => array('before', 'label', 'between', 'input', 'error', 'after'),
                                          'div' => array('class' => 'form-group'),
                                          'label' => array('class' => 'control-label  col-sm-3'),
                                          'between' => '<div class="controls  col-sm-5">',
                                          'after' => '</div>',
                                          'error' => array('attributes' => array('wrap' => 'span', 'class' => 'help-inline')),
                                      ))); 
echo $this->Html->tag('div', $this->Form->input('Composition.etape_id', array('type' => 'hidden')));
echo $this->Form->input('Composition.trigger_id', array('id' => 'selectUser', 'type' => 'select', 'empty' => true, 'required' => true, 'label' =>array('text'=>  CAKEFLOW_TRIGGER_TITLE,'class'=>'control-label  col-sm-5'), "style"=>'color:#555;','class' => 'form-control'));
?>
<!-- <script type="text/javascript">

    function selectService(parentSelect) {
        var desktopId = $('option:selected', $(parentSelect)).val();

        $('#serviceUserFlux option').css('display', 'none');
        $('#serviceUserFlux option.void').css('display', 'block');
        $('#serviceUserFlux option.desktop' + desktopId).css('display', 'block');
    }
</script> -->
    <?php
//    echo $this->Form->input('Composition.trigger_id', array('type' => 'select', 'empty' => true, 'required' => true, 'label' => CAKEFLOW_TRIGGER_TITLE, 'onchange' => 'selectService(this)'));

//    $addService = '<h3>Veuillez sélectionner un service</h3><select id="serviceUserFlux" name="serviceUserFlux">';
//    $addService .= '<option style="display:none;" class="void" value=""> --- </option>';
//    foreach ($triggers as $desktopId => $desktopName) {
//        foreach ($triggerServices[$desktopId] as $serviceId => $serviceName) {
//            $addService .= '<option style="display:none;" class="desktop' . $desktopId . '" value="' . $serviceId . '">' . $serviceName . '</option>';
//        }
//    }
//
//    $addService .= '</select>';
//    echo $addService;

if (CAKEFLOW_GERE_SIGNATURE) {
  echo $this->Form->input('Composition.type_validation', array('type' => 'radio', 'label' =>array('text'=>  'Type de validation','class'=>'control-label  col-sm-5'),'class' => 'form-control'));
} else {
  echo $this->Html->tag('div', $this->Form->input('Composition.type_validation', array('type' => 'hidden', 'value' => 'V')));
}
?>
