<?php

/**
 * Jsonmsg Plugin
 * Jsonmsg component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class JsonmsgComponent extends Component {

	/**
	 * Message JSON
	 *
	 * @access public
	 * @var array
	 */
	public $json = array('success' => '', 'message' => '', 'debug' => '');

	/**
	 * Controller
	 *
	 * @access public
	 * @var Controller
	 */
	public $controller;

	/**
	 * * Component initialization
	 *
	 * @access public
	 * @param Controller $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->controller = $controller;
	}

	/**
	 * Initialise le message standard (json)
	 *
	 * @access public
	 * @param string $message
	 * @return void
	 */
	public function initJsonMessage($message = null) {
		$this->json['success'] = false;
		$this->json['message'] = (isset($message) ? $message : __d('default', 'save.error'));
	}

	/**
	 * Initialise le message standard (json) (wrapper)
	 *
	 * @access public
	 * @param string $message
	 * @return void
	 */
	public function init($message = null) {
		$this->initJsonMessage($message);
	}

	/**
	 * prépare le message standard (json) de reponse positive
	 *
	 * @access public
	 * @param string $message
	 * @return void
	 */
	public function setJsonMessageOk($message = null) {
		$this->json['success'] = true;
		$this->json['message'] = (isset($message) ? $message : __d('default', 'save.ok'));
	}

	/**
	 * prépare le message standard (json) de reponse positive (wrapper)
	 *
	 * @access public
	 * @param string $message
	 * @return void
	 */
	public function valid($message = null) {
		$this->setJsonMessageOk($message);
	}

	/**
	 * Initialise le message standard d'erreur
	 *
	 * @access public
	 * @param string $message
	 * @return void
	 */
	public function error($message = null) {
		$this->initJsonMessage($message);
	}

	/**
	 * Serialize function for use with RequestHandlerComponent, and Route::parseExtensions() (CakePHP >= 2.1)
	 */
	public function serialize() {
		$this->controller->set('json', $this->json);
		$this->controller->set('_serialize', 'json');
	}

	/**
	 * Envoi du message standard encodé en JSON
	 *
	 * @access public
	 * @param array $json
	 * @return void
	 */
	public function sendJson($json) {
		$this->controller->autoRender = false;
		$content = json_encode($json);
		header('Pragma: no-cache');
		header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
		header('Content-Type: text/x-json');
		header("X-JSON: {$content}");
		Configure::write('debug', 0);
		echo $content;
		return;
	}

	/**
	 * Envoi du message standard encodé en JSON (wrapper)
	 *
	 * @access public
	 * @param array $json
	 * @return void
	 */
	public function sendJsonResponse($jsonResponse) {
		$this->sendJson($jsonResponse);
	}

	/**
	 * Envoi du message standard
	 *
	 * @access public
	 * @param boolean $debug affiche des informations de debug
	 * @return void
	 */
	public function sendJsonMessage($debug = false) {
		if (Configure::read('debug') > 0) {
			$this->json['message'] .= '<hr />';
			$this->json['message'] .= 'Controller: ' . $this->controller->name . '<br />';
			$this->json['message'] .= 'Action: ' . $this->controller->action;
		}
		if ($debug && Configure::read('debug') > 0) {
			$this->json['message'] .= '<hr />' . $this->json['debug'];
		}
		$this->sendJson($this->json);
	}

	/**
	 * Envoi du message standard (wrapper)
	 *
	 * @access public
	 * @param array $json
	 * @return void
	 */
	public function send($debug = false) {
		$this->sendJsonMessage($debug);
	}

	/**
	 * Stocke un message de debug dans le message standard
	 *
	 * @access public
	 * @param string $dbg
	 * @return void
	 */
	public function debug($dbg = null) {
		$this->json['debug'] = $dbg;
	}

}

?>
