<?php
	/**
	 * Code source de la classe Gedooo2ConverterUnoconv.
	 *
	 * PHP 5.3
	 *
	 * @package Gedooo2
	 * @subpackage Utility.Converter
	 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
	 */
	App::uses( 'Debugger', 'Utility' );
	App::uses( 'Gedooo2AbstractConverter', 'Gedooo2.Utility/Converter' );

	/**
	 * La classe Gedooo2ConverterUnoconv ...
     *
     * @fixme Le code est totalement incorrect, c'est simplement un copié/collé
     *  du contenu d'une méthode d'une classe équivalente du plugin Gedooo (WebRSA).
	 *
	 * @package Gedooo2
	 * @subpackage Utility.Converter
	 */
	abstract class Gedooo2ConverterUnoconv extends Gedooo2AbstractConverter
	{
		/**
		 * Le chemin vers l'exécutable unoconv
		 *
		 * @var string
		 */
		protected static $_path = null;

		/**
		 * Le chemin vers le répertoire temporaire.
		 *
		 * @var string
		 */
		protected static $_tmpDir = null;

        /**
         * Les chemins vers les variables dans la configuration CakePHP:
         *  - Gedooo2.Gedooo2ConverterUnoconv.path (string)
         *
         * @var array
         */
        public static $configured = array(
			'path' => array(
                'path' => 'Gedooo2.Gedooo2ConverterUnoconv.path',
                'type' => 'string',
                'default' => '/usr/bin/unoconv',
			),
			'tmp_dir' => array(
                'path' => 'Gedooo2.Gedooo2ConverterUnoconv.tmp_dir',
                'type' => 'string',
                'default' => TMP,
			),
		);

		/**
		 * Initialisation: si le chemin vers l'exécutable pas été spécifié, on
		 * essaie de lire sa valeur configurée sous la clé
		 * Gedooo2.Gedooo2ConverterUnoconv.path (string).
		 * Gedooo2.Gedooo2ConverterUnoconv.path (string).
		 */
		protected static function _init() {
			self::$_path = self::_configured( self::$configured, 'path', self::$_path );
			self::$_tmpDir = self::_configured( self::$configured, 'tmp_dir', self::$_tmpDir );
		}

		/**
		 * Initialisation et conversion du contenu d'un fichier d'un format vers
		 * un autre.
		 *
		 * @fixme Pas encore implémenté
		 *
		 * @param string $content Le contenu du fichier à convertir
		 * @param string $inputFormat Le format d'entrée du fichier.
		 * @param string $outputFormat Le format de sortie du fichier.
		 * @return string
		 */
		public static function convert( $content, $inputFormat = 'odt', $outputFormat = 'pdf' ) {
			self::_init();
			return false;

//			if( empty( self::$_path ) ) {
//				Debugger::log( 'Exécutable unoconv non spécifié' , LOG_ERROR );
//				return false;
//			}
			// exécution
//			$fileName = escapeshellarg( $fileName );
//			$cmd = "LANG=fr_FR.UTF-8; {$bin} -f {$format} --stdout {$fileName}";
//			$result = shell_exec( $cmd );

			// guess that if there is less than this characters probably an error
//			if( strlen( $result ) < 10 ) {
//				Debugger::log( sprintf( "Résultat de la conversion erroné: %s", $result ) , LOG_ERROR );
//				return false;
//			}

//			return $result;
		}
	}
?>
