-- DataAcl : CakePHP Data Acl plugin
-- PostgreSQL table creation
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

CREATE TABLE dacos (
  id serial not null primary key,
  parent_id INTEGER DEFAULT NULL,
  model character varying(255),
  foreign_key INTEGER DEFAULT NULL,
  alias character varying(255),
  lft INTEGER DEFAULT NULL,
  rght INTEGER DEFAULT NULL
);

CREATE TABLE daros_dacos (
  id serial not null primary key,
  daro_id integer NOT NULL,
  daco_id integer NOT NULL,
  _create CHAR(2) NOT NULL DEFAULT 0,
  _read CHAR(2) NOT NULL DEFAULT 0,
  _update CHAR(2) NOT NULL DEFAULT 0,
  _delete CHAR(2) NOT NULL DEFAULT 0
);

CREATE TABLE daros (
  id serial not null primary key,
  parent_id integer DEFAULT NULL,
  model character varying(255),
  foreign_key integer DEFAULT NULL,
  alias character varying(255),
  lft integer DEFAULT NULL,
  rght integer DEFAULT NULL
);
