<?php

App::uses('Component', 'Controller');
App::uses('AclComponent', 'Controller/Component');
App::uses('Session', 'Controller/Component');

/**
 * Acl Extended Component class
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * * based on macduy SessionAcl Component
 * @link http://bakery.cakephp.org/fre/%20articles/view/4cb560cb-60e0-4aa5-b7e1-4b4ed13e7814
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Controller.Component
 */
class AclExtendedComponent extends AclComponent {

	public $components = array('Session');

	/**
	 *
	 * @param type $controller
	 */
	public function initialize(Controller $controller) {
		parent::initialize($controller);
		$controller->Acl = $this;
	}

	/**
	 *
	 * @param type $aro
	 * @param type $aco
	 * @param type $action
	 * @return type
	 */
	public function check($aro, $aco, $action = "*") {
		$path = $this->__cachePath($aro, $aco, $action);
		if ($this->Session->check($path)) {
			return $this->Session->read($path);
		} else {
			$check = parent::check($aro, $aco, $action);
			$this->Session->write($path, $check);
			return $check;
		}
	}

	/**
	 *
	 * @param type $aro
	 * @param type $aco
	 * @param type $action
	 */
	public function allow($aro, $aco, $action = "*") {
		parent::allow($aro, $aco, $action);
		$this->__delete($aro, $aco, $action);
	}

	/**
	 *
	 * @param type $aro
	 * @param type $aco
	 * @param type $action
	 */
	public function deny($aro, $aco, $action = "*") {
		parent::deny($aro, $aco, $action);
		$this->__delete($aro, $aco, $action);
	}

	/**
	 * Inherit method.
	 *
	 * This method overrides and uses the original
	 * method. It only adds cache to it.
	 *
	 * @param string $aro ARO
	 * @param string $aco ACO
	 * @param string $action Action (defaults to *)
	 * @access public
	 */
	public function inherit($aro, $aco, $action = "*") {
		parent::inherit($aro, $aco, $action);
		$this->__delete($aro, $aco, $action);
	}

	/**
	 * Grant method.
	 *
	 * This method overrides and uses the original
	 * method. It only adds cache to it.
	 *
	 * @param string $aro ARO
	 * @param string $aco ACO
	 * @param string $action Action (defaults to *)
	 * @access public
	 */
	public function grant($aro, $aco, $action = "*") {
		parent::grant($aro, $aco, $action);
		$this->__delete($aro, $aco, $action);
	}

	/**
	 * Revoke method.
	 *
	 * This method overrides and uses the original
	 * method. It only adds cache to it.
	 *
	 * @param string $aro ARO
	 * @param string $aco ACO
	 * @param string $action Action (defaults to *)
	 * @access public
	 */
	public function revoke($aro, $aco, $action = "*") {
		parent::revoke($aro, $aco, $action);
		$this->__delete($aro, $aco, $action);
	}

	/**
	 * Returns a unique, dot separated path to use as the cache key. Copied from CachedAcl.
	 *
	 * @param string $aro ARO
	 * @param string $aco ACO
	 * @param boolean $acoPath Boolean to return only the path to the ACO or the full path to the permission.
	 * @access private
	 */
	public function __cachePath($aro, $aco, $action, $acoPath = false) {
		if ($action != "*") {
			$aco .= '/' . $action;
		}
		$path = Inflector::slug($aco);

		if (!$acoPath) {
			if (!is_array($aro)) {
				$_aro = explode(':', $aro);
			} elseif (Set::countDim($aro) > 1) {
				$_aro = array(key($aro), current(current($aro)));
			} else {
				$_aro = array_values($aro);
			}
			$path .= '.' . Inflector::slug(implode('.', $_aro));
		}

		return "Acl." . $path;
	}

	/**
	 *
	 * @param type $aro
	 * @param type $aco
	 * @param type $action
	 */
	public function __delete($aro, $aco, $action) {
		$key = $this->__cachePath($aro, $aco, $action, true);
		if ($this->Session->check($key)) {
			$this->Session->delete($key);
		}
	}

	/**
	 *
	 */
	public function flushCache() {
		$this->Session->delete('Acl');
	}

	/**
	 *
	 * @param type $aro
	 * @param type $pairs
	 * @return boolean
	 */
	public function all($aro, $pairs) {
		foreach ($pairs as $aco => $action) {
			if (!$this->check($aro, $aco, $action)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks that AT LEAST ONE of given pairs of aco-action is satisfied
	 *
	 * @param type $aro
	 * @param type $pairs
	 * @return boolean
	 */
	public function one($aro, $pairs) {
		foreach ($pairs as $aco => $action) {
			if ($this->check($aro, $aco, $action)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Returns an array of booleans for each $aco-$aro pair
	 *
	 * @param type $aro
	 * @param type $pairs
	 * @return type
	 */
	public function can($aro, $pairs) {
		$can = array();
		$i = 0;
		foreach ($pairs as $aco => $action) {
			$can[$i] = $this->check($aro, $aco, $action);
			$i++;
		}
		return $can;
	}

	/**
	 *
	 * @param type $aro
	 * @return type
	 */
	public function getRightsGrid($aro, $acos = array(), $options = array('actions' => '*', 'alias' => '')) {
		if (empty($acos)) {
			$acos = $this->_Instance->Aco->find('threaded');
		}
		$return = array();
		foreach ($acos as $aco) {
			$return[$aco['Aco']['id']] = $this->getRights($aro, $aco, $options);
		}
		return $return;
	}

	/**
	 *
	 * Recursive check
	 *
	 * @param type $aro
	 * @param type $aco
	 * @return type
	 */
	public function getRights($aro, $aco, $options = array('actions' => '*')) {

		if (empty($options['alias'])) {
			$options['alias'] = '';
		}

		$options['alias'] .= '/' . $aco['Aco']['alias'];
		$options['alias'] = ltrim($options['alias'], '/');

		$actions = $options['actions'];
		$checks = array();
		if ($actions == '*' || is_array($actions) && in_array('*', $actions)) {
			$chk = $this->check($aro['Aro'], $options['alias']);
			$checks['read'] = $chk;
		}
		if ($actions == 'create' || is_array($actions) && in_array('create', $actions)) {
			$checks['create'] = $this->check($aro['Aro'], $options['alias'], 'create');
		}
		if ($actions == 'read' || is_array($actions) && in_array('read', $actions)) {
			$checks['read'] = $this->check($aro['Aro'], $options['alias'], 'read');
		}
		if ($actions == 'update' || is_array($actions) && in_array('update', $actions)) {
			$checks['update'] = $this->check($aro['Aro'], $options['alias'], 'update');
		}
		if ($actions == 'delete' || is_array($actions) && in_array('delete', $actions)) {
			$checks['delete'] = $this->check($aro['Aro'], $options['alias'], 'delete');
		}

		$return = array(
			'checks' => $checks,
			'alias' => $options['alias']
		);
		if (!empty($aco['children'])) {
			$return['children'] = $this->getRightsGrid($aro, $aco['children'], $options);
			$this->getRightsGrid($aro, $aco['children']);
		}
		return $return;
	}

	/**
	 *
	 * @param type $requester
	 * @param type $rightsGrid
	 */
	public function setRights($requester, $rightsGrid) {
		$querydata = array(
			'fields' => array(
				'Aco.id',
				'Aco.alias',
				'Aco.parent_id'
			)
		);

		$arbo = $this->_Instance->Aco->find('threaded', $querydata);
		$keys = array_keys(current($rightsGrid));

		$acos = array();
		foreach ($arbo as $item) {
			$acos[] = $this->_normalizeAcos($item, $rightsGrid, current($keys));
		}
		$acos = $this->_processAcos($requester, $acos);
		$aro = array('model' => key($requester), 'foreign_key' => current($requester));
		$saved = $this->_saveAcos($aro, $acos);
		$this->flushCache();
		return $saved;
	}

	/**
	 *
	 * @param type $aco
	 * @param type $rights
	 * @param type $key
	 * @param type $parentAlias
	 * @return type
	 */
	private function _normalizeAcos($aco, $rights, $key, $parentAlias = null) {
		$alias = (!empty($parentAlias) ? $parentAlias . '__' . $aco['Aco']['alias'] : $aco['Aco']['alias']);
		$value = $rights[$alias][$key];
		$children = array();

		if (!empty($aco['children'])) {
			foreach ($aco['children'] as $child) {
				$children[] = $this->_normalizeAcos($child, $rights, $key, $alias);
			}
		}

		$return = array(
			'Aco' => array(
				'alias' => $alias,
				'value' => $value
			),
			'children' => $children
		);
		return $return;
	}

	/**
	 *
	 * @param type $requester
	 * @param type $acos
	 * @param type $parentValue
	 * @return type
	 */
	private function _processAcos($requester, $acos, $parentValue = false) {
		foreach ($acos as $key => $aco) {
			if (!empty($aco['children'])) {
				$aco['children'] = $this->_processAcos($requester, $aco['children'], $aco['Aco']['value']);
			}
			$acos[$key] = $this->_processAco($aco, $parentValue);
		}
		return $acos;
	}

	/**
	 *
	 * @param type $aco
	 * @param type $parentValue
	 * @return boolean
	 */
	private function _processAco($aco, $parentValue = false) {
		$aco['Aco']['save'] = false;
		$aco['Aco']['inherit'] = false;
		if (!empty($aco['children'])) {
			//recuperation des valeurs des enfants
			$values = array();
			foreach ($aco['children'] as $child) {
				$values[] = $child['Aco']['value'];
			}
			if ($this->_array_sum_boolean($values) == count($values) || $this->_array_sum_boolean($values) == 0 && $aco['Aco']['value'] == 1) {
				$aco['Aco']['save'] = true;
				for ($i = 0; $i < count($aco['children']); $i++) {
					$aco['children'][$i]['Aco']['inherit'] = true;
				}
			}
		}
		if ($aco['Aco']['value'] == true && $parentValue != true) {
			$aco['Aco']['save'] = true;
		}
		return $aco;
	}

	/**
	 *
	 * @param type $aro
	 * @param type $acos
	 * @param type $first
	 * @return type
	 */
	private function _saveAcos($aro, $acos, $first = true) {
		$return = array();

		if ($first) {
			$aroId = $this->_Instance->Aro->field('Aro.id', $aro);
			$this->_Instance->Aco->Permission->query('delete from aros_acos where aro_id = ' . $aroId . ';');
		}
		foreach ($acos as $aco) {
			if (!empty($aco['children'])) {
				$this->_saveAcos($aro, $aco['children'], false);
			}
			$acoAlias = implode('/', explode('__', $aco['Aco']['alias']));

			if ($aco['Aco']['inherit'] == true) {
				$return[] = $this->_Instance->inherit($aro, $acoAlias);
			} else if ($aco['Aco']['value'] == true/* && $aco['Aco']['save'] == true */) {
				$return[] = $this->_Instance->allow($aro, $acoAlias);
			} else {
				$return[] = $this->_Instance->deny($aro, $acoAlias);
			}
		}
		return !in_array(false, $return, true);
	}

	/**
	 *
	 * @param type $array
	 * @return int
	 */
	private function _array_sum_boolean($array) {
		$cpt = 0;
		for ($i = 0; $i < count($array); $i++) {
			if ($array[$i] === true) {
				$cpt++;
			}
		}
		return $cpt;
	}

	/**
	 *
	 * @param type $var
	 */
	public function dbg($var) {
		if (Configure::read('debug') > 2) {
			$this->log(var_export($var, true), 'debug');
		}
	}

}

?>
