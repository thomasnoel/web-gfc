<?php

App::uses('AppModel', 'Model');
App::uses('AclNode', 'Model');
App::uses('DataAclNode', 'DataAcl.Model');

/**
 * DataAcl Daro model class
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Model
 */
class Daro extends DataAclNode {

	/**
	 * Model name
	 *
	 * @var string
	 */
	public $name = 'Daro';

	/**
	 *
	 * @var type
	 */
	public $useTable = "daros";

	/**
	 *
	 * @var type
	 */
	public $alias = "Daro";

	/**
	 * AROs are linked to ACOs by means of Permission
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array('Daco' => array('className' => 'DataAcl.Daco', 'with' => 'DataAcl.Dpermission'));

	/**
	 *
	 * @param type $daroId
	 * @return null
	 */
	public function initRights($daroId = null) {
		$defaultValue = Configure::read('DataAcl.defaultAccessValue.Daro');
		if ($defaultValue == null) {
			$defaultValue = 1;
		}

		if ($daroId != null) {
			$this->id = $daroId;
		}

		if (!$this->id) {
			return null;
		}

		$parent_id = $this->field('parent_id');

		if (empty($parent_id)) {
			$dacos = $this->Daco->find('list', array('conditions' => array('parent_id' => null)));
			foreach ($dacos as $daco_id) {

				$data = array(
					'DarosDaco' => array(
						'daro_id' => $this->id,
						'daco_id' => $daco_id,
						'_create' => $defaultValue,
						'_read' => $defaultValue,
						'_update' => $defaultValue,
						'_delete' => $defaultValue,
					)
				);
				$this->Dpermission->create();
				$this->Dpermission->save($data);
			}
		}

		return true;
	}

}

