<?php

App::uses('AppModel', 'Model');

/**
 * DataAcl Dpermission model class
 *
 * Dpermissions linking DAROs with DACOs
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * DataAcl : CakePHP Data Acl plugin
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		DataAcl
 * @subpackage		DataAcl.Model
 */
class Dpermission extends DataAclAppModel {

	/**
	 * Model name
	 *
	 * @var string
	 */
	public $name = 'Dpermission';

	/**
	 * Explicitly disable in-memory query caching
	 *
	 * @var boolean
	 */
	public $cacheQueries = false;

	/**
	 * Override default table name
	 *
	 * @var string
	 */
	public $useTable = 'daros_dacos';

	/**
	 *
	 * @var type
	 */
	public $alias = "DarosDaco";

	/**
	 * Dpermissions link AROs with ACOs
	 *
	 * @var array
	 */
	public $belongsTo = array(
		'Daro' => array('className' => 'DataAcl.Daro'),
		'Daco' => array('className' => 'DataAcl.Daco')
	);

	/**
	 * No behaviors for this model
	 *
	 * @var array
	 */
	public $actsAs = null;

	/**
	 * Constructor, used to tell this model to use the
	 * database configured for ACL
	 */
	public function __construct() {
		$config = Configure::read('DataAcl.database');
		if (!empty($config)) {
			$this->setDataSource($config);
		}
		parent::__construct();
	}

}
