<?php

App::uses('Helper', 'View');
App::uses('Controller', 'Controller');
App::uses('SCCommentHelper', 'SimpleComment.View/Helper');

/**
 * SimpleComment Plugin
 * SimpleComment SCComment helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package             SimpleComment
 * @subpackage          SimpleComment.View.Helper
 */
class SCCommentHelper2 extends SCCommentHelper {

    public function drawComment($comment, $editParams = array()) {
        return $this->_drawComment($comment, $editParams);
    }

}

class SCCommentHelperTest extends CakeTestCase {

    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = array();

    /**
     *
     * @var type
     */
    public $SCComment;

    /**
     *
     */
    public function setUp() {
        parent::setUp();
        $Controller = new Controller();
        $View = new View($Controller);
        $this->SCComment = new SCCommentHelper2($View);
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown() {
        parent::tearDown();
        unset($this->View, $this->App);
    }

    /**
     * @expectedException BadMethodCallException
     *
     */
    public function testDrawCommentEmptyException() {
        $this->SCComment->drawComment(null);
    }

    /**
     *
     */
    public function testDrawCommentPublic() {
        $editParams = array(
            'url' => ''
        );

        $comment = array(
            'id' => 64,
            'date' => '24/01/2014 16:04:16',
            'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
            'comment' => 'Bonjour ceci est un commentaire 2',
            'private' => false,
            'editable' => false,
            'readers' => array()
        );

        $expected = '<div class="sc-comment"><table><tr><td class="sc-content">Bonjour ceci est un commentaire 2</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 16:04:16</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd></dl></td></tr></table></div>';

        $result = $this->SCComment->drawComment($comment, $editParams);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawCommentPublicEditable() {
        $editParams = array(
            'url' => ''
        );

        $comment = array(
            'id' => 64,
            'date' => '24/01/2014 16:04:16',
            'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
            'comment' => 'Bonjour ceci est un commentaire 2',
            'private' => false,
            'editable' => true,
            'readers' => array()
        );

        $expected = '<div class="sc-comment sc-editable"><div class="sc-header"><img src="/img/delete.png" class="sc-delete-btn" title="Supprimer" alt="Supprimer" idComment="64" /><img src="/img/tool.png" class="sc-edit-btn" title="Modifier" alt="Modifier" idComment="64" /></div><table><tr><td class="sc-content">Bonjour ceci est un commentaire 2</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 16:04:16</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd></dl></td></tr></table></div>';
        $result = $this->SCComment->drawComment($comment, $editParams);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawCommentPublicWithoutParams() {
        $editParams = null;
        $comment = array(
            'id' => 64,
            'date' => '24/01/2014 16:04:16',
            'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
            'comment' => 'Bonjour ceci est un commentaire 2',
            'private' => false,
            'editable' => false,
            'readers' => array()
        );

        $expected = '<div class="sc-comment"><table><tr><td class="sc-content">Bonjour ceci est un commentaire 2</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 16:04:16</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd></dl></td></tr></table></div>';
        $result = $this->SCComment->drawComment($comment, $editParams);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawCommentPrivate() {
        $editParams = array(
            'url' => ''
        );

        $comment = array(
            'id' => 62,
            'date' => '24/01/2014 14:02:23',
            'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
            'comment' => 'aze editaze ',
            'private' => true,
            'editable' => false,
            'readers' => array(
                0 => array(
                    'id' => 2,
                    'name' => 'Auzolat Arnaud (Rôle Valideur Auzolat Arnaud)'
                ),
                1 => array(
                    'id' => 6,
                    'name' => 'Desmaretz Francois (Rôle Valideur Desmaretz Francois)'
                )
            )
        );

        $expected = '<div class="sc-comment sc-private"><table><tr><td class="sc-content">aze editaze </td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 14:02:23</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd><dt class="ui-state-focus">Destinataires</dt><dd class="sc-author"><ul><li>Auzolat Arnaud (Rôle Valideur Auzolat Arnaud)</li><li>Desmaretz Francois (Rôle Valideur Desmaretz Francois)</li></ul></dd></dl></td></tr></table></div>';

        $result = $this->SCComment->drawComment($comment, $editParams);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawCommentPrivateEditable() {
        $editParams = array(
            'url' => ''
        );

        $comment = array(
            'id' => 62,
            'date' => '24/01/2014 14:02:23',
            'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
            'comment' => 'aze editaze ',
            'private' => true,
            'editable' => true,
            'readers' => array(
                0 => array(
                    'id' => 2,
                    'name' => 'Auzolat Arnaud (Rôle Valideur Auzolat Arnaud)'
                ),
                1 => array(
                    'id' => 6,
                    'name' => 'Desmaretz Francois (Rôle Valideur Desmaretz Francois)'
                )
            )
        );

        $expected = '<div class="sc-comment sc-private sc-editable"><div class="sc-header"><img src="/img/delete.png" class="sc-delete-btn" title="Supprimer" alt="Supprimer" idComment="62" /><img src="/img/tool.png" class="sc-edit-btn" title="Modifier" alt="Modifier" idComment="62" /></div><table><tr><td class="sc-content">aze editaze </td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 14:02:23</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd><dt class="ui-state-focus">Destinataires</dt><dd class="sc-author"><ul><li>Auzolat Arnaud (Rôle Valideur Auzolat Arnaud)</li><li>Desmaretz Francois (Rôle Valideur Desmaretz Francois)</li></ul></dd></dl></td></tr></table></div>';

        $result = $this->SCComment->drawComment($comment, $editParams);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawCommentPrivateWithoutParams() {
        $editParams = null;
        $comment = array(
            'id' => 62,
            'date' => '24/01/2014 14:02:23',
            'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
            'comment' => 'aze editaze ',
            'private' => true,
            'editable' => false,
            'readers' => array(
                0 => array(
                    'id' => 2,
                    'name' => 'Auzolat Arnaud (Rôle Valideur Auzolat Arnaud)'
                ),
                1 => array(
                    'id' => 6,
                    'name' => 'Desmaretz Francois (Rôle Valideur Desmaretz Francois)'
                )
            )
        );

        $expected = '<div class="sc-comment sc-private"><table><tr><td class="sc-content">aze editaze </td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 14:02:23</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd><dt class="ui-state-focus">Destinataires</dt><dd class="sc-author"><ul><li>Auzolat Arnaud (Rôle Valideur Auzolat Arnaud)</li><li>Desmaretz Francois (Rôle Valideur Desmaretz Francois)</li></ul></dd></dl></td></tr></table></div>';
        $result = $this->SCComment->drawComment($comment, $editParams);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDraw() {
        $comments = array(
            0 => array(
                'id' => 64,
                'date' => '24/01/2014 16:04:16',
                'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
                'comment' => 'Bonjour ceci est un commentaire 2',
                'private' => false,
                'editable' => false,
                'readers' => array()
            ),
            1 => array(
                'id' => 49,
                'date' => '22/01/2014 16:04:40',
                'author' => 'Audrey ROY (Rôle Initiateur ROY Audrey)',
                'comment' => 'Bonjour ceci est un commentaire public',
                'private' => false,
                'editable' => false,
                'readers' => array()
            )
        );

        $options = array(
            'title' => 'Commentaires publics',
            'empty_message' => 'Aucun commentaire'
        );

        $expected = '<div class="col-sm-10"><legend class="legend">Commentaires publics</legend><div class="sc-comment"><table><tr><td class="sc-content">Bonjour ceci est un commentaire 2</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 16:04:16</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd></dl></td></tr></table></div><div class="sc-comment"><table><tr><td class="sc-content">Bonjour ceci est un commentaire public</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">22/01/2014 16:04:40</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Audrey ROY (Rôle Initiateur ROY Audrey)</dd></dl></td></tr></table></div></div>';
        $result = $this->SCComment->draw($comments, $options);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawWithPrivateComments() {
        $comments = array(
            0 => array(
                'id' => 62,
                'date' => '24/01/2014 14:02:23',
                'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
                'comment' => 'aze editaze ',
                'private' => true,
                'editable' => false,
                'readers' => array()
            ),
            1 => array(
                'id' => 50,
                'date' => '22/01/2014 16:04:58',
                'author' => 'Audrey ROY (Rôle Initiateur ROY Audrey)',
                'comment' => 'Bonsoir ceci est un commentaire privé',
                'private' => true,
                'editable' => false,
                'readers' => array()
            )
        );

        $options = array(
            'title' => '<img src="/img/comment_private_16.png" title="Commentaires privés" alt="Commentaires privés" /> Commentaires privés',
            'empty_message' => 'Aucun commentaire'
        );

        $expected = '<div class="col-sm-10"><legend class="legend"><img src="/img/comment_private_16.png" title="Commentaires privés" alt="Commentaires privés" /> Commentaires privés</legend><div class="sc-comment sc-private"><table><tr><td class="sc-content">aze editaze </td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 14:02:23</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd></dl></td></tr></table></div><div class="sc-comment sc-private"><table><tr><td class="sc-content">Bonsoir ceci est un commentaire privé</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">22/01/2014 16:04:58</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Audrey ROY (Rôle Initiateur ROY Audrey)</dd></dl></td></tr></table></div></div>';
        $result = $this->SCComment->draw($comments, $options);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawWithEmptyOptions() {
        $comments = array(
            0 => array(
                'id' => 62,
                'date' => '24/01/2014 14:02:23',
                'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
                'comment' => 'aze editaze ',
                'private' => true,
                'editable' => false,
                'readers' => array()
            ),
            1 => array(
                'id' => 50,
                'date' => '22/01/2014 16:04:58',
                'author' => 'Audrey ROY (Rôle Initiateur ROY Audrey)',
                'comment' => 'Bonsoir ceci est un commentaire privé',
                'private' => true,
                'editable' => false,
                'readers' => array()
            )
        );

        $options = array();

        $expected = '<div class="sc-list"><div class="sc-comment sc-private"><table><tr><td class="sc-content">aze editaze </td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">24/01/2014 14:02:23</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)</dd></dl></td></tr></table></div><div class="sc-comment sc-private"><table><tr><td class="sc-content">Bonsoir ceci est un commentaire privé</td><td class="sc-infos"><dl><dt class="ui-state-focus">Date de rédaction</dt><dd class="sc-date">22/01/2014 16:04:58</dd><dt class="ui-state-focus">Auteur</dt><dd class="sc-author">Audrey ROY (Rôle Initiateur ROY Audrey)</dd></dl></td></tr></table></div></div>';
        $result = $this->SCComment->draw($comments, $options);
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     * @expectedException BadMethodCallException
     */
    public function testDrawWithoutOption() {
        $comments = array(
            0 => array(
                'id' => 62,
                'date' => '24/01/2014 14:02:23',
                'author' => 'Arnaud Auzolat (Rôle Valideur Auzolat Arnaud)',
                'comment' => 'aze editaze ',
                'private' => true,
                'editable' => false,
                'readers' => array()
            ),
            1 => array(
                'id' => 50,
                'date' => '22/01/2014 16:04:58',
                'author' => 'Audrey ROY (Rôle Initiateur ROY Audrey)',
                'comment' => 'Bonsoir ceci est un commentaire privé',
                'private' => true,
                'editable' => false,
                'readers' => array()
            )
        );
        $this->SCComment->draw($comments, null);
    }

    /**
     *
     */
    public function testDrawWithEmptyComment() {
        $expected = '<div class="sc-list"><div class="sc-empty">No comment found.</div></div>';
        $result = $this->SCComment->draw(array(), array());
        $this->assertEqual($result, $expected, var_export($result, true));
    }

    /**
     *
     */
    public function testDrawWithoutComment() {
        $expected = '<div class="sc-list"><div class="sc-empty">No comment found.</div></div>';
        $result = $this->SCComment->draw(null, array());
        $this->assertEqual($result, $expected, var_export($result, true));
    }

}
