<?php

/**
 * SimpleComment Reader behavior class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * CakePHP version 2.0.x
 *
 * @author Stéphane Sampaio
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package			SimpleComment
 * @subpackage		SimpleComment.Model.Behavior
 */
App::uses('SCComment', 'SimpleComment.Model');
App::uses('SCReaderBehavior', 'SimpleComment.Model/Behavior');

/**
 * Classe SCReaderBehaviorTest.
 *
 * @package app.Test.Case.Model.Behavior
 */
class SCReaderBehaviorTest extends CakeTestCase {

    /**
     * Modèle Reader utilisé par ce test.
     *
     * @var Model
     */
    public $Reader = null;

    /**
     *
     * @var type
     */
    public $Comment = null;

    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = array(
        'plugin.SimpleComment.SCReader'
    );

    /**
     * PrÃ©paration du test.
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->Reader = ClassRegistry::init('Reader');
        $this->Reader->Behaviors->attach('SimpleComment.SCReader');
    }

    /**
     * Nettoyage postérieur au test.
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Reader);
        parent::tearDown();
    }

    /**
     *
     */
    public function testSetup() {

       $bindModelCommentParams = array(
            'hasAndBelongsToMany' => array(
                'Comment' => array(
                    'className' => 'SimpleComment.SCComment',
                    'joinTable' => 'comments_readers',
                    'foreignKey' => 'reader_id',
                    'associationForeignKey' => 'comment_id',
                    'unique' => 'keepExisting',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'finderQuery' => '',
                    'deleteQuery' => '',
                    'insertQuery' => '',
                    'with' => 'SimpleComment.SCCommentReader'
                )
            )
        );

        //!empty hasMany
        $checkHABTMReaderCommentKey = !empty($this->Reader->hasAndBelongsToMany);
        $this->assertEqual($checkHABTMReaderCommentKey, true, var_export($this->Reader, true));
        //equals
        $this->assertEqual($this->Reader->hasAndBelongsToMany['Comment'], $bindModelCommentParams['hasAndBelongsToMany']['Comment'], var_export($this->Reader->hasAndBelongsToMany['Comment'], true));




        //$bindModelReaderParams
        $bindModelReaderParams = array(
            'hasAndBelongsToMany' => array(
                'Reader' => array(
                    'className' => $this->Reader->name,
                    'joinTable' => 'comments_readers',
                    'foreignKey' => 'comment_id',
                    'associationForeignKey' => 'reader_id',
                    'unique' => 'keepExisting',
                    'conditions' => '',
                    'fields' => '',
                    'order' => '',
                    'limit' => '',
                    'offset' => '',
                    'finderQuery' => '',
                    'deleteQuery' => '',
                    'insertQuery' => '',
                    'with' => 'SimpleComment.SCCommentReader'
                )
            )
        );

        //!empty belongsTo
        $checkHABTMCommentReaderKey = !empty($this->Reader->Comment->hasAndBelongsToMany);
        $this->assertEqual($checkHABTMCommentReaderKey, true, var_export($this->Reader->Comment->hasAndBelongsToMany, true));

        //equals
        $this->assertEqual($this->Reader->Comment->hasAndBelongsToMany['Reader'], $bindModelReaderParams['hasAndBelongsToMany']['Reader'], var_export($this->Reader->Comment->hasAndBelongsToMany['Reader'], true));
    }

}

?>
