<?php

App::uses('SimpleCommentAppModel', 'SimpleComment.Model');
App::uses('SCTarget', 'SimpleComment/Model/Behavior');

/**
 * Carnet d'adresse
 *
 * SimpleComment Plugin
 * SimpleComment Comment model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package             SimpleComment
 * @subpackage          SimpleComment.Model
 *
 * @property Owner $Owner
 * @property Target $Target
 * @property Reader $Reader
 */
class SCComment extends SimpleCommentAppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'slug';

    /**
     *
     * @var type
     */
    public $recursive = -1;

    /**
     *
     * @var type
     */
    public $useTable = "comments";

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'owner_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'target_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );
    public $belongsTo = array(
        'Owner' => array(
            'className' => 'Desktop',
//            'className' => 'Desktopmanager',
            'foreignKey' => 'owner_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Target' => array(
            'className' => 'Courrier',
            'foreignKey' => 'target_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasAndBelongsToMany = array(
        'Reader' => array(
            'className' => 'SimpleComment.SCReader',
            'joinTable' => 'comments_readers',
            'foreignKey' => 'comment_id',
            'associationForeignKey' => 'reader_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'SimpleComment.SCCommentReader'
        )
    );

    /**
     *
     * @param type $options
     * @return type
     */
    public function beforeSave($options = array()) {
        $result = parent::beforeSave($options);

        if (!empty($this->data[$this->alias]['objet'])) {
            if (empty($this->data[$this->alias]['slug'])) {
                $this->data[$this->alias]['slug'] = Inflector::slug(substr($this->data[$this->alias]['objet'], 0, 255));
            }
        }
        return $result;
    }

    /**
     * Retourne le nombre de commentaires par courrier_id
     * @param type $courrier_id
     * @param type $ownerID = liste des profils auxquels l'uilisateur a accès
     */
    public function getNumberOfCommentByCourrierId($courrier_id, $ownerID) {
        $countComment = 0;
        $comments = $this->find(
                'all', array(
            'fields' => array(
                'Comment.id',
                'Comment.target_id',
                'Comment.owner_id',
                'Comment.private'
            ),
            'recursive' => -1,
            'conditions' => array(
                'Comment.target_id' => $courrier_id
            ),
            'contain' => false
                )
        );

        if (!empty($comments)) {
            // Pour chaque commentaires
            foreach ($comments as $i => $comment) {
                // si le commentaire est associé à un de mes profils, je le comptabilise
                if (in_array($comment['Comment']['owner_id'], $ownerID)) {
                    $countComment += count($comment);
                }
                // si le commentaire n'est pas associé à un de mes profils et qu'il n'est pas privé, alors je le comptabilise avec les précédents
                if (!in_array($comment['Comment']['owner_id'], $ownerID)) {
                    if ($comment['Comment']['private'] == false) {
                        $countComment += count($comment);
                    }
                }

                // Si le commentaire est privé
                if ($comment['Comment']['private'] == true) {
                    // onregarde la liste des personnes autorisées à le lire
                    $this->SCCommentReader = ClassRegistry::init('SimpleComment.SCCommentReader');
                    $readers = $this->SCCommentReader->find(
                            'all', array(
                        'conditions' => array(
                            'SCCommentReader.comment_id' => $comment['Comment']['id']
                        ),
                        'contain' => false
                            )
                    );
                    $ownerIds = Hash::extract($readers, '{n}.SCCommentReader.reader_id');
                    // pour chacun des lecteurs
                    foreach ($ownerIds as $key => $oId) {
                        // si l'utilsiateur connecté est un lecteur autorisé, alors on comptabilise
                        if (in_array($oId, $ownerID)) {
                            $countComment += 1;
                        }
                    }
                }
            }
        }
        return $countComment;
    }

	/**
	 * Fonction pêrmettant d emarquer les messages de lecture de comentaire comme lu
	 * Commentaire public uniquement
	 * @param $fluxId
	 * @param $desktopId
	 */
	public function setRead($fluxId, $desktopId) {
		$this->updateAll(
			array(
				'Comment.read' => 'true'
			),
			array(
				'Comment.target_id' => $fluxId,
				'Comment.private' => false
			)
		);
	}

}
