<?php

/**
 * SimpleComment Plugin
 * SimpleComment SCComment helper class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package             SimpleComment
 * @subpackage          SimpleComment.View.Helper
 */
class SCCommentHelper extends Helper {

    /**
     *
     * @var type
     */
    public $helpers = array('Html');

    /**
     *
     * @param type $comment
     * @param type $author
     * @return type
     * @throws BadMethodCallException
     */
    protected function _drawComment($comment, $editParams = array()) {
        if (empty($comment)) {
            throw new BadMethodCallException();
        }
		//panel-body
		$comment['comment'] = str_replace("\r", "<br>", $comment['comment'] );
		$comment['comment'] = str_replace("\n", "<br>", $comment['comment'] );
        $panel_body = $this->Html->div('panel-body', $comment['comment'], array('style' => "padding-left:15px;"));
        //panel-heading
        $panel_heading = '';
        $readerList = '';
        if (!empty($comment['readers'])) {
            $readerList = '';
            foreach ($comment['readers'] as $reader) {
                $readerList .= $this->Html->tag('li', 'À: '.$reader['name']) . ' ';
            }
        }
        $functionSpan = '';
        if (!empty($comment['editable'])) {

            $editImg = $this->Html->tag('a', '<i class="fa fa-pencil"  aria-hidden="true"></i>', array('class' => 'sc-edit-btn btn btn-info-webgfc btn-secondary', 'title' => __d('default', 'Button.edit'), 'alt' => __d('default', 'Button.edit'), 'idComment' => $comment['id']));
            $deleteImg = $this->Html->tag('a', '<i class="fa fa-trash"  aria-hidden="true"></i>', array('class' => 'sc-delete-btn btn btn-danger btn-secondary', 'title' => __d('default', 'Button.delete'), 'alt' => __d('default', 'Button.delete'), 'idComment' => $comment['id']));
            $functionSpan .= $this->Html->tag('div', $editImg . ' ' . $deleteImg, array('id' => 'commentActionsBtn', 'class' => 'btn-group', 'role' => 'group'));
        }

        $createur = $this->Html->tag('span', 'De : ' . $comment['author']);
        $date = $this->Html->tag('span', 'Le : ' . $comment['date']);
        $panel_heading = $this->Html->div('panel-heading', $createur . '<br>' . $date);


        if (strlen($readerList) != 0 || strlen($functionSpan) != 0) {
            $panel_footer = $this->Html->div('panel-footer', $functionSpan . '<br/>' . $readerList);
        } else {
            $title = __d('comment', 'Comment.' . (!empty($comment['private']) ? 'privateComment' : 'publicComment'));
            //$panel_footer = $this->Html->div('panel-footer', $title);
            $panel_footer = "";
        }
        //panel-footer



        $panel_default = $this->Html->div('panel panel-default', $panel_heading . $panel_body . $panel_footer);
        $panel_comment = $this->Html->div('panel col-sm-12 commentPanel', $panel_default, array('style' => 'margin-left:-15px;margin-top:-10px'));
        return $panel_comment;
    }

    /**
     *
     *
     * @param type $comments
     * @param type $options
     * @return type
     * @throws BadMethodCallException
     */
    public function draw($comments, $options) {
        if (is_null($options)) {
            throw new BadMethodCallException();
        }

        if (!is_array($options)) {
            throw new BadMethodCallException();
        }

        if (empty($options)) {
            $options = array();
        }

//        debug($comments);
        $params = array_merge(array('empty_message' => 'No comment found.', 'edit' => array('url' => '')), $options);
//        debug($params);
        $content = !empty($params['title']) ? $this->Html->tag('legend', $params['title'], array('class' => 'legend')) : '';
        if (empty($comments)) {
            $content .= $this->Html->tag('div', $params['empty_message'], array('class' => 'alert alert-warning'));
        } else {
            foreach ($comments as $comment) {
				if( !empty($comment['id'])) {
					$comment = str_replace( ["\r", "\n"], '', $comment );
					$content .= $this->_drawComment($comment, $params['edit']);
				}
            }
        }

        $tagAttrs = array('class' => ' col-sm-12');
        if (!empty($params['html'])) {
            foreach ($params['html'] as $key => $val) {
                if ($key == 'class') {
                    $tagAttrs['class'] .= ' ' . $val;
                } else {
                    $tagAttrs['class'] = $val;
                }
            }
        }
        return $this->Html->tag('div', $content, $tagAttrs);
    }

}
