<?php

/**
 *
 * Addressbook Plugin
 * Addressbook shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		Addressbook
 * @subpackage		Addressbook.Console.Command
 */
class AddressbookShell extends Shell {

	/**
	 * Shell uses
	 *
	 * @access public
	 * @var array
	 */
	public $uses = array('Addressbook.Abaddressbook');

//	/**
//	 *
//	 */
//	public function create_addressbook() {
//		$ab = array('Abaddressbook' => array());
//
//		$this->out('New addressbook : ');
//		$ab['Abaddressbook']['name'] = $this->in('Name : ');
//		$ab['Abaddressbook']['description'] = $this->in('Description : ');
//		$ab['Abaddressbook']['foreign_key'] = $this->in('Foreign key : ');
//
//		$this->Abaddressbook->create($ab);
//		$this->Abaddressbook->save();
//	}
//
//	/**
//	 *
//	 */
//	public function import_contact_vcards() {
//		$filename = $this->args[0];
//
//		$abList = $this->Abaddressbook->find('list');
//		if (!empty($abList)) {
//			$this->out('Addressbook list : ');
//			foreach ($abList as $kab => $ab) {
//				$this->out($kab . '. -> ' . $ab);
//			}
//			$this->Abaddressbook->importContactVcards($filename, $this->in('Pick an addressbook in the above list : '));
//		} else {
//			$this->out('Please choose an addressbook');
//		}
//	}
//
//	/**
//	 *
//	 */
//	public function import_secondary_vcards() {
//		$filename = "";
//		if (!empty($this->args[0])) {
//			$filename = $this->args[0];
//		}
//
//		$abList =  $this->Abaddressbook->find('list');
//		$abId = $this->in('Pick an addressbook in the above list : ');
//
//		$search = $this->in('Please enter part or full name to search : ');
//		$contactList = $this->Abaddressbook->Abcontact->find('list', array('conditions' => array('Abcontact.name ILIKE %' . $search . '%', 'Abcontact.addressbook_id' => $abId)));
//
//
//		if (!empty($contactList)) {
//			$this->out('Contact list : ');
//			foreach ($abList as $kab => $ab) {
//				$this->out($kab . '. -> ' . $ab);
//			}
//			$this->Abaddressbook->importSecondaryVcards($filename, $this->in('Pick a contact in the above list : '));
//		} else {
//			$this->out('Please choose a contact.');
//		}
//	}

	/**
	 * Définition des options
	 *
	 * @access public
	 * @return ConsoleOptionParser
	 */
	public function getOptionParser() {
		return parent::getOptionParser()
						->description(__("Addressbook shell"))
		/* ->addSubcommand('create_addressbook', array(
		  'help' => __('Creates an addressbook.')
		  ))
		  ->addSubcommand('import_contact_vcards', array(
		  'help' => __('Import contact vcards (primary).')
		  ))
		  ->addSubcommand('import_secondary_vcards', array(
		  'help' => __('Import secondary vcards.')
		  )) */;
	}

}
