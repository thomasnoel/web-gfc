<?php

App::import('Vendor', 'Addressbook.VCard', array('file' => 'vcardphp-1.1.2' . DS . 'vcard.php'));

/**
 * Addressbook Plugin
 * Addressbook App model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		Addressbook
 * @subpackage		Addressbook.Model
 */
class AddressbookAppModel extends AppModel {

}

