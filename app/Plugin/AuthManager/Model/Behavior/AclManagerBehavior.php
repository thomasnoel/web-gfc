<?php
/**
 * AuthManager : Plugin Authenticate User CakePhp
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/scm/viewvc.php/AuthManager/?root=plugins-cakephp AuthManager Project
 * @since       AuthManager v 0.9.0
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */

App::uses('AclBehavior', 'Model/Behavior');

/**
 * ACL behavior
 *
 * Enables objects to easily tie into an ACL system
 *
 * @package AuthManager.Model.Behavior
 */

class AclManagerBehavior extends AclBehavior {


    /**
     * Sets up the configuration for the model, and loads ACL models if they haven't been already
     *
     * @param Model $model Model using this behavior.
     * @param array $config Configuration options.
     * @return void
     */
    public function setup(Model $model, $config = array()) {

        parent::setup($model, $config);

        if (!method_exists($model, 'parentNodeAlias')) {
                trigger_error(__d('cake_dev', 'Callback %s not defined in %s', 'parentNodeAlias()', $model->alias), E_USER_WARNING);
        }
    }
    /**
     * Creates a new ARO/ACO node bound to this record
     *
     * @param Model $model Model using this behavior.
     * @param bool $created True if this is a new record
     * @param array $options Options passed from Model::save().
     * @return void
     */
    public function afterSave(Model $model, $created, $options = array()) {
            $types = $this->_typeMaps[$this->settings[$model->name]['type']];
            if (!is_array($types)) {
                    $types = array($types);
            }
            foreach ($types as $type) {
                    $parent = $model->parentNode();
                    $alias = $model->parentNodeAlias();
                    if (!empty($parent)) {
                            $parent = $this->node($model, $parent, $type);
                    }
                    $data = array(
                            'parent_id' => isset($parent[0][$type]['id']) ? $parent[0][$type]['id'] : null,
                            'model' => $model->name,
                            'alias' => isset($alias[$model->name]['alias']) ? $alias[$model->name]['alias'] : null,
                            'foreign_key' => $model->id
                    );
                    if (!$created) {
                            $node = $this->node($model, null, $type);
                            $data['id'] = isset($node[0][$type]['id']) ? $node[0][$type]['id'] : null;
                    }
                    $model->{$type}->create();
                    $model->{$type}->save($data);
            }
    }
}
