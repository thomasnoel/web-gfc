<?php

App::uses('AclSync', 'AuthManager.Lib');

/**
 * Shell for ACO extras
 *
 * @package     AuthManager
 * @subpackage	AuthManager.Console.Command
 */
class AclSyncShell extends Shell {

/**
 * Contains arguments parsed from the command line.
 *
 * @var array
 * @access public
 */
	public $args;

/**
 * AclSync instance
 */
	public $AclSync;

/**
 * Constructor
 */
	public function __construct($stdout = null, $stderr = null, $stdin = null) {
		parent::__construct($stdout, $stderr, $stdin);
		$this->AclSync = new AclSync();
	}

/**
 * Start up And load Acl Component / Aco model
 *
 * @return void
 **/
	public function startup() {
		parent::startup();
		$this->AclSync->startup();
		$this->AclSync->Shell = $this;
	}

/**
 * Sync the ACO table
 *
 * @return void
 **/
	public function aco_sync() {
		$this->AclSync->aco_sync($this->params);
	}

/**
 * Updates the Aco Tree with new controller actions.
 *
 * @return void
 **/
	public function aco_update() {
		$this->AclSync->aco_update($this->params);
		return true;
	}

	public function getOptionParser() {
		$plugin = array(
			'short' => 'p',
			'help' => __('Plugin to process'),
			);
		return parent::getOptionParser()
			->description(__("Better manage, and easily synchronize you application's ACO tree"))
			->addSubcommand('aco_update', array(
				'parser' => array(
					'options' => compact('plugin'),
					),
				'help' => __('Add new ACOs for new controllers and actions. Does not remove nodes from the ACO table.')
			))->addSubcommand('aco_sync', array(
				'parser' => array(
					'options' => compact('plugin'),
					),
				'help' => __('Perform a full sync on the ACO table.' .
					'Will create new ACOs or missing controllers and actions.' .
					'Will also remove orphaned entries that no longer have a matching controller/action')
			))->addSubcommand('verify', array(
				'help' => __('Verify the tree structure of either your Aco or Aro Trees'),
				'parser' => array(
					'arguments' => array(
						'type' => array(
							'required' => true,
							'help' => __('The type of tree to verify'),
							'choices' => array('aco', 'aro')
						)
					)
				)
			))->addSubcommand('recover', array(
				'help' => __('Recover a corrupted Tree'),
				'parser' => array(
					'arguments' => array(
						'type' => array(
							'required' => true,
							'help' => __('The type of tree to recover'),
							'choices' => array('aco', 'aro')
						)
					)
				)
			));
	}

/**
 * Verify a Acl Tree
 *
 * @param string $type The type of Acl Node to verify
 * @access public
 * @return void
 */
	public function verify() {
		$this->AclSync->args = $this->args;
		return $this->AclSync->verify();
	}
/**
 * Recover an Acl Tree
 *
 * @param string $type The Type of Acl Node to recover
 * @access public
 * @return void
 */
	public function recover() {
		$this->AclSync->args = $this->args;
		$this->AclSync->recover();
	}
}
