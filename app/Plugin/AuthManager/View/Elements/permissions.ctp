<?php
if(!empty(${'acos_'.$model})){
echo $this->Html->script('AuthManager.main.js'); 
echo $this->Html->css('/components/bootstrap-table/dist/bootstrap-table.min') .
$this->Html->script('/components/bootstrap-table/dist/bootstrap-table.min');
$this->BsForm->setLeft(0);
$this->BsForm->setRight(12);

$titles=array(  array('title' => ''), 
                array('title' => __d('auth_manager', 'permissions')));
foreach (${'permKeys_'.$model} as $key => $value) {
    $titles[]=array('title' =>__d('auth_manager', $value),
                    'class'=> 'text-center'
        );
}
$tableDroits= $this->Bs->table($titles, array('hover', 'striped'), array('class' => array('tableAco'.$model),'data-toggle'=>'table'));
$configCRUD=Configure::read('AuthManager.configCRUD');

$groups_cruds=array();
foreach ($configCRUD['CRUD'] as $key => $value) {
    if (!empty($value['group']) && $value['group'] === true) {
            $groups_cruds[] = $key;
        }
}


$fullAllChecked=true;
if(!empty(${'acos_'.$model})){
    foreach (${'acos_'.$model} as $key_aco=>$aco){
        if(!empty(${'acos_' . $model}[$key_aco + 1]['Aco']['parent_id'] ) && 
            ${'acos_' . $model}[$key_aco + 1]['Aco']['parent_id'] == $aco['Aco']['id'] && 
                    $aco['Aco']['id'] != 1) {
            $aco_select = $this->Bs->icon('arrow-circle-down');
        } 
        else
        {
            $aco_select = $this->Bs->icon('arrow-circle-right');
        }

        $label='';
        if ($aco['Aco']['parent_id']===1 || empty($aco['Aco']['parent_id'])) {//$aco['Aco']['parent_id'] != $aco_encour || 
                 $label=$this->Bs->tag('b',__d('auth_manager', $aco['Action']));
        }
        else {
            $aco_select = $this->Bs->icon('arrow-circle-right',array(), array('style'=>'padding-left: '.substr_count($aco['Action'], '/') .'em'));
            $label=__d('auth_manager', $aco['Action']);
        }
        $this->Bs->lineAttributes(array('data-model'=>$model, 'data-aco_id'=>$aco['Aco']['id'], 'data-parent_id'=>$aco['Aco']['parent_id']));
        
        $tableDroits.= $this->Bs->cell($aco_select);

        
       // $tableDroits.= $this->Bs->cell($aco['Aco']['id']);

        $allChecked=true;
        foreach (${'permKeys_'.$model} as $key => $value) {
           // debug($aco['Aro']);
            if ($allChecked && (
                empty($aco['Aro'][0]['Permission'][$value])
                XOR (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] !== '1')
                )) {
                $allChecked = false ;
            }
        }
        if (!$allChecked) {
                $fullAllChecked = false;
            }
            $checkbox = $this->BsForm->checkbox('Aco.'.$model.'.'. $aco['Aco']['id'], 
                    array(  'checked' => $allChecked,
                            'class'=> 'childCheckboxAuthManager',
                            'autocompete'=>false,
                            'data-model' => $model,
                            'data-aco_id' => $aco['Aco']['id'],
                            'title'=>__d('auth_manager', 'Donner tous les droits à la ligne'),
                            //'inline'=>'inline',
                            'label' => $label)
        );
        $checkbox.=$this->BsForm->hidden('Aco.' .$model.'.'. $aco['Aco']['id'].'.alias', array('value' => $aco['Action']));
        $checkbox.=$this->BsForm->hidden('Aco.' .$model.'.'. $aco['Aco']['id'].'.foreign_key', array('value' => $aco['Aco']['foreign_key']));
        $tableDroits.= $this->Bs->cell($checkbox);
        
//        if (!in_array(, $controllers_list)) {
//            foreach (array_diff(${'permKeys_' . $model}, array('_update','_create','_delete')) as $key => $value) {
//                $checkbox = $this->BsForm->checkbox('Aco.' . $model . '.' . $aco['Aco']['id'] . '.permKeys.' . $value, array(
//                    'checked' => (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] === '1') ? true : false,
//                    'inline' => 'inline',
//                    'autocompete' => false,
//                    'title' => __d('auth_manager', $value),
//                    'label' => false)
//                );
//
//                $tableDroits.= $this->Bs->cell($checkbox, 'text-center', array('colspan' => 4));
//                }
//                $this->Bs->linePosition(0);
//            } else {
                $diff=array();
                $intersect=array();
                $groupAll=false;
                
                if(in_array($aco['Aco']['alias'], $groups_cruds))
                {
                    $groupAll=true;
                    $diff = array_diff(${'permKeys_' . $model}, array('_update','_create','_delete'));
                    $intersect = array_intersect(${'permKeys_' . $model}, array('_update','_create','_delete'));
                }
                        
                foreach (${'permKeys_' . $model} as $key => $value) {
                    $checkbox = $this->BsForm->checkbox('Aco.' . $model . '.' . $aco['Aco']['id'] . '.permKeys.' . $value, array(
                        'checked' => (!empty($aco['Aro'][0]['Permission'][$value]) && $aco['Aro'][0]['Permission'][$value] === '1') ? true : false,
                        'inline' => 'inline',
                        'style' => (in_array($value, $intersect)?'display:none':''),
                        'autocompete' => false,
                        "data-model" => $model,
                        "data-aco_id" => $aco['Aco']['id'],
                        'class'=> (($value=='_read'&& $groupAll)?'AuthManage_groupAllCRUDCheckbox':''),
                        'title' => __d('auth_manager', $value),
                        'label' => false)
                    );

                    $tableDroits.= $this->Bs->cell($checkbox, 'text-center '.($value=='_manager'?'danger':null));
                }
       //     }
        }
}
else
{
    $tableDroits.= $this->Bs->cell(
           $this->Bs->tag('p', $this->Bs->icon('remove')
                   .' '.__('Aucun droit disponible')), 'text-center', array('colspan'=> count(${'permKeys_'.$model})));
}

$tableDroits.= $this->Bs->endTable();

echo $this->Bs->tag('h4', 
$this->BsForm->checkbox('Aco.'.$model, 
                array(  'checked' => $fullAllChecked,
                        'class'=> 'masterCheckboxAuthManager',
                        'data-model'=>$model,
                        //'inline'=>'inline',
                        'title'=>__d('auth_manager', 'Donner tous les droits au tableau'),
                        'autocompete' => false,
                        'label' => $this->Bs->tag('span',__d('auth_manager','%s permissions', __d('auth_manager', ${'acoAlias_'.$model})), array('class'=>'label label-primary'))
)));
                        
echo $tableDroits;                        

//echo $this->Html->scriptBlock('
//    $(document).ready(function(){
//       if ($(".parapheur_error").length > 0){
//           $("#attribuer").remove();
//       }
//    });
//');
}