<?php
	/**
	 * Code source de la classe TemplatemailFixture.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Fixture
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */

	/**
	 * Classe TemplatemailFixture.
	 *
	 * @package app.Test.Fixture
	 */
	class TemplatemailFixture
	{
		/**
		 * On importe la définition de la table, pas les enregistrements.
		 *
		 * @var array
		 */
		public $import = array(
			'model' => 'Templatemail',
			'records' => false
		);

		/**
		 * Définition des enregistrements.
		 *
		 * @var array
		 */
        public $records = array(
            array(
                'Templatemail' => array(
                    'id' => 1,
                    'name' => 'Réponse à une demande',
                    'conn' => 'Courrier réponse à votre demandetest',
                    'active' => true,
					'created' => 'now()',
					'modified' => 'now()'
                )
            )
        );
	}
?>

