<?php

/**
 * SCCommentBehavior test class.
 *
 * i-delibRE : le porte-document nomade des élus pour le suivi des séances délibérantes (https://adullact.net/projects/idelibre)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 * @encoding UTF-8
 *
 * SVN Informations
 * $Date: 2013-10-21 17:57:34 +0200 (lun., 21 oct. 2013) $
 * $Revision: 302 $
 * $Author: ssampaio $
 * $HeadURL: svn+ssh://ssampaio@scm.adullact.net/scmrepos/svn/idelibre/trunk/app/Model/Behavior/PasswordBehavior.php $
 * $Id: PasswordBehavior.php 302 2013-10-21 15:57:34Z ssampaio $
 *
 *
 * @package			SimpleComment
 * @package       SimpleComment.Test.Fixture
 */
class SCCommentFixture extends CakeTestFixture {

	public $import = ['model' => 'SimpleComment.SCComment', 'connection' => 'test',  'records' => false];

	/**
	 * name property
	 *
	 * @var string 'SCComment'
	 */
	public $name = 'SCComment';

	/**
	 * table property
	 *
	 * @var string
	 */
	public $table = 'comments';

	/**
	 * fields property
	 *
	 * @var array
	 */
	public $fields = array(
		'id' => array('type' => 'integer', 'key' => 'primary'),
		'slug' => array('type' => 'string', 'length' => 255),
		'content' => array('type' => 'binary'),
		'private' => array('type' => 'boolean', 'null' => false, 'default' => false),
		'owner_id' => array('type' => 'integer', 'null' => true),
		'target_id' => array('type' => 'integer', 'null' => true),
		'created' => 'datetime',
		'modified' => 'datetime'
	);

	/**
	 * records property
	 *
	 * @var array
	 */
	public $records = array(
//        array('profil_id' => 2, 'name' => 'Init SCComment 1', 'created' => '2006-11-22 10:38:58', 'modified' => '2006-12-01 13:31:26'),
	);
}
