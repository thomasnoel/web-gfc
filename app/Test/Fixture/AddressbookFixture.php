<?php
	/**
	 * Code source de la classe AddressbookFixture.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Fixture
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */

	/**
	 * Classe AddressbookFixture.
	 *
	 * @package app.Test.Fixture
	 */
	class AddressbookFixture
	{
		/**
		 * On importe la définition de la table, pas les enregistrements.
		 *
		 * @var array
		 */
		public $import = array(
			'model' => 'Addressbook',
			'records' => false
		);

		/**
		 * Définition des enregistrements.
		 *
		 * @var array
		 */
        public $records = array(
            array(
                'id' => 1,
                'name' => 'Carnet principal',
                'description' => 'Carnet adresse principal',
                'private' => false,
                'active' => true
            ),
            array(
                'id' => 2,
                'name' => 'Elus',
                'description' => 'Carnet des élus',
                'private' => true,
                'active' => true
            )
        );
	}
?>
