<?php
/**
 * Code source de la classe RechercheTacheFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe RechercheTacheFixture.
 *
 * @package app.Test.Fixture
 */
class RechercheTacheFixture extends CakeTestFixture {

	public $useDbConfig = 'test';
	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'RechercheTache', 'records' => false, 'connection' => 'test');

}
?>
