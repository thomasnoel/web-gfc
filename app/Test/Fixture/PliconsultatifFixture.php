<?php
/**
 * Code source de la classe PliconsultatifFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe PliconsultatifFixture.
 *
 * @package app.Test.Fixture
 */
	class PliconsultatifFixture extends CakeTestFixture {

	public $useDbConfig = 'test';
	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'Pliconsultatif', 'records' => false, 'connection' => 'test');

}
?>
