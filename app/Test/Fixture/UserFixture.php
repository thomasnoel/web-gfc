<?php
/**
 * User Fixture
 */
class UserFixture extends CakeTestFixture {

	/**
	 * Import
	 *
	 * @var array
	 */
	public $import = array('model' => 'User');

	/**
	 * Records
	 *
	 * @var array
	 */
	public $records = array(
		array(
			'id' => 1,
			'username' => 'aauzolat',
			'nom' => 'AUZOLAT',
			'prenom' => 'Arnaud',
			'password' => 'user',
			'active' => true,
			'created' => '2020-06-10 00:29:10',
			'modified' => '2020-06-10 00:29:10',
			'pwdmodified' => false,
			'desktop_idk' => 1
		)
	);

}
