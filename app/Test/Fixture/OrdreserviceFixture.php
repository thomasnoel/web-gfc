<?php
/**
 * Code source de la classe OrdreserviceFixture.
 *
 * PHP 5.3
 *
 * @package app.Test.Fixture
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */

/**
 * Classe OrdreserviceFixture.
 *
 * @package app.Test.Fixture
 */
class OrdreserviceFixture extends CakeTestFixture {

	/**
	 * On importe la définition de la table, pas les enregistrements.
	 *
	 * @var array
	 */
	public $import = array('model' => 'Ordreservice', 'records' => false, 'connection' => 'test');
}
?>
