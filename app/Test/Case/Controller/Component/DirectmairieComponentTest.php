<?php

/**
 * DirectmairieComponentTest file
 *
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @since       web-delib v4.3
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('DirectmairieComponent', 'Controller/Component');

/**
 * Classe DirectmairieComponentTest.
 *
 * @version 3
 * @package app.Test.Case.Controller
 */
class DirectmairieComponentTest extends CakeTestCase
{

	public $DirectmairieComponent = null;
	public $Controller = null;

	public function setUp()
	{
		parent::setUp();
		// Configurer notre component et faire semblant de tester le controller
		$Collection = new ComponentCollection();
		$this->DirectmairieComponent = new DirectmairieComponent($Collection);
		$CakeRequest = new CakeRequest();
		$CakeResponse = new CakeResponse();
		$this->Controller = new TestDirectmairieController($CakeRequest, $CakeResponse);
		$this->DirectmairieComponent->startup($this->Controller);

		ClassRegistry::init(('Connecteur'));
		$this->Connecteur = new Connecteur();
		$this->Connecteur->setDataSource('test');
		$this->connecteur = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array('Connecteur.name ILIKE' => '%Direct-Mairie%'),
				'contain' => false
			)
		);
	}

	/**
	 * Méthode exécutée avant chaque test.
	 *
	 * @return void
	 */
	public function tearDown()
	{
		parent::tearDown();
		unset($this->DirectmairieComponent);
		unset($this->Controller);
	}

	/**
	 * Test EchoTest()
	 *
	 * @version 4.3
	 * @return void
	 */
	public function testEchoTest()
	{
		return ($this->DirectmairieComponent->echoTest());
	}

}

// Un faux controller pour tester against
class TestDirectmairieController extends Controller
{

	public $paginate = null;

}
