<?php

/**
 * PastellComponentTest file
 *
 * web-delib : Application de gestion des actes administratifs
 * Copyright (c) Adullact (http://www.adullact.org)
 *
 * Licensed under The AGPL v3 License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright   Copyright (c) Adullact (http://www.adullact.org)
 * @link        https://adullact.net/projects/webdelib web-delib Project
 * @since       web-delib v4.3
 * @license     https://choosealicense.com/licenses/agpl-3.0/ AGPL v3 License
 */
App::uses('Controller', 'Controller');
App::uses('CakeRequest', 'Network');
App::uses('CakeResponse', 'Network');
App::uses('ComponentCollection', 'Controller');
App::uses('PastellComponent', 'Controller/Component');

/**
 * Classe PastellComponentTest.
 *
 * @version 3
 * @package app.Test.Case.Controller
 */
class PastellComponentTest extends CakeTestCase
{

	public $PastellComponent = null;
	public $Controller = null;

	public function setUp()
	{
		parent::setUp();
		// Configurer notre component et faire semblant de tester le controller
		$Collection = new ComponentCollection();
		$this->PastellComponent = new PastellComponent($Collection);
		$CakeRequest = new CakeRequest();
		$CakeResponse = new CakeResponse();
		$this->Controller = new TestPastellController($CakeRequest, $CakeResponse);
		$this->PastellComponent->startup($this->Controller);

	}

	/**
	 * Méthode exécutée avant chaque test.
	 *
	 * @version 4.3
	 * @return void
	 */
	public function tearDown()
	{
		parent::tearDown();
		unset($this->PastellComponent);
		unset($this->Controller);
	}

	/**
	 * Test GetVersion()
	 *
	 * @version 4.3
	 * @return void
	 */
	public function testGetVersion()
	{
//		return ($this->PastellComponent->getVersion());
		return true;
	}

	/**
	 * Test GetDocumentTypes()
	 *
	 * @version 4.3
	 * @return void
	 */
//	public function testGetDocumentTypes()
//	{
//		return ($this->PastellComponent->getDocumentTypes());
//	}
//
//	/**
//	 * Test GetDocumentTypeInfos()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
//	public function testGetDocumentTypeInfos()
//	{
//		return ($this->PastellComponent->getDocumentTypeInfos('mailsec'));
//	}
//
//	/**
//	 * Test GetDocumentTypeActions()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
//	public function testGetDocumentTypeActions()
//	{
//		return ($this->PastellComponent->getDocumentTypeActions('mailsec'));
//	}
//
//	/**
//	 * Test ListEntities()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
//	public function testListEntities()
//	{
//		return ($this->PastellComponent->listEntities(true));
//	}
//
//	/**
//	 * Test ListDocuments()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
////	public function testListDocuments()
////	{
////		return ($this->PastellComponent->listDocuments(185, 'mailsec'));
////	}
//
//	/**
//	 * Test RechercheDocument()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
////	public function testRechercheDocument()
////	{
////		$options = array(
////			'id_e' => 185,
////			'type' => 'mailsec',
////			'search' => 'test sans sources'
////		);
////		$this->PastellComponent->rechercheDocument($options);
////	}
//
//	/**
//	 * Test GetInfosField()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
//	public function testGetInfosField()
//	{
//		$this->PastellComponent->getInfosField(185, 'JJsUb6e', 'objet');
//	}
//
//	/**
//	 * Test DetailDocument()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
//	public function testDetailDocument()
//	{
//		$this->PastellComponent->detailDocument(185, 'JJsUb6e');
//	}
//
//	/**
//	 * FIXME
//	 * Test DetailDocuments()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
////	public function testDetailDocuments()
////	{
////		$this->PastellComponent->detailDocuments(185, array('JJsUb6e'));
////	}
//
//	/**
//	 * OK en v1.3
//	 * Test CreateDocument()
//	 *
//	 * @version 4.3
//	 * @return void
//	 */
//	public function testCreateDocument()
//	{
//		$this->PastellComponent->createDocument(185, 'mailsec');;
//	}

	/**
	 * FIXME
	 * Test GetInfosField()
	 *
	 * @return void
	 */
	/* public function testGetInfosField(){
	  //debug ($this->PastellComponent->getInfosField(3,'L4iaPx6', null));
	  $this->PastellComponent->getInfosField(48,'mailsec');
	  } */
}

// Un faux controller pour tester against
class TestPastellController extends Controller
{

	public $paginate = null;

}
