<?php

class AppModelTest extends CakeTestCase {

    public $Apple = null;

    /**
     * Fixtures.
     *
     * @var array
     */
    public $fixtures = array(
        'core.Apple'
    );

    /**
     * Set up the test
     *
     * @return void
     */
    public function setUp() {
        parent::setUp();
        $this->Apple = ClassRegistry::init('Apple');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown() {
        unset($this->Apple);
        parent::tearDown();
    }

    /**
     * Test de la méthode AppModel::wildcard()
     *
     * public function wildcard($value) {
		return str_replace('*', '%', Sanitize::escape($value));
	}
     * @return void
     */
    public function testWildcard() {
        $result = $this->Apple->wildcard( '*Coucou*' );
        $expected = '%Coucou%';
        $this->assertEqual( $result, $expected, var_export( $result, true ) );
    }

}

?>
