<?php
/**
 * Code source de la classe SmsTest.
 *
 * PHP 5.3
 *
 * @package app.Test.Case.Model
 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
 */
App::uses( 'Sms', 'Model' );

/**
 * La classe SmsTest réalise les tests unitaires de la classe Sms.
 *
 * @package app.Test.Case.Model
 */
class SmsTest extends CakeTestCase
{

	public $fixtures = array('app.sms');


	/**
	 * Le modèle à tester.
	 *
	 * @var Sms
	 */
	public $Sms = null;

	/**
	 * Préparation du test.
	 */
	public function setUp() {
		parent::setUp();
		$this->Sms = ClassRegistry::init('Sms');
		$this->Sms->setDataSource('test');
		$this->Sms->recursive = -1;
	}

	/**
	 * Nettoyage postérieur au test.
	 */
	public function tearDown() {
		unset( $this->Sms );
		parent::tearDown();
	}

	/**
	 * Test de la méthode Sms::add()
	 */
	public function testSendSms() {
		$this->Sms = ClassRegistry::init('Sms');
		$this->Sms->setDataSource('test');
		$this->Sms->recursive = -1;

		$data = array(
			'Sms' => array(
				'numero' => '0666666666',
				'message' => 'Test envoi SMS',
				'courrier_id' => 1,
				'created' => 'now()',
				'modified' => 'now()'
			)
		);
		// la création se fait-elle ?
		$this->Sms->create( $data );
		$this->assertTrue( $this->Sms->beforeSave() );

		$result = $this->Sms->data;

		// la sauvegarde se fait-elle ?
		$success = $this->Sms->save( $data );
		$this->assertTrue( !empty( $success ) );

		$expected = array(
			'Sms' => array(
				'numero' => '0666666666',
				'message' => 'Test envoi SMS',
				'courrier_id' => 1,
				'created' => 'now()',
				'modified' => 'now()'
			),
		);
		// le résultat correspond à ce qui est attendu ?
		$this->assertEqual( $result, $expected, var_export( $result, true ) );

		$expected = array(
			'Sms' => array(
				'numero' => '0505050505',
				'message' => 'Test envoi SMS',
				'courrier_id' => 1,
				'created' => 'now()',
				'modified' => 'now()'
			),
		);
		// le résultat ne correspond pas à ce qui est attendu ?
		$this->assertNotEquals( $result, $expected, var_export( $result, true ) );


	}

}
?>
