<?php
	/**
	 * Code source de la classe ActiviteTest.
	 *
	 * PHP 5.3
	 *
	 * @package app.Test.Case.Model
	 * @license AGPL v3  (https://choosealicense.com/licenses/agpl-3.0/)
	 */
	App::uses( 'Activite', 'Model' );

	/**
	 * La classe ActiviteTest réalise les tests unitaires de la classe Activite.
	 *
	 * @package app.Test.Case.Model
	 */
	class ActiviteTest extends CakeTestCase
	{

		/**
		 * Le modèle à tester.
		 *
		 * @var Activite
		 */
		public $Activite = null;

		/**
		 * Préparation du test.
		 */
		public function setUp() {
			parent::setUp();
            $this->Activite = ClassRegistry::init('Activite');
            $this->Activite->setDataSource('test');
            $this->Activite->recursive = -1;
		}

		/**
		 * Nettoyage postérieur au test.
		 */
		public function tearDown() {
			unset( $this->Activite );
			parent::tearDown();
		}

		/**
		 * Test de la méthode Activite::add()
		 */
		public function testAdd() {
            $this->Activite = ClassRegistry::init('Activite');
            $this->Activite->setDataSource('test');
            $this->Activite->recursive = -1;

            $data = array(
				'Activite' => array(
                    'active' => true,
					'name' => 'Social',
                    'description' => 'Association à but non lucratif',
				)
			);
			$this->Activite->create( $data );
			$this->assertTrue( $this->Activite->beforeSave() );

			$result = $this->Activite->data;

			$success = $this->Activite->save( $data );
			$this->assertTrue( !empty( $success ) );

			$expected = array(
				'Activite' => array(
                    'active' => true,
					'name' => 'Social',
                    'description' => 'Association à but non lucratif',
                    'created' => 'now()',
                    'modified' => 'now()'
				),
			);
			$this->assertEqual( $result, $expected, var_export( $result, true ) );
		}
		/**
		 * Test de la méthode Activite::edit()
		 */
		public function testEdit() {
            $this->Activite = ClassRegistry::init('Activite');
            $this->Activite->setDataSource('test');
            $this->Activite->recursive = -1;

            // 0. Enregistrement de la première orientation
			$data = array(
				'Activite' => array(
					'name' => 'Bâtiment',
                    'description' => 'Entreprise de BTP',
                    'active' => true
				)
			);
			$this->Activite->create();
			$success = $this->Activite->save( $data );
			$this->assertTrue( !empty( $success ) );

			$id = $this->Activite->id;

			// 1. Modification du template mail
			$data = array(
				'Activite' => array(
					'id' => $id,
					'name' => 'Bâtiment',
                    'description' => 'Entreprise de BTP spéciélisé dans les gros immeubles',
                    'active' => true
				)
			);
			$this->Activite->create( $data );
			$this->assertTrue( $this->Activite->beforeSave() );

			$result = $this->Activite->data;

			$expected = array(
				'Activite' => array(
                    'id' => $id,
                    'active' => true,
					'name' => 'Bâtiment',
                    'description' => 'Entreprise de BTP spéciélisé dans les gros immeubles',
                    'created' => 'now()',
                    'modified' => 'now()'
				),
			);
			$this->assertEqual( $result, $expected, var_export( $result, true ) );
		}

		/**
		 * Test de la méthode Activite::getName()
		 */
		public function testGetActivites() {
            $this->Activite = ClassRegistry::init('Activite');
            $this->Activite->setDataSource('test');
            $this->Activite->recursive = -1;

            $result = $this->Activite->find(
                'first',
                array(
                    'fields' => array(
                        'Activite.id',
                        'Activite.name',
                        'Activite.description',
                        'Activite.active'
                    ),
                    'conditions' => array('Activite.id' => 1), 'contain'  => false));
            $expected = array(
                'Activite' => array(
                    'id' => 1,
					'name' => 'Social',
                    'description' => 'Association à but non lucratif',
                    'active' => true
                )
            );
			if( empty($result) ) {
				$expected = array();
			}
            $this->assertEqual( $expected, $result, var_export( $result, true ) );
		}

        /**
		 * Test de la méthode Activite::delete()
		 */
		public function testDelete() {
            $this->Activite = ClassRegistry::init('Activite');
            $this->Activite->setDataSource('test');
            $this->Activite->recursive = -1;

			$activite1 = $this->Activite->find(
				'first',
				array(
					'conditions' => array('Activite.id' => 1),
					'contain'  => false
				)
			);
            if( !empty($activite1) ) {
				$result = $this->Activite->delete( 1 );
				$this->assertEqual( $result, true, var_export( $result, true ) );
			}


			$query = array(
				'fields' => array(
					'Activite.id',
					'Activite.name'
				),
				'conditions' => array(
					'Activite.id' => 1
				),
                'contain' => false,
                'recursive' => -1
			);
			$result = $this->Activite->find( 'first', $query );
			$this->assertEqual( $result, array(), var_export( $result, true ) );
		}
	}
?>
