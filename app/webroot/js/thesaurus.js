/**
 * Thesaurus helping functions
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 */


var thesaurus = Class.extend({

	init: function(){
		var thesaurus = $(this);
		var thesaurusList;
		var options1;
		var options2;
		var input1;
		var input2;
		var bttn;
		var bttnCancel;
		var container;
		var globalSettings = {};
	},

	create: function(thesaurusList, options){

		thesaurus.globalSettings = {
			'posStart': '',
			'valueStart': 0,
			'input1Default': ' --- Choisir un thésaurus ---',
			'input2Default': ' --- Choisir un mot-clef ---',
			'containerId': 'thesaurusContainer',
			'defaultContainerClass': 'panel',
			'bttnOkContent': '<span class="btn btn-success btn-secondary">Ok</span>',
			'bttnCancelContent': '<span class="btn btn-danger-webgfc btn-inverse btn-secondary">Cancel</span>'
		};

		if (options) {
			$.extend( thesaurus.globalSettings, options );
		}

		thesaurus.options1 = '<option pos="" value="0">' + thesaurus.globalSettings.input1Default + '</option>';
		thesaurus.options2 = '<option value="0">' + thesaurus.globalSettings.input2Default + '</option>';
		thesaurus.thesaurusList = thesaurusList;
		thesaurus.input1 = $('<select></select>').css({"height": "35px","background": "#fff","margin-bottom": "10px","border-radius": "5px"});
		thesaurus.input2 = $('<select></select>').css({"height": "35px","background": "#fff","margin-bottom": "10px","border-radius": "5px"});
		thesaurus.container = $('<div></div>').attr('id', thesaurus.globalSettings.containerId).css({
			'background-color': '#ffffff',
			position: 'absolute',
			display: 'none',
			padding: '5px;',
			'z-index': 100
		});

		for (var pos = 0; pos < thesaurusList.length; pos++){
			thesaurus.options1 += '<option value=' + thesaurusList[pos][0]['id'] + ' pos="' + pos + '">'+thesaurusList[pos][0]['nom'] + '</option>';
		}

		thesaurus.input1.html(thesaurus.options1).change(function(){
			var pos = $('option:selected', $(this)).attr('pos');
			thesaurus.secondInputInit();
			if (pos != ''){
				thesaurus.firstInputChange(pos);
			}
		});

		thesaurus.bttnCancel = $(thesaurus.globalSettings.bttnCancelContent).button().click(function(){
			thesaurus.hide();
		});
		thesaurus.bttn = $(thesaurus.globalSettings.bttnOkContent).button();

		var content = $('<div></div>').addClass('panel panel-default').css({"margin-bottom": "0px"});
		var panel = $('<div></div>').addClass('panel-body form-horizontal');
		var header = $('<div></div>').addClass('panel-heading').html('<h4 class="panel-title">Thésaurus</h4>');
		var controls = $('<div></div>').addClass('panel-footer btn-group').css({"text-align": "right","width":"100%"});

		panel.append(thesaurus.input1);
		panel.append('<br />');
		panel.append(thesaurus.input2);

		controls.append(thesaurus.bttn);
		controls.append(thesaurus.bttnCancel);

		content.append(header).append(panel).append(controls);
		thesaurus.container.append(content);
		$('body').append(thesaurus.container);
	},

	secondInputInit: function(){
		thesaurus.input2.html(thesaurus.options2);
	},

	firstInputChange: function(pos){
		for (i in thesaurus.thesaurusList[pos][1]){
			thesaurus.input2.append('<option value="' + i + '">' + thesaurus.thesaurusList[pos][1][i] + '</option>');
		}
	},

	destroy: function(){
		thesaurus.container.remove();
	},

	hide: function(){
		thesaurus.container.fadeOut('fast');
		$('option', thesaurus.container).removeAttr('selected');
		thesaurus.secondInputInit();
	},

	show: function(container, target){
		container.append(thesaurus.container);
		thesaurus.hide();
		thesaurus.container.fadeIn('fast').position({
			my: "left top",
			at: "right top",
			of: target
		});

		thesaurus.bttn.unbind('click');
		thesaurus.bttn.click(function(){
			if ($('option:selected', thesaurus.input2).val() != 0){
				$(target).val($('option:selected', thesaurus.input2).html());
				thesaurus.hide();
			}
		});
	}
});

var thesaurus = new thesaurus();
