/*
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author JIN Yuzhu
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 */
/*
 function setJSelect(select) {
 var id = select.attr('id');

 $('option', select).each(function () {
 $(this).addClass('item');
 $(this).attr('sId', $(this).val());
 });
 //        editItem( $('select#DesktopDesktop.JSelectMultiple'));

 var newList = $('<ul>');
 newList.attr('id', id + 'List');
 newList.addClass('jSelectList');

 var addAllBttn = $('<span title="Tout ajouter">');
 addAllBttn.html('<i class="fa fa-plus"></i>');
 addAllBttn.addClass('addAllBttn btn btn-info-webgfc');
 addAllBttn.button();
 $('span', $(addAllBttn)).css('padding', '3px');
 addAllBttn.click(function () {
 var typeChoix = new Array();
 $('option', select).removeAttr('selected').each(function () {
 if ($(this).css('display') == 'block') {
 $(this).attr('selected', 'selected');
 addItem(select);
 }
 if ($(this).parents('.panel-body').find('#TypeTypeList').length != 0) {
 typeChoix.push($(this).val());
 }
 });
 chargeSoustypes(typeChoix);
 });

 select.after(newList.before('<br />Liste des critères selectionnés : ')).after(addAllBttn.before(' '));

 var duplicatedSelect = select.clone(true);
 duplicatedSelect.attr('multiple', 'multiple');
 duplicatedSelect.attr('id', duplicatedSelect.attr('id') + "duplicated");
 $('option', duplicatedSelect).each(function () {
 if ($(this).val() == '' || $(this).val() == 'undefined') {
 $(this).remove();
 }
 });
 duplicatedSelect.css('display', 'none');
 select.before(duplicatedSelect);
 select.attr('name', '');
 select.removeAttr('multiple');
 select.addClass('form-control');
 //    select.css('width', '250px');
 //    select.css({
 //        width: '250px',
 //        position: 'relative',
 //        top: '-3px'
 //    });

 select.change(function () {
 addItem($(this));
 });


 }
 function chargeSoustypes(types) {
 $.ajax({
 type: 'post',
 url: '/recherches/charge_soustypes',
 data: {TypeChoix: types},
 success: function (data) {
 $('#SoustypeSoustype').html(data);
 $('#SoustypeSoustypeduplicated').html(data);
 }
 });
 }

 function delItem(e) {
 var select = $('#' + e.attr('parentSelect'));
 var list = e.parent().parent().parent();
 var listItem = e.parent().parent();
 var sId = e.attr('sId');
 var item;
 $('option', select).each(function () {
 if ($(this).attr('sId') == sId) {
 item = $(this);
 }
 });
 var realItem;
 $('#' + select.attr('id') + 'duplicated option').each(function () {
 if ($(this).attr('sId') == sId) {
 realItem = $(this);
 }
 });
 listItem.remove();
 if (item != null) {
 item.css('display', 'block');
 }
 if (realItem != null) {
 realItem.removeAttr('selected');
 }
 if ($('li', list).length == 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length != 0) {
 $('.delAllBttn[listId=' + list.attr('id') + ']').remove();
 }
 if (e.attr('parentSelect') == 'TypeType') {
 var typeChoix = new Array();
 list.find('li').each(function () {
 typeChoix.push($(this).attr('sid'));
 });

 //        if (typeChoix.length != 0) {
 chargeSoustypes(typeChoix);
 //        }
 }
 }

 function addItem(select) {
 var id = select.attr('id');
 var item = $('option:selected', select);
 var list = $('#' + id + 'List');
 var realItem;
 $('#' + id + 'duplicated option').each(function () {
 if ($(this).attr('sId') == item.attr('sId')) {
 realItem = $(this);
 }
 });

 if (item !== 'undefined' && item.html() !== 'undefined' && item.html() != "") {
 var li = $('<li>');
 li.attr('sId', item.val());
 li.addClass('duplicatedId' + item.val());

 var div = $('<div>');
 div.css({
 'padding': '5px'
 }).addClass('ui-corner-all ui-widget-content');
 //			div.hover(function() {
 //				$(this).addClass('ui-state-hover');
 //			}, function() {
 //				$(this).removeClass('ui-state-hover');
 //			});
 div.html(item.html());


 var delBttn = $('<span>');
 delBttn.html('<img src="/img/icons/cancel.png" alt="enlever" title="enlever" style="vertical-align:middle;"/>');
 delBttn.addClass('delBttn');
 delBttn.css({
 'float': 'right',
 'cursor': 'pointer'
 });
 delBttn.attr('parentSelect', id);
 delBttn.attr('sId', item.val());

 $('span', $(delBttn)).css('padding', '1px');
 delBttn.click(function () {
 delItem($(this));
 });

 list.append(li.append(div.append(delBttn)));
 item.css('display', 'none');
 $('option:selected', select).removeAttr('selected');
 realItem.attr('selected', 'selected');
 }

 if ($('li', list).length > 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length == 0) {
 var delAllBttn = $('<span title="Tout enlever">');
 delAllBttn.html('<i class="fa fa-minus"></i>');
 delAllBttn.addClass('delAllBttn btn btn-info-webgfc');
 delAllBttn.attr('listId', list.attr('id'));
 delAllBttn.button();
 $('span', $(delAllBttn)).css('padding', '3px');
 delAllBttn.click(function () {
 $('span.delBttn', list).each(function () {
 delItem($(this));
 });
 $(this).remove();
 });

 list.after(delAllBttn);
 }

 // Spécifique aux sous-valeurs des métadonnéees
 //         if( select[0].id == 'MetadonneeMetadonnee' ) {
 //             displayValueMeta(item.attr('sId'));
 //         }
 }
 function editItem(select) {
 var id = select.attr('id');
 var item = $('option:selected', select);
 var list = $('#' + id + 'List');
 var realItem = [];

 select.each(function () {
 if ($(this).is(':selected')) {
 realItem.push($(this));
 }
 });

 if (item !== 'undefined' && item.html() !== 'undefined' && item.html() != "") {

 var li = $('<li>');
 li.attr('sId', item.val());
 li.addClass('duplicatedId' + item.val());

 var div = $('<div>');
 div.css({
 'width': '350px',
 'padding': '5px'
 }).addClass('ui-corner-all ui-widget-content');
 div.hover(function () {
 $(this).addClass('ui-state-hover');
 }, function () {
 $(this).removeClass('ui-state-hover');
 });
 div.html(item.html());


 var delBttn = $('<span>');
 delBttn.html('<img src="/img/icons/cancel.png" alt="enlever" title="enlever" style="vertical-align:middle;"/>');
 delBttn.addClass('delBttn');
 delBttn.css({
 'float': 'right',
 'cursor': 'pointer'
 });
 delBttn.attr('parentSelect', id);
 delBttn.attr('sId', item.val());

 $('span', $(delBttn)).css('padding', '1px');
 delBttn.click(function () {
 delItem($(this));
 });

 list.append(li.append(div.append(delBttn)));
 item.css('display', 'none');
 $('option:selected', select).removeAttr('selected');

 for (var i = 0; i < realItem.length; i++) {
 realItem[i].attr('selected', 'selected');
 }

 }


 if ($('li', list).length > 0 && $('.delAllBttn[listId=' + list.attr('id') + ']').length == 0) {
 var delAllBttn = $('<span>');
 delAllBttn.html('Tout enlever');
 delAllBttn.addClass('delAllBttn');
 delAllBttn.attr('listId', list.attr('id'));
 delAllBttn.button();
 $('span', $(delAllBttn)).css('padding', '3px');
 delAllBttn.click(function () {
 $('span.delBttn', list).each(function () {
 delItem($(this));
 });
 $(this).remove();
 });
 list.after(delAllBttn);
 }
 }

 function resetJSelect(formId) {
 $(formId).find(':input').each(function () {
 switch (this.type) {
 case 'password':
 case 'select-multiple':
 case 'select-one':
 $('span.select2-chosen').empty();
 $('div.select2-container').addClass('select2-default');
 $('div.select2-container').removeClass('select2-allowclear');
 $('li.select2-search-choice').remove();
 case 'text':
 case 'textarea':
 $(this).val('');
 break;
 case 'checkbox':
 case 'radio':
 this.checked = false;
 }
 });

 $('.itemSelectList', $(formId)).empty();
 $('select option', $(formId)).removeAttr('selected');
 $('select option', $(formId)).css('display', 'block');
 $('.deleteAllBtn').remove();
 $('.table-list h3 span').remove();
 }
 */
function switchSave() {
    if ($('#RechercheSave').prop("disabled", false)) {
        $('#RechercheName').attr('disabled', 'disabled');
    } else {
        $('#RechercheName').removeAttr('disabled');
    }
}
