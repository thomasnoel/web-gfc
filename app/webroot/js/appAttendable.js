/**
 * attendable
 * Requires: jQuery v1.3+, Bootstrap
 */
(function ( $ ) {

	"use strict";

var sendGetProgress, downloadTimer, config = {
                baseUrl: './',
            };
/**
 * Ajoute le token en fin d'url si lien (GET) ou en champ caché si formulaire (POST)
 * @returns {number}
 */
function addToken(elt, token) {
    if ($(elt).get(0).tagName == 'A') {
        //Ajout du num de cookie en dernier argument url
        var href = $(elt).attr('href');
    } else if ($(elt).get(0).tagName == 'FORM') {
        var href = $(elt).attr('action');
    }
    if (href.substr(href.length - 1) != '/') {
        href += '/';
    }

    return href + token
}

/**
 * génère un numéro unique (token) à partir du temps courant
 * @returns {number} Token
 */
function setToken() {
    downloadToken = new Date().getTime();
    return downloadToken.toString();
}

function getProgress(token, source) {
    $.ajaxSetup({cache: false, async: true});

    if(!_.isNull(token) && sendGetProgress){
        sendGetProgress = false;
        $.get(config.baseUrl + 'documents/getProgress/' + token, null, function (response, statut) {
            var fusion = response[token.toString()];
            sendGetProgress = true;

            if (fusion == null) {
                sendGetProgress = false;
                unblockUI();
                $.growl({
                    title: "<strong>Erreur: </strong>",
                    message: "Identifiant inconnu",
                }, {type: "danger"});
            } else if (fusion['status'] == 3) {
                sendGetProgress = false;
                unblockUI();
                source.trigger("progress_send");
                $.growl({
                    title: "<strong>Erreur: </strong>",
                    message: (fusion['error']['message']).replace(/\\n/g, "<br />") + " (" + fusion['error']['code'] + ")",
                }, {type: "danger", delay: 30000});
            }

            $("#waiter").find(".progress-bar").attr("aria-valuenow", fusion['progress']).html(fusion['progress'] + "%").css("width", fusion['progress'] + "%");
            if (!_.isNull(fusion['progress_comment'])) {
                $("#waiter").find(".waiter-comment").html(fusion['progress_comment']);
            }

            if (fusion['status'] == 4) {
                sendGetProgress = false;
                setTimeout(function () {
                    if (fusion['download']) {
                        window.location = config.baseUrl + 'documents/getProgressDocument/' + token;
                    } else if (!_.isUndefined(fusion['redirect']) && !_.isNull(fusion['redirect'])) {
                        window.location = fusion['redirect'];
                    }
                    unblockUI();
                    source.trigger("progress_send");
                }, 0);
            }
            if (fusion['status'] == 5) {
                sendGetProgress = false;
            }
        }, 'json');
    }
}

// Prevents double-submits by waiting for a cookie from the server.
function blockUI(token, source, titre) {
    var modalOptions = {
        backdrop: 'static',
        keyboard: false
    };
    if (source.attr('data-waiter-title')) {
        $('#waiterLabel').text(source.attr('data-waiter-title'));
    } else {
        $('#waiterLabel').text(titre);
    }

    $("#waiter").modal(modalOptions);
    $("#waiter").find(".waiter-progress").hide();
    $("#waiter").find(".waiter-default").hide();
    $("#waiter").find(".waiter-comment").html("Veuillez patienter...");

    if (source.attr('data-waiter-type') === 'progress-bar') {
        $("#waiter").find(".waiter-progress").show();
        $("#waiter").find(".progress-bar").attr("aria-valuenow", "0").html("0%").css("width", "0%");
    } else {
        $("#waiter").find(".waiter-default").show();
    }

    if (source.attr('data-waiter-callback')) {
        sendGetProgress = true;
        downloadTimer = setInterval(function () {
            if(!_.isUndefined(source.attr('data-waiter-callback'))){
                eval(source.attr('data-waiter-callback') + '(token, source);');
            }
        }, 2000);
    }

    return token;
}

    function unblockUI() {
        clearInterval(downloadTimer);
        $("#waiter").modal('hide');
    }

    //jQuery.fn.extend
    return $.fn.appAttendable = function (options) {

        var defaults = {
            titre: 'Action en cours de traitement'
        },
        titre,
        options = $.extend({}, defaults, options);//Fusion des options avec celles par défaut

        config.baseUrl = $("meta[name='base']").attr('href')+'/';
        //A vérifier
        sendGetProgress = false;

        return this.each(function () {
            // Titre de la fenêtre modale
            if ($(this).attr('data-waiter-title') !== undefined)
                titre = $(this).attr('data-waiter-title');
            else
                titre = options.titre;

            if ($(this).attr('data-waiter-callback') == 'getGeneration') {
                $.ajaxSetup({cache: false, timeout: 600000});
            }

            if (this.tagName == 'A') {
                //Si onclick présent sur l'élément
                var patt = new RegExp(/confirm\((.+)\)/);
                if ($(this).attr('onclick') && patt.test($(this).attr('onclick').toLowerCase())) {
                    var confirm_msg = $(this).attr('onclick');
                    $(this).prop('onclick', null);
                }
                $(this).off('click');
                $(this).on('click', function (e) {
                    var token = setToken();
                    if ($(this).attr('data-waiter-send') && $(this).attr('data-waiter-send') == 'XHR') {
                        e.preventDefault();
                    }
                    if (confirm_msg) {
                        var confirm_objet = eval('(function () {' + confirm_msg + '})();');
                    }
                    if (_.isUndefined(confirm_objet) || confirm_objet) {
                        blockUI(token, $(this), titre);
                        if ($(this).attr('data-waiter-send') && $(this).attr('data-waiter-send') == 'XHR') {
                            $.get(addToken($(this), token));
                        }
                    } else {
                        e.preventDefault();
                    }
                });

            } else if (this.tagName == 'FORM') {
                //Remove the event handler submit
                $(this).off('submit');
                $(this).on('submit', function (e) {
                    var token = setToken();
                    var hrefToken = addToken($(this), token);

                    blockUI(token, $(this), titre);
                    if ($(this).attr('data-waiter-send') && $(this).attr('data-waiter-send') == 'XHR') {
                        e.preventDefault();
                        //FIX
                        if(tinyMCE){
                            tinyMCE.triggerSave();
                        }
                        $.post(hrefToken, $(this).serialize());
                    } else {
                        //Demande post sans XHR ne rien faire
                    }
                });
            }

        });
    };

})(jQuery);

