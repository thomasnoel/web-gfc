///*
// * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
// *
// * @author JIN Yuzhu
// * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
// * @link http://adullact.org/
// * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
// *
// */
//
////Addressbooks
///* Form Create Addressbooks */
$(document).ready(function () {

    $('#AddressbookAddForm').validator('validate');
    $('#AddressbookEditForm').validator();
////Affaires
//    /*Form create Affaire*/
    $('#AffaireAddForm').validator();
//    /*Form modifier Affaire */
    $('#AffaireEditForm').validator();
////Armodels
//    /*Form create Armodel*/
    $('#ArmodelAddForm').validator();
////Bans
//    /*Form modifier Bans*/
    $('#BanEditForm').validator();
////Banscommunes
//    /*Form modifier Bans-Communes*/
    $('#BancommuneEditForm').validator();
////Collectivites
//    /*Form create un nouveau collectivite*/
    $("#CollectiviteAddForm").validator();//有问题
//    /* Form modifier collectivite pour son administrateur*/
    $("#CollectiviteAdminForm").validator();
//    /*Form modifier un collectivite */
    $("#CollectiviteEditForm").validator();
////Contactinfo
//    /*Form Ajouter un nouveau contactInfo*/
//    $('#ContactinfoAddForm').validate({
//        rules: {
//            'data[Contactinfo][name]': 'required',
//            'data[Contactinfo][nom]': 'required'
//        }
//    });
//    /*Form modifier contactInfo*/
//    $('#ContactinfoEditForm').validate({
//        rules: {
//            'data[Contactinfo][name]': 'required',
//            'data[Contactinfo][nom]': 'required'
//        }
//    });
////Contacts
//    /*Form ajouter un nouveau contact*/
    $('#ContactAddForm').validator();
//    /*Form modifier contact*/
    $('#ContactEditForm').validator();
    $('#CourrierAddContactForm').validator();
    $('#CourrierAddOrganismeForm').validator();
    $('#CourrierEditOrganismeForm').validator();
//    /*Form ajouter un nouvaeu contact depuis creation d'un flux*/
//    $('input[name="data[Contact][citoyen]"]').change(function () {
//        if ($('input[name="data[Contact][citoyen]"]:checked').val() == 0) {
//            $("#ContactAddFromCourrierForm").validate({
//                rules: {
//                    "data[Contact][addressbook_id]": "required",
//                    "data[Contact][name]": "required",
//                    "data[Organisme][email]": "required",
//                    "data[Contact][nom]": "required",
//                    "data[Organisme][name]": "required",
//                    "data[Contactinfo][name]": "required"
//                }
//            });
//            $('#ContactAddFromCourrierForm').valida();
//        } else {
//            console.log($('input[name="data[Contact][citoyen]"]:checked').val());
//            $("#ContactAddFromCourrierForm").validate({
//                rules: {
//                    "data[Contact][addressbook_id]": "required",
//                    "data[Contact][name]": "required",
//                    "data[Contact][nom]": "required",
//                    "data[Contactinfo][name]": "required"
//                }
//            });
//        }
//    })

    $("#DossierAddForm").validator();
    $("#DossierEditForm").validator();
////Courriers
//    /*Form Create infos pour nouveau flux */
    $("#CourrierCreateInfosForm").validator();
//    /*Form ************/
    $('#CourrierDispmultiplesendForm').validator();
//    /*Form insert un flux reponse dans un soustype/circuit ************/
    $('#CourrierReponseReponseForm').validator();
//    /*Form envoyer le flux à l'aiguilleur */
    $('#CourrierSendBackToDispForm').validator();
//    /*Form modifier un infos d'un flux*/
    $("#CourrierSetInfosForm").validator();
////Desktops
//    /* Form envoyer un rôle delegue*/
    $('#DesktopsUserSetDelegForm').validator();
////Desktopsmanagers
//    /*Form ajouter un nouveau DesktopsManager*/
    $('#DesktopmanagerAddForm').validator();
//    /*Form modifier un desktopsmanager*/
    $('#DesktopmanagerEditForm').validator();
////Groups
//    /*Form ajouter une profile privié******************/
    $('#ProfilAddForm').validator();
//    /*Form modifier les infos d'une profile*******************/????
    $('#ProfilSetInfosForm').validator();
////Métadonnées
//    /* Form ajouter un nouveau metadonnee*/
    $('#MetadonneeAddForm').validator();
//    /*Form mofidier metadonnee*/
    $('#MetadonneeEditForm').validator();
////Organismes
//    /*Form ajouter un nouveau organiseme*/
    $('#OrganismeAddForm').validator();
//    /*Form modifier organiseme*/
    $('#OrganismeEditForm').validator();
////Recherches
//    /*Form recherche enregistrer demander un nom oblige*/ ????
    $('#RechercheFormulaireForm').validator();
////Repertoires (à enlever après)
//    /*Form ajouter un nouveau repertoire*/
    $('#RepertoireAddForm').validator();
//    /*Form modifier repertoire*/
    $('#RepertoireEditForm').validator();
////Scanemails
//    /*Form ajouter scane mail */
    $('#ScanemailAddForm').validator();
//    /* Form modifier scane mail*/
    $('#ScanemailEditForm').validator();
////Services
//    /* Form ajouter un nouveau service*/
    $('#ServiceAddForm').validator();
//    /*Form modifer service*/
    $('#ServiceSetInfosForm').validator();
////Soustypes
//    /*Form ajouter un nouveau soustype*/
    $("#SoustypeAddForm").validator();
//    /*Form modifier soustype*/
    $("#SoustypeEditForm").validator();
////Statistique
//    /*Form recher indicateurs_geo ?????????*/
    $('#StatistiqueIndicateursGeoForm').validator();
////Tache
//    /*Form ajouter un tache*/
    $('#TacheAddForm').validator()
//    /*Form modifier tache*/
    $('#TacheEditForm').validator();
////Types
//    /*Form ajouter un nouveau type*/
    $("#TypeAddForm").validator();
//    /*Form modifier type*/
    $("#TypeEditForm").validator();
////Originesflux
//    /*Form ajouter un nouveau Originesflux*/
    $('#OriginefluxAddForm').validator();
//    /*Form modifier Originesflux*/
    $('#OriginefluxEditForm').validator();
////User
//    /*Form ajouter un nouveau utilisateur*/
    $('#UserAddForm').validator();
//    /*Form changement de mot de passe*/
    $("#UserChangemdpForm").validator();
//    /*Form modifier un utilisateur*/
    $('#UserEditForm').validator();
//    /*Form login ???????????*/
    $('#UserLoginForm').validator();
//    /*Form mot de passe oublie ?????????????*/
    $('#UserMdplostForm').validator();
//    /*Form ajouet un nouveau desktop*/
    $('#DesktopAddForm').validator();
//    /*Form modifier desktop*/
    $('#DesktopEditForm').validator();
//    /*Form gestion Absence Form*/
    $("#UserAbsenceGestionForm").validator();
//    /*Form plan de absence*******/
    $("#UserAbsencePlanificationForm").validator();
//    /*Form set infos de person*/
    $("#UserSetInfosPersosForm").validator();
//    /*Form set mot de passe d'un utilisateur*********/
    $("#UserSetMdpForm").validator();
////Circuit
//    /*Form ajouter un nouveau circuit*/
    $('#CircuitAddForm').validator();
//    /*Form modifier un circuit*/
    $('#CircuitEditForm').validator();

//    /*Form modifier un etape
    $('#EtapeEditForm').validator();
//    /*Form ajouter un nouveau etape*/
    $('#EtapeAddForm').validator();

    $("#NotificationRefusForm").validator();
////Gestion Templatemails
    $('#TemplatemailAddForm').validator();

    $('#TemplatemailEditForm').validator();

    $('#nouveau_fluxNomFLux').validator();

    $('#TemplatenotificationAddForm').validator();

    $('#TemplatenotificationEditForm').validator();

    $('#ContactAddParCarnetForm').validator();

    $('#OperationAddForm').validator();

    $('#OperationEditForm').validator();

    $('#IntituleagentAddForm').validator();

    $('#IntituleagentEditForm').validator();

    $('#MarcheAddForm').validator();

    $('#MarcheEditForm').validator();

    $('#OrdreserviceAddForm').validator();

    $('#OrdreserviceEditForm').validator();

    $('#PliconsultatifAddForm').validator();

    $('#PliconsultatifEditForm').validator();

    $('#EnvironnementTestenvoimailForm').validator();
});
