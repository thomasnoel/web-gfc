/**
 * jQuery gui plugin
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * using jQuery
 * @link http://jquery.com/
 *
 * using jQuery-ui
 * @link http://jqueryui.com/
 *
 * using jQuery validate plugin
 * @link http://bassistance.de/jquery-plugins/jquery-plugin-validation/
 */

$.maxZIndex = $.fn.maxZIndex = function(opt) {
	/// <summary>
	/// Returns the max zOrder in the document (no parameter)
	/// Sets max zOrder by passing a non-zero number
	/// which gets added to the highest zOrder.
	/// </summary>
	/// <param name="opt" type="object">
	/// inc: increment value,
	/// group: selector for zIndex elements to find max for
	/// </param>
	/// <returns type="jQuery" />
	var def = {
		inc: 10,
		group: "*"
	};
	$.extend(def, opt);
	var zmax = 0;
	$(def.group).each(function() {
		var cur = parseInt($(this).css('z-index'));
		zmax = cur > zmax ? cur : zmax;
	});
	if (!this.jquery)
		return zmax;

	return this.each(function() {
		zmax += def.inc;
		$(this).css("z-index", zmax);
	});
}

var gui = Class.extend({
	/*
	 *  Initialisation de la classe
	 *
	 *  options{
	 *  }
	 */
	init: function(options) {

		var gui = $(this);
		var globalSettings = {};

		if (options) {
			$.extend(globalSettings, options);
		}

		if ($.validator) {
			$.validator.setDefaults({
				errorClass: "ui-state-error",
				highlight: function(input) {
					$(input).addClass("ui-state-highlight");
				},
				unhighlight: function(input) {
					$(input).removeClass("ui-state-highlight");
				}
			});
		}

	},
	/*
	 *  Conversion de caracteres speciaux html
	 */
	htmlEntities: function(str) {
		return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/, '&#39;');
	},
	/*
	 *  Systeme de message
	 *
	 *  affiche une boite de dialogue avec un message. on peut definir si la boite est prioritaire et interdit l acces a tout autre element
	 *  de la page.
	 *
	 *  options{
	 *      string title: titre de la boite
	 *      string msg: le message a afficher
	 *      integer width: largeur de la boite
	 *      boolean modal: defini si le message doit etre prioritaire et s il interdit l interaction avec tout autre element de la page
	 *      object buttons: defini les bouton de la boite de dialogue
	 *          ex: buttons: {
	 *              "Nom du bouton": function(){
	 *                  //ici le code javascript
	 *
	 *                  //si on veut fermer la boite de dialogue apres les action definies au-dessus
	 *                  $(this).dialog('close');
	 *              }
	 *          }
	 *  }
	 */

	message: function(options) {

		var settings = {
			dialogClass: '',
			width: 300,
			maxHeight: 600,
			maxWidth: 1000,
			modal: true,
			title: "",
			buttons: {
				Ok: function() {
					$(this).dialog('close');
				}
			}
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.msg && settings.title) {

			var event = new $.Event();
			var box_id = event.timeStamp;

			var box = $("<div title='" + settings.title + "' id='dialogbox_" + box_id + "'>" + settings.msg + "</div>");
			$('body').append(box);
			$('#dialogbox_' + box_id).dialog({
				modal: settings.modal,
				width: settings.width,
				height: settings.height,
				maxWidth: settings.maxWidth,
				maxHeight: settings.maxHeight,
				close: function() {
					$(this).remove();
				},
				buttons: settings.buttons,
				dialogClass: settings.dialogClass
			});
			if (settings.height) {
				$('#dialogbox_' + box_id).dialog('option', 'height', settings.height);
			}
		}
	},
	/*
	 *  Systeme de message d erreur
	 *
	 *  options{
	 *      string msg: le message a afficher
	 *      boolean refresh: affiche un bouton pour rafraichir/recharger la page entiere
	 *  }
	 */

	errorMessage: function(msg, refresh) {

		var buttons = {
			Ok: function() {
				$(this).dialog('close');
			},
			"Retour à l'environnement": function() {
				window.location.href = "/users/logout";
			}
		}
		if (refresh) {
			$.extend(buttons, {
				"Recharger la page": function() {
					window.location.reload();
				}
			});
		}

		var settings = {
			dialogClass: 'ui-state-focus',
			width: 450,
			modal: true,
			title: gui.htmlEntities("La requete n'a pas pu être effectuée"),
			msg: "<span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>" + msg,
			buttons: buttons
		};

		gui.message(settings);
	},
	/*
	 *  Systeme de message d erreur
	 *
	 *  options{
	 *      string msg: le message a afficher
	 *      boolean refresh: affiche un bouton pour rafraichir/recharger la page entiere
	 *  }
	 */

	formMessage: function(options) {
		var settings = {
			data: '',
			width: '450px',
			title: gui.htmlEntities("Formulaire"),
			noErrorMsg: false,
			buttons: {
				Submit: function() {
					$(".ui-dialog-content form").submit();
					$(this).dialog('close');
				},
				Cancel: function() {
					$(this).dialog('close');
				}
			}
		};

		if (options) {
			$.extend(settings, options);
		}

		var event = new $.Event();
		var loader_id = event.timeStamp;


		if (settings.url) {
			$.ajax({
				async: true,
				dataType: "html",
				type: "post",
				data: settings.data,
				beforeSend: function() {
					if (settings.updateElement && settings.loader && settings.loaderMessage) {
						gui.loader({
							id: loader_id,
							element: settings.updateElement,
							message: settings.loaderMessage
						});
					} else if (settings.loader) {
						gui.loader({
							id: loader_id
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (!settings.noErrorMsg) {
						if (textStatus != "abort") {
							if (jqXHR.status == 403) {
								gui.errorMessage("Accès refusé.<br />Votre session a peut-être expirée.", true);
							} else if (jqXHR.status == 404) {
								gui.errorMessage("URL invalide");
							} else {
								gui.errorMessage("erreur : " + jqXHR.status);
							}
						}
					}
					if (settings.erroraction) {
						eval(settings.erroraction);
					}
				},
				complete: function() {
					$("#loader_" + loader_id).remove();
					if (settings.action) {
						eval(settings.action);
					}
				},
				success: function(data) {
					var settingsToSend = {
						width: settings.width,
						modal: true,
						title: settings.title,
						msg: data,
						buttons: settings.buttons

					};

					if (settings.height) {
						$.extend(settingsToSend, {
							height: settings.height
						});
					}

					gui.message(settingsToSend);
				},
				url: settings.url
			});
		}
	},
	/*  systeme de requete ajax
	 *
	 *  permet d effectuer des requetes ajax. on peut mettre a jour le contenu d un element et
	 *  afficher un message d attente sur cet element. on peut aussi definir des actions s executant dans le cas
	 *  d un succes ou d une erreur de la requete.
	 *
	 *  options{
	 *      string url: url de la requete
	 *      string action: action effectuee une fois la requete terminee avec succes
	 *      string erroraction: action effectuee une fois la requete terminee avec erreur
	 *      object updateElement: element mis a jour avec le resultat html de la requete
	 *      boolean loader: affiche un message de chargement au dessus de l element mis a jour
	 *      function successFunction: fonction executee en cas de succes de la requete (attention: a priori, on ne se sert de ce parametre que dans le cas d un
	 *          appel de la fonction gui.formMessage. Normalement, on ne doit pas redefinir cette fonction)
	 *
	 *          function par defaut: suppression du loader existant, remplissage de l element indique, execution de l action definie
	 *
	 *          ex:
	 *              successFunction: function(data){
	 *                  //ici  le code javascript
	 *              }
	 *  }
	 */

	request: function(options, func) {

		var event = new $.Event();
		var loader_id = event.timeStamp;

		var settings = {
			type: "post",
			dataType: "html",
			loaderMessage: "Chargement ...",
			stopMouseEvents: true,
			noErrorMsg: false,
			data: null,
			successFunction: function(data) {
				if (settings.updateElement) {
					settings.updateElement.html(data);
				}
			}
		};



		if (options) {
			$.extend(settings, options);
		}

		if (func !== undefined) {
			settings.successFunction = func;
		}

		if (settings.url) {
			$.ajax({
				type: settings.type,
				async: true,
				dataType: settings.dataType,
				data: settings.data,
				beforeSend: function() {
					if (settings.updateElement && settings.loader) {
						gui.loader({
							id: loader_id,
							stopMouseEvents: settings.stopMouseEvents,
							element: settings.updateElement,
							message: settings.loaderMessage
						});
					} else if (settings.loader) {
						gui.loader({
							stopMouseEvents: settings.stopMouseEvents,
							id: loader_id
						});
					}
					if (settings.loaderMessage && settings.loaderElement) {
						gui.loader({
							stopMouseEvents: settings.stopMouseEvents,
							id: loader_id,
							element: settings.loaderElement,
							message: settings.loaderMessage
						});
					}
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (!settings.noErrorMsg) {
						if (textStatus != "abort") {
							if (jqXHR.status == 403) {
								gui.errorMessage("Accès refusé.<br />Votre session a peut-être expirée.", true);
							} else if (jqXHR.status == 404) {
								gui.errorMessage("URL invalide");
							} else {
								//								gui.errorMessage("jqXHR.status : " + jqXHR.status + "- textStatus : " + textStatus + "- errorThrown : " + errorThrown);
								gui.errorMessage("Erreur : impossible d'accèder à cette page.");
							}
						}
					}
					if (settings.erroraction) {
						eval(settings.erroraction);
					}
				},
				complete: function() {
					$("#loader_" + loader_id).remove();
					if (settings.action) {
						eval(settings.action);
					}
				},
				success: settings.successFunction,
				url: settings.url
			});
		}
	},
	/*  message d attente
	 *
	 *  genere une div avec un fond transparent qui se place au dessus d un element donne.
	 *
	 *  options{
	 *      int id: identifiant/timestamp du message d attente
	 *      object element: element au dessus duquel va s afficher le message d attente
	 *      string message: message a afficher. peut etre du code html (ex: <img src='chemin/vers/animation.gif alt='' />)
	 *      int messageWidth: largeur du message
	 *  }
	 */

	/*
	 * TODO: prevoir un systeme pour eviter d ajouter un message d attente a un element qui est deja en attente
	 */

	loader: function(options) {
		var settings = {
			id: 0,
			stopMouseEvents: true,
			element: $("body"),
			message: "Chargement ...",
			messageWidth: 150,
			cssClasses: "",
			zindex: $.maxZIndex() + 1
		};

		if (options) {
			$.extend(settings, options);
		}

		var message = $("<div>" + settings.message + "</div>").css({
			color: "white",
			width: settings.messageWidth + "px",
			marginLeft: "auto",
			marginRight: "auto"
		});

		var elementOffset = settings.element.offset();
		var loaderCss = {
			opacity: 0.9,
			background: "url('/img/waiter-bg.png') repeat scroll 0 0 transparent",
			width: settings.element.outerWidth(true) + "px",
			height: settings.element.outerHeight(true) + "px",
			position: "absolute",
			zIndex: settings.zindex
		};

		if (!settings.stopMouseEvents) {
			$.extend(loaderCss, {
				'pointer-events': 'none'
			});
		}

		$.extend(loaderCss, elementOffset);
		var loader = $("<div id='loader_" + settings.id + "' class='loader " + settings.cssClasses + "'></div>").css(loaderCss).append(message);
		$('body').append(loader);
	},
	/*  systeme d onglets
	 *
	 *  utilise le systeme d onglet de jQuery ui (http://jqueryui.com)
	 *
	 *  options{
	 *      object element: element contenant les onglets (voir doc jQuery UI : http://jqueryui.com/demos/tabs/)
	 *  }
	 */


	tabs: function(options) {
		var settings = {
			firstLoaderInside: false,
			noErrorMsg: false,
			loaderMessage: "Chargement ..."
		};

		if (options) {
			$.extend(settings, options);
		}

		if (!settings.updateElement) {
			settings.updateElement = settings.element;
		}

		if (settings.element) {

			var event = new $.Event();
			var loader_id = event.timeStamp;
			var firstLoad = true;

			settings.element.tabs({
				cache: true,
				load: function(e, ui) {
					$("#loader_" + loader_id).remove();
					$(".tabs_loader").remove();
				},
				ajaxOptions: {
					//cache: true,
					cache: false,
					beforeSend: function() {
						var id = settings.element.tabs('option', 'selected');
						if (id == 0) {
							id = 1;
						}
						if (firstLoad && settings.firstLoaderInside) {
							var firstLoader = $('<div></div>').append($(settings.loaderMessage).clone());
							$(firstLoader).addClass('tabs_loader');
							firstLoader.css({
								'background': 'url("/img/waiter-bg.png") repeat scroll 0 0 transparent',
								'opacity': 0.9
							});
							$('.ui-tabs').append(firstLoader);
							firstLoad = false;
						} else {
							gui.loader({
								id: loader_id,
								element: settings.updateElement,
								message: settings.loaderMessage,
								cssClasses: "tabs_loader"
							});
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {
						if (!settings.noErroMsg) {
							if (textStatus != "abort") {
								if (jqXHR.status == 403) {
									gui.errorMessage("Accès refusé.<br />Votre session a peut-être expirée.", true);
								} else if (jqXHR.status == 404) {
									gui.errorMessage("URL invalide");
								} else {
									gui.errorMessage("erreur : " + jqXHR.status);
								}
							}
						}
					}
				}
			});
		}
	},
	/*  masque de tabs
	 *
	 *  permet de masquer les onglets d un systeme d'onglet pour eviter que l utilisateur puisse cliquer dessus
	 *      ex: dans le cas d un wizard, on peut utiliser des boutons suivant et precedent et ne pas utliser les onglets
	 *
	 *  options{
	 *      object element: element contenant les onglets (voir doc jQuery UI : http://jqueryui.com/demos/tabs/)
	 *  }
	 */

	maskTabs: function(options) {

		var settings = {
			element: false,
			color: "#000",
			opacity: 0.2
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element) {
			var masked = $('ul', settings.element);
			masked.parent().parent().append(
					$("<div />").position({
				my: "left top",
				at: "left top",
				of: masked
			}).css({
				'width': masked.width() + parseInt(masked.css("padding-left"), 10) + parseInt(masked.css("padding-right"), 10) + parseInt(masked.css('border-left-width'), 10) + parseInt(masked.css('border-right-width'), 10),
				'height': masked.height() + parseInt(masked.css("padding-top"), 10) + parseInt(masked.css("padding-bottom"), 10) + parseInt(masked.css('border-top-width'), 10) + parseInt(masked.css('border-bottom-width'), 10),
				'position': 'absolute',
				'top': masked.offset().top,
				'left': masked.offset().left,
				'background-color': settings.color,
				opacity: settings.opacity
			}).addClass('ui-corner-all')
					);
		}
	},
	/*  boite de boutons
	 *
	 *  transforme tous les spans d un element en bouton
	 *
	 *  options{
	 *      object element: element contenant les boutons
	 *  }
	 */


	buttonbox: function(options) {
		var settings = {
			align: "left"
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element) {
			settings.element.addClass('ui-widget-header ui-corner-all');
			settings.element.css('text-align', settings.align);

			$("span", settings.element).button();
		}

		if (settings.elements) {
			$(settings.elements).each(function() {
				alert($(this).attr('id'));
				$(this).addClass('ui-widget-header ui-corner-all');
				$(this).css('text-align', settings.align);
				$("span", $(this)).button();
			});
		}

	},
	/*  ajout de bouton
	 *
	 *  ajoute un bouton a une boite de boutons
	 *
	 *  options{
	 *      object element: element contenant les boutons (par defaut : $("body")
	 *      object button: bouton a ajouter
	 *          ex:
	 *                  var buttons = {
	 *                      content: "bttn1",
	 *                      action: function(){
	 *                          alert('bttn1');
	 *                      }
	 *                  }
	 *  }
	 */

	addbutton: function(options) {
		var settings = {
			element: $("body")
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.button) {
                    if(settings.button.class){
                        var bttn = $("<span class='"+settings.button.class+"'>" + settings.button.content + "</span>").button().bind('click', settings.button.action);
                    }else{
                        var bttn = $("<span>" + settings.button.content + "</span>").button().bind('click', settings.button.action);
                    }

			settings.element.append(bttn);
		}
	},
	/*  ajout de boutons
	 *
	 *  ajoute plusieurs boutons a une boite de boutons
	 *
	 *  options{
	 *      object element: element contenant les boutons (par defaut : $("body")
	 *      array buttons: liste des boutons a ajouter
	 *          ex:
	 *                  var buttons = [
	 *                      {
	 *                          content: "1st added bttn",
	 *                          action: "alert('1st bttn clicked');"
	 *                      },
	 *                      {
	 *                          content: "2nd added bttn",
	 *                          action: "alert('2nd bttn clicked');"
	 *                      }
	 *                  ]
	 *  }
	 */

	addbuttons: function(options) {
		var settings = {
			element: $("body")
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.buttons) {
			$(settings.buttons).each(function() {
				gui.addbutton({
					element: settings.element,
					button: this
				})
			});
		}
	},
	/*  suppression de bouton
	 *
	 *  supprime un bouton d une boite de boutons
	 *
	 *  options{
	 *      object element: element contenant les boutons
	 *      integer button: bouton a supprimer (attention: le premier boutton a un index egal a 1)
	 *      text button: text du bouton a supprimer
	 *  }
	 */

	removebutton: function(options) {
		var settings = {};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element && settings.button) {

			var buttontodel = settings.button;
			if (typeof settings.button == "string") {
				var cpt = 1;
				$(".ui-button", settings.element).each(function() {
					if ($('.ui-button-text', this).text() == settings.button) {
						buttontodel = cpt;
					}
					cpt++;
				});
			}

			var cpt = 1;
			$(".ui-button", settings.element).each(function() {

				if (cpt == buttontodel) {
					$(this).remove();
				}
				cpt++;
			});
		}

	},
	/*  suppression de boutons
	 *
	 *  supprime plusieurs boutons d une boite de boutons
	 *
	 *  options{
	 *      object element: element contenant les boutons
	 *      array buttons: boutons a supprimer. le texte du bouton a supprimer
	 *          ex:
	 *              var buttons = ["Test1", "Test2"]
	 *  }
	 */

	removebuttons: function(options) {
		var settings = {};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element && settings.buttons) {
			$(settings.buttons).each(function() {
				gui.removebutton({
					element: settings.element,
					button: String(this)
				});
			});
		}
	},
	/*  activavtion d un bouton
	 *
	 *  active un bouton
	 *
	 *  options{
	 *      object element: element contenant les boutons
	 *      int button: index du bouton a activer. atention: les index commenencent a 1
	 *      string button: texte du bouton a activer.
	 *  }
	 */

	enablebutton: function(options) {
		var settings = {
			action: false
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element && settings.button) {

			var buttontoswitch;

			var cpt = 1;
			$(".ui-button", settings.element).each(function() {
				if (typeof settings.button == "string") {
					if ($('.ui-button-text', this).text() == settings.button) {
						buttontoswitch = this;
					}
				} else if (cpt == parseInt(settings.button)) {
					buttontoswitch = this;
				}
				cpt++;
			});

			$(buttontoswitch).removeClass('ui-state-disabled');
			if (settings.action) {
				$(buttontoswitch).unbind('click');
				$(buttontoswitch).click(settings.action);
			}

		}

	},
	/*  desactivavtion d un bouton
	 *
	 *  desactive un bouton
	 *
	 *  options{
	 *      object element: element contenant les boutons
	 *      int button: index du bouton a desactiver. atention: les index commenencent a 1
	 *      string button: texte du bouton a desactiver.
	 *  }
	 */

	disablebutton: function(options) {
		var settings = {
			unbindAction: false
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element && settings.button) {

			var buttontoswitch;

			var cpt = 1;
			$(".ui-button", settings.element).each(function() {
				if (typeof settings.button == "string") {
					if ($('.ui-button-text', this).text() == settings.button) {
						buttontoswitch = this;
					}
				} else if (cpt == parseInt(settings.button)) {
					buttontoswitch = this;
				}
				cpt++;
			});

			$(buttontoswitch).addClass('ui-state-disabled');
			if (settings.unbindAction) {
				$(buttontoswitch).unbind("click");
			}
		}

	},
	/*  activavtion / desactivation d un bouton
	 *
	 *  active ou desactive un bouton
	 *
	 *  options{
	 *      object element: element contenant les boutons
	 *      int button: index du bouton a activer / desactiver. atention: les index commenencent a 1
	 *      string button: texte du bouton a activer / desactiver.
	 *  }
	 */

	switchbutton: function(options) {
		var settings = {};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element && settings.button) {

			var buttontoswitch;

			var cpt = 1;
			$(".ui-button", settings.element).each(function() {
				if (typeof settings.button == "string") {
					if ($('.ui-button-text', this).text() == settings.button) {
						buttontoswitch = this;
					}
				}
				else if (cpt == parseInt(settings.button)) {
					buttontoswitch = this;
				}
				cpt++;
			});

			if ($(buttontoswitch).hasClass('ui-state-disabled')) {
				$(buttontoswitch).removeClass('ui-state-disabled');
			} else {
				$(buttontoswitch).addClass('ui-state-disabled');
			}
		}

	},
	/*  selecteur de date
	 *
	 *  active ou desactive un bouton
	 *
	 *  options{
	 *      object element: element a trandformer en selecteur de date
	 *      string image: image ouvrant le selecteur de date
	 *  }
	 */

	datepicker: function(options) {
		var settings = {
			altFormat: null,
			altField: null,
			onSelect: null,
			minDate: null,
			maxDate: null
		};

		if (options) {
			$.extend(settings, options);
		}

		if (settings.element) {
			if (settings.image) {
				if (settings.dateFormat) {
					$(settings.element).datepicker({
						showOn: "button",
						buttonImage: settings.image,
						buttonImageOnly: true,
						showButtonPanel: true,
						dateFormat: settings.dateFormat,
						altFormat: settings.altFormat,
						altField: settings.altField,
						onSelect: settings.onSelect,
						minDate: settings.minDate,
						maxDate: settings.maxDate
					});
				} else {
					$(settings.element).datepicker({
						showOn: "button",
						buttonImage: settings.image,
						buttonImageOnly: true,
						showButtonPanel: true,
						altFormat: settings.altFormat,
						altField: settings.altField,
						onSelect: settings.onSelect,
						minDate: settings.minDate,
						maxDate: settings.maxDate
					});
				}
			} else {
				if (settings.dateFormat) {
					$(settings.element).datepicker({
						showOn: "focus",
						showButtonPanel: true,
						dateFormat: settings.dateFormat,
						altFormat: settings.altFormat,
						altField: settings.altField,
						onSelect: settings.onSelect,
						minDate: settings.minDate,
						maxDate: settings.maxDate
					});
				} else {
					$(settings.element).datepicker({
						showOn: "focus",
						showButtonPanel: true,
						altFormat: settings.altFormat,
						altField: settings.altField,
						onSelect: settings.onSelect,
						minDate: settings.minDate,
						maxDate: settings.maxDate
					});
				}
			}
		}
	},
	/*  formulaire d upload de fichier
	 *
	 *  genere un formulaire permettant d uploader un fichier de maniere transparente
	 *
	 *  options{
	 *      url: adresse a laquelle on envoi le formulaire
	 *      string target: nom de l iframe utiliser pour effectuer l upload de maniere transparente
	 *      string fileInputName: nom (name) du champ input file
	 *      string button: texte du bouton d envoi
	 *      object hiddenItem: champ cache permettant l envoi d informations supplementaire
	 *          ex : var hiddenItem = {
	 *              name: "user_id",
	 *              value: "12"
	 *          }
	 *  }
	 */

	createupload: function(options) {

		var settings = {
			label: "Fichier à envoyer",
			target: "upload_target",
			fileInputName: "uploadfile",
			button: "Envoyer",
			buttonIco: ""
		};

		if (options) {
			$.extend(settings, options);
		}

		var ret = false;

		if (settings.url) {
			var form = $("<form method='post' enctype='multipart/form-data' target='" + settings.target + "'></form>");
			$(form).attr('action', settings.url);

			$(form).append("<label for='" + settings.fileInputName + "'>" + settings.label + "</label><input name='" + settings.fileInputName + "' type='file' />");
			if (settings.hiddenItem.name && settings.hiddenItem.value) {
				$(form).append("<input name='" + settings.hiddenItem.name + "' type='hidden' value='" + settings.hiddenItem.value + "'/>")
			}

			var bttn;
			if (settings.button != '') {
				bttn = $("<span></span>").html('<img src="' + settings.buttonIco + '" alt="" style="height: 16px;width: 16px;vertical-align: middle;"/> ' + settings.button);
			} else {
				bttn = $("<span></span>").html(settings.button);
			}

			bttn.button().click(function() {
				if ($('input[type="file"]', $(this).parent('form')).val() != '') {
					$(this).parent('form').submit();
				}
			});
			$(form).append(bttn);

			$("body").append("<iframe id='" + settings.target + "' name='" + settings.target + "' src='#' style='width:0;height:0;border:none;'></iframe>");
			ret = $(form);
		}
		return ret;
	},
	/*  filtres
	 *
	 *  filtre l'affichage des lignes d'un tableau HTML suivant des critères choisis parmis les élements de l'entête du tableau HTML
	 *
	 *  options{
	 *      object table: le tableau auquel s'appliquent les filtres
	 *      object element: element presentant la gestion des filtres
	 *  }
	 */


	filter: function(options) {

		var settings = {};

		if (options) {
			$.extend(settings, options);
		}
		;

		if (settings.element && settings.table) {

			var table = $("<table />");

			function applyFilters() {
				$(".tableBodyTr", settings.table).each(function() {
					$(this).css('display', 'table-row');
				});

				var active = 0;
				$("tr", table).each(function() {
					applyFilter($(this));
					if ($('td:nth-child(3n) input', $(this)).attr('checked') == 'checked') {
						active = 1;
					}
				});

				var str = $(".statusFilter img", settings.element).attr('src');
				if (active == 1) {
					var reg1 = new RegExp("(filter_off_16.png)", "g");
					str = str.replace(reg1, "filter_on_16.png");
				} else {
					var reg2 = new RegExp("(filter_on_16.png)", "g");
					str = str.replace(reg2, "filter_off_16.png");
				}
				$(".statusFilter img", settings.element).attr('src', str);
			}

			function applyFilter(element) {
				var colName = $('td:first', element).html();
				var value = $('td:nth-child(2n) select option:selected', element).html();

				var colNum = 1;
				var cpt = 0;
				$('th', settings.table).each(function() {
					if ($(this).html() == colName) {
						colNum += cpt;
					}
					cpt++;
				});

				$('.tableBodyTr', settings.table).each(function() {
					if ($('td:nth-child(3n) input', element).attr('checked') == 'checked') {
						if ($("td:nth-child(" + colNum + "n)", $(this)).html() != value) {
							$(this).css('display', 'none');
						}
					}
				});
			}

			var filters = [];
			var cpt = 0;
			$('th', settings.table).each(function() {
				if (!$(this).hasClass('thState') && !$(this).hasClass('thActions') && !$(this).hasClass('thCheckable')) {
					var name = $(this).html();
					var content = [];
					$('.tableBodyTr', settings.table).each(function() {
						var tr = this;
						var cpt2 = 0;
						$('td', tr).each(function() {
							if (cpt == cpt2) {
								var val = $(this).html();
								if (val != "" && $.inArray(val, content) == -1) {
									content.push(val);
								}
							}
							cpt2++;
						});
					});

					if (content.length > 1) {
						filters.push({
							name: name,
							content: content
						});
					}
				}
				cpt++;
			});

			if (filters.length > 0) {
				var thead = $('<thead />');
				thead.append('<th class="filterThFiltre">Filtre</th>');
				thead.append('<th class="filterThValeur">Valeur</th>');
				thead.append('<th class="filterThActif">Actif</th>');
				table.append(thead);
				for (i in filters) {
					var input = $('<select />');
					for (j in filters[i].content) {
						input.append('<option>' + filters[i].content[j] + '</option>');
					}
					var line = $('<tr />');
					line.append($('<td />').append(filters[i].name));
					line.append($('<td />').append(input.change(function() {
						applyFilters();
					})));
					line.append($('<td />').css('text-align', 'center').append($('<input />').attr('type', 'checkbox').change(function() {
						applyFilters();
					})));
					line.hover(function() {
						$(this).addClass('ui-state-highlight');
					}, function() {
						$(this).removeClass('ui-state-highlight');
					});
					table.append(line);
				}
				var div = $("<div />").addClass('ui-widget ui-corner-all ui-widget-content').css('margin-bottom', '5px').hide().append(table);
				settings.element.append(div);

				$('span.bttnFilter', settings.element).button().click(function() {
					$('div', settings.element).slideToggle();
					var reg1 = new RegExp("up_16.png", "g");
					var reg2 = new RegExp("down_16.png", "g");
					var str = $("img:nth-child(2n)", $(this)).attr('src');
					if (str.match(reg1)) {
						str = str.replace(reg1, "down_16.png");
					}
					else {
						str = str.replace(reg2, "up_16.png");
					}
					$("img:nth-child(2n)", $(this)).attr('src', str);
				});
			} else {
				$('span.bttnFilter', settings.element).remove();
				$('span.statusFilter', settings.element).remove();
			}
		}
	},
	tree: function(options) {

		var settings = {};

		if (options) {
			$.extend(settings, options);
		}
		;

		if (settings.element && settings.table) {
			var tmp;
		}
	},
	submitAll: function(options) {

		var settings = {
			loader: false,
			loaderMessage: 'chargement ...',
			loaderElement: $('body')
		};

		if (options) {
			$.extend(settings, options);
		}

		//        if (typeof settings.submits === 'object' && settings.index && settings.endFunction){

		if (settings.submits[settings.index] != 'end') {
			$.ajax({
				type: "POST",
				url: settings.submits[settings.index].url,
				data: settings.submits[settings.index].data,
				success: function(data) {
					gui.getJsonResponse(data);
					gui.submitAll({
						submits: settings.submits,
						index: settings.index + 1,
						enfFunction: settings.endFunction,
						loader: settings.loader,
						loaderMessage: settings.loaderMessage,
						loaderElement: settings.loaderElement
					});
				}
			});
		}
		else {
			eval(settings.enfFunction);
		}
		//        }
	},
	getJsonResponse: function(data) {
		var json;
		try {
			json = jQuery.parseJSON(data);

			if (json.success === true) {
				$.jGrowl.defaults.theme = 'ui-state-focus';
			} else {
				$.jGrowl.defaults.theme = 'ui-state-error';
			}

			if (json.message != '') {
				jQuery.jGrowl(json.message, {});
			} else {
				$.jGrowl.defaults.theme = 'ui-state-highlight';
				jQuery.jGrowl('Erreur inattendue (pas de message)', {});
			}
		}
		catch (e) {
			$.jGrowl.defaults.theme = 'ui-state-highlight';
			jQuery.jGrowl('Erreur inattendue (Message standard non reçu)', {});
		}
		return json;
	}

});

var gui = new gui();

gui.loaderMessage = "<br /><br /><br /><br /><div class='ajaxloaderDiv ui-corner-all shadow'>Chargement ...<br />Veuillez patienter !<br /><br /><br /><img src='/img/ajax-loader.gif' alt='' /></div>";
