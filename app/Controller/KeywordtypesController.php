<?php

/**
 * Types de mots-clefs
 *
 * Keywordtypes controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 *
 * based on As@lae keywordlists / keywords (thesaurus)
 * @link https://adullact.net/projects/asalae/
 */
class KeywordtypesController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var array
	 */
	public $name = 'Keywordtypes';

	/**
	 *
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//  public function beforeFilter() {
//    $this->Journalisation->observe(
//            array(
//                'index' => array(
//                    'evenement_nom' => 'Codes des types de mot clé : listage',
//                    'trigger' => 'beforeRender',
//                    'message' => array('Listage des codes des types de mot clé par %s', 'session.Auth.User.toString')
//                )
//            )
//    );
//    parent::beforeFilter();
//  }

	/**
	 *
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//  public function index() {
//    $this->pageTitle = Configure::read('appName') . ' : ' . __('Types de mot clé', true) . ' : ' . __('liste', true);
//    $this->paginate = array(
//        'limit' => $this->Auth->user('nblignes'),
//        'page' => 1
//    );
//    $this->request->data = $this->paginate($this->modelClass);
//  }
}

?>
