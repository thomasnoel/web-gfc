<?php

/**
 * Scanemails
 *
 * Scanemails controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ScanemailsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Scanemails';
    public $uses = array('Scanemail', 'Type');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionScanemails', true)
        ));

        $this->set( 'conn', $this->Session->read('Auth.User.connName') );
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Scanemails
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {

        $mails = $this->Scanemail->find('all');

        // Profils
        $desktopsIds = Hash::extract($mails, '{n}.Desktop.id');
        $listDesktops = $this->Scanemail->Desktop->find(
                'list', array(
            'conditions' => array(
                'Desktop.id NOT' => $desktopsIds,
                'Desktop.profil_id' => array(
                    INIT_GID,
                    DISP_GID
                ),
                'Desktop.active' => true
            ),
            'recursive' => -1
                )
        );
        $this->set('listDesktops', $listDesktops);

        // Bureaux
        $listDesktopsManagers = $this->Scanemail->Desktopmanager->getAllDesktopsByProfils(array(DISP_GID, INIT_GID));
        $this->set('listDesktopsManagers', $listDesktopsManagers);


        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $scanemail = $this->request->data;

			if( Configure::read('Crypt.Password') ) {
				$cle = "MaCleEstIncassable";
				$motdepasseChiffre = $this->encrypt( $cle, $this->request->data['Scanemail']['password'] );
				$scanemail['Scanemail']['password'] = $motdepasseChiffre;
			}

            $this->Scanemail->begin();
            $this->Scanemail->create($scanemail);
            if ($this->Scanemail->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Scanemail->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Scanemail->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }

//        $this->set('types', $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true)));
        $Type = ClassRegistry::init('Type');
//        $this->set('types', $Type->find('all', array('contain' => array('Soustype'), 'order' => array('Type.name ASC'), 'recursive' => -1)));
        $this->set('types', $Type->find('list', array('contain' => array('Soustype'), 'order' => array('Type.name ASC'), 'recursive' => -1)));
    }

    /**
     * Edition d'un type
     *
     * @logical-group Scanemails
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Scanemail->create();
            $scanemail = $this->request->data;

            if( Configure::read('Crypt.Password') ) {
				$cle = "MaCleEstIncassable";
				$motdepasseStocke = $this->Scanemail->find('first', array('conditions' => array('Scanemail.id' => $id)));
				$motdepasseStockeDechiffre = $this->decrypt($cle, $motdepasseStocke['Scanemail']['password'] );
				$motdepasseDevantEtreenregistre = $this->request->data['Scanemail']['password'];
				if(strlen($motdepasseDevantEtreenregistre) >= 40) { // Le mot de passe est déjà chiffré donc il n'a pas à être modifié
					$motdepasseDevantEtreenregistre = $this->decrypt($cle, $this->request->data['Scanemail']['password'] );
				}
				if($motdepasseDevantEtreenregistre != $motdepasseStockeDechiffre) {
					$motdepasseChiffre = $this->encrypt( $cle, $this->request->data['Scanemail']['password'] );
					$scanemail['Scanemail']['password'] = $motdepasseChiffre;
				}
            }
            if ($this->Scanemail->save($scanemail)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Scanemail->find('first', array('conditions' => array('Scanemail.id' => $id)));

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }

        $mails = $this->Scanemail->find('all');
        $desktopsIds = Hash::extract($mails, '{n}.Desktop.id');
        $listDesktops = $this->Scanemail->Desktop->find(
                'list', array(
            'conditions' => array(
                'Desktop.profil_id' => array(
                    INIT_GID,
                    DISP_GID
                ),
                'Desktop.active' => true
            ),
            'recursive' => -1
                )
        );

        $this->set('listDesktops', $listDesktops);
        // Bureaux
        $listDesktopsManagers = $this->Scanemail->Desktopmanager->getAllDesktopsByProfils(array(DISP_GID, INIT_GID));
        $this->set('listDesktopsManagers', $listDesktopsManagers);
        //$this->set('types', $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true)));
        $Type = ClassRegistry::init('Type');
//        $this->set('types', $Type->find('all', array('contain' => array('Soustype'), 'order' => array('Type.name ASC'), 'recursive' => -1)));
        $this->set('types', $Type->find('list', array('contain' => array('Soustype'), 'order' => array('Type.name ASC'), 'recursive' => -1)));

        $this->set('soustypes', $Type->Soustype->find('list', array('order' => array('Soustype.name ASC'), 'recursive' => -1)));
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Scanemail->begin();
        if ($this->Scanemail->delete($id)) {
            $this->Scanemail->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Scanemail->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getScanemails() {
        $scanemails_tmp = $this->Scanemail->find(
                "all", array(
            'order' => 'Scanemail.mail'
                )
        );
        $scanemails = array();
        foreach ($scanemails_tmp as $i => $item) {
            if (!empty($item['Type']['name'])) {
                $typeSoustype[$i] = $scanemails_tmp[$i]['Type']['name'] . '/' . $scanemails_tmp[$i]['Soustype']['name'];
            }

            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $item['Scanemail']['desktopName'] = isset($scanemails_tmp[$i]['Desktopmanager']['name']) ? $scanemails_tmp[$i]['Desktopmanager']['name'] : null;
            $item['Scanemail']['type'] = isset($typeSoustype[$i]) ? $typeSoustype[$i] : null;
            $scanemails[] = $item;
        }
        $this->set(compact('scanemails'));

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Fonction permettant de chiffrer en BDD les mots de passe utilisés pour les boîtes mail scannées
     * @param type $decrypted
     * @param type $password
     * @param type $salt
     * @return boolean
     *  @deprecated : mcrypt ne marche plus en  7.2
     */
//    public function encrypt($decrypted, $password, $salt='!kQm*fF3pXe1Kbm%9') {
//        // Build a 256-bit $key which is a SHA256 hash of $salt and $password.
//        $key = hash('SHA256', $salt . $password, true);
//        // Build $iv and $iv_base64.  We use a block size of 128 bits (AES compliant) and CBC mode.  (Note: ECB mode is inadequate as IV is not used.)
//        srand(); $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
//        if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22) return false;
//        // Encrypt $decrypted and an MD5 of $decrypted using $key.  MD5 is fine to use here because it's just to verify successful decryption.
//        $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $decrypted . md5($decrypted), MCRYPT_MODE_CBC, $iv));
//        // We're done!
//        return $iv_base64 . $encrypted;
//    }

    function encrypt($key, $payload) {
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        $encrypted = openssl_encrypt($payload, 'aes-256-cbc', $key, 0, $iv);
        return base64_encode($encrypted . '::' . $iv);
      }

    function decrypt($key, $garble) {
        list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
    }
}

?>
