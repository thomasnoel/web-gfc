<?php

/**
 * Demarchessimplifiees
 *
 * Demarchessimplifiees controller class
 *
 * web-GFC : Gestion de Flux Citoyens
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class DemarchessimplifieesController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Demarchessimplifiees';

	/**
	 * Controller uses
	 *
	 * @var array
	 * @access public
	 */
//	public $uses = array('Demarchesimplifiee');

	public $components = array('Ds');

	/**
	 *
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('ajaxformvalid');
	}


	/**
	 * Gestion des outils disponibles pour l'administrateur)
	 *
	 * @logical-group Outils
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
			'<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
			__d('menu', 'Outils', true)
		));

		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur->setDataSource($conn);
		$hasDs = false;
		$hasDsActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Démarches%'
				),
				'contain' => false
			)
		);
		if( !empty( $hasDsActif ) && $hasDsActif['Connecteur']['use_ds'] ) {
			$hasDs = true;
		}
		$this->set('hasDs', $hasDs );
	}

	/**
	 * Recherche des procédures de démarches simplifiées existantes
	 *
	 */
	public function search() {
		$retour = array(
			'demarche' => array(),
			'error' => array(),
			'dossier' => array()
		);
		$dossierAlreadyGenerated = array();
		$courrierGeneratedId = array();
		$referenceGenerated = array();
		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur->setDataSource($conn);
		$hasDsActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Démarches%',
					'Connecteur.use_ds' => true
				),
				'contain' => false
			)
		);

		if( !empty($this->request->data) ) {
			if( Configure::read('Webservice.DS') ) {
				// On vérifie si la requête existe toujours dans la GRC
				$this->loadModel('Connecteur');
				$hasDsActif = $this->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Démarches%',
							'Connecteur.use_ds' => true
						),
						'contain' => false
					)
				);

				$procedureID = (int)$this->request->data['Demarchesimplifiee']['procedureId'];
				$dossierID = (int)$this->request->data['Demarchesimplifiee']['dossierId'];

				$host = $hasDsActif['Connecteur']['host'];
				$host = substr($host, 0, strpos($host, "/api"));

				if( !empty( $hasDsActif )) {
					$ds = new DsComponent;
					// On ne recherche que la procédure
					if( !empty($procedureID) && empty($dossierID) ) {
						$retour['demarche'] = $ds->getAllDemarcheInfos($procedureID);
						if( !empty( $retour['demarche']->errors[0]->message ) && $retour['demarche']->errors[0]->message == 'Cannot return null for non-nullable field Demarche.service' ) {

							$retour['error'] = "La démarche $procedureID ne possède pas de service associé. <br /> Veuillez associer un service à cette démarche depuis le site ".$host;
						}
					}
					//  On recherche la procédure ET un dossier spécifique
					if( !empty($procedureID) && !empty($dossierID) ) {
						$retour['demarche'] = $ds->getDemarche($procedureID);
						$retour['dossier'] = $ds->getDossier($dossierID);

						// On ajoute les données du dossier aux résultats globaux
						$retour['demarche']->data->demarche->dossiers = new StdClass();
						$retour['demarche']->data->demarche->dossiers->nodes[] = $retour['dossier']->data->dossier;
						unset( $retour['dossier'] );
					}

					$emailsInstructeurs = array();
					if( !empty($retour['demarche']->data->demarche->groupeInstructeurs) && is_int($procedureID) ) {
						foreach($retour['demarche']->data->demarche->groupeInstructeurs as $key => $instructeur) {
							foreach( $instructeur->instructeurs as $i => $emailInstructeur ) {
								$emailsInstructeurs[] = $emailInstructeur->email;
							}
						}
					}
					if( !empty($emailsInstructeurs) ) {
						$emailsInstructeurs = implode("\n", $emailsInstructeurs);
					}

					if( !empty($retour['demarche']->data->demarche->dossiers) && is_int($procedureID) ) {
						$this->loadModel('Courrier');
						$this->Courrier->setDataSource($conn);
						$courriers = $this->Courrier->find(
							'all',
							array(
								'fields' => array('Courrier.id', 'Courrier.reference', 'Courrier.ds_procedure_id', 'Courrier.ds_dossier_id'),
								'conditions' => array(
									'Courrier.ds_procedure_id' => $procedureID
								),
								'contain' => false,
								'recursive' => -1
							)
						);

						foreach($courriers as $courrier) {
							if( !empty($courrier['Courrier']['ds_dossier_id']) ) {
								foreach( $retour['demarche']->data->demarche->dossiers->nodes as $key => $dossierToCreate) {
									if( $courrier['Courrier']['ds_dossier_id'] == $dossierToCreate->number ) {
										$dossierToCreate->alreadyCreated = true;
										$dossierAlreadyGenerated[] = $retour['demarche']->data->demarche->dossiers->nodes[$key]->number;
										$courrierGeneratedId[$courrier['Courrier']['ds_dossier_id']] =  $courrier['Courrier']['id'];
										$referenceGenerated[$courrier['Courrier']['ds_dossier_id']] =  $courrier['Courrier']['reference'];
									}
								}

							}
						}
					}
				}
			}
		}

		$this->set('data', $retour);
		$this->set('hasDsActif', $hasDsActif);
		$this->set('dossierAlreadyGenerated', $dossierAlreadyGenerated);
		$this->set('courrierGeneratedId', $courrierGeneratedId);
		$this->set('referenceGenerated', $referenceGenerated);
		$this->set('emailsInstructeurs', $emailsInstructeurs);
	}


	/**
	 * Fonction permettant de créer un flux avec les données récupérées sur Démarches Simplifiées
	 * @param $procedureId
	 * @param $dossierId
	 */
	public function createCourrier( $procedureId, $dossierId ) {
		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur->setDataSource($conn);
		$hasDsActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Démarches%'
				),
				'contain' => false
			)
		);
		$this->autoRender = false;

		$this->Jsonmsg->init();

		$this->Courrier = ClassRegistry::init('Courrier');
		$this->Courrier->setDataSource($conn);
		$this->Contact = ClassRegistry::init('Contact');
		$this->Contact->setDataSource($conn);
		$this->Organisme = ClassRegistry::init('Organisme');
		$this->Organisme->setDataSource($conn);
		$this->Bancontenu = ClassRegistry::init('Bancontenu');
		$this->Bancontenu->setDataSource($conn);

		$ds = new DsComponent;
		$procedureInfo = $ds->getDemarche($procedureId);
		$dossierInfo = $ds->getDossier($dossierId);

		// Données de chaque dossier
		$datasDossier = [];
		foreach( $dossierInfo->data->dossier->champs as $key => $value ) {
			if( !empty($value->value) ) {
				$datasDossier[$value->label] = $value->value;
			}
		}
		$datasDossier['Contact']['civilite'] = $dossierInfo->data->dossier->demandeur->civilite;
		$datasDossier['Contact']['nom'] = $dossierInfo->data->dossier->demandeur->nom;
		$datasDossier['Contact']['prenom'] = $dossierInfo->data->dossier->demandeur->prenom;
		$datasDossier['dateEnConstruction'] = $dossierInfo->data->dossier->datePassageEnConstruction;

		// On regarde et on associe le contact si présent
		if( !empty($procedureInfo) ) {

			$this->Courrier->begin();
			// On s'coccupe d'abord de l'organisme
			$addressbooks = $this->Courrier->Organisme->Addressbook->find('list', array('contain' => false));
			$organismeName = $procedureInfo->data->demarche->service->organisme;
			$dataOrganisme = array(
				'Organisme' => array(
					'addressbook_id' => array_keys($addressbooks)[0], //FIXME: voir comment récupérer le bon carnet d'adresse cible
					'name' => $organismeName,
//						'email' => $procedureInfo->procedure->service->email,
//						'tel' => $procedureInfo->procedure->service->phone,
//						'adressecomplete' => $procedureInfo->procedure->service->address
				)
			);

			$OrganismeInBase = $this->Courrier->Organisme->find(
				'first',
				array(
					'conditions' => array(
						'Organisme.name' => $organismeName
					),
					'contain' => false,
					'recursive' => -1
				)
			);

			if( !empty($OrganismeInBase) ) {
				$OrganismeAlreadyExist = true;
				$OrganismeIdForCourrier = $OrganismeInBase['Organisme']['id'];
			}
			else {
				$this->Courrier->Organisme->create($dataOrganisme);
				if ($this->Courrier->Organisme->save()) {
					$OrganismeIdForCourrier = $this->Courrier->Organisme->id;
					if( !empty($OrganismeIdForCourrier) ) {
						$sansContact['Contact']['addressbook_id'] = $dataOrganisme['Organisme']['addressbook_id'];
						$sansContact['Contact']['organisme_id'] = $OrganismeIdForCourrier;
						$sansContact['Contact']['name'] = ' Sans contact';
						$sansContact['Contact']['nom'] = '0Sans contact';
						$this->Courrier->Organisme->Contact->create($sansContact['Contact']);
						$this->Courrier->Organisme->Contact->save();
					}
				}
			}
			$civiliteOptions = array(
				'M' => 1,
				'Mme' => 2
			);

			// Ensuite, on s'coccupe du contact
			if( isset($datasDossier['Contact']) && !empty($datasDossier['Contact']) ) {
				$dataContact = array(
					'Contact' => array(
						'name' => $datasDossier['Contact']['nom'] . ' ' . $datasDossier['Contact']['prenom'],
						'nom' => $datasDossier['Contact']['nom'],
						'prenom' => $datasDossier['Contact']['prenom'],
						'civilite' => $civiliteOptions[$datasDossier['Contact']['civilite']],
						'organisme_id' => $OrganismeIdForCourrier
					)
				);
				$contactAlreadyExist = false;
				$contactInBase = $this->Courrier->Contact->find(
					'first',
					array(
						'conditions' => array(
							'Contact.name' => $datasDossier['Contact']['nom'] . ' ' . $datasDossier['Contact']['prenom'],
							'Contact.nom' => $datasDossier['Contact']['nom'],
							'Contact.prenom' => $datasDossier['Contact']['prenom'],
						),
						'contain' => false,
						'recursive' => -1
					)
				);

				if( !empty($contactInBase) ) {
					$contactAlreadyExist = true;
					$contactIdForCourrier = $contactInBase['Contact']['id'];
				}
				else {
					$this->Courrier->Contact->create($dataContact);
					if ($this->Courrier->Contact->save()) {
						$contactIdForCourrier = $this->Courrier->Contact->id;
					}
				}
			}
			if( isset($datasDossier['Message']) && !empty($datasDossier['Message']) ) {
				$objet = $datasDossier['Message'];
			}
			else {
				$objet = json_encode( $datasDossier );
			}

			// Enfin, on crée le flux
			$data = array(
				'Courrier' => array(
					'name' => '[DS] Dossier '.$dossierId.' de la procédure '.$procedureId,
					'objet' => $objet,
					'user_creator_id' => CakeSession::read('Auth.User.id'),
					'date' => date('Y-m-d'),
					'datereception' => date( 'Y-m-d H:i:s', strtotime($datasDossier['dateEnConstruction'])),
					'reference' => $this->Courrier->genReferenceUnique(),
					'contact_id' => isset( $contactIdForCourrier ) ? $contactIdForCourrier : Configure::read('Sanscontact.id'),
					'organisme_id' => isset( $OrganismeIdForCourrier ) ? $OrganismeIdForCourrier : Configure::read('Sansorganisme.id'),
					'ds_procedure_id' => $procedureId,
					'ds_dossier_id' => $dossierId
				)
			);
			$this->Courrier->create($data);
			if ($this->Courrier->save()) {
				$courrierId = $this->Courrier->id;
				$return['CodeRetour'] = "CREATED";
				$return['Message'] = "Le flux a été créé";
				$return['Severite'] = "normal";
				$return['CourrierId'] = $courrierId;

				$return['Reference'] = $data['Courrier']['reference'];
				$return['ContactId'] = $data['Courrier']['contact_id'];
				$return['OrganismeId'] = $data['Courrier']['organisme_id'];

				$desktopmanagerId = $hasDsActif['Connecteur']['ds_desktopmanager_id'];
				$desktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
				$desktops = array();
				foreach ($desktopsIds as $i => $desktopId) {
					$desktops = $this->Bancontenu->Desktop->find(
						'all', array(
							'conditions' => array(
								'Desktop.id' => $desktopsIds[$i]
							),
							'contain' => false
						)
					);

					if ($desktops[$i]['Desktop']['profil_id'] == DISP_GID) {
						$BAN = BAN_AIGUILLAGE;
					} else if ($desktops[$i]['Desktop']['profil_id'] == INIT_GID) {
						$BAN = BAN_DOCS;
					}
					if( !empty($BAN)) {
						$this->Bancontenu->add($desktopId, $BAN, $courrierId);
					}
					else {
						$return['CodeRetour'] = "ERROR";
						$return['Message'] .= "L'utilisateur n'a pas de profil Aiguilleur ou Initiateur.";
					}
				}

				// génération du document attaché
				foreach($dossierInfo->data->dossier->champs as $key => $value ) {
					if( isset( $value->file) && !empty($value->file)) {
						$filename = $value->file->filename;
						$mimeType = $value->file->contentType;
						$size = $value->file->byteSize;
						$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'], true, 0777);
						$tmpFile = WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'] . DS . "{$filename}";
						file_put_contents($tmpFile, file_get_contents($value->file->url));
						$mainDoc = false;
						$this->_genFileAssoc($tmpFile , $courrierId, $filename, $mimeType, $size, $mainDoc,  $return);
					}
				}

				/*if(  isset($datasDossier['Fichiers joints']) && !empty($datasDossier['Fichiers joints']) ) {
					$filename = $this->get_string_between($datasDossier[$dossierId]['Fichiers joints'], 'filename=', '&inline');
					$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'], true, 0777);
					$tmpFile = WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'] . DS . "{$filename}";
					file_put_contents($tmpFile, file_get_contents($datasDossier[$dossierId]['Fichiers joints']));
					$this->_genFileAssoc($tmpFile , $courrierId, $filename, $return);
				}*/
				$this->Courrier->commit();
				$this->Jsonmsg->valid('Le fux a été créé à partir de ce dossier');
				$courrierId  = $this->Courrier->id;
				$this->Jsonmsg->json['newCourrierId'] = $courrierId;
			}
			else {
				$this->Courrier->rollback();
				$this->Jsonmsg->error( 'Erreur lors de la création du flux');
			}
			$this->Jsonmsg->send();
		}
	}

	/**
	 * Génération d'un document lié
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileAssoc($path, $fluxId, $filename, $mime, $size, $mainDoc, &$return) {
		$fileMimeType = $mime;

		$conn = CakeSession::read('Auth.User.connName');
		$this->Document = ClassRegistry::init('Document');
		$this->Document->setDataSource($conn);


		$fileMimeTypeError = false;
		if ($fileMimeType == "application/pdf" || $fileMimeType == "PDF") {
			$fileExt = "pdf";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.text" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.TEXT" || $fileMimeType == "OCTET-STREAM") {
			$fileExt = "odt";
		} else if ($fileMimeType == "image/bmp") {
			$fileExt = "bmp";
		} else if ($fileMimeType == "image/tiff") {
			$fileExt = "tif";
		} else if ($fileMimeType == "text/plain") {
			$fileExt = "txt";
		} else if ($fileMimeType == "application/vnd.ms-office") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/msword") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/rtf") {
			$fileExt = "rtf";
		} else if ($fileMimeType == "image/png" || $fileMimeType == "PNG") {
			$fileExt = "png";
		} else if ($fileMimeType == "image/jpeg" || $fileMimeType == "JPEG") {
			$fileExt = "jpg";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.spreadsheet" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.SPREADSHEET") {
			$fileExt = "ods";
		} else if ($fileMimeType == "VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT" || $fileMimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
			$fileExt = "docx";
		} else if ($fileMimeType == "GIF" || $fileMimeType == "gif") {
			$fileExt = "gif";
		} else if ($fileMimeType == '') {
			$fileMimeType = 'txt';
			$fileExt = "rtf";
		} else {
			$fileMimeTypeError = true;
		}
//		$filename = $this->Document->genNameUnique(). "." . $fileExt;


		if ($fileMimeTypeError) {
			$return['CodeRetour'] .= "FILENOK";
			$return['Message'] .= ". Le fichier n'est pas reconnu";
		} else {
			$this->Courrier->setDataSource(Configure::read('conn'));
			$this->Courrier->Document->setDataSource(Configure::read('conn'));
			$allDocs = $this->Courrier->Document->find(
				'all',
				array(
					'fields' => array(
						'Document.id'
					),
					'conditions' => array(
						'Document.courrier_id' => $fluxId
					),
					'contain' => false,
					'recursive' => -1
				)
			);

			if( empty( $allDocs ) ) {
				$mainDoc = true;
			}

			$doc = array(
				'Document' => array(
					'name' => urldecode($filename),
					'size' => $size,
					'mime' => $fileMimeType,
					'ext' => $fileExt,
					'courrier_id' => $fluxId,
					'main_doc' => $mainDoc,
					'path' => $path
				)
			);

			$this->Courrier->Document->create($doc);
			$docsaved = $this->Courrier->Document->save();
			if (empty($docsaved)) {
				$return['CodeRetour'] .= "NOFILE";
				$return['Message'] .= "Le flux a bien été créé mais le fichier n'a pas été envoyé<br />Veuillez ré-essayer.";
				$this->Courrier->delete($fluxId);
			} else {
				$return['CodeRetour'] .= "FILEOK";
				$return['Message'] .= "Le flux a bien été créé et le fichier a été envoyé";
			}
		}
	}

	/**
	 * Validation du champ procedureId
	 *
	 * @param type $field
	 */
	public function ajaxformvalid($field = null) {
		if( empty( $this->request->data['Demarchesimplifiee']['procedureId'] ) ) {
			$this->autoRender = false;
			$content = json_encode(array('Demarchesimplifiee' => array($field => 'Valeur obligatoire')));
			header('Pragma: no-cache');
			header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
			header('Content-Type: text/x-json');
			header("X-JSON: {$content}");
			Configure::write('debug', 0);
			echo $content;
			return;
		}

		$models = array();
		if ($field != null) {
			if ($field == 'procedureId') {
				$models['Demarchesimplifiee'] = array('procedureId');
			} else {
				$models['Demarchesimplifiee'] = array('date');
			}
		}

		$rd = array();
		foreach ($this->request->data as $k => $v) {

			if ($field == 'procedureId' && $k == 'Demarchesimplifiee') {
				$rd[$k] = $v;
			}
		}

		$this->Ajaxformvalid->valid($models, $rd);
	}


	public function get_string_between($string, $start, $end){
		$string = ' ' . $string;
		$ini = strpos($string, $start);
		if ($ini == 0) return '';
		$ini += strlen($start);
		$len = strpos($string, $end, $ini) - $ini;
		return substr($string, $ini, $len);
	}

}

?>
