<?php

/**
 * Méta-données
 *
 * Metadonnees controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class MetadonneesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Metadonnees';

    /**
     * Gestion des méta-données (interface graphique)
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionMeta', true)));
    }

    /**
     * Récupération de la liste des méta-données (ajax)
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getMetadonnees() {
		$active = true;
		$translation = [
			'select' => 'Liste déroulante',
			'booléen' => 'Oui/Non',
			'date' => 'Date',
			'texte' => 'Texte'
		];
		if(!empty($this->request->data) ) {
			$querydata = $this->Metadonnee->search($this->request->data);
			if( $this->request->data['Metadonnee']['active'] == '0' ) {
				$active = false;
			}
		}
		else {
			$querydata = array(
				'contain' => [
					'Typemetadonnee'
				],
				'order' => 'Metadonnee.name',
				'recursive' => -1
			);
		}
		$querydata['conditions'][] = array('Metadonnee.active' => $active );

        $metadonnees_tmp = $this->Metadonnee->find("all", $querydata);
        $metadonnees = array();
        foreach ($metadonnees_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
			$item['Metadonnee']['typemetadonneename'] = $translation[$item['Typemetadonnee']['name']];
            $metadonnees[] = $item;
        }
        $this->set('metadonnees', $metadonnees);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Ajout d'une méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        $listeSoustypes = $this->Metadonnee->Soustype->find(
                'list', array(
            'order' => array(
                'Soustype.name DESC'
            )
                )
        );
        $this->set('listeSoustypes', $listeSoustypes);
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Metadonnee->create($this->request->data);
            if ($this->Metadonnee->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->set('typemetadonnees', $this->Metadonnee->Typemetadonnee->find('list'));
        }
    }

    /**
     * Edition d'un méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function edit($id = null) {
        $listeSoustypes = $this->Metadonnee->Soustype->find('list', array(
            'order' => array(
                'Soustype.name DESC'
            ),
            'contain' => false,
            'recursive' => -1
        ));
        $this->set('listeSoustypes', $listeSoustypes);


        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Metadonnee->create($this->request->data);
// debug($this->request->data);
// die();
            if ($this->Metadonnee->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $querydata = array(
                'contain' => array(
                    $this->Metadonnee->Typemetadonnee->alias,
                    $this->Metadonnee->Soustype->alias,
                ),
                'conditions' => array(
                    'Metadonnee.id' => $id
                ),
                'recursive' => -1
            );
            $this->request->data = $this->Metadonnee->find('first', $querydata);
        }
        $this->set('typemetadonnees', $this->Metadonnee->Typemetadonnee->find('list', array( 'recursive' => -1, 'contain' => false)));



        $selectvalues_tmp = $this->Metadonnee->Selectvaluemetadonnee->find('all', array('conditions' => array('Selectvaluemetadonnee.metadonnee_id' => $id), 'contain' => false, 'recursive' => -1, 'order' => array('Selectvaluemetadonnee.name' => 'ASC')
        ));
        $selectvalues = array();
        foreach ($selectvalues_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $selectvalues[] = $item;
        }
        $this->set('selectvalues', $selectvalues);
        $this->set('metadonneeId', $id);
    }

    /**
     * Suppression d'une méta-donnée (delete)
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Metadonnee->begin();
        if ($this->Metadonnee->delete($id)) {
            $this->Metadonnee->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Metadonnee->rollback();
        }
        $this->Jsonmsg->send();
    }

}

?>
