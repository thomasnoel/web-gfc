<?php
/**
 * Flux
 *
 * ContactApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

// TODO add check is uuid

class ContactApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Contact', 'Collectivite'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$contacts = $this->getContacts($this->Api->getLimit(), $this->Api->getOffset());
		return new CakeResponse([
			'body' => json_encode($contacts),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($contactId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$contact = $this->getContact($contactId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($contact),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*$this->request->data = '{
				'name' => 'Nathanaël AUZOLAT',
				'civilite' => (int) 1,
				'nom' => 'AUZOLAT',
				'prenom' => 'Nathanaël',
				'numvoie' => '1 BIS',
				'nomvoie' => 'RUE DEL MAZADE',
				'cp' => '34670',
				'compl' => 'Tout en haut',
				'ville' => 'SAINT-BRES',
				'organismeName' => 'Libriciel SCOP',
				'organisme_id' => 1
		}';*/

		$contact = json_decode($this->request->input(), true);
		$res = $this->Contact->save($contact);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Contact->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['contactId' => $res['Contact']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($contactId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getContact($contactId);
			$this->Contact->delete($contactId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'status' => 204,
		]);
	}


	public function update($contactId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getContact($contactId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}
//		$this->request->data = '{
//			"name": "AUZOLAT Arnaud"
//		}';
//		$contact = json_decode($this->request->data, true);
		$contact = json_decode($this->request->input(), true);
		$contact['id'] = $contactId;
		$res = $this->Contact->save($contact);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Contact->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['contactId' => $res['Contact']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	private function sanitizeContact($jsonData)
	{
		$authorizedFields = ['name', 'nom', 'civilite', 'prenom', 'email', 'titre', 'phone'];
		$contact = array_intersect_key(json_decode($jsonData, true), array_flip($authorizedFields));
		return $contact;
	}

	private function getContacts( $limit, $offset )
	{

		$conditions = array(
			'limit' => $limit,
			'offset' => $offset
		);
		$contacts = $this->Contact->find('all', $conditions);
		return Hash::extract($contacts, "{n}.Contact");
	}


	private function getContact($contactId)
	{
		$contact = $this->Contact->find('first', [
			'conditions' => [
				'Contact.id' => $contactId
			],
			'contain' => [
				'Organisme'
			]
		]);

		if (empty($contact)) {
			throw new NotFoundException('Contact non trouvé / inexistant');
		}
		$formattedContact = Hash::extract($contact, "Contact");

		$formattedContact['organisme'] = $contact['Organisme'] ?? [];

		return $formattedContact;
	}


}
