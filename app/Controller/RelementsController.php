<?php

/**
 * Composants de réponse
 *
 * Relements controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class RelementsController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Relements';

    /**
     * Controller components
     *
     * @access public
     * @var array
     */
    public $components = array('Linkeddocs');

    /**
     * Etat d'envoi des fichiers
     *
     * @access private
     * @var array
     */
    private $_uploadStatus = array(
        'NO_UPLOAD' => 0,
        'UPLOAD_START' => 1,
        'UPLOAD_END' => 2
    );

    /**
     * Types MIME et extensions autorisés
     *
     * @access private
     * @var array
     */
    private $_allowed = array(
        'application/vnd.oasis.opendocument.text' => 'odt',
        'application/force-download' => 'odt'
    );

    /**
     * Ajout d'un document webdav
     *
     * @logical-group Composants de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $flux_id identifiant du flux
     * @return void
     */
    public function add($flux_id = null) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
        if (empty($this->request->data)) {
            $this->set('flux_id', $flux_id);
        } else {
            $upload = $this->_uploadStatus['NO_UPLOAD'];
            $relement = $this->request->data;
            $message = '';

            //traitement de l upload des fichiers
            if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                $upload = $this->_uploadStatus['UPLOAD_START'];
                $ext = '';
                if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                    $ext = $matches[1];
                }

                if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
                    $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                } else {
                    if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
//							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        //FIXME: unifier l analyse des types mime
                    } else {
                        $upload = $this->_uploadStatus['UPLOAD_END'];
                    }
                }
            }

            //enregistrement des infos en base si pas  d upload ou si l upload est terminé
            $create = false;
            if ($upload != $this->_uploadStatus['UPLOAD_START']) {



                //deplacement du fichier si le fichier est uploadé
                if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {

                    $sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS .$this->request->data['fluxId'];
                    $Folder = new Folder($sessionFluxIdDir, true, 0777);
                    if (move_uploaded_file($_FILES['myfile']['tmp_name'], $sessionFluxIdDir . DS . $_FILES['myfile']['name'])) {
                        chmod($sessionFluxIdDir, 0777);
                        $relement['Relement']['courrier_id'] = $this->request->data['fluxId'];
                        $relement['Relement']['name'] = $_FILES['myfile']['name'];
                        $this->Relement->create($relement);
                        $relementsaved = $this->Relement->save();

                        if (!empty($relementsaved)) {
                            $create = true;
                            $message = __d('default', 'save.ok');
                        }
                    }
                }
            }
            $this->set('create', $create);
            $this->set('message', $message);
            $this->set('flux_id', $this->request->data['fluxId']);
        }
    }

    /**
     * Suppression d'un document webdav
     *
     * @logical-group Composants de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du composant de réponse
     * @return void
     */
    public function delete($id = null, $fluxId = null, $filename = null, $soustypeId = null) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
        $this->Jsonmsg->init(__d('default', 'delete.error'));

        if (!empty($soustypeId)) {
            $fileToDelete = WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $soustypeId . DS . $filename;
            if (!file_exists($fileToDelete)) {
                $fileToDelete = WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $filename;
            }
        } else {
            $fileToDelete = WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $filename;
        }

        if (!file_exists($fileToDelete) && !empty($fluxId)) {
            $fileToDelete = WEBDAV_DIR . DS . $conn . DS .$fluxId . DS . $filename;
        }

        if (file_exists($fileToDelete)) {
            if ($this->Relement->delete($id)) {
//                unlink($fileToDelete);
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            }
        }
        else {
			$this->Relement->delete($id);
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		}
        $this->Jsonmsg->send();
    }

    /**
     * Téléchargement d'un document webdav
     *
     * @logical-group Composants de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du composant de réponse
     * @return void
     */
    public function download($id) {
        $relement = $this->Relement->find('first', array('conditions' => array('Relement.id' => $id), 'recursive' => -1));
        if (empty($relement)) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        Configure::write('debug', 0);
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"" . Inflector::slug($relement['Relement']["name"]) . '.' . $this->_allowed[$relement['Relement']['mime']] . "\"");
        header("Content-length: " . $relement['Relement']['size']);
        print $relement['Relement']['content'];
        exit();
    }

    /**
     * Récupération de la pré-visualisation (miniature) d'un document webdav (génération si nécessaire)
     *
     * @logical-group Composants de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $relementId identifiant du composant de réponse
     * @throws NotFoundException
     * @return void
     */
    public function getPreview($relementId) {
        $document = $this->Relement->find(
                'first', array(
            'conditions' => array(
                'Relement.id' => $relementId
            ),
            'recursive' => -1
                )
        );
        if (empty($document)) {
            throw new NotFoundException();
        }
        $preview = $this->Relement->generatePreview($relementId);
        $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Relement']['name']) . "_preview.swf";
        $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
        //purge des fichiers de preview précédents
        $this->Linkeddocs->purge();
        file_put_contents($tmpPreviewFile, $preview);
        $this->set('preview', $preview);
        $this->set('tmpPreviewFile', $tmpPreviewFile);
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
    }

    /**
     *
     * @param type $flux_id
     * @param type $modeleodt
     */
    public function addFromFile($fluxId = null, $modeleodt = null) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
        //enregistrement des infos en base si pas  d upload ou si l upload est terminé
        $create = false;

        $qdFlux = array(
            'recursive' => -1,
            'conditions' => array(
                'Courrier.id' => $fluxId
            ),
            'fields' => array(
                'Courrier.id',
                'Courrier.soustype_id'
            )
        );
        $flux = $this->Relement->Courrier->find('first', $qdFlux);
        $soustypeId = $flux['Courrier']['soustype_id'];

        $message = '';
        $sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS .$fluxId;
        $Folder = new Folder($sessionFluxIdDir, true, 0777);

        if (file_exists(WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $modeleodt)) {
            if (copy(WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $modeleodt, $sessionFluxIdDir . DS . $modeleodt)) {
                chmod($sessionFluxIdDir, 0777);
                $relement['Relement']['courrier_id'] = $fluxId;
                $relement['Relement']['name'] = $modeleodt;
                $this->Relement->create($relement);
                $relementsaved = $this->Relement->save();

                $this->Jsonmsg->init();
                if (!empty($relementsaved)) {
                    $create = true;
                    //                $message = __d('default', 'save.ok');
                    $this->Jsonmsg->valid($message . __d('default', 'save.ok'));
                }
            }
        }
        if (file_exists(WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $soustypeId . DS . $modeleodt)) {
            if (copy(WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $soustypeId . DS . $modeleodt, $sessionFluxIdDir . DS . $modeleodt)) {
                chmod($sessionFluxIdDir, 0777);
                $relement['Relement']['courrier_id'] = $fluxId;
                $relement['Relement']['name'] = $modeleodt;
                $this->Relement->create($relement);
                $relementsaved = $this->Relement->save();

                $this->Jsonmsg->init();
                if (!empty($relementsaved)) {
                    $create = true;
                    //                $message = __d('default', 'save.ok');
                    $this->Jsonmsg->valid($message . __d('default', 'save.ok'));
                }
            }
        }
        $this->Jsonmsg->send();
        $this->set('create', $create);
        $this->set('message', $message);
    }

}
