<?php

App::import('Sanitize');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Flux
 *
 * CollectiviteApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class CollectiviteApiController extends AppController {

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Collectivite', 'User'];

	public $components = ['Api'];


	/**
	 * Retourne toutes les collectivités présentes
	 * @return CakeResponse
	 */
	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->superadminauthentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$collectivites = $this->getCollectivites( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($collectivites),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	/**
	 * Retourne les informations d'une collectivité définie
	 * @param $collectiviteId
	 * @return CakeResponse
	 */
	public function find($collectiviteId)
	{
		$this->autoRender = false;
		try {
			$this->Api->superadminauthentification($this->Api->getApiToken());
			$collectivite = $this->getCollectivite($collectiviteId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($collectivite),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	/**
	 * Déclenche la création d'une collectivité
	 * @return CakeResponse
	 */
	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->superadminauthentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*$this->request->data = '{
			"name": "Nouvelle collectivité",
			"conn": "libriciel",
			"database": "webgfc_libriciel",
			"login_suffix": "libriciel",
			"adresse": "836 RUE DU MAS DE VERCAHNT",
			"codepostal": "34000",
			"complementadresse": "Bâtiment le Tucano",
			"ville": "MONTPELLIER",
			"active": "true",
			"admin": {
				"nom": "AUZOLAT",
				"prenom": "Arnaud",
				"username": "admin",
				"mail": "arnaud.auzolat@libriciel.coop",
				"password": "user"
			}
		}';*/
		$this->Collectivite->create();
		$collectivite = json_decode($this->request->input(), true);
		if( !empty( $collectivite) ) {
			$success = false;
			$this->Collectivite->query('CREATE DATABASE ' . $collectivite['database'] . ' TEMPLATE webgfc_template;');
			$this->Collectivite->begin();
			$res = $this->Collectivite->save($collectivite);

			if (!$res) {
				return $this->Api->formatCakePhpValidationMessages($this->Collectivite->validationErrors);
			}

			$this->loadModel('Desktop');
			$this->Desktop->setDataSource($collectivite['conn']);
			$this->loadModel('User');
			$this->User->setDataSource($collectivite['conn']);
			$this->loadModel('Aco');
			$this->Aco->setDataSource($collectivite['conn']);
			$this->loadModel('Aro');
			$this->Aro->setDataSource($collectivite['conn']);
			$this->loadModel('Permission');
			$this->Permission->setDataSource($collectivite['conn']);

			$desktop_admin = $this->Desktop->create();
			$desktop_admin['Desktop']['name'] = 'Profil administrateur';
			$desktop_admin['Desktop']['profil_id'] = ADMIN_GID;

			if ($this->Desktop->save($desktop_admin)) {

				$this->loadModel('Desktopmanager');
				$this->Desktopmanager->setDataSource($collectivite['conn']);
				$bureau_admin = $this->Desktopmanager->create();
				$bureau_admin['Desktopmanager']['name'] = 'Bureau '.$desktop_admin['Desktop']['name'];
				$bureau_admin['Desktopmanager']['active'] = true;
				$bureau_admin['Desktopmanager']['isautocreated'] = true;

				if($this->Desktopmanager->save($bureau_admin)) {
					$this->loadModel('DesktopDesktopmanager');
					$this->DesktopDesktopmanager->setDataSource($collectivite['conn']);
					$desktopDesktopmanager = $this->DesktopDesktopmanager->create();
					$desktopDesktopmanager['DesktopDesktopmanager']['desktop_id'] = $this->Desktop->id;
					$desktopDesktopmanager['DesktopDesktopmanager']['desktopmanager_id'] = $this->Desktopmanager->id;
					$this->DesktopDesktopmanager->save($desktopDesktopmanager);
				}

				$user = array('User' => $collectivite['admin']);
				$user['User']['desktop_id'] = $this->Desktop->id;
				$user['User']['password'] = Security::hash($collectivite['admin']['password'], 'sha256', true);

				$this->User->create($user);
				if ($this->User->save()) {
					$success = true;
				}
			}
			if ($success) {
				$this->Collectivite->commit();
			} else {
				$this->Collectivite->rollback();
			}
		}

		return new CakeResponse([
			'body' => json_encode(
				[
					'collectiviteId' => $res['Collectivite']['id'],
					'userId' => $this->User->id
				]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	/**
	 * Supprime une collectivité selon son ID
	 * @param $collectiviteId
	 * @return CakeResponse
	 */
	public function delete($collectiviteId){
		$this->autoRender = false;
		try {
			$this->Api->superadminauthentification($this->Api->getApiToken());
			$this->getCollectivite($collectiviteId);
			$this->Collectivite->delete($collectiviteId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'status' => 204,
		]);
	}

	/**
	 * Retourne toutes les collectivités disponibles en base
	 * @return array|array[]
	 */
	private function getCollectivites($limit, $offset)
	{
		$conditions = array(
			'limit' => $limit,
			'offset' => $offset
		);
		$collectivites = $this->Collectivite->find('all', $conditions);

		return Hash::extract($collectivites, "{n}.Collectivite");
	}



	/**
	 * Retourne les infos d'une collectivité
	 * @param $collectiviteId
	 * @return array|array[]
	 */
	private function getCollectivite($collectiviteId)
	{
		$collectivite = $this->Collectivite->find('first', [
			'conditions' => ['Collectivite.id' => $collectiviteId],
		]);

		if (empty($collectivite)) {
			throw new NotFoundException('Collectivité non trouvée / inexistante');
		}

		return Hash::extract($collectivite, "Collectivite");
	}


	public function update($collectiviteId)
	{
		$this->autoRender = false;
		try {
			$this->Api->superadminauthentification($this->Api->getApiToken());
			$this->getCollectivite($collectiviteId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$collectivite = json_decode($this->request->input(), true);
		$collectivite['id'] = $collectiviteId;
		$res = $this->Collectivite->save($collectivite);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Collectivite->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['collectiviteId' => $res['Collectivite']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}
}
