<?php
/**
 * Copyright (c) 2018. Libriciel scop
 * web-GFC 3.0
 * LICENCE AGPL V3
 *
 */

App::uses('AppController', 'Controller');
App::uses("ConnectionManager", "Model");

class Apiv2Controller extends AppController {

	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow();
	}

	/**
	 * simple hello test
	 * @return string
	 */
	public function ping() {
		$this->autoRender = false;
		return "ping";
	}


	public function version() {
		$this->autoRender = false;
		return WEBGFCVERSION;
	}

	/**
	 * post : {
	 *          "conn": "demo",
	 *          "username": "aauzolat",
	 *          "password": "user"
	 * 	}
	 * @return string :(success, error_database //database missing, error_user //auth error)
	 */
	public function check() {

		$this->autoRender = false;

		$data = json_decode($this->request->input(), true);

		//if no connection
		if (empty(ConnectionManager::enumConnectionObjects()[$data["conn"]])) {
			$this->response->statusCode(403);
			return "error_database";
		}

		$this->loadModel('User');
		$this->User->setDataSource($data['conn']);
		$user = $this->User->find('first', array(
			'recursive' => -1,
			'conditions' => array(
				'User.username' => $data["username"],
				'User.password' => Security::hash($data["password"], 'sha256', true)
			)
		));

		if (empty($user)) {
			$this->response->statusCode(403);
			return "error_user";
		} else {
			$this->response->statusCode(200);
			return "success";
		}
	}
}
