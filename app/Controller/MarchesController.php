<?php

/**
 * Marches
 *
 * Marches controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class MarchesController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Marches';
    public $uses = array('Marche');

    /**
     * Gestion des opérations interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            __d('menu', 'marcheSAERP', true)
        ));
        $operations = $this->Marche->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $this->set(compact('operations'));
    }

    /**
     *
     */
    public function beforeFilter() {
        $this->Auth->allow(array('export'));
        parent::beforeFilter();
    }

    /**
     *
     */
    protected function _setOptions() {
        $this->set('marchesList', $this->Marche->find('list', array('conditions' => array('Marche.active' => true))));
        $this->set('operationsList', $this->Marche->Operation->find('list', array('conditions' => array('Operation.active' => true))));
        $this->set('contractants', $this->Marche->Contractant->find('list', array('conditions' => array('Contractant.active' => true))));
        $this->set('uniterifs', $this->Marche->Uniterif->find('list', array('conditions' => array('Uniterif.active' => true))));
    }

    /**
     * Ajout d'un e opération
     *
     * @logical-group Marches
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        $operations = $this->Marche->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $this->set('operations', $operations);
        $marches = $this->Marche->find('all');

        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $marche = $this->request->data;

            $marche['Marche']['name'] = $marche['Marche']['numero'];
//debug($marche);
//            $Operation = ClassRegistry::init('Operation');
//            $operation = array();
//            $operation['Operation']['name'] = $marche['Marche']['name'];
//            $Operation->create($operation);
//            $saveOperation = $Operation->save();
//            if( $saveOperation ) {
//                $marche['Marche']['operation_id'] = $Operation->id;
//            }

            $this->Marche->begin();
            $this->Marche->create($marche);
            if ($this->Marche->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Marche->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Marche->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
        $this->_setOptions();
    }

    /**
     * Edition d'une opération
     *
     * @logical-group Marches
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        $operations = $this->Marche->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $this->set('operations', $operations);
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Marche->create();
            $marche = $this->request->data;

            if ($this->Marche->save($marche)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Marche->find('first', array(
                'conditions' => array(
                    'Marche.id' => $id
                ),
                'contain' => false
            ));
            $datePremiermois = strtotime($this->request->data['Marche']['premiermois']);
            $this->request->data['Marche']['datenotification'] = date("d/m/Y", strtotime($this->request->data['Marche']['datenotification']));
            if( $datePremiermois  != false ) {
                if(strlen($this->request->data['Marche']['premiermois']) == 7 ) {
                    $this->request->data['Marche']['premiermois'] = date("m/Y", strtotime($this->request->data['Marche']['premiermois']));
                }
            }
            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
//debug($this->request->data);
        }
        $this->_setOptions();
    }

    /**
     * Suppression d'une opération
     *
     * @logical-group Marches
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Marche->begin();
        if ($this->Marche->delete($id)) {
            $this->Marche->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Marche->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des opérations (ajax)
     *
     * @logical-group Marches
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getMarches() {
        $operations = $this->Marche->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => 'Operation.name ASC'));
        $conditions = array();

        if( !empty($this->request->data) ) {
            if( isset($this->request->data['SearchMarche']['datenotificationdebut'])  && !empty($this->request->data['SearchMarche']['datenotificationdebut']) ) {
                $dateNotif = $this->request->data['SearchMarche']['datenotificationdebut'];
                if( strpos( $dateNotif,  '/'  ) != 0 ) {
                    $debut = explode( '/', $dateNotif );
                    $day = $debut[0];
                    $month = $debut[1];
                    $year = $debut[2];
                    $dateDebut = $year.'-'.$month.'-'.$day;
                    $this->request->data['SearchMarche']['datenotificationdebut'] = $dateDebut;
                }
            }
            if( isset($this->request->data['SearchMarche']['datenotificationfin'])  && !empty($this->request->data['SearchMarche']['datenotificationfin']) ) {
                $dateNotifFin = $this->request->data['SearchMarche']['datenotificationfin'];
                if( strpos( $dateNotifFin,  '/'  ) != 0 ) {
                    $fin = explode( '/', $dateNotifFin );
                    $day = $fin[0];
                    $month = $fin[1];
                    $year = $fin[2];
                    $dateFin = $year.'-'.$month.'-'.$day;
                    $this->request->data['SearchMarche']['datenotificationfin'] = $dateFin;
                }
            }
        }

        $querydata = $this->Marche->search($this->request->data);
        $marches = $this->Marche->find('all', $querydata);

        foreach ($marches as $i => $item) {
            $item['Marche']['datenotification'] = date("d/m/Y", strtotime($item['Marche']['datenotification']));
            $item['Marche']['premiermois'] = date("m/Y", strtotime($item['Marche']['premiermois']));
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $marches[$i] = $item;
        }

        if( isset($this->request->data['SearchMarche']['datenotificationdebut'])  && !empty($this->request->data['SearchMarche']['datenotificationdebut']) ) {
            $dateNotif = $this->request->data['SearchMarche']['datenotificationdebut'];
            $debut = explode( '-', $dateNotif );
            $day = $debut[0];
            $month = $debut[1];
            $year = $debut[2];
            $dateDebut = $year.'/'.$month.'/'.$day;
            $this->request->data['SearchMarche']['datenotificationdebut'] = $dateDebut;
        }
        if( isset($this->request->data['SearchMarche']['datenotificationfin'])  && !empty($this->request->data['SearchMarche']['datenotificationfin']) ) {
            $dateNotifFin = $this->request->data['SearchMarche']['datenotificationfin'];
            $fin = explode( '-', $dateNotifFin );
            $day = $fin[0];
            $month = $fin[1];
            $year = $fin[2];
            $dateFin = $year.'/'.$month.'/'.$day;
            $this->request->data['SearchMarche']['datenotificationfin'] = $dateFin;
        }
        $this->set(compact('marches', 'operations'));
        $this->_setOptions();
    }

    /**
     * Visualisation de la liste des Ordres de Service liés à 1 marché
     *
     * @logical-group Marches
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getOs($marche_id) {

        $marche = $this->Marche->find("first", array(
            'conditions' => array(
                'Marche.id' => $marche_id
            ),
            'contain' => array(
                'Ordreservice'
            ),
            'order' => 'Marche.name'
        ));
        $marcheId = $marche_id;
        $numeroMarche = $marche['Marche']['numero'];


        $selectordresservices_tmp = $this->Marche->Ordreservice->find('all', array(
            'conditions' => array(
                'Ordreservice.marche_id' => $marche_id
            ),
            'contain' => array(
                'Organisme'
            ),
            'order' => array('Ordreservice.numero ASC')
        ));
        $selectordresservices = array();
        foreach ($selectordresservices_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $item['right_view'] = true;
            $selectordresservices[] = $item;
        }
        $this->set('selectordresservices', $selectordresservices);

        $this->set(compact('marche', 'marcheId', 'selectordresservices', 'numeroMarche'));
    }

    /**
     * Visualisation de la liste des Plis consultatifs liés à 1 marché
     *
     * @logical-group Marches
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    /* public function getPlis($marche_id) {

      $marche = $this->Marche->find("first", array(
      'conditions' => array(
      'Marche.id' => $marche_id
      ),
      'contain' => array(
      'Pliconsultatif'
      ),
      'order' => 'Marche.name'
      ));
      $marcheId = $marche_id;

      $selectplis_tmp = $this->Marche->Pliconsultatif->find('all', array(
      'fields' => array_merge(
      $this->Marche->Pliconsultatif->fields(), $this->Marche->Pliconsultatif->Origineflux->fields()
      ),
      'conditions' => array(
      'Pliconsultatif.marche_id' => $marche_id
      ),
      'joins' => array(
      $this->Marche->Pliconsultatif->join('Origineflux', array('type' => 'LEFT OUTER'))
      ),
      'recursive' => -1,
      'order' => array('Pliconsultatif.numero ASC')
      ));
      $selectplis = array();
      foreach ($selectplis_tmp as $item) {
      $item['right_edit'] = true;
      $item['right_delete'] = true;
      $item['Pliconsultatif']['origine'] = $item['Origineflux']['name'];
      $item['Pliconsultatif']['date'] = date('d/m/Y', strtotime($item['Pliconsultatif']['date']));
      $selectplis[] = $item;
      }
      $this->set('selectplis', $selectplis);
      //        debug($selectplis);
      $this->set(compact('marche', 'marcheId', 'selectplis'));
      } */

    /**
     * Validation d'unicité du nom d'un marché (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null) {

        if ($field == 'marchename') {
            if (in_array($this->request->data['Marche']['name'], $this->Marche->find('list', array('conditions' => array('Marche.active' => true), 'fields' => array('id', 'name'))))) {
                $this->autoRender = false;
                $content = json_encode(array('Marche' => array('name' => 'Valeur déjà utilisée')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
        }
        $models = array();
        if ($field != null) {
            if ($field == 'marchename') {
                $models['Marche'] = array('name');
            }
        } else {
            $models[] = 'Marche';
        }

        $rd = array();
        foreach ($this->request->data as $k => $v) {
            if (($field != 'marchename' && $k == 'Marche')) {
                $rd[$k] = $v;
            }
        }
        $this->Ajaxformvalid->valid($models, $rd);
    }

    /**
     * Export des informations liées aux marchés
     *
     * @logical-group Marches
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function export() {
//        ini_set('memory_limit', '3000M');
        $name = Hash::get($this->request->params['named'], 'SearchMarche__name');
        $numeroMarche = Hash::get($this->request->params['named'], 'SearchMarche__numero');
        $marcheActif = Hash::get($this->request->params['named'], 'SearchMarche__active');
//debug($this->request->params);
//die();
        $conditions = array();
        $conditionsPlis = array();
        $conditionsOS = array();
        if (!empty($this->request->params)) {

            if (isset($name) && !empty($name)) {
                $conditions[] = 'Marche.name ILIKE \'' . $this->Marche->wildcard('%' . $name . '%') . '\'';
            }

            if (isset($marcheActif) && !empty($marcheActif)) {
                $conditions[] = array('Marche.active' => $marcheActif);
            }

            if (isset($numeroMarche) && !empty($numeroMarche)) {
                $conditions[] = array('Marche.numero' => $numeroMarche);
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => array(
                'Marche.name'
            ),
            'contain' => array(
                'Contractant',
                'Uniterif',
                'Ordreservice' => array(
                    'Organisme',
                    'conditions' => array(
                        $conditionsOS
                    )
                ),
                'Operation'
            )
        );
        $origines = $this->Marche->Pliconsultatif->Origineflux->find('list');
        $results = $this->Marche->find('all', $querydata);
        $this->set('results', $results);
        $this->set('origines', $origines);


        $this->_setOptions();
        $this->layout = '';
//$this->set( compact( 'annee', 'origine', 'origineName' ) );
    }

}

?>
