<?php

/**
 * Fiches de contact
 *
 * OrganismesController controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class OrganismesController extends AppController {

    /**
     * Controller uses
     *
     * @var array
     */
    public $uses = array('Organisme');

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('import_organismes', 'import', 'export', 'search_organismes_courriers', 'getAdresses');
    }

    protected function _setOptions() {
        $options = array();
        $options = $this->Session->read('Auth.Typesvoie');
        $civilite = $this->Session->read('Auth.Civilite' );
        $this->set('bans', $this->Session->read('Auth.Ban') );

        $this->set('addressbooks', $this->Organisme->Contact->Addressbook->find('list', array('conditions' => array('Addressbook.active' => true))));
//        debug($options);
        $this->set(compact('options', 'civilite'));

        $this->set('activites', $this->Organisme->Activite->find('list', array('order' => array('Activite.name ASC'), 'conditions' => array('Activite.active' => true))));
        if (Configure::read('Conf.SAERP')) {
            $this->set('operations', $this->Organisme->Operation->find('list', array('order' => array('Operation.name ASC'), 'conditions' => array('Operation.active' => true))));
            $this->set('events', $this->Organisme->Contact->Event->find('list', array('order' => array('Event.name ASC'), 'conditions' => array('Event.active' => true))));
            $this->set('fonctions', $this->Organisme->Contact->Fonction->find('list', array('order' => array('Fonction.name ASC'), 'conditions' => array('Fonction.active' => true))));
        }
        $this->set('titres', $this->Organisme->Contact->Titre->find('list',array('order' => array('Titre.name ASC'), 'contain' => false ) ) );
    }

    /**
     * Ajout d'une fiche de contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @logical-used-by Flux > Ajout d'un flux
     * @user-profile User
     *
     * @logical-used-by Flux > Edition d'un flux
     * @user-profile User
     *
     * @access public
     * @param integer $contactId
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
//            $options = $this->Organisme->Bancommune->find('list');
//            $ville = Hash::get($options, Hash::get($this->request->data, 'Organisme.bancommune_id'));
//            $this->request->data['Organisme']['adressecomplete'] = Hash::get( $this->request->data, 'Organisme.numvoie' ).' '.$typevoie.' '.Hash::get( $this->request->data, 'Organisme.nomvoie' );
//            $this->request->data['Organisme']['adressecomplete'] = Hash::get($this->request->data, 'Organisme.numvoie') . ' ' . Hash::get($this->request->data, 'Organisme.banadresse') . ' ' . Hash::get($this->request->data, 'Organisme.cp') . ' ' . $ville;

            if (!empty($this->request->data['Organisme']['banadresse'])) {
                $adressecomplete = addslashes( Hash::get($this->request->data, 'Organisme.numvoie') ) . ' ' . Hash::get($this->request->data, 'Organisme.banadresse');
            } else {
                $adressecomplete = addslashes( Hash::get($this->request->data, 'Organisme.numvoie') ) . ' ' . addslashes( Hash::get($this->request->data, 'Organisme.nomvoie') );
            }
            $this->request->data['Organisme']['adressecomplete'] = $adressecomplete;
            $this->Organisme->create($this->request->data);

			if( strpos($this->request->data['Organisme']['nomvoie'], "'") !== false ) {
				$this->request->data['Organisme']['nomvoie'] = Sanitize::clean($this->request->data['Organisme']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Organisme']['adressecomplete'], "'") !== false ) {
				$this->request->data['Organisme']['adressecomplete'] = Sanitize::clean($this->request->data['Organisme']['adressecomplete'], array('encode', false));
			}

			$success = $this->Organisme->save();

            if ($success) {
                if ($success) {
                    // On crée le contact 0Sans contact quoiqu'il arrive !

                    if (!empty($sansContact['Organisme']['ban_id'])) {
                        $sansContact['Contact']['ban_id'] = $this->request->data['Organisme']['ban_id'];
                    }
                    if (!empty($sansContact['Organisme']['bancommune_id'])) {
                        $sansContact['Contact']['bancommune_id'] = $this->request->data['Organisme']['bancommune_id'];
                    }
                    if (!empty($sansContact['Organisme']['banadresse'])) {
                        $sansContact['Contact']['banadresse'] = $this->request->data['Organisme']['banadresse'];
                        $sansContact['Contact']['nomvoie'] = @$this->request->data['Organisme']['banadresse'];
                    }
                    else {
                        $sansContact['Contact']['nomvoie'] = addslashes( $this->request->data['Organisme']['nomvoie'] );
                    }

                    if( !isset($sansContact['Organisme']['banadresse'])) {
                        $sansContact['Contact']['banadresse'] = null;
                    }
                    $sansContact['Contact']['addressbook_id'] = $this->request->data['Organisme']['addressbook_id'];
                    $sansContact['Contact']['organisme_id'] = $this->Organisme->id;
                    $sansContact['Contact']['adresse'] = $adressecomplete;
                    $sansContact['Contact']['numvoie'] = addslashes( $this->request->data['Organisme']['numvoie'] );
                    $sansContact['Contact']['cp'] = $this->request->data['Organisme']['cp'];
                    $sansContact['Contact']['compl'] = addslashes( $this->request->data['Organisme']['compl'] );
                    $sansContact['Contact']['ville'] = addslashes( $this->request->data['Organisme']['ville'] );
                    $sansContact['Contact']['name'] = ' Sans contact';
                    $sansContact['Contact']['nom'] = '0Sans contact';
                    $this->Organisme->Contact->create($sansContact['Contact']);
                    $success = $this->Organisme->Contact->save() && $success;
                }


                // Si on crée un contact au moment de l'ajout d'un organisme
                if ($success && $this->request->data['Organisme']['infoscontact'] == 1) {
                    if (!empty($this->request->data['Organisme']['ban_id'])) {
                        $this->request->data['Contact']['ban_id'] = $this->request->data['Organisme']['ban_id'];
                    }
                    if (!empty($this->request->data['Organisme']['bancommune_id'])) {
                        $this->request->data['Contact']['bancommune_id'] = $this->request->data['Organisme']['bancommune_id'];
                    }
                    if (!empty($this->request->data['Organisme']['banadresse'])) {
                        $this->request->data['Contact']['banadresse'] = $this->request->data['Organisme']['banadresse'];
                        $this->request->data['Contact']['nomvoie'] = @$this->request->data['Organisme']['banadresse'];
                    }
                    else {
                        $this->request->data['Contact']['nomvoie'] = addslashes( @$this->request->data['Organisme']['nomvoie'] );
                    }
                    if( !isset($this->request->data['Organisme']['banadresse'])) {
                        $this->request->data['Contact']['banadresse'] = null;
                    }
                    $this->request->data['Contact']['addressbook_id'] = $this->request->data['Organisme']['addressbook_id'];
                    $this->request->data['Contact']['organisme_id'] = $this->Organisme->id;
                    $this->request->data['Contact']['adresse'] = $adressecomplete;
                    $this->Organisme->Contact->create($this->request->data['Contact']);
                    $success = $this->Organisme->Contact->save() && $success;
                }


                // on stocke qui à fait quoi quand
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a créé l'organisme ".$this->request->data['Organisme']['name']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
            }
            if ($success) {
                $this->Organisme->Contact->MiseajourData();
                $this->Jsonmsg->valid();
            } else {
                return false;
            }
            $this->Jsonmsg->send();
        }
        $this->_setOptions();
    }

    /**
     *
     */

    /**
     * Edition d'une fiche de contact
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function edit($id = null) {
        $this->Organisme->recursive = -1;


        if (!empty($this->request->data)) {
//            $options = $this->Organisme->Bancommune->find('list');
//            $ville = Hash::get($options, Hash::get($this->request->data, 'Organisme.bancommune_id'));
//            $this->request->data['Organisme']['adressecomplete'] = Hash::get( $this->request->data, 'Organisme.numvoie' ).' '.$typevoie.' '.Hash::get( $this->request->data, 'Organisme.nomvoie' );
//            $this->request->data['Organisme']['adressecomplete'] = Hash::get($this->request->data, 'Organisme.numvoie') . ' ' . Hash::get($this->request->data, 'Organisme.banadresse') . ' ' . Hash::get($this->request->data, 'Organisme.cp') . ' ' . $ville;
            if (!empty($this->request->data['Organisme']['banadresse'])) {
                $adressecomplete = addslashes( Hash::get($this->request->data, 'Organisme.numvoie') ). ' ' . Hash::get($this->request->data, 'Organisme.banadresse');
            } else {
                $adressecomplete = addslashes( Hash::get($this->request->data, 'Organisme.numvoie') ). ' ' . addslashes( Hash::get($this->request->data, 'Organisme.nomvoie') );
            }
            $this->request->data['Organisme']['adressecomplete'] = $adressecomplete;
            $this->Jsonmsg->init();

			if( strpos($this->request->data['Organisme']['nomvoie'], "'") !== false ) {
				$this->request->data['Organisme']['nomvoie'] = Sanitize::clean($this->request->data['Organisme']['nomvoie'], array('encode', false));
			}
			if( strpos($this->request->data['Organisme']['adressecomplete'], "'") !== false ) {
				$this->request->data['Organisme']['adressecomplete'] = Sanitize::clean($this->request->data['Organisme']['adressecomplete'], array('encode', false));
			}

//            $this->Organisme->create($this->request->data);
            if ($this->Organisme->save($this->request->data)) {

                // Si on décide de diffuser la modif de l'adresse de l'organisme, alors on  enregistre l'adresse de l'organisme pour TOUS les contacts
                if( Configure::read('AdresseOrganisme.DiffuseContact') == 'yes' && $this->request->data['Organisme']['diffuse_contact'] == 'Oui' ) {
                    $this->Organisme->Contact->begin();
                    $this->Organisme->Contact->create();

                    $success = $this->Organisme->Contact->updateAll(
                        array(
//                            'Contact.ban_id' => isset( $this->request->data['Organisme']['ban_id'] ) ? "'".$this->request->data['Organisme']['ban_id']."'" : NULL,
//                            'Contact.bancommune_id' => isset( $this->request->data['Organisme']['bancommune_id'] ) ?  "'".$this->request->data['Organisme']['bancommune_id']."'" : null, // FIXME
//                            'Contact.banadresse' => isset( $this->request->data['Organisme']['banadresse'] ) ?  "'".$this->request->data['Organisme']['banadresse']."'" : null,
							'Contact.nomvoie' => isset( $this->request->data['Organisme']['nomvoie'] ) ? "'".$this->request->data['Organisme']['nomvoie']."'" : null,
							'Contact.numvoie' => isset( $this->request->data['Organisme']['numvoie'] ) ? "'".$this->request->data['Organisme']['numvoie']."'" : null,
							'Contact.compl' => isset( $this->request->data['Organisme']['compl'] ) ? "'".$this->request->data['Organisme']['compl']."'" : null,
							'Contact.cp' => isset( $this->request->data['Organisme']['cp'] ) ? "'".$this->request->data['Organisme']['cp']."'" : null,
							'Contact.adressecomplete' => isset( $this->request->data['Organisme']['adressecomplete'] ) ? "'".$this->request->data['Organisme']['adressecomplete']."'" : null,
							'Contact.adresse' => isset( $this->request->data['Organisme']['adresse'] ) ? "'".$this->request->data['Organisme']['adresse']."'" : "'".@$this->request->data['Organisme']['adressecomplete']."'",
							'Contact.ville' => isset( $this->request->data['Organisme']['ville'] ) ? "'".$this->request->data['Organisme']['ville']."'" : null,
							'Contact.pays' => isset( $this->request->data['Organisme']['pays'] ) ? "'".$this->request->data['Organisme']['pays']."'" : null
						),
						array( 'Contact.organisme_id' => $this->request->data['Organisme']['id'] )
                    );

                    if( $success ) {
                        $this->Organisme->Contact->commit();
                    }
                    else {
                        $this->Organisme->Contact->rollback();
                    }
                }

                // Si on change l'adresse de l'organisme, on répercute la nouvelle adresse quoiqu'il arrive sur le 0Sans contact
                $contactSanscontact = $this->Organisme->Contact->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Contact.organisme_id' => $id,
                            'Contact.name' => ' Sans contact',
                            'Contact.nom' => '0Sans contact'
                        ),
                        'recursive' => -1,
                        'contain' => false
                    )
                );

                $success = $this->Organisme->Contact->updateAll(
                    array(
//                        'Contact.ban_id' => isset( $this->request->data['Organisme']['ban_id'] ) ? $this->request->data['Organisme']['ban_id'] : NULL,
//                        'Contact.bancommune_id' => isset( $this->request->data['Organisme']['bancommune_id'] ) ?  "'".$this->request->data['Organisme']['bancommune_id']."'" : null,
//                        'Contact.banadresse' => isset( $this->request->data['Organisme']['banadresse'] ) ?  "'".$this->request->data['Organisme']['banadresse']."'" : null,
						'Contact.nomvoie' => isset( $this->request->data['Organisme']['nomvoie'] ) ? "'".$this->request->data['Organisme']['nomvoie']."'" : null,
						'Contact.numvoie' => isset( $this->request->data['Organisme']['numvoie'] ) ? "'".$this->request->data['Organisme']['numvoie']."'" : null,
						'Contact.compl' => isset( $this->request->data['Organisme']['compl'] ) ? "'".$this->request->data['Organisme']['compl']."'" : null,
						'Contact.cp' => isset( $this->request->data['Organisme']['cp'] ) ? "'".$this->request->data['Organisme']['cp']."'" : null,
						'Contact.adressecomplete' => isset( $this->request->data['Organisme']['adressecomplete'] ) ? "'".$this->request->data['Organisme']['adressecomplete']."'" : null,
						'Contact.adresse' => isset( $this->request->data['Organisme']['adresse'] ) ? "'".$this->request->data['Organisme']['adresse']."'" : "'".@$this->request->data['Organisme']['adressecomplete']."'",
						'Contact.ville' => isset( $this->request->data['Organisme']['ville'] ) ? "'".$this->request->data['Organisme']['ville']."'" : null,
						'Contact.pays' => isset( $this->request->data['Organisme']['pays'] ) ? "'".$this->request->data['Organisme']['pays']."'" : null
					),
					array( 'Contact.id' => $contactSanscontact['Contact']['id'] )
                ) && $success;

                if( $success ) {
                    $this->Organisme->Contact->MiseajourData();
                    $this->Organisme->Contact->commit();
                }
                else {
                    $this->Organisme->Contact->rollback();
                }


                // on stocke qui à fait quoi quand
//                $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié l'organisme ".$this->request->data['Organisme']['name']." le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié l'organisme ".$this->request->data['Organisme']['name']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else if ($id != null) {
            $querydata = array(
                'conditions' => array(
                    'Organisme.id' => $id
                ),
                'contain' => array(
                    'Activite'
                )
            );
            if (Configure::read('Conf.SAERP')) {
                $querydata['contain'] = array_merge(
                    $querydata['contain'],
                    array(
                        'Operation'
                    )
                );
            }
            $this->request->data = $this->Organisme->find('first', $querydata);
            $this->request->data['Organisme']['name'] = htmlentities( $this->request->data['Organisme']['name'], ENT_QUOTES );
        }
        $this->_setOptions();
        $this->set('id', $id);
    }

    /**
     * Suppression d'une fiche de contact (soft delete - delete)
     *
     * @logical-group Contact
     * @logical-group Fiche de contact
     * @user-profile Admin
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function delete($id = null, $real = true) {
        if ($id != null) {
            $contactInOrganisme = $this->Organisme->Contact->find('count', array(
                'conditions' => array(
                    'organisme_id' => $id
                )
            ));
            $organisme = $this->Organisme->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Organisme.id' => $id
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
            if ($real) {
                $this->Jsonmsg->init(__d('default', 'delete.error'));
                if ($contactInOrganisme == 0) {
                    if ($this->Organisme->delete($id, true)) {
//                        $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé l'organisme ".$organisme['Organisme']['name']." le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                        $this->loadModel('Journalevent');
                        $datasSession = $this->Session->read('Auth.User');
                        $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé l'organisme ".$organisme['Organisme']['name']." le ".date('d/m/Y à H:i:s');
                        $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info');
                        $this->Jsonmsg->valid($this->Organisme->contain());
                        $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                    }
                } else {
                    $this->Jsonmsg->error('Cet organisme n\'est pas vide, pas possible de le supprimer!');
                }
            } else {
                $this->Jsonmsg->init();
                $this->Organisme->id = $id;
                if ($contactInOrganisme == 0) {
                    if ($this->Organisme->saveField('active', false)) {
                        $this->Jsonmsg->valid();
                    }
                } else {
                    $this->Jsonmsg->error('Cet organisme n\'est pas vide, pas possible de le supprimer!');
                }
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Fonction en ajax permettant de remonter les adresses suite à la sélection d'une commune
     * @param type $bancommune_id
     * @return type
     */
    public function getAdresses($bancommune_id) {
		$adresses = [];
		if ($this->RequestHandler->isAjax()) {
			Configure::write('debug', 0);
			$this->autoRender = false;

			$adresses = $this->Organisme->Bancommune->Banadresse->find(
				'all', array(
					'fields' => array(
						'DISTINCT Banadresse.nom_afnor'
					),
					'conditions' => array(
						'Banadresse.bancommune_id' => $bancommune_id,
						array(
							'Banadresse.nom_afnor IS NOT NULL',
							'Banadresse.nom_afnor <>' => '',
						)
					),
					'recursive' => -1,
					'order' => array('Banadresse.nom_afnor')
				)
			);
		}
		return json_encode($adresses);
    }

    /**
     * Récupération de la liste des organismes
     *
     * @logical-group Organisme
     * @user-profile Admin
     *
     * @access public
     * @param integer $addressbook_id identifiant du carnet d'adresse
     * @return void
     */
    public function get_organismes($orgId = null, $clear = null) {
        // On doit pouvoir obtenir les résultats dès le premier accès à la page
        if (!isset($this->request->data['Organisme'])) {
            $this->request->data = Set::merge(
                            $this->Session->read('paginationModel.rechercheOrganisme'), array('Organisme' => array('active' => true, 'id' => $orgId))
            );
            if ($clear != null) {
                $this->request->data = array(
                    'Organisme' => array(
                        'SearchContact' => array(
                            'name' => '',
                            'nom' => '',
                            'prenom' => '',
                            'active' => '1'
                        )
                    )
                );
            }
        }
        if ($orgId != null) {
            $organismes = array();
            if (empty($this->request->data)) {
                $this->Contact->recursive = -1;

                $querydata = array(
                    'conditions' => array(
                        'Contact.organisme_id' => $orgId
                    ),
                    'contain' => false,
                    'limit' => 20
                );
            } else {
                $this->Session->write('paginationModel.rechercheOrganisme', $this->request->data);
                $querydata = $this->Organisme->Contact->search($this->request->data['Organisme'], null, $orgId);
            }
            $this->paginate = $querydata;
            $contacts = $this->paginate($this->Organisme->Contact);

            foreach ($contacts as $i => $contact) {
                $contact['right_edit'] = true;
                $contact['right_delete'] = true;
                if (Configure::read('Conf.SAERP')) {
                    $contact['right_export'] = true;
                }
                $contacts[$i] = $contact;

                // Ajout du nom de l'organisme dans le tableau reprenant les contacts
                if (!empty($contact['Organisme']['name'])) {
                    $contacts[$i]['Contact']['organisme_name'] = $contact['Organisme']['name'];
                } else {
                    $contacts[$i]['Contact']['organisme_name'] = '';
                }
            }

            $this->set('contacts', $contacts);
//            $this->set('addressbook_id', $addressbook_id);
            $this->set('orgId', $orgId);

            $organisme = $this->Organisme->find(
                    'first', array(
                'conditions' => array(
                    'Organisme.id' => $orgId
                ),
                'recursive' => -1,
                'contain' => false
                    )
            );
            $organisme_name = $organisme['Organisme']['name'];
            $addressbook_id = $organisme['Organisme']['addressbook_id'];
            $this->set('organisme_name', $organisme_name);
            $this->set('addressbook_id', $addressbook_id);
            $this->set('organisme_id', $orgId);
            $this->_setOptions();
        }
    }

    /**
     *
     */
    public function getHomonymsOrgs() {
        Configure::write('debug', 0);
        $this->autoRender = false;
        $return = array();
        if ($this->RequestHandler->isAjax() && !empty($this->request->data['Organisme']['name'])) {
            $return = $this->Organisme->getHomonyms($this->request->data['Organisme']['name']);
        }
        echo json_encode($return);
    }

    /**
     * Analyse le fichier importé, et importation de contact et de fiches de contact à partir d'un fichier vcard/csv
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function import($addressbook_id) {
        $this->autoRender = false;
        $return = array();
//var_dump($this->request->data['Import']);
        if (!empty($this->request->data)) {
            $ex = $this->request->data['Import']['format'];
//var_dump($this->request->data);
            //import fichier
            if (!empty($this->request->data['Import']['fileorg']) && empty($this->request->data['Import']['addressbook_id'])) {
//var_dump($this->request->data['Import']);
                $tmpName = $this->request->data['Import']['fileorg']['tmp_name'];
                $filename = TMP . $this->request->data['Import']['fileorg']['name'];
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                if (move_uploaded_file($tmpName, $filename) && $extension == $ex) {
                    $return['situation'] = true;
                    switch ($extension) {
                        case 'csv':
                            $return['filename'] = $filename;
                            $return['message'] = $this->Organisme->Addressbook->importOrganismeCsv($filename);
                            break;
                    }
                } else {
                    $return['situation'] = false;
                    $return['message'] = "L'extension de votre fichier est incorrect. Veuillez sélectionner un autre fichier.";
                }
                //enregistrer dans un carnet exist
            } else if (!empty($this->request->data['Import']['fileorg']) && !empty($this->request->data['Import']['addressbook_id'])) {
//var_dump($this->request->data['Import']);
                $tmpName = $this->request->data['Import']['fileorg']['tmp_name'];
                $filename = TMP . $this->request->data['Import']['fileorg']['name'];
                $extension = pathinfo($filename, PATHINFO_EXTENSION);
                if (move_uploaded_file($tmpName, $filename) && $extension == $ex) {
                    $return['situation'] = true;
                    switch ($extension) {
                        case 'csv':
                            $return['filename'] = $filename;
                            $return['message'] = $this->Organisme->Addressbook->importOrganismeCsv($filename);
                            break;
                    }
                } else {
                    $return['situation'] = false;
                    $return['message'] = "L'extension de votre fichier est incorrect. Veuillez sélectionner un autre fichier.";
                }
                //enregistrer dans un carnet exist
            } else if (!empty($this->request->data['Import']['update_file']) && (!empty($this->request->data['Import']['addressbook_id']) || !empty($addressbook_id) )) {
//var_dump(2);
//var_dump($this->request->data['Import']);die();
                $filename = TMP . $this->request->data['Import']['update_file'];
                $return['filename'] = $filename;
                $return['message'] = $this->Organisme->Addressbook->importContactCsv($filename);
                switch ($ex) {
                    case 'csv':
                        if ($this->Organisme->Addressbook->importOrganismeCsv($this->request->data['Import']['update_file'], $this->request->data['Import']['addressbook_id'], null)) {
                            $return['alert'] = __d('default', 'save.ok');
                        } else {
                            $return['alert'] = __d('default', 'save.error');
                        }
                        break;
                }
                if (is_file($this->request->data['Import']['update_file'])) {
                    unlink($this->request->data['Import']['update_file']);
                }
                //nouveau carnet
            } else if (!empty($this->request->data['Import']['update_file']) && !empty($this->request->data['Import']['carnet_nom'])) {
//var_dump(3);
                $addressbook = array();
                $addressbook = array(
                    'Addressbook' => array(
                        'name' => $this->request->data['Import']['carnet_nom'],
                        'description' => $this->request->data['Import']['carnet_description']
                    )
                );
                $nb = $this->Organisme->Addressbook->find('count', array(
                    'conditions' => array(
                        'Addressbook.name' => $this->request->data['Import']['carnet_nom']
                    )
                ));
                if (!$nb > 0) {
                    $this->Organisme->Addressbook->begin();
                    $this->Organisme->Addressbook->create();
                    $savedAddressbook = $this->Organisme->Addressbook->save($addressbook);
                    $saved = !empty($savedAddressbook);
                    if ($saved) {
                        $this->Organisme->Addressbook->commit();
                        switch ($ex) {
                            case 'csv':
                                if ($this->Organisme->Addressbook->importOrganismeCsv($this->request->data['Import']['update_file'], $savedAddressbook['Addressbook']['id'], null)) {
                                    $return['alert'] = __d('default', 'save.ok');
                                } else {
                                    $return['alert'] = __d('default', 'save.error');
                                }
                                break;
                        }
                        if (is_file($this->request->data['Import']['update_file'])) {
                            unlink($this->request->data['Import']['update_file']);
                        }
                    } else {
                        $this->Organisme->Addressbook->rollback();
                        $return['alert'] = __d('default', 'save.error');
                    }
                }
            }
        }
        return json_encode($return);
    }

    /**
     * chargement la page import_contacts
     *
     * @logical-group Contact
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function import_organismes($update = false) {
        $this->set('addressbooks', $this->Organisme->Addressbook->find('list', array('conditions' => array('Addressbook.active' => 1), 'order' => array('Addressbook.name'))));
    }

    /**
     * Export des carnets d'adresse au format CSV
     *
     * @logical-group Addressbooks
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function export($organisme_id = null) {
        ini_set('memory_limit', '3000M');
//        $this->autoRender = false;


        $name = Hash::get($this->request->params['named'], 'SearchOrganisme__name');
        $addressbook = Hash::get($this->request->params['named'], 'SearchOrganisme__addressbook_id');
        $organismeActif = Hash::get($this->request->params['named'], 'SearchOrganisme__active');

//debug($this->request);
//die();
        $conditions = array();
        $conditions[] = array(
            'Organisme.active' => true
        );
        if (!empty($this->request->params)) {

            if (isset($name) && !empty($name)) {
                $conditions[] = 'Organisme.name ILIKE \'' . $this->Organisme->wildcard('%' . $name . '%') . '\'';
            }

            if (isset($organismeActif) && !empty($organismeActif)) {
                $conditions[] = array('Organisme.active' => $organismeActif);
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => array(
                'Organisme.name ASC'
            ),
            'contain' => array(
                'Activite',
                'Operation',
                'Contact' => array(
                    'Fonction',
                    'Event',
                    'Operation'
                )
            )
        );

        $events = $this->Organisme->Contact->Event->find('list', array('conditions' => array('Event.active' => true)));
        $eventColumns = array();
        foreach ($events as $eventsIds => $eventName) {
            $eventColumns[$eventsIds] = strtoupper($eventName);
        }
        $this->set('eventColumns', $eventColumns);

        if (!empty($organisme_id)) {
            $querydata['conditions'] = array_merge(
                $querydata['conditions'],
                array(
                    'Organisme.id' => $organisme_id
                )
            );

            $results = $this->Organisme->find('first', $querydata);


//            foreach( $results['Organisme'] as $o => $organisme ) {
            $results['Organisme']['activites'] = implode(' - ', Hash::extract($results, 'Activite.{n}.name'));
            $results['Organisme']['operationsorg'] = implode(' - ', Hash::extract($results, 'Operation.{n}.name'));
            foreach ($eventColumns as $eventID => $eventName) {
                foreach ($results['Contact'] as $c => $contact) {
                    if (!empty($contact['Operation'])) {
                        $results['Contact'][$c]['operationscontact'] = implode(' - ', Hash::extract($contact, 'Operation.{n}.name'));
                    }
                    $results['Contact'][$c]['events'][$eventID] = '';
                    foreach ($contact['Event'] as $ev => $event) {
                        if ($eventID == $event['id']) {
                            $results['Contact'][$c]['events'][$eventID] = $eventName;
                        }
                    }
                    if (!empty($contact['observations'])) {
                        $results['Contact'][$c]['observations'] = preg_replace("/[\n\r\"]/", " ", $contact['observations']);
                    }
                }
            }
            $this->set('results', $results);
        } else {
//            $querydata = $this->Organisme->search($this->request->data, null);
//            $resultats_tmp = $this->Organisme->find('all', $querydata);

            if( empty($this->request->params['named']) && !empty($this->request->data) ) {
                $querydata = $this->Organisme->search( $this->request->data, null );
            }
            else {
                $querydata = $this->Organisme->search( Hash::expand( $this->request->params['named'], '__' ), null );
            }
            $results = $this->Organisme->find( 'all', $querydata );

            foreach( $results as $r => $result) {
                $results[$r]['Organisme']['activites'] = implode( ' - ', Hash::extract($result, 'Activite.{n}.name') );
                $results[$r]['Organisme']['operationsorg'] = implode( ' - ', Hash::extract($result, 'Operation.{n}.name') );
//            }
//            $resultats = array();
//            foreach ($resultats_tmp as $i => $results) {
                    foreach ($eventColumns as $eventID => $eventName) {
                        foreach ($result['Contact'] as $c => $contact) {

                        if (!empty($contact['Operation'])) {
                                $result['Contact'][$c]['operationscontact'] = implode(' - ', Hash::extract($contact, 'Operation.{n}.name'));
                            }
                            $result['Contact'][$c]['events'][$eventID] = '';
                            if (isset($contact['Event'])) {
                                foreach ($contact['Event'] as $ev => $event) {
                                    if ($eventID == $event['id']) {
                                        $results[$r]['Contact'][$c]['events'][$eventID] = $eventName;
                                    }
                                }
                            }
                            if (!empty($contact['observations'])) {
                                $results[$r]['Contact'][$c]['observations'] = preg_replace("/[\n\r\"]/", " ", $contact['observations']);
                            }
                        }
                    }
            }
//debug($resultats);
//die();
//            $this->set('results', $resultats);
        }

            $this->set('results', $results);
//        else {
//            $results = $this->Organisme->find( 'first', $querydata);
//        }
//debug($results);
//die();

        $this->_setOptions();
        $this->layout = '';
    }

    /**
     *
     * @param type $organisme_id
     */
    public function get_contact($organisme_id) {
        $querydata = array(
            'fields' => array(
                'Contact.id',
                'Contact.name',
                'Contact.nom',
                'Contact.active',
                'Contact.prenom',
                'Organisme.name'
            ),
            'conditions' => array(
                'Contact.organisme_id' => $organisme_id
            ),
            'joins' => array(
                $this->Organisme->Contact->join('Organisme')
            ),
            'order' => array(
                'Contact.nom' => 'ASC',
                'Contact.prenom' => 'ASC'
            ),
            'contain' => FALSE,
            'limit' => 12
        );
        $this->paginate = $querydata;
        $contacts_tmp = $this->paginate($this->Organisme->Contact);

        $organisme = $this->Organisme->find(
            'first',
            array(
                'fields' => array(
                    'Organisme.name',
                    'Organisme.ville'
                ),
                'conditions' => array(
                    'Organisme.id' => $organisme_id
                ),
                'contain' => false,
                'recursive' => -1
            )
        );
        $organismename = $organisme['Organisme']['name'];
        $organismeville = $organisme['Organisme']['ville'];
        $this->set( compact('organismename', 'organismeville'));


        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
        }
        foreach ($contacts_tmp as $i => $contact) {
            $contacts[$i]['name'] = $contact['Contact']['nom'].' '.$contact['Contact']['prenom'];
            $contacts[$i]['active'] = $contact['Contact']['active'];
            $contacts[$i]['edit'] = '<a href="#" class="editContact" id="contact_' . $contact['Contact']["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" style="color:#5397a7"></i></a>';
            if($isAdmin) {
                $contacts[$i]['delete'] = '<a href="#" class="deleteContact" id="contact_' . $contact['Contact']["id"] . '" title="Supprimer" organismeId ="' . $organisme_id . '"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></a>';
            }
            $contacts[$i]['exportcsv'] = '<a href="#" class="exportContact" id="contact_' . $contact['Contact']["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" style="color:#5397a7"></i></a>';
        }
        if (!empty($contacts)) {
            $this->set('contacts', $contacts);
            $this->set('organisme_id', $organisme_id);
        } else {
            $this->set('contacts', array());
        }
        $this->_setOptions();
    }

    /**
     *
     */
    public function searchOrganismes() {
        if ($this->Session->read('searchAddressbook') != null) {
            $this->paginate = $this->Session->read('searchAddressbook');
        } else {
            $querydata = $this->request->data;
            $this->paginate = $querydata['organismes'];
            $this->Session->write('searchAddressbook', $this->paginate);
        }

        $secondaryDesktops = $this->Session->read('Auth.User.SecondaryDesktops');
        $desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
        $isAdmin = false;
        $groupName = $this->Session->read('Auth.User.Desktop.Profil');
        if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
            $isAdmin = true;
        }
        $organismes_tmp = $this->paginate($this->Organisme);
        $organismes = array();
        foreach ($organismes_tmp as $i => $organisme) {
            $organismes[$i]['name'] = $organisme['Organisme']['name'] . ' ( '.$organisme['Organisme']['ville'].' ) '. '<span style="color:#333333"> (' . $this->Organisme->Contact->getNombreContactByOrganismeId($organisme["Organisme"]["id"]) . ') <span>';
            $organismes[$i]['view'] = '<a href="#" class = "viewOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Visualiser"><i class="fa fa-eye" aria-hidden="true" style="color:#5397a7"></i></a>';
            $organismes[$i]['edit'] = '<a href="#" class="editOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Modifier"><i class="fa fa-pencil" aria-hidden="true" style="color:#5397a7"></i></a>';
            if( $isAdmin ) {
                $organismes[$i]['delete'] = '<a href="#" class="deleteOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Supprimer" addressbookId="' . $organisme["Organisme"]["addressbook_id"] . '"><i class="fa fa-trash" aria-hidden="true" style="color:red"></i></a>';
            }
            $organismes[$i]['exportcsv'] = '<a href="#" class="exportOrganisme" id="organisme_' . $organisme["Organisme"]["id"] . '" title="Exporter"><i class="fa fa-download" aria-hidden="true" style="color:#5397a7"></i></a>';
        }
        if (!empty($organismes)) {
            $this->set('organismes', $organismes);
        } else {
            $this->set('organismes', array());
        }
    }

    /**
     *
     */
    public function search_organismes_courriers() {
        $querydataBase = array();
        $querydataBase['conditions'] = array(
            'Organisme.active' => true
        );
        $querydataBase['contain'] = false;
        $querydataBase['recursive'] = -1;
        $this->set('organismes', array());
        $organismes = array();
        if( !empty($this->request->data ) ) {
            $querydata = $this->Organisme->search( $this->request->data, null );
            $querydata['conditions'][] = array('Organisme.active' => true );
            $organismes_tmp = $this->Organisme->find('all', $querydata );
            $i=0;
            foreach($organismes_tmp as $value){
                $organismes[$i]['id'] = $value['Organisme']['id'];
                $organismes[$i]['name'] = $value['Organisme']['name'];
                $organismes[$i]['ville'] = $value['Organisme']['ville'];
                $i++;
            }
        }
        $this->set('organismes', $organismes);
    }

}
