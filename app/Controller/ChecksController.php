<?php

/**
 * Administration
 *
 * Checks controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 */
class ChecksController extends AppController {

	/**
	 *
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Checks';

    public $components = array(
        'Auth' => array(
            'mapActions' => array(
                'allow' => array('get_test_mail', 'testenvoimail', 'testgedooo', 'getApiKey'),
            )
        ),
        'Conversion'
    );

	/**
	 * Vérifications des pré-requis
	 *
	 * @logical-group Administration
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="/environnement">'.__d('menu', 'Administration').'</a>',
                    __d('menu', 'checkInstall')
                ));

		$verif = array();
		//recupération de la version de cakephp
		$cakeVersion = Configure::version();
		$verif['cake_version'] = $cakeVersion;
		$verif['php_version'] = phpversion();

		exec('cat /etc/issue >> /tmp/ubuntuversion.txt');
		$verif['ubuntults_version'] = str_replace( '\n \l', '', file_get_contents('/tmp/ubuntuversion.txt') );
		unlink('/tmp/ubuntuversion.txt');
		$verif['ubuntutotal_version'] = php_uname();
		$ubuntu = substr($verif['ubuntults_version'], 0, 6 );
		if( $ubuntu == 'Ubuntu') {
			$isubuntu = true;
			$verif['apache_version'] = apache_get_version();
		}
		else {
			$isubuntu = false;
			$verif['ubuntults_version'] = exec("cat /etc/redhat-release" );
			$verif['apache_version'] = exec('rpm -q httpd' );
		}
		$this->set('isubuntu', $isubuntu);


		$this->loadModel('User');
		$verif['postgresql_version'] = $this->_pgVersion($this->User);

		//verification de la présence du patch postgres_bytea pour cake2.0.x (<= 2.0.5)
		$verif['patch_cake2_pg_bytea'] = array('chk' => false, 'patch' => true);
		$tmp_version = explode('.', $cakeVersion);
		$version_patch_unmatch = false;
		if ($tmp_version[0] == "2" && $tmp_version[1] == "0" && (int) $tmp_version[2] < 6) {
			$verif['patch_cake2_pg_bytea']['chk'] = true;
			$file = CAKE . 'Model/Datasource/Database/Postgres.php';
			if (is_file($file)) {
				$handle = fopen($file, "r");
				if ($handle) {
					$content = array();
					$read = false;
					while (($buffer = fgets($handle, 4096)) !== false) {
						if (!$read && preg_match('#function fetchResult#', $buffer)) {
							$read = true;
						}
						if ($read) {
							$content[] = $buffer;
						}
						if ($read && preg_match('#function#', $buffer) && !preg_match('#function fetchResult#', $buffer)) {
							$read = false;
						}
					}
					fclose($handle);
					foreach ($content as $row) {
						if (preg_match('#\$resultRow\[\$table\]\[\$column\] \= is_null\(\$row\[\$index\]\) \? null : stream_get_contents\(\$row\[\$index\]\)\;#', $row)) {
							$verif['patch_cake2_pg_bytea']['patch'] = true;
						}
					}
				}
			}
		} else {
			$version_patch_unmatch = true;
		}
		$this->set('version_patch_unmatch', $version_patch_unmatch);

		//TODO: prevoir un test de génération de document temporaire
		//verification des valeurs dans core.php pour dataacl
		$verif['dataacl'] = array('core' => false);
		if (
				Configure::read('DataAcl.classname') != '' &&
				Configure::read('DataAcl.database') != '' &&
				Configure::read('DataAcl.defaultAccessValue.Daro') != '' &&
				Configure::read('DataAcl.defaultAccessValue.Daco')
		) {
			$verif['dataacl']['core'] = true;
		}


		//Configuration webgfc
		$verif['webgfc'] = array('inc' => false, 'wsdl' => false);
		if (is_file(APP . "Config/webgfc.inc") && is_readable(APP . "Config/webgfc.inc")) {
			$verif['webgfc']['inc'] = true;
		}
		if (is_file(APP . "webroot/files/wsdl/webgfc.wsdl") && is_readable(APP . "webroot/files/wsdl/webgfc.wsdl")) {
			$verif['webgfc']['wsdl'] = true;
		}


		//verification de la presence de l executable pdf2swf
		$verif['swftools'] = array('pdf2swf' => false);
		$result = exec('whereis pdf2swf');
		if (preg_match('#pdf2swf:\s\/(.*\/pdf2swf)#', $result)) {
			$verif['swftools']['pdf2swf'] = true;
		}

		//verification gedooo
		$verif['gedooo'] = array('config' => false, 'up' => false);
		if (
				Configure::read('PROTOCOLE_DL') != '' &&
				Configure::read('USE_GEDOOO') === true &&
				Configure::read('GEDOOO_WSDL') != ''
		) {
			$verif['gedooo']['config'] = true;
		}

		try {
			$client = @new SoapClient(Configure::read('GEDOOO_WSDL'), array('exceptions' => 1));
			$verif['gedooo']['up'] = true;
		} catch (Exception $e) {
			$verif['gedooo']['up'] = false;
		}


		//verification cloudooo
		$verif['cloudooo'] = array('config' => false, 'up' => false);
		if (
				Configure::read('CLOUDOOO_HOST') != '' &&
				Configure::read('CLOUDOOO_PORT') != ''
		) {
			$verif['cloudooo']['config'] = true;
		}

		$cloudoooResult = $this->_socket(Configure::read('CLOUDOOO_HOST'), Configure::read('CLOUDOOO_PORT'));
		$verif['cloudooo']['up'] = $cloudoooResult['success'];


        // affichage du délai de connexion
        $verif['delai_session'] = $this->timeout();


        // Liste des modules PHP nécessaires, notamment pour le CAS
        $moduleNeeded = array("Mail.php","Mail/mime.php","CAS.php","XML/RPC2/Client.php");
		$modules = array();
		foreach($moduleNeeded as $module){
			$modules[$module] = @ include_once($module);
		}
        $verif['modules'] = $modules;

        // Liste des extensions
        $extensionNeeded = array("curl","openssl","imap","apcu","ssh2","pdo","zip","phar","ldap","fileinfo");
		$extensions = array();
		foreach($extensionNeeded as $extension){
			$extensions[$extension] = extension_loaded($extension);
		}
        $verif['extensions'] = $extensions;

		//verification de la presence de l executable tesseract
		$verif['ocr'] = array(
			'tesseract' => false,
			'gs' => false,
		);
		$result = exec('whereis tesseract');
		if (preg_match('#tesseract:\s\/(.*\/tesseract)#', $result)) {
			$verif['ocr']['tesseract'] = true;
		}
		$result2 = exec('whereis gs');
		if (preg_match('#gs:\s\/(.*\/gs)#', $result2)) {
			$verif['ocr']['gs'] = true;
		}

		// Vérification de l'existence du répertoire pour stocker les fichiers sur le disque
		$verif['data_workspace'] = false;

		$this->loadModel('Collectivite');
		$this->Collectivite->setDataSource('default');
		$colls = $this->Collectivite->find('all', array('contain' => false, 'recursive' => -1));
		foreach( $colls as $coll ) {
			if( !empty($coll['Collectivite']['scan_local_path']) ) {
				$isdataWorkspaceDir[$coll['Collectivite']['scan_local_path']] = is_dir($coll['Collectivite']['scan_local_path']) && is_writable($coll['Collectivite']['scan_local_path']);
			}
		}
		$verif['data_workspace'] = $isdataWorkspaceDir;


		//verification de la presence de l executable wkhtmltopdf
		$verif['htmltopdf'] = array('wkhtmltopdf' => false);
		$result = exec('whereis wkhtmltopdf');
		if (preg_match('#wkhtmltopdf:\s\/(.*\/wkhtmltopdf)#', $result)) {
			$verif['htmltopdf']['wkhtmltopdf'] = true;
		}


		//verification de la presence de l executable pdftk
		$verif['pdftk'] = array('pdftk' => false);
		$result = exec('whereis pdftk');
		if (preg_match('#pdftk:\s\/(.*\/pdftk)#', $result)) {
			$verif['pdftk']['pdftk'] = true;
		}

		$this->set('verif', $verif);
	}

	/**
	 *
	 * Vérification de la disponibilité d'un serveur via les sockets
	 *
	 * @access private
	 * @param string $hostname
	 * @param integer $port
	 * @param string $message
	 * @return array
	 */
	private function _socket($hostname, $port, $message = "La machine distante n' est pas accessible (%s)") {
		$timeout = 10;
		Set_Time_Limit(0); //Time for script to run .. not sure how it works with 0 but you need it
// Ignore_User_Abort(True); //this will force the script running at the end
		$handle = @fsockopen($hostname, $port, $errno, $errstr, $timeout);
		$result = array(
			'success' => !empty($handle)
		);
		if (!$result['success']) {
			$result['message'] = sprintf($message, "{$errno}: {$errstr}");
		} else {
			fclose($handle);
		}
		return $result;
	}

	/**
	 *
	 * Récupération de la version de Postgres
	 *
	 * @access private
	 * @param Model $model
	 * @param boolean $full
	 * @return string
	 */
	private function _pgVersion(&$model, $full = false) {
		$psqlVersion = $model->getDataSource($model->useDbConfig)->query('SELECT version();');
		$psqlVersion = Set::classicExtract($psqlVersion, '0.0.version');
		if (!$full) {
			$psqlVersion = preg_replace('/.*PostgreSQL ([^ ]+) .*$/', '\1', $psqlVersion);
		}
		return $psqlVersion;
	}



    	/**
	 * Retourne le nombre de secondes avant l'expiration de la session (basé sur
	 * la configuration du fichier app/Config/core.php).
	 *
	 * @return integer
	 */
	public function readTimeout() {
		if( Configure::read( 'Session.save' ) == 'cake' ) {
			App::uses( 'CakeSession', 'Model/Datasource' );
			return ( CakeSession::$sessionTime - CakeSession::$time );
		}
		else {
			return ini_get( 'session.gc_maxlifetime' );
		}
	}

    /**
	 * Formate un timestamp (nombre entier de secondes) au format hh:mm:ss.
	 *
	 * @see http://snipplr.com/view.php?codeview&id=4688
	 *
	 * @param mixed $sec
	 * @param boolean $padHours
	 * @return string
	 */
	public function sec2hms( $sec, $padHours = false ) {
		$hms = "";

		// there are 3600 seconds in an hour, so if we
		// divide total seconds by 3600 and throw away
		// the remainder, we've got the number of hours
		$hours = intval( intval( $sec ) / 3600 );

		// add to $hms, with a leading 0 if asked for
		$hms .= ($padHours) ? str_pad( $hours, 2, "0", STR_PAD_LEFT ).'h' : $hours.'min';

		// dividing the total seconds by 60 will give us
		// the number of minutes, but we're interested in
		// minutes past the hour: to get that, we need to
		// divide by 60 again and keep the remainder
		$minutes = intval( ($sec / 60) % 60 );

		// then add to $hms (with a leading 0 if needed)
		$hms .= str_pad( $minutes, 2, "0", STR_PAD_LEFT ).'min';

		// seconds are simple - just divide the total
		// seconds by 60 and keep the remainder
		$seconds = intval( $sec % 60 );

		// add to $hms, again with a leading 0 if needed
		$hms .= str_pad( $seconds, 2, "0", STR_PAD_LEFT ).'s';

		return $hms;
	}

    /**
    * Retourne la vérification du timeout, avec en message la configuration
    * utilisée.
    *
    * @fixme La fonction readTimeout n'est pas dans le plugin
    *
    * @return array
    */
   public function timeout() {
           $value = $this->readTimeout();
           $message = null;

           if( Configure::read( 'Session.save' ) == 'php' ) {
                   $message = '<code>session.gc_maxlifetime</code> dans le <code>php.ini</code> (valeur actuelle: <em>'.ini_get( 'session.gc_maxlifetime' ).'</em> secondes)';
           }
           else if( Configure::read( 'Session.save' ) == 'cake' ) {
                   $message = "<code>Configure::write( 'Session.timeout', '".Configure::read( 'Session.timeout' )."' )</code> dans <code>app/config/core.php</code><br/>";
                   $message .= "<code>Configure::write( 'Security.level', '".Configure::read( 'Security.level' )."' )</code> dans <code>app/config/core.php</code>";
           }
           return array(
                   'value' => $this->sec2hms( $value, true ),
                   'message' => $message,
           );
   }

   public function get_test_mail() {
       $this->autoRender = true;
   }

    /**
    * Retourne la vérification de l'envoi d'un mail générique
    * utilisée.
    *
    *
    * @return array
    */
   public function testenvoimail() {
        $this->autoRender = false;
        App::uses('CakeEmail', 'Network/Email');
        $config_mail = 'default';
        $this->Email = new CakeEmail($config_mail);

        if( !empty($this->request->data['Environnement']['mail'])) {
            $sujet = "Ceci est un mail de test";

            $this->Email->addTo($this->request->data['Environnement']['mail']);
            $this->Email->subject($sujet);
            $content = 'TEST';

            $folder = WWW_ROOT . "files/webdav/modeles/texte_reponse.odt";
            $this->Email->attachments( array(
                    'test.odt' => array(
                            'file' => $folder
                    )
            ));

			$this->Email->emailFormat(CakeEmail::MESSAGE_HTML);

            $success = $this->Email->send($content);

             if ($success) {
                $this->Session->setFlash(__('Le mail de test a été envoyé'), 'growl');
            } else {
                $this->Session->setFlash(__("Un problème est survenu lors de l'envoi du mail"), 'growl', array('type' => 'danger'));
            }
        }

        return $this->redirect(array('controller' => 'environnement', 'action' => 'index'));
   }

    /**
    * Retourne la vérification de l'envoi d'un mail générique
    * utilisée.
    *
    *
    * @return array
    */
   public function testgedooo() {
        $this->autoRender = false;
        // initialisations
        global $appli_path, $fichier_conf;
        $convTypes = array('UNOCONV'=>'Conversion Unoconv', 'CLOUDOOO'=> 'Conversion Unoconv');




        // exécutable du convertisseur
		$repModels = APP.WEBROOT_DIR.DS.'files'.DS;
		$modelFileName = 'test_signature.odt';
		require_once 'XML/RPC2/Client.php';
		// initialisations
		$ret = array();
		$options = array(
			'uglyStructHack' => true
		);

		$documentGenerated = file_get_contents($repModels.$modelFileName);
		$document_content = $this->Conversion->convertirFlux($documentGenerated, 'odt', 'pdf');

		$document_name = str_replace('odt', 'pdf', $modelFileName);
		Configure::write('debug', 0);
		header("Content-type: application/pdf");
		header("Content-Disposition: attachment; filename=\"" . $document_name . "\""); // use 'attachment' to force a file download

		print $document_content;
		exit();
   }

	/**
	 * Fonction permettant de générer la clé d'API globale
	 * @throws Exception
	 */
	public function getApiKey() {
		$this->autoRender = true;
		$this->loadModel('User');
		$this->User->setDataSource('default');
		$user = $this->User->find('first');
		$this->set('user', $user);
	}


	/**
	 * Fonction permettant de générer la clé d'API globale
	 * @throws Exception
	 */
	public function genApiKey($userId) {

		$this->Jsonmsg->init();
		$this->autoRender = false;
		$this->loadModel('User');
		$this->User->setDataSource('default');
		$this->User->id = $userId;
		$res = $this->User->saveField('apitoken', bin2hex(random_bytes(30)));
		if($res){
			$jsonMessage = $res['User']['apitoken'] . "<hr /> La clé d'API globale a bien été modifiée " ;
			$this->Jsonmsg->valid( $jsonMessage );
		}else{
			$this->Jsonmsg->error();
		}
		$this->Jsonmsg->send();
	}

}

?>
