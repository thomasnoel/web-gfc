<?php
/**
 * Flux
 *
 * TypeApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

class TypeApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Type', 'Collectivite'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$types = $this->getTypes( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($types),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($typeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$type = $this->getType($typeId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($type),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*$this->request->data = '{
			"name": "Type principal"
		}';*/

		$type = json_decode($this->request->input(), true);
		$res = $this->Type->save($type);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Type->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['typeId' => $res['Type']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($typeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getType($typeId);
			$this->Type->id = $typeId;
			$this->Type->delete();
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'status' => 200,
			'body' => json_encode(['message' => 'Type supprimé']),
			'type' => 'application/json'
		]);
	}


	public function update($typeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getType($typeId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$type = $this->sanitizeType($this->request->input());
		$type['id'] = $typeId;
		$res = $this->Type->save($type);
		if (!$res) {
			return $this->formatCakePhpValidationMessages($this->Type->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['typeId' => $res['Type']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	private function sanitizeType($jsonData)
	{
		$authorizedFields = ['name'];
		return array_intersect_key(json_decode($jsonData, true) ?? [], array_flip($authorizedFields));
	}


	private function getTypes( $limit, $offset )
	{
		$conditions = array(
			'fields' => ['id', 'name'],
			'limit' => $limit,
			'offset' => $offset
		);
		$types = $this->Type->find('all', $conditions);

		return Hash::extract($types, "{n}.Type");
	}


	private function getType($typeId)
	{
		$type = $this->Type->find('first', [
			'fields' => ['Type.id', 'Type.name'],
			'conditions' => ['Type.id' => $typeId],
			'contain' => ['Soustype' => [
				'fields' => ['Soustype.id', 'Soustype.name']
			]]
		]);

		if (empty($type)) {
			throw new NotFoundException('Type non trouvé / inexistant');
		}

		$formattedType = Hash::extract($type, "Type");

		foreach ($type['Soustype'] as &$soustype) {
			unset($soustype['Soustype']);
		}

		$formattedType['soustypes'] = $type['Soustype'] ?? [];

		return $formattedType;
	}


}
