<?php

use Libriciel\LsMessageWrapper\Sms;


/**
 * Sms
 *
 * Sms controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class SmsController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Sms';
	public $uses = array('Sms');

	public $components = array('LsMessage');


	/**
	 *
	 */
	public function beforeFilter() {
		parent::beforeFilter();
	}

	/**
	 * Ajout d'un e opération
	 *
	 * @logical-group Marches
	 * @user-profile Admin
	 *
	 * @access public
	 * @return void
	 */
	public function sendSms($fluxId) {
		$conn = CakeSession::read('Auth.User.connName');
		$success = false;
		if (!empty($this->request->data)) {
			$sms = $this->request->data;
			$this->loadModel('Connecteur');
			$hasSms = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%LsMessage%',
						'Connecteur.use_sms' => true
					),
					'contain' => false
				)
			);
			if (!empty($hasSms)) {
				try {
					$smsToSend = new Sms('webgfc', $sms['Sms']['numero'], $sms['Sms']['message'], $hasSms['Connecteur']['sms_expediteur']);
					$this->LsMessage->sendOne($smsToSend);
				} catch (Exception $e) {
					$this->log($e->getMessage());
				}
				$success = true;
			}

			$this->Jsonmsg->init();
			if ($success) {
				$this->Sms->begin();
				$this->Sms->create($sms);
				if ($this->Sms->save()) {
					$this->Sms->commit();
					$this->Jsonmsg->valid();
				} else {
					$this->Sms->rollback();
				}
			}
			if( !empty( $conn ) ) {
				$this->Jsonmsg->send();
			}
		}

		$this->set('fluxId', $fluxId );
	}


}

?>
