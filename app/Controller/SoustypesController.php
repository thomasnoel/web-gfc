<?php

/**
 * Sous-types
 *
 * Soustypes controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
App::uses('Folder', 'Utility');
class SoustypesController extends AppController {

    /**
     * Component name
     *
     * @access public
     * @var string
     */
    public $name = 'Soustypes';

    /**
     * Controller uses
     *
     * @access public
     * @var array
     */
    public $uses = array('Soustype');

	/**
	 * Controller components
	 *
	 * @var array
	 */
	public $components = array('NewPastell');

	/**
	 * @var repository utilisée pour la récupération de la liste des sous-types IP
	 */
	public $repository;
    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('getCircuit', 'getInformation', 'export');
    }

    /**
     * Gestion des types et sous-types (interface graphique)
     *
     * @logical-group Sous-types
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function index() {

        $this->set('ariane', array(
            '<a href="environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionTypesSoustypes', true)
        ));
        $this->set('listeTypes', $this->Soustype->Type->find('list', array( 'contain' => false, 'recursive' => -1, 'order' => 'Type.name')));
        $this->set('listeSoustypes', $this->Soustype->find('list', array('contain' => false, 'recursive' => -1, 'order' => 'Soustype.name')));
    }

    /**
     * Récupération de la liste imbriquée des types et sous-types (ajax)
     *
     * @logical-group Sous-types
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getTypesSoustypes() {

        if(!empty($this->request->data) ) {
            $querydata = $this->Soustype->Type->search($this->request->data);
        }
        else {
            $querydata = array(
                'contain' => array(
                    $this->Soustype->alias => array(
                        'order' => 'Soustype.name',
                        'conditions' => array('Soustype.active' => true)
                    )
                ),
                'conditions' => array('Type.active' => true),
                'order' => 'Type.name'
            );
        }
        $types_tmp = $this->Soustype->Type->find("threaded", $querydata );

        foreach($types_tmp as $t => $type) {
            foreach( $type['Soustype'] as $s => $soustype ) {
                $types_tmp[$t]['children'][$s]['Soustype'] = $soustype;
                $types_tmp[$t]['children'][$s]['Type'] = $type['Type'];
                $types_tmp[$t]['children'][$s]['Soustype']['hasParent'] = isset( $type['Soustype'][0] ) ?  true : false;
            }
            $types_tmp[$t]['Type']['hasParent'] = isset( $type['Soustype'][0] ) ?  true : false;
        }

        $types = array();
        foreach ($types_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $types[] = $item;
        }
        $this->set('types', $types);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Edition des informations d'un soustype (ajax)
     *
     * @logical-group Sous-types
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du sous-type
     * @return void
     */
    public function setInfos($id = null) {
        if ($id != null) {
            $this->set('id', $id);
        }
    }

    /**
     * Récupération de la liste des modèles d'accusé de réception (ajax)
     *
     * @logical-group Sous-types
     * @logical-group Modèles d'accusé de réception
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du sous-type
     * @return void
     */
    public function setArmodels($id = null) {
        if ($id != null) {
            $this->set('soustype_id', $id);
            $qd = array(
                'order' => array(
                    'Armodel.name'
                ),
                'conditions' => array(
                    'Armodel.soustype_id' => $id
                ),
                'fields' => $this->Soustype->Armodel->getLightFields()
            );
            $armodels = $this->Soustype->Armodel->find('all', $qd);
            $this->set('armodels', $armodels);
        }
    }

    /**
     * Récupération de la liste des modèles de réponse (ajax)
     *
     * @logical-group Sous-types
     * @logical-group Modèles de réponses
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du sous-type
     * @return void
     */
    public function setRmodels($id = null) {
        if ($id != null) {
			$this->set('soustypeId', $id);
            $qd = array(
                'order' => array(
                    'Rmodel.name'
                ),
                'conditions' => array(
                    'Rmodel.soustype_id' => $id
                ),
                'fields' => $this->Soustype->Rmodel->getLightFields()
            );
            $rmodels = $this->Soustype->Rmodel->find('all', $qd);
            $this->set('rmodels', $rmodels);

            $qdGabarit = array(
                'order' => array(
                    'Rmodelgabarit.name'
                ),
                'conditions' => array(
                    'Rmodelgabarit.soustype_id' => $id
                ),
                'fields' => $this->Soustype->Rmodelgabarit->getLightFields()
            );
            $rmodelsgabarits = $this->Soustype->Rmodelgabarit->find('all', $qdGabarit);
            $this->set('rmodelsgabarits', $rmodelsgabarits);
        }
    }

    /**
     *
     * @param type $id
     */
    //TODO: supprimer les fonctions commentées
//	public function view($id) {
//		$soustype = $this->Soustype->find(
//				'first', array(
//			'contain' => array(
//				'Type',
//				'Circuit',
//				'Courrier',
//				'Model'
//			),
//			'conditions' => array('Soustype.id' => $id)
//				)
//		);
//
//		if (empty($soustype)) {
//			throw new NotFoundException();
//		}
//
//		$this->set(compact('soustype'));
//	}

    /**
     * Ajout d'un sous-type
     *
     * @logical-group Sous-types
     * @user-profile Admin
     *
     * @access public
     * @param integer $type_id identifiant du type
     * @return void
     */
    public function add($type_id = null) {

        // Récupération de la liste des métadonnées associées aux sous type en question
        // TODO: donne une valeur obligatoire pour une métadonnée selon le type/sous-type
        $listMeta = $this->Soustype->Metadonnee->find(
                'list', array(
            'order' => array(
                'Metadonnee.name DESC'
            )
                )
        );
        $this->set('listMeta', $listMeta);
        if (!empty($this->request->data)) {

            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );
            //debut des transactions
            $this->Soustype->begin();
            //preparation du tableau des verifications
            $success = array(
                'Soustype' => false,
                'Soustypecible' => false
            );
            //enregistrement du soustype
            $stype = $this->Soustype->create($this->request->data);
            if ($this->Soustype->save($stype)) {
                $success['Soustype'] = true;
            }
            if( !empty( $this->request->data['Soustype']['Soustypecible'])) {
                foreach($this->request->data['Soustype']['Soustypecible'] as $key => $soustypecibleId) {
                    $soustypesCibles[$key]['soustype_id'] = $this->Soustype->id;
                    $soustypesCibles[$key]['soustypecible_id'] = $soustypecibleId;
                }
                $success['Soustypecible'] = $this->Soustype->SoustypeSoustypecible->saveAll($soustypesCibles);
            }
            else {
                $success['Soustypecible'] = true;
            }


            //on effectue les transactions si tout est bon
            if (!in_array(false, $success, true)) {
                $this->Soustype->commit();
                $json['success'] = true;
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Soustype->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        } else {
            $this->set('type_id', $type_id);
            $this->set('types', $this->Soustype->Type->find('list', array('order' => array('Type.name'))));
            $this->set('circuits', $this->Soustype->Circuit->find('list', array('order' => array('Circuit.nom'), 'conditions' => array('Circuit.actif' => 1))));

            $types = $this->Soustype->Type->find(
                    'all', array(
                'contain' => array(
                    'Soustype' => array(
                        'order' => array('Soustype.name ASC'),
                        'conditions' => array('Soustype.active' => true)
                    )
                ),
                'order' => array('Type.name ASC')
                    )
            );

            $soustypesList = array();
            foreach ($types as $type) {
                if (!empty($type['Soustype'])) {
                    foreach ($type['Soustype'] as $soustype) {
                        $soustypesList[$type['Type']['name']][$soustype['id']] = $soustype['name'];
                    }
                }
            }
            $this->set('soustypesList', $soustypesList);

			$this->loadModel('Connecteur');
			$hasPastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell' => true
					),
					'contain' => false
				)
			);

			$this->set('hasPastellActif', $hasPastellActif);


			$listSoustypeIp = array();
			if( !empty($hasPastellActif) && $hasPastellActif['Connecteur']['use_pastell'] ) {

				if( empty($this->Session->read('Pastell.listSoustypeIp')) ) {
					$id_entity = $hasPastellActif['Connecteur']['id_entity'];
					// Récup des sous-types dispos
					$PastellComponent = new NewPastellComponent();
					$listSoustypeIp = $PastellComponent->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
					$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
				}
				$listSoustypeIp = $this->Session->read('Pastell.listSoustypeIp');
				$listSoustypeIp = $listSoustypeIp['soustype'];
			}
			$this->set('listSoustypeIp', $listSoustypeIp);
        }
    }

    /**
     * Edition d'un soustype (interface graphique)
     *
     * @logical-group Sous-types
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du sous-type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id = null) {


        $metas = $this->Soustype->Metadonnee->find('list', array(
            'order' => array(
                'Metadonnee.name DESC'
            )
        ));
        $this->set('metas', $metas);
        // Récupération de la liste des métadonnées associées aux sous type en question
        // TODO: donne une valeur obligatoire pour une métadonnée selon le type/sous-type
        $listMetasSelected = $this->Soustype->MetadonneeSoustype->find('all', array(
            'conditions' => array(
                'MetadonneeSoustype.soustype_id' => $id
            ),
            'contain' => array(
                'Metadonnee'
            ),
            'order' => array(
                'Metadonnee.name DESC'
            )
        ));
		$listMetaSelected = array();
        if (!empty($listMetasSelected)) {
			$listMetaSelected = array_combine(Hash::extract($listMetasSelected, '{n}.Metadonnee.id'), Hash::extract($listMetasSelected, '{n}.Metadonnee.name'));
        }
        $this->set('listMeta', $metas);
        $this->set('listMetaSelected', $listMetaSelected);

// debug($listMetaValue);

        $stype = $this->Soustype->find('first', array('recursive' => -1, 'conditions' => array('Soustype.id' => $id), 'fields' => array('Soustype.id', 'Soustype.entrant')));
        if (empty($stype)) {
            throw new NotFoundException();
        }
        $this->set('id', $id);
        $this->set('sens', $stype['Soustype']['entrant']);

        // On regarde si le soustype sélectionné possède une entrée dans la t ble de liaison
        // cele signifierait qu'il possède des sous-types cibles de référence
        $reponse = false;
        $stypeReponse = $this->Soustype->SoustypeSoustypecible->find(
            'first',
            array(
                'conditions' => array(
                    'SoustypeSoustypecible.soustype_id' => $id
                ),
                'contain ' => false
            )
        );
        if (!empty($stypeReponse)) {
            $reponse = true;
        }
        $this->set('response', $reponse);
//        $this->set('response', !empty($stype['Soustype']['parent_id']));

        if (!empty($this->request->data)) {

            $json = array(
                'success' => false,
                'message' => __d('default', 'save.error')
            );
            $this->Soustype->begin();
            $messages = array(
                'soustype' => false,
            );
//debug($this->request->data);
//die();
            if ($this->Soustype->save($this->request->data)) {
                $messages['soustype'] = true;

                if (!empty($this->request->data['Metadonnee']['obligatoire'])) {
                    $this->Soustype->MetadonneeSoustype->updateAll(
                            array(
                        'MetadonneeSoustype.obligatoire' => 1
                            ), array(
                        'MetadonneeSoustype.soustype_id' => $id,
                        'MetadonneeSoustype.metadonnee_id' => $this->request->data['Metadonnee']['obligatoire']
                            )
                    );
                }

                // on regarde les anciens enregistrements entre les sous-types et les sous-types référence
                $records = $this->Soustype->SoustypeSoustypecible->find(
                    'list',
                    array(
                        'fields' => array("SoustypeSoustypecible.id", "SoustypeSoustypecible.soustypecible_id"),
                        'conditions' => array(
                            "SoustypeSoustypecible.soustype_id" => $id
                        ),
                        'contain' => false
                    )
                );
                if( !empty( $records) ) {
//                        $oldrecordsids = array_values($records);
                    $oldrecordsids = Hash::extract($records, "{n}");
                    // on regarde les nouveautés
                    if (!empty($this->request->data['Soustype']['Soustypecible'])) {
                        $nouveauxids = Hash::extract($this->request->data, "Soustype.Soustypecible.{n}");
                    }
                    else {
                        $nouveauxids = array();
                    }
                    // si des différences existent on note les ids enlevés, les liens coupés entre les sous-types
                    $idsenmoins = array_diff($oldrecordsids, $nouveauxids);
                    // si des différences existent on note les ids ajoutés, les liens associés entre les sous-types
                    $idsenplus = array_diff($nouveauxids, $oldrecordsids);

                    // on supprime les liaisons entre sous-types qui ne sont plus liés à d'autres sous-types
                    if (!empty($idsenmoins)) {
                        $this->Soustype->SoustypeSoustypecible->deleteAll(
                            array(
                                'SoustypeSoustypecible.soustype_id' => $id,
                                'SoustypeSoustypecible.soustypecible_id' => $idsenmoins
                            )
                        );
                    }
                    // on ajoute les sous-types nouvellement associés
                    if(!empty($idsenplus)) {
                         if( !empty( $this->request->data['Soustype']['Soustypecible'])) {
                            foreach($idsenplus as $key => $soustypecibleId) {
                                $soustypesEnPlus[$key]['soustype_id'] = $id;
                                $soustypesEnPlus[$key]['soustypecible_id'] = $soustypecibleId;
                            }
                            $this->Soustype->SoustypeSoustypecible->saveAll($soustypesEnPlus);
                        }
                    }
                }
                else {
                    if( !empty( $this->request->data['Soustype']['Soustypecible'])) {
                        foreach($this->request->data['Soustype']['Soustypecible'] as $key => $soustypecibleId) {
                            $soustypesCibles[$key]['soustype_id'] = $id;
                            $soustypesCibles[$key]['soustypecible_id'] = $soustypecibleId;
                        }
                        $this->Soustype->SoustypeSoustypecible->saveAll($soustypesCibles);
                    }
                }
            }

            if (!in_array(false, $messages, true)) {
                $this->Soustype->commit();
                $json['success'] = true;
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Soustype->rollback();
                $json['message'] = 'Préparation de la modification du soustype : ' . ($messages['soustype'] ? 'ok' : 'erreur');
            }
            $json['message'] = 'Onglet informations<br />' . $json['message'];
            $this->Jsonmsg->sendJsonResponse($json);
        } else {
            $queryData = array(
                'contain' => array(
                    $this->Soustype->Type->alias,
                    $this->Soustype->Armodel->alias,
                    $this->Soustype->Circuit->alias,
                    $this->Soustype->Formatreponse->alias,
                    $this->Soustype->Metadonnee->alias
                ),
                'conditions' => array('Soustype.id' => $id)
            );
            $this->request->data = $this->Soustype->find('first', $queryData);
            $this->set('models', $this->Soustype->Armodel->find('all', array('order' => array('Armodel.name'))));
            $this->set('circuits', $this->Soustype->Circuit->find('list', array('order' => array('Circuit.nom'), /*'conditions' => array('Circuit.actif' => 1)*/)));
            $this->set('types', $this->Soustype->Type->find('list', array('order' => array('Type.name'), 'conditions' => array('Type.active' => true))));
            $types = $this->Soustype->Type->find(
                    'all', array(
                'contain' => array(
                    'Soustype' => array(
                        'order' => array('Soustype.name ASC'),
						'conditions' => array('Soustype.active' => true)
                    )
                ),
                'order' => array('Type.name ASC')
                    )
            );

            $soustypesList = array();
            foreach ($types as $type) {
                if (!empty($type['Soustype'])) {
                    foreach ($type['Soustype'] as $soustype) {
                        $soustypesList[$type['Type']['name']][$soustype['id']] = $soustype['name'];
                    }
                }
            }
            $this->set('soustypesList', $soustypesList);

            $listSoustype = $this->Soustype->SoustypeSoustypecible->find('all', array(
                'conditions' => array(
                    'SoustypeSoustypecible.soustype_id' => $id
                ),
                'contain' => false,
            ));
            $listSoustypeValue = array();
            if (!empty($listSoustype)) {
                $listSoustypeValue = Hash::extract($listSoustype, '{n}.SoustypeSoustypecible.soustypecible_id');
            }
            $this->set('listSoustype', $listSoustypeValue);


            if (!empty($this->request->data['Metadonnee'])) {
                $this->request->data['Metadonnee']['obligatoire'] = array_combine(
                        Hash::extract($this->request->data, 'Metadonnee.{n}.MetadonneeSoustype.metadonnee_id'), Hash::extract($this->request->data, 'Metadonnee.{n}.MetadonneeSoustype.obligatoire')
                );
            }

			$this->loadModel('Connecteur');
			$hasPastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell' => true
					),
					'contain' => false
				)
			);

			$this->set('hasPastellActif', $hasPastellActif);

			$listSoustypeIp = array();
			if( !empty($hasPastellActif) && $hasPastellActif['Connecteur']['use_pastell'] ) {
				if( empty($this->Session->read('Pastell.listSoustypeIp')) ) {
					$id_entity = $hasPastellActif['Connecteur']['id_entity'];
					// Récup des sous-types dispos
					$PastellComponent = new NewPastellComponent();
					$listSoustypeIp = $PastellComponent->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
					$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
				}
				$listSoustypeIp = $this->Session->read('Pastell.listSoustypeIp');
				$listSoustypeIp = $listSoustypeIp['soustype'];
			}
			$this->set('listSoustypeIp', $listSoustypeIp);
        }


    }

    /**
     * Suppression d'un sous-type (delete)
     *
     * @logical-group Sous-types
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du sous-types
     * @throws NotFoundException
     * @return void
     */
    public function delete($id = null) {
        $stype = $this->Soustype->find('first', array('conditions' => array('Soustype.id' => $id)));
        $compteur_id = $stype['Soustype']['compteur_id'];
        $sequence_id = $stype['Compteur']['sequence_id'];
        if (empty($stype)) {
            throw new NotFoundException();
        }
        $this->Soustype->begin();
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $used = false;
        $ref = $this->Soustype->Courrier->find('first', array('conditions' => array('Courrier.soustype_id' => $stype['Soustype']['id'])));

        if (!empty($ref)) {
            $used = true;
        }
        if (!$used) {
            if ($this->Soustype->delete($id, true)) {
                $this->Soustype->Compteur->delete($compteur_id, true);
                $this->Soustype->Compteur->Sequence->delete($sequence_id, true);
                $this->Soustype->commit();
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            }
        } else {
            $this->Soustype->rollback();
            $this->Jsonmsg->init(__d('soustype', 'Soustype.delete.notAllowed'));
        }
        $this->Jsonmsg->send();
//		$this->Jsonmsg->init(__d('default', 'delete.error'));
//		$messages = array(
//			'soustype' => false,
//			'rights' => false,
//			'daco' => false
//		);
//		$this->Soustype->begin();
//		if ($this->Soustype->delete($id)) {
//			$messages['soustype'] = true;
//			$daco = $this->Droits->getDaco(array('model' => 'Soustype', 'foreign_key' => $id));
////      $this->_logDebug($daco);
//			if ($this->Droits->deleteDacoRights($daco)) {
//				$messages['rights'] = true;
//				if ($this->Droits->deleteDaco($daco)) {
//					$messages['daco'] = true;
//				}
//			}
//		}
//
//		if (!in_array(false, $messages, true)) {
//			$this->Soustype->commit();
//			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
//		} else {
//			$this->json['message'] = 'Préparation de la suppression du soustype : ' . ($messages['soustype'] ? 'ok' : 'erreur');
//			$this->json['message'] .= '<br />Préparation de la suppression des droits du soustype : ' . ($messages['rights'] ? 'ok' : 'erreur');
//			$this->json['message'] .= '<br />Préparation de la suppression du daco correspondant au soustype : ' . ($messages['daco'] ? 'ok' : 'erreur');
//			$this->Soustype->rollback();
//		}
//		$this->Jsonmsg->send();
    }

    /**
     * Récupération de la simulation de référence pour un nouveau flux (ajax)
     *
     * @logical-group Sous-type
     *
     * @logical-group Flux
     * @logical-group Création d'un flux
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du sous-type
     * @return void
     */
    public function getRefSimul($id) {
        $this->set('refSimul', $this->Soustype->getRef($id, true));
    }

    /**
     * Récupération des informations du circuit attaché au sous-type
     *
     */
    public function getCircuit($id) {
        $soustype = $this->Soustype->find(
                'first', array(
            'conditions' => array(
                'Soustype.id' => $id
            ),
            'recursive' => -1,
            'contain' => false
                )
        );
        $this->set('soustype', $soustype);


        if (!empty($soustype)) {
            //definition de l identifiant du circuit
            $circuitId = $soustype['Soustype']['circuit_id'];

            if (!empty($circuitId)) {
                //recuperation de la structure initiale du circuit
                //recuperation de la structure initiale du circuit
                $qdEtapes = array(
                    'fields' => array(
                        'Etape.id',
                        'Etape.nom',
                        'Etape.type',
                        'Etape.ordre',
                        'Etape.description',
                        'Etape.type_document',
                        'Etape.inforequired_type_document'
                    ),
                    'contain' => array(
                        'Composition.id',
                        'Composition.type_validation',
                        'Composition.trigger_id',
                        'Composition.soustype',
						'Composition.type_document',
						'Composition.inforequired_type_document',
                        'Composition' => array(
                            CAKEFLOW_TRIGGER_MODEL => array('fields' => CAKEFLOW_TRIGGER_FIELDS)
                        )
                    ),
                    'conditions' => array(
                        'Etape.circuit_id' => $circuitId
                    ),
                    'order' => 'Etape.ordre'
                );
                $etapes = $this->Soustype->Circuit->Etape->find('all', $qdEtapes);
                //    $this->_logDebug($etapes);
                for ($i = 0; $i < count($etapes); $i++) {
                    $etapes[$i]['canAdd'] = $this->Soustype->Circuit->Etape->canAdd($etapes[$i]['Etape']['id']);
                    $etapes[$i]['isDeletable'] = $this->Soustype->Circuit->Etape->isDeletable($etapes[$i]['Etape']['id']);
                }
                $this->set('circuit', $this->Soustype->Circuit->find('first', array('conditions' => array('Circuit.id' => $circuitId))));
                $this->set('etapes', $etapes);
            }

            $this->loadModel('Connecteur');
            $hasParapheurActif = $this->Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%Parapheur%',
						'Connecteur.use_signature'=> true,
						'Connecteur.signature_protocol'=> 'IPARAPHEUR'
                    ),
                    'contain' => false
                )
            );
            if( !empty( $hasParapheurActif )) {
				if (empty($this->Session->read('Iparapheur.listSoustype'))) {
					$soustypes = $this->Soustype->Circuit->Etape->listeSousTypesParapheur($hasParapheurActif);
					$soustypes = $soustypes['soustype'];
				}
				else {
					$soustypes = $this->Session->read( 'Iparapheur.listSoustype' );
				}
			} else {
				$soustypes['soustype'] = array();
			}
			if (empty($soustypes)) {
				$soustypes['soustype'] = array();
			}

			$hasPastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell' => true
					),
					'contain' => false
				)
			);
			$this->set('hasPastellActif', $hasPastellActif);
			// Récup des sous-types dispos
			$listSoustypeIp = array();
			$documents = array();
			if (!empty($hasPastellActif)) {
				if( empty($this->Session->read('Pastell.listSoustypeIp')) ) {
					$id_entity = $hasPastellActif['Connecteur']['id_entity'];
					// Récup des sous-types dispos
					$PastellComponent = new NewPastellComponent();
					$listSoustypeIp = $PastellComponent->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
					$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
				}
				$documents = $this->Soustype->Circuit->getDocNamePastell($circuitId);
				$listSoustypeIp = $this->Session->read('Pastell.listSoustypeIp');
				$listSoustypeIp = $listSoustypeIp['soustype'];
			}

			$this->set('listSoustypeIp', $listSoustypeIp);
			$this->set('documents', $documents);
            $this->set( 'soustypes', $soustypes);
        }
    }

    /**
     * obtenir le commentaire d'un sous-type
     *
     * @logical-group Soustypes
     * @user-profile Admin
     * @user-profile User
     *
     */
    public function getInformation( $id ){
        $this->autoRender = false;
        if($id != null){
//            $information = $this->Soustype->field('information', array('Soustype.id' => $id));
            $information = $this->Soustype->find(
                'first',
                array(
                    'conditions' => array(
                        'Soustype.id' => $id
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            $info = $information['Soustype']['information'];
        }
        else {
            $info = '';
        }
        return $info;
    }


    public function export() {
        $conditions = array();

        $querydata = array(
            'conditions' => array( 'Type.active' => true ),
            'order' => array(
                'Type.name'
            ),
            'contain' => array(
                'Soustype' => array(
                    'conditions' => array( 'Soustype.active' => true ),
                    'order' => array('Soustype.name ASC'),
					'Circuit' => array(
						'conditions' => array( 'Circuit.actif' => true )
					)
                ),

            )
        );
        $results = $this->Soustype->Type->find('all', $querydata);
        $this->set('results', $results);

//debug($results);
//die();
//        $this->_setOptions();
        $this->layout = '';
    }

}

?>
