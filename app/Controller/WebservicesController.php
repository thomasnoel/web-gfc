<?php

App::import('Vendor', 'nusoap/lib/nusoap');

/**
 * WebService
 *
 * Webservices controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class WebservicesController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Webservices';

	/**
	 * Controller uses
	 *
	 * @access public
	 * @var array
	 */
	public $uses = array();


	protected $_lastSequenceDocument = null;

	/**
	 * CakePHP callback
	 *
	 * Désactivation de l'autentification par les ACLs
	 * Mise en place un autentification Basic Auth
	 *
	 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->loginAction = array(
			'controller' => $this->name,
			'action' => 'chk'
		);
		$this->Auth->authenticate = array(
			'Basic' => array('userModel' => 'User')
		);
		$this->Auth->allow('service', 'index', 'add');
	}

	/**
	 * Vérification des informations de connexion (Basic Authenticate)
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @return void
	 */
	public function chk() {
		$data = array(
			'username' => $_SERVER['PHP_AUTH_USER'],
			'password' => $this->Auth->password($_SERVER['PHP_AUTH_PW'])
		);
		if ($this->Auth->login($data)) {
			$this->_logDebug('login ok');
		} else {
			$this->_logDebug('login error');
		}
	}

	/**
	 * Point d'entrée du webservice
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @return void
	 */
	public function service() {
		Configure::write('debug', 2);
		ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
		$server = new SoapServer('https://' . $_SERVER['HTTP_HOST'] . '/files/wsdl/webgfc.wsdl');
		$server->setObject($this);
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$this->autoRender = false;
			Configure::write('debug', 0);
			ob_start();
			$server->handle();
			$out = ob_get_clean();
			$this->response->type('text/xml');
			$this->response->body($out);
		} else {
			echo 'GET requests are not allowed, please use POST requests.';
		}
	}

	/**
	 * Test du webservice
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @return void
	 */
	public function echotest($echoRequest) {
		return $echoRequest . ' ' . env('SERVER_NAME');
	}

	/**
	 * Récupération de la liste des collectivités
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @return void
	 */
	public function getGFCCollectivites() {
		$this->loadModel('Collectivite');
		$collectivites = $this->Collectivite->find('list', array('conditions' => array('Collectivite.active' => true)));
		$return = array();
		foreach ($collectivites as $key => $val) {
			$return[] = array($key, $val);
		}
		return $return;
	}

	/**
	 * Définition de la connexion utilisée
	 *
	 * @access protected
	 * @param integer $collectiviteId indentifiant d'une collectivité
	 * @return void
	 */
	protected function _setConnection($collectiviteId) {
		$this->loadModel('Collectivite');
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.id' => $collectiviteId)));
		Configure::write('conn', $coll['Collectivite']['conn']);
		return $coll['Collectivite']['conn'];
	}

	/**
	 * Récupération de la liste des types
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant d'une collectivité
	 * @return array
	 */
	public function getGFCTypes($collectiviteId) {
		$this->_setConnection($collectiviteId);
		$this->loadModel('Type');
		$this->Type->setDataSource(Configure::read('conn'));
		$types = $this->Type->find('list');
		$return = array();
		foreach ($types as $key => $val) {
			$return[] = array($key, $val);
		}
		return $return;
	}

	/**
	 * Récupération de la liste des types
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant d'une collectivité
	 * @param integer $typeId identifiant d'un type
	 * @return array
	 */
	public function getGFCSoustypes($collectiviteId, $typeId) {
		$this->_setConnection($collectiviteId);
		$this->loadModel('Soustype');
		$this->Soustype->setDataSource(Configure::read('conn'));
		$soustypes = $this->Soustype->find('list', array('conditions' => array('Soustype.type_id' => $typeId)));
		$return = array();
		foreach ($soustypes as $key => $val) {
			$return[] = array($key, $val);
		}
		return $return;
	}

	/**
	 * Création d'un flux
	 *
	 * @logical-group WebService
	 *
	 * @deprecated since version 0.9
	 *
	 * @param integer $collectiviteId identifiant d'une collectivité
	 * @param integer $soustypeId identifiant d'un sous-type
	 * @param string $contactNom nom du contact
	 * @param string $titre titre du flux
	 * @param string $objet objet du flux
	 * @param base64 $fichier_associe contenu du document lié
	 * @param string $username identifiant de connexion de l'utilisateur cible
	 * @param integer $courriergrcId identifiant du courrier dans la GRC Localeo
	 * @return array|string
	 */
	public function createCourrier($collectiviteId, $soustypeId, $contactNom, $titre, $objet, $fichier_associe, $username, $courriergrcId, $contactId, $organismeId) {
		$conn = $this->_setConnection($collectiviteId);
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Reference' => '',
			'Message' => 'Impossible de créer le flux.',
			'Severite' => 'important',
			'ContactId' => -1,
			'OrganismeId' => -1
		);
		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource($conn);
		$this->User->recursive = -1;

		ClassRegistry::init(('Courrier'));
		$this->Courrier = new Courrier();
		$this->Courrier->setDataSource($conn);

		ClassRegistry::init(('Bancontenu'));
		$this->Bancontenu = new Bancontenu();
		$this->Bancontenu->setDataSource($conn);

		$this->Courrier->Metadonnee->setDataSource($conn);
		$this->Courrier->Soustype->setDataSource($conn);
		$this->Courrier->Soustype->Type->setDataSource($conn);
		$this->Courrier->Contact->setDataSource($conn);
		$this->Courrier->Organisme->setDataSource($conn);

		$querydata = array(
			'conditions' => array(
				'User.username' => $username
			),
			'contain' => array(
				'Desktop' => array(
					'Profil'
				)
			)
		);
		$user = $this->User->find('first', $querydata);

		if (empty($user)) {
			$return['CodeRetour'] = "KONOUSER";
			$return['Message'] .= " L'utilisateur n'existe pas.";
		} else {
			$desktopId = null;
			if ($user['Desktop']['profil_id'] == INIT_GID || $user['Desktop']['profil_id'] == DISP_GID) {
				$desktopId = $user['Desktop']['id'];
			} else {
				$found = false;
				for ($i = 0; $i < count($user['SecondaryDesktop']); $i++) {
					if ($user['SecondaryDesktop'][$i]['profil_id'] == DISP_GID && !$found) {
						$desktopId = $user['SecondaryDesktop'][$i]['id'];
						$found = true;
					}
				}
			}
			if (empty($desktopId)) {
				$return['CodeRetour'] = "KONODESKTOP";
				$return['Message'] .= "L'utilisateur n'a pas de profil Aiguilleur ou Initiateur.";
			} else {
				if( empty($courriergrcId)) {
					$courriergrcId = '';
				}
				// On regarde et on associe le contact si présent
				if( !empty($contactNom) ) {
					if( isset($contactNom->item) ) {
						foreach ($contactNom->item as $key => $value) {
							if ($value->key == 'name') {
								$contactName = $value->value;
							}
							if ($value->key == 'nom') {
								$contactNom = $value->value;
							}
							if ($value->key == 'prenom') {
								$contactPrenom = $value->value;
							}

						}
						$dataContact = array(
							'Contact' => array(
								'name' => $contactName,
								'nom' => $contactNom,
								'prenom' => $contactPrenom,
								'organisme_id' => Configure::read('Sansorganisme.id')
							)
						);
						$contactAlreadyExist = false;
						$contactInBase = $this->Courrier->Contact->find(
							'first',
							array(
								'conditions' => array(
									'Contact.name' => $contactName,
									'Contact.nom' => $contactNom,
									'Contact.prenom' => $contactPrenom
								),
								'contain' => false,
								'recursive' => -1
							)
						);
						if (!empty($contactInBase)) {
							$contactAlreadyExist = true;
							$contactIdForCourrier = $contactInBase['Contact']['id'];
							$OrganismeIdForCourrier = $contactInBase['Contact']['organisme_id'];
						} else {
							$this->Courrier->Contact->create($dataContact);
							if ($this->Courrier->Contact->save()) {
								$contactIdForCourrier = $this->Courrier->Contact->id;
							}
						}
					}
					else {

						json_decode($contactNom);
						if (json_last_error() == JSON_ERROR_NONE ||  json_last_error() === 0) {
							$contactNom = json_decode($contactNom);
						}
						if (isset($contactNom->usualName) && empty($contactNom->usualName)) {
							$dataContact = array(
								'Contact' => array(
									'name' => $contactNom->familyName . ' ' . $contactNom->firstname,
									'nom' => $contactNom->familyName,
									'prenom' => $contactNom->firstname,
									'organisme_id' => Configure::read('Sansorganisme.id')
								)
							);
							$contactAlreadyExist = false;
							$contactInBase = $this->Courrier->Contact->find(
								'first',
								array(
									'conditions' => array(
										'Contact.name' => $contactNom->familyName . ' ' . $contactNom->firstname,
										'Contact.nom' => $contactNom->familyName,
										'Contact.prenom' => $contactNom->firstname
									),
									'contain' => false,
									'recursive' => -1
								)
							);
							if (!empty($contactInBase)) {
								$contactAlreadyExist = true;
								$contactIdForCourrier = $contactInBase['Contact']['id'];
								$OrganismeIdForCourrier = $contactInBase['Contact']['organisme_id'];
							} else {
								$this->Courrier->Contact->create($dataContact);
								if ($this->Courrier->Contact->save()) {
									$contactIdForCourrier = $this->Courrier->Contact->id;
								}
							}
						} else {
							if (isset($contactNom->usualName) && !empty($contactNom->usualName)) {
								$addressbooks = $this->Courrier->Organisme->Addressbook->find('list', array('contain' => false));
								$dataOrganisme = array(
									'Organisme' => array(
										'addressbook_id' => array_keys($addressbooks)[0], //FICME: voir comment récupérer le bon carnet d'adresse cible
										'name' => $contactNom->usualName
									)
								);
								$OrganismeAlreadyExist = false;
								$OrganismeInBase = $this->Courrier->Organisme->find(
									'first',
									array(
										'conditions' => array(
											'Organisme.name' => $contactNom->usualName
										),
										'contain' => false,
										'recursive' => -1
									)
								);
								if (!empty($OrganismeInBase)) {
									$OrganismeAlreadyExist = true;
									$OrganismeIdForCourrier = $OrganismeInBase['Organisme']['id'];
								} else {
									$this->Courrier->Organisme->create($dataOrganisme);
									if ($this->Courrier->Organisme->save()) {
										$OrganismeIdForCourrier = $this->Courrier->Organisme->id;
										if (!empty($OrganismeIdForCourrier)) {
											$sansContact['Contact']['addressbook_id'] = $dataOrganisme['Organisme']['addressbook_id'];
											$sansContact['Contact']['organisme_id'] = $OrganismeIdForCourrier;
											$sansContact['Contact']['name'] = ' Sans contact';
											$sansContact['Contact']['nom'] = '0Sans contact';
											$this->Courrier->Organisme->Contact->create($sansContact['Contact']);
											$this->Courrier->Organisme->Contact->save();
											$contactIdForCourrier = $this->Courrier->Organisme->Contact->id;
										}
									}
								}
							}
						}
					}
				}

				if( isset( $contactId) && !empty($contactId )  ) {
					$contactIdForCourrier = $contactId;
				}
				if( isset( $organismeId) && !empty($organismeId )  ) {
					$OrganismeIdForCourrier = $organismeId;
				}
				if( $contactId == '-1' ) {
					$contactIdForCourrier = null;
				}
				if( $organismeId == '-1' ) {
					$OrganismeIdForCourrier = null;
				}

				$data = array(
					'Courrier' => array(
						'name' => $titre,
						'objet' => $objet,
						'user_creator_id' => $user['User']['id'],
						'soustype_id' => !empty($soustypeId) ? $soustypeId : null,
						'date' => date('Y-m-d'),
						'datereception' => date('Y-m-d'),
//						'reference' => $this->Courrier->genRef($soustypeId)
						'reference' => $this->Courrier->genReferenceUnique(),
						'contact_id' => isset( $contactIdForCourrier ) ? $contactIdForCourrier : Configure::read('Sanscontact.id'),
						'organisme_id' => isset( $OrganismeIdForCourrier ) ? $OrganismeIdForCourrier : Configure::read('Sansorganisme.id'),
						'courriergrc_id' => $courriergrcId
					)
				);

				$this->Courrier->begin();
				$this->Courrier->create($data);
				if ($this->Courrier->save()) {

					$this->Courrier->commit();
					$courrierId = $this->Courrier->id;
					$return['CodeRetour'] = "CREATED";
					$return['Message'] = "Le flux a été créé";
//					$return['Message'] = "";
					$return['Severite'] = "normal";
					$return['CourrierId'] = $courrierId;

					$return['Reference'] = $data['Courrier']['reference'];
					$return['ContactId'] = $data['Courrier']['contact_id'];
					$return['OrganismeId'] = $data['Courrier']['organisme_id'];

					$groupId = $user['Desktop']['profil_id'];

					$desktopmanagerId = Configure::read('Desktopmanager.BureauCourrierId');
					$groupId = Configure::read('Desktopmanager.BureauCourrierGroupId');
					$desktopsIds = $this->Courrier->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
					foreach($desktopsIds as $desktopId) {
						if ($groupId == 7) {
							$this->Bancontenu->add($desktopId, BAN_AIGUILLAGE, $courrierId);
						} else if ($groupId == 3) {
							$this->Bancontenu->add($desktopId, BAN_DOCS, $courrierId);
						} else {
							$return['CodeRetour'] = "ERROR";
							$return['Message'] .= "L'utilisateur n'a pas de profil Aiguilleur ou Initiateur.";
						}
					}


//					$courrier = $this->Courrier->read();
					/*
                     $courrier = $this->Courrier->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Courrier.id' => $courrierId
                            ),
                            'contain' => false
                        )
                    );
					$soustype = $this->Courrier->Soustype->find('first', array('conditions' => array('Soustype.id' => $data['Courrier']['soustype_id']), 'contain' => false));

					//ajout dans la bannette du redacteur
					$this->Bancontenu->add($data['Courrier']['user_creator_id'], BAN_DOCS, $data['Courrier']['id']);
					$inserted = false;
					if (!$this->Courrier->Traitement->targetExists($this->Courrier->id)) {
						if ($this->Courrier->Traitement->Circuit->insertDansCircuit($soustype['Soustype']['circuit_id'], $courrier['Courrier']['id'])) {
							$return['CodeRetour'] = "INSERTED";
							$return['Message'] = "Le flux a été créé et inséré dans un circuit";
							//on change l etat du flux dans la bannette du createur
							$this->Bancontenu->end($courrier['Courrier']['user_creator_id'], BAN_DOCS, $courrier['Courrier']['id']);
							//recuperation des prochains utilisateurs concerné par le flux
							$nextUsers = $this->Courrier->Traitement->whoIsNext($courrier['Courrier']['id']);
							foreach ($nextUsers as $nextUserId) { //pour chaque prochain utilisateurs concerné
								//on assigne le flux dans les bannettes des prochains utilisateurs concernés
								$this->Bancontenu->add($nextUserId, BAN_FLUX, $courrier['Courrier']['id']);
								//on envoi une notification aux prochains utilisateurs concernés
//                $message = 'Nouveau flux à traiter: ' . $courrier['Courrier']['name'];
//                $this->Notification->add("Nouveau flux", $courrier['Courrier']['user_creator_id'], $nextUserId, $courrier['Courrier']['id'], $message);
							}
							$inserted = true;
						}
                        else {
                            $this->Bancontenu->add($data['Courrier']['user_creator_id'], BAN_AIGUILLAGE, $data['Courrier']['id']);
                        }
					}
					if (!$inserted) { //si le flux n a pas été inséré dans un circuit
						//notification de nouveau courrier
//            $message = 'Nouveau document: ' . $courrier['Courrier']['name'];
//            $this->Notification->add("Nouveau flux", $courrier['Courrier']['user_creator_id'], $courrier['Courrier']['user_creator_id'], $courrier['Courrier']['id'], $message);
					}*/
					$conn = CakeSession::read('Auth.Collectivite.conn');
					if( !empty($fichier_associe) ) {
						if (is_array($fichier_associe)) {
							for ($j = 0; $j < count($fichier_associe); $j++) {
								if ($j != 0) {
									$this->_genFileAssoc($fichier_associe[$j], $courrierId, $return, false, $conn);
								} else {
									$this->_genFileAssoc($fichier_associe[$j], $courrierId, $return, true, $conn);
								}
							}
						} else {
							$this->_genFileAssoc($fichier_associe, $courrierId, $return, true, $conn);
						}
					}
				}
				else {
					$this->Courrier->rollback();
				}
			}
		}
$this->log( $return );
		return $return;
	}

	/**
	 * Génération d'un document lié
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileAssoc($fichier_associe, $fluxId, &$return, $main_doc = true, $conn ) {
		if(empty($conn) ) {
			$conn = CakeSession::read('Auth.Collectivite.conn');
		}
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$fileMimeType = $finfo->buffer($fichier_associe);
		$this->loadModel('Document');
		$this->loadModel('Courrier');
		$this->Courrier->setDataSource(Configure::read('conn'));
		$this->Courrier->Document->setDataSource(Configure::read('conn'));
//		$filename = "fichier_associe_" . date('Ymd_His');
//        $sql = "SELECT nextval('documents_name_seq'); /*".mktime(true)." "  . $this->_lastSequenceDocument . "*/";

		$fileMimeTypeError = false;
		if ($fileMimeType == "application/pdf" || $fileMimeType == "PDF") {
			$fileExt = "pdf";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.text" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.TEXT" || $fileMimeType == "OCTET-STREAM") {
			$fileExt = "odt";
		} else if ($fileMimeType == "image/bmp") {
			$fileExt = "bmp";
		} else if ($fileMimeType == "image/tiff") {
			$fileExt = "tif";
		} else if ($fileMimeType == "text/plain") {
			$fileExt = "txt";
		} else if ($fileMimeType == "application/vnd.ms-office") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/msword") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/rtf") {
			$fileExt = "rtf";
		} else if ($fileMimeType == "image/png" || $fileMimeType == "PNG") {
			$fileExt = "png";
		} else if ($fileMimeType == "image/jpeg" || $fileMimeType == "JPEG") {
			$fileExt = "jpg";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.spreadsheet" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.SPREADSHEET") {
			$fileExt = "ods";
		} else if ($fileMimeType == "VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT" || $fileMimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
			$fileExt = "docx";
		} else if ($fileMimeType == "GIF" || $fileMimeType == "gif") {
			$fileExt = "gif";
		} else if ($fileMimeType == '') {
			$fileMimeType = 'txt';
			$fileExt = "rtf";
		} else {
			$fileMimeTypeError = true;
		}
		$filename = $this->Document->genNameUnique(). "." . $fileExt;


		if ($fileMimeTypeError) {
			$return['CodeRetour'] .= "FILENOK";
			$return['Message'] .= ". Le fichier n'est pas reconnu";
		} else {
			$courrier = $this->Courrier->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.id' => $fluxId
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$name  = preg_replace( "/[:']/", "_", $filename );
			$name = str_replace( '/', '_', $name );
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
			file_put_contents( $file, $fichier_associe);
			$path = $file;
			$doc = array(
				'Document' => array(
					'name' => $filename,
					'size' => strlen($fichier_associe),
					'mime' => $fileMimeType,
					'ext' => $fileExt,
					'courrier_id' => $fluxId,
					'main_doc' => $main_doc,
					'path' => $path
				)
			);
			$this->Courrier->Document->create($doc);
			$docsaved = $this->Courrier->Document->save();
			if (empty($docsaved)) {
				$return['CodeRetour'] .= "NOFILE";
				$return['Message'] .= "Le flux a bien été créé mais le fichier n'a pas été envoyé<br />Veuillez ré-essayer.";
				$this->Courrier->delete($fluxId);
			} else {
				$return['CodeRetour'] .= "FILEOK";
				$return['Message'] .= "Le flux a bien été créé et le fichier a été envoyé";
			}
		}
	}

	/**
	 * Fonctionnalité de numérisation de masse (création de flux)
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant de la collectivité
	 * @param base64 $fichier_associe contenu du fichier lié
	 * @param string $username identifiant de connexion de l'utilisateur cible
	 * @return array
	 */
	public function scanMass($collectiviteId, $fichier_associe, $username) {
		$this->_setConnection($collectiviteId);
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Reference' => '',
			'Message' => 'Impossible de créer le flux.',
			'Severite' => 'important',
			'ContactId' => -1,
			'OrganismeId' => -1

		);
		$this->loadModel('Courrier');
		$this->loadModel('User');
		$this->loadModel('Bancontenu');
		$this->Courrier->setDataSource(Configure::read('conn'));
		$this->Courrier->Soustype->setDataSource(Configure::read('conn'));
		$this->Courrier->Soustype->Type->setDataSource(Configure::read('conn'));
		$this->Courrier->Metadonnee->setDataSource(Configure::read('conn'));
		$this->User->setDataSource(Configure::read('conn'));
		$this->Bancontenu->setDataSource(Configure::read('conn'));
		$this->Bancontenu->setDataSource(Configure::read('conn'));
		$this->Bancontenu->Bannette->setDataSource(Configure::read('conn'));
		$querydata = array(
			'conditions' => array(
				'User.username' => $username
			),
			'contain' => array(
				$this->User->Desktop->alias => array(
					$this->User->Profil->alias
				),
				$this->User->SecondaryDesktop->alias => array(
					$this->User->SecondaryDesktop->Profil->alias
				),
			)
		);
		$user = $this->User->find('first', $querydata);
		if (empty($user)) {
			$return['CodeRetour'] = "KONOUSER";
			$return['Message'] .= " L'utilisateur n'existe pas.";
		} else {
			$desktopId = null;
			if ($user['Desktop']['profil_id'] == DISP_GID) {
				$desktopId = $user['Desktop']['id'];
			} else {
				$found = false;
				for ($i = 0; $i < count($user['SecondaryDesktop']); $i++) {
					if ($user['SecondaryDesktop'][$i]['profil_id'] == DISP_GID && !$found) {
						$desktopId = $user['SecondaryDesktop'][$i]['id'];
						$found = true;
					}
				}
			}
			if (empty($desktopId)) {
				$return['CodeRetour'] = "KONODESKTOP";
				$return['Message'] .= "L'utilisateur n'a pas de profil Aiguilleur.";
			} else {
				$reference = "";
				if( empty($reference)) {
					$reference = $this->Courrier->genReferenceUnique();
				}
				$data = array(
					'Courrier' => array(
						'name' => 'Flux_importé_' . date('Y-m-d_H:i:s'),
						'reference' => $reference
					)
				);
				$this->Courrier->create($data);
				if ($this->Courrier->save()) {
					$courrierId = $this->Courrier->id;
					$return['CodeRetour'] = "CREATED";
					$return['Message'] = "Le flux a été créé";
					$return['Severite'] = "normal";
					$return['CourrierId'] = $courrierId;
					$return['ContactId'] = $data['Courrier']['contact_id'];
					$return['OrganismeId'] = $data['Courrier']['organisme_id'];
					$return['Reference'] = $reference;
					//ajout dans la bannette de l aiguilleur
					$this->Bancontenu->add($desktopId, BAN_AIGUILLAGE, $courrierId);
//TODO: permettre la notification d un lot complet et non la notification de chaque flux comme ci dessous
//          $name = __d('notification', 'ban_wsnew.name');
//          $description = __d('notification', 'ban_wsnew.description');
//          $type = 'ban_wsnew';
//          $desktops = array(
//            $desktopId => array(
//                $courrierId
//            )
//          );
//          $this->Courrier->Notifieddesktop->Notification->add($name, $description, $type, $desktops);
					$this->_genFileAssoc($fichier_associe, $courrierId, $return);
				}
			}
		}
		return $return;
	}

	/**
	 * Vérification de l'état d'un flux
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant de la collectivité
	 * @param integer $courrierId identifiant du flux
	 * @return array
	 */
	public function getStatut($collectiviteId, $courrierId) {
		$this->_setConnection($collectiviteId);
		$this->loadModel('Courrier');
		$this->Courrier->setDataSource(Configure::read('conn'));
		$this->Courrier->Soustype->setDataSource(Configure::read('conn'));
		$this->Courrier->Metadonnee->setDataSource(Configure::read('conn'));
		$courrier = $this->Courrier->find('first', array('conditions' => array('Courrier.id' => $courrierId)));
//var_dump($courrier);exit;
		//valeur de retour par defaut : erreur, le flux n existe pas
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Reference' => '',
			'Message' => 'Le flux n\'existe pas',
			'Severite' => 'normal',
			'ContactId' => -1,
			'OrganismeId' => -1
		);
		//si le flux existe
		if (!empty($courrier)) {
			//flux refuse
			$return['CourrierId'] = $courrier['Courrier']['id'];
			if ($this->Courrier->isRefused($courrier['Courrier']['id'])) {
				$return['CodeRetour'] = 'REFUS';
				$return['Message'] = 'Le flux a été refusé car '.$courrier['Courrier']['motifrefus'];
//        $this->loadModel('Notification');
//        $this->Notification->setDataSource(Configure::read('conn'));
//        $notif = $this->Notification->find('first', array('conditions' => array('courrier_id' => $courrier['Courrier']['id'])));
				//on retourne le code REFUS et on donne le Message de la notification de refus
//        if (!empty($notif)) {
//          if (!empty($notif['Notification']['message'])) {
//            $return['Message'] = $notif['Notification']['message'];
//          } else {
//            $return['Message'] = "Aucun message de refus";
//          }
//        }
			} else if ($this->Courrier->isTreated($courrier['Courrier']['id'])) {
				//on retourne le code OK
				$return['CodeRetour'] = 'OK';
				$return['Message'] = 'Le flux a été traité';
			} else if ($this->Courrier->isClosed($courrier['Courrier']['id'])) {
				//on retourne le code OK
				$return['CodeRetour'] = 'TREATED';
				$return['Message'] = 'Le flux est clos';
			}
			else {
				//on retourne le code ENCOURS
				$return['CodeRetour'] = 'ENCOURS';
				$return['Message'] = 'Le flux est en cours de traitement';
			}
		}
		return $return;
	}


	/**
	 * Génération d'un document lié à partir d'un mail
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileFromMail($fichier_associe, $fluxId, &$return) {
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$fileMimeType = $finfo->buffer($fichier_associe);
		$this->loadModel('Document');

		$fileMimeTypeError = false;
		if ($fileMimeType == "application/pdf") {
			$fileExt = "eml";
		} else {
			$fileMimeTypeError = true;
		}


		if ($fileMimeTypeError) {
			$return['CodeRetour'] .= "FILENOK";
			$return['Message'] .= ". Le fichier n'est pas reconnu";
		} else {
			$doc = array(
				'Document' => array(
					'name' => $this->Document->genNameUnique(),
					'size' => strlen($fichier_associe),
					'mime' => $fileMimeType,
					'content' => $fichier_associe, //TODO: prevoir l utilisation d une GED
					'ext' => $fileExt,
					'courrier_id' => $fluxId,
					'main_doc' => true
				)
			);
			$this->Courrier->setDataSource(Configure::read('conn'));
			$this->Courrier->Document->setDataSource(Configure::read('conn'));
			$this->Courrier->Document->create($doc);
			$docsaved = $this->Courrier->Document->save();
			if (empty($docsaved)) {
				$return['CodeRetour'] .= "NOFILE";
				$return['Message'] .= ".Le fichier n'a pas été envoyé";
			} else {
				$return['CodeRetour'] .= "FILEOK";
				$return['Message'] .= ". Le fichier a été envoyé";
			}
		}
	}

	/**
	 * Création d'un contact
	 *
	 * @logical-group WebService
	 *
	 *
	 * @param integer $collectiviteId identifiant d'une collectivité
	 * @param integer $courrierId identifiant du flux
	 * @param string $contactData données du contact
	 * @param string $username identifiant de connexion de l'utilisateur cible
	 * @return array|string
	 */
	public function createContact($collectiviteId, $contactData, $courrierId, $contactgrcId, $courriergrcId) {
		$this->_setConnection($collectiviteId);
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Message' => 'Impossible de créer le contact.',
			'Severite' => 'important',
			'Reference' => '',
			'Datas' => array(),
			'ContactId' => -1,
			'OrganismeId' => -1
		);
		$this->loadModel('Courrier');
		$this->loadModel('Contact');
		$this->loadModel('Organisme');
		$this->loadModel('Addressbook');
		$this->Courrier->setDataSource(Configure::read('conn'));
		$this->Contact->setDataSource(Configure::read('conn'));
		$this->Organisme->setDataSource(Configure::read('conn'));
		$this->Addressbook->setDataSource(Configure::read('conn'));

		$addressbooks = $this->Addressbook->find('list', array('contain' => false));

		json_decode($contactData);
		if(json_last_error() == JSON_ERROR_NONE || json_last_error() === 0) {
			$contactData = json_decode($contactData);
		}
//Civilité
//Nom
//Prénom
//Date de naissance
//Email
//Téléphone
//Portable
//Numéro de voie
//Extension du numéro (bis, ter, etc)
//Nom de la voie
//Complément d’adresse
//Complément d’adresse 2
//Code postal
//Ville
//Pays
//Identifiant GRC du citoyen (contactgrc_id)
//Identifiant WebGFC de l’organisme

		if (empty($contactgrcId)) {
			$return['CodeRetour'] = "KONOCONTACT";
			$return['Message'] .= " Veuillez fournir l'identifiant du contact.";
		} else {

			// Demande issue de la GRC Localeo
			$contactUsed = array(
				'Contact' => array(
					'civilite' => $contactData->civility,
					'name' => $contactData->familyName . ' ' . $contactData->firstname,
					'nom' => $contactData->familyName,
					'prenom' => $contactData->firstname,
					'email' => $contactData->email,
					'numvoie' =>  $contactData->num,
					'nomvoie' =>  $contactData->street,
					'cp' =>  $contactData->ZC,
					'ville' =>  $contactData->city,
					'contactgrc_id' =>  $contactData->externalId,
					'organisme_id' => ( isset($contactData->organismeId) && !empty($contactData->organismeId) ) ? $contactData->organismeId : Configure::read('Sansorganisme.id'),
					'addressbook_id' => array_keys( $addressbooks )[0],
					'active' => true
				)
			);

			$contactAlreadyExist = false;
			$contactInBase = $this->Contact->find(
				'first',
				array(
					'conditions' => array(
						'Contact.nom' => $contactUsed['Contact']['nom'],
						'Contact.prenom' => $contactUsed['Contact']['prenom'],
						'Contact.email' => $contactUsed['Contact']['email']
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			if( !empty($contactInBase) ) {
				$contactAlreadyExist = true;
			}

			if( !$contactAlreadyExist ){
				$this->Contact->create($contactUsed);
				if ($this->Contact->save()) {
					$contactId = $this->Contact->id;
					$return['CodeRetour'] = "CREATED";
					$return['Message'] = "Le contact a été créé";
					$return['ContactId'] = $contactId;
					$return['OrganismeId'] = $contactUsed['Contact']['organisme_id'];
					$return['Severite'] = "normal";
					$return['Reference'] = '';
//                    $return['Datas'] = $contactData;

					if(!empty( $courrierId ) ) {
						$return['CourrierId'] = $courrierId;

						// On lit le contact au bon courrier
						$this->Courrier->updateAll(
							array(
								'Courrier.contact_id' => $contactId
							),
							array(
								'Courrier.id' => $courrierId
							)
						);
					}
				}
			}
			else {
				$return['CodeRetour'] = "NOCREATED";
				$return['Message'] = "Le contact n'a pas été créé, il existe déjà";
				$return['Severite'] = "important";
				$return['Reference'] = '';
				$return['ContactId'] = $contactInBase['Contact']['id'];
				$return['OrganismeId'] = $contactInBase['Contact']['organisme_id'];
				$return['Datas'] = $contactInBase;
				if(!empty( $courrierId ) ) {
					$return['CourrierId'] = $courrierId;
					$this->Courrier->updateAll(
						array(
							'Courrier.contact_id' => $contactInBase['Contact']['id']
						),
						array(
							'Courrier.id' => $courrierId
						)
					);
				}
			}
		}
		return $return;
	}


	/**
	 * Création d'un organisme
	 *
	 * @logical-group WebService
	 *
	 *
	 * @param integer $collectiviteId identifiant d'une collectivité
	 * @param integer $courrierId identifiant du flux
	 * @param string $organismeData données du contact
	 * @return array|string
	 */
	public function createOrganisme($collectiviteId, $organismeData, $courrierId, $organismegrcId, $courriergrcId) {
		$this->_setConnection($collectiviteId);
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Message' => "Impossible de créer l'organisme.",
			'OrganismeId' => -1,
			'ContactId' => -1,
			'Severite' => 'important',
			'Reference' => '',
			'Datas' => array()
		);

		$this->loadModel('Courrier');
//		$this->loadModel('User');
		$this->loadModel('Contact');
		$this->loadModel('Organisme');
		$this->loadModel('Addressbook');
		$this->Courrier->setDataSource(Configure::read('conn'));
//		$this->User->setDataSource(Configure::read('conn'));
//		$this->User->recursive = -1;
		$this->Contact->setDataSource(Configure::read('conn'));
		$this->Organisme->setDataSource(Configure::read('conn'));
		$this->Addressbook->setDataSource(Configure::read('conn'));

		$addressbooks = $this->Addressbook->find('list', array('contain' => false));

		json_decode($organismeData);
		if(json_last_error() == JSON_ERROR_NONE || json_last_error() === 0) {
			$organismeData = json_decode($organismeData);
		}

		if (empty($organismegrcId)) {
			$return['CodeRetour'] = "KONOORG";
			$return['Message'] .= " Veuillez fournir l'identifiant de l'organisme.";
		} else {
			// Demande issue de la GRC Localeo
			$dataOrg = array(
				'Organisme' => array(
					'organismegrc_id' => $organismeData->externalId,
					'name' => $organismeData->usualName,
					'email' => $organismeData->email,
					'numvoie' =>  $organismeData->num,
					'nomvoie' =>  $organismeData->street,
					'compl' =>  $organismeData->ext,
					'cp' =>  $organismeData->ZC,
					'ville' =>  $organismeData->city,
					'addressbook_id' => array_keys( $addressbooks )[0],
					'active' => true
				)
			);

			$organismeAlreadyExist = false;
			$organismeInBase = $this->Organisme->find(
				'first',
				array(
					'conditions' => array(
						'Organisme.name' => $dataOrg['Organisme']['name']
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			if( !empty($organismeInBase) ) {
				$organismeAlreadyExist = true;
			}

			if( !$organismeAlreadyExist ){
				$this->Organisme->create($dataOrg);
				if ($this->Organisme->save()) {
					$organismeId = $this->Organisme->id;

					if( !empty($organismeId) ) {
						$sansContact['Contact']['addressbook_id'] = $dataOrg['Organisme']['addressbook_id'];
						$sansContact['Contact']['organisme_id'] = $organismeId;
						$sansContact['Contact']['numvoie'] = $dataOrg['Organisme']['numvoie'];
						$sansContact['Contact']['nomvoie'] = $dataOrg['Organisme']['nomvoie'];
						$sansContact['Contact']['cp'] = $dataOrg['Organisme']['cp'];
						$sansContact['Contact']['compl'] = $dataOrg['Organisme']['compl'];
						$sansContact['Contact']['ville'] = $dataOrg['Organisme']['ville'];
						$sansContact['Contact']['name'] = ' Sans contact';
						$sansContact['Contact']['nom'] = '0Sans contact';
						$this->Organisme->Contact->create($sansContact['Contact']);
						$this->Organisme->Contact->save();
					}
					$return['CodeRetour'] = "CREATED";
					$return['Message'] = "L'organisme a été créé";
					$return['OrganismeId'] = $organismeId;
					$return['ContactId'] = $sansContact['Contact']['id'];
					$return['Severite'] = "normal";
					$return['Reference'] = '';
//                    $return['Datas'] = $organismeData;

					if(!empty( $courrierId ) ) {
						$return['CourrierId'] = $courrierId;// On lit le contact au bon courrier
						$this->Courrier->updateAll(
							array(
								'Courrier.organisme_id' => $organismeId
							),
							array(
								'Courrier.id' => $courrierId
							)
						);
					}
				}
			}
			else {
				$return['CodeRetour'] = "NOCREATED";
				$return['Message'] = "L'organisme n'a pas été créé, il existe déjà";
				$return['OrganismeId'] = $organismeInBase['Organisme']['id'];
				$return['ContactId'] = '';
				$return['Severite'] = "important";
				$return['Reference'] = '';
				$return['Datas'] = $organismeInBase;
				if(!empty( $courrierId ) ) {
					$return['CourrierId'] = $courrierId;
					$this->Courrier->updateAll(
						array(
							'Courrier.organisme_id' => $organismeInBase['Organisme']['id']
						),
						array(
							'Courrier.id' => $courrierId
						)
					);
				}
			}

		}
		return $return;
	}

	public function index() {

	}

	public function add() {
		$this->set('addressbook_id', 10);
		$civiliteOptions = $this->Session->read( 'Auth.Civilite');
		$this->set('civilite', $civiliteOptions);

		if( !empty($this->request->data)) {
			if( !empty($this->request->data['Contact']['name'])) {
				if(empty($this->request->data['Contact']['organisme_id'])) {
					$this->request->data['Contact']['organisme_id'] = 1;
				}
				$this->createContact(72, $this->request->data['Contact'], null, 1, 1);
			}
			if( !empty($this->request->data['Organisme'])) {
				$this->createOrganisme(72, $this->request->data['Organisme'], null, 1, 1);
			}
		}
		else {

		}
	}

	/**
	 * Récupération de la liste des bureaux
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant d'une collectivité
	 * @return array
	 */
	public function getGFCDesktopsmanagers($collectiviteId) {
		$this->_setConnection($collectiviteId);
		$this->loadModel('Desktopmanager');
		$this->Desktopmanager->setDataSource(Configure::read('conn'));
		$desktopsmanagers = $this->Desktopmanager->find('list');
		$return = array();
		foreach ($desktopsmanagers as $key => $val) {
			$return[] = array($key, $val);
		}
		return $return;
	}

	/**
	 * Fonction permettant de transformer les données de la GRC Localeo vers la BDD de web-GFC
	 * @param type $data
	 * @param type $collectiviteId
	 * @return type
	 */
	public function table_correspondance($data) {
//$this->log($data);
		// Demande de création d'un contact depuis la GRC
		if( isset($data['citoyen']) && !empty( $data['citoyen'] ) ) {
			if(!empty($data['ext'])) {
				$numvoie = $data['num'].' '.$data['ext'];
			}
			else {
				$numvoie = $data['num'];
			}
			if( !empty($data['additionalAddress2'])) {
				$complAddress = $data['additionalAddress'].' '.$data['additionalAddress2'];
			}
			else {
				$complAddress = $data['additionalAddress'];
			}

			if( !empty($data['usualName']) ) {
				$usualName = $data['usualName'];
			}
			else {
				$usualName = $data['firstname'].' ' .$data['familyName'];
			}

			$dataForGRC = array(
				'civilite' => isset( $data['civility'] ) ? $data['civility'] : null,
				'name' => $usualName,
				'nom' => isset( $data['familyName'] ) ? $data['familyName'] : null,
				'prenom' => isset( $data['firstname'] ) ? $data['firstname'] : null,
				'email' => isset( $data['email'] ) ? $data['email'] : null,
				'tel' => isset($data['phone']) ? $data['phone'] : null,
				'portable' => isset( $data['gsm'] ) ? $data['gsm'] : null,
				'numvoie' => $numvoie, // n° de voie + extension de n°
				'adresse' => isset( $data['street'] ) ? $data['street'] : null,
				'adressecomplete' => isset( $data['street'] ) ? $data['street'] : null,
				'compl' => $complAddress,
				'cp' => isset( $data['ZC'] ) ? $data['ZC'] : null,
				'ville' => isset( $data['city'] ) ? $data['city'] : null,
				'pays' => isset( $data['country'] ) ? $data['country'] : null,
				'lignedirecte' => isset( $data['phonePro'] ) ? $data['phonePro'] : null,
				'contactgrc_id' => $data['externalId'],
				'organisme_id' => $data['organismeId']
			);
		}
		// demande de création d'une requête depuis GFC vers la GRC
		else if( isset($data['canalId']) && !empty($data['canalId'])) {
			$this->Contact = ClassRegistry::init('Contact');
			$contact = $this->Contact->find(
				'first',
				array(
					'conditions' => array(
						'Contact.id' => $data['contact_id']
					),
					'recursive' => -1,
					'contain' => false
				)
			);
			if( $contact['Contact']['name'] == ' Sans contact') {
				$dataForGRC = array(
					'subject' => $data['name'],
					'content' => $data['objet'],
					'geo' => '',
					'externalId' => $data['id']
				);
			}
			else {
				$dataForGRC = array(
					'subject' => $data['name'],
					'content' => $data['objet'],
					'geo' => '',
					'externalId' => $data['id'],
					//                'civility' => 1,
					'usualName' => $contact['Contact']['name'],
					'familyName' => $contact['Contact']['nom'],
					'firstname' => $contact['Contact']['prenom'],
					'email' => $contact['Contact']['email'],
					//                'num' => $contact['Contact']['numvoie'],
					//            'ext' => '',
					//                'street' => $contact['Contact']['adresse'],
					'ZC' => $contact['Contact']['cp'],
					'city' => $contact['Contact']['ville'],
					'civility' => $contact['Contact']['civilite']
				);
			}
		}
		return $dataForGRC;
	}
}

?>
