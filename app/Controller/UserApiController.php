<?php
/**
 * Flux
 *
 * UserApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

// TODO add check is uuid

class UserApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['User', 'Collectivite', 'Desktop', 'Desktopmanager'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$users = $this->getUsers($this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($users),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($userId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$user = $this->getUser($userId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($user),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	/**
	 * @return CakeResponse
	 * key = user
	 * data = {
	"username":"aauzolat",
	"nom":"AUZOLAT",
	"prenom":"arnaud"
	"password":"user",
	"desktop" : {
	"name": "Initiateur Arnaud AUZOLAT",
	"profil_id": 3
	}
	"service" : {
	"name": "Logements"
	}
	}
	 */
	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$user = json_decode($this->request->input(), true);
		$user['password'] = Security::hash($user['password'], 'sha256', true);

		// On vérifie si l'utilsiateur n'existe pas déjà
		$isUserAlreadyExisting = $this->User->find(
			'first',
			array(
				'conditions' => array(
					'User.username' => $user['username'],
					'User.nom' => $user['nom'],
					'User.prenom' => $user['prenom']
				),
				'contain' => false
			)
		);
		if( empty($isUserAlreadyExisting) ) {

			if (!empty($isDesktopAlreadyExisting)) {
				$success = true;
				$user['desktop_id'] = $isDesktopAlreadyExisting['Desktop']['id'];
			}

			// On vérifie d'abord que le rôle existe déjà
			$isDesktopAlreadyExisting = $this->User->Desktop->find(
				'first',
				array(
					'conditions' => array(
						'Desktop.name' => $user['desktop']['name'],
						'Desktop.profil_id' => $user['desktop']['profil_id']
					),
					'contain' => false
				)
			);

			if (!empty($isDesktopAlreadyExisting)) {
				$success = true;
				$user['desktop_id'] = $isDesktopAlreadyExisting['Desktop']['id'];
			} else {
				// création du profil associé
				$success = $this->User->Desktop->save($user['desktop']);
				$user['desktop_id'] = $this->User->Desktop->id;
			}

			// création du bureau associé
			$desktomanagerData = [
				'name' => 'Bureau ' . $user['desktop']['name'],
				'isautocreated' => true
			];
			if ($success) {
				$success = $this->User->Desktop->Desktopmanager->save($desktomanagerData) && $success;
				if ($success) {
					// associaiton bureau / profil
					$datasLiaisons = [
						'desktop_id' => $user['desktop_id'],
						'desktopmanager_id' => $this->User->Desktop->Desktopmanager->id
					];
					$success = $this->User->Desktop->Desktopmanager->DesktopDesktopmanager->save($datasLiaisons) && $success;
				}

				// association du profil au  service
				// On vérifie d'abord que le service existe ou non
				$isServiceAlreadyExisting = $this->User->Desktop->Service->find(
					'first',
					array(
						'conditions' => array(
							'Service.name' => $user['service']
						),
						'contain' => false
					)
				);
				if (!$isServiceAlreadyExisting) {
					$success = $this->User->Desktop->Service->save($user['service']) && $success;
					if ($success) {
						$datasLiaisonsService = [
							'desktop_id' => $user['desktop_id'],
							'service_id' => $this->User->Desktop->Service->id
						];
					}
				} else {
					$datasLiaisonsService = [
						'desktop_id' => $user['desktop_id'],
						'service_id' => $isServiceAlreadyExisting['Service']['id']
					];
				}
				$this->User->Desktop->Service->DesktopsService->save($datasLiaisonsService) && $success;
			}
			$res = $this->User->save($user);

			if (!$res) {
				return $this->Api->formatCakePhpValidationMessages($this->User->validationErrors);
			}
		}
		else {
			return new CakeResponse([
				'body' => json_encode(['message' => "L'utilisateur existe déjà en base de donnéés" ]),
				'status' => 200,
				'type' => 'application/json'
			]);

		}

		return new CakeResponse([
			'body' => json_encode([
				'userId' => $res['User']['id'],
				'desktopId' => $user['desktop_id'],
				'desktopmanagerId' => $this->User->Desktop->Desktopmanager->id
			]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($userId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getUser($userId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$this->User->delete($userId);
		return new CakeResponse([
			'body' => json_encode(['message' => "L'utilisateur a bien été supprimé" ]),
			'type' => 'application/json',
			'status' => 200
		]);
	}


	public function update($userId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getUser($userId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}


		$user = json_decode($this->request->input(), true);
		$user['id'] = $userId;
		$res = $this->User->save($user);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->User->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['userId' => $res['User']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	public function associateDesktop($userId, $desktopId)
	{

		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getUser($userId);
			$this->getDesktop($desktopId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		if ($this->isAlreadyAssociated($userId, $desktopId)) {
			return new CakeResponse([
				'status' => 204,
				'type' => 'application/json'
			]);
		}

		$data = [
			'user_id' => $userId,
			'desktop_id' => $desktopId
		];

		$res = $this->User->DesktopsUser->save($data);

		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->User->validationErrors);
		}

		return new CakeResponse([
			'status' => 204,
			'type' => 'application/json'
		]);

	}


	public function dissociateDesktop($userId, $desktopId)
	{

		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getUser($userId);
			$this->getType($desktopId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		if (!$this->isAlreadyAssociated($userId, $desktopId)) {
			return new CakeResponse([
				'status' => 204,
				'type' => 'application/json'
			]);
		}

		$data = [
			'user_id' => $userId,
			'desktop_id' => $desktopId
		];

		$res = $this->User->DesktopsUser->deleteAll($data);

		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->User->validationErrors);
		}

		return new CakeResponse([
			'status' => 204,
			'type' => 'application/json'
		]);

	}



	private function isAlreadyAssociated($userId, $desktopId)
	{
		$association = $this->User->DesktopsUser->find('first', [
			'conditions' => [
				'user_id' => $userId,
				'desktop_id' => $desktopId
			]
		]);

		return !empty($association);
	}


	private function sanitizeUser($jsonData)
	{
		$authorizedFields = ['username', 'nom', 'prenom', 'password', 'mail'];
		$user = array_intersect_key(json_decode($jsonData, true), array_flip($authorizedFields));
		return $user;

	}

	private function getUsers( $limit, $offset)
	{
		$conditions = array(
			'limit' => $limit,
			'offset' => $offset
		);
		$users = $this->User->find('all', $conditions);
		return Hash::extract($users, "{n}.User");
	}


	private function getUser($userId)
	{
		$user = $this->User->find('first', [
			'conditions' => [
				'User.id' => $userId,
				'Desktop.profil_id <>' => ADMIN_GID
			],
			'contain' => [
				'Desktop'
			],
			'recursive' => -1
		]);

		if (empty($user)) {
			throw new NotFoundException('Utilisateur non trouvé / inexistant');
		}
		$formattedUser = Hash::extract($user, "User");

		$formattedUser['profils'] = $user['Desktop'] ?? [];

		return $formattedUser;
	}


	private function getDesktop($desktopId)
	{
		$desktop = $this->Desktop->find('first', [
			'conditions' => [
				'id' => $desktopId
			]
		]);

		if (empty($desktop)) {
			throw new NotFoundException('Profil non trouvé / inexistant');
		}

		return Hash::extract($desktop, "Desktop");
	}

}
