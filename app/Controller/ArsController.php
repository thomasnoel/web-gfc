<?php

/**
 * Accusés de réception
 *
 * Ars controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ArsController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Ars';

    /**
     *
     * Correspondance type MIME / extensions de fichier
     *
     * @access private
     * @var array
     */
    private $_allowed = array(
        'audio' => array(
            'audio/x-wav' => 'wav',
            'audio/mpeg' => 'mp3',
            'application/ogg' => 'ogg',
            'audio/ogg' => 'ogg'
        ),
        'video' => array(
            'video/x-msvideo' => 'avi',
            'video/mpeg' => 'mpg',
            'video/mp4' => 'mp4',
            'application/ogg' => 'ogg',
            'video/ogg' => 'ogg'
        ),
        'ott' => array(
            'application/vnd.oasis.opendocument.text' => 'odt',
            'application/vnd.oasis.opendocument.text-template' => 'ott',
            'application/force-download' => 'odt'
        ),
        'pdf' => array(
            'application/pdf' => 'pdf'
        )
    );

    /**
     *
     * Controller's Components
     *
     * @access public
     * @var array
     */
    public $components = array('Linkeddocs', 'Conversion');

    /**
     * Sélection d'un accusé de réception (marqué comme envoyé / utilisé)
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Accusés de réception
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function select() {
        if (!empty($this->request->data)) {
//debug($this->request->data);
//echo '<pre>';var_dump($this->request->data);
//die();

			$conn = $this->Session->read('Auth.Collectivite.conn');
            $ar = $this->Ar->find(
                    'first', array(
                'conditions' => array(
                    'Ar.id' => $this->request->data['Ar']['id']
                ),
                'contain' => false
                    )
            );
            $ar_name = str_replace('odt', 'pdf', $ar['Ar']['name']);
            $this->request->data['Ar']['name'] = $ar_name;
//            $this->request->data['Ar']['content'] = $ar['Ar']['content'];
            $file = $ar['Ar']['name'];
            $arGenerated = file_get_contents(WEBDAV_DIR . DS . $conn . DS .$ar['Ar']['courrier_id'] . DS . $file);
            $this->request->data['Ar']['content'] = $this->Conversion->convertirFlux($arGenerated, 'odt', 'pdf');

            $this->request->data['Ar']['mime'] = 'application/pdf';
            $this->request->data['Ar']['format'] = 'pdf';
            $this->request->data['Ar']['size'] = $ar['Ar']['size'];
            $this->request->data['Ar']['ext'] = 'pdf';
            $this->request->data['Ar']['courrier_id'] = $ar['Ar']['courrier_id'];


//            $this->convertToPdf($this->request->data['Ar']['id']);
            $this->Jsonmsg->init();
            $this->Ar->create($this->request->data);
            $saved = $this->Ar->save();


			$qdFlux = array(
				'fields' => array('Courrier.id', 'Courrier.reference'),
				'recursive' => -1,
				'conditions' => array(
					'Courrier.id' => $ar['Ar']['courrier_id']
				)
			);
			$flux = $this->Ar->Courrier->find('first', $qdFlux);
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$ar_name}";
			file_put_contents( $file, $this->request->data['Ar']['content'] );
			$path = $file;


            if (!empty($saved)) {
				$documentAr = array(
                    'Document' => array(
                        'name' => $this->request->data['Ar']['name'],
                        'ext' => $this->request->data['Ar']['ext'],
                        'size' => $this->request->data['Ar']['size'],
                        'main_doc' => false,
                        'desktop_creator_id' => $this->Session->read('Auth.User.id'),
                        'courrier_id' => $ar['Ar']['courrier_id'],
                        'content' => null,
                        'mime' => $this->request->data['Ar']['mime'],
                        'path' => $path
                    )
                );


				$this->Ar->Courrier->Document->create($documentAr);
				$saved = $this->Ar->Courrier->Document->save() && $saved;
				$this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('convertToPdf', 'deletefile');
    }

    /**
     * Suppression d'un accusé de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Accusés de réception
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        if ($this->Ar->delete($id)) {
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        }
        $this->Jsonmsg->send();
    }

    /**
     * Téléchargement d'un accusé de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Accusés de réception
     * @user-profile User
     *
     * @access public
     * @throws NotFoundException
     * @return void
     */
    public function download($id) {
        $ar = $this->Ar->find('first', array('conditions' => array('Ar.id' => $id), 'recursive' => -1));
        if (empty($ar)) {
            throw new NotFoundException();
        }


        $this->autoRender = false;
        Configure::write('debug', 0);

        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
//echo '<pre>';var_dump($ar['Ar']['mime']);
//die();
        if ($ar['Ar']['mime'] == 'application/pdf') {
            header("Content-type: application/pdf");
            header("Content-Disposition: attachment; filename=\"" . $ar['Ar']["name"] . "\""); // use 'attachment' to force a file download
        } else {
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"" . Inflector::slug($ar['Ar']["name"]) . '.' . $this->_allowed[$ar['Ar']['format']][$ar['Ar']['mime']] . "\"");
            header("Content-length: " . $ar['Ar']['size']);
        }

        print $ar['Ar']['content'];
        exit();
    }

    /**
     * Récupération de la pré-visualisation d'un accusé de réception (génération si nécessaire)
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Accusés de réception
     * @user-profile User
     *
     * @access public
     * @param integer $arId
     * @throws NotFoundException
     * @return void
     */
    public function getPreview($arId) {
        $ar = $this->Ar->find('first', array('conditions' => array('Ar.id' => $arId), 'recursive' => -1));
        if (empty($ar)) {
            throw new NotFoundException();
        }

        $preview = __d('document', 'Document.preview.error');

        $preview = $this->Ar->generatePreview($arId);


        $mime = $ar['Ar']['mime'];

        $tmpPreviewFile = '';
        $tmpPreviewFileBasename = '';
        $previewType = '';

        if (in_array($mime, $this->Ar->convertibleMimeTypes) || $mime == "application/pdf" || $mime == "PDF" /* || $mime == "application/vnd.oasis.opendocument.text" */) {
            $previewType = 'flexpaper';
            $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($ar['Ar']['name']) . "_preview.pdf";
            $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
            //purge des fichiers de preview précédents
            $this->Linkeddocs->purge();
            file_put_contents($tmpPreviewFile, $preview);
        } else {
            foreach ($this->Ar->otherMimeTypes as $type => $mimes) {

                if (in_array($mime, array_keys($mimes))) {
                    $previewType = $type;
                    $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($ar['Ar']['name']) . "_preview." . $mimes[$mime];
                    $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
                    //purge des fichiers de preview précédents
                    $this->Linkeddocs->purge();
                    file_put_contents($tmpPreviewFile, $ar['Ar']['content']);
                }
            }
        }
//debug($tmpPreviewFileBasename);
//debug($mime);
//die();
        if (!empty($tmpPreviewFile)) {
            chmod($tmpPreviewFile, 0777);
        }
        $this->set('fileName', $tmpPreviewFileBasename);

        $this->set('previewType', $previewType);
        $this->set('mime', "pdf");
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
    }

    /**
     * Conversion de l'AR en PDF
     * @param type $fluxId
     */
    public function convertToPdf($arId) {
        $conn = $this->Session->read('Auth.Collectivite.conn');
        $this->autoRender = false;
        $ar = $this->Ar->find(
                'first', array(
            'conditions' => array(
                'Ar.id' => $arId
            ),
            'contain' => false
                )
        );

        if (!empty($ar)) {
            $arToPdf['Ar']['name'] = $ar['Ar']['name'];
            $arToPdf['Ar']['content'] = $ar['Ar']['content'];
            $arToPdf['Ar']['mime'] = 'application/pdf';
            $arToPdf['Ar']['format'] = 'pdf';
            $arToPdf['Ar']['size'] = $ar['Ar']['size'];
            $arToPdf['Ar']['ext'] = 'pdf';
            $arToPdf['Ar']['courrier_id'] = $ar['Ar']['courrier_id'];

            if(strpos($ar['Ar']['name'], 'odt') !== false ) {
                $ar_name = str_replace('odt', 'pdf', $ar['Ar']['name']);
            }
            else if( strpos($ar['Ar']['name'], 'pdf') !== false ) {
                $ar_name = $ar['Ar']['name'];
            }
            else {
                $ar_name = $ar['Ar']['name'].'.pdf';
            }

            $courrier = $this->Ar->Courrier->find(
                'first',
                array(
                    'conditions' => array(
                        'Courrier.id' => $ar['Ar']['courrier_id']
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            $fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
            $name  = preg_replace( "/[:']/", "_", $ar_name);
            $name = str_replace( '/', '_', $name );
            $file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
            file_put_contents( $file, $arToPdf['Ar']['content']);
            $path = $file;

            $documentAr = array(
                'Document' => array(
                    'name' => $ar_name,
                    'ext' => $arToPdf['Ar']['ext'],
                    'size' => $arToPdf['Ar']['size'],
                    'main_doc' => false,
                    'desktop_creator_id' => $this->Session->read('Auth.User.id'),
                    'courrier_id' => $arToPdf['Ar']['courrier_id'],
//                    'content' => $arToPdf['Ar']['content'],
                    'path' => $path,
                    'mime' => $arToPdf['Ar']['mime']
                )
            );
            $this->Ar->Courrier->Document->create($documentAr);
            $saved = $this->Ar->Courrier->Document->save();

            if ($saved) {

                $file = $ar['Ar']['name'];
                $arGenerated = file_get_contents(WEBDAV_DIR . DS . $conn . DS .$ar['Ar']['courrier_id'] . DS . $file);

                if (!empty($arGenerated) && isset($arGenerated)) {
                    $ar_content = $this->Conversion->convertirFlux($arGenerated, 'odt', 'pdf');
                }
                else {
//                    $ar_content = $this->Conversion->convertirFlux($documentAr['Document']['content'], 'odt', 'pdf');
                    $ar_content = $this->Conversion->convertirFlux(file_get_contents($path), 'odt', 'pdf');
                }

//                $ar_name = str_replace('odt', 'pdf', 'AR ' . $ar['Ar']['name'] . " " . date('YmdHis') . '.pdf');
                $ar_name = $documentAr['Document']['name'];
                Configure::write('debug', 0);
                header("Content-type: application/pdf");
                header("Content-Disposition: attachment; filename=\"" . $ar_name . "\""); // use 'attachment' to force a file download

                print $ar_content;
                exit();
            }
        } else {
            return false;
        }
    }

    public function deletefile($arId) {
        $this->autoRender = false;
        $ar = $this->Ar->find('first', array('conditions' => array('Ar.id' => $arId), 'recursive' => -1));
        $name = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($ar['Ar']['name']) . "_preview.pdf";
        unlink( APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $name );
    }

}
