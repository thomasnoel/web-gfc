<?php
/**
 * Flux
 *
 * CheckApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

// TODO add check is uuid

class CheckApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Collectivite'];
	public $components = ['Api', 'Conversion'];



	public function testgedooo( )
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// exécutable du convertisseur
		$modelFileName = APP . WEBROOT_DIR . DS . 'files' . DS . 'test_signature.odt';

		require_once 'XML/RPC2/Client.php';
		$options = array(
			'uglyStructHack' => true
		);

		$url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
		$client = XML_RPC2_Client::create($url, $options);
		$documentGenerated = file_get_contents($modelFileName);
		try {
			$result = $client->convertFile(base64_encode($documentGenerated), 'odt', 'pdf', false, true);
			$return = base64_decode($result);
		}
		catch(Exception $e) {
			$return = $e;
		}

		$body = [
			'message' => 'Le fichier est bien généré. Cloudooo et Gedooo OK',
			'erreur' => 0
		];

		if( strpos( $return, 'Unable to connect' ) !== false ) {
			$body = [
				'message' =>  'Erreur. Cloudoo KO ',
				'erreur' => 1
			];
		}
		else if( $return == false ) {
			$body = [
				'message' =>  'Erreur. Cloudoo ou Gedooo KO ',
				'erreur' => 1
			];
		}

		return new CakeResponse([
			'body' => json_encode( $body ),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


}
