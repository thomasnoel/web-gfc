<?php

class ApiComponent extends Component
{

	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;


	public function initialize(Controller $controller)
	{
		$this->controller = $controller;
	}


	public function sendErrorJson($e)
	{
		$this->controller->log($e->getMessage(), 'restapi');
		return new CakeResponse([
			'body' => json_encode(['message' => $e->getMessage()]),
			'status' => $e->getCode(),
			'type' => 'application/json'
		]);
	}


	public function authentification($apiToken)
	{
		$collectivite = $this->controller->Collectivite->find('first', [
			'conditions' => ['apitoken' => $apiToken],
			'fields' => ['id', 'name', 'conn', 'login_suffix']
		]);

		if (empty($collectivite)) {
			throw new UnauthorizedException("Bad api token");
		}

		$this->controller->Session->write('collectivite', $collectivite['Collectivite']);
		$this->controller->Session->write('Auth', ['collectivite' => ['id' => $collectivite['Collectivite']['id']]]);
		$this->controller->Session->write('Auth.User.id',  Configure::read('Api.UserId'));
		$this->controller->Session->write('Auth.Collectivite.conn', $collectivite['Collectivite']['conn']);
		$this->controller->Session->write('Auth.User.connName', $collectivite['Collectivite']['conn']);
	}

	/**
	 *
	 * @return mixed
	 */
	public function getApiToken()
	{
		$headers = apache_request_headers();
		$apiToken = $headers['Authorization'];

		if (empty($apiToken)) {
			throw new BadRequestException("Missing Authorization header");
		}
		return $apiToken;

	}


	public function formatCakePhpValidationMessages($validationErrors)
	{
		$messages = [];
		foreach ($validationErrors as $ressource => $error) {
			$messages[$ressource] = $error [0];
		}

		$this->controller->log($messages, 'restapi');
		return new CakeResponse([
			'body' => json_encode(['message' => $messages]),
			'status' => 400,
			'type' => 'application/json'
		]);
	}

	public function superadminauthentification($apiToken)
	{
		$this->controller->User->setDataSource('default');
		$user = $this->controller->User->find('first', [
			'conditions' => ['User.apitoken' => $apiToken],
			'fields' => ['id', 'username', 'nom']
		]);

		if (empty($user)) {
			throw new UnauthorizedException("Bad api token");
		}

//		$this->controller->Session->write('collectivite', $collectivite['Collectivite']);
//		$this->controller->Session->write('Auth', ['collectivite' => ['id' => $collectivite['Collectivite']['id']]]);
		$this->controller->Session->write('Auth.User.id',  Configure::read('Api.UserId'));
//		$this->controller->Session->write('Auth.Collectivite.conn', $collectivite['Collectivite']['conn']);
//		$this->controller->Session->write('Auth.User.connName', $collectivite['Collectivite']['conn']);
	}


	public function getLimit() {
		$limit = null;
		if (array_key_exists('limit', $_GET)) {
			$limit = $_GET['limit'];
		}

		return $limit;
	}
	public function getFields() {
		$fields = [];
		if (array_key_exists('fields', $_GET)) {
			$fields = explode(',', $_GET['fields']);
		}

		return $fields;
	}


	public function getOffset() {
		$offset = null;
		if (array_key_exists('offset', $_GET)) {
			$offset = $_GET['limit'];
		}

		return $offset;
	}

	public function getParams() {
		$params = [
			'equals' => [],
			'or'     => []
		];

		$parentModel = $this->loadModel();

		$array = array_merge($_GET, $_POST);
		foreach ($array as $field=>$value) {
			$comparisonType = 'equals';
			$operator = substr($field, strlen($field) - 1);
			if (in_array($operator, ['!', '>', '<'])) {
				$field = substr($field, 0, strlen($field) - 1);
				$operator .= '=';
			} else if (in_array($operator, ['|'])) {
				$field = substr($field, 0, strlen($field) - 1);
				$comparisonType = 'or';
				$operator = '=';
			} else if (in_array($operator, ['%'])) {
				$field = substr($field, 0, strlen($field) - 1);
				$operator = 'LIKE';
				$value = '%'.$value.'%';
			} else {
				$operator = '=';
			}

			if ($value == 'null') {
				$operator = (strpos($operator, '!') === false) ? 'IS' : 'IS NOT';
				$value = null;
			}

			$field = str_replace('_', '.', $field);
			if (strpos($field, '.') === false) {
				$alias = $parentModel->alias();
			} else {
				$fieldExplosion = explode('.', $field);
				$alias = $fieldExplosion[0];
				$field = $fieldExplosion[1];
			}

			$model = null;
			if ($parentModel->alias() !== $alias) {
				$association = $parentModel->associations()->get($alias);
				if (!is_null($association)) {
					$model = $this->loadModel($association->className());
				}
			} else {
				$model = $parentModel;
			}

			if (!is_null($model)) {
				if ($model->hasField(rtrim($field, 's')) && !$model->hasField($field)) {
					$field = rtrim($field, 's');
					$value = '(' . $value . ')';
					$operator = ' IN';
				}

				if ($model->hasField($field)) {
					$params[$comparisonType][$alias.'.'.$field . ' ' . $operator] = $value;
				}
			}
		}

		return $params;
	}
}
