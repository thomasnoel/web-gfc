<?php

/**
 * Class NewPastellComponent
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 * @copyright  Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
//
//App::uses('Component', 'Controller');
//App::uses('ComponentCollection', 'Controller');
//App::uses('SessionComponent', 'Controller/Component');
//App::uses('AppTools', 'Lib');
//App::uses('Folder', 'Utility');
//App::uses('File', 'Utility');
//App::uses('HttpSocket', 'Network/Http');

//use Buzz\Client\Curl;
use PastellClient\Client;
use PastellClient\Api\Version;
use PastellClient\Api\DocumentsRequester;
use PastellClient\Api\ConnectorsRequester;
use PastellClient\Api\EntitesRequester;
/**
 *[PastellComponent description]
 * @package app.Controller.Component
 * @version 5.1.1
 * @since 4.3.0
 */

class NewPastellComponent extends Component
{
	/**
	 * Controller
	 *
	 * @var type
	 */
	public $controller;


	/**
	 * @var client
	 */
	public $pastellClient;

	/**
	 * [public description]
	 * @var [type]
	 */
	private $parapheur_type;

	/**
	 * [public description]
	 * @var [type]
	 */
	private $pastell_type;

	/**
	 * [public description]
	 * @var [type]
	 */
	private $config;

	public function __construct()
	{
		if( Configure::read('USE_PASTELL' ) ) {
			$conn = CakeSession::read('Auth.User.connName');
			if (CakeSession::read('Auth.User.Env.main') != 'superadmin') {
				$this->Connecteur = ClassRegistry::init('Connecteur');
				$this->Connecteur->setDataSource($conn);
				$parapheurConnector = $this->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Parapheur%',
							'Connecteur.use_signature' => true
						),
						'contain' => false
					)
				);
				$this->parapheur_type = '';
				if (!empty($parapheurConnector)) {
					$this->parapheur_type = $parapheurConnector['Connecteur']['pastell_parapheur_type'];
				}
				$pastellConnector = $this->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Pastell%',
							'Connecteur.use_pastell' => true
						),
						'contain' => false
					)
				);
				$this->pastellClient = new Client();
				if (!empty($pastellConnector)) {
					$this->pastellClient->setUrl($pastellConnector['Connecteur']['host']);
					$this->pastellClient->authenticate($pastellConnector['Connecteur']['login'], $pastellConnector['Connecteur']['pwd']);
				}
				return $this->pastellClient;
			}
		}
	}

	/**
	 * Component initialization
	 *
	 * @access public
	 * @param type $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		if( Configure::read('USE_PASTELL' ) ) {
			if (CakeSession::read('Auth.User.Env.main') != 'superadmin') {
				$this->controller = $controller;
				$this->controller->loadModel('Connecteur');
				$connector = $this->controller->Connecteur->find(
					'first',
					array(
						'conditions' => array(
							'Connecteur.name ILIKE' => '%Pastell%',
							'Connecteur.use_pastell' => true
						),
						'contain' => false
					)
				);

				$this->pastellClient = new Client();
				if (!empty($connector)) {
					$this->pastellClient->setUrl($connector['Connecteur']['host']);
					$this->pastellClient->authenticate($connector['Connecteur']['login'], $connector['Connecteur']['pwd']);
				}
				return $this->pastellClient;
			}
		}
	}


	/**
	 * Retourne le n° de version de PASTELL
	 * @return array
	 * @throws \Libriciel\PastellApiPhp\Version
	 */
	public function getVersion(){
		$version = new Version($this->pastellClient);
		return $version->show();
	}

	/**
	 * @return \Pastell\Model\Document[]
	 * @throws \Psr\Http\Client\ClientExceptionInterface
	 * Retourne tous les documents d'une entité
	 */
	public function getAllDocuments($idEntite) {
		$documentsRequester = new DocumentsRequester($this->pastellClient);
		$listDocuments = $documentsRequester->all($idEntite);
		return $listDocuments;
	}

	/**
	 * @param $listDocuments
	 * @return \Pastell\Model\Document
	 * @throws \Psr\Http\Client\ClientExceptionInterface
	 * Retourne le tout premier document d'une entité donnée
	 */
	public function getOneDocument($listDocuments) {
		$documentsRequester = new DocumentsRequester($this->pastellClient);
		$oneDocument = $documentsRequester->show($listDocuments[0]->getIdE(), $listDocuments[0]->getIdD());
		return $oneDocument;
	}

	/**
	 * Listes des entités
	 *
	 * Liste l'ensemble des entités sur lesquelles l'utilisateur a des droits. Liste également les entités filles.
	 *
	 * @param bool $details si on veut afficher toutes les infos des entités
	 * si $details :
	 * @return array( array*(
	 *  id_e => Identifiant numérique de l'entité
	 *  denomination => Libellé de l'entité (ex : Saint-Amand-les-Eaux)
	 *  siren => Numéro SIREN de l'entité (si c'est une collectivité ou un centre de gestion)
	 *  centre_de_gestion => Identifiant numérique (id_e) du CDG de la collectivité
	 *  entite_mere => Identifiant numérique (id_e) de l'entité mère de l'entité (par exemple pour un service)
	 * ))
	 * sinon
	 * @return array( id_e => denomination )
	 */
	public function listEntities($details = false)
	{
		$entitesRequester = new EntitesRequester($this->pastellClient);
		$entities = $entitesRequester->all();

		if ($details) {
			return $entities;
		} else {
			$collectivites = [];
			foreach ($entities as $entity) {
				$collectivites[ $entity->getId()] = $entity->denomination;
			}
			return $collectivites;
		}
	}



	/**
	 * Listes des types du i-Parapheur
	 *
	 * Liste l'ensemble des entités sur lesquelles l'utilisateur a des droits. Liste également les entités filles.
	 *
	 * @param id Identifiant de l'entité ciblée
	 * @return liste des sous-types remontés dans le connecteur ciblé
	 */
	public function listTypesIParapheur($idEntite)
	{
		$idCe = $this->getIdCeByEntiteAndConnecteur($idEntite, 'signature');
		$typesIP = $this->pastellClient->get('/entite/' . $idEntite . '/connecteur/' . $idCe . '/recupFile/iparapheur_sous_type');

		return $typesIP;

	}

	/**
	 * Fonction remontant l'Id du connecteur entre le flux fourni (document-a-signer) et le type (signature)
	 * [getIdCeByEntiteAndConnecteur description]
	 * @param  [type] $id_e       [description]
	 * @param  [type] $connecteur [description]
	 * @return [type]             [description]
	 *
	 */
	public function getIdCeByEntiteAndConnecteur($idEntite, $connecteurType)
	{
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$ip = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%'
				),
				'contain' => false
			)
		);

		if (!empty($ip) ) {
			$this->flux = $ip['Connecteur']['type_doc_pastell'];
		}

		$path = '/entite/' . $idEntite . '/flux?flux=' . $this->flux . '&type=' . $connecteurType;
		$infos = $this->pastellClient->get( $path );

		// Il ne doit y avoir qu'un seul connecteur
		if (!empty($infos[0]['id_ce']) ) {
			return $infos[0]['id_ce'];
		}

		return null;
	}

	/**
	 * @param int $id_e identifiant de la collectivité
	 * @param string $type type de flux (pastell)
	 * @return Document Document Information du document crée.
	 * @throws Exception Si erreur lors de la création
	 */
	public function createDocument($id_e, $type = null)
	{
		$documentsRequester = new DocumentsRequester($this->pastellClient);
		$document = $documentsRequester->create($id_e, $type);
		return $document;
	}

	/**
	 * Modification d'un document
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param array $delib
	 * @param string $document (Pdf)
	 * @param array $annexes
	 * @return bool|string
	 * @since 4.2
	 * @version 5.0
	 * result : ok - si l'enregistrement s'est bien déroulé
	 * formulaire_ok : 1 si le formulaire est valide, 0 sinon
	 * message : Message complémentaire
	 *
	 * A noter que pour connaître la liste et les intitulés exacts des champs modifiables,
	 * il convient d'utiliser la fonction document-type-info.php, en lui précisant le type concerné.
	 */
	public function modifDocument($id_e, $id_d, $data)
	{
		//filtre les eléments vides
		$data = array_filter($data);
		// Remplissage des données du formulaire PASTELL précédemment créé
		$this->pastellClient->patch('/entite/'.$id_e.'/document/'.$id_d, $data);
	}

	/**
	 * Modification d'un document de type Document à signer
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param array $data
	 * @param string $document (Pdf)
	 * @param string $dcoSupp (Pdf)
	 * @param array $annexes
	 * @return bool|string
	 * @since 4.2
	 * @version 5.0
	 * result : ok - si l'enregistrement s'est bien déroulé
	 * formulaire_ok : 1 si le formulaire est valide, 0 sinon
	 * message : Message complémentaire
	 *
	 */
	public function modifDocumentDocumentASigner($id_e, $id_d, $data, $document = [], $docSupp = [], $pjs = [])
	{
		$success = true;
		$dataForPastell = array(
			'id_e' => $id_e,
			'id_d' => $id_d,
			'libelle' => $data['Courrier']['name']
		);

		//filtre les eléments vides
		$dataForPastell = array_filter($dataForPastell);
		// Remplissage des données du formulaire PASTELL précédemment créé
		$this->pastellClient->patch('/entite/'.$id_e.'/document/'.$id_d, $dataForPastell);

		if( isset( $document ) && !empty( $document ) ) {
			$options = array(
				'field_name' => 'document',
				'file_name' => $document['Document']['name'],
				'file_num' => 0,
				'file_content' => file_get_contents($document['Document']['path'])
			);
			$this->sendFile($id_e, $id_d, $options);
		}

		// Partie pour les PJs ajoutées lors de l'envoi
		if (!empty($pjs)) {
			foreach ($pjs as $key => $pj) {
				$pjoptions = array(
					'field_name' => 'autre_document_attache',
					'file_name' => $pj['filename'],
					'file_num' => $key,
					'file_content' => file_get_contents($pj['path'])
				);
				$this->sendAnnexeFileToDocument($id_e, $id_d, $pjoptions, $key );
			}
		}
		return $success;
	}

	/**
	 * Modification d'un document de type mail sécurisé
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param array $data
	 * @param string $document (Pdf)
	 * @param array $annexes
	 * @return bool|string
	 * result : ok - si l'enregistrement s'est bien déroulé
	 * formulaire_ok : 1 si le formulaire est valide, 0 sinon
	 * message : Message complémentaire
	 *
	 */
	public function modifDocumentMailsec($id_e, $id_d, $data, $document = [], $pjs = [])
	{
		$success = true;
		$dataForPastell = array(
			'id_e' => $id_e,
			'id_d' => $id_d,
			'to' => $data['Sendmail']['email'],
			'cc' => !empty($data['Sendmail']['ccmail']) ? $data['Sendmail']['ccmail'] : null,
			'bcc' => !empty($data['Sendmail']['ccimail']) ? $data['Sendmail']['ccimail'] : null,
			'objet' => $data['Sendmail']['sujet'],
			'message' => $data['Sendmail']['message']
		);

		//filtre les eléments vides
		$dataForPastell = array_filter($dataForPastell);
		// Remplissage des données du formulaire PASTELL précédemment créé
		$this->pastellClient->patch('/entite/'.$id_e.'/document/'.$id_d, $dataForPastell);

		if( isset( $document ) && !empty( $document ) ) {
			$options = array(
				'field_name' => 'document_attache',
				'file_name' => $document['Document']['name'],
				'file_num' => 0,
				'file_content' => file_get_contents($document['Document']['path'])
			);
			$this->sendFile($id_e, $id_d, $options);
		}

		// Partie pour les PJs ajoutées lors de l'envoi
		if (!empty($pjs)) {
			foreach ($pjs as $key => $pj) {
				$key ++;
				$pjoptions = array(
					'field_name' => 'document_attache',
					'file_name' => $pj['Document']['name'],
					'file_num' => $key + 1,
					'file_content' => file_get_contents($pj['Document']['path'])
				);
				$this->sendAnnexeFileToDocument($id_e, $id_d, $pjoptions, $key );
			}
		}
		return $success;
	}

	/**
	 * Envoi d'un fichier simple (pour modifier un document)
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param array $options
	 * [] =>
	 *  string field_name le nom du champs
	 *  string file_name le nom du fichier
	 *  integer file_number le numéro du fichier (pour les fichier multiple)
	 *  string file_content le contenu du fichier
	 *
	 * @return array
	 * [] =>
	 *  result : ok - si l'enregistrement s'est bien déroulé
	 *  formulaire_ok : 1 si le formulaire est valide, 0 sinon
	 *  message : Message complémentaire
	 */
	public function sendFile($id_e, $id_d, $options = [])
	{
		$default_options = [
			'id_e' => $id_e,
			'id_d' => $id_d,
			'field_name' => null, //le nom du champs
			'file_name' => null, //le nom du fichier
			'file_num' => null, //le numéro du fichier (pour les fichier multiple)
			'file_content' => null, //le contenu du fichier
		];
		$options = array_merge($default_options, $options);

		return $this->pastellClient->post('/entite/'.$id_e.'/document/'.$id_d.'/file/'.$options['field_name'], $options);
	}


	/**
	 * Envoi des documents en PJs
	 * @param int $idOrchestration ID de la structure sous pastell (id_e)
	 * @param int $idDocument ID du document sous pastell (id_d)
	 * @param array $data Les données nécessaire à l'envoi du fichier en annexe.
	 * @param int $key Numero de l'annexe
	 * @return array
	 */
	public function sendAnnexeFileToDocument($id_e, $id_d, $data, $key)
	{
		$field = $data['field_name'];
		if( $key > 0 ) {
			$field = $data['field_name'].'/'.$key;
		}
		return $this->pastellClient->post('/entite/'.$id_e.'/document/'.$id_d.'/file/'.$field, $data);
	}


	/**
	 * Execute une action sur un document
	 *
	 * @param int $id_e Identifiant de l'entité (retourné par list-entite)
	 * @param int $id_d Identifiant unique du document (retourné par list-document)
	 * @param string $action Nom de l'action (retourné par detail-document, champs action-possible)
	 * @param array $options
	 * @return array
	 * [] =>
	 *  result : 1 si l'action a été correctement exécute. Sinon, une erreur est envoyé
	 *  message : Message complémentaire en cas de réussite
	 *
	 */
	public function action($id_e, $id_d, $action, $options = [])
	{
		return $this->pastellClient->post('/entite/'.$id_e.'/document/'.$id_d . '/action/' . $action, $options);
	}

	/**
	 *
	 * Récupération des choix possibles pour un champs spécial du document : external-data
	 *
	 * Récupère les valeurs possible d'un champs.
	 * En effet, certaine valeur sont « externe » a Pastell : classification Actes, classification CDG, etc..
	 * Ce script permet de récupérer l'ensemble de ces valeurs.
	 * Ce script est utilisable sur tous les champs qui dispose d'une propriétés « controler »
	 *
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param string $field le nom d'un champ du document
	 * @return array
	 * valeur_possible * => Information supplémentaire sur la valeur possible (éventuellement sous forme de tableau associatif)
	 */
	public function getInfosField($id_e, $id_d, $field)
	{
		return $this->pastellClient->get('/entite/'.$id_e.'/document/'.$id_d . '/externalData/' . $field);
	}

	/**
	 * Retourne la liste des circuits du PASTELL
	 * La liste est enregistrée en Session
	 * pour économiser du traffic réseau entre WD et Pastell lors des appels suivants
	 *
	 * @param int $id_e identifiant de la collectivité
	 * @param string $type document dans PASTELL (ici = document-a-signer)
	 * @return array
	 */
	public function getCircuits($id_e, $type)
	{
		CakeSession::write('Pastell.circuits', null);
		$soustypes = array();
		if (empty( CakeSession::read('Pastell.circuits') ) ) {
			$document = $this->createDocument($id_e, $type);
			$tmp_id_d = $document->getIdD();

			$soustypesIp = $this->getInfosField($id_e, $tmp_id_d, 'iparapheur_sous_type');
			foreach ($soustypesIp as $row) {
				$soustypes['soustype'][$row] = $row;
			}
			if (!empty($soustypes)) {
				CakeSession::write('Pastell.circuits', $soustypes);
			}
			$this->action($id_e, $tmp_id_d, 'supression');
		} else {
			$soustypes = CakeSession::read('Pastell.circuits');
		}
		return $soustypes;
	}


	/**
	 * Envoi à Pastell l'information sur le circuit du parapheur à emprunter
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param string $sous_type
	 * @return bool|array résultat
	 */
	public function selectCircuit($id_e, $id_d, $sous_type)
	{
		$infos = [
			'id_e' => $id_e,
			'id_d' => $id_d,
			'iparapheur_type' => $this->parapheur_type,
			'iparapheur_sous_type' => utf8_decode($sous_type),
			'envoi_auto' => true
		];
		return $this->modifDocument($id_e, $id_d, $infos);
	}

	/**
	 * Détail sur un document
	 *
	 * Récupère l'ensemble des informations sur un document Liste également les entités filles.
	 *
	 * @param int $id_e Identifiant de l'entité (retourné par list-entite)
	 * @param int $id_d Identifiant unique du document (retourné par list-document)
	 * @return array
	 */
	public function detailDocument($id_e, $id_d)
	{
		return $this->pastellClient->get('/entite/' . $id_e . '/document/'. $id_d );
	}

	/**
	 * get the signed document file
	 * @param int $id_e documentId
	 * @param int $id_d pastell entityId
	 * @return array
	 */
	public function getSignedDocument($id_e, $id_d)
	{
		$path = "entite/$id_e/document/$id_d/file/signature";
		return $this->pastellClient->get($path);
	}

	/**
	 * get documentSigné
	 * @param int $id_e The id of the structure on pastell
	 * @param int $id_d The id of the document on PASTELL
	 * @return Response
	 */
	public function getSignBordereau($id_e, $id_d)
	{
		$path = "entite/$id_e/document/$id_d/file/bordereau";
		return $this->pastellClient->get($path);
	}

	/**
	 * get Annexe sortie I-Parapheur
	 * @param int $id_e The id of the structure on pastell
	 * @param int $id_d The id of the document on PASTELL
	 * @return Response
	 */
	public function getIparapheurAnnexeSortie(int $id_e, string $id_d)
	{
		$path = "entite/$id_e/document/$id_d/file/iparapheur_annexe_sortie";
		return $this->pastellClient->get($path);
	}

	/**
	 * get Annexe sortie I-Parapheur
	 * @param int $id_e The id of the structure on pastell
	 * @param int $id_d The id of the document on PASTELL
	 * @return Response
	 */
	public function getIparapheurHistoriqueFile(int $id_e, string $id_d)
	{
		$path = "entite/$id_e/document/$id_d/file/iparapheur_historique";
		return $this->pastellClient->get($path);
	}

	/**
	 * Récupère le contenu d'un fichier
	 * @param int $id_e Identifiant de l'entité (retourné par list-entite)
	 * @param int $id_d Identifiant unique du document (retourné par list-document)
	 * @param string $field le nom d'un champ du document
	 * @param int $num le numéro du fichier, s'il s'agit d'un champ fichier multiple
	 * @return array
	 */
	public function getFile($id_e, $id_d, $field, $num = 0)
	{
		$documentsRequester = new DocumentsRequester($this->pastellClient);
		if( $num  > 0 ) {
			$field = $field. '/' . $num;
		}

		$document = $documentsRequester->show($id_e, $id_d);
		return $documentsRequester->getFile($document, $field);
	}

	/**
	 * Détail sur un flux
	 *
	 * Récupère l'ensemble des champs d'un flux
	 *
	 * @param string $type Le nom du document ciblé (mailsec, document-a-signer. ...)
	 * @return array
	 */
	public function detailFlux($type)
	{
		return $this->pastellClient->get('/flux/'. $type);
	}


	/**
	 * Retourne tous les documents d'une entité
	 */
	public function getAllConnectors($idEntite) {
		$connectorsRequester = new \PastellClient\Api\ConnectorsRequester($this->pastellClient);
		$listConnectors = $connectorsRequester->all($idEntite);
		return $listConnectors;
	}



	/**
 * @param string $page script php
	 * @param array $data
	 * @param bool $file_transfert attente d'un fichier en retour ?
	 * @return bool|mixed|string retour du webservice
	 */
	public function execute($page, $data = [])
	{
		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur ->setDataSource($conn);
		$pastell = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%',
					'Connecteur.use_pastell' => true
				),
				'contain' => false
			)
		);

		if (!empty($pastell) && $pastell['Connecteur']['use_pastell']) {
			$this->host = $pastell['Connecteur']['host'];
			$this->login = $pastell['Connecteur']['login'];
			$this->pwd = $pastell['Connecteur']['pwd'];

		}
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($curl, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_USERPWD, $this->login . ":" . $this->pwd);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'GET'); //FIXME
		if( Configure::read('Curl.UseProxy' ) ) {
			curl_setopt($curl, CURLOPT_PROXY, Configure::read('Curl.ProxyHost' ) );
		}
		$api = $this->host . "/api/v2/$page";


		curl_setopt($curl, CURLOPT_URL, $api);
		//Hack Pastell pour champ multiple en api V1
		if (!empty($data)) {
			foreach ($data as $key => $value) {
				if (is_array($value)) {
					$data[$key] = json_encode($value);
				}
			}

			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			// pour gérer le cas de la modification
			if( !isset( $data['type']) ) {
				curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
			}
		}

		$response = curl_exec($curl);

		if ($response === false) {
			CakeLog::error(curl_error($curl), 'Pastell');
			throw new Exception(curl_error($curl));
		}
		curl_close($curl);

		$content = null;

		return $this->parseRetour($page, $data, $response, $content);
	}

	/**
	 * Fonction de parse des réponses de pastell
	 * @param string $page script ws
	 * @param array $data paramètres
	 * @param array $retourWS reponse ws
	 * @return bool false si en erreur
	 */
	private function parseRetour($page, $data, $retourWS, $content)
	{
		//CakeLog::info(var_export($retourWS, true), 'pastell');
		$result = json_decode($retourWS, true);

		// Test si le retour est un fichier ou un message json (erreur)
		//CakeLog::info(print_r(['Request' => $page, 'Data' => $data], true), 'pastell');

		if (!empty($result['message'])) {
			return $result['message'];
			//$this->log('Message : ' . $result['message'], 'pastell');
		}
		if (!empty($result['error-message'])) {
			CakeLog::error('Error : ' . $result['error-message'], 'pastell');
			return false;
		}

		if (!empty($result) && is_array($result)) {
			CakeLog::info('Récupération du json Ok', 'pastell');
			return $result;
		}
		if (!empty($content)) {
			CakeLog::info('Récupération du json Ok', 'pastell');
			return $content;
		}
		if ($page==='action.php' && empty($result)) {
			CakeLog::info('Action effectuée', 'pastell');
			return true;
		}
		if (@simplexml_load_string($retourWS)!==false  && empty($result)) {
			CakeLog::info('Classification récupérée', 'pastell');
			return $retourWS;
		}

		CakeLog::error(print_r(array(
			'Request' => $page,
			'Data' => $data ,
			'reponse' => var_export($retourWS, true),
			'reponseData' => $content), true), 'pastell');

		throw new Exception('Aucune réponse du serveur distant', 500);
	}

	/**
	 * Modification d'un document
	 * @param int $id_e identifiant de la collectivité
	 * @param int $id_d identifiant du dossier pastell
	 * @param array $delib
	 * @param string $document (Pdf)
	 * @param array $annexes
	 * @return bool|string
	 * @since 4.2
	 * @version 5.0
	 * result : ok - si l'enregistrement s'est bien déroulé
	 * formulaire_ok : 1 si le formulaire est valide, 0 sinon
	 * message : Message complémentaire
	 *
	 * A noter que pour connaître la liste et les intitulés exacts des champs modifiables,
	 * il convient d'utiliser la fonction document-type-info.php, en lui précisant le type concerné.
	 */
	public function modifDocumentByType($id_e, $id_d, $type, $data, $document, $pjs, $options)
	{
		$success = true;

		$dataForPastell = array(
			'id_e' => $id_e,
			'id_d' => $id_d
		);
		$dataForPastell = array_merge(
			$dataForPastell,
			$this->table_correspondance($data, $type, $options)
		);


		//filtre les eléments vides
		$dataForPastell = array_filter($dataForPastell);
		// Remplissage des données du formulaire PASTELL précédemment créé
		$this->pastellClient->patch('/entite/'.$id_e.'/document/'.$id_d, $dataForPastell);


		if( isset( $document ) && !empty( $document ) || !empty($dataForPastell['pj'])) {
			$fileName = $document['docPrincipale']['Document']['name'];
			$fileContent = file_get_contents($document['docPrincipale']['Document']['path']);
			$fileoptions = array(
				'field_name' => 'document',
				'file_name' => $fileName,
				'file_num' => 0,
				'file_content' => $fileContent
			);
			$this->sendFile($id_e, $id_d, $fileoptions);
		}

		// Partie pour les PJs ajoutées lors de l'envoi
		if (!empty($pjs)) {
			foreach ($pjs as $key => $pj) {
				$pjoptions = array(
					'field_name' => 'pj',
					'file_name' => $pj['Document']['name'],
					'file_num' => $key,
					'file_content' => file_get_contents($pj['Document']['path'])
				);
				$this->sendAnnexeFileToDocument($id_e, $id_d, $pjoptions, $key );
			}
		}

		// EAjout des données en json dans le champ metadonnees
		if( strpos( $type, Configure::read('Pastell.fluxStudioName') ) !== false ) {
			$jsonData = [];
			foreach ($data as $key => $value) {
				$jsonData[] = json_encode([$key => $value]);
			}
			$conn = CakeSession::read('Auth.User.connName');
			$fileName = 'flux_datas.json';
			$file = WORKSPACE_PATH . DS . $conn . DS . $data['Courrier']['reference'] . DS . "{$fileName}";
			file_put_contents($file, json_encode($jsonData));
			$fileoptions = array(
				'field_name' => 'metadonnees',
				'file_name' => $fileName,
				'file_num' => 0,
				'file_content' => file_get_contents($file)
			);
			$this->sendFile($id_e, $id_d, $fileoptions);
		}


		return $success;
	}

	/**
	 * Fonction permettant de transformer les données d'un type de document Pastell
	 * @param type $data
	 * @param type $typeDocument
	 * @return type
	 */
	function table_correspondance($data, $typeDocument, $options) {
		$dataForPastell = [];
		/*if( $typeDocument == 'pdf-generique' ) {
			$dataForPastell = array(
				'libelle' => $data['Courrier']['name'],
				'iparapheur_type' => $options['typeIp'],
				'iparapheur_sous_type' => $options['soustypeIp'],
				'envoi_signature' => true,
				'envoi_mailsec' => true,
				'envoi_depot' => $data['Soustype']['envoi_ged'],
				'envoi_sae' => $data['Soustype']['envoi_sae'],
				'to' => isset( $options['email'] ) ? $options['email'] : (isset($data['Contact']['email']) ? $data['Contact']['email'] : null)
			);
		}
		else*/
		if( !empty($typeDocument) ) {
			$delaiuniteCourrier = '';
			if(isset($data['Courrier']['delai_unite']) ) {
				if ($data['Courrier']['delai_unite'] == '0') {
					$delaiuniteCourrier = 'jour(s)';
				}
				else if ($data['Courrier']['delai_unite'] == '1') {
					$delaiuniteCourrier = 'semaine(s)';
				}
				else if ($data['Courrier']['delai_unite'] == '2') {
					$delaiuniteCourrier = 'mois';
				}
			}
			$this->User = ClassRegistry::init('User');
			$userAffaire = $this->User->find('first', array(
				'fields' => array(
					'User.nom',
					'User.prenom',
					'User.mail',
					'User.numtel',
					'User.username'
				),
				'conditions' => array(
					'User.desktop_id' => $data['Courrier']['affairesuiviepar_id']
				),
				'recursive' => -1
			));

			if (!empty($userAffaire)) {
				$nameAffairesuivie = $userAffaire['User']['prenom'] . ' ' . $userAffaire['User']['nom'];
			}

			$envoiSignature = false;
			$envoiMailsec = false;
			$envoiGed = false;
			$envoiSae = false;
			$hasCheminement = true;
			$soustypeIPCible = $options['soustypeIp'];
			if($typeDocument == Configure::read('Pastell.fluxStudioName').'-visa') {
				$envoiSignature = true;
				$envoiMailsec = false;
				$envoiGed = false;
				$envoiSae = false;
				$hasCheminement = false;
			}
			if($typeDocument == Configure::read('Pastell.fluxStudioName').'-mailsec') {
				$envoiSignature = true;
				$envoiMailsec = true;
				$envoiGed = false;
				$envoiSae = false;
				$hasCheminement = false;
			}
			if($typeDocument == Configure::read('Pastell.fluxStudioName').'-ged') {
				$envoiSignature = true;
				$envoiMailsec = true;
				$envoiGed = true;
				$envoiSae = false;
				$hasCheminement = false;
			}
			if($typeDocument == Configure::read('Pastell.fluxStudioName')) {
				$envoiSignature = true;
				$envoiMailsec = true;
				$envoiGed = true;
				$envoiSae = true;
				$soustypeIPCible = !empty( $data['Soustype']['soustype_parapheur'] ) ? $data['Soustype']['soustype_parapheur'] : $options['soustypeIp'];

				$cheminement = $data['Soustype']['envoi_signature'] + $data['Soustype']['envoi_mailsec'] + $data['Soustype']['envoi_ged'] + $data['Soustype']['envoi_sae'];
				if( $cheminement == 0 ) {
					$hasCheminement = false;
				}
			}

			$dataForPastell = array(
				'libelle' => $data['Courrier']['name'],
				'reference' => $data['Courrier']['reference'],
				'intitule' => $data['Courrier']['name'],
				'origine' => $data['Origineflux']['name'],
				'objet' => $data['Courrier']['objet'],
				'priorite' => $data['Courrier']['priorite'],
				'suivi' => $nameAffairesuivie,
				'delai' => $data['Courrier']['delai_nb']. ' ' .$delaiuniteCourrier,
				'nom_organisme' => $data['Organisme']['name'],
				'entree' => $data['Courrier']['datereception'],
				'creation' => $data['Courrier']['date'],
				'iparapheur_type' => $options['typeIp'],
				'iparapheur_sous_type' => $soustypeIPCible,
				'envoi_signature' => $hasCheminement ? $data['Soustype']['envoi_signature'] : $envoiSignature,
				'envoi_mailsec' => $hasCheminement ? $data['Soustype']['envoi_mailsec'] : $envoiMailsec,
				'envoi_depot' => $hasCheminement ? $data['Soustype']['envoi_ged'] : $envoiGed,
				'envoi_sae' => $hasCheminement ? $data['Soustype']['envoi_sae'] : $envoiSae,
				'to' => isset( $options['email'] ) ? $options['email'] : (isset($data['Contact']['email']) ? $data['Contact']['email'] : null)
			);
		}

		return $dataForPastell;
	}

	/**
	 * @param int $id_d
	 * @return array
	 */
	public function delete($id_e, $id_d) {
		$this->action($id_e, $id_d, 'supression');
	}

	/**
	 * get documentSigné
	 * @param int $id_e The id of the structure on pastell
	 * @param int $id_d The id of the document on PASTELL
	 * @return Response
	 */
	public function getJournal($id_e, $id_d)
	{
		$path = '/Journal?id_e=' . $id_e . '&id_d=' . $id_d;
		return $this->pastellClient->get($path);
	}

	/**
	 * get documentSigné
	 * @param int $id_j The id of the journal
	 * @return Response
	 */
	public function getInfoJournal($id_j)
	{
		$path = '/journal/' . $id_j;
		return $this->pastellClient->get($path);
	}


	/**
	 * @param int $id_d
	 * @return array
	 */
	public function getPreuveMailsec($id_e, $id_d, $preuve) {
		return $this->getFile($id_e, $id_d, 'preuve');
	}

	/**
	 * @param $connector
	 * @return \Pastell\Model\Connector
	 * @throws \Psr\Http\Client\ClientExceptionInterface
	 * Retourne le tout premier document d'une entité donnée
	 */
	public function getOneConnector($id_e, $idCe) {
		$connectorsRequester = new ConnectorsRequester($this->pastellClient);
		$oneConnector = $connectorsRequester->show($id_e, $idCe);
		return $oneConnector;
	}


	/**
	 * @param $connector
	 * @return \Pastell\Model\Connector
	 * @throws \Psr\Http\Client\ClientExceptionInterface
	 * Retourne le tout premier document d'une entité donnée
	 */
	public function getAllConnectorByFluxAndByType($id_e, $flux, $type) {
		$allConnector = $this->pastellClient->get('/entite/'.$id_e.'/flux?flux='.$flux.'&type='.$type);
		return $allConnector;
	}

}
