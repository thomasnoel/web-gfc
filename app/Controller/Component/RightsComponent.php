<?php

/**
 *
 * RightComponent component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class RightsComponent extends Component {

    /**
     * Controller
     *
     * @var type
     */
    public $controller;

    /**
     * Component initialization
     *
     * @access public
     * @param type $controller
     * @return void
     */
    public function initialize(Controller $controller) {
        $this->controller = $controller;
    }

    /**
     *
     * @param array $requestData
     * @return array
     */
    public function parseRightsData($data) {
        $options = array(
            'tabs' => array(),
            'env' => array(),
            'workflow' => array(),
            'context' => array(),
            'rightSets' => array()
        );
        foreach ($data as $key => $val) {
            if (in_array($key, array('administration', 'aiguillage', 'creation', 'edition', 'validation', 'documentation')) && $val) {
                $options['rightSets'][] = $key;
            } else if (in_array($key, array('create_infos', 'get_infos', 'set_infos', 'get_meta', 'set_meta', 'get_taches', 'set_taches', 'get_affaire', 'set_affaire')) && $val) {
                $options['tabs'][] = $key;
            } else if (in_array($key, array('mes_services', 'recherches', 'suppression_flux', 'carnet_adresse', 'statistiques')) && $val) {
                $options['env'][] = $key;
            } else if (in_array($key, array('document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen')) && $val) {
                $options['context'][] = $key;
            } else if (in_array($key, array('envoi_cpy', 'envoi_wkf', 'envoi_ged', 'consult_scan', 'action_carnet_adresse_flux')) && $val) {
                $options['workflow'][] = $key;
            }
        }
        return $options;
    }

}

?>
