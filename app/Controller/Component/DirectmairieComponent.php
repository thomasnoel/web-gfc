<?php
/**
 * Class DirectmairieComponent
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 * @copyright  Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
App::uses('HttpSocket', 'Network/Http');
class DirectmairieComponent extends Component {

	public $wsto;
	public $wsdl;
	public $boundary;

	function DirectmairieComponent() {
		$conn = CakeSession::read('Auth.User.connName');
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$this->Connecteur ->setDataSource($conn);
		$hasDirectmairieActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Direct%',
					'Connecteur.use_dm' => true
				),
				'contain' => false
			)
		);
		if( !empty($hasDirectmairieActif) ) {
			$this->wsto = $hasDirectmairieActif['Connecteur']['host'];
			$this->host = $hasDirectmairieActif['Connecteur']['host'];
			$this->login = $hasDirectmairieActif['Connecteur']['login'];
			$this->pwd = $hasDirectmairieActif['Connecteur']['pwd'];
			$this->apiKey = $hasDirectmairieActif['Connecteur']['dm_apikey'];
		}
	}

	/**
	 * Paramètres pour les requêtes en GET
	 * @return array
	 */
	function getParams() {
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$dm = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Direct-Mairie%'
				),
				'contain' => false
			)
		);

		$request = array(
			'method' => 'GET',
			'auth' => array(
				'method' => 'Basic',
				'user' => $this->login,
				'pass' => $this->pwd
			)
		);
		return $request;
	}

	/**
	 * Fonction de test de connexion simple
	 * @return array|bool|false|HttpSocketResponse|string
	 */
	function echoTest() {
		return $this->getConnexion();
	}


	/**
	 * Fonction permettant de remonter les informations de connexion
	 */
	public function getConnexion() {
		$httpSocket = new HttpSocket(
			[
				'ssl_verify_peer' => false
			]
		);

		if( Configure::read('Curl.UseProxy' ) ) {
			$httpSocket->configProxy(Configure::read('Curl.ProxyHost' ) );
		}
		$request = $this->getParams();
		try {
			$infos = $httpSocket->get($this->host . '/api/issue-groups', [], $request);
		}
		catch (Exception $e) {
			$infos = $e->getMessage();
		}
		return $infos;
	}



	/**
	 * Fonction permettant de remonter les informations de la remontée
	 * @param $issueId
	 */
	public function getAllIssues() {
		$httpSocket = new HttpSocket(
			[
				'ssl_verify_peer' => false
			]
		);

		if( Configure::read('Curl.UseProxy' ) ) {
			$httpSocket->configProxy(Configure::read('Curl.ProxyHost' ) );
		}
		$request = $this->getParams();
		try {
			$infos = $httpSocket->get($this->host  . '/api/issues?status=CREATED', [], $request );
		}
		catch (Exception $e) {
			$infos = $e->getMessage();
		}

		return $infos;
	}

	/**
	 * Fonction permettant de remonter les informations de la remontée
	 * @param $issueId
	 */
	public function getIssue($issueId) {
		$httpSocket = new HttpSocket(
			[
				'ssl_verify_peer' => false
			]
		);
		if( Configure::read('Curl.UseProxy' ) ) {
			$httpSocket->configProxy(Configure::read('Curl.ProxyHost' ) );
		}
		$request = $this->getParams();
		try {
			$infos = $httpSocket->get($this->host  . '/api/issues/'.$issueId, [], $request );
		}
		catch (Exception $e) {
			$infos = $e->getMessage();
		}

		return $infos;
	}

	/**
	 * Fonction permettant de remonter les informations des images associées à la remontée
	 * @param issueId
	 * @param pictureId
	 */
	public function getPictures($issueId, $pictureId) {
		$httpSocket = new HttpSocket(
			[
				'ssl_verify_peer' => false
			]
		);
		if( Configure::read('Curl.UseProxy' ) ) {
			$httpSocket->configProxy(Configure::read('Curl.ProxyHost' ) );
		}
		$request = $this->getParams();
		try {
			$datasPictures = $httpSocket->get($this->host  . '/api/issues/'.$issueId.'/pictures/'.$pictureId.'/bytes', [], $request );
		}
		catch (Exception $e) {
			$datasPictures = $e->getMessage();
		}

		return $datasPictures;
	}

	/**
	 * Fonction permettant de remonter le token de connexion
	 * @param mail
	 * @param pwd
	 */
	public function getToken($login, $pwd) {
		$httpSocket = new HttpSocket(
			[
				'ssl_verify_peer' => false
			]
		);
		if( Configure::read('Curl.UseProxy' ) ) {
			$httpSocket->configProxy(Configure::read('Curl.ProxyHost' ) );
		}
		$request = array(
			'method' => 'POST',
			'auth' => array(
				'method' => 'Basic',
				'user' => $this->login,
				'pass' => $this->pwd
			),
			'header' => array(
				'Content-Type' => 'application/json'
			)
		);
		$data = array(
			"email" => $login,
			"password" => $pwd
		);
		try {
			$datasToken = $httpSocket->post($this->host  . '/api/authentications', json_encode( $data ), $request );
		}
		catch (Exception $e) {
			$datasToken = $e->getMessage();
		}

		return $datasToken;
	}


	function makeCurlFile($file){
		$mime = mime_content_type($file);
		$info = pathinfo($file);
		$name = $info['basename'];
		$output = new CURLFile($file, $mime, $name);
		return $output;
	}
}

