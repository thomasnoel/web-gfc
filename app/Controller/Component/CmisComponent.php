<?php

/**
 *
 * CmisComponent component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright  Développé par Libriciel SCOP
 * @link http://libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class CmisComponent extends Component {

    var $client;
    var $folder;

    function CmisComponent() {

    }

    function CmisComponent_Service() {
//		require_once(ROOT.DS.APP_DIR.DS.'Vendor'.DS.'apache_chemistry'.DS.'cmis_repository_wrapper.php');
        require_once( ROOT . DS . APP_DIR . DS . 'Vendor' . DS . 'apache_chemistry' . DS . 'cmis_repository_wrapper.php' );
//		require_once(ROOT.DS.APP_DIR.DS.'Vendor'.DS.'apache_chemistry'.DS.'cmis_service.php');

        $this->Connecteur = ClassRegistry::init('Connecteur');
        $cmis = $this->Connecteur->find(
                'first', array(
            'conditions' => array(
                'Connecteur.name ILIKE' => '%GED%'
            ),
            'contain' => false
                )
        );

        if ($cmis['Connecteur']['use_ged']) {
            $this->client = new CMISService(
                    $cmis['Connecteur']['ged_url'], $cmis['Connecteur']['ged_login'], $cmis['Connecteur']['ged_passwd']
            );
            $this->folder = $this->client->getObjectByPath($cmis['Connecteur']['ged_repo']);
//			$this->client = new CMISService(Configure::read('CMIS_HOST'), Configure::read('CMIS_LOGIN'), Configure::read('CMIS_PWD'));
//			$this->folder = $this->client->getObjectByPath(Configure::read('CMIS_REPO'));
        }
    }

    function list_objs($objs) {
        foreach ($objs->objectList as $obj) {
            if ($obj->properties['cmis:baseTypeId'] == "cmis:document") {
                print "Document: " . $obj->properties['cmis:name'] . "\n";
            } elseif ($obj->properties['cmis:baseTypeId'] == "cmis:folder") {
                print "Folder: " . $obj->properties['cmis:name'] . "\n";
            } else {
                print "Unknown Object Type: " . $obj->properties['cmis:name'] . "\n";
            }
        }
    }

    function check_response($client) {
        if ($this->client->getLastRequest()->code > 299) {
            print "There was a problem with this request!\n";
            exit(255);
        }
    }

}

?>
