<?php

/**
 *
 * PasswordComponent component class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */

	class PasswordComponent extends Component {

		/**
		* Controller
		*
		* @var type
		*/
		public $controller;

		/**
		* Component initialization
		*
		* @access public
		* @param type $controller
		* @return void
		*/
		public function initialize(Controller $controller) {
			$this->controller = $controller;
		}


		/**
		* Password generator function
		*
		*/
		function generatePassword ($length = 8){
			// inicializa variables
			$motdepasse = "";
			$i = 0;
			$possible = "0123456789bcdfghjkmnpqrstvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ,;.!?*+-";

			// agrega random
			while ($i < $length){
				$char = substr($possible, mt_rand(0, strlen($possible)-1), 1);

				if (!strstr($motdepasse, $char)) {
					$motdepasse .= $char;
					$i++;
				}
			}
			return $motdepasse;
		}
	}
?>
