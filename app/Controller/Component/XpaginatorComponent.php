<?php
    /**
     * Paginator Component extends PaginatorComponent
     *
     * PHP 5
     *
     * @package       Cake.Controller.Component
     */
    App::uses( 'PaginatorComponent', 'Controller/Component' );

    /**
     * This component is used to handle automatic model data pagination.  The primary way to use this
     * component is to call the paginate() method. There is a convenience wrapper on Controller as well.
     *
     * @package       Cake.Controller.Component
     * @link http://book.cakephp.org/2.0/en/core-libraries/components/pagination.html
     */
    class XpaginatorComponent extends PaginatorComponent
    {
        /**
         * Validate that the desired sorting can be performed on the $object.  Only fields or
         * virtualFields can be sorted on.  The direction param will also be sanitized.  Lastly
         * sort + direction keys will be converted into the model friendly order key.
         *
         * You can use the whitelist parameter to control which columns/fields are available for sorting.
         * This helps prevent users from ordering large result sets on un-indexed values.
         *
         * @param Model $object The model being paginated.
         * @param array $options The pagination options being used for this request.
         * @param array $whitelist The list of columns that can be used for sorting.  If empty all keys are allowed.
         * @return array An array of options with sort + direction removed and replaced with order if possible.
         */
        public function validateSort(Model $object, array $options, array $whitelist = array()) {
            if (isset($options['sort'])) {
                $direction = null;
                if (isset($options['direction'])) {
                    $direction = strtolower($options['direction']);
                }
                if ($direction != 'asc' && $direction != 'desc') {
                    $direction = 'asc';
                }
                $options['order'] = array($options['sort'] => $direction);
            }

            if (!empty($whitelist) && isset($options['order']) && is_array($options['order'])) {
                $field = key($options['order']);
                if (!in_array($field, $whitelist)) {
                    $options['order'] = null;
                }
            }

            if (!empty($options['order']) && is_array($options['order'])) {
                // Les champs du querydata sur lesquels on va permettre le tri
                $fields = array();
                foreach( (array)Hash::extract( $options, 'fields' ) as $field ) {
                    $fields[] = preg_replace( '/^.*AS "(.*)__(.*)"$/m', '\1.\2', str_replace( "\n", '', $field ) );
                }

                $order = array();
                foreach ($options['order'] as $key => $value) {
                    $field = $key;
                    $alias = $object->alias;
                    if (strpos($key, '.') !== false) {
                        list($alias, $field) = explode('.', $key);
                    }

                    if ($object->hasField($field)) {
                        $order[$alias . '.' . $field] = $value;
                    } elseif ($object->hasField($key, true)) {
                        $order[$field] = $value;
                    } elseif (isset($object->{$alias}) && $object->{$alias}->hasField($field, true)) {
                        $order[$alias . '.' . $field] = $value;
                    } elseif( in_array( "{$alias}.{$field}", $fields ) ) {
                        $order[$alias . '.' . $field] = $value;
                    }
                }
                $options['order'] = $order;
            }

            return $options;
        }
    }
?>