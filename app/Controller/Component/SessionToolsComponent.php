<?php

/**
 * Set of tools to use webgfc session recorded informations
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller.Component
 */
class SessionToolsComponent extends AclComponent {

	/**
	 * Component intialisation
	 *
	 * @access public
	 * @param Controller $controller
	 * @return void
	 */
	public function initialize(Controller $controller) {
		$this->Session = $controller->Session;
	}

	/**
	 * Récupération de la liste des rôle d'un utilisateur stockés en session (PHP)
	 *
	 * @logical-group Environnement
	 * @user-profile User
	 *
	 * @access public
	 * @return array
	 */
	public function getDesktopList() {
		$desktops = array();
		$sd = $this->Session->read('Auth.User.SecondaryDesktops');
		if (!empty($sd)) {
			$desktops = array_merge(array($this->Session->read('Auth.User.Desktop.id')), Set::classicExtract($sd, '{n}.id'));
		} else {
			$desktops[] = $this->Session->read('Auth.User.Desktop.id');
		}
		return $desktops;
	}

}

?>
