<?php

/**
 * Méta-données
 *
 * Sousoperations controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class SousoperationsController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Sousoperations';

    /**
     * Récupération de la liste des méta-données (ajax)
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
//	public function getSousoperations() {
//        $this->Metadonnee->recursive = -1;
//		$metadonnees_tmp = $this->Metadonnee->find("all", array('order' => 'Metadonnee.name'));
//		$metadonnees = array();
//		foreach ($metadonnees_tmp as $item) {
//			$item['right_edit'] = true;
//			$item['right_delete'] = true;
//			$metadonnees[] = $item;
//		}
//		$this->set('metadonnees', $metadonnees);
//	}

    /**
     * Ajout d'une méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function add($operation_id) {

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $sousoperation = $this->request->data;

            $operation = $this->Sousoperation->Operation->find(
                    'first', array(
                'conditions' => array(
                    'Operation.id' => $operation_id
                ),
                'contain' => false
                    )
            );

            $Soustype = ClassRegistry::init('Soustype');
            $soustype = array();
            $soustype['Soustype']['name'] = $sousoperation['Sousoperation']['name'];
            $soustype['Soustype']['type_id'] = $operation['Operation']['type_id'];
            $Soustype->create($soustype);
            $savesoustype = $Soustype->save();
            if ($savesoustype) {
                $sousoperation['Sousoperation']['soustype_id'] = $Soustype->id;
            }

            $this->Sousoperation->create($sousoperation);
            if ($this->Sousoperation->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        }
        $this->set('operationId', $operation_id);
    }

    /**
     * Edition d'un méta-donnée
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function edit($id = null) {
        if (!empty($this->request->data)) {

            $sousoperation = $this->request->data;

            // on regarde les anciens enregistrements entre la sous operation et les postes
            $records = $this->Sousoperation->DesktopmanagerSousoperation->find(
                'all',
                array(
                    'fields' => array(
                        "DesktopmanagerSousoperation.intituleagent_id",
                        "DesktopmanagerSousoperation.ordre",
                        "DesktopmanagerSousoperation.sousoperation_id",
                        "DesktopmanagerSousoperation.id"
                    ),
                    'conditions' => array(
                        "DesktopmanagerSousoperation.sousoperation_id" => $this->request->data['Sousoperation']['id']
                    ),
                    'contain' => false
                )
            );

            foreach ($records as $k => $value) {
                $oldrecordsids[$value['DesktopmanagerSousoperation']['ordre']][] = $value['DesktopmanagerSousoperation']['intituleagent_id'];
            }

            // on regarde les nouveautés
            $nouveauxids = Hash::extract($this->request->data, "DesktopmanagerSousoperation.DesktopmanagerSousoperation.{n}");
            foreach ($nouveauxids as $l => $val) {
                if(!empty($val['ordre']) && $val['ordre'] != '') {
                    $newids[$val['ordre']] = $val['intituleagent_id'];
                }
            }

            foreach ($oldrecordsids as $key => $values) {
                $idsenmoins[$key] = array_diff($values, $newids[$key]);
            }
            foreach ($newids as $key => $values) {
                $idsenplus[$key] = array_diff($values, $oldrecordsids[$key]);
            }


            // on met à jour les sous-types qui ne sont plus liés au circuit en cours
            if (!empty($idsenmoins)) {
                foreach ($idsenmoins as $ordre => $idmoins) {
                    if (!empty($ordre)) {
                        $success = $this->Sousoperation->DesktopmanagerSousoperation->deleteAll(
                                array(
                                    'DesktopmanagerSousoperation.sousoperation_id' => $this->request->data['Sousoperation']['id'],
                                    'DesktopmanagerSousoperation.intituleagent_id' => array_values($idmoins),
                                    'DesktopmanagerSousoperation.ordre' => $ordre
                                )
                        );
                    }
                }
            }

            if (!empty($idsenplus)) {
                foreach ($idsenplus as $ordre => $idplus) {
                    if (!empty($idplus)) {
                        foreach ($idplus as $idTosave) {
                            $newIntitule['DesktopmanagerSousoperation']['ordre'] = $ordre;
                            $newIntitule['DesktopmanagerSousoperation']['sousoperation_id'] = $sousoperation['Sousoperation']['id'];
                            $newIntitule['DesktopmanagerSousoperation']['typeetape'] = $sousoperation['DesktopmanagerSousoperation']['DesktopmanagerSousoperation'][$ordre]['typeetape'];
                            $newIntitule['DesktopmanagerSousoperation']['intituleagent_id'] = $idTosave;
                            $this->Sousoperation->DesktopmanagerSousoperation->create($newIntitule);
                            $this->Sousoperation->DesktopmanagerSousoperation->save();
                        }
                    }
                }
            }


//            $sousoperation['DesktopmanagerSousoperation']['DesktopmanagerSousoperation'] = $nouveauxids;
//debug($sousoperation);

            $sousoperationEdit = $this->Sousoperation->find('first', array(
                'conditions' => array(
                    'Sousoperation.id' => $this->request->data['Sousoperation']['id']
                ),
                'joins' => array(
                    $this->Sousoperation->join('Soustype')
                ),
                'contain' => false
            ));
//debug($sousoperationEdit);
            $soustype = $this->Sousoperation->Soustype->find('first', array(
                'conditions' => array(
                    'Soustype.id' => $sousoperationEdit['Sousoperation']['soustype_id']
                ),
                'contain' => false
            ));
            $this->Sousoperation->Soustype->updateAll(
                    array('Soustype.name' => '\'' . $this->request->data['Sousoperation']['name'] . '\''), array('Soustype.id' => $soustype['Soustype']['id'])
            );

            $this->Jsonmsg->init();
            $this->Sousoperation->create($sousoperation);
            if ($this->Sousoperation->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $querydata = array(
                'contain' => array(
                    'Intituleagent' => array(
                        'order' => array('DesktopmanagerSousoperation.ordre ASC')
                    )
                ),
                'conditions' => array(
                    'Sousoperation.id' => $id
                )
            );
            $this->request->data = $this->Sousoperation->find('first', $querydata);

            foreach ($this->request->data['Intituleagent'] as $i => $values) {
                $this->request->data[$values['DesktopmanagerSousoperation']['ordre']]['DesktopmanagerSousoperation'][] = $values['DesktopmanagerSousoperation'];
                unset($this->request->data['Intituleagent']);
                unset($this->request->data['Sousoperation']);
            }
        }

//        $bureaux = $this->request->data['Desktopmanager'];
//        $intitules = $this->request->data['Intituleagent'];
        $intitules = $this->request->data;
//debug($intitules);
//        $bureaux = $this->request->data;
        $sousoperations_tmp = $this->request->data;
        $sousoperations = array();
        foreach ($sousoperations_tmp as $item) {

            $item['right_add'] = true;
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $sousoperations[] = $item;
        }


        $sousoperation = $this->Sousoperation->find('first', array('conditions' => array('Sousoperation.id' => $id), 'contain' => false));
        $operationId = $sousoperation['Sousoperation']['operation_id'];
        $sousoperationName = $sousoperation['Sousoperation']['name'];
        $this->set('sousoperations', $sousoperations);
        $this->set('sousoperationName', $sousoperationName);
        $this->set('sousoperationId', $id);
//        $operationId = $this->request->data['Sousoperation']['operation_id'];
        $this->set('operationId', $operationId);
//        $this->set( 'bureaux', $bureaux );
        $this->set('intitules', $intitules);
        $desktopsmanagers = $this->Sousoperation->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => 'Desktopmanager.name ASC'));
        $this->set('desktopsmanagers', $desktopsmanagers);
        $intitulesagents = $this->Sousoperation->DesktopmanagerSousoperation->Intituleagent->find(
            'all',
            array(
                'contain' => array(
                    'Desktopmanager' => array(
                        'order' => array(
                            'Desktopmanager.name ASC'
                        )
                    )
                ),
                'order' => 'Intituleagent.name ASC'
            )
        );
//debug($intitulesagents);
        $this->set('intitulesagents', $intitulesagents);


        $intitulesagents2 = $this->Sousoperation->Intituleagent->find('list', array('order' => array('Intituleagent.name ASC')));
        $this->set('intitulesagents2', $intitulesagents2);

        $types = array(
            CAKEFLOW_SIMPLE => 'Simple',
            CAKEFLOW_CONCURRENT => 'Concurrent [OU]',
            CAKEFLOW_COLLABORATIF => 'Collaboratif [ET]'
        );
        $this->set('types', $types);


//debug($this->request->data);
    }

    /**
     * Suppression d'une méta-donnée (delete)
     *
     * @logical-group Méta-données
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant de la méta-donnée
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Sousoperation->begin();

        $sousop = $this->Sousoperation->find('first', array(
            'conditions' => array(
                'Sousoperation.id' => $id
            ),
            'contain' => false
        ));
        $soustypeId = $sousop['Sousoperation']['soustype_id'];

        if (!empty($soustypeId)) {
            $this->Sousoperation->Soustype->delete($soustypeId);
        }
        if ($this->Sousoperation->delete($id)) {
            $this->Sousoperation->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Sousoperation->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Ajout de la liste des utilisateurs intervenants sur 1 OP
     *
     * @logical-group Operations
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function addUsers($sousoperation_id) {

        $this->Jsonmsg->init(__d('default', 'save.error'));
        $intitulesagents = $this->Sousoperation->DesktopmanagerSousoperation->Intituleagent->find(
                'all', array(
            'contain' => array(
                'Desktopmanager' => array(
                    'order' => array(
                        'Desktopmanager.name ASC'
                    )
                )
            ),
            'order' => 'Intituleagent.name ASC'
                )
        );
        $sousoperation = $this->Sousoperation->find(
                "first", array(
            'conditions' => array(
                'Sousoperation.id' => $sousoperation_id
            ),
            'contain' => false,
            'order' => 'Sousoperation.name'
                )
        );
        $sousoperationId = $sousoperation_id;
        $operationId = $sousoperation['Sousoperation']['operation_id'];
        $desktopsmanagers = $this->Sousoperation->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => 'Desktopmanager.name ASC'));

        if (!empty($this->request->data)) {


//            $Etapeop = ClassRegistry::init('Etapeop');
//            $etapeop['Etapeop']['name'] = 'Etape '.$this->request->data['DesktopmanagerSousoperation']['ordre'];
//            $etapeop['Etapeop']['sousoperation_id'] = $this->request->data['DesktopmanagerSousoperation']['sousoperation_id'];
//            $etapeop['Etapeop']['typeetape'] = $this->request->data['DesktopmanagerSousoperation']['typeetape'];
//            $Etapeop->create($etapeop);
//            $Etapeop->save();

            if ($this->request->data['DesktopmanagerSousoperation']['typeetape'] == '1') {
                $this->request->data['DesktopmanagerSousoperation']['intituleagent_id'] = $this->request->data['DesktopmanagerSousoperation']['intituleagent_id'][0];
                $this->Sousoperation->DesktopmanagerSousoperation->create($this->request->data);
                $this->Sousoperation->DesktopmanagerSousoperation->save();
            } else {
                $toSave = array();
                foreach ($this->request->data['DesktopmanagerSousoperation']['intituleagent_id'] as $i => $value) {
                    $toSave[$i]['DesktopmanagerSousoperation']['intituleagent_id'] = $value;
                    $toSave[$i]['DesktopmanagerSousoperation']['ordre'] = $this->request->data['DesktopmanagerSousoperation']['ordre'];
                    $toSave[$i]['DesktopmanagerSousoperation']['typeetape'] = $this->request->data['DesktopmanagerSousoperation']['typeetape'];
                    $toSave[$i]['DesktopmanagerSousoperation']['sousoperation_id'] = $this->request->data['DesktopmanagerSousoperation']['sousoperation_id'];
                    unset($this->request->data['Sousoperation']);
                }

//debug($toSave);
                $this->Sousoperation->DesktopmanagerSousoperation->create();
                $this->Sousoperation->DesktopmanagerSousoperation->saveAll($toSave);
                $this->Jsonmsg->valid(__d('default', 'save.ok'));
            }
            $this->Jsonmsg->send();
        }
        $this->set(compact('sousoperation', 'desktopsmanagers', 'sousoperationId', 'intitulesagents', 'operationId'));

        $types = array(
            CAKEFLOW_SIMPLE => 'Simple',
            CAKEFLOW_CONCURRENT => 'Concurrent [OU]',
            CAKEFLOW_COLLABORATIF => 'Collaboratif [ET]'
        );
        $this->set('types', $types);
    }

    /**
     *  Suppresson d'un bureau associé au sous-type d'opération
     *
     */
    public function deleteAssociation($sousoperation_id, $ordre) {

        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Sousoperation->DesktopmanagerSousoperation->begin();

        $querydata = array(
            'contain' => false,
            'conditions' => array(
                'DesktopmanagerSousoperation.ordre' => $ordre,
                'DesktopmanagerSousoperation.sousoperation_id' => $sousoperation_id
            )
        );
        $association = $this->Sousoperation->DesktopmanagerSousoperation->find('all', $querydata);
        if (!empty($association)) {
            $dIdSsOPId = Hash::extract($association, '{n}.DesktopmanagerSousoperation.id');
        }
//debug($dIdSsOPId);

        if (!empty($association)) {
            if ($this->Sousoperation->DesktopmanagerSousoperation->delete($dIdSsOPId)) {
                $this->Sousoperation->DesktopmanagerSousoperation->commit();
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            } else {
                $this->Sousoperation->DesktopmanagerSousoperation->rollback();
            }
        }
        $this->Jsonmsg->send();
    }

}

?>
