<?php

/**
 * Compteurs
 *
 * Compteurs controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		Controller
 *
 * based on Web-delib compteurs / sequences
 * @link https://adullact.net/projects/webdelib/
 */

//TODO: supprimer les controlleurs inutilisés
class CompteursController extends AppController {
//
//	/**
//	 *
//	 * @var type
//	 */
//	public $name = 'Compteurs';
//
//	/**
//	 *
//	 * @var type
//	 */
//	public $components = array('Security');
//
//	/**
//	 *
//	 */
//	public function beforeFilter() {
//		if (property_exists($this, 'demandePost'))
//			call_user_func_array(array($this->Security, 'requirePost'), $this->demandePost);
//		parent::beforeFilter();
//	}
//
//	/**
//	 *
//	 */
//	public function index() {
//		$this->set('compteurs', $this->Compteur->find('all'));
//	}
//
//	/**
//	 *
//	 * @param type $id
//	 */
//	public function view($id = null) {
//		if (!$this->Compteur->exists()) {
//			$this->Session->setFlash('Invalide id pour le compteur');
//			$this->redirect('/compteurs/index');
//		} else
//			$this->set('compteur', $this->Compteur->read(null, $id));
//	}
//
//	/**
//	 *
//	 */
//	public function add() {
//		$sortie = false;
//		if (!empty($this->request->data)) {
//			if ($this->Compteur->save($this->request->data)) {
//				$this->Session->setFlash('Le compteur \'' . $this->request->data['Compteur']['nom'] . '\' a &eacute;t&eacute; ajout&eacute;');
//				$sortie = true;
//			} else
//				$this->Session->setFlash('Veuillez corriger les erreurs ci-dessous.');
//		}
//		if ($sortie)
//			$this->redirect('/compteurs/index');
//		else {
//			$this->set('sequences', $this->Compteur->Sequence->find('list'));
//			$this->render('edit');
//		}
//	}
//
//	/**
//	 *
//	 * @param type $id
//	 */
//	public function edit($id = null) {
//		$sortie = false;
//		if (empty($this->request->data)) {
//			$this->request->data = $this->Compteur->read(null, $id);
//			if (empty($this->request->data)) {
//				$this->Session->setFlash('Invalide id pour le compteur');
//				$sortie = true;
//			}
//		} else {
//			if ($this->Compteur->save($this->request->data)) {
//				$this->Session->setFlash('Le compteur \'' . $this->request->data['Compteur']['nom'] . '\' a &eacute;t&eacute; modifi&eacute;');
//				$sortie = true;
//			} else
//				$this->Session->setFlash('Veuillez corriger les erreurs ci-dessous.');
//		}
//		if ($sortie)
//			$this->redirect('/compteurs/index');
//		else
//			$this->set('sequences', $this->Compteur->Sequence->find('list'));
//	}
//
//	/**
//	 *
//	 * @param type $id
//	 */
//	public function delete($id = null) {
//		$compteur = $this->Compteur->read('id, nom', $id);
//		if (empty($compteur)) {
//			$this->Session->setFlash('Invalide id pour le compteur');
//		} elseif (!empty($compteur['Typeseance'])) {
//			$this->Session->setFlash('Le compteur \'' . $compteur['Compteur']['nom'] . '\' est utilis&eacute; par un type de s&eacute;ance. Suppression impossible.');
//		} elseif ($this->Compteur->del($id)) {
//			$this->Session->setFlash('La compteur \'' . $compteur['Compteur']['nom'] . '\' a &eacute;t&eacute; supprim&eacute;');
//		}
//		$this->redirect('/compteurs/index');
//	}
//
}

?>
