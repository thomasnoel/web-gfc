<?php
/**
 * Flux
 *
 * MetadonneeApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */

class MetadonneeApiController extends AppController
{

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Metadonnee', 'Collectivite'];
	public $components = ['Api'];


	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$metadonnees = $this->getMetadonnees( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($metadonnees),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	public function find($metadonneeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$metadonnee = $this->getMetadonnee($metadonneeId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($metadonnee),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	public function add()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		// Jeu d'essai
		/*$this->request->data = '{
			"name": "Metadonnee principal"
		}';*/

		$metadonnee = json_decode($this->request->input(), true);
		$res = $this->Metadonnee->save($metadonnee);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Metadonnee->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['metadonneeId' => $res['Metadonnee']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	public function delete($metadonneeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getMetadonnee($metadonneeId);
			$this->Metadonnee->id = $metadonneeId;
			$this->Metadonnee->delete();
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'status' => 200,
			'body' => json_encode(['message' => 'Metadonnee supprimée']),
			'type' => 'application/json'
		]);
	}


	public function update($metadonneeId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getMetadonnee($metadonneeId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$metadonnee = $this->sanitizeMetadonnee($this->request->input());
		$metadonnee['id'] = $metadonneeId;
		$res = $this->Metadonnee->save($metadonnee);
		if (!$res) {
			return $this->formatCakePhpValidationMessages($this->Metadonnee->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['typeId' => $res['Metadonnee']['id']]),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	private function sanitizeMetadonnee($jsonData)
	{
		$authorizedFields = ['name'];
		return array_intersect_key(json_decode($jsonData, true) ?? [], array_flip($authorizedFields));
	}


	private function getMetadonnees( $limit, $offset )
	{
		$conditions = array(
			'fields' => ['id', 'name'],
			'limit' => $limit,
			'offset' => $offset
		);
		$metadonnees = $this->Metadonnee->find('all', $conditions);

		return Hash::extract($metadonnees, "{n}.Metadonnee");
	}


	private function getMetadonnee($metadonneeId)
	{
		$metadonnee = $this->Metadonnee->find('first', [
			'fields' => ['Metadonnee.id', 'Metadonnee.name'],
			'conditions' => ['Metadonnee.id' => $metadonneeId],
			'contain' => ['Selectvaluemetadonnee' => [
				'fields' => ['Selectvaluemetadonnee.id', 'Selectvaluemetadonnee.name']
			]]
		]);

		if (empty($metadonnee)) {
			throw new NotFoundException('Metadonnee non trouvé / inexistant');
		}

		$formattedMetadonnee = Hash::extract($metadonnee, "Metadonnee");

		foreach ($metadonnee['Selectvaluemetadonnee'] as &$selectvalue) {
			unset($selectvalue['Selectvaluemetadonnee']);
		}

		$formattedMetadonnee['selectvalues'] = $metadonnee['Selectvaluemetadonnee'] ?? [];

		return $formattedMetadonnee;
	}


}
