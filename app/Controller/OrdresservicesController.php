<?php

/**
 * Ordresservices
 *
 * Ordresservices controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class OrdresservicesController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Ordresservices';
    public $uses = array('Ordreservice');

    /**
     * Gestion des opérations interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'marcheSAERP', true)
        ));
    }

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('ajaxformdata', 'export');
    }

    /**
     * Ajout d'un e opération
     *
     * @logical-group Ordresservices
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add($marcheId) {

        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $ordreservice = $this->request->data;
            $ordreservice['Ordreservice']['name'] = $ordreservice['Ordreservice']['numero'];


            $this->Ordreservice->begin();
            $this->Ordreservice->create($ordreservice);
            if ($this->Ordreservice->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Ordreservice->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Ordreservice->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }

        $marche = $this->Ordreservice->Marche->find(
                'first', array(
            'conditions' => array(
                'Marche.id' => $marcheId
            ),
            'contain' => array(
                'Ordreservice' => array(
                    'order' => array('Ordreservice.numero ASC')
                )
            )
                )
        );
        if (!empty($marche['Ordreservice'])) {
            $numeroOS = $marche['Ordreservice'][count($marche['Ordreservice']) - 1]['numero'] + 1;
        } else {
            $numeroOS = 1;
        }
        $this->set(compact('numeroOS'));

        $orgs = $this->Ordreservice->Organisme->find('list', array('conditions' => array('Organisme.active' => true), 'order' => array('Organisme.name ASC')));
        $this->set(compact('orgs'));
        $this->set('marcheId', $marcheId);
    }

    /**
     * Edition d'une opération
     *
     * @logical-group Ordresservices
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Ordreservice->create();
            $this->request->data['Ordreservice']['name'] = $this->request->data['Ordreservice']['numero'];
            $ordreservice = $this->request->data;


            if ($this->Ordreservice->save($ordreservice)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Ordreservice->find(
                    'first', array(
                'conditions' => array(
                    'Ordreservice.id' => $id
                ),
                'contain' => array(
                    'Marche'
                )
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
        $marcheId = $this->request->data['Marche']['id'];
        $this->set('ordreserviceId', $id);
        $this->set('marcheId', $marcheId);

        $orgs = $this->Ordreservice->Organisme->find('list', array('conditions' => array('Organisme.active' => true), 'order' => array('Organisme.name ASC')));
        $this->set(compact('orgs'));
    }

    /**
     * Suppression d'une opération
     *
     * @logical-group Ordresservices
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Ordreservice->begin();
        if ($this->Ordreservice->delete($id)) {
            $this->Ordreservice->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Ordreservice->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des opérations (ajax)
     *
     * @logical-group Ordresservices
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getOrdresservices() {

        $ordresservices_tmp = $this->Ordreservice->find("all", array(
            'order' => array('Ordreservice.name')
        ));

        $ordresservices = array();
        foreach ($ordresservices_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $item['right_view'] = true;
            $ordresservices[] = $item;
        }
        $this->set(compact('ordresservices'));
        $orgs = $this->Ordreservice->Organisme->find('list', array('conditions' => array('Organisme.active' => true), 'order' => array('Organisme.name ASC')));
        $this->set(compact('orgs'));
    }

    public function ajaxformdata($field = null, $marcheId = null) {

        if ($field == 'typedocument') {
            $this->autoRender = false;

            if (!empty($this->request->data['Courrier']['marche_id']) && empty($marcheId)) {
                $marche = $this->Ordreservice->Marche->find(
                        'first', array(
                    'conditions' => array(
                        'Marche.id' => $this->request->data['Courrier']['marche_id']
                    ),
                    'contain' => array(
                        'Ordreservice' => array(
                            'order' => array('Ordreservice.numero ASC')
                        )
                    )
                        )
                );
                if (!empty($marche['Ordreservice'])) {
                    $valueNumberOs = $marche['Ordreservice'][count($marche['Ordreservice']) - 1]['numero'] + 1;
                } else {
                    $valueNumberOs = 1;
                }
            } else {
                $marche = $this->Ordreservice->Marche->find(
                        'first', array(
                    'conditions' => array(
                        'Marche.id' => $marcheId
                    ),
                    'contain' => array(
                        'Ordreservice' => array(
                            'order' => array('Ordreservice.numero ASC')
                        )
                    )
                        )
                );
                if (!empty($marche['Ordreservice'])) {
                    $valueNumberOs = $marche['Ordreservice'][count($marche['Ordreservice']) - 1]['numero'] + 1;
                } else {
                    $valueNumberOs = 1;
                }
            }

            $osEntrant = $this->request->data['Courrier']['direction'];
            if( $osEntrant == '1' ) {
                $valueNumberOs = ' ';
            }
            $this->set('valueNumberOs', $valueNumberOs);

            return $valueNumberOs;
        }


        if ($field == 'numero') {
            $this->autoRender = false;
            $marcheWithOs = $this->Ordreservice->find(
                    'first', array(
                'conditions' => array(
                    'Ordreservice.marche_id' => $this->request->data['Courrier']['marche_id'],
                    'Ordreservice.numero' => $this->request->data['Ordreservice']['numero']
                ),
                'contain' => false
                    )
            );
            if (!empty($marcheWithOs)) {
                $this->autoRender = false;
                $content = json_encode(array('Ordreservice' => array('numero' => 'Valeur déjà utilisée')));
                header('Pragma: no-cache');
                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
                header('Content-Type: text/x-json');
                header("X-JSON: {$content}");
                Configure::write('debug', 0);
                echo $content;
                return;
            }
//            if (in_array($this->request->data['Ordreservice']['numero'], $this->Ordreservice->find('list', array('conditions' => array('Ordreservice.active' => true), 'fields' => array('id', 'numero'))))) {
//                $this->autoRender = false;
//                $content = json_encode(array('Ordreservice' => array('numero' => 'Valeur déjà utilisée')));
//                header('Pragma: no-cache');
//                header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
//                header('Content-Type: text/x-json');
//                header("X-JSON: {$content}");
//                Configure::write('debug', 0);
//                echo $content;
//                return;
//            }
        }
    }

    /**
     * Export des informations liées aux OS
     *
     * @logical-group Marches
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function export($marcheId) {
        $orgs = $this->Ordreservice->Organisme->find('list', array('conditions' => array('Organisme.active' => true), 'order' => array('Organisme.name ASC')));
        $this->set(compact('orgs'));
        $marche = $this->Ordreservice->Marche->find(
                'first', array(
            'conditions' => array(
                'Marche.id' => $marcheId
            ),
            'contain' => false
                )
        );
        $marcheNumero = $marche['Marche']['numero'];
        $this->set('marcheNumero', $marcheNumero);

        $querydata = array(
            'conditions' => array(
                'Ordreservice.marche_id' => $marcheId
            ),
            'order' => array(
                'Ordreservice.numero ASC'
            ),
            'contain' => array(
                'Organisme'
            )
        );
        $results = $this->Ordreservice->find('all', $querydata);
        $this->set('results', $results);

        $this->layout = '';
    }

    /**
     *
     */
    public function getExistance() {
        Configure::write('debug', 0);
        $this->autoRender = false;
        $return = array();
        if ($this->RequestHandler->isAjax() && !empty($this->request->data['Ordreservice']['numero'])) {
            $return = $this->Ordreservice->getExistance($this->request->data['Ordreservice']['numero'], $this->request->data['Courrier']['marche_id']);
        }
        echo json_encode($return);
    }


    public function view( $id ) {
        $os = $this->Ordreservice->find(
            'first',
            array(
                'conditions' => array(
                    'Ordreservice.id' => $id
                ),
                'contain' => false
            )
        );
        $courrierId = $os['Ordreservice']['courrier_id'];
        if(!empty($courrierId)) {
            $this->redirect(array('controller' => 'courriers', 'action' => 'historiqueGlobal', $courrierId));
        }
        else {
            return false;
        }
    }
}

?>
