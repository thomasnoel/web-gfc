<?php

/**
 * Fonctions
 *
 * Fonctions controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class FonctionsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Fonctions';
    public $uses = array('Fonction');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Fonctions', true)
        ));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Fonctions
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $fonction = $this->request->data;


            $this->Fonction->begin();
            $this->Fonction->create($fonction);

            if ($this->Fonction->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Fonction->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Fonction->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Fonctions
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Fonction->create();
            $fonction = $this->request->data;

            if ($this->Fonction->save($fonction)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Fonction->find(
                    'first', array(
                'conditions' => array('Fonction.id' => $id),
                'contain' => false
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Fonctions
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Fonction->begin();
        if ($this->Fonction->delete($id)) {
            $this->Fonction->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Fonction->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Fonctions
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getFonctions() {
        $fonctions_tmp = $this->Fonction->find(
                "all", array(
            'order' => 'Fonction.name'
                )
        );
        $fonctions = array();
        foreach ($fonctions_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $fonctions[] = $item;
        }
        $this->set(compact('fonctions'));
    }

}

?>
