<?php

/**
 * Thésaurus
 *
 * Keywordlists controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 *
 * based on As@lae keywordlists / keywords (thesaurus)
 * @link https://adullact.net/projects/asalae/
 */
class KeywordlistsController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Keywordlists';

	/**
	 * Controller uses
	 *
	 * @access public
	 * @var array
	 */
	public $uses = array('Keywordlist', 'Keywordtype', 'Keyword');

	/**
	 * Gestion des thésaurus
	 *
	 * @logical-group Thésaurus
	 * @user-profil Admin
	 *
	 * @access public
	 * @return void
	 */
	public function index() {
		$this->set('ariane', array(
                    '<a href="environnement/index/0/admin">'.__d('menu', 'Administration', true).'</a>',
                    __d('menu', 'gestionThesaurus', true)
                ));
	}

	/**
	 *
	 */
	//TODO: supprimer les fonctions commentées
	public function get_keywordlists() {
		$this->Keywordlist->recursive = -1;
		$keywordlists = $this->Keywordlist->find('all', array('order' => array('Keywordlist.nom')));
		$this->set('keywordlists', $keywordlists);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
	}

	/**
	 * Ajout d'une liste
	 *
	 * @param type $collectiviteId
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function add($collectiviteId = null) {
//		$sortie = false;
//		if (empty($this->request->data)) {
//			// Si la collectivité n'est pas connue, on demande à l'utilisateur de faire la sélection
//			if (empty($collectiviteId))
//				$this->redirect(array(
//					'controller' => 'Collectivites',
//					'action' => 'selectCollectivite',
//					$this->name,
//					$this->action,
//					__('Ajout d\'une liste de mots clés', true)));
//			elseif (!$this->_collectiviteValide($collectiviteId)) {
//				$this->Session->setFlash(__('Invalide id pour la', true) . ' ' . __('collectivit&eacute;', true) . ' : ' . __('ajout impossible.', true), 'growl', array('type' => 'important'));
//				$sortie = true;
//			} else {
//				/* Initialisations */
//				$this->request->data[$this->modelClass]['active'] = 1;
//			}
//		} else {
//			/* Initialisations avant sauvegarde */
//			$this->request->data[$this->modelClass]['active'] = 1;
//			$this->request->data[$this->modelClass]['version'] = 0;
//			$this->request->data[$this->modelClass]['created_user_id'] = $this->Auth->user('id');
//			$this->request->data[$this->modelClass]['modified_user_id'] = $this->Auth->user('id');
//			if ($this->{$this->modelClass}->save($this->request->data)) {
//				$this->Session->setFlash(__('La liste de mots clés', true) . ' \'' . $this->request->data[$this->modelClass]['nom'] . '\' ' . __('a été ajoutée.', true), 'growl');
//				$sortie = true;
//			} else
//				$this->Session->setFlash(__('Veuillez corriger les erreurs du formulaire.', true), 'growl', array('type' => 'erreur'));
//		}
//		if ($sortie)
//			$this->redirect(array('action' => 'index'));
//		else {
//			$this->pageTitle = Configure::read('appName') . ' : ' . __('Listes des mots clés', true) . ' : ' . __('ajout', true);
//			$this->set('keywordtypes', $this->Keywordtype->listFields(array('key' => 'id')));
//			$this->request->data['Collectivite']['nom'] = $this->{$this->modelClass}->Collectivite->field('nom', array('id' => $this->request->data[$this->modelClass]['collectivite_id']));
//			$this->render('edit');
//		}
//	}

	/**
	 * édition d'un thésaurus (association à une collectivité) et visualisation de la liste des mots-clefs
	 *
	 * @logical-group Thésaurus
	 * @user-profile Admin
	 *
	 * @access public
	 * @param type $id identifiant du thésaurus
	 * @return void
	 */
	public function edit($id = null) {
		$this->loadModel('CollectiviteKeywordlist');
		if (!empty($this->request->data)) {
			$this->Jsonmsg->init();

			$this->Keywordlist->create($this->request->data);
			$resultSave = $this->Keywordlist->save();
			if (!empty($resultSave)) {
				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			$this->Keywordlist->recursive = -1;
			$this->Keywordlist->Keyword->recursive = -1;
			$this->request->data = $this->Keywordlist->find('first', array('conditions' => array('Keywordlist.id' => $id)));
//      $this->set('checked', !empty($assoc));
			$keywords = $this->Keywordlist->Keyword->generateTreeList(array('Keyword.keywordlist_id' => $id));
			$this->set('keywords', $keywords);
		}
	}

	/**
	 * Suppression d'une liste de mots clés
	 *
	 * @param type $id
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function delete($id = null) {
//		$eleASupprimer = $this->Keywordlist->find('first', array(
//			'conditions' => array('Keywordlist.id' => $id),
//			'recursive' => 0));
//		if (!$this->Keywordlist->isDeletable($id))
//			$this->Session->setFlash(__('La liste', true) . ' \'' . $eleASupprimer[$this->modelClass]['nom'] . '\' ' . __('ne peut pas &ecirc;tre supprim&eacute;.', true), 'growl');
//		elseif (!$this->{$this->modelClass}->del($id, true))
//			$this->Session->setFlash(__('Une erreur est survenue pendant la suppression', true), 'growl', array('type' => 'erreur'));
//		else {
//			/* Suppression des modèles liés par belongsTo qui ne permet pas la suppression automatique en cascade */
//			$this->Session->setFlash(__('La liste ', true) . ' \'' . $eleASupprimer[$this->modelClass]['nom'] . '\' ' . __('a &eacute;t&eacute; supprim&eacute;.', true), 'growl');
//		}
//
//		$this->redirect(array('action' => 'index'));
//	}

	/**
	 * Visualisation des informations d'un thésaurus
	 *
	 * @logical-group Thésaurus
	 * @user-profile Admin
	 *
	 * @access public
	 * @param integer $id identifiant du thésaurus
	 * @return void
	 */
	public function view($id = null) {
		$this->Keywordlist->recursive = -1;
		$this->request->data = $this->Keywordlist->find('first', array('conditions' => array('Keywordlist.id' => $id)));
	}

	/**
	 * Retourne true ou false selon que l'utilisateur connecté a le droit d'accès à la collectivité collecititeId
	 * en fonction du droits Collectivite:accesAToutes
	 *
	 * @param type $collectiviteId
	 * @return boolean
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	private function _collectiviteAccessible($collectiviteId) {
//		if ($this->Droits->check($this->Auth->user('id'), 'Collectivites:accesAToutes'))
//			return true;
//		else
//			return $collectiviteId == $this->Auth->user('collectivite_id');
//	}

	/**
	 * Retourne true si la collectivite $collectiviteId est valide sur les points :
	 * - si l'utilisateur connecté a les droits d'accès à cette collectivité
	 * - existence de cette collectivite en base active
	 * et false dans le cas contraire
	 *
	 * @param type $collectiviteId
	 * @return type
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	private function _collectiviteValide($collectiviteId) {
//		if ($this->Droits->check($this->Auth->user('id'), 'Collectivites:accesAToutes')) {
//			return ($this->{$this->modelClass}->Collectivite->find('count', array(
//						'conditions' => array(
//							'Collectivite.active' => 1,
//							'Collectivite.id' => $collectiviteId),
//						'recursive' => -1)) > 0);
//		} else
//			return $collectiviteId == $this->Auth->user('collectivite_id');
//	}

	/**
	 * Fonction d'import des thésaurus publiés par les archives de France : http://www.archivesdefrance.culture.gouv.fr/thesaurus/
	 *
	 * @return boolean
	 */
	//TODO: supprimer ou garder les méthodes provenant d'As@lae
//	public function importThesaurus() {
//		// chargement du thésaurus xml
//		$fichierThesaurus = WWW_ROOT . 'files' . DS . 'thesaurus' . DS . 'Matiere.xml';
////	$fichierThesaurus = WWW_ROOT.'files'.DS.'thesaurus'.DS.'Actions.xml';
////	$fichierThesaurus = WWW_ROOT.'files'.DS.'thesaurus'.DS.'Contexte.xml';
////	$fichierThesaurus = WWW_ROOT.'files'.DS.'thesaurus'.DS.'Origine.xml';
//		libxml_use_internal_errors(true);
//		try {
//			$thesaurus = new SimpleXMLElement($fichierThesaurus, null, true);
//		} catch (Exception $e) {
//			return false;
//		}
//
//		// création de la liste
//		$scheme_id = (string) $thesaurus->ConceptScheme->title;
//		$a = 'ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöøùúûýýþÿŔŕ';
//		$b = 'aaaaaaaceeeeiiiidnoooooouuuuybsaaaaaaaceeeeiiiidnoooooouuuyybyRr';
//		$scheme_id = utf8_decode($scheme_id);
//		$scheme_id = strtr($scheme_id, utf8_decode($a), $b);
//		$scheme_id = strtoupper($scheme_id);
//		$schemeId = utf8_encode($scheme_id);
//
//		$keyWordList = array(
//			'collectivite_id' => 1,
//			'nom' => (string) $thesaurus->ConceptScheme->title,
//			'description' => (string) $thesaurus->ConceptScheme->creator . ' : ' . strtolower((string) $thesaurus->ConceptScheme->description),
//			'version' => 1,
//			'active' => true,
//			'keywordtype_id' => 7,
//			'scheme_id' => $schemeId,
//			'scheme_name' => (string) $thesaurus->ConceptScheme->title,
//			'scheme_agency_name' => (string) $thesaurus->ConceptScheme->creator,
//			'scheme_version_id' => (string) $thesaurus->ConceptScheme->modified,
//			'scheme_data_uri' => str_replace('resource', 'data', (string) $thesaurus->ConceptScheme['about']) . "?includeSchemes=true",
//			'scheme_uri' => (string) $thesaurus->ConceptScheme['about'],
//			'created_user_id' => 1,
//			'modified_user_id' => 1
//		);
//		$this->Keywordlist->save($keyWordList);
//		$keyWordListId = $this->Keywordlist->id;
//
//		// ajout des mots clés
//		foreach ($thesaurus->Concept as $concept) {
//			$keyWord = $this->Keywordlist->Keyword->create();
//
//			$about = (string) $concept['about'];
//			$aboutArray = explode('/', $about);
//			$code = array_pop($aboutArray);
//
//			$keyWord = array(
//				'keywordlist_id' => $keyWordListId,
//				'version' => 1,
//				'code' => $code,
//				'libelle' => (string) $concept->prefLabel,
//				'parent_id' => null,
//				'created_user_id' => 1,
//				'modified_user_id' => 1
//			);
//			$this->Keywordlist->Keyword->save($keyWord);
//		}
//		debug('traitement terminé');
//		die;
//	}
}

?>
