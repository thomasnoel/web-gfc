<?php

/**
 * Operations
 *
 * Operations controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class OperationsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Operations';
    public $uses = array('Operation');

    /**
     * Gestion des opérations interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'operations', true)
        ));
    }

    /**
     * Ajout d'un e opération
     *
     * @logical-group Operations
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {

        $operations = $this->Operation->find('all');

        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $operation = $this->request->data;


//debug($operation);

            $Type = ClassRegistry::init('Type');
            $type = array();
            $type['Type']['name'] = $operation['Operation']['name'];
            $Type->create($type);
            $saveType = $Type->save();
            if ($saveType) {
                $operation['Operation']['type_id'] = $Type->id;
            }

            $this->Operation->begin();
            $this->Operation->create($operation);
            if ($this->Operation->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Operation->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Operation->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'une opération
     *
     * @logical-group Operations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Operation->create();
            $operation = $this->request->data;

            // A la modification, on vérifie s'il y a un lien entre le type et l'opéraiton
            if (empty($operation['Operation']['type_id'])) {
                $Type = ClassRegistry::init('Type');
                $type = array();

                $type = $this->Operation->Type->find(
                        'first', array(
                    'conditions' => array(
                        'Type.name' => $operation['Operation']['name']
                    ),
                    'contain' => false
                        )
                );
                // on vérifie si le type existe déjà et possède le même intitulé que l'opération
                if (!empty($type)) {
                    // Si oui, on associe l'ID du type à l'opération
                    $operation['Operation']['type_id'] = $type['Type']['id'];
                } else {
                    // Sinon, on crée le type
                    $type['Type']['name'] = $operation['Operation']['name'];
                    $Type->create($type);
                    $saveType = $Type->save();
                    if ($saveType) {
                        // on associe le nouveau type_id à l'opération
                        $operation['Operation']['type_id'] = $Type->id;
                    }
                }
            }


            if ($this->Operation->save($operation)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Operation->find(
                    'first', array(
                'conditions' => array(
                    'Operation.id' => $id
                ),
                'contain' => false
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'une opération
     *
     * @logical-group Operations
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Operation->begin();
        if ($this->Operation->delete($id)) {
            $this->Operation->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Operation->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des opérations (ajax)
     *
     * @logical-group Operations
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getOperations() {
        $querydata = $this->Operation->search($this->request->data);
        $operations_tmp = $this->Operation->find('all', $querydata);

        $listoperations = $this->Operation->find('list', array('conditions' => array('Operation.active' => true), 'order' => array('Operation.name ASC')));
        $this->set(compact('listoperations'));

        $operations = array();
        foreach ($operations_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            foreach ($item['Sousoperation'] as $s => $sousope) {
                $item['Operation']['sousope'][] = $sousope['name'];
                sort($item['Operation']['sousope']);
            }
            $operations[] = $item;
        }
        $this->set(compact('operations'));
    }

    /**
     * Visualisation de la liste des utilisateurs intervenants sur 1 OP
     *
     * @logical-group Operations
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getSousoperations($operation_id) {
        $intitulesagents = $this->Operation->Sousoperation->DesktopmanagerSousoperation->Intituleagent->find('list', array(
            'conditions' => array('Intituleagent.active' => true),
            'order' => 'Intituleagent.name ASC'
        ));
        $operation = $this->Operation->find("first", array(
            'conditions' => array(
                'Operation.id' => $operation_id
            ),
            'contain' => false,
            'order' => 'Operation.name'
        ));
        $operationId = $operation_id;
        $numOP = $operation['Operation']['name'];
        $this->set('numOP', $numOP);

        $selectsousoperations_tmp = $this->Operation->Sousoperation->find('all', array(
            'contain' => array(
                'Intituleagent' /* => array(
              'DesktopmanagerSousoperation' => array(
              'order' => 'DesktopmanagerSousoperation.ordre ASC'
              )
              ) */
            ),
            'conditions' => array(
                'Sousoperation.operation_id' => $operation_id
            ),
            'order' => array('Sousoperation.name ASC')
        ));
        $selectsousoperations = array();
        foreach ($selectsousoperations_tmp as $item) {
            $valueToSet = array();
            $valueToSet['right_add'] = true;
            $valueToSet['right_edit'] = true;
            $valueToSet['right_delete'] = true;
            $valueToSet['Sousoperation'] = $item['Sousoperation'];

            foreach ($item['Intituleagent'] as $a => $agent) {
                $valueToSet['ordreetape'][$agent['DesktopmanagerSousoperation']['ordre']][] = $agent;
            }
            if (!empty($valueToSet['ordreetape'])) {
                ksort($valueToSet['ordreetape']);

                foreach ($valueToSet['ordreetape'] as $ordre => $value) {
                    $infos[$ordre] = Hash::extract($value, '{n}.name');
                    $typeEtape[$ordre] = Hash::extract($value, '{n}.DesktopmanagerSousoperation.typeetape');

                    if (in_array(CAKEFLOW_CONCURRENT, array_values($typeEtape[$ordre]))) {
                        //$valueToSet['Sousoperation']['etapeou'][] = 'Etape '.$ordre.' : '.implode(' OU ', $infos[$ordre]);
                        $valueToSet['Sousoperation']['etape'][$ordre] = 'Etape ' . $ordre . ' : ' . implode(' OU ', $infos[$ordre]);
                    } else if (in_array(CAKEFLOW_COLLABORATIF, array_values($typeEtape[$ordre]))) {
                        //$valueToSet['Sousoperation']['etapeet'][] = 'Etape '.$ordre.' : '.implode(' ET ', $infos[$ordre]);
                        $valueToSet['Sousoperation']['etape'][$ordre] = 'Etape ' . $ordre . ' : ' . implode(' ET ', $infos[$ordre]);
                    } else {
                        //$valueToSet['Sousoperation']['etapesimple'][] = 'Etape '.$ordre.' '.$infos[$ordre][0];
                        $valueToSet['Sousoperation']['etape'][$ordre] = 'Etape ' . $ordre . ' : ' . $infos[$ordre][0];
                    }
                }
            }
            $selectsousoperations[] = $valueToSet;
        }
//debug( $selectsousoperations );
        $this->set('selectsousoperations', $selectsousoperations);

        $this->set(compact('operation', 'desktopsmanagers', 'operationId', 'intitulesagents'));
    }

    /**
     * Export des informations liées aux marchés
     *
     * @logical-group Marches
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function export() {
        ini_set('memory_limit', '3000M');

        $name = Hash::get($this->request->params['named'], 'Operation__id');
        $operationActive = Hash::get($this->request->params['named'], 'Operation__active');

        $conditions = array();
        if (!empty($this->request->params)) {

            if (isset($name) && !empty($name)) {
                $conditions[] = array('Operation.id' => $name);
            }

            if (isset($operationActive) && !empty($operationActive)) {
                $conditions[] = array('Operation.active' => $operationActive);
            }
        }

        $querydata = array(
            'conditions' => array(
                $conditions
            ),
            'order' => array(
                'Operation.name'
            ),
            'contain' => array(
                'Sousoperation',
                'Intituleagentbyoperation' => array(
                    'Intituleagent',
                    'Desktopmanager'
                )
            )
        );
        $results = $this->Operation->find('all', $querydata);
//debug($results);
//die();

        foreach ($results as $i => $operation) {
            $results[$i]['Operation']['nomoperation'] = str_replace(array(","), " / ", $results[$i]['Operation']['nomoperation']);
            $results[$i]['Operation']['nature'] = str_replace(array(","), " / ", $results[$i]['Operation']['nature']);
            foreach ($operation['Intituleagentbyoperation'] as $intitule => $datas) {
                $results[$i]['Operation']['Intituleagent'][$datas['Intituleagent']['name']] = $datas['Desktopmanager']['name'];
            }

            $results[$i]['Operation']['Sousop'] = Hash::extract($operation, 'Sousoperation.{n}.name');
            unset($results[$i]['Sousoperation']);
            unset($results[$i]['Intituleagentbyoperation']);
        }
        $this->set('results', $results);

//debug($results);
//die();

        $this->layout = '';
    }

}

?>
