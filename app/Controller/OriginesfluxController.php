<?php

/**
 * Scanemails
 *
 * Scanemails controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class OriginesfluxController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Originesflux';
    public $uses = array('Origineflux');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Originesflux
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {

    }

    /**
     * Ajout d'un email
     *
     * @logical-group Originesflux
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {

        if (!empty($this->request->data)) {
            $this->Origineflux->begin();

            $this->Origineflux->create($this->request->data);
            if ($this->Origineflux->save()) {
                $this->Origineflux->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Origineflux->rollback();
                $this->Jsonmsg->error('Erreur de sauvegarde');
            }
            $this->Jsonmsg->send();
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Originesflux
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $json = array(
                'success' => false,
                'message' => __d('default', 'save.error')
            );
//            $this->Jsonmsg->init();
            $originesflux = $this->request->data;
            $this->Origineflux->create($originesflux);
            if ($this->Origineflux->save()) {
                $messages['origineflux'] = true;
//                $this->Jsonmsg->valid();
            }
//            $this->Jsonmsg->send();

            if (!in_array(false, $messages, true)) {
                $this->Origineflux->commit();
                $json['success'] = true;
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Origineflux->rollback();
                $json['message'] = 'Préparation de la modification origine : ' . ($messages['origineflux'] ? 'ok' : 'erreur');
            }
            $json['message'] = 'Onglet informations<br />' . $json['message'];
            $this->Jsonmsg->sendJsonResponse($json);
        } else {
            $this->request->data = $this->Origineflux->find(
                'first',
                array(
                    'conditions' => array('Origineflux.id' => $id),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Originesflux
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Origineflux->begin();
        if ($this->Origineflux->delete($id)) {
            $this->Origineflux->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Origineflux->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Originesflux
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getOrigineflux() {

         if(!empty($this->request->data) ) {
            $querydata = $this->Origineflux->search($this->request->data);
        }
        else {
            $querydata = array(
                'contain' => false,
                'conditions' => array('Origineflux.active' => true),
                'order' => 'Origineflux.name'
            );
        }
        $originesflux_tmp = $this->Origineflux->find("all", $querydata );
        $originesflux = array();
        foreach ($originesflux_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $originesflux[] = $item;
        }
        $this->set(compact('originesflux'));
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Validation d'unicité du nom d'une collectivité (création / édition)
     *
     * @param type $field
     */
    public function ajaxformvalid($field = null, $collectiviteId = 0) {

        $models = array();
        if ($field != null) {
            $models['Origineflux'] = array($field);
        } else {
            $models[] = 'origineflux';
        }

        $rd = array();

        foreach ($this->request->data as $k => $v) {
            if ($k == 'Origineflux') {
                $rd[$k] = $v;
            }
        }

        $this->Ajaxformvalid->valid($models, $rd, $collectiviteId);
    }

}

?>
