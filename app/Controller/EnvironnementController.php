<?php

/**
 * Environnement
 *
 * Environnement controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
//    @set_time_limit( 0 );
// Mémoire maximum allouée à l'exécution de ce script
//	@ini_set( 'memory_limit', '2048M' );
// Temps maximum d'exécution du script (en secondes)
//	@ini_set( 'max_execution_time', 2000 );
// Temps maximum (en seconde), avant que le script n'arrête d'attendre la réponse de Gedooo
//	@ini_set( 'default_socket_timeout', 12000 );

class EnvironnementController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Environnement';

    /**
     * Controller uses
     *
     * @access public
     * @var array
     */
    public $uses = array('User', 'Notification', 'Right');

    /**
     * Liste des bannettes
     *
     * @access private
     * @var array
     */
    private $_bannettes = array();

    /**
     * Controller helpers
     *
     * @var array
     * @access public
     */
    public $helpers = array('Paginator');

    /**
     * Liste des rôles de l'utilisateur pouvant ajouter un flux
     *
     * @access private
     * @var array
     */
    private $_add = array();

    /**
     * Liste des rôles de l'utilisateur pouvant ajouter un flux
     *
     * @access private
     * @var array
     */
    private $_addService = array();

    /**
     * Détermine si on a obtenu la liste des flux des services dépendants
     *
     * @access private
     * @var boolean
     */
    private $_initBancontenuService = false;

	/**
	 * Controller components
	 *
	 * @var array
	 */
	public $components = array('NewPastell');

    /**
     * Traitement des informations brutes d'un flux en vue de l'affichage
     *
     * @access private
     * @param array $flux informations du flux
     * @param boolean $isParent si le flux est un flux parent (des réponses à ce flux ont été
     * @param boolean $delete détermine si l'action de suppression sera disponible à l'affichage
     * @return array les informations du flux traitées
     */
    private function _processFlux($flux, $isParent = false, $view = true, $edit = true, $delete = true, $parentInfo = array(), $read = true) {
        // Dans le cas où un organisme est renseigné, on l'affiche
        $contact = '';
        if (!empty($flux['Organisme']['name']) && empty($flux['Contact']['name'])) {
            $contact = $flux['Organisme']['name'];
        } else if (!empty($flux['Organisme']['name']) && !empty($flux['Contact']['name'])) {
            if ($flux['Organisme']['id'] == Configure::read('Sansorganisme.id')) {
                if( in_array( $flux['Contact']['name'], array(' Sans contact' , 'Sans contact' ) ) ){
                    $contact = '';
                }
                else {
                    $contact = $flux['Contact']["name"];
                }
            } else {
                if( in_array( $flux['Contact']['name'], array(' Sans contact' , 'Sans contact' ) ) ){
                    $contact = $flux['Organisme']['name'];
                }
                else {
                    $contact = $flux['Organisme']['name'] . ' / ' . $flux['Contact']['name'];
                }

            }
        } else if (empty($flux['Organisme']['name']) && !empty($flux['Contact']['name'])) {
            if( in_array( $flux['Contact']['name'], array(' Sans contact' , 'Sans contact' ) ) ){
                $contact = '';
            }
            else {
                $contact = $flux['Contact']['name'];
            }
        }

        $flux['Courrier']['commentaire'] = 0;
        $this->loadModel('Courrier');
        $ownerId = $this->SessionTools->getDesktopList();
        $flux['Courrier']['commentaire'] = $this->Courrier->Comment->getNumberOfCommentByCourrierId($flux['Courrier']['id'], $ownerId);

        $flux['Courrier']['ficassoc'] = $this->Courrier->hasAssociatedFile($flux['Courrier']['id']);

        if( strpos( $flux['Courrier']['objet'], '<!DOCTYPE') !== false || strpos( $flux['Courrier']['objet'], '<html>') !== false || strpos( $flux['Courrier']['objet'], '--_000') !== false ) {
            $flux['Courrier']['objet'] = strip_tags( $flux['Courrier']['objet'], "<html>" );
            $substring = substr($flux['Courrier']['objet'] ,strpos($flux['Courrier']['objet'] ,"<style"),strpos($flux['Courrier']['objet'] ,"</style>"));
            $flux['Courrier']['objet'] = str_replace($substring,"",$flux['Courrier']['objet']);
            $flux['Courrier']['objet'] = trim($flux['Courrier']['objet']);
        }

        $pos2 = strpos($flux['Courrier']['objet'], '--_000');
        if ($pos2 !== false) {
            $flux['Courrier']['objet'] = mb_decode_mimeheader($flux['Courrier']['objet']);
        }
        $pos3 = strpos($flux['Courrier']['objet'], '_000_');
        if ($pos3 !== false) {
            $flux['Courrier']['objet'] = mb_decode_mimeheader($flux['Courrier']['objet']);
        }
        $pos4 = strpos($flux['Courrier']['objet'], 'PCFET');
        if ($pos4 !== false) {
            $flux['Courrier']['objet'] = base64_decode($flux['Courrier']['objet']);
        }

		if( is_JSON($flux['Courrier']['objet'] ) ) {
			$flux['Courrier']['objet'] = json_decode($flux['Courrier']['objet'], true);
			$objetData = array();
			foreach ($flux['Courrier']['objet'] as $key => $val) {
				if( is_array($val) ) {
					foreach( $val as $fields => $values ) {
						$data[] = "\n".$fields.': '.$values;
					}
					$objetData[] = $key . ' : ' . implode( ' ', $data );
				}
				else {
					$objetData[] = $key . ' : ' . $val;
				}
			}
			$flux['Courrier']['objet'] = implode("\n", $objetData);
		}

		$flux['Courrier']['delairetard'] = $this->Courrier->getRetardDelaiNbOfDays($flux['Courrier']['id']);
		$objet = '';
		if (!empty($flux['Courrier']['objet'])) {
			$objet = replace_accents(substr($flux['Courrier']['objet'], 0, 10)) . '...';
		}

        return array(
            "id" => $flux['Courrier']['id'],
            "priorite" => $flux['Courrier']['priorite'],
//            "retard" => !$flux['Courrier']['retard'],
            "retard" => $flux['Courrier']['mail_retard_envoye'],
            "ficassoc" => $flux['Courrier']['ficassoc'],
            "direction" => $flux['Courrier']['direction'],
            "etat" => $flux['Bancontenu']['etat'],
            "bannette_id" => $flux['Bancontenu']['bannette_id'],
            "reference" => $flux['Courrier']['reference'],
            "nom" => $flux['Courrier']['name'],
			"objet" => $objet,
//			"objet" => $flux['Courrier']['objet'],
            "contact" => @$contact,
            "date" => $flux['Courrier']['datereception'],
            "created" => $flux['Courrier']['created'],
            "typesoustype" => ($flux['Type']['name'] . '/' . $flux['Soustype']['name'] != '/') ? $flux['Type']['name'] . '/' . $flux['Soustype']['name'] : '',
            "bureau" => $flux['Desktop']['name'],
            "commentaire" => $flux['Courrier']['commentaire'],
            "modified" => $flux['Courrier']['modified'],
            "desktopId" => $flux['Desktop']['id'],
            "view" => ($view) ? $this->Acl->check(array('model' => 'Desktop', 'foreign_key' => $flux['Desktop']['id']), 'controllers/Courriers/view') : false,
            "edit" => ($edit) ? $this->Acl->check(array('model' => 'Desktop', 'foreign_key' => $flux['Desktop']['id']), 'controllers/Courriers/view') : false, //Arnaud ('/controllers/Courriers/view')
            "delete" => ($delete) ? $this->Acl->check(array('model' => 'Desktop', 'foreign_key' => $flux['Desktop']['id']), 'controllers/Courriers/delete') : false,
            "detache" => true, // Arnaud +
            "isParent" => $isParent,
            "parent_id" => $flux['Courrier']['parent_id'],
            "parentInfo" => $parentInfo,
            'read' => $read,
            "creatorId" => $flux['Courrier']['user_creator_id'],
            "userconnectedId" => $this->Session->read('Auth.User.id'),
            "isInCircuit" => $this->Courrier->isInCircuit($flux['Courrier']['id']),
			"delairetard" => $flux['Courrier']['delairetard']
                //"mailRetardEnvoye" => $flux['Courrier']['mail_retard_envoye']
        );
    }

    /**
     * Fonction permettant de créer des champs virtuels pour les colonnes présentes dans les différentes bannettes
     *  - fichiers associés oui/non
     *  - sens du flux (entrant, sortant, interne)
     *  - flux en retard ou non
     *
     * @return array
     */
//    protected function _virtualFieldsCourrier() {
//        return array(
//            'EXISTS( SELECT documents.id FROM documents WHERE documents.courrier_id = "Courrier"."id" ) AS "Courrier__ficassoc"',
//            '( CASE WHEN "Courrier"."direction" = \'0\' THEN \'S\' WHEN "Courrier"."direction" = \'1\' THEN \'E\' WHEN "Courrier"."direction" = \'2\' THEN \'I\' ELSE NULL END ) AS "Courrier__direction"',
//            '( CASE
//                WHEN "Courrier"."soustype_id" IS NULL THEN true
//                WHEN "Bancontenu"."etat" IN ( -1, 2 ) THEN true
//                WHEN "Soustype"."delai_unite" = \'0\' THEN ( ( "Courrier"."datereception" + ( "Soustype"."delai_nb" * INTERVAL \'1 day\' ) ) > NOW() )
//                WHEN "Soustype"."delai_unite" = \'1\' THEN ( ( "Courrier"."datereception" + ( "Soustype"."delai_nb" * INTERVAL \'1 week\' ) ) > NOW() )
//                WHEN "Soustype"."delai_unite" = \'2\' THEN ( ( "Courrier"."datereception" + ( "Soustype"."delai_nb" * INTERVAL \'1 month\' ) ) > NOW() )
//                ELSE true
//            END ) AS "Courrier__is_in_time"',
//        );
//    }
    /*
      protected function _virtualFieldsCourrier() {
      return array(
      'EXISTS( SELECT documents.id FROM documents WHERE documents.courrier_id = "Courrier"."id" ) AS "Courrier__ficassoc"',
      '( CASE WHEN "Courrier"."direction" = \'0\' THEN \'S\' WHEN "Courrier"."direction" = \'1\' THEN \'E\' WHEN "Courrier"."direction" = \'2\' THEN \'I\' ELSE NULL END ) AS "Courrier__direction"',
      '( CASE
      WHEN "Courrier"."soustype_id" IS NULL THEN true
      WHEN "Bancontenu"."etat" IN ( -1, 2 ) THEN true
      WHEN "Courrier"."delai_unite" = \'0\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 day\' ) ) > NOW() )
      WHEN "Courrier"."delai_unite" = \'1\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 week\' ) ) > NOW() )
      WHEN "Courrier"."delai_unite" = \'2\' THEN ( ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 month\' ) ) > NOW() )
      ELSE true
      END ) AS "Courrier__is_in_time"',
      );
      }
     *
     */

    /**
     * Récupération de la liste des flux disponibles via les contenus de bannettes d'un rôle
     *
     * @param array $bannettesParams liste d'array() contenant
     *  - array avec pour clé desktop_id pour les IDs de desktop
     *  - array avec pour clé bannettes pour les noms de bannettes
     * @return array listes des flux par bannettes pour l'ensemble des rôles passés en paramètre
     */
    private function _getAllBancontenus(array $bannettesParams) {
        $this->loadModel('Bancontenu');

        $bannettes = array();
        foreach ($bannettesParams as $bannetteParams) {
            foreach ($bannetteParams['bannettes'] as $bannetteName) {
                if (!isset($bannettes[$bannetteName])) {
                    $bannettes[$bannetteName] = $bannetteParams['desktop_id'];
                } else {
                    $bannettes[$bannetteName] = array_merge($bannettes[$bannetteName], $bannetteParams['desktop_id']);
                }
            }
        }

//        $this->Session->write( 'paginationModel', null );
        $paginationModel = $this->Session->read('paginationModel');

        $page = Hash::get($this->request->params, 'named.page');
        $namedModel = Hash::get($this->request->params, 'named.model');

        foreach ($bannettes as $bannetteName => $desktopId) {
            $params = array('class' => 'Bannette', 'alias' => Inflector::camelize("bannette_{$bannetteName}"));
            $alias = $params['alias'];
            $this->{$alias} = ClassRegistry::init($params);

            $querydata = array(
                'fields' => array_merge(
                    array(
                        'Courrier.id',
                        'Courrier.objet',
                        'Courrier.reference',
                        'Courrier.priorite',
                        'Courrier.mail_retard_envoye',
                        'Courrier.direction',
                        'Courrier.name',
                        'Courrier.created',
                        'Courrier.modified',
                        'Courrier.datereception',
                        'Courrier.parent_id',
                        'Courrier.user_creator_id',
                        'Organisme.id',
                        'Organisme.name',
                        'Contact.id',
                        'Contact.name',
                        'Soustype.id',
                        'Soustype.name',
                    ),
                    $this->{$alias}->Bancontenu->fields(),
                    $this->{$alias}->fields(),
                    $this->{$alias}->Bancontenu->Desktop->fields(),
                    $this->{$alias}->Bancontenu->Courrier->Soustype->Type->fields()
                ),
                'contain' => false,
                'joins' => array(
                    $this->{$alias}->join($this->{$alias}->Bancontenu->alias),
                    $this->{$alias}->Bancontenu->join($this->{$alias}->Bancontenu->Desktop->alias),
                    $this->{$alias}->Bancontenu->join($this->{$alias}->Bancontenu->Courrier->alias),
                    $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Organisme->alias),
                    $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Soustype->alias, array('type' => 'LEFT OUTER')),
                    $this->{$alias}->Bancontenu->Courrier->Soustype->join($this->{$alias}->Bancontenu->Courrier->Soustype->Type->alias, array('type' => 'LEFT OUTER')),
                    $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Traitement->alias, array('type' => 'LEFT OUTER')),
                    $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Contact->alias, array('type' => 'LEFT OUTER'))
                ),
                'conditions' => array(
                    'Desktop.id' => $desktopId,
                    'Bancontenu.etat' => "1",
                    "{$alias}.name" => $bannetteName,
                ),
                'order' => array(
                    'Bancontenu.created' => 'DESC',
                    'Bancontenu.id' => 'DESC'
                )
            );
            // Gestion de la pagination pour ne passer de page en page que sur
            // le modèle qui nous intéresse
            if (isset($paginationModel[$alias])) {
                $params = $this->request->params;
                if ($namedModel === $alias) {
                    $querydata['page'] = $params['named']['page'] = $page;
                    if (!empty($params['named']['sort']) && !empty($params['named']['direction'])) {
                        $querydata['order'] = array($params['named']['sort'] => $params['named']['direction']);
                    }
                } else {
                    $querydata['limit'] = $paginationModel[$alias]['limit'];
                    $querydata['order'] = $paginationModel[$alias]['order'];
                    $querydata['page'] = max((int) @$paginationModel[$alias]['page'], 1);
                    $querydata['offset'] = ( ( (int) $paginationModel[$alias]['page'] ) <= 1 ? 0 : ( $paginationModel[$alias]['limit'] * ( $paginationModel[$alias]['page'] - 1 ) ) );
                }

                $params['options'] = (array) Hash::get($paginationModel, "{$alias}.options");

                $this->request->params = $params;
            }

            $paginate = $this->paginate;
            $paginate[$alias] = $querydata;
            // $paginate[$alias]['limit'] = 10; // Afin de n'afficher que 10 lignes par bannettes
            $this->paginate = $paginate;

            $this->request->params['named']['page'] = @$querydata['page'];
            $bannettes[$bannetteName] = $this->paginate($this->{$alias});
            if (!empty($page)) {
                $this->request->params['named']['page'] = $page;
            }

            //preparation de chaque flux
            $parent = array();
            for ($i = 0; $i < count($bannettes[$bannetteName]); $i++) {

                if (isset($bannettes[$bannetteName][$i][$alias]) && !empty($bannettes[$bannetteName][$i][$alias])) {

                    $nomBannette = $bannettes[$bannetteName][$i][$alias]['name'];
                    //si parent_id => insertion du parent traité dans la bannette
                    if (!empty($bannettes[$bannetteName][$i]['Courrier']['parent_id'])) {
                        $qdParent = $querydata;
                        $qdParent['conditions'] = array('Courrier.id' => $bannettes[$bannetteName][$i]['Courrier']['parent_id']);
                        $parent = $this->_dataParent($bannettes[$bannetteName][$i]['Courrier']['parent_id']);
                    }

                    $delete = $nomBannette != 'refus' && $nomBannette != 'reponses' && $nomBannette != 'copy';
                    $bannettes[$bannetteName][$i] = $this->_processFlux($bannettes[$bannetteName][$i], false, true, true, $delete, $parent, $bannettes[$bannetteName][$i]['Bancontenu']['read']);
                }
            }
        }
        if (!empty($this->request->params['paging'])) {
            $this->Session->write('paginationModel', $this->request->params['paging']);
        }

        // Récupération de la liste des flux présents dans la bannette "Mes flux en copie"
        if (!empty($bannettes['copy'])) {
            for ($i = 0; $i < count($bannettes['copy']); $i++) {
                $fluxCopy[] = $bannettes['copy'][$i]['id'];
            }
        }
        if (!empty($fluxCopy)) {
            $this->Session->delete('Auth.User.Courrier.mesCopies');
            $this->Session->write('Auth.User.Courrier.mesCopies', $fluxCopy);
        }
        return $bannettes;
    }

    /**
     *  Fonction permettant de récupérer les données parentes de flux réponses
     *
     */
    protected function _dataParent($courrierParent_id) {
        $courrierParent = $this->Bancontenu->Courrier->find(
                'first', array(
            'fields' => array_merge(
                    $this->Bancontenu->Courrier->fields(), $this->Bancontenu->Courrier->Organisme->fields(), $this->Bancontenu->Courrier->Contact->fields(), $this->Bancontenu->Courrier->Soustype->fields(), $this->Bancontenu->Courrier->Soustype->Type->fields(), $this->Bancontenu->Courrier->Desktop->fields()
            ),
            'conditions' => array(
                'Courrier.id' => $courrierParent_id
            ),
            'joins' => array(
                $this->Bancontenu->Courrier->join('Organisme'),
                $this->Bancontenu->Courrier->join('Contact'),
                $this->Bancontenu->Courrier->join('Soustype'),
                $this->Bancontenu->Courrier->Soustype->join('Type'),
                $this->Bancontenu->Courrier->join('Desktop')
            ),
            'recursive' => -1
                )
        );
//debug($courrierParent);
        return $courrierParent;
    }

    /**
     * Uniquement pour les droits
     */
    public function getBancontenusServices() {

    }

    /**
     * Récupération de la liste des flux des services dépendants
     *
     * @access public
     * @return array liste des flux de la bannette "Mes services"
     */
    private function _getBancontenusServices() {

//        return array( ); // FIXME

        $bannetteService = array();
        $this->loadModel('Desktop');
        $services = array();
        // En fonction du profil de l'utilsiateur qu'elle la liste des flux auxquels il a accès
        $desktops = $this->SessionTools->getDesktopList();
        foreach ($desktops as $desktop) {
            $qd = array(
                'fields' => array(
                    'DesktopsService.service_id'
                ),
                'conditions' => array(
                    'DesktopsService.desktop_id' => $desktop
                ),
                'recursive' => -1
            );
            $srvs = $this->Desktop->DesktopsService->find('all', $qd);

            if (!empty($srvs)) {
                $services = array_merge($services, array_unique(Set::classicExtract($srvs, '{n}.DesktopsService.service_id'), SORT_REGULAR));
            }
        }
        // Liste des services auxquels il a accès
        $allServices = $services;
        foreach ($services as $service) {
            $children = $this->Desktop->Service->children($service);
            if (!empty($children)) {
                $allServices = array_merge($allServices, array_unique(Set::classicExtract($children, '{n}.Service.id'), SORT_REGULAR));
            }
        }
        $allServices = array_unique($allServices, SORT_REGULAR);
        $childrenDesktops = array();
        // Liste des profils liés aux services auxquels il a accès
        foreach ($allServices as $srv) {
            $qd = array(
                'fields' => array(
                    'DesktopsService.desktop_id'
                ),
                'conditions' => array(
                    'DesktopsService.service_id' => $srv
                ),
                'recursive' => -1
            );
            $dktps = $this->Desktop->DesktopsService->find('all', $qd);
            if (!empty($dktps)) {
                $childrenDesktops = array_merge($childrenDesktops, array_unique(Set::classicExtract($dktps, '{n}.DesktopsService.desktop_id'), SORT_REGULAR));
            }
        }
        $childrenDesktops = array_unique($childrenDesktops, SORT_REGULAR);
        $childrenDesktops = array_diff($childrenDesktops, $desktops);

        $params = array('class' => 'Bannette', 'alias' => Inflector::camelize("bannette_services"));
        $alias = $params['alias'];
        $this->{$alias} = ClassRegistry::init($params);

        $page = Hash::get($this->request->params, 'named.page');
        $namedModel = Hash::get($this->request->params, 'named.model');

        $querydata = array(
            'fields' => array_merge(
                    $this->{$alias}->Bancontenu->fields(), $this->{$alias}->fields(), $this->{$alias}->Bancontenu->Courrier->fields(), $this->{$alias}->Bancontenu->Desktop->fields(), $this->{$alias}->Bancontenu->Courrier->Organisme->fields(), $this->{$alias}->Bancontenu->Courrier->Soustype->fields(), $this->{$alias}->Bancontenu->Courrier->Soustype->Type->fields(), $this->{$alias}->Bancontenu->Courrier->Traitement->Circuit->fields(), $this->{$alias}->Bancontenu->Courrier->Contact->fields()
            ),
            'contain' => false,
            'joins' => array(
                $this->{$alias}->join($this->{$alias}->Bancontenu->alias),
                $this->{$alias}->Bancontenu->join($this->{$alias}->Bancontenu->Desktop->alias),
                $this->{$alias}->Bancontenu->join($this->{$alias}->Bancontenu->Courrier->alias),
                $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Organisme->alias),
                $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Soustype->alias, array('type' => 'LEFT OUTER')),
                $this->{$alias}->Bancontenu->Courrier->Soustype->join($this->{$alias}->Bancontenu->Courrier->Soustype->Type->alias, array('type' => 'LEFT OUTER')),
                $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Traitement->alias, array('type' => 'LEFT OUTER')),
                $this->{$alias}->Bancontenu->Courrier->Traitement->join($this->{$alias}->Bancontenu->Courrier->Traitement->Circuit->alias, array('type' => 'LEFT OUTER')),
                $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Contact->alias)
            ),
            'conditions' => array(
                'Desktop.id' => $childrenDesktops,
                'Bancontenu.etat' => array(1, 0),
                'Bancontenu.bannette_id <>' => BAN_COPY,
                "Courrier.datereception >" => date('Y-m-d', strtotime("-1 months")),
                "Courrier.datereception <=" => date('Y-m-d')
            ),
            'order' => array(
//                "{$alias}.name" => 'ASC',
//                 'Courrier.created' => 'DESC',
                'Bancontenu.created' => 'DESC'
            )
        );
// debug($childrenDesktops);
        if ($namedModel == $alias && !empty($this->request->params['named']['sort']) && !empty($this->request->params['named']['direction'])) {
            $querydata['order'] = array($this->request->params['named']['sort'] => $this->request->params['named']['direction']); //FIXME
        }
        $bannettes['services'] = $this->{$alias}->find('all', $querydata);
// debug($bannettes['services']);
        //preparation de chaque flux
        $fluxCtrl = array();
        if (isset($this->_bannettes['flux'])) {
            $banFlux = Set::classicExtract($this->_bannettes['flux'], '{n}.id'); //on recupere les flux présent dans la bannette flux
            if (!empty($banFlux)) {
                $fluxCtrl = $banFlux;
            }
        }

        //preparation de chaque flux
        //ici on evite de traiter les flux deja presents dans la bannette flux et les doublons
        $parent = array();
        for ($i = 0; $i < count($bannettes['services']); $i++) {
            if (!in_array($bannettes['services'][$i]['Courrier']['id'], $fluxCtrl)) {
                if (!empty($bannettes['services'][$i]['Courrier']['parent_id'])) {
                    $qdParent = $querydata;
                    $qdParent['conditions'] = array('Courrier.id' => $bannettes['services'][$i]['Courrier']['parent_id']);
//                    $parent = $this->{$alias}->find('first', $qdParent);
                    $parent = $this->_dataParent($bannettes['services'][$i]['Courrier']['parent_id']);
//                    $bannetteService[] = $this->_processFlux($parent, true, false);
                }
                $flux = $this->_processFlux($bannettes['services'][$i], false, true, true, false, $parent); //le parametre read est toujours défini a true pour les flux de mes services
                $bannetteService[] = $flux;

                $fluxCtrl[] = $bannettes['services'][$i]['Courrier']['id'];
            }
        }


        $paginationModel = (array) $this->Session->read('paginationModel');

        if (!isset($paginationModel[$alias])) {
            $paginationModel[$alias] = array(
                'page' => 1,
                'current' => null,
                'count' => null,
                'prevPage' => null,
                'nextPage' => null,
                'pageCount' => null,
                'order' => array(),
                'limit' => 20,
                'options' => array(),
                'paramType' => 'named'
            );
        }

        $requestparams = $this->request->params;

        $requestparams['paging'][$alias] = array('page' => 1, 'limit' => 20, 'order' => array(), 'options' => array(), 'paramType' => 'named') + (array) Hash::get($requestparams, "paging.{$alias}");
        if ($namedModel == $alias) {
            $requestparams['paging'][$alias]['page'] = $page;

            if (!empty($requestparams['named']['sort']) && !empty($requestparams['named']['direction'])) {
                $requestparams['paging'][$alias]['order'] = array($requestparams['named']['sort'] => $requestparams['named']['direction']); //FIXME
                $requestparams['paging'][$alias]['options']['order'] = $requestparams['paging'][$alias]['order']; //FIXME
                $requestparams['paging'][$alias]['options']['sort'] = $requestparams['named']['sort']; //FIXME
                $requestparams['paging'][$alias]['options']['direction'] = $requestparams['named']['direction']; //FIXME
            }
        } else {
            $requestparams['paging'][$alias]['page'] = max((int) @$paginationModel[$alias]['page'], 1);
            $requestparams['paging'][$alias]['order'] = $paginationModel[$alias]['order']; //FIXME
        }

//            $requestparams['options'] = (array)Hash::get($paginationModel, "{$alias}.options" ); //FIXME


        $requestparams['paging'][$alias]['limit'] = 20;
        $requestparams['paging'][$alias]['count'] = count($bannetteService);

        $reminder = $requestparams['paging'][$alias]['count'] % $requestparams['paging'][$alias]['limit'];

        $requestparams['paging'][$alias]['pageCount'] = ceil($requestparams['paging'][$alias]['count'] / $requestparams['paging'][$alias]['limit']);
        $requestparams['paging'][$alias]['prevPage'] = ( $requestparams['paging'][$alias]['page'] > 1 );
        $requestparams['paging'][$alias]['nextPage'] = ( $requestparams['paging'][$alias]['page'] < $requestparams['paging'][$alias]['pageCount'] );

        if ($reminder == 0) {
            $requestparams['paging'][$alias]['current'] = $requestparams['paging'][$alias]['limit'];
        } else {
            if ($requestparams['paging'][$alias]['page'] == $requestparams['paging'][$alias]['pageCount']) {
                $requestparams['paging'][$alias]['current'] = $reminder;
            } else {
                $requestparams['paging'][$alias]['current'] = $requestparams['paging'][$alias]['limit'];
            }
        }

        $this->request->params = $requestparams;

        $offset = ( ( $requestparams['paging'][$alias]['page'] <= 1 ) ? 0 : ( ( $requestparams['paging'][$alias]['page'] - 1 ) * $requestparams['paging'][$alias]['limit'] ) );
        $bannetteService = array_slice($bannetteService, $offset, $requestparams['paging'][$alias]['limit']);


        $this->Session->delete('Auth.User.Courrier.mesServices');
        $this->Session->write('Auth.User.Courrier.mesServices', $fluxCtrl);
        $this->_initBancontenuService = true;


        $this->Session->write('paginationModel', $this->request->params['paging']);
// debug($bannetteService[0]);
        return $bannetteService;
    }

    /**
     * Initialisation de l'environnement "Superadmin"
     *
     * @access private
     * @return void
     */
    private function _initSuperadminEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', SUPERADMIN_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'superadmin');
    }

    /**
     * Initialisation de l'environnement "Admin"
     *
     * @access private
     * @return void
     */
    private function _initAdminEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', ADMIN_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'admin');
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
        $this->loadModel('Service');
        $this->set('nbServices', $this->Service->find('count'));
        $this->loadModel('User');
        $this->set('nbUsers', $this->User->find('count'/*, array('conditions' => array('User.active' => true))*/));
        $this->loadModel('Profil');
        $this->set('nbProfils', $this->Profil->find('count', array('conditions' => array('Profil.active' => true, 'Profil.id !=' => 1))));
        $this->loadModel('Cakeflow.Circuit');
        $this->set('nbCircuits', $this->Circuit->find('count', array('conditions' => array('Circuit.actif' => 1))));
        $this->loadModel('Type');
        $this->set('nbTypes', $this->Type->find('count'));
        $this->set('nbSoustypes', $this->Type->Soustype->find('count'));
        $this->loadModel('Metadonnee');
        $this->set('nbMeta', $this->Metadonnee->find('count'));
        $this->loadModel('Dossier');
        $this->set('nbDossiers', $this->Dossier->find('count'));
        $this->set('nbAffaires', $this->Dossier->Affaire->find('count'));

        $this->loadModel('Desktopmanager');
        $this->set('nbBureaux', $this->Desktopmanager->find('count'));

        ////////write session menu histoire///////////////////////////
        if ($this->Session->check('Auth.User.menu.historique.items')) {
            $menuHistorique = array();
//            $desktops = $this->getSessionAllDesktops();
            $desktops = $this->Session->read('Auth.UserDesktops');

            foreach ($desktops as $desktopId => $desktopName) {
                $tabName = str_replace(" ", "", ucwords($desktopName));
                $tabName = preg_replace("/[[:punct:]]/i", "", $tabName);
                if (!in_array($tabName, array("ProfilAdministrateur", "BureauAdministrateur"))) {
                    array_push($menuHistorique, array(
                        'type' => 'menuItem',
                        'img' => 'menu_env_env_bannettes.png',
                        'txt' => $desktopName,
                        'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'historique', $tabName)
                    ));
                }
            }
            $this->Session->write('Auth.User.menu.historique.items', $menuHistorique);
        }
    }

    /**
     * Initialisation de l'environnement "Initialisateur"
     *
     * @access private
     * @return void
     */
    private function _initInitEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', INIT_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'init');
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
        $initDesktops = $this->getSessionInitDesktops();
        $initServices = $this->getSessionInitServices();

        return array(
            'desktop_id' => array_keys($initDesktops),
            'bannettes' => array('docs', 'refus', 'reponses', 'copy')
        );
    }

    /**
     * Initialisation de l'environnement "Valideur"
     *
     * @access private
     * @return void
     */
    private function _initValEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', VAL_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'val');
        $valDesktops = $this->getSessionValDesktops();
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        return array(
            'desktop_id' => array_keys($valDesktops),
            'bannettes' => array('flux', 'copy')
        );
    }

    /**
     * Initialisation de l'environnement "Valideur Editeur"
     *
     * @access private
     * @return void
     */
    private function _initValeditEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', VALEDIT_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'valedit');
        $valeditDesktops = $this->getSessionValeditDesktops();
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        return array(
            'desktop_id' => array_keys($valeditDesktops),
            'bannettes' => array('flux', 'copy')
        );
    }

    /**
     * Initialisation de l'environnement "Documentaliste" (Archiveur)
     *
     * @access private
     * @return void
     */
    private function _initArchEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', ARCH_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'arch');
        $archDesktops = $this->getSessionArchDesktops();
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        return array(
            'desktop_id' => array_keys($archDesktops),
            'bannettes' => array('flux', 'copy')
        );
    }

    /**
     * Initialisation de l'environnement "Aiguilleur" (Dispatcher)
     *
     * @access private
     * @return void
     */
    private function _initDispEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', DISP_GID);
        $this->Session->write('Auth.User.Env.actual.name', 'disp');
        $dispDesktops = $this->getSessionDispDesktops();
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        return array(
            'desktop_id' => array_keys($dispDesktops),
            'bannettes' => array('aiguillage', 'copy')
        );
    }

    /**
     * Initialisation des environnements défini dans l'application (profils personnalisés)
     *
     * @access private
     * @return void
     */
    private function _initAppdefinedEnv() {
        $this->Session->write('Auth.User.Env.actual.profil_id', 0);
        $this->Session->write('Auth.User.Env.actual.name', 'appdefined');
        $appDefinedDesktops = $this->getSessionAppDefinedDesktops();
        $initServices = $this->getSessionInitServices();
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

        if ($this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Courriers/add')) {
            $this->_add = $this->getSessionInitServices();
        }

        $listeBannettes = array('flux', 'docs', 'copy');
        return array(
            'desktop_id' => array_keys($appDefinedDesktops),
            'bannettes' => $listeBannettes
        );
    }

    /**
     * Accès à la page d'environnement
     *
     * @logical-group Environnement
     * @user-profile Superadmin
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @param boolean $fromEdit détermine si l'on vient d'enregistrer des informations
     * @param string $env détermine quel environnement sera actif (superadminenv, adminenv, userenv)
     * @return void
     */
    public function index($fromEdit = null, $env = null) {
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');

        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
        if ($fromEdit == 1) {
            $this->Session->setFlash('Les informations ont bien été enregistrées', 'growl');
        }
        if (empty($env)) {
            $env = $this->Session->read('Auth.User.Env.main');
        }
        if( $conn != 'default') {
            $collectivite = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn)) );
            $collectiviteConn = $collectivite['Collectivite']['conn'];
            if( !empty($collectiviteConn) ) {
                $this->Session->write('Auth.Collectivite.conn', $collectiviteConn ) ;
            }
        }
        // Appel pour la mise en cache
         if ($env != 'superadmin') {
			 if( empty( $this->Session->read('Auth.AllDesktopsmanagers') ) ) {
				$this->putInCache($collectiviteConn);
			}
         }
        if ($env == 'superadmin') {
            $this->setAction('superadminenv');
        } else if ($env == 'admin') {
            $this->setAction('adminenv');
        } else if ($env == 'disp') {
            $this->setAction('dispenv');
        } else {
            $this->setAction('userenv');
        }
    }

    /**
     * Initialisation de l'environnement superadmin (affichage graphique)
     *
     * @logical-group Environnement
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function superadminenv() {
        $this->set('ariane', array(__d('menu', 'Environnement') . ' ' . __d('menu', 'Env.Superadministrateur', true)));
        $this->_initSuperadminEnv();
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Initialisation de l'environnement administrateur (affichage graphique)
     *
     * @logical-group Environnement
     * @user-profile Superadmin
     *
     * @access public
     * @return void
     */
    public function adminenv() {
//        $this->set('ariane', array(__d('menu', 'Environnement') . ' ' . __d('menu', 'Env.Administrateur', true)));
        $envs = array_unique(array_merge(array($this->Session->read('Auth.User.Env.main')), array_unique($this->Session->read('Auth.User.Env.secondary'), SORT_REGULAR)), SORT_REGULAR);

        $bannettesParams = array();
        foreach ($envs as $env) {
            if ($env != 'admin' && $env != 'disp') {
                if ($env == 'init') {
                    $bannettesParams[] = $this->_initInitEnv();
                } else if ($env == 'val') {
                    $bannettesParams[] = $this->_initValEnv();
                } else if ($env == 'valedit') {
                    $bannettesParams[] = $this->_initValeditEnv();
                } else if ($env == 'arch') {
                    $bannettesParams[] = $this->_initArchEnv();
                } else if ($env == 'appdefined') {
                    $bannettesParams[] = $this->_initAppdefinedEnv();
                }
            }
        }

        $this->_bannettes = $this->_getAllBancontenus($bannettesParams);

        ////write session menu /////////////////////////////////////
        if ($this->Session->check('Auth.User.menu.flux.items')) {
            $menuFlux = array();
            foreach ($this->_bannettes as $bannetteName => $value) {
                $item = array(
                    'type' => 'menuItem',
                    'img' => 'menu_env_env_bannettes.png',
                    'txt' => __d('bannette', 'Bannette.' . $bannetteName),
                    //Emilie:Todo link correct
                    'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'userenv', $bannetteName)
                );
                array_push($menuFlux, $item);
            }
            $this->Session->write('Auth.User.menu.flux.items', $menuFlux);
        }

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
        $this->_initAdminEnv();
    }

    /**
     * Récupération des notifications
     *
     * @logical-group Environnement
     * @logical-group Notifications
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function getNotifications() {
        $settings = array(
            'read' => false,
            'date_start' => null,
            'date_end' => null,
//            'limit' => 10,
            'order' => 'desc'
        );
        $settings = array_merge($settings, $this->request->params['named']);

        $this->loadModel('Notification');
        $querydata = array(
            'fields' => array(
                'Notification.id',
                'Notification.name',
                'Notification.description',
                'Typenotif.name',
                'Courrier.id',
                'Courrier.name',
                'Notifieddesktop.desktop_id',
                'Desktop.name',
                'Notification.created'
            ),
            'contain' => false,
            'joins' => array(
                $this->Notification->join($this->Notification->Typenotif->alias),
                $this->Notification->join($this->Notification->Desktop->alias),
                $this->Notification->join($this->Notification->Notifieddesktop->alias),
                $this->Notification->Notifieddesktop->join($this->Notification->Notifieddesktop->Courrier->alias)
            ),
            'conditions' => array(
                'Notifieddesktop.read' => $settings['read'],
                'Notifieddesktop.desktop_id' => $this->SessionTools->getDesktopList(),
                'Notifieddesktop.created >' => date('Y-m-d', strtotime("-1 months")),
                'Notifieddesktop.created <=' => date('Y-m-d', strtotime("+1 day")),
            ),
//            'limit' => $settings['limit'],
            'order' => 'Notification.created ' . $settings['order']
        );
        if (!empty($settings['start']) && !empty($settings['end']) && $settings['start'] < $settings['end']) {
            $querydata['conditions'] = array_merge($querydata['conditions'], array(array('Notification.created >' => $settings['start']), array('Notification.created <' => $settings['end'])));
        }
        $notifications = $this->Notification->find('all', $querydata);
        $this->set('notifications', $notifications);
    }

    /**
     * Marquer une notification comme lue
     *
     * @logical-group Environnement
     * @logical-group Notifications
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @param integer $notificationId identifiant de la notification
     * @param integer $desktopId identifiant du profil
     * @param integer $courrierId identifiant du flux
     * @return void
     */
    public function setNotificationRead($notificationId, $desktopId, $courrierId) {
        $this->Jsonmsg->init();
        $this->loadModel('Notifieddesktop');
        $qd = array(
            'conditions' => array(
                'Notifieddesktop.desktop_id' => $desktopId,
                'Notifieddesktop.notification_id' => $notificationId,
                'Notifieddesktop.courrier_id' => $courrierId
            ),
            'recursive' => -1
        );
        $item = $this->Notifieddesktop->find('first', $qd);
        $item['Notifieddesktop']['read'] = true;
        $this->Notifieddesktop->create($item);
        if ($this->Notifieddesktop->save()) {
            $this->Jsonmsg->valid();
        } else {
            $this->Jsonmsg->error();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Marquer toutes les notifications comme lues
     *
     * @logical-group Environnement
     * @logical-group Notifications
     * @user-profile Admin
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function setAllNotificationRead() {
        $verif = array();
        $this->Jsonmsg->init();

        $desktops = $this->SessionTools->getDesktopList();
        foreach ($desktops as $desktop) {
            $verif[] = $this->Notification->setAllReadByDesktop($desktop);
        }

        if (!in_array(false, $verif, true)) {
            $this->Jsonmsg->valid();
        } else {
            $this->Jsonmsg->error();
        }
        $this->Jsonmsg->Send();
    }

    /**
     * Initialisation de l'environnement utilisateur (affichage graphique)
     *
     * @logical-group Environnement
     * @user-profile User
     *
     * @access public
     * @param integer $tab onglet à recharger après modification
     * @return void
     */
    public function userenv($tab = null) {
        $this->set('ariane', array(__d('menu', 'Environnement') . ' ' . __d('menu', 'Env.Utilisateur', true)));
        $envs = array_unique(array_merge(array($this->Session->read('Auth.User.Env.main')), array_unique($this->Session->read('Auth.User.Env.secondary'), SORT_REGULAR)), SORT_REGULAR);

        $bannettesParams = array();
        foreach ($envs as $env) {
            if ($env != 'admin' && $env != 'disp') {
                if ($env == 'init') {
                    $bannettesParams[] = $this->_initInitEnv();
                } else if ($env == 'val') {
                    $bannettesParams[] = $this->_initValEnv();
                } else if ($env == 'valedit') {
                    $bannettesParams[] = $this->_initValeditEnv();
                } else if ($env == 'arch') {
                    $bannettesParams[] = $this->_initArchEnv();
                } else if ($env == 'appdefined') {
                    $bannettesParams[] = $this->_initAppdefinedEnv();
                }
            }
        }

        $this->_bannettes = $this->_getAllBancontenus($bannettesParams);

        ////write session menu /////////////////////////////////////
        $menuFlux = array();
        foreach ($this->_bannettes as $bannetteName => $value) {

            $item = array(
                'type' => 'menuItem',
                'img' => 'menu_env_env_bannettes.png',
                'txt' => __d('bannette', 'Bannette.' . $bannetteName),
                'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'userenv', $bannetteName)
            );
            array_push($menuFlux, $item);
        }

        $this->Session->write('Auth.User.menu.flux.items', $menuFlux);

		$this->loadModel('Desktop');
		$this->Desktop->setDataSource(Configure::read('conn'));
        ////////write session menu histoire///////////////////////////
        $menuHistorique = array();
//        $desktops = $this->getSessionAllDesktops();
		$desktops = $this->Session->read('Auth.UserDesktops');
        foreach ($desktops as $desktopId => $desktopName) {
            $tabName = str_replace(" ", "", ucwords($desktopName));
            $tabName = preg_replace("/[[:punct:]]/i", "", $tabName);
            if (!in_array($tabName, array("ProfilAdministrateur", "BureauAdministrateur"))) {
                array_push($menuHistorique, array(
                    'type' => 'menuItem',
                    'img' => 'menu_env_env_bannettes.png',
                    'txt' => $desktopName,
                    'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'historique', $tabName)
                ));
            }
        }

		$this->Session->write('Auth.User.menu.historique.items', $menuHistorique);

        /////////////////////////////////////////////////////////////
        $this->set('userId', $this->Session->read('Auth.User.id'));
        $this->set('bannettes', $this->_bannettes);
        $this->set('tab', $tab);
        $fromMesServices = false;
        $this->set('fromMesServices', $fromMesServices);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));

		$isDsActif = false ;
		if( Configure::read('Webservice.DS') ) {
			$this->loadModel('Connecteur');
			$hasDsActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Démarches%',
						'Connecteur.use_ds' => true
					),
					'contain' => false
				)
			);
			if ($hasDsActif) {
				$isDsActif = true;
			}
		}
		$this->set( 'isDsActif', $isDsActif );

		$isDmActif = false ;
		if( Configure::read('Webservice.Directmairie') ) {
			$this->loadModel('Connecteur');
			$hasDmActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Direct-Mairie%',
						'Connecteur.use_dm' => true
					),
					'contain' => false
				)
			);
			if ($hasDmActif) {
				$isDmActif = true;
			}
		}
		$this->set( 'isDmActif', $isDmActif );
    }

    /**
     * Initialisation de l'environnement aiguilleur (affichage graphique)
     *
     * @logical-group Environnement
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function dispenv($tab = null) {


        ////////write session menu histoire///////////////////////////
        $menuHistorique = array();
//        $desktops = $this->getSessionAllDesktops();
		$desktops = $this->Session->read('Auth.UserDesktops');

        foreach ($desktops as $desktopId => $desktopName) {
            $tabName = str_replace(" ", "", ucwords($desktopName));
            $tabName = preg_replace("/[[:punct:]]/i", "", $tabName);
            if (!in_array($tabName, array("ProfilAdministrateur", "BureauAdministrateur"))) {
                array_push($menuHistorique, array(
                    'type' => 'menuItem',
                    'img' => 'menu_env_env_bannettes.png',
                    'txt' => $desktopName,
                    'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'historique', $tabName)
                ));
            }
        }
        $this->Session->write('Auth.User.menu.historique.items', $menuHistorique);
        ///////////////////////////////////////////////////////////////////////
        $this->set('tab', $tab);
        $this->set('ariane', array(
            __d('menu', 'Environnement') . ' ' . __d('menu', 'Env.Aiguilleur', true)
        ));

        $bannettesParams = array($this->_initDispEnv());
        $this->_bannettes = $this->_getAllBancontenus($bannettesParams);
        ////write session menu /////////////////////////////////////
        $menuFlux = array();
        foreach ($this->_bannettes as $bannetteName => $value) {
            $item = array(
                'type' => 'menuItem',
                'img' => 'menu_env_env_bannettes.png',
                'txt' => __d('bannette', 'Bannette.' . $bannetteName),
                'url' => array('plugin' => null, 'controller' => 'environnement', 'action' => 'dispenv', $bannetteName)
            );
            array_push($menuFlux, $item);
        }
        $this->Session->write('Auth.User.menu.flux.items', $menuFlux);

        $this->loadModel('Desktop');
        $this->Desktop->setDataSource(Configure::read('conn'));
        $this->set('userId', $this->Session->read('Auth.User.id'));

        $this->set('bannettes', $this->_bannettes);

        // Autorisation de fusion des scans
        $isAuthFusionScan = false;
        $isAuthFusionScan = $this->userAclCheck(array('model' => 'User', 'foreign_key' => $this->Session->read('Auth.User.id')), 'controllers/Documents/getFichierScane');
        $this->set('isAuthFusionScan', $isAuthFusionScan);

        $fromMesServices = false;
        $this->set('fromMesServices', $fromMesServices);
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Récupération de l'historique des flux d'un utilisateur
     *
     * @logical-group Environnement
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function historique($tabName = null) {
        $this->set('ariane', array(
            '<a href="/environnement/userenv/0">' . __d('menu', 'Environnement') . '</a>',
            __d('menu', 'Historique', true)
        ));
        $this->loadModel('Bancontenu');
//        $desktops = $this->getSessionAllDesktops();
		$desktops = $this->Session->read('Auth.UserDesktops');
        $historique = array();


        $paginationModel = $this->Session->read('paginationModel');
        $page = Hash::get($this->request->params, 'named.page');
        $namedModel = Hash::get($this->request->params, 'named.model');

		$this->loadModel('Desktop');
		$this->Desktop->setDataSource(Configure::read('conn'));
        foreach ($desktops as $desktopId => $desktopName) {
            if ($this->Session->read('Auth.User.Desktop.id') == $desktopId) {
                $groupId = $this->Session->read('Auth.User.Desktop.Profil.id');
            } else {
                $secDktp = ($this->Session->read('Auth.User.SecondaryDesktops'));
                for ($i = 0; $i < count($secDktp); $i++) {
                    if ($secDktp[$i]['id'] == $desktopId) {
                        $groupId = $secDktp[$i]['Profil']['id'];
                    }
                }
            }

            $params = array('class' => 'Desktop', 'alias' => Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($desktopName))));
            $alias = $params['alias'];
            $this->{$alias} = ClassRegistry::init($params);

            if ($groupId != ADMIN_GID) {
                $historique[$desktopName] = array();

                $sq = $this->{$alias}->Bancontenu->sq(
                    array(
                        'fields' => array('DISTINCT bancontenus.courrier_id'),
                        'alias' => 'bancontenus',
                        'conditions' => array(
                            'bancontenus.desktop_id' => $desktopId
                        ),
                        'contain' => false
                    )
                );
				$limitationDelai = Configure::read('Bannettehistorique.Restrictionmax');
				$limitationType = trim( substr( Configure::read('Bannettehistorique.Restrictionmax'), 3, 6) );
				$translateLimitationType = [
					'day' => 'jour',
					'days' => 'jours',
					'month' => 'mois',
					'months' => 'mois',
					'year' => 'an',
					'years' => 'an'
				];
				$translationType = $translateLimitationType[$limitationType];
				$this->set('translationType', $translationType );

				$querydata = array(
                    'fields' => array_merge(
                        array(
                            'DISTINCT Courrier.id',
                            'Courrier.objet',
                            'Courrier.reference',
                            'Courrier.priorite',
                            'Courrier.mail_retard_envoye',
                            'Courrier.direction',
                            'Courrier.name',
                            'Courrier.created',
                            'Courrier.modified',
                            'Courrier.datereception',
                            'Courrier.parent_id',
                            'Courrier.user_creator_id',
                            'Organisme.id',
                            'Organisme.name',
                            'Contact.id',
                            'Contact.name',
                            'Soustype.id',
                            'Soustype.name'
                        ),
                        $this->{$alias}->fields(),
                        $this->{$alias}->Bancontenu->fields(),
                        $this->{$alias}->Bancontenu->Desktop->fields(),
                        $this->{$alias}->Bancontenu->Courrier->Soustype->Type->fields()
                    ),
                    'contain' => false,
                    'joins' => array(
                        $this->{$alias}->join($this->{$alias}->Bancontenu->alias, array('conditions' => array( "Bancontenu.courrier_id IN ( {$sq} )"))),
                        $this->{$alias}->Bancontenu->join($this->{$alias}->Bancontenu->Desktop->alias),
                        $this->{$alias}->Bancontenu->join($this->{$alias}->Bancontenu->Courrier->alias),
                        $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Organisme->alias),
                        $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Contact->alias),
                        $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Soustype->alias, array('type' => 'LEFT OUTER')),
                        $this->{$alias}->Bancontenu->Courrier->Soustype->join($this->{$alias}->Bancontenu->Courrier->Soustype->Type->alias, array('type' => 'LEFT OUTER')),
                        $this->{$alias}->Bancontenu->Courrier->join($this->{$alias}->Bancontenu->Courrier->Traitement->alias, array('type' => 'LEFT OUTER')),
                        $this->{$alias}->Bancontenu->Courrier->Traitement->join($this->{$alias}->Bancontenu->Courrier->Traitement->Circuit->alias, array('type' => 'LEFT OUTER'))
                    ),
                    'conditions' => array(
						'Desktop.id' => $desktopId,
						'Bancontenu.etat <>' => '-1', // A vérifier
						"Courrier.datereception >" => date('Y-m-d', strtotime($limitationDelai)),
						"Courrier.datereception <=" => date('Y-m-d'),
						"Bancontenu.courrier_id IN ( {$sq} )"
                    ),
                    'order' => array(
                        'Desktop.name' => 'DESC',
                        'Courrier.modified' => 'DESC'
                    )
                );

				$historique[$desktopName] = $this->{$alias}->find('all', $querydata); //NEW
                $parent = array();

                for ($i = 0; $i < count($historique[$desktopName]); $i++) {
                    if (!empty($historique[$desktopName][$i]['Courrier']['parent_id'])) {
                        $qdParent = $querydata;
                        $qdParent['conditions'] = array('Courrier.id' => $historique[$desktopName][$i]['Courrier']['parent_id']);
                        $parent = $this->_dataParent($historique[$desktopName][$i]['Courrier']['parent_id']);
                    }
                    $historique[$desktopName][$i] = $this->_processFlux($historique[$desktopName][$i], false, true, true, false, $parent); //le parametre read est a true pour les flux dans l historique
                }

				// Fonction permettant de supprimer les "doublons" de flux dans les bannettes
				// Permet de ne pas afficher les doublons mais uniquement LE flux associé à l'agent
				foreach($historique[$desktopName] as $k => $flux) {
					if ($historique[$desktopName][$k]['id'] === @$historique[$desktopName][$k+1]['id'] ) {
						unset($historique[$desktopName][$k]);
					}
				}

                // Recherche sur historique
                if (!empty($this->request->data['Courrier'])) {
                    $this->loadModel('Courrier');

                    $conditionsSearch = $this->Courrier->search($this->request->data);
                    $querydata['conditions'] = Hash::merge($querydata['conditions'], $conditionsSearch);

                    $flux = $this->{$alias}->find('all', $querydata);
                    $historique[$desktopName] = $flux;

                    for ($i = 0; $i < count($historique[$desktopName]); $i++) {
                        $historique[$desktopName][$i] = $this->_processFlux($historique[$desktopName][$i], false, true, true, false, $parent); //le parametre read est a true pour les flux dans l historique;
                    }
                }

                // Pour avoir le nombre en entête
				if( !empty($historique[$desktopName])) {
					foreach ($historique[$desktopName] as $desktopname => $flux) {
						$number[$desktopName] = count($historique[$desktopName]);
					}
				}
				else {
					$number[$desktopName] = 0;
				}
            }
        }

//        $this->Session->write('paginationModel', $this->request->params['paging']);
        $this->set('historique', $historique);
		$this->set( 'number', $number );

        $this->set('tabName', $tabName);
        if (Configure::read('Use.AuthRights') ) {
//            $this->set('types', $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true)));
//            $types = $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true));
            $types = $this->Session->read('Auth.TypesAll');
            $newTypes = Hash::map($types, "{n}", array($this, '__getType'));
            $newSoustypes = Hash::map($types, "{n}", array($this, '__getSousType'));
            foreach ($newTypes as $key => $value) {
                $typeOptions[$value['id']] = $value['name'];
            }
            foreach ($newSoustypes as $key => $value) {
                if (isset($value) && !empty($value)) {
                    $soustype = array();
                    foreach ($value as $keySoustype => $valueSoustype) {
                        $soustype[$valueSoustype['id']] = $valueSoustype['name'];
                        $soustypeOptionsOrigine[$valueSoustype['id']] = $valueSoustype['name'];
                    }
                    if (!empty($value[0])) {
                        $soustypeOptions[$value[0]['type_id']] = $soustype;
                    }
                }
            }
            $this->set('typeOptions', $typeOptions);
            $this->set('soustypeOptions', $soustypeOptions);
            $this->set('soustypeOptionsOrigine', $soustypeOptionsOrigine);
        } else {
            $this->loadModel('Type');
            $types = $this->Type->find('all', array('contain' => array('Soustype' => array('conditions' => array('Soustype.active' => true), 'order' => 'Soustype.name ASC')), 'order' => array('Type.name ASC'), 'conditions' => array('Type.active' => 1), 'recursive' => -1));
            $newTypes = Hash::map($types, "{n}", array($this, '__getType'));
            $newSoustypes = Hash::map($types, "{n}", array($this, '__getSousType'));
            foreach ($newTypes as $key => $value) {
                $typeOptions[$value['id']] = $value['name'];
            }
            foreach ($newSoustypes as $key => $value) {
                if (isset($value) && !empty($value)) {
                    $soustype = array();
                    foreach ($value as $keySoustype => $valueSoustype) {
                        $soustype[$valueSoustype['id']] = $valueSoustype['name'];
                        $soustypeOptionsOrigine[$valueSoustype['id']] = $valueSoustype['name'];
                    }
                    $soustypeOptions[$value[0]['type_id']] = $soustype;
                }
            }
            $this->set('typeOptions', $typeOptions);
            $this->set('soustypeOptions', $soustypeOptions);
            $this->set('soustypeOptionsOrigine', $soustypeOptionsOrigine);
        }

        $fromMesServices = false;
        $this->set('fromMesServices', $fromMesServices);
        $this->set(
                'etat', array(
            '-1' => 'Refusé',
            '0' => 'Validé',
            '1' => 'En cours de traitement',
            '2' => 'Clos'
                )
        );
        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Récupération de l'historique des flux d'un utilisateur
     *
     * @logical-group Environnement
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function infosLogiciel() {

    }

    public function setSessionPagination($bannetteName, $colone) {
        $this->autoRender = false;
        $alias = Inflector::camelize("bannette_" . Inflector::camelize(Inflector::slug($bannetteName)));
//        $colone = 'Courrier.' . $colone;
        $paginationModelBannette = $this->Session->read('paginationModel.' . $alias);
		if( isset($paginationModelBannette['order']) && !empty($paginationModelBannette['order']) ) {
			$paginationModelBannetteOrderIndex = array_keys($paginationModelBannette['order']);
			$paginationModelBannetteOrderValue = array_values($paginationModelBannette['order']);
			if( !empty($paginationModelBannetteOrderIndex) ) {
				if (in_array($colone, $paginationModelBannetteOrderIndex)) {
					for ($i = 0; $i < count($paginationModelBannetteOrderIndex); $i++) {
						if ($paginationModelBannetteOrderIndex[$i] == $colone) {
							if (!empty($paginationModelBannetteOrderValue)) {
								if ($paginationModelBannetteOrderValue[$i] == 'desc') {
									$paginationModelBannette['order'][$paginationModelBannetteOrderIndex[$i]] = 'asc';
								} else {
									$paginationModelBannette['order'][$paginationModelBannetteOrderIndex[$i]] = 'desc';
								}
							}
						}
					}
				}
			}
			else {
				$paginationModelBannette['order'] = array($colone => 'asc');
			}
        }
        else {
            $paginationModelBannette['order'] = array($colone => 'asc');
        }
        return ($this->Session->write('paginationModel.' . $alias, $paginationModelBannette));

//        debug($this->Session->read('paginationModel.'.$alias));
//        debug('/environnement/userenv/'.$bannetteName);
//        $this->redirect('/environnement/userenv/'.$bannetteName);
    }

    function __getType($array) {
        return $array['Type'];
    }

    function __getSousType($array) {
        return $array['Soustype'];
    }

    function __getOrganisme($array) {
        return $array['Organisme'];
    }

    function __getContact($array) {
        return $array['Contact'];
    }

    /**
     * Fonction permettant de mettre les informations en cache lors de la 1ère connexion de l'utilisateur
     */
    public function putInCache( $collectiviteConn ) {
        $this->loadModel('Courrier');
        $this->loadModel('Desktop');

        if( !empty($collectiviteConn) ) {
            $this->Session->write('Auth.Collectivite.conn', $collectiviteConn ) ;
        }

		$this->Session->write('Auth.ProfilAdmin', '');
		if( Configure::read( 'Use.AuthRights') && empty( $this->Session->read('Auth.ProfilAdmin') ) ) {
			if( $this->Session->read('Auth.User.Env.main' ) == 'admin' ) {
				$this->Session->write('Auth.ProfilAdmin', 'Oui');
			}
			if( in_array( 'admin', $this->Session->read('Auth.User.Env.secondary' ) ) ) {
				$this->Session->write('Auth.ProfilAdmin', 'Oui');
			}
		}
		// Mise en cache de la liste des entrées BAN
		if( empty( $this->Session->read('Auth.Ban') ) ) {
			$this->Session->write('Auth.Ban', $this->Courrier->Organisme->Ban->find('all', array('order' => array('Ban.id ASC'),'contain' => array('Bancommune' => array('order' => 'Bancommune.name ASC')),'recursive' => -1) ) );
		}
		// Mise en cache des origines de flux
		$this->Session->write('Auth.Origineflux', '');
		if( empty($this->Session->read('Auth.Origineflux') ) ) {
			$this->Session->write('Auth.Origineflux', $this->Courrier->Origineflux->find('list', array('conditions' => array('Origineflux.active' => true), 'order' => array('Origineflux.name ASC'))) );
		}

        // Mise en cache de tous les bureaux
		$this->Session->write('Auth.AllDesktopsmanagers', '');
		$this->Session->write('Auth.DesktopmanagerInit', '');
		$this->Session->write('Auth.UserDesktops', '');
        if( empty($this->Session->read('Auth.AllDesktopsmanagers') ) ) {
            $userDesktops = $this->getSessionAllDesktops();
			$this->Session->write('Auth.UserDesktops', $userDesktops );
            $dsktManager = array();
			$desktopmanagerInit = [];
            foreach($userDesktops as $dId => $desktopname ) {
                $dsktManager[$dId] = $this->Courrier->Desktop->getDesktopManager( $dId );

				$desktopInit = $this->Desktop->find(
					'first',
					[
						'conditions' => [
							'Desktop.id' => $dId,
							'Desktop.profil_id' => INIT_GID
						],
						'contain' => false,
						'recursive' => -1
					]
				);

				if( !empty($desktopInit) ) {
					$desktopmanagerInit = $this->Desktop->getDesktopManager($dId);
				}
            }
			$this->Session->write('Auth.DesktopmanagerInit', $desktopmanagerInit);
            $desktopsmanagers = array();
            foreach( $dsktManager as $dId => $key ) {
                foreach( $key as $desktopsmanagerId) {
                    $desktopsmanagers[$dId] = $this->Courrier->Desktop->Desktopmanager->find(
                        'first',
                        array(
                            'conditions' => array(
                                'Desktopmanager.id' => $desktopsmanagerId
                            ),
                            'contain' => false,
                            'recursive' => -1
                        )
                    );
                }
            }
            $this->Session->write('Auth.AllDesktopsmanagers', $desktopsmanagers );
        }
        // Mise en cache des habilitatins sur les types actifs
		$this->Session->write('Auth.TypesActif', '');
        if( Configure::read( 'Use.AuthRights') && empty( $this->Session->read('Auth.TypesActif') ) ) {
            $this->Session->write('Auth.TypesActif', $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true, 'onlyActif' => true)) );
        }
        // Mise en cache des habilitatins sur tous les types
		$this->Session->write('Auth.TypesAll', '');
        if( Configure::read( 'Use.AuthRights') && empty( $this->Session->read('Auth.TypesAll') ) ) {
            $this->Session->write('Auth.TypesAll', $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true, 'onlyActif' => false)) );
        }
        // Mise en cache des habilitatins sur les sous-types
		$this->Session->write('Auth.Soustypes', '');
        if( Configure::read( 'Use.AuthRights') && empty( $this->Session->read('Auth.Soustypes') ) ) {
            $this->Session->write('Auth.Soustypes', $this->DataAuthorized->getAuthSoustypes(array('find' => 'all')) );
        }

        // Mise en cache de tous les bureaux
		$this->Session->write('Auth.AllDesktops', '');
        if( empty($this->Session->read('Auth.AllDesktops') ) ) {
            $users = $this->Courrier->Desktop->User->find(
                'all',
                array(
                    'recursive' => -1,
                    'contain' => array(
                        'Desktop'
                    ),
                    'conditions' => array('User.active' => true),
                    'order' => 'User.nom ASC'
                )
            );
            foreach( $users as $user) {
                $allDesktops[$user['Desktop']['id']] = $user['User']['nom'].' '.$user['User']['prenom'];
            }
            $this->Session->write('Auth.AllDesktops', $allDesktops );
        }
        // Mise en cache des organismes et contacts pour les listes déroulantes
        if( empty( $this->Session->read('Auth.OrganismesOptions')) || empty( $this->Session->read('Auth.ContactsOptions')) ) {
            $addPrivateIds = $this->Courrier->Organisme->Addressbook->find('list', array('conditions' => array('Addressbook.private' => true)) );
            $privateAccess = $this->Session->read('Auth.User.addressbook_private');
            if($privateAccess) {
                $conditionsAddress = array(
                    'Organisme.active' => true
                );
            }
            else {
                if(!empty($addPrivateIds) ) {
                    $conditionsAddress = array(
                        'Organisme.active' => true,
                        'Organisme.addressbook_id NOT IN' => array_keys( $addPrivateIds)
                    );
                }
                else {
                    $conditionsAddress = array(
                        'Organisme.active' => true
                    );
                }
            }

            $organismes = $this->Courrier->Organisme->find('all', array(
                'conditions' => $conditionsAddress,
                'order' => array('Organisme.name ASC'),
                'contain' => array(
                    'Contact' => array(
                        'conditions' => array('Contact.active' => true),
                        'order' => 'Contact.nom ASC'
                    )
                ),
                'recursive' => -1
            ));
            $newOrganismes = Hash::map($organismes, "{n}", array($this, '__getOrganisme'));
            $newContacts = Hash::map($organismes, "{n}", array($this, '__getContact'));
            foreach ($newOrganismes as $key => $value) {
                if( Configure::read('Conf.CD')  ) {
                    $organismOptions[$value['id']] = $value['name'].' ('.$value['ville'].') ';
                }
                else {
                    $organismOptions[$value['id']] = $value['name'];
                }

                if( $value['name'] == 'Sans organisme') {
                    $organismOptions[$value['id']] = $value['name'];
                }
            }

            foreach ($newContacts as $key => $value) {
                if (isset($value) && !empty($value)) {
                    $contact = array();
                    foreach ($value as $keyContact => $valueContact) {
                        $contact[$valueContact['id']] = $valueContact['nom'] . ' ' . $valueContact['prenom'];
                        if( Configure::read('Conf.CD')  ) {
                            $contact[$valueContact['id']] = $valueContact['nom'] . ' ' . $valueContact['prenom'] . ' ('. $valueContact['ville'] .')';
                        }
                        if( $valueContact['nom'] == '0Sans contact') {
                            $contact[$valueContact['id']] = $valueContact['name'];
                        }
                    }
                    $contactOptions[$value[0]['organisme_id']] = $contact;
                }
            }
            $this->Session->write('Auth.OrganismesOptions', $organismOptions );
            $this->Session->write('Auth.ContactsOptions', $contactOptions );
        }
        // Mise en cache des entrées de carnet d'adresse
        if( empty($this->Session->read('Auth.Addressbook') ) ) {
            $this->Session->write('Auth.Addressbook', $this->Courrier->Organisme->Addressbook->find('list', array('conditions' => array('Addressbook.active' => true), 'order' => 'Addressbook.name ASC', 'recursive' => -1)) );
        }
        if( empty($this->Session->read('Auth.AddressbookPrivate') ) ) {
            $this->Session->write('Auth.AddressbookPrivate', $this->Courrier->Organisme->Addressbook->find('list', array('conditions' => array('Addressbook.active' => true, 'Addressbook.private' => true), 'order' => 'Addressbook.name ASC', 'recursive' => -1)) );
        }
        // Mise en cache des types de voie
        if( empty($this->Session->read('Auth.Typesvoie') ) ) {
            $this->Session->write('Auth.Typesvoie', $this->Courrier->Organisme->Addressbook->listTypesVoie() );
        }
        // Mise en cache des habilitatins de métadonnées
		$this->Session->write('Auth.MetasList', '');
        if( Configure::read( 'Use.AuthRights') && empty($this->Session->read('Auth.MetasList') ) ) {
            $this->Session->write('Auth.MetasList', $this->DataAuthorized->getAuthMetadonnees(array('find' => 'list') ) );
        }
        // Mise en cache des habilitatins de métadonnées
		$this->Session->write('Auth.MetasUpdate', '');
        if( Configure::read( 'Use.AuthRights') && empty($this->Session->read('Auth.MetasUpdate') ) ) {
            $this->Session->write('Auth.MetasUpdate', $this->DataAuthorized->getAuthMetadonnees(array('action' => 'update') ) );
        }

		// Mise en cache des habilitatins pour l'ajout/2dtion d'un contact dans le flux
		$this->Session->write('Auth.ContactInFluxAllowed', '');
		if( Configure::read( 'Use.AuthRights') && empty( $this->Session->read('Auth.ContactInFluxAllowed') ) ) {
			$rightToContactAllowed = true;
			// On regarde si un droit est défini pour le profil qui se connecte
			$aroUserConnected = $this->Aro->find(
				'first',
				array(
					'conditions' => array(
						'Aro.foreign_key' => $this->Session->read(  'Auth.User.Desktop.id')
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			// Si oui, on cherche la lsite des drotis définis en administration pour le profil connecté
			if( !empty($aroUserConnected ) ) {
				$rightToContact = $this->Right->find(
					'first',
					array(
						'conditions' => array(
							'Right.model' => 'Desktop',
							'Right.aro_id' => $aroUserConnected['Aro']['id']
						),
						'contain' => false
					)
				);
				// Si les droits existent, on retourne la valeur définie pour l'ajout/édition d'un contact/organisme
				if (!empty($rightToContact)) {
					$rightToContactAllowed = $rightToContact['Right']['action_carnet_adresse_flux'];
				}
			}
			$this->Session->write('Auth.ContactInFluxAllowed', $rightToContactAllowed );
		}

        // Mise en cache des droits d'accès au courrier pour l'utilisateur connecté
		$this->Session->write('Auth.Rights', '');
        if( empty($this->Session->read('Auth.Rights') ) ) {
            $userDesktops = $this->getSessionAllDesktops();
            foreach($userDesktops as $dId => $dName ) {
                $aro = array('model' => 'Desktop', 'foreign_key' => $dId);
                $droits[$dId] = array(
                    'getInfos' => $this->Acl->check($aro, 'controllers/Courriers/getInfos'),
                    'getMeta' => $this->Acl->check($aro, 'controllers/Courriers/getMeta'),
                    'getTache' => $this->Acl->check($aro, 'controllers/Courriers/getTache'),
                    'getAffaire' => $this->Acl->check($aro, 'controllers/Courriers/getAffaire'),
                    'getFlowControls' => $this->Acl->check($aro, 'controllers/Courriers/getFlowControls'),
                    'setInfos' => $this->Acl->check($aro, 'controllers/Courriers/setInfos'),
                    'setMeta' => $this->Acl->check($aro, 'controllers/Courriers/setMeta'),
                    'setTache' => $this->Acl->check($aro, 'controllers/Courriers/setTache'),
                    'setAffaire' => $this->Acl->check($aro, 'controllers/Courriers/setAffaire'),
                    'getArs' => $this->Acl->check($aro, 'controllers/Courriers/getArs'),
                    'getRelements' => $this->Acl->check($aro, 'controllers/Courriers/getRelements'),
                    'getDocuments' => $this->Acl->check($aro, 'controllers/Courriers/getDocuments'),
                    'getCircuit' => $this->Acl->check($aro, 'controllers/Courriers/getCircuit'),
                    'getSms' => $this->Acl->check($aro, 'controllers/Courriers/getSms'),
                    'edit' => true
                );
            }

            $rights = array();
            foreach($droits as $droit) {
                foreach($droit as $key => $value) {
                    if(isset($rights[$key]) === false) {
                        $rights[$key] = $value;
                    } else {
                        $rights[$key] = $rights[$key] || $value;
                    }
                }
            }
            $this->Session->write('Auth.Rights', $rights);
        }

		$this->Session->write('Auth.Civilite', '');
        if( empty( $this->Session->read( 'Auth.Civilite') )) {
            $civilite = array(1 => 'Monsieur', 2 => 'Madame', 3 => 'Monsieur et Madame', '' => '');
            if(Configure::read('CD') == 81) {
                $civilite = array(
                    '1' => 'Monsieur',
                    '2' => 'Madame',
                    '3' => 'Madame et Monsieur',
                    '4' => 'Messieurs',
                    '5' => 'Mesdames',
                    '6' => "Mesdames et Messieurs",
                    '-1' => "--Sans--"
                );
            }
            $this->Session->write( 'Auth.Civilite', $civilite );
        }

        // Mise en cache des habilitatins sur les types actifs
		$this->Session->write('Auth.Services', '');
        if( empty( $this->Session->read('Auth.Services') ) ) {
            $this->Session->write('Auth.Services', $this->Courrier->Service->find('list') );
        }

		$this->Session->write('Auth.Desktops', '');
		if( empty( $this->Session->read('Auth.Desktops') ) ) {
			$this->Session->write('Auth.Desktops', $this->Courrier->Desktop->find('list') );
		}

        // Mise en cache des bureaux par profils
		$this->Session->write('Auth.DesktopsByProfils', '');
        if( empty( $this->Session->read('Auth.DesktopsByProfils') ) ) {
            $allDesktopsManagers = $this->Courrier->User->Desktop->Desktopmanager->getAllDesktopsByProfils();
			// On ne doit pas/peut pas envoyer de flux en copie au IP ou Pastell
			unset( $allDesktopsManagers['-3'] );
			unset( $allDesktopsManagers['-1'] );
            $this->Session->write('Auth.DesktopsByProfils', $allDesktopsManagers);
        }

        // Mise en cache des bureaux liés à des aiguilleurs et initiateurs uniquement
		$this->Session->write('Auth.DeskDispInits', '');
        if( empty( $this->Session->read('Auth.DeskDispInits') ) ) {
            $deskDispInits = $this->Courrier->User->Desktop->Desktopmanager->getAllDesktopsByProfils(array(DISP_GID, INIT_GID));
            $this->Session->write('Auth.DeskDispInits', $deskDispInits);
        }


		// Mise en cache des bureaux liés à des acteurs pouvant intervenir dnas un circuit
		$this->Session->write('Auth.DeskInCircuit', '');
		if( empty( $this->Session->read('Auth.DeskInCircuit') ) ) {
			$deskInCircuit = $this->Courrier->User->Desktop->Desktopmanager->getAllDesktopsByProfils(array(VALEDIT_GID, VAL_GID, ARCH_GID));
			$this->Session->write('Auth.DeskInCircuit', $deskInCircuit);
		}

		// Mise en cache des bureaux liés à des acteurs pouvant intervenir dnas un circuit avec étape de type iParpaheur
		$this->Session->write('Auth.DeskInCircuitWithParapheur', '');
		if( empty( $this->Session->read('Auth.DeskInCircuitWithParapheur') ) ) {
			$deskInCircuitWithParapheur = $this->Courrier->User->Desktop->Desktopmanager->getAllDesktopsByProfils(array(VALEDIT_GID, VAL_GID, ARCH_GID), true);
			$this->Session->write('Auth.DeskInCircuitWithParapheur', $deskInCircuitWithParapheur);
		}
        // Mise en cache des types/sous-types lors de la création d'un flux (avant insertion en circuit
		$this->Session->write('Auth.TypeOptions', '');
        if( Configure::read( 'Use.AuthRights') && empty( $this->Session->read('Auth.TypeOptions') ) ) {
            $types = $this->DataAuthorized->getAuthTypes(array('withSoustypes' => true, 'onlyActif' => true));
            $newTypes = Hash::map($types, "{n}", array($this, '__getType'));
            $newSoustypes = Hash::map($types, "{n}", array($this, '__getSousType'));
            foreach ($newTypes as $key => $value) {
                $typeOptions[$value['id']] = $value['name'];
            }

            foreach ($newSoustypes as $key => $value) {
                if (isset($value) && !empty($value)) {
                    $soustype = array();
                    foreach ($value as $keySoustype => $valueSoustype) {
                        $soustype[$valueSoustype['id']] = $valueSoustype['name'];
                    }
                    if (!empty($value[0])) {
                        $soustypeOptions[$value[0]['type_id']] = $soustype;
                    }
                    else {
                        $type_id = Hash::extract($value, '{n}.type_id');
                        $soustypeOptions[$type_id[0]] = $soustype;
                    }
                }
            }
            $this->Session->write('Auth.TypeOptions', @$typeOptions);
            $this->Session->write('Auth.SoustypeOptions', @$soustypeOptions);
        }


        // Mise en cache des données liées à PASTELL
		$this->loadModel('Connecteur');
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%PASTELL%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->Session->write('Pastell.listSoustypeIp', '');
		if( !empty($hasPastellActif) && empty( $this->Session->read('Pastell.listSoustypeIp') ) ) {
			$PastellComponent = new NewPastellComponent();
			$id_entity = $hasPastellActif['Connecteur']['id_entity'];
			// Récup des sous-types dispos
			$listSoustypeIp = $PastellComponent->getCircuits($id_entity, Configure::read('Pastell.fluxStudioName'));
			$this->Session->write('Pastell.listSoustypeIp', $listSoustypeIp);
		}

		 // Mise en cache des données liées au iParapheur
		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true
				),
				'contain' => false
			)
		);
		$this->Session->write('Iparapheur.listSoustype', '');
		if( !empty($hasParapheurActif) && empty( $this->Session->read('Iparapheur.listSoustype') ) ) {
			$typeParapheur = $this->Courrier->Traitement->Circuit->Etape->listeSousTypesParapheur($hasParapheurActif);
			if (!empty($typeParapheur)) {
				$this->Session->write('Iparapheur.listSoustype', $typeParapheur['soustype']);
			} else {
				$this->Session->write('Iparapheur.listSoustype', array());
			}
		}


		// Mise en cache des informations des fonctions de contact
		$this->Session->write('Auth.Fonctions', '');
		if( empty( $this->Session->read('Auth.Fonctions') ) ) {
			$this->Session->write('Auth.Fonctions', $this->Courrier->Contact->Fonction->find('list') );
		}
		// Mise en cache de tous les utilisateurs actifs
		if( empty($this->Session->read('Auth.AllUsers') ) ) {
			$users = [];
			$userActif = $this->Courrier->User->find(
				'all',
				array(
					'conditions' => array(
						'User.active'=> true
					),
					'contain' => false,
					'order' => 'User.nom ASC'
				)
			);
			foreach( $userActif as $user) {
				$users[$user['User']['id']] = $user['User']['nom'].' '.$user['User']['prenom'];
			}
			$this->Session->write('Auth.AllUsers', $users);
		}
    }
}

?>
