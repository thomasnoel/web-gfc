<?php

/**
 * Modèles de réponse
 *
 * Rmodels controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class RmodelsController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Rmodels';

	/**
	 * Controller components
	 *
	 * @var array
	 * @access public
	 */
	public $components = array('Linkeddocs');

	/**
	 * Etat d'envoi des fichiers
	 *
	 * @access private
	 * @var array
	 */
	private $_uploadStatus = array(
		'NO_UPLOAD' => 0,
		'UPLOAD_START' => 1,
		'UPLOAD_END' => 2
	);

	/**
	 * Types MIME et extensions autorisés
	 *
	 * @access private
	 * @var array
	 */
	private $_allowed = array(
		'application/vnd.oasis.opendocument.text' => 'odt',
		'application/vnd.oasis.opendocument.text-template' => 'ott',
		'application/force-download' => 'odt' //FIXME
	);

     public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('deletefile', 'add', 'edit');
    }

	/**
	 * Ajout d'un modèle de réponse
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @logical-group Modèles de réponses
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $soustype_id identifiant du sous-type
	 * @return void
	 */
	public function add($soustype_id = null) {
		if (empty($this->request->data)) {
			$this->set('soustype_id', $soustype_id);
		} else {
			if ($this->request->is('post')) {
				$upload = $this->_uploadStatus['NO_UPLOAD'];
				$rmodel = $this->request->data;
				//traitement de l upload des fichiers
				if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
					$upload = $this->_uploadStatus['UPLOAD_START'];
					$ext = '';
					if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
						$ext = $matches[1];
					}
					if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
						$message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
					} else {
						if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
//							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
						} else {
							$upload = $this->_uploadStatus['UPLOAD_END'];
							$rmodel['Rmodel']['size'] = $_FILES['myfile']['size'];
							$rmodel['Rmodel']['ext'] = $ext;
							$rmodel['Rmodel']['mime'] = $_FILES['myfile']['type'];
						}
					}
				}
				//enregistrement des infos en base si pas  d upload ou si l upload est terminé
				$create = false;
				if ($upload != $this->_uploadStatus['UPLOAD_START']) {
					$this->Rmodel->create($rmodel);
					$rmodelsaved = $this->Rmodel->save();
					if (!empty($rmodelsaved)) {
						$create = true;
						$message = __d('default', 'save.ok');
					}
					//suppression du fichier si un fichier est uploadé
					if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
						unlink($_FILES['myfile']['tmp_name']);
					}
				}
				$this->set('create', $create);
				$this->set('message', $message);
			}
		}
	}

	/**
	 * Suppression d'un modèle de réponse
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @logical-group Modèles de réponses
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant du modèle de réponse
	 * @return void
	 */
	public function delete($id = null) {
		$this->Jsonmsg->init(__d('default', 'delete.error'));
		if ($this->Rmodel->delete($id)) {
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		}
		$this->Jsonmsg->send();
	}

	/**
	 * Téléchargement d'un modèle de réponse
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @logical-group Modèles de réponses
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant du modèle de réponse
	 * @throws NotFoundException
	 * @return void
	 */
	public function download($id) {
		$rmodel = $this->Rmodel->find('first', array('conditions' => array('Rmodel.id' => $id), 'recursive' => -1));
		if (empty($rmodel)) {
			throw new NotFoundException();
		}
		$this->autoRender = false;
		Configure::write('debug', 0);
		header("Pragma: public");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Content-type: application/octet-stream");
		header("Content-Disposition: attachment; filename=\"" . Inflector::slug($rmodel['Rmodel']["name"]) . '.' . $this->_allowed[$rmodel['Rmodel']['mime']] . "\"");
		header("Content-length: " . $rmodel['Rmodel']['size']);
		print $rmodel['Rmodel']['content'];
		exit();
	}


    /**
     * Récupération de la pré-visualisation d'un modèle d'accusé de réception (génération si nécessaire)
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles d'accusés de réception
     * @user-profile User
     *
     * @access public
     * @param integer $rmodelId
     * @throws NotFoundException
     */
    public function getPreview($rmodelId) {
        $rmodel = $this->Rmodel->find('first', array('conditions' => array('Rmodel.id' => $rmodelId), 'recursive' => -1));
        if (empty($rmodel)) {
            throw new NotFoundException();
        }

        $preview = __d('document', 'Document.preview.error');

        $preview = $this->Rmodel->generatePreview($rmodelId);


        $mime = $rmodel['Rmodel']['mime'];

        $tmpPreviewFile = '';
        $tmpPreviewFileBasename = '';
        $previewType = '';

        if (in_array($mime, $this->Rmodel->convertibleMimeTypes) || $mime == "application/pdf" || $mime == "PDF" /* || $mime == "application/vnd.oasis.opendocument.text" */) {
            $previewType = 'flexpaper';
            $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($rmodel['Rmodel']['name']) . "_preview.pdf";
            $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
            //purge des fichiers de preview précédents
            $this->Linkeddocs->purge();
            file_put_contents($tmpPreviewFile, $preview);
        } else {
            foreach ($this->Rmodel->otherMimeTypes as $type => $mimes) {

                if (in_array($mime, array_keys($mimes))) {
                    $previewType = $type;
                    $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($rmodel['Rmodel']['name'])  . "_preview." . $mimes[$mime];
                    $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
                    //purge des fichiers de preview précédents
                    $this->Linkeddocs->purge();
                    file_put_contents($tmpPreviewFile, $rmodel['Rmodel']['content']);
                }
            }
        }
//debug($tmpPreviewFileBasename);
//debug($mime);
//die();
        if (!empty($tmpPreviewFile)) {
            chmod($tmpPreviewFile, 0777);
        }
        $this->set('fileName', $tmpPreviewFileBasename);

        $this->set('previewType', $previewType);
        $this->set('mime', "pdf");
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
    }

    public function deletefile($rmodelId) {
        $this->autoRender = false;
        $rmodel = $this->Rmodel->find('first', array('conditions' => array('Rmodel.id' => $rmodelId), 'recursive' => -1));
        $name = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($rmodel['Rmodel']['name']) . "_preview.pdf";
        unlink( APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $name );
    }

    /**
     * Edition d'un document Bordereau
     *
     * @logical-group Documents
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {

//        $this->autoRender = false;
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Rmodel->create();
            $rmodel = $this->request->data;


            if ($this->Rmodel->save($rmodel)) {
                if ($this->request->is('post')) {
                    $upload = $this->_uploadStatus['NO_UPLOAD'];
                    $rmodel = $this->request->data;

                    //traitement de l upload des fichiers
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        $upload = $this->_uploadStatus['UPLOAD_START'];
                        $ext = '';
                        if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                            $ext = $matches[1];
                        }
                        if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
                            $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
    //							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                            } else {
                                $upload = $this->_uploadStatus['UPLOAD_END'];
                                $rmodel['Rmodel']['size'] = $_FILES['myfile']['size'];
                                $rmodel['Rmodel']['ext'] = $ext;
                                $rmodel['Rmodel']['mime'] = $_FILES['myfile']['type'];
                            }
                        }
                    }
                    //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                    $create = false;
//$this->log( $upload );
//$this->log( $rmodel );
                    if ($upload != $this->_uploadStatus['UPLOAD_START']) {
                        $this->Rmodel->create($rmodel);
                        $rmodelsaved = $this->Rmodel->save();
                        if (!empty($rmodelsaved)) {
                            $create = true;
                            $message = __d('default', 'save.ok');
                            $this->Jsonmsg->valid($message);
                        }
                        //suppression du fichier si un fichier est uploadé
                        if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                            unlink($_FILES['myfile']['tmp_name']);
                        }
                    }
                    $this->set('create', $create);
                    $this->set('message', $message);
                }



                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $rmodel = $this->Rmodel->find(
                'first',
                array(
                    'fields' => array(
                        'Rmodel.id',
                        'Rmodel.name',
                        'Rmodel.ext'
                    ),
                    'conditions' => array(
                        'Rmodel.id' => $id
                    ),
                    'contain' => false
                )
            );
            if (empty($rmodel)) {
                throw new NotFoundException();
            }
        }

        $this->set('rmodel', $rmodel);
    }

}
