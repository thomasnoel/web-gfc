<?php

/**
 * Répertoires
 *
 * Repertoires controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class RepertoiresController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Repertoires';

    /**
     * Controller uses
     *
     * @var array
     * @access public
     */
    public $uses = array('Repertoire', 'Recherche');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $isRecherche = array_keys($this->Session->read('Auth.User.menu.gestion_mes_flux.items'));
        if(in_array('recherche', $isRecherche)){

            $this->set('isRecherche','true');
        }


    }

    /**
     * Récupération de la liste des répertoires
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function getRepertoires() {
        /*
        $repertoires = $this->Repertoire->find(
            'threaded', array(
                'conditions' => array(
                    'Repertoire.user_id' => $this->Session->read('Auth.User.id')
                ),
                'contain' => false
            )
        );
        */
        $repertoires = $this->Repertoire->find(
            'threaded', array(
                'conditions' => array(
                    'Repertoire.user_id' => $this->Session->read('Auth.User.id')
                ),
                /*
                'joins' => array(
                    $this->Repertoire->join( 'CourrierRepertoire', array( 'type' => 'INNER' ) ),

                )*/
                'contain' => array(
                    'Courrier'
                )
            )
        );
        for ($i = 0; $i < count($repertoires); $i++) {
            $repertoires[$i]['right_view'] = true;
            $repertoires[$i]['right_edit'] = true;
            $repertoires[$i]['right_delete'] = true;
            $repertoires[$i]['nbCourrier'] = count($repertoires[$i]['Courrier']);
        }
        $this->set('repertoires', $repertoires);
    }

    /**
     * Ajout d'un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Repertoire->create($this->request->data);
            if ($this->Repertoire->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
            $this->redirect(array('controller' => 'environnement', 'action' => 'userenv/1'));
        } else {
            $user_id = $this->Session->read('Auth.User.id');
            $repertoires = $this->Repertoire->find('list', array('conditions' => array('Repertoire.user_id' => $user_id)));

            $this->set('repertoires', $repertoires);
            $this->set('user_id', $user_id);
        }
    }

    /**
     * Edition d'un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du répertoire
     * @return void
     */
    public function edit($id = null) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Repertoire->create($this->request->data);
            if ($this->Repertoire->save()) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
            $this->redirect(array('controller' => 'environnement', 'action' => 'userenv/1'));
        } else {
            $querydata = array(
                'conditions' => array(
                    'Repertoire.user_id' => $this->Session->read('Auth.User.id'),
                    'Repertoire.id <>' => $id
                )
            );
            $repertoires = $this->Repertoire->find('list', $querydata);
            $this->request->data = $this->Repertoire->find('first', array('conditions' => array('Repertoire.id' => $id)));
            $this->set('repertoires', $repertoires);
        }
    }

    /**
     * Récupération de la liste des flux d'un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du répertoire
     * @return void
     */
    /* public function getFlux($id = null) {
      if ($id != null) {
      $querydata = array(
      'conditions' => array(
      'Repertoire.id' => $id
      ),
      'contain' => array(
      'Courrier' => array(
      'Contactinfo' => array(
      'Contact'
      )
      )
      )
      );

      $rep_flux = $this->Repertoire->find('first', $querydata);

      //            debug($rep_flux);
      $flux = array();
      foreach ($rep_flux['Courrier'] as $item) {
      //                debug($item);
      $flux[] = array(
      'Courrier' => $item,
      'Contact' => array_merge( array('name' => ''), Hash::extract( $item, 'Contactinfo.Contact' ) ),
      'right_read' => false,
      'right_view' => true,
      'retard' => !$this->Repertoire->Courrier->isInTime($item['id'])
      );
      }
      //            debug($flux);
      $this->set('flux', $flux);
      $this->set('repertoire_id', $id);
      $this->set('viewAction', $this->Session->read('Auth.User.Actions.view'));
      }
      } */


    public function getFlux($id = null) {
        if ($id != null) {
//			$querydata = array(
//				'conditions' => array(
//					'Repertoire.id' => $id
//				),
//                'contain' => array(
//                    'Courrier' => array(
//                        'Contactinfo' => array(
//                            'Contact'
//                        )
//                    )
//                )
//			);
//            $cr = $this->Repertoire->CourrierRepertoire->find(
//                'all',
            $querydata = array(
                'conditions' => array(
                    'CourrierRepertoire.repertoire_id' => $id
                ),
                'contain' => array(
                    'Courrier' => array(
                        'Contact',
                         'Organisme'
                    )
                )
//                )
            );

//            debug($cr);
//			$alias = 'courrier';
//            $this->{$alias} = ClassRegistry::init( 'Courrier' );

            $paginate = $this->paginate;
            $paginate['CourrierRepertoire'] = $querydata;
            $this->paginate = $paginate;
            $rep_flux = $this->paginate('CourrierRepertoire');

            $flux = array();
            foreach ($rep_flux as $i => $item) {
                $flux[] = array(
                    'Courrier' => $item['Courrier'],
                    'Contact' => array_merge(array('name' => ''), Hash::extract($item['Courrier'], 'Contact')),
                    'right_read' => false,
                    'right_view' => true,
                    'retard' => !$this->Repertoire->Courrier->isInTime($item['Courrier']['id'])
                );
            }
//debug($flux);
            $this->set('flux', $flux);
            $this->set('repertoire_id', $id);
            $this->set('viewAction', $this->Session->read('Auth.User.Actions.view'));
        }
    }

    /**
     * Suppression d'un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du répertoire
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $repertoires = $this->Repertoire->find(
            'threaded', array(
                'conditions' => array(
                    'Repertoire.id' => $id
                ),
                'contain' => array(
                    'Courrier'
                )
            )
        );
        $sousRepertoires = $this->Repertoire->find('count',array('conditions'=>array('Repertoire.parent_id'=>$id)));
        foreach ($repertoires as $repertoire) {
            if(count($repertoire['Courrier'])==0 && $sousRepertoires ==0){
                 if ($id != null) {

                    if ($this->Repertoire->delete($id)) {
                        $this->Jsonmsg->valid(__d('default', 'delete.ok'));
                    }
                }
            }
        }

        $this->Jsonmsg->send();
    }

    /**
     * Associe ou dissocie un flux à un répertoire
     *
     * @access protected
     * @param integer $id identifiant du répertoire
     * @param integer $flux_id identifiant du flux
     * @param boolean $link si true le flux est associé au répertoire, sinon il est dissocié
     * @return void
     */
    protected function _attach($id, $flux_id, $link) {
        if ($id != null && $flux_id != null) {
            $this->Repertoire->id = $id;


            $repertoire = $this->Repertoire->read();
            if (!empty($repertoire)) {
                $this->Repertoire->Courrier->id = $flux_id;
                $flux = $this->Repertoire->Courrier->read();
                if (!empty($flux)) {
                    if ($link) {
                        $exist = $this->Repertoire->CourrierRepertoire->find(
                            'first',
                            array(
                                'fields' => array(
                                    'CourrierRepertoire.id'
                                ),
                                'conditions' => array(
                                    'CourrierRepertoire.courrier_id' => $flux['Courrier']['id'],
                                    'CourrierRepertoire.repertoire_id' => $id
                                ),
                                'contain' => false
                            )
                        );
                        //Si le flux est déjà présent dans un répertoire, on ne fait rien
                        if( empty( $exist ) ) {
                            $repertoire['Courrier'][] = $flux['Courrier']['id'];
                            $this->Repertoire->save($repertoire);
                        }
                    } else {
                        $courrierRepertoire = $this->Repertoire->CourrierRepertoire->find('first', array('conditions' => array(array('repertoire_id' => $repertoire['Repertoire']['id']), array('courrier_id' => $flux['Courrier']['id']))));
                        if (!empty($courrierRepertoire)) {
                            $this->Repertoire->CourrierRepertoire->delete($courrierRepertoire['CourrierRepertoire']['id']);
                        }
                    }
                }
            }
        }
    }

    /**
     * Liaison de plusieurs flux à un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function linkProfil() {
        $this->Repertoire->User->id = $this->Session->read('Auth.User.id');
        $user = $this->Repertoire->User->find(
            'first', array(
                'conditions' => array(
                    'User.id' => $this->Repertoire->User->id
                ),
                'contain' => false
            )
        );
        $this->set('user', $user);

        if (!empty($this->request->data)) {
            if (!empty($this->request->data['Repertoire']['name'])) {
                $this->render(false);
                $this->Repertoire->create($this->request->data);
                $this->Repertoire->save();
                foreach ($this->request->data['itemToLink'] as $item) {
                    $this->_attach($this->Repertoire->id, $item, true);
                }
            } else if (!empty($this->request->data['RepertoireToLink'])) {
                $this->render(false);
                foreach ($this->request->data['itemToLink'] as $item) {
                    $this->_attach($this->request->data['RepertoireToLink'], $item, true);
                }
            } else if (!empty($this->request->data['itemToLink'])) {
                $repertoires = $this->Repertoire->find(
                    'list',
                    array(
                        'conditions' => array(
                            'Repertoire.user_id' => $user['User']['id']
                    ),
                    'contain' => false
                    )
                );
                $this->set('repertoires', $repertoires);
                $this->set('itemList', $this->request->data);
            }
        } else {
            $this->render(false);
        }
    }

    /**
     * Liaison d'un flux à un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du répertoire
     * @param integer $flux_id identifiant du flux
     * @return void
     */
    public function link($id, $flux_id) {
        $this->_attach($id, $flux_id, true);
    }

    /**
     * Suppression des liaisons des flux d'un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @param type $id identifiant du répertoire
     * @return void
     */
    public function unlinkProfil($id = null) {
        $this->render(false);
        if ($id != null) {
            foreach ($this->request->data['itemToUnlink'] as $item) {
                $this->_attach($id, $item, false);
            }

        }
    }

    /**
     * Suppression de la liaison d'un flux à un répertoire
     *
     * @logical-group Répertoires
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du répertoire
     * @param integer $flux_id identifiant du flux
     * @return void
     */
    public function unlink($id, $flux_id) {
        $this->_attach($id, $flux_id, false);
    }

}

?>
