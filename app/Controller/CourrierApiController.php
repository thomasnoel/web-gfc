<?php

App::import('Sanitize');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Flux
 *
 * CourrierApi controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Développé par LIBRICIEL SCOP
 * @link https://www.libriciel.fr/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class CourrierApiController extends AppController {

	public function beforeFilter()
	{
		parent::beforeFilter();
		$this->Auth->allow();
	}

	public $uses = ['Courrier', 'Collectivite', 'Bancontenu'];

	public $components = ['Api'];


	/**
	 * Retourne tousles flux
	 * @return CakeResponse
	 */
	public function list()
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$courriers = $this->getCourriers( $this->Api->getLimit(), $this->Api->getOffset() );
		return new CakeResponse([
			'body' => json_encode($courriers),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

	/**
	 * Retourne les informations d'un flux
	 * @param $courrierId
	 * @return CakeResponse
	 */
	public function find($courrierId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$courrier = $this->getCourrier($courrierId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($courrier),
			'status' => 200,
			'type' => 'application/json'
		]);
	}


	/**
	 * Déclenche la création d'un flux
	 * @return CakeResponse
	 */
	public function add()
	{
		//Jeu d'essai avec contact
		/*'{
			"name": "Test",
			"objet": "Objet du test",
			"user_id": 2,
			"soustype_id": 7,
			"desktopmanager_id": 25,
			"profil_id": 3,
			"contact": {
				"name": "Nathanaël AUZOLAT",
				"civilite": 1,
				"nom": "AUZOLAT",
				"prenom": "Nathanaël",
				"numvoie": "1 BIS",
				"nomvoie": "RUE DEL MAZADE",
				"cp": "34670",
				"compl": "Tout en haut",
				"ville": "SAINT-BRES",
				"organismeName": "Libriciel SCOP",
				"organismeId": 8
			}
		}';	*/
		// Jeu essai sans contact
		/*{
		"name": "Test",
		"objet": "Objet du test",
		"user_id": 2,
		"soustype_id": 7,
		"desktopmanager_id": 25,
		"profil_id": 3,
		"contact": {}
	}';*/


		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());

			$data = json_decode($this->request->input(), true);
			if( !isset($data['desktopmanager_id']) ) {
				$this->response->statusCode(403);
				return "error_desktopmanager";
			}
			if( !isset($data['profil_id']) ) {
				$this->response->statusCode(403);
				return "error_profil";
			}

			$files = array();
			if( isset( $this->request->params['form'] ) && !empty($this->request->params['form'] ) ) {
				$files = $this->request->params['form'];
			}

			if( !empty($data['contact']) ) {
				$contactCreated = $this->createContact($data['contact']);
				if( !empty($contactCreated) ) {
					$contactId = $contactCreated['contactId'];
					$organismeId = $contactCreated['organismeId'];
					$dataCourrierWithContactUpdated = json_decode( $this->request->input() );
					$dataCourrierWithContactUpdated->contactId = $contactId;
					$dataCourrierWithContactUpdated->organismeId = $organismeId;
					$courrierCreated = $this->createCourrier((array)$dataCourrierWithContactUpdated, $files);
				}
				else {
					$contactCreated['contactId'] = Configure::read('Sanscontact.id');
					$contactCreated['organismeId'] = Configure::read('Sansorganisme.id');
					$courrierCreated = $this->createCourrier($data, $files);
				}
			}
			else {
				$contactCreated['contactId'] = Configure::read('Sanscontact.id');
				$contactCreated['organismeId'] = Configure::read('Sansorganisme.id');
				$courrierCreated = $this->createCourrier($data, $files);
			}
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}
		if(!$courrierCreated['success']) {
			return $this->Api->formatCakePhpValidationMessages($courrierCreated['errors']);
		}

		return new CakeResponse([
			'body' => json_encode([
				'courrierId' => $courrierCreated['courrierId'],
				'contactId' => $contactCreated['contactId'],
				'organismeId' => $contactCreated['organismeId']
			]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	/**
	 * Focntion de création du courrier
	 * @param array $data
	 * @param $files
	 * @return mixed
	 */
	private function createContact(array $data)
	{
		return $this->Courrier->createContactForCourrierApiV1($data);
	}

	/**
	 * Focntion de création du courrier
	 * @param array $data
	 * @param $files
	 * @return mixed
	 */
	private function createCourrier(array $data, $files)
	{
		return $this->Courrier->createCourrierApiV1($data, $files);
	}


	/**
	 * Supprime un flux selon son ID
	 * @param $courrierId
	 * @return CakeResponse
	 */
	public function delete($courrierId){
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		$bancontenus = array_keys($this->Courrier->Bancontenu->find('list', array('conditions' => array('Bancontenu.courrier_id' => $courrierId))));
		for ($i = 0; $i < count($bancontenus); $i++) {
			$this->Courrier->Bancontenu->delete($bancontenus[$i]);
		}
		$this->Courrier->delete($courrierId);
		return new CakeResponse([
			'status' => 200,
			'body' => json_encode(['message' => 'Flux supprimé']),
			'type' => 'application/json'
		]);
	}

	/**
	 * Retourne tous les flux
	 * @return array|array[]
	 */
	private function getCourriers( $limit, $offset )
	{
		$conditions = array(
			'limit' => $limit,
			'offset' => $offset
		);
		$courriers = $this->Courrier->find('all', $conditions);

		return Hash::extract($courriers, "{n}.Courrier");
	}


	/**
	 * Retourne les infos d'un flux
	 * @param $courrierId
	 * @return array|array[]
	 */
	private function getCourrier($courrierId)
	{
		$courrier = $this->Courrier->find('first', [
			'conditions' => ['Courrier.id' => $courrierId],
		]);

		if (empty($courrier)) {
			throw new NotFoundException('Courrier non trouvé / inexistant');
		}

		return Hash::extract($courrier, "Courrier");
	}


	/**
	 * Déclenche la modification d'un flux
	 * @return CakeResponse
	 */
	public function update($courrierId)
	{
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$this->getCourrier($courrierId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}
		$courrier = json_decode($this->request->input(), true);
		$courrier['id'] = $courrierId;
//		$res = $this->createCourrier($courrier, $_FILES);
		$res = $this->Courrier->save($courrier);
		if (!$res) {
			return $this->Api->formatCakePhpValidationMessages($this->Courrier->validationErrors);
		}

		return new CakeResponse([
			'body' => json_encode(['courrierId' => $res['Courrier']['id']]),
			'status' => 201,
			'type' => 'application/json'
		]);
	}


	/**
	 * Vérification de l'état d'un flux
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $courrierId identifiant du flux
	 * @return array
	 */
	public function status($courrierId) {
		$this->autoRender = false;
		try {
			$this->Api->authentification($this->Api->getApiToken());
			$courrier = $this->Courrier->getStatus($courrierId);
		} catch (Exception $e) {
			return $this->Api->sendErrorJson($e);
		}

		return new CakeResponse([
			'body' => json_encode($courrier),
			'status' => 200,
			'type' => 'application/json'
		]);
	}

}
