<?php

App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 * Documents principaux et pièces jointes
 *
 * Documents controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class DocumentsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var array
     */
    public $name = 'Documents';

    /**
     * Controller components
     *
     * @access public
     * @var array
     */
    public $components = array('Linkeddocs', 'Conversion', 'Progress');

    /**
     * Etat d'envoi des fichiers
     *
     * @access private
     * @var array
     */
    private $_uploadStatus = array(
        'NO_UPLOAD' => 0,
        'UPLOAD_START' => 1,
        'UPLOAD_END' => 2
    );

    /**
     * Types MIME et extensions autorisés
     *
     * @access private
     * @var array
     */
    private $_allowed = array(
        'ott' => array(
            'application/vnd.oasis.opendocument.text' => 'odt',
            'application/vnd.oasis.opendocument.text-template' => 'ott',
            'application/force-download' => 'odt'
        )
    );

    /**
     *
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('convertToPdf', 'getProgress', 'getProgressDocument', 'getFileListPreview', 'getFileScannePreview', 'getPreview', 'setMainDoc', 'getFichierScane');
    }

    /**
     * Définition d'une pièce jointe comme document principal
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du document
     * @return void
     */
    public function setMainDoc($id) {
        $this->Jsonmsg->init();
        $this->Document->begin();

        $fluxId = $this->Document->field('Document.courrier_id', array('Document.id' => $id));
        $docs = $this->Document->find('all', array('conditions' => array('Document.courrier_id' => $fluxId)));

        $valid = array();
        foreach ($docs as $doc) {
            $this->Document->create();
            $doc['Document']['main_doc'] = false;
            $saved = $this->Document->save($doc);
            $valid[] = !empty($saved);
        }

        $this->Document->id = $id;
        $valid[] = $this->Document->saveField('main_doc', true);

        if (!in_array(false, $valid, true)) {
            $this->Jsonmsg->valid();
            $this->Document->commit();
        } else {
            $this->Document->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Ajout d'un document pricipal ou d'une pièce jointe
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @return void
     */
    public function upload() {
        $conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        if ($this->request->is('post')) {
            $ext = '';

            if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                $ext = $matches[1];
            }

            $courrier = $this->Document->Courrier->find(
                'first',
                array(
                    'conditions' => array(
                        'Courrier.id' => $this->request->data['courrierId']
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            $fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
            $name  = preg_replace( "/[:']/", "_", $_FILES['myfile']['name'] );
            $name = str_replace( '/', '_', $name );
            $file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
            file_put_contents( $file, file_get_contents($_FILES['myfile']['tmp_name']));
            $path = $file;

			$doc = $this->Document->find(
				'first',
				array(
					'conditions' => array(
						'Document.courrier_id' => $this->request->data['courrierId'],
						'Document.path' => $path
					),
					'contain' => false
				)
			);

			if( empty( $doc ) ) {

				$docs = $this->Document->find('list', array('conditions' => array('Document.courrier_id' => $this->request->data['courrierId'])));
				$docName = str_replace(' ', '_', replace_accents($_FILES['myfile']['name']));
				$docName = str_replace('’', '_', $docName);
				$doc = array(
					'Document' => array(
						'size' => $_FILES['myfile']['size'],
						'mime' => $_FILES['myfile']['type'],
						'ext' => $ext,
						'courrier_id' => $this->request->data['courrierId'],
						'desktop_creator_id' => $this->Session->read('Auth.User.Desktop.id'),
						'main_doc' => empty($docs),
						//                    'content' => file_get_contents($_FILES['myfile']['tmp_name']),
						'path' => $path,
						'name' => $docName
					)
				);

				if (strtolower($doc['Document']['ext']) == 'pdf') {
					$doc['Document']['mime'] = 'application/pdf';
				}

				$flux = $this->Document->Courrier->find(
					'first',
					array(
						'conditions' => array(
							'Courrier.id' => $this->request->data['courrierId']
						),
						'contain' => false,
						'recursive' => -1
					)
				);


				$this->Document->create($doc);
				$docsaved = $this->Document->save();
				if ($docsaved) {
					$uploadError = 'Upload.error';
					if ($doc['Document']['mime'] == 'application/vnd.oasis.opendocument.text') {
						$sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS . $this->request->data['courrierId'];
						$Folder = new Folder($sessionFluxIdDir, true, 0777);
						$generatedFile = file_put_contents($sessionFluxIdDir . DS . $doc['Document']['name'], file_get_contents($file));
					}
					$this->loadModel('Journalevent');
					$datasSession = $this->Session->read('Auth.User');
					$msg = "L'utilisateur " . $this->Session->read('Auth.User.username') . " a ajouté le document " . $docsaved['Document']['name'] . " (" . $docsaved['Document']['id'] . ") au flux " . $flux['Courrier']['reference'] . ", le " . date('d/m/Y à H:i:s');
					$this->Journalevent->saveDatas($datasSession, $this->action, $msg, 'info', $flux);
				}
			}
			else {
				$uploadValid = false;
				$uploadError = 'Upload.error.same_name';
			}
            $this->set('uploadValid', !empty($docsaved));
            $this->set('uploadError', $uploadError);
            $this->set('fluxId', $this->request->data['courrierId']);
        }
    }

    /**
     * Suppression d'un document principal ou d'une pièce jointe
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $documentId identifiant du document
     * @throws NotFoundException
     * @return void
     */
    public function delete($documentId) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        $document = $this->Document->find('first', array('fields' => array('Document.id', 'Document.name', 'Document.path', 'Document.courrier_id'), 'conditions' => array('Document.id' => $documentId), 'recursive' => -1));
        $fluxId = $document['Document']['courrier_id'];
        $documentName = $document['Document']['name'];
        $dirFileGenerated = @scandir(WEBDAV_DIR . DS . $conn . DS .$fluxId);
        //
        if (isset($dirFileGenerated) && !empty($dirFileGenerated)) {
            if (in_array($documentName, $dirFileGenerated)) {
                $fileToDelete = WEBDAV_DIR . DS . $conn . DS .$fluxId . DS . $documentName;
                unlink($fileToDelete);
            }
        }
        if (empty($document)) {
            throw new NotFoundException();
        }

        $documentPath = $document['Document']['path'];
        if( !empty( $documentPath ) ) {
			if( file_exists($documentPath) ) {
				unlink($documentPath);
			}
        }

        $this->Jsonmsg->init(__d('default', 'delete.error'));
        if ($this->Document->delete($documentId)) {
            $flux = $this->Document->Courrier->find(
                'first',
                array(
                    'conditions' => array(
                        'Courrier.id' => $fluxId
                    ),
                    'contain' => false,
                    'recursive' => -1
                )
            );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            if( !empty( $flux['Courrier']['reference'] ) ) {
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le document ".$documentName. " (".$documentId.") du flux ".$flux['Courrier']['reference'].", le ".date('d/m/Y à H:i:s');
            }
            else {
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a supprimé le document ".$documentName. " (".$documentId."), le ".date('d/m/Y à H:i:s');
            }
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        }
        $this->Jsonmsg->send();
    }

    /**
     * Téléchargement d'un document principal ou d'une pièce jointe
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $documentId identifiant du document
     * @throws NotFoundException
     * @return void
     */
    public function download($documentId) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        $document = $this->Document->find('first', array('conditions' => array('Document.id' => $documentId), 'recursive' => -1));
        if (empty($document)) {
            throw new NotFoundException();
        }

        ignore_user_abort(true);
        set_time_limit(0); // disable the time limit for this script


        $path = APP . DS . WEBROOT_DIR . DS . 'files/webdav' . DS . $conn . DS . "{$document['Document']['courrier_id']}" . DS . "{$document['Document']['name']}";

        $documentPath = $document['Document']['path'];
        if( !empty( $documentPath ) ) {
            $file = file_get_contents($documentPath );
            $path = $documentPath;
        }
        else {
            if( file_exists( $path) ) {
                $file = file_get_contents($path);
            }
            else {
                $path = APP . DS . WEBROOT_DIR . DS . 'files' . DS . "{$document['Document']['name']}";
                $file = file_put_contents($path, $document['Document']['content']);
            }
        }
        $ext = $document['Document']['ext'];
        $handle = fopen($path, "r");

        $fullPath = $path;

        if ($fd = fopen($fullPath, "r")) {
            $fsize = filesize($fullPath);
            $path_parts = pathinfo($fullPath);
            switch ($ext) {
                case "pdf":
                    header("Content-type: application/pdf");
                    header("Content-Disposition: attachment; filename=\"" . $path_parts["basename"] . "\""); // use 'attachment' to force a file download
                    break;
                case "odt":
                    header("Pragma: public");
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Content-type: application/vnd.oasis.opendocument.text");
                    header("Content-Disposition: attachment; filename=\"" . $path_parts["basename"] . "\"");
                    break;
                // add more headers for other content types here
                default;
                    header("Pragma: public");
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Content-type: application/octet-stream");
                    header('Content-Transfer-Encoding: binary');
                    header("Content-Disposition: attachment; filename=\"" . $path_parts["basename"] . "\"");
                    break;
            }
            header("Content-length: $fsize");
            header("Cache-control: private"); //use this to open files directly
            while (!feof($fd)) {
                $buffer = fread($fd, 2048);
                echo $buffer;
            }
        }
        fclose($fd);
        $pathToUnlink = APP . DS . WEBROOT_DIR . DS . 'files' . DS . "{$document['Document']['name']}";
        if( file_exists( $pathToUnlink ) ) {
            unlink($pathToUnlink);
        }
        exit;
    }

    /**
     * Récupération de la pré-visualisation (miniature) d'un document principal ou d'une pièce jointe
     *
     * @logical-group Flux
     * @logical-group Context
     * @user-profile User
     *
     * @access public
     * @param integer $documentId identifiant du document
     * @throws NotFoundException
     * @return void
     */
    public function getPreview($documentId, $fullEcran = null) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        $document = $this->Document->find('first', array('conditions' => array('Document.id' => $documentId), 'recursive' => -1, 'contain' => false));
//debug($document['Document']['name']);die();
        if (empty($document)) {
            throw new NotFoundException();
        }
        $preview = __d('document', 'Document.preview.error');

        $fluxId = $document['Document']['courrier_id'];
        $documentName = $document['Document']['name'];
        $documentPath = $document['Document']['path'];
		$mime = $document['Document']['mime'];

        if (!empty($fluxId) && is_dir(WEBDAV_DIR . DS . $conn . DS .$fluxId) /*&& empty( $documentPath )*/ && $mime != 'application/pdf') {
            $dirFileGenerated = @scandir(WEBDAV_DIR . DS . $conn . DS .$fluxId);
            if (in_array($documentName, $dirFileGenerated)) {
                $pathToDocToRegenerate = WEBDAV_DIR . DS . $conn . DS .$fluxId . DS . $documentName;

                if( isset($pathToDocToRegenerate) && !empty(file_get_contents($pathToDocToRegenerate)) ) {
                	if( !empty( $documentPath ) ) {
						file_put_contents($documentPath, file_get_contents($pathToDocToRegenerate));
					}
				}

				chmod($pathToDocToRegenerate, 0777);
                if (!empty($pathToDocToRegenerate)) {
                    $preview = $this->Document->generatePreview($documentId, $pathToDocToRegenerate);
                    $document['Document']['preview'] = $preview;
                }
            }
        }
        if (empty($document['Document']['preview'])) {
            $preview = $this->Document->generatePreview($documentId);
        } else {
            $preview = $document['Document']['preview'];
        }

		$tmpPreviewFile = '';
		$tmpPreviewFileBasename = '';
		$previewType = '';


		// Concaténation des PDFs
		$docConcatenatedNotExists = $this->Document->find(
			'first',
			array(
				'conditions' => array(
					'Document.courrier_id' => $fluxId,
					'Document.name' => 'allConcatenatedPdf.pdf',
					'Document.mime' => 'application/pdf',
					'Document.ext' => 'pdf',
				),
				'recursive' => -1,
				'contain' => false
			)
		);

		if( Configure::read( 'Concatenation.Pdf') && empty($docConcatenatedNotExists) ) {
			$courrier = $this->Document->Courrier->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.id' => $fluxId
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			// On regarde tous les documents PDFs présents
			$allDocuments = $this->Document->find(
				'all',
				array(
					'fields' => array(
						'Document.id',
						'Document.courrier_id',
						'Document.mime',
						'Document.ext',
						'Document.path',
						'Document.main_doc'
					),
					'conditions' => array(
						'Document.courrier_id' => $fluxId
					),
					'recursive' => -1,
					'contain' => false
				)
			);

			$path = [];
			foreach( $allDocuments as $doc ) {
				if( file_exists($doc['Document']['path']) ){
					// On les passe en Pièce jointe
					$this->Document->updateAll(
						array( 'Document.main_doc' => false ),
						array( 'Document.id' => $doc['Document']['id'])
					);
					if( $doc['Document']['mime'] == 'application/pdf' && ( $doc['Document']['ext'] == 'pdf' || $doc['Document']['ext'] == 'PDF' ) ) {
						// on récupère leurs chemins respectifs
						$path[] = $doc['Document']['path'];
					}
				}
			}
			$path = implode( ' ', $path);

			if( !empty($path) ) {
				// On concatène le tout dans un fichier nommé allConcatenatedPdf.pdf
				$fileoutput = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . 'allConcatenatedPdf.pdf';
				$cmd = 'pdftk ' . $path . ' cat output ' . $fileoutput;
				exec(escapeshellcmd($cmd));

				// Ojn enregistre ce fichier
				$doc = array(
					'Document' => array(
						'size' => strlen($fileoutput),
						'mime' => 'application/pdf',
						'ext' => 'pdf',
						'courrier_id' => $fluxId,
						'desktop_creator_id' => $this->Session->read('Auth.User.Desktop.id'),
						'main_doc' => true,
						'path' => $fileoutput,
						'name' => 'allConcatenatedPdf.pdf'
					)
				);
				$this->Document->create($doc);
				$docsaved = $this->Document->save();
				$documentPath = $docsaved['Document']['path'];
			}
		}

        if( !empty( $documentPath ) && file_exists($documentPath) ){
        	// PDF
        	if( $mime == "application/pdf" || $mime == "PDF" ) {
				$previewType = 'flexpaper';
				$preview = file_get_contents( $documentPath );
				$tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview.pdf";
				$tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
				//purge des fichiers de preview précédents
				$this->Linkeddocs->purge();
				file_put_contents($tmpPreviewFile, $preview);
			}
        	// Autre convertible
            if (in_array($mime, $this->Document->convertibleMimeTypes) || $mime == "application/vnd.oasis.opendocument.text" ) {
                $preview = $this->Document->generatePreview($documentId, $documentPath);
				$previewType = 'flexpaper';
				$tmpPreviewFileBasename = $document['Document']['name'];
				$tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
				//purge des fichiers de preview précédents
				$this->Linkeddocs->purge();
				file_put_contents($tmpPreviewFile, $preview);
            }
            else {
            	// image
				if( strpos( $document['Document']['mime'], 'image' ) !== false ) {
					$preview = file_get_contents( $documentPath );
					$previewType = 'image';
					$tmpPreviewFileBasename = $document['Document']['name'];
					$tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
					$this->Linkeddocs->purge();
					file_put_contents($tmpPreviewFile, $preview);
				}
            }
        }
        else {
			if (in_array($mime, $this->Document->convertibleMimeTypes) || $mime == "application/pdf" || $mime == "PDF" /* || $mime == "application/vnd.oasis.opendocument.text" */) {
				$previewType = 'flexpaper';
				$tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview.pdf";
				$tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
				//purge des fichiers de preview précédents
				$this->Linkeddocs->purge();
				file_put_contents($tmpPreviewFile, $preview);
			} else {
				foreach ($this->Document->otherMimeTypes as $type => $mimes) {

					if (in_array($mime, array_keys($mimes))) {
						$previewType = $type;
						$tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview." . $mimes[$mime];
						$tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
						//purge des fichiers de preview précédents
						$this->Linkeddocs->purge();
						file_put_contents($tmpPreviewFile, $document['Document']['content']);
					}
				}
			}
		}
//debug($tmpPreviewFile);
//die();
        if (!empty($tmpPreviewFile)) {
            chmod($tmpPreviewFile, 0777);
        }

        $this->set('previewType', $previewType);
        $this->set('mime', $mime);
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
        $this->set('path', $tmpPreviewFile);
        if (isset($fullEcran)) {
            $this->set('fullEcran', $fullEcran);
        }
    }

    public function getFileScannePreview($fileName, $relements = null) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $connName = $this->Session->read('Auth.User.connName');

        $oldFileScanne = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . 'fileScannePreview*.*';
        array_map('unlink', glob($oldFileScanne));

        $coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $connName)));
        if (isset($relements) && $relements == 'relements') {
            if (copy(APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'webdav' . DS . $conn . DS . 'modeles' . DS . $fileName, APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . 'fileScannePreview_' . $fileName)) {
                $this->set('fileName', 'fileScannePreview_' . $fileName);
            }
        } else {
            if (copy($coll['Collectivite']['scan_local_path'] . '/' . $fileName, APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . 'fileScannePreview_' . $fileName)) {
                $this->set('fileName', 'fileScannePreview_' . $fileName);
//            $this->set('filePath', APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS .$fileName);
            }
        }
    }

    public function getFileListPreview($documentId) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn );
        $document = $this->Document->find('first', array('conditions' => array('Document.id' => $documentId), 'recursive' => -1));
        if (empty($document)) {
            throw new NotFoundException();
        }
        $preview = __d('document', 'Document.preview.error');

        $fluxId = $document['Document']['courrier_id'];
        $documentName = $document['Document']['name'];
        $mime = $document['Document']['mime'];
        $documentPath = $document['Document']['path'];
        if( !empty( $documentPath ) ) {
            $preview = file_get_contents( $documentPath );
            if (in_array($mime, $this->Document->convertibleMimeTypes) || $mime == "application/vnd.oasis.opendocument.text" ) {
                $preview = $this->Document->generatePreview($documentId, $documentPath);
            }
        }
        else {
            if (!empty($fluxId) && is_dir(WEBDAV_DIR . DS . $conn . DS .$fluxId)) {
                $dirFileGenerated = @scandir(WEBDAV_DIR . DS . $conn . DS .$fluxId);
                if (in_array($documentName, $dirFileGenerated)) {
                    $pathToDocToRegenerate = WEBDAV_DIR . DS . $conn . DS .$fluxId . DS . $documentName;
                    chmod($pathToDocToRegenerate, 0777);
                    if (!empty($pathToDocToRegenerate)) {
                        $preview = $this->Document->generatePreview($documentId, $pathToDocToRegenerate);
                        $document['Document']['preview'] = $preview;
                    }
                }
            }

            if (empty($document['Document']['preview'])) {
                $preview = $this->Document->generatePreview($documentId);
            } else {
                $preview = $document['Document']['preview'];
            }
        }
//$this->log($preview);
//die();
        $tmpPreviewFile = '';
        $tmpPreviewFileBasename = '';
        $previewType = '';

        if (in_array($mime, $this->Document->convertibleMimeTypes) || $mime == "application/pdf" || $mime == "PDF" /* || $mime == "application/vnd.oasis.opendocument.text" */) {
            $previewType = 'flexpaper';
            $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview.pdf";
            $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
            //purge des fichiers de preview précédents
            $this->Linkeddocs->purge();
            file_put_contents($tmpPreviewFile, $preview);
        } else {
            foreach ($this->Document->otherMimeTypes as $type => $mimes) {

                if (in_array($mime, array_keys($mimes))) {
                    $previewType = $type;
                    $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview." . $mimes[$mime];
                    $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
                    //purge des fichiers de preview précédents
                    $this->Linkeddocs->purge();
                    if( !empty( $documentPath ) ) {
                        file_put_contents($tmpPreviewFile, $preview);
                    }
                    else {
                        file_put_contents($tmpPreviewFile, $document['Document']['content']);
                    }
                }
            }
        }
//debug($tmpPreviewFile);
//die();
        if (!empty($tmpPreviewFile)) {
            chmod($tmpPreviewFile, 0777);
        }
        $this->set('fileName', $tmpPreviewFileBasename);
        $this->set('previewType', $previewType);
        $mime = explode(".", $tmpPreviewFileBasename);
        $mime = @$mime[1];
        $this->set('mime', $mime);
    }

    public function getFichierScane() {
        $fileArray[] = NULL;
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $connName = $this->Session->read('Auth.User.connName');
        $coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $connName)));
        if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
            $repo = $coll['Collectivite']['scan_local_path'];
        }

        if (!empty($repo)) {
            $repoSingle = $repo . DS . 'single';
            $repoMerge = $repo . DS . 'merge';
            $isdirMerge = is_dir($repoMerge);
            $isdirSingle = is_dir($repoSingle);

            $setRepo = true;
            $fileArray = array();
            if (is_dir($repo)) {
                if (false != ($handle = opendir($repo))) {
                    while (false !== ($file = readdir($handle))) {
                        if ($file != "." && $file != ".." && strpos($file, ".")) {
                            if (pathinfo($file, PATHINFO_EXTENSION) == 'pdf') {
                                $fileArray[] = $repo . DS . $file;
                            }
                        }
                    }
                    closedir($handle);
                }
            }
        } else {
            $setRepo = false;
        }
        $this->set(compact('setRepo'));
        $this->set(compact('repo'));
        $this->set(compact('isdirSingle'));
        $this->set(compact('isdirMerge'));
        $this->set('pathFichiers', $fileArray);
    }

    public function mergeFile() {
        $fichierScanes = $this->request->data['fichierScanes'];
        $fichierScanes = array_values(array_filter($fichierScanes));
        $this->set('fichierScanes', $fichierScanes);
    }

    public function sousRepertoireMerge() {
        $this->autoRender = false;
        $data = $this->request->data;
        $listFiles = explode(",", $this->request->data['listFiles']);
        $nomFLux = $this->request->data['nouveau_flux']['nomFLux'];
        $main_doc = $this->request->data['nouveau_flux']['main_doc'];
        $folder = new Folder();
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $connName = $this->Session->read('Auth.User.connName');
        $coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $connName)));
        if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
            $repo = $coll['Collectivite']['scan_local_path'];
        }

        $statutDeplacement = array();
        $repertoire = $repo . DS . 'merge' . DS . $nomFLux;
        if ($folder->create($repertoire)) {
            $i = 1;
            foreach ($listFiles as $file) {
                $extension = pathinfo($file, PATHINFO_EXTENSION);
                $tmpFile = new File($file);
                if ($file != $main_doc) {
                    $statutDeplacement[] = $tmpFile->copy($repo . DS . 'merge' . DS . $nomFLux . DS . 'fichier_' . $nomFLux . '_' . $i . '.' . $extension);
                    $i++;
                } else {
                    $statutDeplacement[] = $tmpFile->copy($repo . DS . 'merge' . DS . $nomFLux . DS . 'fichier_' . $nomFLux . '_mainDoc.' . $extension);
                }
            }
        }
        if (!in_array(false, $statutDeplacement)) {
            $doossierSousMerge = new Folder($repertoire);
            $arrayFile = $doossierSousMerge->find('.*\.pdf', true);
            $role = $this->Session->read('Auth.User.Env.secondary');
            $role[$this->Session->read('Auth.User.desktop_id')] = $this->Session->read('Auth.User.Env.main');
            $desktop_id = array_search('disp', $role);
            $this->_create($arrayFile, $desktop_id, $repo, $repertoire, $nomFLux);
            foreach ($listFiles as $file) {
                $tmpFile = new File($file);
                $tmpFile->delete();
            }
        }
        $this->_purge(true, $repo);
    }

    public function nouveauFlux() {
        $this->autoRender = false;
//        debug($this->request->data);
        $fichierScanes = $this->request->data['fichierScanes'];
        $fichierScanes = array_values(array_filter($fichierScanes));
        $folder = new Folder();
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $connName = $this->Session->read('Auth.User.connName');
        $coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $connName)));
        if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
            $repo = $coll['Collectivite']['scan_local_path'];
        }
        $statutDeplacement = array();
        foreach ($fichierScanes as $file) {
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            $tmpFile = new File($file);
            $statutDeplacement[] = $tmpFile->copy($repo . DS . 'single' . DS . $tmpFile->name);
        }
        if (!in_array(false, $statutDeplacement)) {
            $single = $repo . DS . 'single';
            $doossierSousMerge = new Folder($single);
            $arrayFile = $doossierSousMerge->find('.*\.pdf', true);
            $role = $this->Session->read('Auth.User.Env.secondary');
            $role[$this->Session->read('Auth.User.desktop_id')] = $this->Session->read('Auth.User.Env.main');
            $desktop_id = array_search('disp', $role);
            $repertoire = $repo . DS . 'single';

            foreach ($arrayFile as $file) {
				$pos_point = strpos($file, '.');
				$filename = substr($file, 0, $pos_point);
				$this->_create($file, $desktop_id, $repo, $repertoire, $filename);
            }
            foreach ($fichierScanes as $file) {
                $tmpFile = new File($file);
                $tmpFile->delete();
            }
        }

        $this->_purge(true, $repo);
    }

    /**
     *
     * @param type $file
     * @return type
     */
    private function _create($file, $desktop_id, $repo, $repertoire, $fluxName = null) {

		$conn = $this->Session->read('Auth.User.connName');
        if ($fluxName != null) {
			if( !is_array($file) ) {
				$remoteReturn = $this->Document->Courrier->remoteCreate(file_get_contents($repertoire . DS . $file), '', true, $desktop_id, $file, $conn, $fluxName);
			}
			else {
				$arrayFileContents = array();
				$i = 1;
				foreach ($file as $fichier) {
					$nomExplodeFichier = explode("_", $fichier);
					if ($nomExplodeFichier[2] == 'mainDoc.pdf') {
						$arrayFileContents[0] = file_get_contents($repertoire . DS . $fichier);
					} else {
						$arrayFileContents[$i] = file_get_contents($repertoire . DS . $fichier);
					}
					$i++;
				}
				$remoteReturn = $this->Document->Courrier->remoteCreate($arrayFileContents, '', true, $desktop_id, $file, $conn, $fluxName);
			}
        } else {
            $remoteReturn = $this->Document->Courrier->remoteCreate(file_get_contents($repertoire . DS . $file), '', true, $desktop_id, $conn, $file);
        }


        $return = false;
        if ($remoteReturn['CodeRetour'] == 'CREATEDFILEOK') {
            if ($fluxName != null) {
				$isMerge = true;
				if(strpos($repertoire, 'single') !== false ) {
					$isMerge = false;
				}
				$return = $this->_setAsOld($fluxName, $repo, false, $isMerge, $repertoire);
            } else {
                $return = $this->_setAsOld($file, $repo);
            }
        } else {
            $bancontenu = $this->Courrier->Bancontenu->find('first', array('conditions' => array('Bancontenu.courrier_id' => $remoteReturn['CourrierId'], 'Bancontenu.desktop_id' => $desktop_id)));
            if (!empty($bancontenu)) {
                $this->Document->Courrier->Bancontenu->delete($bancontenu['Bancontenu']['id']);
                $this->Document->Courrier->delete($remoteReturn['CourrierId']);
            }
        }

        return $return;
    }

    /**
     *
     * @param type $file
     * @param type $invalid
     * @return type
     */
    private function _setAsOld($file, $repo, $invalid = false, $merge = false, $repertoire = null, $removeFromImport = false) {
        $return = false;
        if (!$removeFromImport) {
            if ($merge == true) {
                $repertoireFolder = new Folder($repertoire);
                $fichiers = $repertoireFolder->find('.*\.pdf', true);
                foreach ($fichiers as $fichier) {
                    $tmpFile = new File($repertoireFolder->path . DS . $fichier);
                    //deplacer et supprimer les fichiers
                    if ($tmpFile->copy($repo . DS . 'old' . DS . $fichier . '.webgfcold_' . date('Ymd'))) {
                        $return = $tmpFile->delete();
                    }
                }
                //supprimer répertoire
                $return = $repertoireFolder->delete();
            } else {
                $tmpFile = new File($repo . DS . 'single' . DS . $file.'.pdf');
                if ($tmpFile->copy($repo . DS . 'old' . DS . $file . '.webgfcold_' . date('Ymd'))) {
                    $return = $tmpFile->delete();
                }
            }
        } else {
            $tmpFile = new File($repo . DS . $file);
            if ($tmpFile->copy($repo . DS . 'old' . DS . 'invalid' . DS . $file . '.webgfcold_' . date('Ymd'))) {
                $return = $tmpFile->delete();
            }
        }
        return $return;
    }

    /**
     *
     * @param type $withInvalid
     * @return type
     */
    private function _purge($withInvalid = false, $repo) {
        $return = array();
        $purgeableFiles = $this->_getOldFiles(true, $repo);
        foreach ($purgeableFiles as $file) {
            $tmpFile = new File($repo . DS . 'old' . DS . $file);
            $return[$tmpFile->name] = $tmpFile->delete();
        }
        if ($withInvalid) {
            foreach ($purgeableFiles as $file) {
                $tmpFile = new File($repo . DS . 'old' . DS . $file);
                $return[$tmpFile->name] = $tmpFile->delete();
            }
        }
        return $return;
    }

    /**
     *
     * @param type $purgeable
     * @return type
     */
    private function _getOldFiles($purgeable = false, $repo) {
        $return = array();
        $repositoryOld = $repo . DS . 'old';
        $repositoryOldFolder = new Folder($repositoryOld);
        $purgeDate = CakeTime::format('Ymd', '-1 week');
        $files = $repositoryOldFolder->find('.*', true);
        foreach ($files as $file) {
            $matches = array();
            preg_match('#.*\.webgfcold_([0-9]*)#', $file, $matches);
            if (count($matches) > 1) {
                if ($purgeable) {
                    if (intval($matches[1]) <= intval($purgeDate)) {
                        $return[] = $file;
                    }
                } else {
                    if (intval($matches[1]) > intval($purgeDate)) {
                        $return[] = $file;
                    }
                }
            }
        }

        return $return;
    }

    public function getScanFilePreview($document, $fullEcran = null) {

        $preview = __d('document', 'Document.preview.error');

        file_get_contents($document);


		$mime = $document['Document']['mime'];
//var_dump($preview);
//die();
        $tmpPreviewFile = '';
        $tmpPreviewFileBasename = '';
        $previewType = '';

        if (in_array($mime, $this->Document->convertibleMimeTypes) || $mime == "application/pdf" || $mime == "PDF" /* || $mime == "application/vnd.oasis.opendocument.text" */) {
            $previewType = 'flexpaper';
            $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview.swf";
            $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
            //purge des fichiers de preview précédents
            $this->Linkeddocs->purge();
            file_put_contents($tmpPreviewFile, $preview);
        } else {
            foreach ($this->Document->otherMimeTypes as $type => $mimes) {
                if (in_array($mime, array_keys($mimes))) {
                    $previewType = $type;
                    $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($document['Document']['name']) . mktime(true) . "_preview." . $mimes[$mime];
                    $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
                    //purge des fichiers de preview précédents
                    $this->Linkeddocs->purge();
                    file_put_contents($tmpPreviewFile, $preview);
                }
            }
        }
//debug($tmpPreviewFile);
//die();
        if (!empty($tmpPreviewFile)) {
            chmod($tmpPreviewFile, 0777);
        }

        $this->set('previewType', $previewType);
        $this->set('mime', $mime);
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
        $this->set('path', $tmpPreviewFile);
        if (isset($fullEcran)) {
            $this->set('fullEcran', $fullEcran);
        }
    }

    public function getFusionPreview($document, $fullEcran = true) {

//        $this->autoRender = false;
        $preview = __d('document', 'Document.preview.error');
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $connName = $this->Session->read('Auth.User.connName');
        $coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $connName)));
        if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
            $repo = $coll['Collectivite']['scan_local_path'];
        }

//debug($repo);
        $tmpPreviewFile = $repo . DS . $document;
        $pdfContent = file_get_contents($tmpPreviewFile);

        $date_suffix = date('Ymd_His');
        $tmp_pdf_to_convert = TMP . "pdf_to_convert_" . $date_suffix;
        $tmp_pdf_converted = TMP . "pdf_converted_" . $date_suffix;

        file_put_contents($tmp_pdf_to_convert, $pdfContent);
        $cmd = "pdf2swf -i " . $tmp_pdf_to_convert . " -o " . $tmp_pdf_converted . " -f -T 9 -t -s storeallcharacters";
        $sys_result = exec($cmd, $output, $return_var);
        if ($return_var == 0) {
            $preview = file_get_contents($tmp_pdf_converted);
        }
        unlink($tmp_pdf_converted);
        unlink($tmp_pdf_to_convert);




        $tmpPreviewFileBasename = $document . mktime(true) . "_preview.swf";
        $viewPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
        //purge des fichiers de preview précédents
        $this->Linkeddocs->purge();
        file_put_contents($viewPreviewFile, $preview);

        if (!empty($viewPreviewFile)) {
            chmod($viewPreviewFile, 0777);
        }

        $this->set('document', $document);
        $fullEcran = true;
        $this->set('fullEcran', $fullEcran);
        $this->set('viewPreviewFile', $tmpPreviewFileBasename);
    }

    /**
     * Foction permettant de supprimer un flux scanné par "erreur".
     * @info Les flux ne sont aps supprimés, ils ont déplacés du dossier import vers import/old/invalid
     *
     *
     */
    public function removeFromImport($document) {
        $this->Jsonmsg->init();
        $this->autoRender = false;
        $this->autoLayout = false;
//        $success = false;
        $this->Collectivite = ClassRegistry::init('Collectivite');
        $this->Collectivite->useDbConfig = 'default';
        $connName = $this->Session->read('Auth.User.connName');
        $coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $connName)));
        if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
            $repo = $coll['Collectivite']['scan_local_path'];
        }
        $success = $this->_setAsOld($document, $repo, true, false, null, true);
        if ($success) {
            $this->Jsonmsg->valid();
        }
        $this->Jsonmsg->send();
//        return $success;
    }

    /**
     * Conversion de l'AR en PDF
     * @param type $fluxId
     */
    public function convertToPdf($documentId) {
		$conn = $this->Session->read('Auth.Collectivite.conn');
		$this->set('conn', $conn);
        $this->autoRender = false;
        $document = $this->Document->find(
                'first', array(
            'conditions' => array(
                'Document.id' => $documentId
            ),
            'contain' => false, 'recursive' => -1
                )
        );

        if (!empty($document)) {
            $documentToPdf['Document']['name'] = $document['Document']['name'];
            $documentToPdf['Document']['content'] = $document['Document']['content'];
            $documentToPdf['Document']['mime'] = 'application/pdf';
            $documentToPdf['Document']['format'] = 'pdf';
            $documentToPdf['Document']['size'] = $document['Document']['size'];
            $documentToPdf['Document']['ext'] = 'pdf';
            $documentToPdf['Document']['courrier_id'] = $document['Document']['courrier_id'];

            $file = $document['Document']['name'];
            $documentGenerated = file_get_contents(WEBDAV_DIR . DS . $conn . DS .$document['Document']['courrier_id'] . DS . $file);


            if (!empty($documentGenerated) && isset($documentGenerated) && $documentGenerated !== false) {
                $document_content = $this->Conversion->convertirFlux($documentGenerated, 'odt', 'pdf');
            } else {
                $document_content = $this->Conversion->convertirFlux($document['Document']['content'], 'odt', 'pdf');
            }

            if(strpos($document['Document']['name'], 'odt') !== false ) {
                $document_name = str_replace('odt', 'pdf', $document['Document']['name']);
            }
            else {
                $document_name = $document['Document']['name'].'.pdf';
            }
            Configure::write('debug', 0);
            header("Content-type: application/pdf");
            header("Content-Disposition: attachment; filename=\"" . $document_name . "\""); // use 'attachment' to force a file download

            print $document_content;
            exit();
//            }
        } else {
            return false;
        }
    }

    /**
     * Gestion du document Bordereau pour le CD 81
     *
     * @logical-group Documents
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {

    }

    /**
     * Ajout d'un document Bordereau
     *
     * @logical-group Documents
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (empty($this->request->data)) {
            $options = array();
            $warnings = array();

            $this->set('warnings', $warnings);
            $this->set('options', $options);
        } else {

            if ($this->request->is('post')) {
                $upload = $this->_uploadStatus['NO_UPLOAD'];
                $document = $this->request->data;
//debug($document);
                //traitement de l upload des fichiers
                if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                    $upload = $this->_uploadStatus['UPLOAD_START'];
                    $ext = '';
                    if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                        $ext = $matches[1];
                    }
//debug($ext);
                    if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed[$this->request->data['Document']['format']]))) {
                        $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                    } else {
                        if ($ext != $this->_allowed[$this->request->data['Document']['format']][$_FILES['myfile']['type']]) {
                            $message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            $upload = $this->_uploadStatus['UPLOAD_END'];
                            $document['Document']['size'] = $_FILES['myfile']['size'];
                            $document['Document']['ext'] = $ext;
                            $document['Document']['mime'] = $_FILES['myfile']['type'];
                        }
                    }
                }

                //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                $create = false;
                if ($upload != $this->_uploadStatus['UPLOAD_START']) {

                    $this->Document->create($document);
                    $documentsaved = $this->Document->save();

                    if (!empty($documentsaved)) {
                        $create = true;
                        $message = __d('default', 'save.ok');
                    }

                    //suppression du fichier si un fichier est uploadé
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        unlink($_FILES['myfile']['tmp_name']);
                    }
                }
                $this->set('create', $create);
                $this->set('message', $message);
            }
        }
    }

    /**
     * Edition d'un document Bordereau
     *
     * @logical-group Documents
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        $message = '';
         if (empty($this->request->data)) {
            $options = array();
            $warnings = array();

            $this->set('warnings', $warnings);
            $this->set('options', $options);


            $document = $this->Document->find(
                'first',
                array(
                    'conditions' => array(
                        'Document.id' => $id
                    ),
                    'contain' => false
                )
            );

            if (empty($document)) {
                throw new NotFoundException();
            }
        } else {

            $this->Jsonmsg->init();
            if ($this->request->is('post')) {
                $upload = $this->_uploadStatus['NO_UPLOAD'];
                $document = $this->request->data;
                //traitement de l upload des fichiers
                if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                    $upload = $this->_uploadStatus['UPLOAD_START'];
                    $ext = '';
                    if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                        $ext = $matches[1];
                    }
                    if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed[$this->request->data['Document']['format']]))) {
                        $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                    } else {
                        if ($ext != $this->_allowed[$this->request->data['Document']['format']][$_FILES['myfile']['type']]) {
                            $message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            $upload = $this->_uploadStatus['UPLOAD_END'];
                            $document['Document']['size'] = $_FILES['myfile']['size'];
                            $document['Document']['ext'] = $ext;
                            $document['Document']['mime'] = $_FILES['myfile']['type'];
                        }
                    }
                }
                //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                $create = false;
                if ($upload != $this->_uploadStatus['UPLOAD_START']) {

                    $this->Document->create($document);
                    $documentsaved = $this->Document->save();

                    if (!empty($documentsaved)) {
                        $create = true;
                        $message = __d('default', 'save.ok');
                        $this->Jsonmsg->valid($message);
                        $this->Session->setFlash('Les informations ont été enregistrées', 'growl', array('type' => 'info'));
                        $this->Jsonmsg->send();
                    }

                    //suppression du fichier si un fichier est uploadé
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        unlink($_FILES['myfile']['tmp_name']);
                    }
                }
                $this->set('create', $create);
                $this->set('message', $message);
                $this->Jsonmsg->send();
//$this->log($message);
            }
        }
        $this->set('id', $id);
        $this->set('document', $document);
    }
     /**
     * Récupération de la liste des documents
     *
     * @logical-group Documents
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getDocuments() {
        $documents_tmp = $this->Document->find(
            "all",
            array(
                'conditions' => array(
                    'Document.name' => 'Bordereau historique.odt'
                ),
                'order' => 'Document.name',
                'contain' => false
            )
        );
        $documents = array();
        foreach ($documents_tmp as $i => $item) {
            $item['right_read'] = true;
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $documents[] = $item;
        }
        $this->set(compact('documents'));
        $message = '';
        $this->set('message', $message);
    }


    /**
     * @version 4.3
     * @access public
     */
    public function getProgress($key)
    {
        if ($this->request->is('ajax')) {
            $this->autoRender = false;

            $this->response->type('json', 'text/x-json');
            $this->RequestHandler->respondAs('json');
            $tokens = $this->Session->read('Progress.Tokens');
            $json = array();
            if (!empty($tokens) && array_key_exists($key, $tokens)) {
                $json = array($key => $tokens[$key]);
            }else{
                $json = array($key => array('status'=> ProgressComponent::STATUS_FAILED,
                                            'debug' => var_export($tokens, true),
                                            'error'=> array(
                                                'code' => 404,
                                                'message' => 'Ressource non trouvée'),
                    ));
            }

            $this->autoRender = false;
            $this->response->type('json', 'text/x-json');
            $this->RequestHandler->respondAs('json');
            $this->response->body(json_encode($json));
            return $this->response;

        }
    }

    public function getProgressDocument($cookieTokenKey)
    {
        $this->Progress->clear($cookieTokenKey);
        $tokens = $this->Session->read('Progress.Tokens');

        if(empty($cookieTokenKey)){
            CakeLog::error(__('Impossible de recupérer le fichier, le "cookieTokenKey" est vide'));
            $this->response->statusCode(404);
            return $this->response;
        }

        // fusion du document
        $folderTmp = new Folder(TMP . 'files' . DS . 'progress' . DS . $cookieTokenKey);
        $file_generation = new File($folderTmp->pwd() . DS . $tokens[$cookieTokenKey]['filename']);
        $file_generation->close();

        $this->response->disableCache();
        $this->response->body($file_generation->read());
        $this->response->type($tokens[$cookieTokenKey]['typemime']);
        $this->response->download($tokens[$cookieTokenKey]['filename']);

        //Suppresssion du dossier temporaire
        if(!$folderTmp->delete()){
            CakeLog::error(__('Impossible de supprimer le répertoire du "Token" de génération :')."\n".var_export($folderTmp->messages(), true));
        }

        return $this->response;
    }

	/**
	 * Définition du document à signer dans le iParapheur
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant du document
	 * @return void
	 */
	public function setASigner($id, $designer = null) {
		$this->Jsonmsg->init();
		$this->Document->begin();

		$this->Document->id = $id;
		$valid = $this->Document->saveField('asigner', true);
		if( !empty($designer) ) {
			$valid = $this->Document->saveField('asigner', false);
		}

		if ($valid) {
			$this->Jsonmsg->valid();
			$this->Document->commit();
		} else {
			$this->Document->rollback();
		}
		$this->Jsonmsg->send();
	}
}

?>
