<?php

/**
 * Modèles de gabarit pour les réponses
 *
 * Rmodelsgabarits controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

class RmodelgabaritsController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Rmodelgabarits';

    /**
     * Controller components
     *
     * @var array
     * @access public
     */
    public $components = array('Linkeddocs');

    /**
     * Etat d'envoi des fichiers
     *
     * @access private
     * @var array
     */
    private $_uploadStatus = array(
        'NO_UPLOAD' => 0,
        'UPLOAD_START' => 1,
        'UPLOAD_END' => 2
    );

    /**
     * Types MIME et extensions autorisés
     *
     * @access private
     * @var array
     */
    private $_allowed = array(
        'application/vnd.oasis.opendocument.text' => 'odt',
        'application/vnd.oasis.opendocument.text-template' => 'ott'
    );

    /**
     * Suppression d'un modèle de réponse
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du modèle de réponse
     * @return void
     */
    public function delete($id = null, $soustypeId = null, $filename = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
		$conn = $this->Session->read('Auth.Collectivite.conn');

        $fileToDelete = WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $soustypeId . DS . $filename;
        if (file_exists($fileToDelete)) {
            if ($this->Rmodelgabarit->delete($id)) {
                unlink($fileToDelete);
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            }
        }
        $this->Jsonmsg->send();
    }

    /**
     * Téléchargement d'un modèle de réponse
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $id identifiant du modèle de réponse
     * @throws NotFoundException
     * @return void
     */
    public function download($id) {
        $rmodelgabarit = $this->Rmodelgabarit->find('first', array('conditions' => array('Rmodelgabarit.id' => $id), 'recursive' => -1));
        if (empty($rmodelgabarit)) {
            throw new NotFoundException();
        }
        $this->autoRender = false;
        Configure::write('debug', 0);
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"" . Inflector::slug($rmodelgabarit['Rmodelgabarit']["name"]) . '.' . $this->_allowed[$rmodelgabarit['Rmodelgabarit']['mime']] . "\"");
        header("Content-length: " . $rmodelgabarit['Rmodelgabarit']['size']);
        print $rmodelgabarit['Rmodelgabarit']['content'];
        exit();
    }

    /**
     * Récupération de la pré-visualisation (miniature) d'un modèle de réponse (génération si nécessaire)
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $rmodelgabaritId identifiant du modèle de réponse
     * @throws NotFoundException
     * @return void
     */
    public function getPreview($rmodelgabaritId) {
        $rmodelgabarit = $this->Rmodelgabarit->find('first', array('conditions' => array('Rmodelgabarit.id' => $rmodelgabaritId), 'recursive' => -1));
        if (empty($rmodelgabarit)) {
            throw new NotFoundException();
        }
        $preview = __d('document', 'Document.preview.error');
        if (empty($rmodelgabarit['Rmodelgabarit']['preview'])) {
            $preview = $this->Rmodelgabarit->generatePreview($rmodelgabaritId);
        } else {
            $preview = $rmodelgabarit['Rmodelgabarit']['preview'];
        }
        $tmpPreviewFile = '';
        $tmpPreviewFileBasename = '';
        $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($rmodelgabarit['Rmodelgabarit']['name']) . "_preview.swf";
        $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
        //purge des fichiers de preview précédents
        $this->Linkeddocs->purge();
        file_put_contents($tmpPreviewFile, $preview);
        $this->set('preview', $preview);
        $this->set('tmpPreviewFile', $tmpPreviewFile);
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
    }

    /**
     * Ajout d'un gabarit pour le soustype défini
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles de réponses
     * @user-profile User
     *
     * @access public
     * @param integer $soustype_id identifiant du sous-type
     * @return void
     */
    /* public function add($soustype_id = null) {
      if (empty($this->request->data)) {
      $this->set('soustype_id', $soustype_id);
      } else {
      if ($this->request->is('post')) {
      //debug($this->request->data);
      //debug($_FILES);
      $upload = $this->_uploadStatus['NO_UPLOAD'];
      $rmodelgabarit = $this->request->data;
      //traitement de l upload des fichiers
      if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
      $upload = $this->_uploadStatus['UPLOAD_START'];
      $ext = '';
      if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
      $ext = $matches[1];
      }
      if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
      $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
      } else {
      if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
      //							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
      } else {
      $upload = $this->_uploadStatus['UPLOAD_END'];
      $rmodelgabarit['Rmodelgabarit']['size'] = $_FILES['myfile']['size'];
      $rmodelgabarit['Rmodelgabarit']['ext'] = $ext;
      $rmodelgabarit['Rmodelgabarit']['mime'] = $_FILES['myfile']['type'];
      }
      }
      }
      //enregistrement des infos en base si pas  d upload ou si l upload est terminé
      $create = false;
      if ($upload != $this->_uploadStatus['UPLOAD_START']) {
      $this->Rmodelgabarit->create($rmodelgabarit);
      $rmodelgabaritsaved = $this->Rmodelgabarit->save();
      if (!empty($rmodelgabaritsaved)) {
      $create = true;
      $message = __d('default', 'save.ok');
      }
      //suppression du fichier si un fichier est uploadé
      if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
      unlink($_FILES['myfile']['tmp_name']);
      }
      }
      $this->set('create', $create);
      $this->set('message', $message);
      }
      }
      } */


    public function add($soustype_id = null) {

		$conn = $this->Session->read('Auth.Collectivite.conn');
        if (empty($this->request->data)) {
            $this->set('soustype_id', $soustype_id);
        } else {
            $upload = $this->_uploadStatus['NO_UPLOAD'];
            $rmodelgabarit = $this->request->data;
            $message = '';

            //traitement de l upload des fichiers
            if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                $upload = $this->_uploadStatus['UPLOAD_START'];
                $ext = '';
                if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                    $ext = $matches[1];
                }

                if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
                    $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                } else {
                    if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
//							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        //FIXME: unifier l analyse des types mime
                    } else {
                        $upload = $this->_uploadStatus['UPLOAD_END'];
                    }
                }
            }

            //enregistrement des infos en base si pas  d upload ou si l upload est terminé
            $create = false;
            if ($upload != $this->_uploadStatus['UPLOAD_START']) {


                //deplacement du fichier si le fichier est uploadé
                if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
					$rmodelgabarit['Rmodelgabarit']['name'] = str_replace( ' ', '_', $rmodelgabarit['Rmodelgabarit']['name'] );
					if( strpos( $rmodelgabarit['Rmodelgabarit']['name'], '.odt') === false ) {
						$rmodelgabarit['Rmodelgabarit']['name'] = $rmodelgabarit['Rmodelgabarit']['name'].'.odt';
					}
                    $sessionFluxIdDir = WEBDAV_DIR . DS . $conn . DS .'modeles' . DS . $this->request->data['Rmodelgabarit']['soustype_id'];
                    $Folder = new Folder($sessionFluxIdDir, true, 0777);
                    if (move_uploaded_file($_FILES['myfile']['tmp_name'], $sessionFluxIdDir . DS . $rmodelgabarit['Rmodelgabarit']['name'])) {

                        $ext = '';
                        if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                            $ext = $matches[1];
                        }
//						$relement['Relement']['courrier_id'] = $this->request->data['fluxId'];
//                        $rmodelgabarit['Rmodelgabarit']['name'] = $_FILES['myfile']['name'];
                        $rmodelgabarit['Rmodelgabarit']['size'] = $_FILES['myfile']['size'];
                        $rmodelgabarit['Rmodelgabarit']['ext'] = $ext;
                        $rmodelgabarit['Rmodelgabarit']['mime'] = $_FILES['myfile']['type'];
                        $rmodelgabarit['Rmodelgabarit']['path'] = $sessionFluxIdDir;

                        $this->Rmodelgabarit->create($rmodelgabarit);
                        $rmodelgabaritsaved = $this->Rmodelgabarit->save();

                        if (!empty($rmodelgabaritsaved)) {
                            $create = true;
                            $message = __d('default', 'save.ok');
                        }
                    }
                }
            }
            $this->set('create', $create);
            $this->set('message', $message);
        }
    }

}
