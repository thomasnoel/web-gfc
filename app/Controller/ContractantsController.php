<?php

/**
 * Contractants
 *
 * Contractants controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ContractantsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Contractants';
    public $uses = array('Contractant');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Contractants', true)
        ));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Contractants
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $contractant = $this->request->data;


            $this->Contractant->begin();
            $this->Contractant->create($contractant);

            if ($this->Contractant->save()) {
                $json['success'] = true;
            }
            if ($json['success']) {
                $this->Contractant->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Contractant->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Contractants
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Contractant->create();
            $contractant = $this->request->data;

            if ($this->Contractant->save($contractant)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Contractant->find(
                    'first', array(
                'conditions' => array('Contractant.id' => $id),
                'contain' => false
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Contractants
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Contractant->begin();
        if ($this->Contractant->delete($id)) {
            $this->Contractant->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Contractant->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Contractants
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getContractants() {
        $contractants_tmp = $this->Contractant->find(
                "all", array(
            'order' => 'Contractant.name'
                )
        );
        $contractants = array();
        foreach ($contractants_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $contractants[] = $item;
        }
        $this->set(compact('contractants'));
    }

}

?>
