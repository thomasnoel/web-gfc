<?php

/**
 * Statistiques
 *
 * Statistiques controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
//App::uses('GoogleCharts', 'GoogleCharts.Lib');
class StatistiquesController extends AppController {

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Statistiques';

    /**
     * Controller uses
     *
     * @var array
     * @access public
     */
    public $uses = array('Statistique', 'Courrier', 'Banadresse');

    /**
     * Controller components
     *
     * @var array
     * @access public
     */
    public $components = array('DataAuthorized', 'Ajaxformvalid');

    /**
     * Controller helpers
     *
     * @var array
     * @access public
     */
    public $helpers = array('Csv', 'Paginator');

    //,'GoogleCharts.GoogleCharts'
    //TODO: enlever le beforeFilter et régénérer les droits
    public function beforeFilter() {
        $this->Auth->allow(array('index', 'indicateursQuant', 'indicateursQual', 'exportqual', 'exportqualattente', 'exportquant', 'exportgeo', 'ajaxformvalid', 'indicateursAgents', 'exportagent')); //FIXME
        parent::beforeFilter();
    }

    protected function _setOptions() {

        $Service = ClassRegistry::init('Service');
//        $poles = $Service->find(
//            'list',
//            array(
//                'conditions' => array(
//                    'Service.parent_id IS NULL'
//                ),
//                'order' => 'Service.name ASC'
//            )
//        );
//        $directions = $Service->find(
//            'list',
//            array(
//                'conditions' => array(
//                    'Service.parent_id IS NOT NULL'
//                ),
//                'order' => 'Service.name ASC'
//            )
//        );
        $services = $Service->find(
                'list', array(
            'order' => 'Service.name ASC'
                )
        );
        $this->set('services', $services);

        $mois = array(
            '01' => 'Jan.',
            '02' => 'Fév.',
            '03' => 'Mars',
            '04' => 'Avr.',
            '05' => 'Mai',
            '06' => 'Juin',
            '07' => 'Juillet',
            '08' => 'Août',
            '09' => 'Sept.',
            '10' => 'Oct.',
            '11' => 'Nov.',
            '12' => 'Déc.'
        );
        $this->set('mois', $mois);

        $typesStats = array(
            'cloture' => 'Taux de réponses closes',
            'sanssuite' => 'Taux de réponses closes en attente d\'une réponse',
            'delai' => 'Taux de réponses closes dans les délais'
        );
//debug($typesStats);
        $this->set('typesStats', $typesStats);

        $Metadonnee = ClassRegistry::init('Metadonnee');
        $metas = $Metadonnee->find(
                'all', array(
//            'conditions' => array('Metadonnee.typemetadonnee_id' => Configure::read('Selectvaluemetadonnee.id')),
            'order' => 'Metadonnee.name ASC',
            'contain' => array(
                'Selectvaluemetadonnee' => array(
                    'order' => 'Selectvaluemetadonnee.name ASC'
                )
            )
                )
        );
//        debug($metas);
        foreach ($metas as $i => $meta) {
            foreach ($meta['Selectvaluemetadonnee'] as $j => $selectvalue) {
                $metas[$i]['ValueInSelect'][$selectvalue['id']] = $selectvalue['name'];
            }
        }

        $this->set('metas', $metas);

        $Origineflux = ClassRegistry::init('Origineflux');
        $origines = $Origineflux->find(
                'list', array(
            'order' => 'Origineflux.name ASC'
                )
        );
        $this->set('origines', $origines);

        $conn = $this->Session->read('Auth.User.connName');
        $this->loadModel('Collectivite');
        $this->set('collectivite', $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn))));
    }

    /**
     * Moteur de recherche pour les indicateurs d'orientation.
     *
     * @return void
     */
    public function indicateursGeo() {
        $this->set('ariane', array(
            '<a href="/environnement/userenv/0">' . __d('menu', 'Environnement') . '</a>',
            __d('menu', 'statistiquesGeo', true)
        ));

        $Ban = ClassRegistry::init('Ban');

        $this->set('bans', $Ban->find(
                        'all', array(
                    'order' => array('Ban.name ASC'),
                    'contain' => array(
                        'Bancommune' => array(
                            'order' => 'Bancommune.name ASC'
                        )
                    )
                        )
                )
        );

//        debug( $this->request->data);

        if (!empty($this->request->data)) {
            $results = $this->Statistique->getIndicateursGeos($this->request->data);
//            if( !empty($this->request->data['Statistique']['adresse'])) {
//                $this->request->data['Statistique']['adresse'] = $this->request->data['Statistique']['adresse'];
//            }
            if (isset($this->request->data['alreadyLoad'])) {
                $this->set('alreadyLoad', true);
            }
            $this->set(compact('results'));

//            debug($results);
            $origineSelected = $this->request->data['Statistique']['origineflux_id'];
            $this->set('origineSelected', $origineSelected);
        }

        $this->_setOptions();
        $this->set('title_for_layout', 'Indicateurs Géographiques');
    }

    /**
     * Moteur de recherche pour les indicateurs de délais.
     *
     * @return void
     */
    public function indicateursQuant() {
        //$this->autoRender = true;
        $this->set('ariane', array(
            '<a href="/environnement/userenv/0">' . __d('menu', 'Environnement') . '</a>',
            __d('menu', 'statistiquesQuant', true)
        ));




        if (!empty($this->request->data)) {
            // Tableau Etat stistique Nb de flux

            $results = $this->Statistique->getIndicateursQuants($this->request->data);
            $this->set('results', $results);
            if (isset($this->request->data['alreadyLoad'])) {
                $this->set('alreadyLoad', true);
            }
            // Tableau Etat stistique Nb de flux liés à des métadonnées
            $typemetaCounts = $this->Statistique->getIndicateursMetas($this->request->data);
            $this->set('typemetaCounts', $typemetaCounts);
            // Tableau taux de réponses
            $resultsPourcents = $this->Statistique->getIndicateursPourcents($this->request->data);
            $this->set('resultsPourcents', $resultsPourcents);

            $origineSelected = $this->request->data['Statistique']['origineflux_id'];
            $this->set('origineSelected', $origineSelected);
        }
        $this->_setOptions();
    }

    /**
     * Moteur de recherche pour les indicateurs de qualité
     *
     * @return void
     */
    public function indicateursQual() {
        $this->set('ariane', array(
            '<a href="/environnement/userenv/0">' . __d('menu', 'Environnement') . '</a>',
            __d('menu', 'statistiquesQual', true)
        ));



        if (!empty($this->request->data)) {
            // Tableau taux de réponses
            $results = $this->Statistique->getIndicateursPourcents($this->request->data);
            if (isset($this->request->data['alreadyLoad'])) {
                $this->set('alreadyLoad', true);
            }
            foreach ($results['Indicateur'] as $key => $result) {
                if (!empty($result['Statistique']['name'])) {
                    $results['service'][$result['Statistique']['name']][] = $result['Statistique'];
                }
            }

            $this->set('results', $results);

            $origineSelected = $this->request->data['Statistique']['origineflux_id'];
            $this->set('origineSelected', $origineSelected);
        }
        $this->_setOptions();
    }

    /**
     * Moteur de recherche pour les indicateurs pour les agents de la collectivité
     *
     * @return void
     */
    public function indicateursAgents() {
        //$this->autoRender = true;
        $this->set('ariane', array(
            '<a href="/environnement/userenv/0">' . __d('menu', 'Environnement') . '</a>',
            __d('menu', 'statistiquesAgents', true)
        ));


        if (!empty($this->request->data)) {
            // Tableau Etat stistique Nb de flux
            $results = $this->Statistique->getIndicateursAgents($this->request->data);
            $this->set('results', $results);
            if (isset($this->request->data['alreadyLoad'])) {
                $this->set('alreadyLoad', true);
            }

            $agentSelected = $this->request->data['Agent']['Agent'];
            $User = ClassRegistry::init('User');
//debug($agentSelected);
            if (empty($agentSelected)) {
                $conditions = array();
            } else {
                $conditions = array('User.id' => $agentSelected);
            }
            $agents = $User->find(
                    'all', array(
                'fields' => array(
                    'User.id',
                    'User.nom',
                    'User.prenom'
                ),
                'conditions' => array(
                    $conditions
                ),
                'contain' => false
                    )
            );
            $agentsNames = array();
            foreach ($agents as $key => $agent) {
                $agentName[$agent['User']['id']] = $agent['User']['nom'] . ' ' . $agent['User']['prenom'];
            }
//            $this->request->data['Agent']['Agent'] = array_keys( $agentName );

            $this->set('agentName', $agentName);
        }
        $User = ClassRegistry::init('User');
        $listagents = array();
        $agents = $User->find(
                'all', array(
            'fields' => array(
                'User.id',
                'User.nom',
                'User.prenom'
            ),
            'conditions' => array(
                'User.active' => true
            ),
            'contain' => false,
            'order' => array('User.nom ASC, User.prenom ASC')
                )
        );
        foreach ($agents as $agent) {
            $listagents[$agent['User']['id']] = $agent['User']['nom'] . ' ' . $agent['User']['prenom'];
        }


//debug(  $this->request->data );
        $agents = $User->find('list');
        $this->set('agents', $agents);
        $this->set('listagents', $listagents);
        $this->_setOptions();
    }

    /**
     * Index method
     *
     * @access public
     * @return void
     */

    /**
     * Index method
     *
     * @access public
     * @return void
     */
    public function index() {

    }

    /**
     * Export de la Statistique au format CSV
     *
     * @logical-group Statistiques
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportqual() {
        ini_set('memory_limit', '3000M');
        $annee = Hash::get($this->request->params['named'], 'Statistique__annee');
        $origine = Hash::get($this->request->params['named'], 'Statistique__origineflux_id');
        if (!empty($origine)) {
            $Origineflux = ClassRegistry::init('Origineflux');
            $origines = $Origineflux->find(
                    'list', array(
                'order' => 'Origineflux.name ASC'
                    )
            );
            $origineName = $origines[$origine];
        }

        $results = $this->Statistique->getIndicateursPourcents(Hash::expand($this->request->params['named'], '__'), null);

        foreach ($results['Indicateur'] as $key => $result) {
            if (!empty($result['Statistique']['name'])) {
                $results['service'][$result['Statistique']['name']][] = $result['Statistique'];
            }
        }
        $this->set('results', $results);

        $this->_setOptions();
        $this->layout = '';
        $this->set(compact('annee', 'origine', 'origineName'));
    }

    /**
     * Export de la Statistique au format CSV
     *
     * @logical-group Statistiques
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportqualattente() {
        $annee = Hash::get($this->request->params['named'], 'Statistique__annee');
        $origine = Hash::get($this->request->params['named'], 'Statistique__origineflux_id');
        if (!empty($origine)) {
            $Origineflux = ClassRegistry::init('Origineflux');
            $origines = $Origineflux->find(
                    'list', array(
                'order' => 'Origineflux.name ASC'
                    )
            );
            $origineName = $origines[$origine];
        }

        $results = $this->Statistique->getIndicateursPourcents(Hash::expand($this->request->params['named'], '__'), null);

        foreach ($results['Indicateur'] as $key => $result) {
            if (!empty($result['Statistique']['name'])) {
                $results['service'][$result['Statistique']['name']][] = $result['Statistique'];
            }
        }
        $this->set('results', $results);

        $this->_setOptions();
        $this->layout = '';
        $this->set(compact('annee', 'origine', 'origineName'));
    }

    /**
     * Export de la Statistique au format CSV
     *
     * @logical-group Statistiques
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportquant() {
        ini_set('memory_limit', '3000M');
        $annee = Hash::get($this->request->params['named'], 'Statistique__annee');
        $origine = Hash::get($this->request->params['named'], 'Statistique__origineflux_id');
        if (!empty($origine)) {
            $Origineflux = ClassRegistry::init('Origineflux');
            $origines = $Origineflux->find(
                    'list', array(
                'order' => 'Origineflux.name ASC'
                    )
            );
            $origineName = $origines[$origine];
        }


        $results = $this->Statistique->getIndicateursQuants(Hash::expand($this->request->params['named'], '__'), null);
        $this->set('results', $results);

        $this->_setOptions();
        $this->layout = '';
        $this->set(compact('annee', 'origine', 'origineName'));
    }

    /**
     * Export de la Statistique au format CSV
     *
     * @logical-group Statistiques
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportquantmeta() {
        $annee = Hash::get($this->request->params['named'], 'Statistique__annee');
        $origine = Hash::get($this->request->params['named'], 'Statistique__origineflux_id');

        if (!empty($origine)) {
            $Origineflux = ClassRegistry::init('Origineflux');
            $origines = $Origineflux->find(
                    'list', array(
                'order' => 'Origineflux.name ASC'
                    )
            );
            $origineName = $origines[$origine];
        }


        // Tableau Etat stistique Nb de flux liés à des métadonnées
        $results = $this->Statistique->getIndicateursMetas(Hash::expand($this->request->params['named'], '__'), null);
        $this->set('results', $results);


        $this->_setOptions();
        $this->layout = '';
        $this->set(compact('annee', 'origine', 'origineName'));
    }

    /**
     * Export de la Statistique au format CSV
     *
     * @logical-group Statistiques
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportgeo() {

        $annee = Hash::get($this->request->params['named'], 'Statistique__annee');
        $origine = Hash::get($this->request->params['named'], 'Statistique__origineflux_id');
        if (!empty($origine)) {
            $Origineflux = ClassRegistry::init('Origineflux');
            $origines = $Origineflux->find(
                    'list', array(
                'order' => 'Origineflux.name ASC'
                    )
            );
            $origineName = $origines[$origine];
        }

        $Ban = ClassRegistry::init('Ban');

        // Tableau Etat stistique Nb de flux liés à des métadonnées
        $communeId = Hash::get($this->request->params['named'], 'Statistique__commune');
        $bancommune = $Ban->Bancommune->find('first', array('fields' => array('Bancommune.name'), 'conditions' => array('Bancommune.id' => $communeId)));
        $nameCommune = $bancommune['Bancommune']['name'];


        $resultatnonvide = Hash::get($this->request->params['named'], 'Statistique__resultatNonvide');
        $results = $this->Statistique->getIndicateursGeos(Hash::expand($this->request->params['named'], '__'), null);
        $this->set('results', $results);

        $this->_setOptions();
        $this->layout = '';
        $this->set(compact('annee', 'nameCommune', 'resultatnonvide', 'origine', 'origineName'));
    }

    /**
     * Fonction en ajax permettant de remonter les adresses suite à la sélection d'une commune
     * @param type $bancommune_id
     * @return type
     */
    public function getAdresses($bancommune_id) {
        $this->autoRender = false;
        $Ban = ClassRegistry::init('Ban');
        $adresses = $Ban->Bancommune->Banadresse->find(
                'all', array(
            'fields' => array(
                'DISTINCT Banadresse.nom_afnor'
            ),
            'conditions' => array(
                'Banadresse.bancommune_id' => $bancommune_id,
                array(
                    'Banadresse.nom_afnor IS NOT NULL',
                    'Banadresse.nom_afnor <>' => '',
                )
            ),
            'recursive' => -1,
            'order' => array('Banadresse.nom_afnor')
                )
        );

        $content = json_encode($adresses);
        header('Pragma: no-cache');
        header('Cache-Control: no-store, no-cache, max-age=0, must-revalidate');
        header('Content-Type: text/x-json');
        header("X-JSON: {$content}");
        Configure::write('debug', 0);
        echo $content;
        return;
    }

    /**
     * Export de la Statistique au format CSV
     *
     * @logical-group Statistiques
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function exportagent() {
        ini_set('memory_limit', '3000M');
        $annee = Hash::get($this->request->params['named'], 'Statistique__annee');
        $results = $this->Statistique->getIndicateursAgents(Hash::expand($this->request->params['named'], '__'), null);
        $this->set('results', $results);

        foreach ($results['IndicateurAgent'] as $agentId => $values) {
            if (!empty($agentId)) {
                $User = ClassRegistry::init('User');
                $agent = $User->find(
                        'first', array(
                    'fields' => array(
                        'User.id',
                        'User.nom',
                        'User.prenom'
                    ),
                    'conditions' => array(
                        'User.id' => $agentId
                    ),
                    'contain' => false
                        )
                );
                $agentName[$agent['User']['id']] = $agent['User']['nom'] . ' ' . $agent['User']['prenom'];
            } else {
                $agentName = array();
                $this->set('agentName', $agentName);
            }
        }
        $this->layout = '';
        $this->set(compact('annee', 'agent', 'agentName'));
    }

}

?>
