<?php

/**
 * Tâches
 *
 * Taches controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class TachesController extends AppController {

	/**
	 * Controller name
	 *
	 * @access public
	 * @var string
	 */
	public $name = 'Taches';

	/**
	 * Controller uses
	 *
	 * @access public
	 * @var type
	 */
	public $uses = array('Tache');

	/**
	 * Ajout d'une tâche
	 *
	 * @logical-group Tâches
	 *
	 * @logical-group Flux
	 * @logical-group Création d'un flux
	 * @user-profile User
	 *
	 * @logical-group Flux
	 * @logical-group Edition d'un flux
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $fluxId identifiant du flux
	 * @throws BadMethodCallException
	 * @return void
	 */
	public function add($fluxId) {
		if (!empty($this->request->data)) {
			$this->Jsonmsg->init();

			$this->request->data['Tache']['creator_desktop_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');

			if( count($this->request->data['Desktop']['Desktop'] ) == 1 ) {
				$this->request->data['Tache']['statut'] = 0;
				$this->request->data['Tache']['act_desktop_id'] = $this->request->data['Desktop']['Desktop'][0];
			}
			$this->Tache->create($this->request->data);
			if ($this->Tache->save()) {
                $this->loadModel('Courrier');

                // on stocke qui fait quoi quand
                $this->loadModel('Courrier');
                $flux = $this->Courrier->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Courrier.id' => $fluxId
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
                $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (ajout d'une tâche) le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                $this->Courrier->Bancontenu->updateAll(
                    array(
                        'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s')."'"
                    ),
                    array(
                        'Bancontenu.courrier_id' => $fluxId,
                        'Bancontenu.desktop_id' => $this->Session->read('Auth.User.Courrier.Desktop.id')
                    )
                );


				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			if (empty($fluxId)) {
				throw new BadMethodCallException();
			}
			$this->request->data = $this->Tache->create();
			$this->request->data['Tache']['courrier_id'] = $fluxId;
//			$soustypeId = $this->Tache->Courrier->field('Courrier.soustype_id', array('Courrier.id' => $fluxId));
//			$desktops_tmp = $this->Tache->Desktop->find('all', array('contain' => array('User', 'SecondaryUser'), 'conditions' => array('Desktop.active' => true)));
//			$desktops = array();
//			foreach ($desktops_tmp as $desktop) {
//				if ((!empty($desktop['User']) || !empty($desktop['SecondaryUser'])) && $desktop['Desktop']['profil_id'] > ADMIN_GID) {
//					if ($this->DataAuthorized->checkSoustype(array('mode' => 'desktop', 'desktopId' => $desktop['Desktop']['id'], 'soustypeId' => $soustypeId))) {
//						$desktops[$desktop['Desktop']['id']] = $desktop['Desktop']['name'];
//					}
//				}
//			}
            $desktops = $this->Tache->Desktop->find('list', array('conditions' => array('Desktop.active' => true), 'order' => array('Desktop.name ASC')));
			$this->set('users', $this->Session->read('Auth.AllDesktops') );

			$this->set('fluxId', $fluxId);
			$this->set('desktops', $desktops);
		}
	}

	/**
	 * Edition d'un tâche
	 *
	 * @logical-group Tâches
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la tâche
	 * @throws BadMethodCallException
	 * @throws NotFoundException
	 * @return void
	 */
	public function edit($id) {
		if (!empty($this->request->data)) {
			$this->Jsonmsg->init();

			$this->request->data['Tache']['creator_desktop_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');

			if( count($this->request->data['Desktop']['Desktop'] ) == 1 ) {
				$this->request->data['Tache']['statut'] = 1;
				$this->request->data['Tache']['act_desktop_id'] = $this->request->data['Desktop']['Desktop'][0];
			}
			$this->Tache->create($this->request->data);
			if ($this->Tache->save()) {
                $tache = $this->Tache->find('first', array('conditions' => array('Tache.id' => $id), 'contain' => false, 'recursive' => '-1'));
                // on stocke qui fait quoi quand
                $this->loadModel('Courrier');
                $flux = $this->Tache->Courrier->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Courrier.id' => $tache['Tache']['courrier_id']
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
                $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (modification d'une tâche) le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                $this->Courrier->Bancontenu->updateAll(
                    array(
                        'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s')."'"
                    ),
                    array(
                        'Bancontenu.courrier_id' => $tache['Tache']['courrier_id'],
                        'Bancontenu.desktop_id' => $this->Session->read('Auth.User.Courrier.Desktop.id')
                    )
                );

				$this->Jsonmsg->valid();
			}
			$this->Jsonmsg->send();
		} else {
			if (empty($id)) {
				throw new BadMethodCallException();
			}
			$this->request->data = $this->Tache->find('first', array('conditions' => array('Tache.id' => $id), 'contain' => array('Desktop')));
			if (empty($this->request->data)) {
				throw new NotFoundException();
			}
//			$soustypeId = $this->Tache->Courrier->field('Courrier.soustype_id', array('Courrier.id' => $this->request->data['Tache']['courrier_id']));
//			$desktops_tmp = $this->Tache->Desktop->find('all', array('contain' => array('User', 'SecondaryUser'), 'conditions' => array('Desktop.active' => true)));
//			$desktops = array();
//			foreach ($desktops_tmp as $desktop) {
//				if ((!empty($desktop['User']) || !empty($desktop['SecondaryUser'])) && $desktop['Desktop']['profil_id'] > ADMIN_GID) {
//					if ($this->DataAuthorized->checkSoustype(array('mode' => 'desktop', 'desktopId' => $desktop['Desktop']['id'], 'soustypeId' => $soustypeId))) {
//						$desktops[$desktop['Desktop']['id']] = $desktop['Desktop']['name'];
//					}
//				}
//			}
            $desktops = $this->Tache->Desktop->find('list', array('conditions' => array('Desktop.active' => true), 'order' => array('Desktop.name ASC')));
			$this->set('desktops', $desktops);
			$this->set('users', $this->Session->read('Auth.AllDesktops') );
		}
	}

	/**
	 * Suppression d'une tâche (delete)
	 *
	 * @logical-group Tâches
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la tâche
	 * @return void
	 */
	public function delete($id) {
		$this->Jsonmsg->init(__d('default', 'delete.error'));
        $tache = $this->Tache->find('first', array('conditions' => array('Tache.id' => $id), 'contain' => false, 'recursive' => '-1'));
        // on stocke qui fait quoi quand
        $this->loadModel('Courrier');
        $flux = $this->Tache->Courrier->find(
            'first',
            array(
                'conditions' => array(
                    'Courrier.id' => $tache['Tache']['courrier_id']
                ),
                'contain' => false,
                'recursive' => -1
            )
        );

		if ($this->Tache->delete($id)) {
            $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (suppression d'une tâche) le ".date('d/m/Y à H:i:s'), LOG_WARNING );
			$this->Jsonmsg->valid(__d('default', 'delete.ok'));
		}
		$this->Jsonmsg->send();
	}

	/**
	 * Validation du traitement d'une tâche dont on a, au préalable, pris en charge le traitement
	 *
	 * @logical-group Tâches
	 *
	 * @logical-group Flux
	 * @logical-group Création d'un flux
	 * @user-profile User
	 *
	 * @logical-group Flux
	 * @logical-group Edition d'un flux
	 * @user-profile User
	 *
	 * @logical-group Environnment
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la tâche
	 * @throws NotFoundException
	 * @return void
	 */
	public function checkTache($id) {
		$tache = $this->Tache->find('first', array('conditions' => array('Tache.id' => $id)));
		if (empty($tache)) {
			throw new NotFoundException();
		}
		$this->Jsonmsg->init();
		$tache['Tache']['act_desktop_id'] = $this->Session->read('Auth.User.Desktop.id');
		$tache['Tache']['statut'] = 2;
		$this->Tache->create($tache);
		if ($this->Tache->save()) {
			$this->Jsonmsg->valid();
		}
		$this->Jsonmsg->send();
	}

	/**
	 * Prise en charge du traitement d'une tâche
	 *
	 * @logical-group Tâches
	 *
	 * @logical-group Flux
	 * @logical-group Création d'un flux
	 * @user-profile User
	 *
	 * @logical-group Flux
	 * @logical-group Edition d'un flux
	 * @user-profile User
	 *
	 * @logical-group Environnment
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la tâche
	 * @throws NotFoundException
	 * @return void
	 */
	public function grabTache($id = null) {
		$tache = $this->Tache->find('first', array('conditions' => array('Tache.id' => $id)));
		if (empty($tache)) {
			throw new NotFoundException();
		}
		$this->Jsonmsg->init();
		$tache['Tache']['act_desktop_id'] = $this->Session->read('Auth.User.Desktop.id');
		$tache['Tache']['statut'] = 1;
		$this->Tache->create($tache);
		if ($this->Tache->save()) {
			$this->Jsonmsg->valid();
		}
		$this->Jsonmsg->send();
	}

	/**
	 * Annulation de la prise en charge du traitement d'un tâche
	 *
	 * @logical-group Tâches
	 *
	 * @logical-group Flux
	 * @logical-group Création d'un flux
	 * @user-profile User
	 *
	 * @logical-group Flux
	 * @logical-group Edition d'un flux
	 * @user-profile User
	 *
	 * @logical-group Environnment
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id identifiant de la tâche
	 * @throws NotFoundException
	 * @return void
	 */
	public function releaseTache($id = null) {
		$tache = $this->Tache->find('first', array('conditions' => array('Tache.id' => $id)));
		if (empty($tache)) {
			throw new NotFoundException();
		}
		$this->Jsonmsg->init();
		$tache['Tache']['act_desktop_id'] = 0;
		$tache['Tache']['statut'] = 0;
		$this->Tache->create($tache);
		if ($this->Tache->save()) {
			$this->Jsonmsg->valid();
		}
		$this->Jsonmsg->send();
	}

	/**
	 * Détermine si le traitement d'une tâche est hors délai
	 *
	 * @access private
	 * @param array $tache tableau représentant la tâche
	 * @return boolean
	 */
	private function _isLate($tache) {
		$retard = false;
		if ($tache['Tache']['statut'] != 2) {
			$tmpTime = preg_split('# #', $tache['Tache']['created']);
			$tmpTime1 = preg_split('#-#', $tmpTime[0]);
			if ($tache['Tache']['delai_unite'] == 0) {
				$endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + $tache['Tache']['delai_nb'], $tmpTime1[0]);
			} else if ($tache['Tache']['delai_unite'] == 1) {
				$endTime = mktime(0, 0, 0, $tmpTime1[1], $tmpTime1[2] + (7 * $tache['Tache']['delai_nb']), $tmpTime1[0]);
			} else if ($tache['Tache']['delai_unite'] == 2) {
				$endTime = mktime(0, 0, 0, $tmpTime1[1] + $tache['Tache']['delai_nb'], $tmpTime1[2], $tmpTime1[0]);
			}
			if ($endTime < mktime()) {
				$retard = true;
			}
		}
		return $retard;
	}

	/**
	 * Récupération de la liste des tâches d'un utilisateur
	 *
	 * @logical-group Tâches
	 * @logical-group Environnement
	 * @user-profile User
	 *
	 * @access public
	 * @return void
	 */
	public function getTaches() {
		$desktops = $this->SessionTools->getDesktopList();
		$waitingTasks = array();
		$grabbedTasks = array();
		foreach ($desktops as $desktopId) {
			$waitingTasks_tmp = $this->Tache->getTachesATraiter($desktopId);
			$waitingTasks_new = array();
			foreach ($waitingTasks_tmp as $tache) {
				$tache['retard'] = $this->_isLate($tache);
				$waitingTasks_new[] = $tache;
			}
			$waitingTasks = array_merge($waitingTasks, $waitingTasks_new);
			$grabbedTasks_tmp = $this->Tache->getTachesEnCoursPerso($desktopId);
			$grabbedTasks_new = array();
			foreach ($grabbedTasks_tmp as $tache) {
				$tache['retard'] = $this->_isLate($tache);
				$grabbedTasks_new[] = $tache;
			}
			$grabbedTasks = array_merge($grabbedTasks, $grabbedTasks_new);
		}
		$taches = array(
			"aTraiter" => $waitingTasks,
			"enCoursPerso" => $grabbedTasks
		);
		$this->set('taches', $taches);

		$delaiTranslate = [
			'0' => 'jour(s)',
			'1' => 'semaine(s)',
			'2' => 'mois'
		];
		$this->set('delaiTranslate', $delaiTranslate);
	}

	/**
	 *
	 */
	public function setFromTask(){
		$this->autoRender = false;
		$this->Session->write('Auth.User.Env.fromTask', true);
	}


}

?>
