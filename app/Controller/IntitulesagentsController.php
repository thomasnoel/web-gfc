<?php

/**
 * Intitulesagents
 *
 * Intitulesagents controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class IntitulesagentsController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Intitulesagents';
    public $uses = array('Intituleagent');

    /**
     * Gestion des emails interface graphique)
     *
     * @logical-group Scanemails
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="/environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'Intitulesagents', true)
        ));
    }

    /**
     * Ajout d'un email
     *
     * @logical-group Intitulesagents
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        $desktopsmanagers = $this->Intituleagent->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => array('Desktopmanager.name ASC')));
        $this->set(compact('desktopsmanagers'));
        if (!empty($this->request->data)) {
            $json = array(
                'message' => __d('default', 'save.error'),
                'success' => false
            );

            $intituleagent = $this->request->data;

            $this->Intituleagent->begin();
            $this->Intituleagent->create($intituleagent);
            if ($this->Intituleagent->save()) {
                if (!empty($intituleagent['Desktopmanager']['Desktopmanager'])) {
                    foreach ($intituleagent['Desktopmanager']['Desktopmanager'] as $desktopmanagerId) {
                        $this->Intituleagent->Desktopmanager->updateAll(
                                array(
                            'Desktopmanager.intituleagent_id' => $this->Intituleagent->id
                                ), array('Desktopmanager.id' => $desktopmanagerId)
                        );
                    }
                }
                $json['success'] = true;
            } else {
                $this->Intituleagent->invalidFields();
                die;
            }
            if ($json['success']) {
                $this->Intituleagent->commit();
                $json['message'] = __d('default', 'save.ok');
            } else {
                $this->Intituleagent->rollback();
            }
            $this->Jsonmsg->sendJsonResponse($json);
        }
    }

    /**
     * Edition d'un type
     *
     * @logical-group Intitulesagents
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {
        $desktopsmanagers = $this->Intituleagent->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.active' => true), 'order' => array('Desktopmanager.name ASC')));
        $this->set(compact('desktopsmanagers'));
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Intituleagent->create();
            $intituleagent = $this->request->data;

            if ($this->Intituleagent->save($intituleagent)) {
                if (!empty($this->request->data['Desktopmanager']['Desktopmanager'])) {
                    // on regarde les anciens enregistrements entre le circut et les sous-types
                    $records = $this->Intituleagent->Desktopmanager->find(
                            'list', array(
                        'fields' => array("Desktopmanager.id", "Desktopmanager.intituleagent_id"),
                        'conditions' => array(
                            "Desktopmanager.intituleagent_id" => $id
                        )
                            )
                    );
                    $oldrecordsids = array_keys($records);
                    // on regarde les nouveautés
                    $nouveauxids = Hash::extract($this->request->data, "Desktopmanager.Desktopmanager.{n}");
                    // si des différences existent on note les ids enlevés, les liens coupés entre circuit et sous-types
                    $idsenmoins = array_diff($oldrecordsids, $nouveauxids);

                    // on met à jour les sous-types qui ne sont plus liés au circuit en cours
                    if (!empty($idsenmoins)) {
                        $success = $this->Intituleagent->Desktopmanager->updateAll(
                                array('Desktopmanager.intituleagent_id' => null), array(
                            'Desktopmanager.intituleagent_id' => $id,
                            'Desktopmanager.id' => $idsenmoins
                                )
                        );
                    }
                    // on sauvegarde les sous-types
                    $save = $this->Intituleagent->Desktopmanager->updateAll(
                            array('Desktopmanager.intituleagent_id' => $id), array('Desktopmanager.id' => $this->request->data['Desktopmanager']['Desktopmanager'])
                    );
                    if ($save) {
                        $this->Intituleagent->Desktopmanager->commit();
                        $this->Jsonmsg->valid();
                    } else {
                        $this->Intituleagent->Desktopmanager->rollback();
                        $this->Jsonmsg->error('Erreur de sauvegarde lors de l\'association du bureau au poste');
                    }
                } else {
                    $records = $this->Intituleagent->Desktopmanager->find(
                            'list', array(
                        'fields' => array("Desktopmanager.id", "Desktopmanager.intituleagent_id"),
                        'conditions' => array(
                            "Desktopmanager.intituleagent_id" => $id,
                            "Desktopmanager.active" => true
                        )
                            )
                    );
                    $oldrecordsids = array_keys($records);
                    // si des différences existent on note les ids enlevés, les liens coupés entre circuit et sous-types
                    $idsenmoins = $oldrecordsids;

                    // on met à jour les intitulés qui ne sont plus liés au bureau
                    if (!empty($idsenmoins)) {
                        $success = $this->Intituleagent->Desktopmanager->updateAll(
                                array('Desktopmanager.intituleagent_id' => null), array(
                            'Desktopmanager.intituleagent_id' => $id,
                            'Desktopmanager.id' => $idsenmoins
                                )
                        );
                    }
                }

                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $this->request->data = $this->Intituleagent->find(
                    'first', array(
                'conditions' => array('Intituleagent.id' => $id),
                'contain' => array(
                    'Desktopmanager'
                )
                    )
            );

            if (empty($this->request->data)) {
                throw new NotFoundException();
            }
        }
    }

    /**
     * Suppression d'un email
     *
     * @logical-group Intitulesagents
     * @user-profil Admin
     *
     * @access public
     * @param integer $id identifiant du scanemail
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $this->Intituleagent->begin();
        if ($this->Intituleagent->delete($id)) {
            $this->Intituleagent->commit();
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        } else {
            $this->Intituleagent->rollback();
        }
        $this->Jsonmsg->send();
    }

    /**
     * Récupération de la liste des mails à scruter (ajax)
     *
     * @logical-group Intitulesagents
     * @user-profil Admin
     *
     * @access public
     * @return void
     */
    public function getIntitulesagents() {
        $intitulesagents_tmp = $this->Intituleagent->find(
            "all",
            array(
                'contain' => false,
                'order' => 'Intituleagent.name'
            )
        );
        $intitulesagents = array();
        foreach ($intitulesagents_tmp as $i => $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $intitulesagents[] = $item;
        }
        $this->set(compact('intitulesagents'));
    }

}

?>
