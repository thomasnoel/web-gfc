<?php

App::uses('SimpleCommentAppController', 'SimpleComment.Controller');

/**
 * SimpleComment Plugin
 * SimpleComment App controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package             SimpleComment
 * @subpackage          SimpleComment.Controller
 */
class CommentsController extends AppController {

    /**
     * Controller name
     *
     * @var array
     */
    public $name = 'Comments';

    /**
     * Controller uses
     *
     * @var array
     */
    public $uses = array('SimpleComment.Comment');

    /**
     * Controller uses
     *
     * @var array
     */
    public $helpers = array('Html', 'Form');

    /**
     *
     * @param type $flux_id
     * @return type
     */
    private function _getAllowedReaders($flux_id) {
		$this->loadModel('User');
        //FIXME : revoir l'utilisation du plugin
        if( !Configure::read('Commentaire.AllDesktops') ) {
            $this->loadModel('Courrier');
            $Reader = ClassRegistry::init('Desktop');
            $Desktopmanager = ClassRegistry::init('Desktopmanager');

            $qd = array(
                'fields' => array('id', 'name'),
                'recursive' => -1,
                'joins' => array(
                    $Desktopmanager->join($Reader->Composition->name),
                    $Reader->Composition->join($Reader->Composition->Etape->name),
                    $Reader->Composition->Etape->join($Reader->Composition->Etape->Circuit->name),
                    $Reader->Composition->Etape->Circuit->join($Reader->Composition->Etape->Circuit->Soustype->name),
                    $Reader->Composition->Etape->Circuit->Soustype->join($this->Courrier->name)
                ),
                'conditions' => array(
                    $this->Courrier->name . '.id' => $flux_id
                ),
                'contain' => false
            );
			$desktopmanagers = $Desktopmanager->find('list', $qd);
			$ids = array_keys($desktopmanagers);
			$qd_desktop = array(
				'fields' => array('id', 'name'),
				'recursive' => -1,
				'joins' => array(
					array(
						'table' => '"public"."desktops_desktopsmanagers"',
						'alias' => 'DesktopDesktopmanager',
						'type' => 'INNER',
						'conditions' => array(
							'DesktopDesktopmanager.desktopmanager_id' => $ids,
							'Desktop.id = DesktopDesktopmanager.desktop_id',
							'Desktop.id NOT IN (-1, -3)'
						)
					),
				),
				'contain' => false
			);
			$desktops = $Reader->find('all', $qd_desktop);

			foreach($desktops as $desktop) {
				$userIds = $Reader->getUsers($desktop['Desktop']['id']);
				$users = $this->User->find(
					'all',
					array(
						'recursive' => -1,
						'contain' => array(
							'Desktop'
						),
						'conditions' => array(
							'User.id' => $userIds,
							'User.active' => true
						),
						'order' => 'User.nom ASC'
					)
				);
				foreach( $users as $user) {
					$allDesktops[$user['Desktop']['id']] = $user['User']['nom'].' '.$user['User']['prenom'];
				}
			}
			return $allDesktops;
        }
        else {
            $users = $this->User->find(
				'all',
				array(
					'recursive' => -1,
					'contain' => array(
						'Desktop'
					),
					'conditions' => array('User.active' => true),
					'order' => 'User.nom ASC'
				)
			);
			foreach( $users as $user) {
				$allDesktops[$user['Desktop']['id']] = $user['User']['nom'].' '.$user['User']['prenom'];
			}

            return $allDesktops;
        }

    }

    /**
     *
     * @param type $flux_id
     */
    public function add($flux_id, $canAddPrivateComment = false) {


        $this->loadModel('Courrier');
        $flux = $this->Courrier->find(
                'first', array(
            'conditions' => array(
                'Courrier.id' => $flux_id
            ),
            'recursive' => -1
                )
        );
        $isSoustype = !empty($flux['Courrier']['soustype_id']) ? true : false;

        $this->set('canAddPrivateComment', $canAddPrivateComment);
        $this->set('isSoustype', $isSoustype);
        $this->set('readers', $this->_getAllowedReaders($flux_id));
        $this->set('flux_id', $flux['Courrier']['id']);
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init(__d('default', 'save.error'));



            $this->Comment->create();
            $this->request->data['Comment']['owner_id'] = $this->Session->read('Auth.User.Courrier.Desktop.id');
            $this->request->data['Comment']['target_id'] = $flux_id;
            $this->request->data['Comment']['created'] = date( 'Y-m-d H:i:s' );
            $this->request->data['Comment']['modified'] = date( 'Y-m-d H:i:s' );
//            $this->request->data['Comment']['content'] = trim($this->request->data['Comment']['content'] );
            $this->request->data['Comment']['objet']= preg_replace("/[\n\r]/", " ", $this->request->data['Comment']['objet']);
            $success = $this->Comment->save($this->request->data);

            // En cas d'ajout de commentaire, on met à jour la date de modification de la table bancontenus pour le profil étant intervenu
            if( $success ) {
                $this->loadModel('Courrier');
                $this->Courrier->Bancontenu->updateAll(
                    array(
                        'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s' )."'"
                    ),
                    array(
                        'Bancontenu.courrier_id' => $flux_id,
                        'Bancontenu.desktop_id' => $this->request->data['Comment']['owner_id']
                    )
                );
                // on stocke qui fait quoi quand
//                $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (ajout d'un commentaire) le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (ajout d'un commentaire) le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);
            }

            if (!empty($success)) {
                $this->Jsonmsg->valid(__d('default', 'save.ok'));
            }
            $this->Jsonmsg->send();
        }

    }

    /**
     *
     * @param type $id
     * @param type $flux_id
     * @throws BadMethodCallException
     */
    public function edit($flux_id, $canAddPrivateComment = false, $id) {
        if (empty($id) || empty($flux_id)) {
            throw new BadMethodCallException();
        }

        if (!empty($this->request->data)) {
            $this->Jsonmsg->init(__d('default', 'save.error'));

            $this->Comment->create();
            $this->request->data['Comment']['id'] = $id;
            $this->request->data['Comment']['modified'] = date( 'Y-m-d H:i:s' );
            $comment = $this->Comment->find('first', array('conditions' => array('Comment.id' => $id), 'contain' => false, 'recursive' => '-1'));
            $success = $this->Comment->save($this->request->data);

            if (!empty($success)) {
                // on stocke qui fait quoi quand
                $this->loadModel('Courrier');
                $flux = $this->Courrier->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Courrier.id' => $flux_id
                        ),
                        'contain' => false,
                        'recursive' => -1
                    )
                );
//                $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (modification d'un commentaire) le ".date('d/m/Y à H:i:s'), LOG_WARNING );
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (modification d'un commentaire) le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);
                // En cas d'ajout de commentaire, on met à jour la table bancontenus pour le cycle de vie du flux
                if( $success ) {
                    $this->Courrier->Bancontenu->updateAll(
                        array(
                            'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s')."'"
                        ),
                        array(
                            'Bancontenu.courrier_id' => $flux_id,
                            'Bancontenu.desktop_id' => $comment['Comment']['owner_id']
                        )
                    );
                }





                $this->Jsonmsg->valid(__d('default', 'save.ok'));
            }
            $this->Jsonmsg->send();
        } else {

            $this->loadModel('Courrier');
            $flux = $this->Courrier->find(
                    'first', array(
                'conditions' => array(
                    'Courrier.id' => $flux_id
                ),
//                    'recursive' => -1
                    )
            );
            $isSoustype = !empty($flux['Courrier']['soustype_id']) ? true : false;
            $this->set('isSoustype', $isSoustype);

            $this->request->data = $this->Comment->find('first', array('conditions' => array('Comment.id' => $id), 'contain' => array('Reader')));
            $this->set('canAddPrivateComment', $canAddPrivateComment);
            $this->set('readers', $this->_getAllowedReaders($flux_id));
            $this->set('flux_id', $flux_id);
        }
    }

    /**
     *
     * @param type $id
     */
    public function delete($id) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $comment = $this->Comment->find('first', array('conditions' => array('Comment.id' => $id), 'contain' => false, 'recursive' => '-1'));
        // on stocke qui fait quoi quand
        $this->loadModel('Courrier');
        $flux = $this->Courrier->find(
            'first',
            array(
                'conditions' => array(
                    'Courrier.id' => $comment['Comment']['target_id']
                ),
                'contain' => false,
                'recursive' => -1
            )
        );
        if ($this->Comment->delete($id)) {
//            $this->log( "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (suppression d'un commentaire) le ".date('d/m/Y à H:i:s'), LOG_WARNING );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le flux ".$flux['Courrier']['reference']." (suppression d'un commentaire) le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', $flux);
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        }
        $this->Jsonmsg->send();
    }

}

?>
