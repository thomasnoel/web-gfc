<?php

/**
 * Services
 *
 * Services controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ServicesController extends AppController {

    /**
     * Controller name
     *
     * @access public
     * @var string
     */
    public $name = 'Services';

    /**
     * Controller uses
     *
     * @access public
     * @var array
     */
    public $uses = array('Service', 'Daro');

    /**
     * Controller helpers
     *
     * @access public
     * @var array
     */
    public $helpers = array('DataAcl.DataAcl');

    /**
     * Gestion des services (interface graphique)
     *
     * @logical-group Services
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function index() {
        $this->set('ariane', array(
            '<a href="environnement/index/0/admin">' . __d('menu', 'Administration', true) . '</a>',
            __d('menu', 'gestionServices', true)
        ));
    }


    protected function __setOptions() {

        $services = $this->Service->find('list', array('order' => 'Service.name ASC'));
        $this->set('services', $services);
        $desktops = $this->Service->Desktop->find('list', array('conditions' => array('Desktop.active' => true), 'order' => 'Desktop.name ASC'));
        $this->set('desktops', $desktops);

        $scanDesktops = $this->Service->Desktop->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.isdispatch' => true)));
        $this->set('scanDesktops', $scanDesktops);

    }
    /**
     * Récupération de la liste des services (ajax)
     *
     * @logical-group Services
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function getServices() {
        $services_tmp = $this->Service->find("threaded", array('contain' => array($this->Service->Desktop->alias), 'order' => 'Service.name'));
        $services = array();
        foreach ($services_tmp as $item) {
            $item['right_edit'] = true;
            $item['right_delete'] = true;
            $services[] = $item;
        }
        $this->set('services', $services);
        $this->set('nbItem', $this->Service->find('count'));
    }

    //TODO: supprimer les fonctions commentées
//	public function view($id) {
//		$service = $this->Service->find(
//				'first', array(
//			'contain' => array(
//				'Collectivite',
//				'User',
//			),
//			'conditions' => array('Service.id' => $id)
//				)
//		);
//		if (empty($service)) {
//			throw new NotFoundException();
//		}
//		$this->set(compact('service'));
//	}

    /**
     * Ajout d'un service
     *
     * @logical-group Services
     * @user-profile Admin
     *
     * @access public
     * @return void
     */
    public function add() {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Service->create($this->request->data);
            $this->Service->begin();
            if ($this->Service->save()) {
                $this->Service->commit();
                $this->Jsonmsg->valid();
            } else {
                $this->Service->rollback();
            }
            $this->Jsonmsg->send();
        }

        $scanDesktops = $this->Service->Desktop->Desktopmanager->find('list', array('conditions' => array('Desktopmanager.isdispatch' => true)));
        $this->set('scanDesktops', $scanDesktops);
        $this->__setOptions();
    }

    /**
     * Edition d'un service (interface graphique)
     *
     * @logical-group Services
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du service
     * @return void
     */
    public function edit($id = null) {
        if ($id != null) {
            $this->set('id', $id);
        }
    }

    /**
     * Edition des informations d'un service
     *
     * @logical-group Services
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du service
     * @return void
     */
    public function setInfos($id = null) {

        if (!empty($this->request->data['Service'])) {
            $this->Jsonmsg->init();
            $this->Service->create();
            $saved = $this->Service->save($this->request->data);

            if (!empty($saved)) {
                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
            // On enregistre dans le journal
            $service = $this->Service->find(
                'first',
                array(
                    'conditions' => array(
                        'Service.id' => $id
                    ),
                    'contain' => false
                )
            );
            $this->loadModel('Journalevent');
            $datasSession = $this->Session->read('Auth.User');
            $journalmsg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a modifié le service ".$service['Service']['name']." le ".date('d/m/Y à H:i:s');
            $this->Journalevent->saveDatas( $datasSession, $this->action, $journalmsg, 'info', []);
        } else {
            $this->request->data = $this->Service->find('first', array(
                'recursive' => -1,
                'contain' => array(
                    'Desktop'
                ),
                'conditions' => array('Service.id' => $id)
            ));
            $services = $this->Service->find('list', array('conditions' => array('Service.id !=' => $id, 'OR' => array('Service.parent_id !=' => $id, 'Service.parent_id' => '')), 'order' => 'Service.name ASC'));
            $this->set('services', $services);
            $this->__setOptions();
        }
    }

    /**
     * Edition de la liste des habilitations de sous-types
     *
     * @logical-group Services
     * @logical-group Gestion des habilitations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du service
     * @return void
     */
    public function setDroitsData($id = null) {
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            if ($this->request->data['Acl']['Type'] == 'DataAcl') {
                if ($this->DataAcl->setRights($this->request->data['Requester'], $this->request->data['Rights'])) {
                    $this->Jsonmsg->valid();
                }


                $this->Service->id = $id;
                $daro = $this->Service->node();
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a édité les droits sur les types du service ".$daro[0]['Daro']['alias']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', array());
            }
            $this->Jsonmsg->send();
        } else {
            $this->Service->id = $id;
            $daro = $this->Service->node();
            $dataRights = $this->DataAcl->getRightsGrid($daro[0], array(), array('actions' => array('read'), 'models' => array('Type', 'Soustype')));
            $this->set('dataRights', $dataRights);
            $this->set('id', $id);
        }
    }

    /**
     * Edition de la liste des habilitations de méta-données
     *
     * @logical-group Services
     * @logical-group Gestion des habilitations
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du service
     * @return void
     */
    public function setDroitsMeta($id = null) {
        //mise a jour des droits sur les meta donnees
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            if ($this->request->data['Acl']['Type'] == 'DataAcl') {
                if ($this->DataAcl->setRights($this->request->data['Requester'], $this->request->data['Rights'])) {
                    $this->Jsonmsg->valid();
                }

                $this->Service->id = $id;
                $daro = $this->Service->node();
                $this->loadModel('Journalevent');
                $datasSession = $this->Session->read('Auth.User');
                $msg = "L'utilisateur ". $this->Session->read('Auth.User.username'). " a édité les droits sur les métadonnées du service ".$daro[0]['Daro']['alias']." le ".date('d/m/Y à H:i:s');
                $this->Journalevent->saveDatas( $datasSession, $this->action, $msg, 'info', array());
            }
            $this->Jsonmsg->send();
        } else {
            $this->Service->id = $id;
            $daro = $this->Service->node();
            $dataRights = $this->DataAcl->getRightsGrid($daro[0], array(), array('actions' => array('read', 'update'), 'models' => array('Metadonnee')));
            $this->set('dataRights', $dataRights);
            $this->set('id', $id);
        }
    }

    /**
     * Suppression d'un service
     *
     * @logical-group Services
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du service
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        $valid = false;
        //s'il y a des utilisateurs dasn ce service, on ne peut pas le supprimer
        $user = $this->Service->getUsers($id);

        if (!count($user) > 0) {
            $servicesEnfants = $this->Service->find(
                    'all', array(
                'conditions' => array(
                    'Service.parent_id' => $id
                ),
                'contain' => false
                    )
            );

            $servicesEnfants = Hash::extract($servicesEnfants, '{n}.Service.id');

            $success = true;
            if (!empty($servicesEnfants)) {
                $success = false;
                foreach ($servicesEnfants as $i => $enfantId) {
                    $this->Service->delete($enfantId);
                    $success = true;
                }
            }

            if ($success && $this->Service->delete($id)) {
                $this->Jsonmsg->valid(__d('default', 'delete.ok'));
            }
        } else {
            $this->Jsonmsg->error(__d('service', 'delete.userlinked'));
        }
        $this->Jsonmsg->send();
    }

}

?>
