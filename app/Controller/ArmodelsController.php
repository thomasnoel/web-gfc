<?php

/**
 * Modèles d'accusés de réception
 *
 * Armodels controller class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		Controller
 */
class ArmodelsController extends AppController {

    /**
     * Etat du téléchargement
     *
     * @access private
     * @var array
     */
    private $_uploadStatus = array(
        'NO_UPLOAD' => 0,
        'UPLOAD_START' => 1,
        'UPLOAD_END' => 2
    );

    /**
     *
     * Correspondance type MIME / extensions de fichier
     *
     * @access private
     * @var array
     */
    private $_allowed = array(
        'audio' => array(
            'audio/x-wav' => 'wav',
            'audio/mpeg' => 'mp3',
            'application/ogg' => 'ogg',
            'audio/ogg' => 'ogg'
        ),
        'video' => array(
            'video/x-msvideo' => 'avi',
            'video/mpeg' => 'mpg',
            'video/mp4' => 'mp4',
            'application/ogg' => 'ogg',
            'video/ogg' => 'ogg'
        ),
        'ott' => array(
            'application/vnd.oasis.opendocument.text' => 'odt',
            'application/vnd.oasis.opendocument.text-template' => 'ott',
            'application/force-download' => 'odt'
        ),
		'application/vnd.oasis.opendocument.text' => 'odt',
		'application/vnd.oasis.opendocument.text-template' => 'ott',
		'application/force-download' => 'odt' //FIXME
    );

    /**
     * Controller name
     *
     * @var string
     * @access public
     */
    public $name = 'Armodels';

    /**
     * Controller components
     *
     * @var mixed
     * @access public
     */
    public $components = array('Linkeddocs');


     public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('deletefile');
    }


    /**
     * Ajout d'un modèle d'accusé de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles d'accusés de réception
     * @user-profile Admin
     *
     * @access public
     * @param integer $soustype_id
     * @return void
     */
    public function add($soustype_id = null) {
        if (empty($this->request->data)) {
            $options = array();
            $warnings = array();
            foreach ($this->Armodel->formats as $format) {
                if (
                        ($format == "sms" && Configure::read('sms.conn')) ||
                        ($format == "email" && Configure::read('email.conn')) ||
                        ($format != "email" && $format != "sms")
                ) {
                    $options[$format] = __d('armodel', 'Armodel.' . $format);
                } else {
                    $warnings[] = $format;
                }
            }

            $this->set('warnings', $warnings);
            $this->set('options', $options);
            $this->set('soustype_id', $soustype_id);
        } else {
            if ($this->request->is('post')) {
                $upload = $this->_uploadStatus['NO_UPLOAD'];

                $armodel = $this->request->data;

                //traitement de l upload des fichiers
                if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                    $upload = $this->_uploadStatus['UPLOAD_START'];
                    $ext = '';
                    if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                        $ext = $matches[1];
                    }

                    if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed[$this->request->data['Armodel']['format']]))) {
                        $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                    } else {
                        if ($ext != $this->_allowed[$this->request->data['Armodel']['format']][$_FILES['myfile']['type']]) {
                            $message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            $upload = $this->_uploadStatus['UPLOAD_END'];
                            $armodel['Armodel']['size'] = $_FILES['myfile']['size'];
                            $armodel['Armodel']['ext'] = $ext;
                            $armodel['Armodel']['mime'] = $_FILES['myfile']['type'];
                        }
                    }
                }

                //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                $create = false;
                if ($upload != $this->_uploadStatus['UPLOAD_START']) {

                    $this->Armodel->create($armodel);
                    $armodelsaved = $this->Armodel->save();

                    if (!empty($armodelsaved)) {
                        $create = true;
                        $message = __d('default', 'save.ok');
                    }

                    //suppression du fichier si un fichier est uploadé
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        unlink($_FILES['myfile']['tmp_name']);
                    }
                }
                $this->set('create', $create);
                $this->set('message', $message);
            }
        }
    }

    /**
     * Suppression d'un modèle d'accusé de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles d'accusés de réception
     * @user-profile User
     *
     * @access public
     * @param integer $soustype_id
     * @return void
     */
    public function delete($id = null) {
        $this->Jsonmsg->init(__d('default', 'delete.error'));
        if ($this->Armodel->delete($id)) {
            $this->Jsonmsg->valid(__d('default', 'delete.ok'));
        }
        $this->Jsonmsg->send();
    }

    /**
     * Téléchargement d'un modèle d'accusée de réception
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles d'accusés de réception
     * @user-profile User
     *
     * @access public
     * @param integer $id
     * @return void
     */
    public function download($id) {
        $armodel = $this->Armodel->find('first', array('conditions' => array('Armodel.id' => $id), 'recursive' => -1));
        if (empty($armodel)) {
            throw new NotFoundException();
        }


        $this->autoRender = false;
        Configure::write('debug', 0);

        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");

        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"" . Inflector::slug($armodel['Armodel']["name"]) . '.' . $this->_allowed[$armodel['Armodel']['format']][$armodel['Armodel']['mime']] . "\"");
        header("Content-length: " . $armodel['Armodel']['size']);

        print $armodel['Armodel']['content'];
        exit();
    }

    /**
     * Récupération de la pré-visualisation d'un modèle d'accusé de réception (génération si nécessaire)
     *
     * @logical-group Flux
     * @logical-group Context
     * @logical-group Modèles d'accusés de réception
     * @user-profile User
     *
     * @access public
     * @param integer $armodelId
     * @throws NotFoundException
     */
    public function getPreview($armodelId) {
        $armodel = $this->Armodel->find('first', array('conditions' => array('Armodel.id' => $armodelId), 'recursive' => -1));
        if (empty($armodel)) {
            throw new NotFoundException();
        }

        $preview = $this->Armodel->generatePreview($armodelId);


        $mime = $armodel['Armodel']['mime'];

        $tmpPreviewFile = '';
        $tmpPreviewFileBasename = '';
        $previewType = '';

        if (in_array($mime, $this->Armodel->convertibleMimeTypes) || $mime == "application/pdf" || $mime == "PDF" /* || $mime == "application/vnd.oasis.opendocument.text" */) {
            $previewType = 'flexpaper';
            $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($armodel['Armodel']['name']) . "_preview.pdf";
            $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
            //purge des fichiers de preview précédents
            $this->Linkeddocs->purge();
            file_put_contents($tmpPreviewFile, $preview);
        } else {
            foreach ($this->Armodel->otherMimeTypes as $type => $mimes) {

                if (in_array($mime, array_keys($mimes))) {
                    $previewType = $type;
                    $tmpPreviewFileBasename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($armodel['Armodel']['name'])  . "_preview." . $mimes[$mime];
                    $tmpPreviewFile = APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $tmpPreviewFileBasename;
                    //purge des fichiers de preview précédents
                    $this->Linkeddocs->purge();
                    file_put_contents($tmpPreviewFile, $armodel['Armodel']['content']);
                }
            }
        }

        if (!empty($tmpPreviewFile)) {
            chmod($tmpPreviewFile, 0777);
        }
        $this->set('fileName', $tmpPreviewFileBasename);

        $this->set('previewType', $previewType);
        $this->set('mime', "pdf");
        $this->set('tmpPreviewFileBasename', $tmpPreviewFileBasename);
    }

    public function deletefile($armodelId) {
        $this->autoRender = false;
        $armodel = $this->Armodel->find('first', array('conditions' => array('Armodel.id' => $armodelId), 'recursive' => -1));
        $armodelfilename = Inflector::slug($this->Session->read('Auth.User.username')) . "_" . Inflector::slug($armodel['Armodel']['name']) . "_preview.pdf";
        unlink( APP . DS . WEBROOT_DIR . DS . 'files' . DS . 'previews' . DS . $armodelfilename );
    }


    /**
     * Edition d'un document Bordereau
     *
     * @logical-group Documents
     * @user-profile Admin
     *
     * @access public
     * @param integer $id identifiant du type
     * @throws NotFoundException
     * @return void
     */
    public function edit($id) {

//        $this->autoRender = false;
        if (!empty($this->request->data)) {
            $this->Jsonmsg->init();
            $this->Armodel->create();
            $armodel = $this->request->data;


            if ($this->Armodel->save($armodel)) {
                if ($this->request->is('post')) {
                    $upload = $this->_uploadStatus['NO_UPLOAD'];
                    $armodel = $this->request->data;
                    //traitement de l upload des fichiers
                    if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                        $upload = $this->_uploadStatus['UPLOAD_START'];
                        $ext = '';
                        if (preg_match("/\.([^\.]+)$/", $_FILES['myfile']['name'], $matches)) {
                            $ext = $matches[1];
                        }
                        if (!in_array($_FILES['myfile']['type'], array_keys($this->_allowed))) {
                            $message = __d('default', 'Upload.error.format') . (Configure::read('debug') > 0 ? ' (mime : ' . $_FILES['myfile']['type'] . ')' : '');
                        } else {
                            if ($ext != $this->_allowed[$_FILES['myfile']['type']]) {
    //							$message = __d('default', 'Upload.error.bad_mime_ext') . (Configure::read('debug') > 0 ? ' (ext :  ' . $ext . ' - mime : ' . $_FILES['myfile']['type'] . ')' : '');
                            } else {
                                $upload = $this->_uploadStatus['UPLOAD_END'];
                                $armodel['Armodel']['size'] = $_FILES['myfile']['size'];
                                $armodel['Armodel']['ext'] = $ext;
                                $armodel['Armodel']['mime'] = $_FILES['myfile']['type'];
                            }
                        }
                    }
                    //enregistrement des infos en base si pas  d upload ou si l upload est terminé
                    $create = false;
                    if ($upload != $this->_uploadStatus['UPLOAD_START']) {
                        $this->Armodel->create($armodel);
                        $armodelsaved = $this->Armodel->save();
                        if (!empty($armodelsaved)) {
                            $create = true;
                            $message = __d('default', 'save.ok');
                            $this->Jsonmsg->valid($message);
                        }
                        //suppression du fichier si un fichier est uploadé
                        if ($_FILES['myfile']['error'] != UPLOAD_ERR_NO_FILE) {
                            unlink($_FILES['myfile']['tmp_name']);
                        }
                    }
                    $this->set('create', $create);
                    $this->set('message', $message);
                }



                $this->Jsonmsg->valid();
            }
            $this->Jsonmsg->send();
        } else {
            $armodel = $this->Armodel->find(
                'first',
                array(
                    'fields' => array(
                        'Armodel.id',
                        'Armodel.name',
                        'Armodel.ext',
                        'Armodel.soustype_id'
                    ),
                    'conditions' => array(
                        'Armodel.id' => $id
                    ),
                    'contain' => false
                )
            );
            if (empty($armodel)) {
                throw new NotFoundException();
            }
        }

        $this->set('armodel', $armodel);
    }

}
