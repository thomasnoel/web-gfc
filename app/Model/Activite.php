<?php

/**
 * Activite model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Activite extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Activite';

    /**
     *
     * @var type
     */
    public $useTable = 'activites';

    /**
     * hasMany
     *
     * @access public
     */
    public $hasMany = array(
        'Sousactivite' => array(
            'className' => 'Sousactivite',
            'foreignKey' => 'activite_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );
    public $hasAndBelongsToMany = array(
        'Organisme' => array(
            'className' => 'Organisme',
            'joinTable' => 'activites_organismes',
            'foreignKey' => 'activite_id',
            'associationForeignKey' => 'organisme_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'ActiviteOrganisme'
        )
    );
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

}

?>
