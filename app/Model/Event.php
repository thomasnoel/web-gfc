<?php


/**
 * Event model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Event extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Event';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'events';


    public $hasAndBelongsToMany = array(
		'Contact' => array(
			'className' => 'Contact',
			'joinTable' => 'contacts_events',
			'foreignKey' => 'event_id',
			'associationForeignKey' => 'contact_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'ContactEvent'
		)
	);


    public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );

}

?>
