<?php

/**
 * Gabaritdocument model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Gabaritdocument extends AppModel {

    /**
     *
     * @var type
     */
    public $name = 'Gabaritdocument';

    /**
     *
     * @var type
     */
    public $useTable = 'gabaritsdocuments';

    /**
     *
     * @var type
     */
    public $actsAs = array('Linkeddocs');
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

}

?>
