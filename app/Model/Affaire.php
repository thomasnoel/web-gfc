<?php

App::uses('AppModel', 'Model');

/**
 * Affaire model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		app.Model
 */
class Affaire extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Affaire';

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'name' => array(
			'rule' => 'isUnique',
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false
			)
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'affaire_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
//      'Collectivite' => array(
//          'className' => 'Collectivite',
//          'foreignKey' => 'collectivite_id',
//          'type' => 'INNER',
//          'conditions' => null,
//          'fields' => null,
//          'order' => null
//      ),
		'Dossier' => array(
			'className' => 'Dossier',
			'foreignKey' => 'dossier_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);
	public $hasAndBelongsToMany = array(
		'Recherche' => array(
			'className' => 'Recherche',
			'joinTable' => 'recherches_affaires',
			'foreignKey' => 'affaire_id',
			'associationForeignKey' => 'recherche_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheAffaire'
		)
	);

}

?>
