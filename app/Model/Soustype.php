<?php

App::uses('AppModel', 'Model');

/**
 * Soustype model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Soustype extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Soustype';

    /**
     *
     * @var type
     */
    public $actsAs = array('DataAcl.DataAcl' => array('type' => 'controlled'), 'Tree');

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'name' => array(
            //TODO: remettre les regles d unicite
//          'rule' => 'isUnique',
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        ),
        'type_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => false,
            ),
        ),
    );

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Circuit' => array(
            'className' => 'Cakeflow.Circuit',
            'foreignKey' => 'circuit_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Compteur' => array(
            'className' => 'Compteur',
            'foreign_key' => 'compteur_id',
            'type' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

    /**
     * hasMany
     *
     * @access public
     */
    public $hasMany = array(
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'soustype_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Armodel' => array(
            'className' => 'Armodel',
            'foreignKey' => 'soustype_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Rmodel' => array(
            'className' => 'Rmodel',
            'foreignKey' => 'soustype_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Rmodelgabarit' => array(
            'className' => 'Rmodelgabarit',
            'foreignKey' => 'soustype_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Sousoperation' => array(
            'className' => 'Sousoperation',
            'foreignKey' => 'soustype_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );

    /**
     * hasAndBelongsToMany
     *
     * @access public
     */
    public $hasAndBelongsToMany = array(
        'Formatreponse' => array(
            'className' => 'Formatreponse',
            'joinTable' => 'formatsreponse_soustypes',
            'foreignKey' => 'soustype_id',
            'associationForeignKey' => 'formatreponse_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'FormatreponseSoustype'
        ),
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_soustypes',
            'foreignKey' => 'soustype_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheSoustype'
        ),
        'Metadonnee' => array(
            'className' => 'Metadonnee',
            'joinTable' => 'metadonnees_soustypes',
            'foreignKey' => 'soustype_id',
            'associationForeignKey' => 'metadonnee_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'MetadonneeSoustype'
        ),
        'Soustypecible' => array(
            'className' => 'Soustypecible',
            'joinTable' => 'soustypes_soustypescibles',
            'foreignKey' => 'soustype_id',
            'associationForeignKey' => 'soustypecible_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'SoustypeSoustypecible'
        )
    );

    /**
     *
     * @param type $data
     * @return boolean
     */
    public function checkParadox($data) {
        if (isset($this->data[$this->alias]['id'])) {
            return $data['parent_id'] != $this->data[$this->alias]['id'];
        }
        return true;
    }

    /**
     *
     * @return null
     */
    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['Soustype']['type_id'])) {
            $typeId = $this->data['Soustype']['type_id'];
        } else {
            $typeId = $this->field('type_id');
        }
        if (!$typeId) {
            return null;
        } else {
            return array('Type' => array('id' => $typeId));
        }
    }

    /**
     *
     * @param type $id
     * @param type $alias
     * @return null
     */
    public function getDaco($id = null, $alias = false) {
        if ($id != null) {
            $this->id = $id;
        }

        if (!$this->id) {
            return null;
        }

        $daco = $this->Daco->node(array('model' => 'Soustype', 'foreign_key' => $this->id));

        if ($alias) {
            $return = $daco[0]['Daco']['alias'];
        } else {
            $return = $daco[0]['Daco'];
        }

        return $return;
    }

    /**
     *
     * @param type $alias
     * @return type
     */
    public function getDacos($alias = false) {
        $soustypes = $this->find('list');
        $return = array();
        foreach ($soustypes as $ksoustype => $soustype) {
            $this->id = $ksoustype;
            $return[] = $this->getDaco($ksoustype, $alias);
        }
        return !empty($return) ? $return : null;
    }

    /**
     *
     * @param type $id
     * @param type $simul
     * @return type
     */
    public function getRef($id, $simul = false) {
        $refInOut = array(
            0 => 'S_',
            1 => 'E_',
            2 => 'I_'
        );
        $stype = $this->find(
                'first', array(
//                'recursive' => -1,
            'fields' => array(
                'Soustype.compteur_id',
                'Soustype.entrant'
            ),
            'conditions' => array(
                'Soustype.id' => $id
            ),
            'join' => array($this->join('Courrier'))
                )
        );
        $courrier_id = Hash::get($stype, 'Courrier.id');
//		return $refInOut[$stype['Soustype']['entrant']] . $this->Compteur->genereCompteur($stype['Soustype']['compteur_id'], $simul);
        return $this->Courrier->genReferenceUnique($courrier_id);
    }

    /**
     * Suppression du soustype à partir des sousoperatins (SAERP)
     */
    public function deleteStype($id) {
        $stype = $this->find('first', array('conditions' => array('Soustype.id' => $id), 'contain' => false));
        if (empty($stype)) {
            throw new NotFoundException();
        }
        $this->begin();

        $used = false;
        $delete = false;
        $ref = $this->Courrier->find('first', array('conditions' => array('Courrier.soustype_id' => $stype['Soustype']['id']), 'contain' => false));

        if (!empty($ref)) {
            $used = true;
        }
//debug($used);
        if (!$used) {
            if ($this->delete($id, true)) {
                $this->commit();
                $delete = true;
            }
        } else {
            $this->rollback();
            $delete = false;
        }
        return $delete;
    }

}

?>
