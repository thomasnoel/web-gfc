<?php


/**
 * Templatenotification model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Templatenotification extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Templatenotification';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'templatenotifications';

    public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'subject' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		),
		'object' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			)

		)
    );

}

?>
