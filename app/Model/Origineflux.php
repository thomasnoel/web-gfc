<?php

App::uses('AppModel', 'Model');

/**
 * Metadonnee model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Origineflux extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Origineflux';
    public $useTable = 'originesflux';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'name' => array(
            array(
                'rule' => 'notBlank',
            ),
        ),
    );

    /**
     *
     */
    public $hasMany = array(
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'origineflux_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Pliconsultatif' => array(
            'className' => 'Pliconsultatif',
            'foreignKey' => 'origineflux_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * Fonciton permettant de rechercher les origines selon certains critères
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres) {
         /// Conditions de base
        if (empty($criteres)) {
            $conditions = array(
                'Origineflux.active' => true
            );
        }

        if (!empty($criteres)) {
            if (isset($criteres['Origineflux']['active'])) {
                $conditions[] = array('Origineflux.active' => $criteres['Origineflux']['active']);
            }
            if (isset($criteres['Origineflux']['name']) && !empty($criteres['Origineflux']['name'])) {
                $conditions[] = 'Origineflux.name ILIKE \'' . $this->wildcard('%' . $criteres['Origineflux']['name'] . '%') . '\'';
            }
        }

        $querydata = array(
            'contain' => false,
            'conditions' => $conditions,
            'order' => 'Origineflux.name'
        );

        return $querydata;
    }

}

?>
