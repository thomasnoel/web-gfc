<?php

App::uses('AppModel', 'Model');

/**
 * Metadonnee model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Selectvaluemetadonnee extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Selectvaluemetadonnee';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'metadonnee_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Metadonnee' => array(
			'className' => 'Metadonnee',
			'foreignKey' => 'metadonnee_id',
			'type' => 'LEFT OUTER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

    /**
     *
     */
    public $hasMany = array(
        'RechercheSelectvaluemetadonnee' => array(
			'className' => 'RechercheSelectvaluemetadonnee',
			'foreignKey' => 'selectvaluemetadonnee_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
    );


}

?>
