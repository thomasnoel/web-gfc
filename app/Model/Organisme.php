<?php

App::uses('Aborganisme', 'Addressbook.Model');

/**
 * Contact model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Organisme extends Aborganisme {

    /**
     *
     * @var type
     */
    public $name = 'Organisme';

    /**
     *
     * @var type
     */
    public $useTable = 'organismes';
    public $hasMany = array(
        'Contact' => array(
            'className' => 'Contact',
            'foreignKey' => 'organisme_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'organisme_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
        'Ordreservice' => array(
            'className' => 'Ordreservice',
            'foreignKey' => 'organisme_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );

    /**
     *
     * @var type
     */
    public $belongsTo = array(
        'Addressbook' => array(
            'className' => 'Addressbook',
            'foreignKey' => 'addressbook_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Ban' => array(
            'className' => 'Ban',
            'foreignKey' => 'ban_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Bancommune' => array(
            'className' => 'Bancommune',
            'foreignKey' => 'bancommune_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     *
     * @var type
     */
    public $hasAndBelongsToMany = array(
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_organismes',
            'foreignKey' => 'organisme_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheOrganisme'
        ),
        'Operation' => array(
            'className' => 'Operation',
            'joinTable' => 'organismes_operations',
            'foreignKey' => 'organisme_id',
            'associationForeignKey' => 'operation_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'OrganismeOperation'
        ),
        'Activite' => array(
            'className' => 'Activite',
            'joinTable' => 'activites_organismes',
            'foreignKey' => 'organisme_id',
            'associationForeignKey' => 'activite_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'ActiviteOrganisme'
        )
    );

    /**
     *
     * @param type $created
     * @return type
     */
    public function beforeSave($options = array()) {
        parent::beforeSave($options);
        if (!empty($this->data['Organisme']['name'])) {
            $this->data['Organisme']['slug'] = strtolower(Inflector::slug($this->data['Organisme']['name']));
        }
        return true;
    }

    /**
     * Fonciton permettant de rechercher les organismes selon certains critères
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres, $orgId = null) {

        $conditions = array();
        /// Conditions de base
        if (!empty($orgId)) {
            $conditions = array(
                'Organisme.id' => $orgId
            );
        }
//debug($criteres);die();
        // Critères sur l'utilisateur
        foreach (array('name', 'email') as $critereOrganisme) {
            if (isset($criteres['Organisme'][$critereOrganisme]) && !empty($criteres['Organisme'][$critereOrganisme])) {
                $conditions[] = 'NOACCENTS_UPPER(Organisme.'.$critereOrganisme.' ) LIKE \'' . $this->wildcard('%' . noaccents_upper( $criteres['Organisme'][$critereOrganisme] ) . '%') . '\'';
            }
        }


        if (isset($criteres['Organisme']['active'])) {
            $conditions[] = array(
                'Organisme.active' => $criteres['Organisme']['active']
            );
        }

        if (Configure::read('Conf.SAERP')) {
            // Filtre pour les OPs
            if (!empty($criteres['Organisme']['Operation']['id'])) {
                    $opsids = $criteres['Organisme']['Operation'];
                    $organismeOps = $this->OrganismeOperation->find(
                        'all',
                        array(
                            'conditions' => array(
                                'OrganismeOperation.operation_id' => $opsids
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty( $organismeOps ) ) {
                        $organismeOpsIds = Hash::extract($organismeOps, '{n}.OrganismeOperation.organisme_id');
                        $conditions[] = array(
                            'Organisme.id' => $organismeOpsIds
                        );
                    }
                    else {
                        $conditions[] = array(
                            'Organisme.id' => null
                        );
                    }
            }
            // Filtre pour les activités
            if (!empty($criteres['Organisme']['Activite']['id'])) {
                    $activitesids = $criteres['Organisme']['Activite'];
                    $organismeActivites = $this->ActiviteOrganisme->find(
                        'all',
                        array(
                            'conditions' => array(
                                'ActiviteOrganisme.activite_id' => $activitesids
                            ),
                            'contain' => false
                        )
                    );
                    if( !empty( $organismeActivites ) ) {
                        $organismeActivitesIds = Hash::extract($organismeActivites, '{n}.ActiviteOrganisme.organisme_id');
                        $conditions[] = array(
                            'Organisme.id' => $organismeActivitesIds
                        );
                    }
                    else {
                        $conditions[] = array(
                            'Organisme.id' => null
                        );
                    }
            }

            if( !empty( $criteres['SearchEvent']['SearchEvent']) ) {
                $contactsEvent = $this->Contact->ContactEvent->find(
                    'all',
                     array(
                       'conditions' => array(
                           'ContactEvent.event_id' => $criteres['SearchEvent']['SearchEvent']
                       ),
                      'contain' => false
                    )
                );
                $contactsEventsIds = Hash::extract($contactsEvent, '{n}.ContactEvent.contact_id');

                $contacts = $this->Contact->find(
                     'all',
                      array(
                        'conditions' => array(
                            'Contact.id' => $contactsEventsIds
                        ),
                       'contain' => false
                    )
                );

                if( !empty( $contacts )) {
                    $orgsEventsIds = Hash::extract($contacts, '{n}.Contact.organisme_id');

                    $conditions[] = array(
                        'Organisme.id' => $orgsEventsIds
                    );
                }
                else {
                    $conditions[] = array(
                        'Organisme.id' => null
                    );
                }

            }



            if( !empty($criteres['SearchFonction']['id']) ){
                if( !empty($criteres['SearchFonction']['id']) ){
                    $fonctionsIds = $criteres['SearchFonction']['id'];
                }
                $orgsFonction = $this->Contact->find(
                    'all',
                    array(
                        'conditions' => array(
                            'Contact.fonction_id' => $fonctionsIds
                        ),
                        'contain' => false
                    )
                );
                $orgsFonctionsIds = Hash::extract($orgsFonction, '{n}.Contact.organisme_id');
                $conditions[] = array(
                    'Organisme.id' => $orgsFonctionsIds
                );

           }


        }

        if (Configure::read('Conf.SAERP')) {
            $contain = array(
                'Operation',
                'Activite',
                'Contact' => array(
                    'conditions' => array(
                        'Contact.active' => true
                    ),
                    'order' => array(
                        'Contact.name ASC'
                    ),
                    'Event',
                    'Fonction',
                    'Operation'
                )
            );
        }
        else {
            $contain = array(
                'Contact' => array(
                    'conditions' => array(
                        'Contact.active' => true
                    ),
                    'order' => array(
                        'Contact.name ASC'
                    )
                )
            );
        }

        $query = array(
            'contain' => $contain,
            'order' => 'Organisme.name ASC',
            'conditions' => array(
                $conditions
            ),
        );

//        $query = array(
//            'contain' => array(
//                'Contact' => array(
//                    'conditions' => array(
//                        'Contact.active' => true
//                    ),
//                    'order' => array(
//                        'Contact.name ASC'
//                    )
//                /* ,
//                  'Contactinfo' => array(
//                  'conditions' => array(
//                  'Contactinfo.active' => true
//                  ),
//                  'order' => array(
//                  'Contactinfo.name ASC'
//                  )
//                  ) */
//                )
//            ),
////            'limit' => 20,
//            'order' => 'Organisme.name ASC',
//            'conditions' => array(
//                $conditions
//            ),
//        );

        return $query;
    }

    /**
     *
     * @param type $nom
     * @param type $prenom
     * @return type
     */
    public function getHomonyms($name) {
        $return = array();
        if (!empty($name)) {
//			$slug = strtolower($name);
            $name = strtolower(Inflector::slug($name));
//debug($name);
            $qd = array(
                'recursive' => -1,
                'fields' => array(
                    'Organisme.name',
                    'Addressbook.name'
                ),
                'contain' => false,
                'joins' => array(
                    $this->join($this->Addressbook->alias)
                ),
                'conditions' => array(
                    'OR' => array(
                        'Organisme.name ILIKE' => '%' . $name . '%',
                        "levenshtein('" . $name . "', Organisme.name) < " . Configure::read('levenshteinThreshold')
                    )
                ),
                'order' => array(
                    "levenshtein('" . $name . "', Organisme.name) DESC",
                    'Organisme.name'
                )
            );
//debug($qd);
            $return = $this->find('all', $qd);
//debug($return);
        }
        return $return;
    }

    public function getNombreOrganismeByCarnetId($addressbook_id) {
        $qd = array(
            'conditions' => array(
                'Organisme.addressbook_id' => $addressbook_id
            )
        );

        $return = $this->find('count', $qd);
        return $return;
    }

    /**
     *
     * @param type $organisme
     * @return type
     */
    public function organismeTreated( $organisme = array() ) {
        $oInfos = array();
        foreach ($organisme['Organisme'] as $key => $val) {
            if ($val != '' && $val != 'active' && $key != 'addressbook_id' && $key != 'ban_id' && $key != 'bancommune_id' && $key != 'adressecomplete') {
                $oInfos[$key != 'id' ? __d('organisme', 'Organisme.' . $key) : $key] = $val;
            }
        }

        $bInfos = array();
        foreach ($organisme['Ban'] as $key => $val) {
            if ($val != '' && $val != 'active' && $key != 'created' && $key != 'modified') {
                $bInfos[$key != 'id' ? __d('ban', 'Ban.' . $key) : $key] = $val;
            }
        }
        $bcInfos = array();
        foreach ($organisme['Bancommune'] as $key => $val) {
            if ($val != '' && $val != 'active' && $key != 'created' && $key != 'modified' && $key != 'ban_id') {
                $bcInfos[$key != 'id' ? __d('bancommune', 'Bancommune.' . $key) : $key] = $val;
            }
        }

        return array( 'Organisme' => ( $oInfos + $bInfos + $bcInfos) );
    }

    /**
     *
     * @return string
     */
    public function listOrganismes() {
        $addPrivateIds = CakeSession::read('Auth.AddressbookPrivate');
        $privateAccess = CakeSession::read('Auth.User.addressbook_private');
        if($privateAccess) {
            $conditionsAddress = array(
                'Organisme.active' => true
            );
        }
        else {
            if(!empty($addPrivateIds) ) {
                $conditionsAddress = array(
                    'Organisme.active' => true,
                    'Organisme.addressbook_id NOT IN' => array_keys( $addPrivateIds)
                );
            }
            else {
                $conditionsAddress = array(
                    'Organisme.active' => true
                );
            }
        }
        $organsme = $this->find('list', array( 'conditions' => $conditionsAddress, 'fields' => array('Organisme.name', 'Organisme.ville',  'Organisme.id'), 'order' => array('Organisme.name ASC')));
        $organismes = array();
        foreach( $organsme as $id => $value ) {
            $org = implode( ",", array_keys( $value )  );
            $ville = implode( ",", $value );
            $organismes[$id] = $org.' ( '.$ville . ' )';
        }
        return $organismes;
    }
}

?>
