<?php

App::uses('AppModel', 'Model');

/**
 * RechercheDesktop model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class DesktopmanagerOperation extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'DesktopmanagerOperation';

    /**
     *
     * @var type
     */
    public $useTable = 'desktopsmanagers_operations';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'desktopmanager_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
            ),
        ),
        'operation_id' => array(
            array(
                'rule' => array('numeric'),
                'allowEmpty' => true,
            ),
        )
    );

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'foreignKey' => 'desktopmanager_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Operation' => array(
            'className' => 'Operation',
            'foreignKey' => 'operation_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Intituleagent' => array(
            'className' => 'Intituleagent',
            'foreignKey' => 'intituleagent_id',
            'type' => 'INNER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

}

?>
