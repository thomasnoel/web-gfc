<?php

use Spipu\Html2Pdf\Html2Pdf;

App::uses('AppModel', 'Model');
App::uses('SCTarget', 'SimpleComment/Model/Behavior');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
/**
 * Courrier model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Courrier extends AppModel {

	/**
	 * Model name
	 *
	 * @var string
	 * @access public
	 */
	public $name = 'Courrier';


	public $actsAs = array('SimpleComment.SCTarget');

	/**
	 * Validation rules
	 *
	 * @var mixed
	 * @access public
	 */
	public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			),
		),
		'etat' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'typeflux' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'circuit_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'soustype_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'contact_id' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false
			),
		),
		'organisme_id' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false
			),
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasOne = array(
		'Traitement' => array(
			'className' => 'Cakeflow.Traitement',
			'foreignKey' => 'target_id'
		),
		'Ordreservice' => array(
			'className' => 'Ordreservice',
			'foreignKey' => 'courrier_id'
		),
		'Pliconsultatif' => array(
			'className' => 'Pliconsultatif',
			'foreignKey' => 'courrier_id'
		)
	);

	/**
	 * belongsTo
	 *
	 * @var mixed
	 * @access public
	 */
	public $belongsTo = array(
		'Soustype' => array(
			'className' => 'Soustype',
			'foreignKey' => 'soustype_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Affaire' => array(
			'className' => 'Affaire',
			'foreignKey' => 'affaire_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_creator_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Desktop' => array(
			'className' => 'Desktop',
			'foreignKey' => 'desktop_creator_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Affairesuivie' => array(
			'className' => 'Desktop',
			'foreignKey' => 'affairesuiviepar_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Service' => array(
			'className' => 'Service',
			'foreignKey' => 'service_creator_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Origineflux' => array(
			'className' => 'Origineflux',
			'foreignKey' => 'origineflux_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Organisme' => array(
			'className' => 'Organisme',
			'foreignKey' => 'organisme_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Contact' => array(
			'className' => 'Contact',
			'foreignKey' => 'contact_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Marche' => array(
			'className' => 'Marche',
			'foreignKey' => 'marche_id',
			'type' => 'LEFT OUTER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Consultation' => array(
			'className' => 'Consultation',
			'foreignKey' => 'consultation_id',
			'type' => 'LEFT OUTER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

	/**
	 * hasMany
	 *
	 * @var mixed
	 * @access public
	 */
	public $hasMany = array(
		'Tache' => array(
			'className' => 'Tache',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Traitement' => array(
			'className' => 'Cakeflow.Traitement',
			'foreignKey' => 'target_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Bancontenu' => array(
			'className' => 'Bancontenu',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Notifieddesktop' => array(
			'className' => 'Notifieddesktop',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Document' => array(
			'className' => 'Document',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Ar' => array(
			'className' => 'Ar',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Relement' => array(
			'className' => 'Relement',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Sendmail' => array(
			'className' => 'Sendmail',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Ordreservice' => array(
			'className' => 'Ordreservice',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Pliconsultatif' => array(
			'className' => 'Pliconsultatif',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Notifquotidien' => array(
			'className' => 'Notifquotidien',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
		'Sms' => array(
			'className' => 'Sms',
			'foreignKey' => 'courrier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

	/**
	 * hasAndBelongsToMany
	 *
	 * @var mixed
	 * @access public
	 */
	public $hasAndBelongsToMany = array(
		'Metadonnee' => array(
			'className' => 'Metadonnee',
			'joinTable' => 'courriers_metadonnees',
			'foreignKey' => 'courrier_id',
			'associationForeignKey' => 'metadonnee_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'CourrierMetadonnee'
		),
		'Repertoire' => array(
			'className' => 'Repertoire',
			'joinTable' => 'courriers_repertoires',
			'foreignKey' => 'courrier_id',
			'associationForeignKey' => 'repertoire_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'CourrierRepertoire'
		)
	);
	// Variable permettant de définir le dernier numéro de séqauence d'un courrier généré
	protected $_lastSequenceReference = null;
	protected $_lastSequenceDocument = null;

	/**
	 *
	 * @param type $id
	 * @return boolean
	 */
	public function isInCircuit($id = null) {
		$return = false;
		if ($id != null) {
			$tmp = $this->Traitement->find('first', array('fields' => array('id'), 'conditions' => array('target_id' => $id), 'recursive' => -1));
			if (!empty($tmp)) {
				$return = true;
			}
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return boolean
	 */
	public function isRefused($id = null) {
		$return = false;
		if ($id != null) {
			$courriers = $this->find('first', array('fields' => array('id'), 'conditions' => array('ref_ancien' => $id), 'recursive' => -1));
			if (!empty($courriers)) {
				$return = true;
			}
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return boolean
	 */
	public function isTreated($id = null) {
		$return = false;
		if ($id != null) {
			$traitement = $this->Traitement->find('first', array('fields' => array('treated'), 'conditions' => array('target_id' => $id), 'contain' => false));
			if (!empty($traitement) && $traitement['Traitement']['treated']) {
				$return = true;
			}
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return boolean
	 */
	public function isDuplicated($id = null) {
		$return = false;
		if ($id != null) {
			$courrier = $this->read($id);
			if (!empty($courrier) && !empty($courrier['Courrier']['ref_ancien'])) {
				$return = true;
			}
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getDuplicated($id = null) {
		$return = null;
		if ($id != null && $this->isRefused($id) ) {
			$return = $this->find('first', array('conditions' => array('ref_ancien' => $id)));
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getOriginal($id = null) {
		$return = null;
		if ($id != null) {
			$courrier = $this->read($id);
			$return = $this->find('first', array('conditions' => array('id' => $courrier['Courrier']['ref_ancien'])));
		}
		return $return;
	}

	/**
	 * check there is a 'etape' in a 'circuit' correspond a 'flux'
	 * @param type $fluxId
	 */
	public function isEtape($fluxId) {
		$isetape = false;
		$querydata = array(
			'fields' => array(
				'Soustype.circuit_id'
			),
			'contain' => false,
			'joins' => array(
				$this->join($this->Soustype->alias)
			),
			'conditions' => array(
				'Courrier.id' => $fluxId
			)
		);
		$flux = $this->find('first', $querydata);
		$circuit_id = $flux['Soustype']['circuit_id'];
		$etapes = $this->Soustype->Circuit->Etape->find('all', array(
			'fields' => array(
				'Etape.id',
				'Etape.type'
			),
			'conditions' => array(
				'Etape.circuit_id' => $circuit_id
			)
		));
		foreach ($etapes as $etape) {
			if ($etape['Etape']['type'] != 1) {
				if (count($etape['Composition']) < 2) {
					$isetape = false;
				} else {
					$isetape = true;
				}
			} else {
				if (count($etape['Composition']) < 1) {
					$isetape = false;
				} else {
					$isetape = true;
				}
			}
		}
		return $isetape;
	}

	/**
	 * Checks if the Flux is outaded or not
	 *
	 * @param integer $id The id of a Flux
	 * @return boolean if true the Flux is outdated
	 */
	public function isInTime($id = null) {
		$return = true;
		if ($id != null) {

			$this->id = $id;
			$courrier = $this->find(
				'first', array(
					'conditions' => array(
						'Courrier.id' => $id
					),
					'contain' => false
				)
			);
			if (!empty($courrier['Courrier']['soustype_id'])) {

				$soustype = $this->Soustype->find(
					'first', array(
						'conditions' => array(
							'Soustype.id' => $courrier['Courrier']['soustype_id']
						),
						'contain' => false
					)
				);

				if (!empty($soustype['Soustype']['delai_nb']) && !empty($soustype['Soustype']['delai_unite'])) {

					//recuperation de la date du jour
					$date_act = mktime();

					//calcul de la date limite
					$date_courrier_tmp = explode("-", $courrier['Courrier']['datereception']);
					$date_courrier = mktime(0, 0, 0, $date_courrier_tmp[1], $date_courrier_tmp[2], $date_courrier_tmp[0]);

					if ($soustype['Soustype']['delai_unite'] == 0) { // 0 : jour
						$thePHPDate = getdate($date_courrier);
						$thePHPDate['mday'] = $thePHPDate['mday'] + $soustype['Soustype']['delai_nb'];
						$date_valid = mktime($thePHPDate['hours'], $thePHPDate['minutes'], $thePHPDate['seconds'], $thePHPDate['mon'], $thePHPDate['mday'], $thePHPDate['year']); // Convert back to timestamp
					} else if ($soustype['Soustype']['delai_unite'] == 1) { // 1 : semaine
						$thePHPDate = getdate($date_courrier);
						$thePHPDate['mday'] = $thePHPDate['mday'] + ($soustype['Soustype']['delai_nb'] * 7);
						$date_valid = mktime($thePHPDate['hours'], $thePHPDate['minutes'], $thePHPDate['seconds'], $thePHPDate['mon'], $thePHPDate['mday'], $thePHPDate['year']); // Convert back to timestamp
					} else if ($soustype['Soustype']['delai_unite'] == 2) { // 2 : mois
						$thePHPDate = getdate($date_courrier);
						$thePHPDate['mon'] = $thePHPDate['mon'] + $soustype['Soustype']['delai_nb'];
						$date_valid = mktime($thePHPDate['hours'], $thePHPDate['minutes'], $thePHPDate['seconds'], $thePHPDate['mon'], $thePHPDate['mday'], $thePHPDate['year']); // Convert back to timestamp
					}

					//si la date est depassee
					if ($date_valid < $date_act) {
						$return = false;
					}
				}
			}
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function isClosed($id = null) {
		$return = $this->Bancontenu->find('list', array('conditions' => array('Bancontenu.courrier_id' => $id, 'Bancontenu.etat' => 2), 'recursive' => -1));
		return count($return) > 0;
	}

	/**
	 *
	 * @param type $id
	 * @return string
	 */
	public function getDirection($id) {
		$this->id = $id;
		$directionDef = array(
			0 => "S",
			1 => "E",
			2 => "I"
		);
		$direction = $this->field('direction');
		$return = null;
		if (isset($direction) && in_array($direction, array_keys($directionDef))) {
			$return = $directionDef[$direction];
		}
		return $return;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function hasAssociatedFile($id) {
		$associatedFiles = $this->Document->find('first', array(
			'fields' => array('id'),
			'conditions' => array('Document.courrier_id' => $id, 'Document.main_doc' => true),
			'recursive' => -1
		));
		return !empty($associatedFiles);
	}

	/**
	 * Checks integrity of tree table before modification
	 *
	 * @param mixed $data
	 * @return boolean
	 */
	public function checkParadox($data) {
		if (isset($this->data[$this->alias]['id'])) {
			return $data['parent_id'] != $this->data[$this->alias]['id'];
		}
		return true;
	}

	/**
	 * Creates a response to Flux and store reference to original
	 *
	 * @param type $flux
	 * @param type $soustype_id
	 * @return type
	 */
	public function createResponse($flux, $soustype_id) {
		$newFlux = array(
			'Courrier' => array(
				'name' => 'Réponse à ' . $flux['Courrier']['name'],
				'organisme_id' => isset($flux['Courrier']['organisme_id']) ? $flux['Courrier']['organisme_id'] : null,
				'user_creator_id' => $flux['Courrier']['user_creator_id'],
				'desktop_creator_id' => $flux['Courrier']['desktop_creator_id'],
				'soustype_id' => $soustype_id,
				'parent_id' => $flux['Courrier']['id'],
				'reference' => $this->Soustype->getRef($soustype_id),
				'affairesuiviepar_id' => isset($flux['Courrier']['affairesuiviepar_id']) ? $flux['Courrier']['affairesuiviepar_id'] : null,
				'contact_id' => isset($flux['Courrier']['contact_id']) ? $flux['Courrier']['contact_id'] : null,
				'organisme_id' => isset($flux['Courrier']['organisme_id']) ? $flux['Courrier']['organisme_id'] : null,
				'direction' => 0,
				'date' => date('Y-m-d')
			)
		);
		$this->create($newFlux);
		$datas = $this->save();

		if( Configure::read('Webservice.GRC') ) {
			$this->Connecteur = ClassRegistry::init('Connecteur');
			$hasGrcActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%GRC%',
						'Connecteur.use_grc' => true
					),
					'contain' => false
				)
			);
			if( !empty( $hasGrcActif )) {
				$localeo = new LocaleoComponent;
				$newFlux['Courrier']['id'] = $this->id;
				$retour = $localeo->createRequestGRC( $newFlux );
				$result = $retour;
//				$this->Jsonmsg->json['courriergrc_id'] = $result->id;
				if( !empty($result->id) ) {
					$this->updateAll(
						array( 'Courrier.courriergrc_id' => $result->id),
						array('Courrier.id' => $this->id)
					);
				}
			}
		}


		// Remplissage de la métdonnée avec la valeur précédemment renseignée dans leprécédent flux
		$allmetas = $this->CourrierMetadonnee->find(
			'all',
			array(
				'conditions' => array(
					'CourrierMetadonnee.courrier_id' => $flux['Courrier']['id']
				),
				'contain' => array(
					'Metadonnee' => array(
						'Selectvaluemetadonnee'
					)
				),
				'recursive' => -1
			)
		);
		foreach($allmetas as $meta ) {
			if( $meta['Metadonnee']['isrepercutable'] ) {
				$newCourrierMetadonnee = array(
					'CourrierMetadonnee' => array(
						'courrier_id' => $this->id,
						'metadonnee_id' => $meta['Metadonnee']['id'] ,
						'valeur' => $meta['CourrierMetadonnee']['valeur']
					)
				);
				$this->CourrierMetadonnee->create($newCourrierMetadonnee);
				$this->CourrierMetadonnee->save();
			}
		}

		return $datas;
	}

	/**
	 * Creates a copy of a Flux and store reference to refused one
	 *
	 * @param type $flux
	 * @return type
	 */
	public function createRefus($flux) {
// 		$success = true;


		$newFlux = $flux;
		$newFlux['Courrier']['id'] = null;
		$newFlux['Courrier']['ref_ancien'] = $flux['Courrier']['id'];
		$newFlux['Courrier']['desktop_creator_id'] = $flux['Courrier']['desktop_creator_id'];
		$newFlux['Courrier']['parapheur_etat'] = 1;
		$newFlux['Courrier']['parapheur_id'] = null;
		$newFlux['Courrier']['signee'] = null;
		$newFlux['Courrier']['reference'] = $this->genReferenceUnique();
		$this->create($newFlux);
		$data = $this->save();

		$success = !empty($data);
		if ($success) {
			//On récupère les documents liés au courrier nouvellement créé
			if (isset($flux['Document']) && !empty($flux['Document'])) {
				$newDocument['Document'] = $flux['Document'];

				foreach ($newDocument['Document'] as $i => $document) {
					$document['id'] = null;
					$document['courrier_id'] = $this->id;
					$this->Document->create($document);
					$success = $this->Document->save() && $success;
				}
			}
		}

//		return $this->save();
		return ( $success ? $data : false );
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	/* public function genRef($soustype_id) {
      $refInOut = array(
      0 => "S_",
      1 => "E_",
      2 => "I_"
      );
      $compteur = ClassRegistry::init('Compteur');
      $compteur->setDataSource($this->useDbConfig);
      $this->Soustype->setDataSource($this->useDbConfig);

      $stype = $this->Soustype->find('first', array('conditions' => array('Soustype.id' => $soustype_id)));
      return $refInOut[$stype['Soustype']['entrant']] . $compteur->genereCompteur($stype['Soustype']['compteur_id']);
      } */

	/**
	 * Génération auto de la référence unique du courrier
	 * @param type $id
	 * @return string|null
	 */
	public function genReferenceUnique($id = null, $data = null) {
		$referenceunique = null;
		$sql = "SELECT nextval('courriers_reference_seq'); /*" . mktime(true) . " " . $this->_lastSequenceReference . "*/";

		// quel est l'année du dernier courrier référencé ?
		$lastNumReference = $this->lastNumReference();

		if (!empty($id)) {
			$courrier = $this->find('first', array(
				'fields' => array(
					'Courrier.reference',
					'Courrier.soustype_id'
				),
				'conditions' => array(
					'Courrier.id' => $id
				),
				'contain' => array(
					'Soustype'
				)
			));
			$referenceunique = $courrier['Courrier']['reference'];

			$soustypeId = $courrier['Courrier']['soustype_id'];
			if (!empty($data['Courrier']['soustype_id'])) {
				$soustypeId = $data['Courrier']['soustype_id'];
			}

			if (empty($referenceunique)/* && !empty( $soustypeId ) */) {
				// à chaque début d'année, on remet le compteur de référence à zéro
				// si l'année du dernier courrier est != de l'année en cours, on remet à zéro
				if ($lastNumReference != date('Y')) {
					$sqlNewYear = "ALTER SEQUENCE courriers_reference_seq RESTART WITH 1 ;";
					$this->query($sqlNewYear);
				}
				$numSeq = $this->query($sql);

				if ($numSeq === false) {
					return null;
				}
				$referenceunique = date('Y') . sprintf("%06s", $numSeq[0][0]['nextval']);
			}
		} else {

			// à chaque début d'année, on remet le compteur de référence à zéro
			// si l'année du dernier courrier est != de l'année en cours, on remet à zéro
			if ($lastNumReference != date('Y')) {
				$sqlNewYear = "ALTER SEQUENCE courriers_reference_seq RESTART WITH 1 ;";
				$this->query($sqlNewYear);
			}
			$numSeq = $this->query($sql);

			if ($numSeq === false) {
				return null;
			}
			$this->_lastSequenceReference = $numSeq[0][0]['nextval'];
			$referenceunique = date('Y') . sprintf("%06s", $numSeq[0][0]['nextval']);
		}

		return $referenceunique;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function genRef2($data) {
		$id = $data['Courrier']['id'];
		$courrier = $this->find('first', array(
			'conditions' => array(
				'Courrier.id' => $id
			),
			'contain' => false
		));
		return $this->genReferenceUnique($id, $data);
	}

	/**
	 *
	 * @param type $targetId
	 * @return type
	 */
	public function cakeflowIsEnd($targetId) {
		$before_num_traitement = $this->Traitement->field('numero_traitement', array('Traitement.target_id' => $targetId));
		$last_num_traitement_query = 'select max(V.numero_traitement) from wkf_traitements T, wkf_visas V where T.id = V.traitement_id and T.target_id = ' . $targetId . ';';
		$last_num_traitement = $this->Traitement->query($last_num_traitement_query);
		return ($before_num_traitement === $last_num_traitement[0][0]['max']);
	}

	/**
	 *
	 * @param type $fluxId
	 * @return type
	 */
	private function _cakeflowGetInfo($fluxId) {
		return array(
			'ref_traitement' => $this->Traitement->field('numero_traitement', array('Traitement.target_id' => $fluxId)),
			'desktops' => $this->Traitement->whoIsNext($fluxId),
			'typeEtape' => $this->Traitement->typeEtape($fluxId),
			'treated' => $this->Traitement->field('treated', array('Traitement.target_id' => $fluxId)),
		);
	}

	/**
	 *
	 * @param type $fluxId
	 * @param type $desktopId
	 * @return type
	 * @throws NotFoundException
	 * @throws InvalidArgumentException
	 */
	public function cakeflowInsert($fluxId, $desktopOrigId, $insertAuto = 0) {
		$options = [];
		$userId = CakeSession::read('Auth.User.id');
		$querydata = array(
			'fields' => array(
				'Courrier.id',
				'Courrier.desktop_creator_id',
				'Courrier.ref_ancien',
				'Courrier.parent_id',
				'Courrier.priorite',
				'Courrier.date',
				'Courrier.datereception',
				'Courrier.motifrefus',
				'Courrier.soustype_id',
				'Soustype.circuit_id',
				'Soustype.name'
			),
			'contain' => false,
			'joins' => array(
				$this->join($this->Soustype->alias)
			),
			'conditions' => array(
				'Courrier.id' => $fluxId
			)
		);
		$flux = $this->find('first', $querydata);

		if (empty($flux)) {
			throw new NotFoundException();
		}

		//mise a jour de champ desktop_creator_id (utilisé en cas de refus du flux dans un circuit)
		if( $insertAuto == 0 ) {
			$flux['Courrier']['desktop_creator_id'] = $desktopOrigId;
		}
		$this->create();
		$this->save($flux);


		if ($insertAuto == 0) {
			if ($this->Traitement->targetExists($fluxId)) {
				throw new InvalidArgumentException();
			}
		}

		$valid = array(
			'bancontenuEnd' => false,
			'bancontenuAdd' => false,
			'notification' => true
		);

		$circuitId = $flux['Soustype']['circuit_id'];

		$valuefantome = Configure::read('Soustype.Fantome');
		if( !empty($valuefantome) ){
			$soustype = $this->Soustype->find(
				'first', array(
					'conditions' => array(
						'Soustype.id' => $flux['Courrier']['soustype_id']
					),
					'contain' => false
				)
			);
			$soustypeId = $soustype['Soustype']['id'];
			$soustypeName = $soustype['Soustype']['name'];
			$soustypeFantome = false;
			if ($soustypeName == $valuefantome) {
				$soustypeFantome = true;
			}

			if ($soustypeFantome) {
				$valid = array(
					'bancontenuAdd' => true,
					'Notification' => true
				);
				$valid['bancontenuEnd'] = $this->Bancontenu->fullend($fluxId, $userId);
				return !in_array(false, $valid, true);
			}
		}

		if ($this->Traitement->Circuit->insertDansCircuit($circuitId, $fluxId)) {
			$after = $this->_cakeflowGetInfo($fluxId);
			$desktopmanagerId = $this->Desktop->getDesktopManager($desktopOrigId);

			// On récupère le Chef de si on en a 1
			$parentId = array();
			foreach( $desktopmanagerId as $i => $dskMId) {
				$parentId[] = $this->Desktop->Desktopmanager->getParentId($dskMId);
			}
			$desktopsChefIds = array();
			if( !empty($parentId) ) {
				$desktopsChefIds = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
			}

			$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
			foreach ($desktopsIds as $i => $desktopId) {
				// On vérifie si les desktops possèdent bien le flux dans leur bannette avant de les enlever
				$bancontenuWithDesktopIdExist = $this->Bancontenu->find(
					'first',
					array(
						'conditions' => array(
							'Bancontenu.courrier_id' => $fluxId,
							'Bancontenu.desktop_id' => $desktopId,
							'Bancontenu.etat' => 1
						),
						'contain' => false
					)
				);
				if ($insertAuto != 1 && $after['desktops'][0] != -1 && $bancontenuWithDesktopIdExist ) {
					$valid['bancontenuEnd'] = $this->Bancontenu->end($desktopId, BAN_DOCS, $fluxId, $userId);
				}
			}


			// Si le premier bureau est un parapheur, on exécute le traitement pour verser au parpaheur
			if ( in_array( $after['desktops'][0], array( '-1', '-3') ) ){
				$traitement = $this->Traitement->find('first', array(
					'recursive' => -1,
					'fields' => array('id', 'numero_traitement', 'treated', 'circuit_id'),
					'conditions' => array(
						'target_id' => $fluxId,
						'treated' => '0'
					)
				));
				if( $after['desktops'][0] == '-1') {
					$typeConnecteur = 'PARAPHEUR';
				}
				else if( $after['desktops'][0] == '-3') {
					$typeConnecteur = 'PASTELL';
					$this->Bancontenu->add('-3', BAN_FLUX, $fluxId, $userId);
					$this->saveField('pastell_etat', 1);
				}
				$success = $this->Traitement->delegSiBesoin($fluxId, $traitement, $typeConnecteur, $flux);


				$valid['bancontenuAdd'] = $success;
				if ($success !== true) {
					$ret = $success;
					$this->Traitement->delete($traitement['Traitement']['id']);
					return $ret;
				}

				if ($insertAuto != 1) {
					$desktopmanagerId = $this->Desktop->getDesktopManager($desktopOrigId);
					$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
					foreach ($desktopsIds as $i => $desktopId) {
						$valid['bancontenuEnd'] = $this->Bancontenu->end($desktopId, BAN_DOCS, $fluxId, $userId);
					}
				}

				if (!empty($flux['Courrier']['ref_ancien']) || !empty($flux['Courrier']['parent_id']) || !empty($flux['Courrier']['motifrefus'])) {
					$desktopmanagerId = $this->Desktop->getDesktopManager($desktopOrigId);
					$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
					foreach ($desktopsIds as $i => $desktopId) {
						$valid['bancontenuEnd'] = $this->Bancontenu->end($desktopId, BAN_REFUS, $fluxId, $userId);
					}
				}

				$valid['notification'] = true;
			} else {

				$notifyDesktops = array();
				$validBancontenuAdd = array();
				$desktopsIds = array();
				foreach ($after['desktops'] as $i => $desktopmanager) {
					$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
					$desktopsIds[] = $this->Desktopmanager->getDesktops($desktopmanager);
				}


				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						$validBancontenuAdd[] = $this->Bancontenu->add($desktopId[$i], BAN_FLUX, $fluxId, false , $userId);
						$notifyDesktops[$desktopId[$i]] = array($fluxId);

						$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_new');
						$valid['notification'] = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopId[$i], $notifyDesktops);
					}
				}
				// On termine le flux dans la bannette flux refusé à retraiter
				if (!empty($flux['Courrier']['ref_ancien']) || !empty($flux['Courrier']['parent_id']) || !empty($flux['Courrier']['motifrefus'])) {
					$desktopmanagerId = $this->Desktop->getDesktopManager($desktopOrigId);
					$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
					foreach ($desktopsIds as $i => $desktopId) {
						if (!empty($flux['Courrier']['motifrefus'])) {
							$valid['bancontenuEnd'] = $this->Bancontenu->end($desktopId, BAN_REFUS, $fluxId, $userId);
						} else {
							$valid['bancontenuEnd'] = $this->Bancontenu->end($desktopId, BAN_REPONSES, $fluxId, $userId);
						}
					}
				}

				$valid['bancontenuAdd'] = !in_array(false, $validBancontenuAdd, true);
			}
			/*
             *
             */
			$banettesActuelles = $this->Bancontenu->find(
				'all', array(
					'conditions' => array(
						'Bancontenu.courrier_id' => $fluxId,
						'Bancontenu.etat' => 1,
						'Bancontenu.bannette_id' => array(1, 2, 5)
					),
					'contain' => false
				)
			);
			$oldBannettesDesktopsIds = Hash::extract($banettesActuelles, '{n}.Bancontenu.desktop_id');

			// Dans le cas où le même flux est chez plusieurs utilisateurs,
			// on l'enlève des bannetes car il a été traité par l'un d'entre eux
			if (!empty($oldBannettesDesktopsIds)) {
				foreach ($oldBannettesDesktopsIds as $j => $otherDesktopId) {

					$desktops = $this->Desktop->find(
						'all', array(
							'conditions' => array(
								'Desktop.id' => $otherDesktopId
							),
							'contain' => false
						)
					);

					if ($desktops[$j]['Desktop']['profil_id'] == DISP_GID) {
						$BAN = BAN_AIGUILLAGE;
					} else if ($desktops[$j]['Desktop']['profil_id'] == INIT_GID) {
						$BAN = BAN_DOCS;
					}
					if (!empty($flux['Courrier']['ref_ancien']) || !empty($flux['Courrier']['parent_id']) || !empty($flux['Courrier']['motifrefus'])) {
						$BAN = BAN_REFUS;
					}
					if( !empty($BAN) ) {
						$valid['bancontenuEnd'] = $this->Bancontenu->end($otherDesktopId, $BAN, $fluxId, $userId);
					}
				}
			}


			// Envoi du mail au destinataire présent
			$destinataires = $this->Traitement->whoIsNext($fluxId);
			foreach ($destinataires as $destinataire_id) {
				$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
				$desktopsIds = $this->Desktopmanager->getDesktops($destinataire_id);
				foreach ($desktopsIds as $deskId) {
					$user_id = $this->User->userIdByDesktop($deskId);
					$typeNotif = $this->User->find(
						'first', array(
							'fields' => array('User.typeabonnement'),
							'conditions' => array('User.id' => $user_id),
							'contain' => false
						)
					);
					$notificationId = $this->Notifieddesktop->Notification->id;
					if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
						$this->User->saveNotif($fluxId, $user_id, $notificationId, 'traitement');
					} else {
						$this->User->notifier($fluxId, $user_id, $notificationId, 'traitement');
					}
				}
			}
		}
		return !in_array(false, $valid, true);
	}

	/**
	 *
	 * @param type $code
	 * @param type $desktopId
	 * @param type $fluxId
	 * @param type $options
	 * @return type
	 */
	public function cakeflowExecuteSend($code, $desktopId, $fluxId, $options) {
		return $this->cakeflowExecute($code, $desktopId, $fluxId, $options);
	}

	/**
	 *
	 * @param type $desktopId
	 * @param type $fluxId
	 * @param type $response
	 * @param type $stype_id
	 * @return type
	 */
	public function cakeflowExecuteNext($desktopId, $fluxId, $response = 0, $stype_id = null, $insertAuto = 0) {
		return $this->cakeflowExecute('OK', $desktopId, $fluxId, array(), $response, $stype_id, $insertAuto);
	}

	/**
	 *
	 * @param type $code
	 * @param type $desktopId
	 * @param type $fluxId
	 * @param type $options
	 * @param type $response
	 * @param type $stype_id
	 * @return type
	 * @throws BadMethodCallException
	 * @throws InvalidArgumentException
	 */
	public function cakeflowExecute($code, $desktopOrigId, $fluxId, $options = array(), $response = 0, $stype_id = null, $insertAuto = 0) {
		$userId = CakeSession::read('Auth.User.id');
		if (!in_array($code, array('IP', 'IL', 'OK', 'KO')) || $fluxId <= 0 || $desktopOrigId < -3) {
			throw new BadMethodCallException();
		}

		if (!$this->Traitement->targetExists($fluxId)) {
			throw new InvalidArgumentException();
		}

		$valid = array(
			'cakeflow' => false,
			'notification' => false,
			'bancontenu_treated' => false
		);

		$parapheurDesktopId = '';
		if( $desktopOrigId == '-1' ) {
			$parapheurDesktopId = $desktopOrigId;
		}

		$pastellDesktopId = '';
		if( $desktopOrigId == '-3' ) {
			$pastellDesktopId = $desktopOrigId;
		}

		$fluxOrigine = $this->find('first', array(
			'fields' => array(
				'Courrier.id',
				'Courrier.desktop_creator_id',
				'Courrier.reference'
			),
			'conditions' => array(
				'Courrier.id' => $fluxId
			),
			'contain' => false,
			'recursive' => -1
		));

		// -- -- -- -- -- -- --
		// Traitement Cakeflow
		// -- -- -- -- -- -- --
		$desktopsManagersIds = $this->Desktop->getDesktopManager($desktopOrigId);
		// Si on est en admin et qu'on tente de pousser un flux à la palce d'un autre compte
		// on prend l'ID de ce bureau au lieu de celui de l'admin
		$secondaryDesktops = CakeSession::read('Auth.User.SecondaryDesktops');
		if( !empty($secondaryDesktops)) {
			$desktopName = Hash::extract($secondaryDesktops, '{n}.Profil.name');
		}
		else {
			$desktopName = array();
		}
		$isAdmin = false;
		$groupName = CakeSession::read('Auth.User.Desktop.Profil');
		if (in_array('Admin', $desktopName) || !empty($groupName) && $groupName['name'] == 'Admin') {
			$isAdmin = true;
		}
		if( $isAdmin ) {
			$desktopsManagersIds = $this->Traitement->whoIs($fluxId);
		}

		// dans le cas de l'étape collaborative
		$before = $this->_cakeflowGetInfo($fluxId);

		$desktopsNext = $this->Traitement->whoIs($fluxId, 'next');

		if ($before['typeEtape'] == ETAPEET) {
			$desktopsValids = $this->Desktop->getDesktopManager($desktopOrigId);
			$cakeflowReturn = $this->Traitement->execute($code, $desktopsValids, $fluxId, $options);
		}
		else {
			$cakeflowReturn = $this->Traitement->execute($code, $desktopsManagersIds, $fluxId, $options);
		}

		$after = $this->_cakeflowGetInfo($fluxId);

		/*
         *  Ajout suite à modif circuit
         */
		/// FIXME
		$desktopsIds = array();
		if (!empty($after['desktops'])) {
			foreach ($after['desktops'] as $i => $desktopmanager) {
				$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
				$desktopsIds[] = $this->Desktopmanager->getDesktops($desktopmanager);
			}
		} else {
			$desktopsIds[] = $after['desktops'];
		}

		$desktopsExpIds = array();
		if (!empty($before['desktops'])) {
			foreach ($before['desktops'] as $i => $desktops) {
				$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
				$desktopsExpIds[] = $this->Desktopmanager->getDesktops($desktops);
			}
		} else {
			$desktopsExpIds[] = $before['desktops'];
		}
		/*
         * Fin ajout
         */

		$remainingDesktops = array_intersect($before['desktops'], $after['desktops']);
		$diffDesktops = array_diff($before['desktops'], $after['desktops']);

		if ($before['typeEtape'] == ETAPEET) {
			$valid['cakeflow'] = ($before['ref_traitement'] == $after['ref_traitement']) && !empty($diffDesktops);
		} else {
			$valid['cakeflow'] = ($before['ref_traitement'] != $after['ref_traitement']);
		}



		if (is_bool($cakeflowReturn)) {
			$valid['cakeflow'] = true;
		} else {
			$ret = $cakeflowReturn;
			return $ret;
		}

		// -- -- -- -- -- -- -- -- --
		// Notification et Bannettes
		// -- -- -- -- -- -- -- -- --
		//preparation
		$notifyDesktops = array();
		$notifInfos = array(
			'name' => '',
			'description' => '',
			'typeNotif' => ''
		);
		if ($code == 'KO') {
			$desktop = $this->field('Courrier.desktop_creator_id', array('Courrier.id' => $fluxId));
			$notifyDesktops[$desktop] = array($fluxId);
			$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_cancel');
		} else if ($code == 'OK') {
			//--------
			//Etape ET
			//--------
			if ($before['typeEtape'] == ETAPEET) {
				// FIXME: modification ici
				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						$notifyDesktops[$desktopId[$i]] = array($fluxId);
					}
				}

				if (empty($remainingDesktops)) {
					$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_new');
				} else {
					//si en attente
					$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_wait');
				}
				//--------
				//Etape OU
				//--------
			} else if ($before['typeEtape'] == ETAPEOU) {
				// FIXME: modification ici
				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						$notifyDesktops[$desktopId[$i]] = array($fluxId);
					}
				}
				$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_new');
				//------------
				//Etape Simple
				//------------
			} else if ($before['typeEtape'] == ETAPESIMPLE) {//Etape simple
				$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_new');
				// FIXME: modification ici
				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						$notifyDesktops[$desktopId[$i]] = array($fluxId);
					}
				}
			}
			//----------------
			//Rebond et Retour
			//----------------
		} else if ($code == 'IL' || $code == 'IP') {
			$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_jmp');
			if ($code == 'IL') {
				$notifInfos = $this->Notifieddesktop->Notification->setNotification('wkf_jbk');
			}
			foreach ($desktopsIds as $i => $desktopId) {
				for ($i = 0; $i < count($desktopId); $i++) {
					$notifyDesktops[$desktopId[$i]] = array($fluxId);
				}
			}
		} else {
			throw new BadMethodCallException();
		}

		// Cas de l'envoi à Pastell en dernière étape (notamment)
		if( $code = 'OK' && empty($after['desktops'])) {
			$after['treated'] = true;
		}
		// -- -- -- --
		// Traitement
		// -- -- -- --
		//si le flux est traité
		if ($after['treated'] && $code == 'OK') {
			$this->Bancontenu->fullend($fluxId, $userId);
			//reponse
			if ($response != 0) {
				$flux = $this->find('first', array('conditions' => array('Courrier.id' => $fluxId), 'contain ' => false));
				$newFlux = $this->createResponse($flux, $stype_id);
				$valid['response_copy'] = false;
				if (!empty($newFlux)) {
					$valid['response_copy'] = true;
					if ($insertAuto == 0) {
						$valid['bancontenu_treated'] = $valid['bancontenu'] = $this->Bancontenu->add($newFlux['Courrier']['desktop_creator_id'], BAN_REPONSES, $newFlux['Courrier']['id'], false , $userId);
						$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_response');
						$notifyDesktops = array($newFlux['Courrier']['desktop_creator_id'] => array($newFlux['Courrier']['id']));
						if (!empty($desktopsExpIds)) {
							foreach ($desktopsExpIds as $k => $deskId) {
								for ($i = 0; $i < count($deskId); $i++) {

									$valid['notification'] = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $deskId[$i], $notifyDesktops);
								}
							}
						} else {
							$valid['notification'] = true;
						}

						// Flux à insérer
						$user_id = $this->User->userIdByDesktop($newFlux['Courrier']['desktop_creator_id']);
						$typeNotif = $this->User->find(
							'first', array(
								'fields' => array('User.typeabonnement'),
								'conditions' => array('User.id' => $user_id),
								'contain' => false
							)
						);
						$notificationId = $this->Notifieddesktop->Notification->id;
						if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
							$this->User->saveNotif($newFlux['Courrier']['id'], $user_id, $notificationId, 'insertion_reponse');
						} else {
							$this->User->notifier($newFlux['Courrier']['id'], $user_id, $notificationId, 'insertion_reponse');
						}
					} else {
						$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_response');
						$notifyDesktops = array($newFlux['Courrier']['desktop_creator_id'] => array($newFlux['Courrier']['id']));
						$valid['bancontenu_treated'] = true;

						if (!empty($desktopsExpIds)) {
							foreach ($desktopsExpIds as $k => $deskId) {
								for ($i = 0; $i < count($deskId); $i++) {
									$this->cakeflowInsert($newFlux['Courrier']['id'], $deskId[$i], $insertAuto);
									$valid['notification'] = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $deskId[$i], $notifyDesktops);
								}
							}
						}

						$destinataires = $this->Traitement->whoIsNext($newFlux['Courrier']['id']);
						foreach ($destinataires as $destinataire_id) {
							$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
							$desktopsToSendIds = $this->Desktopmanager->getDesktops($destinataire_id);
							foreach ($desktopsToSendIds as $deskId) {
								if ($deskId != -1) {
									$user_id = $this->User->userIdByDesktop($deskId);
									if (!empty($user_id)) {
										$typeNotif = $this->User->find(
											'first', array(
												'fields' => array('User.typeabonnement'),
												'conditions' => array('User.id' => $user_id),
												'contain' => false
											)
										);
										$notificationId = $this->Notifieddesktop->Notification->id;
										if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
											$this->User->saveNotif($fluxId, $user_id, $notificationId, 'traitement');
										} else {
											$this->User->notifier($fluxId, $user_id, $notificationId, 'traitement');
										}
									}
								}
							}
						}
					}
				}
			} else {
				$valid['notification'] = true;
				$valid['bancontenu_treated'] = true;
				// Flux traité
			}
		} else {
			//Dans le cas d une etape de type ou, il faut marquer comme lues les notifications de tous les utilisateurs des bureaux de l'etape validée.
			//Il faut aussi changer l'etat de bancontenu de la meme maniere
			/*if ($before['typeEtape'] == ETAPEOU) {
				$validEtapeOuNotif = array();
				$validEtapeOuBan = array();
				foreach ($before['desktops'] as $prevDesktop) {
					$desktopsEtapeOuIds = $this->Desktopmanager->getDesktops($prevDesktop);
					foreach( $desktopsEtapeOuIds as $k => $deskOuId ) {
						$valid['readnotification'] = $this->Notifieddesktop->Notification->setReadByDesktopCourrier($deskOuId, $fluxId);
						$valid['bancontenu_treated'] = $this->Bancontenu->end($deskOuId, BAN_FLUX, $fluxId, $userId);
					}
				}
				$valid['readnotification'] = !in_array(false, $validEtapeOuNotif, true);
				$valid['bancontenu_treated'] = !in_array(false, $validEtapeOuBan, true);
			} else {*/ //sinon dans tous les autres cas, on marque comme lue la notification de l'utilisateur ayant validé et on change l'état de bancontenu
//$this->log($before);
//$this->log($after);

			$traitement = $this->Traitement->find('first', array('conditions' => array('Traitement.target_id' => $fluxId), 'contain' => false));
			$traitement_id = $traitement['Traitement']['id'];
			$visas = $this->Traitement->Visa->find(
				'all',
				array(
					'conditions' => array(
						'Visa.traitement_id' => $traitement_id,
					),
					'contain' => false,
					'recursive' => -1,
					'order' => 'Visa.numero_traitement ASC'
				)
			);

			if($before['typeEtape'] == ETAPEET) {
				if( !empty($desktopOrigId) ){
					$triggersIDs = Hash::extract( $visas, '{n}.Visa.trigger_id');
					$desktopsTests = $this->Desktop->getDesktopManager($desktopOrigId);
					$desktopsEtapeEtIds = $this->Desktopmanager->getDesktops($desktopsTests[0]);
					if( isset($desktopsTests[1]) &&  in_array( $desktopsTests[1], $triggersIDs ) ){
						$desktopsEtapeEtIds[] = $this->Desktopmanager->getDesktops($desktopsTests[1]);
					}
					if( isset($desktopsTests[2]) &&  in_array( $desktopsTests[2], $triggersIDs ) ){
						$desktopsEtapeEtIds[] = $this->Desktopmanager->getDesktops($desktopsTests[2]);
					}

					foreach( $desktopsEtapeEtIds as $k => $deskEtId ) {
						$valid['readnotification'] = $this->Notifieddesktop->Notification->setReadByDesktopCourrier($deskEtId, $fluxId);
						$valid['bancontenu_treated'] = $this->Bancontenu->end($deskEtId, BAN_FLUX, $fluxId, $userId);
					}
				}
			}

			if($before['typeEtape'] == ETAPESIMPLE || $before['typeEtape'] == ETAPEOU) {
				foreach ($before['desktops'] as $beforeDesktop) {
					$desktopsEtapeSimpleIds = $this->Desktopmanager->getDesktops($beforeDesktop);
					foreach( $desktopsEtapeSimpleIds as $k => $deskSimpleId ) {
						$valid['readnotification'] = $this->Notifieddesktop->Notification->setReadByDesktopCourrier($deskSimpleId, $fluxId);
						$valid['bancontenu_treated'] = $this->Bancontenu->end($deskSimpleId, BAN_FLUX, $fluxId, $userId);
					}
				}
			}

			if (!empty($desktopsExpIds)) {
				foreach ($desktopsExpIds as $k => $deskId) {

					for ($i = 0; $i < count($deskId); $i++) {
						$deskInBancontenu[$i] = $this->Bancontenu->find(
							'first', array(
								'conditions' => array(
									'Bancontenu.desktop_id' => $deskId[$i],
									'Bancontenu.bannette_id' => BAN_FLUX,
									'Bancontenu.courrier_id' => $fluxId,
									'Bancontenu.etat' => 0
								),
								'contain' => false
							)
						);

						if (!empty($deskInBancontenu[$i])) {
							$valid['readnotification'] = $this->Notifieddesktop->Notification->setReadByDesktopCourrier($deskId[$i], $fluxId);
							$valid['bancontenu_treated'] = $this->Bancontenu->end($deskId[$i], BAN_FLUX, $fluxId, $userId);
						} else {
							$valid['readnotification'] = $this->Notifieddesktop->Notification->setReadByDesktopCourrier($deskId[$i], $fluxId);
							$valid['bancontenu_treated'] = true;
						}
					}
				}
			}

			if( !empty($parapheurDesktopId ) && $parapheurDesktopId == '-1' ) {
				$deskParapheurInBancontenu = $this->Bancontenu->find(
					'first',
					array(
						'conditions' => array(
							'Bancontenu.desktop_id' => $parapheurDesktopId,
							'Bancontenu.bannette_id' => BAN_FLUX,
							'Bancontenu.courrier_id' => $fluxId
						),
						'contain' => false
					)
				);

				if (!empty($deskParapheurInBancontenu)) {
					$valid['readnotification'] = true;
					$valid['bancontenu_treated'] = $this->Bancontenu->end($parapheurDesktopId, BAN_FLUX, $fluxId, $userId);
				} else {
					$valid['readnotification'] = true;
					$valid['bancontenu_treated'] = true;
				}
			}

			if( !empty($pastellDesktopId ) && $pastellDesktopId == '-3' ) {
				$deskPastellInBancontenu = $this->Bancontenu->find(
					'first',
					array(
						'conditions' => array(
							'Bancontenu.desktop_id' => $pastellDesktopId,
							'Bancontenu.bannette_id' => BAN_FLUX,
							'Bancontenu.courrier_id' => $fluxId
						),
						'contain' => false
					)
				);

				if (!empty($deskPastellInBancontenu)) {
					$valid['readnotification'] = true;
					$valid['bancontenu_treated'] = $this->Bancontenu->end($pastellDesktopId, BAN_FLUX, $fluxId, $userId);
				} else {
					$valid['readnotification'] = true;
					$valid['bancontenu_treated'] = true;
				}
			}

			if (!empty($desktopsExpIds)) {
				foreach ($desktopsExpIds as $k => $deskId) {
					for ($i = 0; $i < count($deskId); $i++) {
						if ($this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $deskId[$i], $notifyDesktops)) {
							$valid['notification'] = true;
						}
					}
				}
			} else {
				$valid['notification'] = true;
			}
			//On met à jour les contenus de bannette pour les bureaux de l etape suivante
			//Si on est à une étape de type ET et que tous les bureaux ont validé ou si etape autre que ET
			if (($before['typeEtape'] == ETAPEET && $before['ref_traitement'] != $after['ref_traitement']) || $before['typeEtape'] != ETAPEET ) {
				$validEtapeEtBan = array();
				// utiliser ici pour ajouter le flux dans la bannette du bureau ajouté par le module Envoyer à
				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						if ($desktopId[$i] != -1) {
							if( $after['treated'] && $after['typeEtape'] == ETAPEOU ) {
								$this->Bancontenu->fullend($fluxId, $userId);
							}
							else {
								$validEtapeEtBan[] = $this->Bancontenu->add($desktopId[$i], BAN_FLUX, $fluxId, false, $userId);
							}
						} else {
							$validEtapeEtBan[] = true;
							$valid['notification'] = true;
						}
					}
				}
				$valid['bancontenu'] = !in_array(false, $validEtapeEtBan, true);
			}


			// Ajout pour les notifs durant le traitement
			$destinataires = $this->Traitement->whoIsNext($fluxId);
			foreach ($destinataires as $destinataire_id) {
				$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
				$desktopsToSendIds = $this->Desktopmanager->getDesktops($destinataire_id);
				foreach ($desktopsToSendIds as $deskId) {
					if ($deskId != -1) {
						$user_id = $this->User->userIdByDesktop($deskId);
						if (!empty($user_id)) {
							$typeNotif = $this->User->find(
								'first', array(
									'fields' => array('User.typeabonnement'),
									'conditions' => array('User.id' => $user_id),
									'contain' => false
								)
							);
							$notificationId = $this->Notifieddesktop->Notification->id;
							if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
								$this->User->saveNotif($fluxId, $user_id, $notificationId, 'traitement');
							} else {
								$this->User->notifier($fluxId, $user_id, $notificationId, 'traitement');
							}
						}
					}
				}
			}
		}
		// En fin de traitement, on met à jour le bancontenu de l'agent du bureau ayant réelleent validé le flux
		if( $valid['bancontenu_treated'] == true || $valid['bancontenu_treated'] == 1 ) {
			$this->Bancontenu->updateAll(
				array(
					'Bancontenu.modified' => "'".date( 'Y-m-d H:i:s')."'"
				),
				array(
					'Bancontenu.courrier_id' => $fluxId,
					'Bancontenu.desktop_id' =>  $desktopOrigId
				)
			);
		}
		$this->log(CakeSession::read('Auth.User.username') );
		$this->log($fluxOrigine['Courrier']['id'], LOG_ERROR);
		$this->log($fluxOrigine['Courrier']['reference'], LOG_ERROR);
		return !in_array(false, $valid, true);
	}

	/**
	 *
	 * @param type $desktopId
	 * @param type $fluxId
	 * @param type $destId
	 * @return type
	 */
	public function copy($desktopId, $fluxId, $destId) {
		$userId = CakeSession::read('Auth.User.id');
		$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_copy');

		$desktopmanagerId = $destId;
		$desktopsIds[] = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
		foreach ($desktopsIds as $i => $dId) {

			for ($i = 0; $i < count($dId); $i++) {

				// On regarde si le flux est déjà en copie
				$allbancontenusCopyForFluxId = $this->Bancontenu->find(
					'all',
					array(
						'conditions' => array(
							'Bancontenu.desktop_id' => $dId[$i],
							'Bancontenu.bannette_id' => BAN_COPY,
							'Bancontenu.courrier_id' => $fluxId
						),
						'contain' => false,
						'recursive' => -1
					)
				);
				// si les bannettes ne sont pas vides, cela signifie que le flux a déjà été adressé en copie
				if( empty($allbancontenusCopyForFluxId) ) {
					$validBancontenuAdd[] = $this->Bancontenu->add($dId[$i], BAN_COPY, $fluxId, false , $userId);
					$notifyDesktops[$dId[$i]] = array($fluxId);
				}
			}
		}

		$valid['bancontenu'] = !in_array(false, $validBancontenuAdd, true);
		$valid['notification'] = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopId, $notifyDesktops);
		return !in_array(false, $valid, true);
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getLightMainDoc($id) {
		$conn = CakeSession::read('Auth.Collectivite.conn');
		$qdMainDoc = array(
			'recursive' => -1,
			'fields' => array(
				'Document.id',
				'Document.courrier_id',
				'Document.name',
				'Document.ext',
				'Document.size',
				'Document.mime',
				'Document.main_doc',
				'Document.desktop_creator_id',
				'Document.ocr_data'
			),
			'conditions' => array(
				'Document.courrier_id' => $id,
				'Document.main_doc' => true
			),
			'contain' => false
		);

		$document = $this->Document->find('first', $qdMainDoc);
		if (!empty($document)) {
			$document['Document']['isgenerated'] = false;
			if (is_dir(WEBDAV_DIR . DS . $conn . DS .$id)) {
				if ($document['Document']['ext'] === 'odt') {
					$document['Document']['isgenerated'] = true;
				}
			}
		} else {
			$document = array();
		}

		return $document;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getMainDoc($id) {
		$conn = CakeSession::read('Auth.Collectivite.conn');
		$qdMainDoc = array(
			'recursive' => -1,
			'fields' => array(
				'Document.id',
				'Document.courrier_id',
				'Document.name',
				'Document.ext',
				'Document.content',
				'Document.gedpath',
				'Document.size',
				'Document.mime',
				'Document.main_doc',
				'Document.desktop_creator_id',
				'Document.path'
			),
			'conditions' => array(
				'Document.courrier_id' => $id,
				'Document.main_doc' => true
			),
			'contain' => false
		);

		$document = $this->Document->find('first', $qdMainDoc);
		if (!empty($document)) {
			$document['Document']['isgenerated'] = false;
			if (is_dir(WEBDAV_DIR . DS . $conn . DS .$id)) {
				if ($document['Document']['ext'] === 'odt') {
					$document['Document']['isgenerated'] = true;
				}
			}
		} else {
			$document = array();
		}
		return $document;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getDocs($id) {
		$qdMainDoc = array(
			'recursive' => -1,
			'fields' => $this->Document->getLightFields(),
			'conditions' => array(
				'Document.courrier_id' => $id,
				'Document.main_doc' => false
			)
		);
		return $this->Document->find('all', $qdMainDoc);
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getDocsSignes($id) {
		$qdDocSigne = array(
			'recursive' => -1,
			'fields' => $this->Document->getLightFields(),
			'conditions' => array(
				'Document.courrier_id' => $id,
				'Document.signee' => true
			)
		);
		$docssignes = $this->Document->find('all', $qdDocSigne);

		if( !empty($docssignes) ) {
			foreach( $docssignes as $i => $doc ) {
				$docssignes[$i]['Document']['isgenerated'] = false;
			}
		}
		return $docssignes;
	}

	/**
	 * Génération d'un document lié (à partir du script lancé à la main)
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileAssoc($fichier_associe, $fluxId, &$return, $main_doc = true, $filename, $conn ) {
		if( empty($conn) ) {
			$conn = CakeSession::read('Auth.Collectivite.conn');
		}
		$finfo = new finfo(FILEINFO_MIME_TYPE);

		$fileMimeType = $finfo->buffer($fichier_associe);

		$sql = "SELECT nextval('documents_name_seq'); /*" . mktime(true) . " " . $this->_lastSequenceDocument . "*/";



		$fileMimeTypeError = false;
		if ($fileMimeType == "application/pdf" || $fileMimeType == "PDF") {
			$fileExt = "pdf";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.text" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.TEXT" || $fileMimeType == "OCTET-STREAM") {
			$fileExt = "odt";
		} else if ($fileMimeType == "image/bmp") {
			$fileExt = "bmp";
		} else if ($fileMimeType == "image/tiff") {
			$fileExt = "tif";
		} else if ($fileMimeType == "text/plain") {
			$fileExt = "txt";
		} else if ($fileMimeType == "application/vnd.ms-office") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/msword") {
			$fileExt = "doc";
		} else if ($fileMimeType == "application/rtf") {
			$fileExt = "rtf";
		} else if ($fileMimeType == "image/png" || $fileMimeType == "PNG") {
			$fileExt = "png";
		} else if ($fileMimeType == "image/jpeg" || $fileMimeType == "JPEG") {
			$fileExt = "jpg";
		} else if ($fileMimeType == "application/vnd.oasis.opendocument.spreadsheet" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.SPREADSHEET") {
			$fileExt = "ods";
		} else if ($fileMimeType == "VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT" || $fileMimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
			$fileExt = "docx";
		} else if ($fileMimeType == "GIF" || $fileMimeType == "gif") {
			$fileExt = "gif";
		} else if ($fileMimeType == '') {
			$fileMimeType = 'txt';
			$fileExt = "rtf";
		} else {
			$fileMimeTypeError = true;
		}


		if ($fileMimeTypeError) {
			$return['CodeRetour'] .= "FILENOK";
			$return['Message'] .= ". Le fichier n'est pas reconnu";
		} else {

			$numSeqDoc = $this->query($sql);
			if ($numSeqDoc === false) {
				return null; // FIXME: Throw exception
			}
			$this->_lastSequenceDocument = $numSeqDoc[0][0]['nextval'];
//            $filename = date('Y-m-d') . "_fichier_" . sprintf("%06s", $numSeqDoc[0][0]['nextval']);
//            $filename .= "." . $fileExt;

			// On stocke sur le disque désormais
			$courrier = $this->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.id' => $fluxId
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$filename = str_replace( ' ', '_', $filename );
			$filename  = preg_replace( "/[:'()]/", "_", $filename );
			$filename = str_replace( '/', '_', $filename );
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$filename}";
			file_put_contents( $file, $fichier_associe);

			$path = $file;

			$doc = array(
				'Document' => array(
					'name' => $filename,
					'size' => strlen($fichier_associe),
					'mime' => $fileMimeType,
//                    'content' => $fichier_associe,
					'ext' => $fileExt,
					'courrier_id' => $fluxId,
					'main_doc' => $main_doc,
					'path' => $path
				)
			);
			$this->setDataSource($conn);
			$this->Document->setDataSource($conn);
			$this->Document->create($doc);
			$docsaved = $this->Document->save();

			$docId = $this->Document->id;
			// LAD / RAD : conversion + OCR
			if( Configure::read('Ocerisation.active') ) {
				if( !empty($fluxId) ) {
					$courrier = $this->find(
						'first',
						array(
							'conditions' => array('Courrier.id' => $fluxId),
							'contain' => false,
							'recursive' => -1
						)
					);

					$this->_convertPdfToPng($filename, $courrier, $conn);
				}
				if( !empty($docId) ) {
					$folderPath= WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'];
					$folderToLook = new Folder($folderPath);
					$txtFiles = $folderToLook->find( '.*\.txt', true);
					foreach ($txtFiles as $txtFile) {
						$dataFile = file_get_contents( WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference']  . DS . $txtFile);

						$this->Document->updateAll(
							array('Document.ocr_data' => "'" . pg_escape_bytea($dataFile) . "'"),
							array(
								'Document.id' => $docId,
								'Document.courrier_id' => $fluxId
							)
						);

						$this->_deleteTesseractTxtFiles($txtFile, $courrier, $conn);


						// Extraction de(s) l'email(s) s'il(s) existe(nt) dans l'ocr_data
						$pattern = '/([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';
						preg_match_all($pattern, $dataFile, $matches);
						$emailDetected = $matches[0];

						// Extraction de(s) n° de téléphone s'il(s) existe(nt) dans l'ocr_data
						preg_match_all('/\b[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\b/',$dataFile,$number);
						$numberTelDetected = $number;

						// On met le nom du courrier à jour avec la valeur présente dans le doc océrisé
						preg_match('/Objet :(.*)/', $dataFile, $objetvalue);
						preg_match('/Tel:(.*)/', $dataFile, $telvalue);
						preg_match('/Tél :(.*)/', $dataFile, $telvalue2);
						preg_match('/Mail :(.*)/', $dataFile, $mailvalue);
						$objetvalueDetected = '';
						$telvalueDetected = '';
						$mailvalueDetected = '';
						if( !empty($objetvalue[1]) ){
							$objetvalueDetected = $objetvalue[1];
						}
						if( !empty($telvalue[1]) ){
							$telvalueDetected = $telvalue[1];
						}
						if( !empty($mailvalue[1]) ){
							$mailvalueDetected = $mailvalue[1];
						}

						if( isset($objetvalueDetected) && !empty($objetvalueDetected) ) {
							$this->updateAll(
								array(
									'Courrier.name' => "'" . $objetvalueDetected . "'"
								),
								array(
									'Courrier.id' => $fluxId
								)
							);
						}
					}
				}
			}




			if (empty($docsaved)) {
				$return['CodeRetour'] .= "NOFILE";
				$return['Message'] .= ".Le document joint n'a pas été envoyé";
				$return['DocumentId'] .= null;
			} else {
				$return['CodeRetour'] .= "FILEOK";
				$return['Message'] .= ". Le document joint a été envoyé";
				$return['DocumentId'] .= $this->Document->id;
			}
		}
	}

	/**
	 * Fonctionnalité de numérisation de masse (création de flux)
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant de la collectivité
	 * @param base64 $fichier_associe contenu du fichier lié
	 * @param string $username identifiant de connexion de l'utilisateur cible
	 * @return array
	 */
	public function remoteCreate($fichier_associe, $username, $dispDesktop = true, $desktopId = null, $filename, $conn, $fluxName = null) {
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Message' => 'Impossible de créer le flux.',
			'Severite' => 'important'
		);
		$userId = CakeSession::read('Auth.User.id');

		$noRoleErrMsg = "Le rôle n'existe pas.";

		if (empty($desktopId)) {
			$GID = $dispDesktop ? DISP_GID : INIT_GID;
			$BAN = $dispDesktop ? BAN_AIGUILLAGE : BAN_DOCS;
			$noRoleErrMsg = $dispDesktop ? "L'utilisateur n'a pas de rôle Aiguilleur." : "L'utilisateur n'a pas de rôle Initiateur.";

			$querydata = array(
				'conditions' => array(
					'User.username' => $username
				),
				'contain' => array(
					$this->Bancontenu->Desktop->alias => array(
						$this->Bancontenu->Desktop->Profil->alias
					),
					$this->Bancontenu->SecondaryDesktop->alias => array(
						$this->Bancontenu->SecondaryDesktop->Profil->alias
					),
				)
			);
			$user = $this->User->find('first', $querydata);

			if (empty($user)) {
				$return['CodeRetour'] = "KONOUSER";
				$return['Message'] .= " L'utilisateur n'existe pas.";
			} else {
				if ($user['Desktop']['profil_id'] == $GID) {
					$desktopId = $user['Desktop']['id'];
				} else {
					$found = false;
					for ($i = 0; $i < count($user['SecondaryDesktop']); $i++) {
						if ($user['SecondaryDesktop'][$i]['profil_id'] == $GID && !$found) {
							$desktopId = $user['SecondaryDesktop'][$i]['id'];
							$found = true;
						}
					}
				}
			}
		} else {
			$desktop = $this->Bancontenu->Desktop->find('first', array('conditions' => array('Desktop.id' => $desktopId)));
			if ($desktop['Desktop']['profil_id'] == DISP_GID) {
				$BAN = BAN_AIGUILLAGE;
			} else if ($desktop['Desktop']['profil_id'] == INIT_GID) {
				$BAN = BAN_DOCS;
			}
		}


		if (empty($desktopId)) {
			$return['CodeRetour'] = "KONODESKTOP";
			$return['Message'] .= $noRoleErrMsg;
		} else if (empty($BAN)) {
			$return['CodeRetour'] = "KONOBAN";
			$return['Message'] .= 'Aucun bannette renseignée';
		} else {
			if (!is_array($filename)) {
				$pos_point = strpos($filename, '.');
				$courrierName = substr($filename, 0, $pos_point);
			}
			$referenceUnique = $this->genReferenceUnique();

			$userConnected = !empty($desktop['User'][0]['id']) ? $desktop['User'][0]['id'] : null;
			if( Configure::read( 'Scan.UserCreatorEmpty') ) {
				$userConnected = null;
			}

			if ($fluxName != null) {
				$data = array(
					'Courrier' => array(
						'name' => $fluxName . '_' . date('Y-m-d_H:i:s'),
						'reference' => $referenceUnique,
						'user_creator_id' => $userConnected,
						'desktop_creator_id' => !empty($desktop['Desktop']['id']) ? $desktop['Desktop']['id'] : null
					)
				);
			} else {
				$data = array(
					'Courrier' => array(
						'name' => $courrierName . '_' . date('Y-m-d_H:i:s'),
						'reference' => $referenceUnique,
						'user_creator_id' => $userConnected,
						'desktop_creator_id' => !empty($desktop['Desktop']['id']) ? $desktop['Desktop']['id'] : null
					)
				);
			}
			$this->begin();
			$this->create($data);
			if ($this->save()) {
				$courrierId = $this->id;
				$return['CodeRetour'] = "CREATED";
				$return['Message'] = "Le flux a été créé";
				$return['Severite'] = "normal";
				$return['CourrierId'] = $courrierId;

				$this->commit();

				//ajout dans la bannette de l aiguilleur
				$this->Bancontenu->add($desktopId, $BAN, $courrierId, false , $userId);
				if (is_array($fichier_associe)) {
					for ($i = 0; $i < count($fichier_associe); $i++) {
						if ($i != 0) {
							$this->_genFileAssoc($fichier_associe[$i], $courrierId, $return, false, $filename[$i], $conn);
						} else {
							$this->_genFileAssoc($fichier_associe[$i], $courrierId, $return, true, $filename[$i], $conn);
						}
					}

					$codeRetour = str_split($return['CodeRetour'], 13);
					$return['CodeRetour'] = $codeRetour[0];
				} else {
					$this->_genFileAssoc($fichier_associe, $courrierId, $return, true, $filename, $conn);
				}
				// Notification par mail de la génération du flux
				$user_id = $this->User->userIdByDesktop($desktopId);
				$typeNotif = $this->User->find(
					'first', array(
						'fields' => array('User.typeabonnement'),
						'conditions' => array('User.id' => $user_id),
						'contain' => false
					)
				);

				$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_xnew');
				for ($d = 0; $d < count($desktopId); $d++) {
					$notifyDesktops[$desktopId[$d]] = array($courrierId);
					$notified = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopId, $notifyDesktops);
				}
				$notificationId = $this->Notifieddesktop->Notification->id;
				if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
					$this->User->saveNotif($courrierId, $user_id, $notificationId, 'aiguillage');
				} else {
					$this->User->notifier($courrierId, $user_id, $notificationId, 'aiguillage');
				}
			} else {
				$this->rollback();
			}
		}
		return $return;
	}

	/**
	 * Construit le tableau à envoyer au parapheur
	 * @param $acte_id
	 * @return array
	 */
	public function getDocumentsForDelegation($courrier_id) {
		$docs = array(
			'docPrincipale' => $this->getMainDoc($courrier_id),
			'docSupp' => array(),
			'annexes' => array()
		);

		$conn = CakeSession::read('Auth.Collectivite.conn');

		$courrier = $this->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $courrier_id
				),
				'contain' => false,
				'recursive' => -1
			)
		);

		if( !empty( $courrier['Courrier']['parent_id'] ) ) {
			$parent = $this->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.id' => $courrier['Courrier']['parent_id']
					),
					'contain' => array(
						'Document'
					),
					'recursive' => -1
				)
			);
			foreach ($parent['Document'] as $pjorigine ) {
				if (strpos($pjorigine['name'], 'htm') !== false && $pjorigine['mime'] != 'application/zip' && $pjorigine['mime'] != 'application/x-octetstream' && $pjorigine['ext'] != 'zip') {

					$pjpath = APP . DS . WEBROOT_DIR . DS . 'files/webdav'.  DS . $conn  . DS . "{$pjorigine['courrier_id']}" . DS . "{$pjorigine['name']}";
					$documentPath = $pjorigine['path'];
					if( !empty( $documentPath ) ) {
						$file = file_get_contents($documentPath );
						$pjpath = $documentPath;
					}
					else {
						if( file_exists( $pjpath) ) {
							$file = file_get_contents($pjpath);
						}
						else {
							$pjpath = APP . DS . WEBROOT_DIR . DS . 'files' . DS . "{$pjorigine['name']}";
							$file = file_put_contents($pjpath, $pjorigine['content']);
						}
					}
					$docs['annexes'][] = array(
						'content' => $file,
						'mimetype' => $pjorigine['mime'],
						'filename' => $pjorigine['name'],
						'ext' => $pjorigine['ext'],
						'path' => $pjpath
					);
				}
			}
		}


		// On envoie le document principal édité via webdav si tel est le cas.
		$path = APP . DS . WEBROOT_DIR . DS . 'files/webdav' . DS . $conn . DS . "{$courrier_id}" . DS . "{$docs['docPrincipale']['Document']['name']}";
		if( file_exists( $path) ) {
			$docs['docPrincipale']['Document']['content'] = file_get_contents($path);
		}
		else {
			if( !empty($docs['docPrincipale']['Document']['path']) ) {
				$docs['docPrincipale']['Document']['content'] = file_get_contents($docs['docPrincipale']['Document']['path']);
			}
		}

		// Si le doc n'est pas un Pdf, on le convertit avant envoi au i-Parapheur
		if( $docs['docPrincipale']['Document']['mime'] == 'application/vnd.oasis.opendocument.text' && $docs['docPrincipale']['Document']['mime'] != 'application/pdf' ) {
			$this->Document->updateAll(
				array( 'Document.main_doc' => false ),
				array( 'Document.id' => $docs['docPrincipale']['Document']['id'])
			);

			App::uses('ConversionComponent', 'Controller/Component');
			$this->Conversion = new ConversionComponent;
			$docInPdfContent = $this->Conversion->convertirFlux($docs['docPrincipale']['Document']['content'], 'odt', 'pdf');
			$name = substr( $docs['docPrincipale']['Document']['name'], 0, -4);
			$docs['docPrincipale']['Document']['name'] = $name.'.pdf';
			$docs['docPrincipale']['Document']['content'] = $docInPdfContent;
			$docs['docPrincipale']['Document']['mime'] = 'application/pdf';
			$docs['docPrincipale']['Document']['ext'] = 'pdf';


			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}.pdf";
			file_put_contents( $file, $docInPdfContent);
			$pathForDocToSign = $file;

			$docs['docPrincipale']['Document']['path'] = $pathForDocToSign;

			$documentPdfToSign = array(
				'Document' => array(
					'name' => $name.'.pdf',
					'ext' => $docs['docPrincipale']['Document']['ext'],
					'size' => strlen($docs['docPrincipale']['Document']['content']),
					'main_doc' => true,
					'courrier_id' => $docs['docPrincipale']['Document']['courrier_id'],
					'desktop_creator_id' => $docs['docPrincipale']['Document']['desktop_creator_id'],
					'path' => $pathForDocToSign,
					'mime' => $docs['docPrincipale']['Document']['mime']
				)
			);

			$this->Document->create($documentPdfToSign);
			$this->Document->save();
		}



		foreach ($this->Document->getPiecesJointes($courrier_id) as $piecejointe) {

			if (strpos($piecejointe['Document']['filename'], 'htm') !== false && $piecejointe['Document']['mime'] != 'application/zip' && $piecejointe['Document']['mime'] != 'application/x-octetstream' && !$piecejointe['Document']['asigner'] && $piecejointe['Document']['ext'] != 'zip') {
				$annexespath = APP . DS . WEBROOT_DIR . DS . 'files/webdav' . DS . $conn  . DS . "{$piecejointe['Document']['courrier_id']}" . DS . "{$piecejointe['Document']['name']}";
				$documentAnnexePath = $piecejointe['Document']['path'];
				if( !empty( $documentAnnexePath ) ) {
					$file = file_get_contents($documentAnnexePath );
					$annexespath = $documentAnnexePath;
				}
				else {
					if( file_exists( $annexespath) ) {
						$file = file_get_contents($annexespath);
					}
					else {
						$annexespath = APP . DS . WEBROOT_DIR . DS . 'files' . DS . "{$piecejointe['Document']['name']}";
						$file = file_put_contents($annexespath, $piecejointe['Document']['content']);
					}
				}
				$docs['annexes'][] = array(
					'content' => $file,
					'mimetype' => $piecejointe['Document']['mime'],
					'filename' => $piecejointe['Document']['name'],
					'ext' => $piecejointe['Document']['ext'],
					'path' => $annexespath
				);
			}

			if($piecejointe['Document']['asigner'] && $piecejointe['Document']['mime'] != 'application/zip'  && $piecejointe['Document']['ext'] != 'zip') {
				$suppath = APP . DS . WEBROOT_DIR . DS . 'files/webdav' . DS . $conn . DS . "{$piecejointe['Document']['courrier_id']}" . DS . "{$piecejointe['Document']['name']}";
				$documentSuppAnnexePath = $piecejointe['Document']['path'];
				if( !empty( $documentSuppAnnexePath ) ) {
					$file = file_get_contents($documentSuppAnnexePath );
					$suppath = $documentSuppAnnexePath;
				}
				else {
					if( file_exists( $suppath) ) {
						$file = file_get_contents($suppath);
					}
					else {
						$suppath = APP . DS . WEBROOT_DIR . DS . 'files' . DS . "{$piecejointe['Document']['name']}";
						$file = file_put_contents($suppath, $piecejointe['Document']['content']);
					}
				}
				$docs['docSupp'][] = array(
					'content' => $file,
					'mimetype' => $piecejointe['Document']['mime'],
					'filename' => $piecejointe['Document']['name'],
					'ext' => $piecejointe['Document']['ext'],
					'path' => $suppath
				);
			}
		}
		return $docs;
	}

	public function majTraitementsParapheur($courrier_id, $conn = null) {

		$before = $this->Traitement->whoIs($courrier_id, 'current');
		$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($before);

		$ret = $this->Traitement->majTraitementsParapheur($courrier_id, $conn);

		$msg = 'Traitement en cours Parapheur';
		if ($ret == 'TRAITEMENT_TERMINE_OK') {
			$parent = 0;
			$type_id = null;
			$insertAuto = 0;
			$desktop_id = @$desktopsIds[0];
			if (!empty($desktop_id)) {
				if ($this->cakeflowExecuteNext($desktop_id, $courrier_id, $parent, $type_id, $insertAuto)) {
					$msg = 'Flux récupéré et transféré à l\'étape suivante';
				} else {
					$msg = 'Echec de la transmission à l\'étape suivante';
				}
			} else {
				$msg = 'Aucun rôle destinataire défini';
			}
		}
		return $msg;
	}

	public function createRefusParapheur($fluxRefuse = array(), $desktopId, $message) {
		$this->Traitement->execute('KO', $desktopId, $fluxRefuse['Courrier']['id']);
		$msg = array();
		$userId = CakeSession::read('Auth.User.id');
		$traitements = $this->Traitement->find(
			'all', array(
				'conditions' => array(
					'Traitement.target_id' => $fluxRefuse['Courrier']['id']
				),
				'contain' => false
			)
		);
		foreach ($traitements as $t => $traitement) {
			$this->Traitement->delete($traitement['Traitement']['id']);
		}

		if (empty($message)) {
			$this->updateAll(
				array(
					'Courrier.motifrefus' => "'" . Sanitize::clean($this->request->data['Notification']['message'], array('encode', false)) . "'",
					'Courrier.ref_ancien' => $fluxRefuse['Courrier']['id'],
					'Courrier.parapheur_etat' => null,
					'Courrier.parapheur_id' => null,
					'Courrier.signee' => false
				),
				array('Courrier.id' => $fluxRefuse['Courrier']['id'])
			);
		} else {
			$this->updateAll(
				array(
					'Courrier.motifrefus' => "'" . Sanitize::clean($message, array('encode', false)) . "'",
					'Courrier.ref_ancien' => $fluxRefuse['Courrier']['id'],
					'Courrier.parapheur_etat' => null,
					'Courrier.parapheur_id' => null,
					'Courrier.signee' => false
				),
				array('Courrier.id' => $fluxRefuse['Courrier']['id'])
			);
		}
		//on change l etat du flux dans toutes les bannettes
		$this->Bancontenu->cancel($fluxRefuse['Courrier']['id']);
		$this->Bancontenu->refus('-1', $fluxRefuse['Courrier']['id']);

		$desktopCreatorId = $fluxRefuse['Courrier']['desktop_creator_id'];
		$desktop = $this->Desktop->find(
			'first', array(
				'conditions' => array(
					'Desktop.id' => $fluxRefuse['Courrier']['desktop_creator_id']
				),
				'contain' => array(
					'User' => array(
						'SecondaryDesktop'
					)
				),
				'recursive' => -1
			)
		);
		$dataToCheck = [];
		foreach( $desktop['User'] as $key => $user ) {
			$dataToCheck = $user['SecondaryDesktop'];
		}
		if( !empty( $dataToCheck ) ) {
			$groupId = Hash::extract($dataToCheck, '{n}.profil_id');
			$desktopId = Hash::extract($dataToCheck, '{n}.id');
			if( isset($groupId) && !empty($groupId) ) {
				if ( in_array( $groupId, array( 3 ) ) ) {
					$desktopCreatorId = $desktopId;
				}
			}
		}
		//on ajoute le flux dupliqué dans la bannette du rédacteur
		if ($this->Bancontenu->add($desktopCreatorId, BAN_REFUS, $fluxRefuse['Courrier']['id'], false , $userId)) {
			//notification au rédacteur
			$notifyDesktops = array($fluxRefuse['Courrier']['desktop_creator_id'] => array($fluxRefuse['Courrier']['id']));
			if ($this->Notifieddesktop->Notification->add(__d('notification', 'ban_cancel.name'), __d('notification', 'ban_cancel.description'), BAN_REFUS, $fluxRefuse['Courrier']['desktop_creator_id'], $notifyDesktops)) {


				// Ajout pour les notifs
				$destinataires = $this->Traitement->whoIs($fluxRefuse['Courrier']['id'], 'in', array('OK', 'RI', 'IN'));
				$destinataires = Hash::merge($destinataires, $fluxRefuse['Courrier']['desktop_creator_id']);

				foreach ($destinataires as $destinataire_id) {
					if ($destinataire_id != '-1') {
						$user_id = $this->User->userIdByDesktop($destinataire_id);
						$typeNotif = $this->User->find(
							'first', array(
								'fields' => array('User.typeabonnement'),
								'conditions' => array('User.id' => $user_id),
								'contain' => false
							)
						);
						$notificationId = $this->Notifieddesktop->Notification->id;
						if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
							$this->User->saveNotif($fluxRefuse['Courrier']['id'], $user_id, $notificationId, 'refus');
						} else {
							$this->User->notifier($fluxRefuse['Courrier']['id'], $user_id, $notificationId, 'refus');
						}
					}
				}


				if (empty($message)) {
					$msg['message'] = 'Le flux ' . $fluxRefuse['Courrier']['name'] . ' a été refusé avec le motif suivant : <br />' . $this->request->data['Notification']['message'];
				} else {
					$msg['message'] = 'Le flux ' . $fluxRefuse['Courrier']['name'] . ' a été refusé avec le motif suivant : <br />' . $message;
				}
				$msg['success'] = true;
			} else {
				$msg['message'] .= '<br />Notification: error';
			}
		} else {
			$msg['message'] .= '<br />Bannette add: error';
		}
		return $msg;
	}

	/**
	 * 	Mise à jour du document avec la récupératin de la signature et du bordereau
	 */
	public function majDocument($id, $dossier, $conn) {
		$courrier = $this->Document->Courrier->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $id
				),
				'contain' => false,
				'recursive' => -1
			)
		);
		$document = $this->Document->find('first', array(
			'fields' => array(
				'Document.id',
				'Document.name',
				'Document.courrier_id',
				'Document.main_doc',
				'Document.size',
				'Document.path'
			),
			'conditions' => array(
				'Document.courrier_id' => $id,
				'Document.main_doc' => true
			),
			'contain' => false,
			'recursive' => -1
		));

		$this->Document->id = $document['Document']['id'];
		$this->Document->saveField('parapheur_bordereau', base64_decode($dossier['getdossier']['bordereau']));
		$this->Document->saveField('signature', base64_decode($dossier['getdossier']['signature']));
		$this->Document->saveField('signee', true);

		if (!empty($dossier['getdossier']['docprinc']) ) {
			// Sauvegarde du doc principal
			$documentPath = $document['Document']['path'];
			if( !empty( $documentPath ) ) {
				@unlink($documentPath);
				$name = $document['Document']['name'];
				$name = Sanitize::clean($name, array('encode', false));
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
				file_put_contents( $file, base64_decode($dossier['getdossier']['docprinc']));
				$path = $file;

				$this->Document->updateAll(
					array(
						'Document.path' => "'".$path."'"
					),
					array(
						'Document.id' => $document['Document']['id']
					)
				);
			}
			else {
				$this->Document->saveField('content', base64_decode($dossier['getdossier']['docprinc']));
			}
		}

		if (empty($dossier['getdossier']['signatureformat']) || strpos($dossier['getdossier']['signatureformat'], 'PAdES') === false) {
			// Sauvegarde des signatures (zip)
			if (!empty($dossier['getdossier']['signature'])) {
				$this->Document->saveField('signature', base64_decode($dossier['getdossier']['signature'], true));
			}
		}


		// Affichage de la signature
		if( !empty($dossier['getdossier']['signature']) ) {

			if( !empty( $document['Document']['path'] ) ) {
				$name = 'Signature_Recuperee_' . $id . '.zip';
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
				file_put_contents( $file, base64_decode($dossier['getdossier']['signature']));
				$pathsign = $file;
				$documentSignature = array(
					'Document' => array(
						'courrier_id' => $id,
						'size' => strlen(base64_decode($dossier['getdossier']['signature'])),
						'main_doc' => false,
						'content' => null,
						'path' => $pathsign,
						'name' => 'Signature_Recuperee_' . $id . '.zip',
						'ext' => 'pdf',
						'mime' => 'application/pdf'
					)
				);
			}
			else {
				$documentSignature = array(
					'Document' => array(
						'courrier_id' => $id,
						'size' => strlen(base64_decode($dossier['getdossier']['signature'])),
						'main_doc' => false,
						'content' => base64_decode($dossier['getdossier']['signature']),
						'name' => 'Signature_Recuperee_' . $id . '.zip',
						'ext' => 'zip'
					)
				);
			}
			$this->Document->create($documentSignature);
			$this->Document->save();
		}

		if( !empty( $dossier['getdossier']['bordereau'] ) ) {
			if (!empty($document['Document']['path'])) {
				$name = 'Bordereau_'.date('Y-m-d_H:i:s').'_' . $id . '.pdf';
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
				file_put_contents($file, base64_decode($dossier['getdossier']['bordereau']));
				$pathbordereau = $file;
				$documentBordereau = array(
					'Document' => array(
						'courrier_id' => $id,
						'size' => strlen(base64_decode($dossier['getdossier']['bordereau'])),
						'main_doc' => false,
						'content' => null,
						'path' => $pathbordereau,
						'name' => $name,
						'ext' => 'pdf',
						'mime' => 'application/pdf'
					)
				);
			} else {
				// Affichage du bordereau
				$documentBordereau = array(
					'Document' => array(
						'courrier_id' => $id,
						'size' => strlen(base64_decode($dossier['getdossier']['bordereau'])),
						'main_doc' => false,
						'content' => base64_decode($dossier['getdossier']['bordereau']),
						'name' => 'Bordereau_'.date('Y-m-d_H:i:s').'_' . $id . '.pdf',
						'ext' => 'pdf',
						'mime' => 'application/pdf'
					)
				);
			}

			$this->Document->create($documentBordereau);
			$this->Document->save();
		}
		/*if( !empty( $dossier['getdossier']['autredocprinc'] )) {
			$autredocument = $this->Document->find('first', array(
				'fields' => array(
					'Document.id',
					'Document.name',
					'Document.courrier_id',
					'Document.main_doc',
					'Document.size',
					'Document.path'
				),
				'conditions' => array(
					'Document.courrier_id' => $id,
					'Document.asigner' => true
				),
				'contain' => false,
				'recursive' => -1
			));

			@unlink($autredocument['Document']['path']);
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$autredocument['Document']['name']}";
			file_put_contents( $file, base64_decode($dossier['getdossier']['autredocprinc']));
			$path = $file;

			$this->Document->id = $autredocument['Document']['id'];
			$this->Document->saveField('path', $path);
			$this->Document->saveField('signature', base64_decode($dossier['getdossier']['autredocprinc']));
			$this->Document->saveField('signee', true);
		} */
		if( !empty( $dossier['getdossier']['autredocprinc'] )) {

			$autredocument = $this->Document->find('all', array(
				'fields' => array(
					'Document.id',
					'Document.name',
					'Document.courrier_id',
					'Document.main_doc',
					'Document.size',
					'Document.path'
				),
				'conditions' => array(
					'Document.courrier_id' => $id,
					'Document.asigner' => true
				),
				'contain' => false,
				'recursive' => -1
			));
			foreach ( $dossier['getdossier']['autrenomdocprinc'] as $key => $autrenom ){
				foreach ($autredocument as $annexe) {
					if ($annexe['Document']['name'] == $autrenom ) {
						@unlink($annexe['Document']['path']);
						$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
						$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$annexe['Document']['name']}";
						file_put_contents($file, base64_decode($dossier['getdossier']['autredocprinc'][$key]));
						$path = $file;

						$this->Document->id = $annexe['Document']['id'];
						$this->Document->saveField('path', $path);
						$this->Document->saveField('signature', base64_decode($dossier['getdossier']['autredocprinc'][$key]));
						$this->Document->saveField('signee', true);
					}

				}
			}
		}
	}

	/**
	 * 	Mise à jour du document avec la récupératin de la signature et du bordereau
	 */
	public function majDocumentPastell($id, $dossier, $conn) {
		$success = true;

		$courrier = $this->Document->Courrier->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $id
				),
				'contain' => false,
				'recursive' => -1
			)
		);
		$document = $this->Document->find('first', array(
			'fields' => array(
				'Document.id',
				'Document.name',
				'Document.courrier_id',
				'Document.main_doc',
				'Document.size',
				'Document.path'
			),
			'conditions' => array(
				'Document.courrier_id' => $id,
				'Document.main_doc' => true
			),
			'contain' => false,
			'recursive' => -1
		));

		$this->Document->id = $document['Document']['id'];
		$this->Document->saveField('signee', true);
		// Cas du doc principal avec la signature incorporé
		if ( !empty( $dossier['document']) ){
			// Sauvegarde du doc principal
			$documentPath = $document['Document']['path'];
			if( !empty( $documentPath ) ) {
//				@unlink($documentPath);
				$name = $document['Document']['name'];
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
				file_put_contents($file, $dossier['document']);
				$path = $file;

				$this->Document->updateAll(
					array(
						'Document.path' => "'".$path."'"
					),
					array(
						'Document.id' => $document['Document']['id']
					)
				);
			}
		}

		// Cas de la signature au format ZIP
		if( isset( $dossier['signaturezip'] ) && !empty($dossier['signaturezip']) ) {
			$name = 'Signature_Recuperee_' . $id . '.zip';
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
			file_put_contents( $file, $dossier['signaturezip']);
			$pathsign = $file;
			$documentSignature = array(
				'Document' => array(
					'courrier_id' => $id,
					'size' => strlen($dossier['signaturezip']),
					'main_doc' => false,
					'content' => null,
					'path' => $pathsign,
					'name' => 'Signature_Recuperee_' . $id . '.zip',
					'ext' => 'pdf',
					'mime' => 'application/pdf'
				)
			);
			$this->Document->create($documentSignature);
			$success = $this->Document->save() && $success;
		}

		// Cas du bordereau
		if( !empty( $dossier['bordereau'] ) ) {
			$name = 'Bordereau_' . $id . '.pdf';
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
			file_put_contents( $file, $dossier['bordereau']);
			$pathbordereau = $file;
			$documentBordereau = array(
				'Document' => array(
					'courrier_id' => $id,
					'size' => strlen($dossier['bordereau']),
					'main_doc' => false,
					'content' => null,
					'path' => $pathbordereau,
					'name' => $name,
					'ext' => 'pdf',
					'mime' => 'application/pdf'
				)
			);
			$this->Document->create($documentBordereau);
			$success = $this->Document->save() && $success;
		}

		// Cas de l'historique
		if( !empty( $dossier['historique'] ) ) {
			$name = 'Historique' . $id . '.xml';
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
			file_put_contents( $file, $dossier['historique']);
			$pathhistorique = $file;
			$documentHistorique = array(
				'Document' => array(
					'courrier_id' => $id,
					'size' => strlen($dossier['historique']),
					'main_doc' => false,
					'content' => null,
					'path' => $pathhistorique,
					'name' => $name,
					'ext' => 'xml',
					'mime' => 'application/xml'
				)
			);
			$this->Document->create($documentHistorique);
			$success = $this->Document->save() && $success;
		}

		// Cas du mail sécurisé PASTELL
		if( !empty( $dossier['horodatage'] ) ) {
			$name = 'Preuve_Horodatage' . $id . '.tsa';
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$name}";
			file_put_contents( $file, $dossier['horodatage']);
			$pathhorodatage = $file;
			$documentHorodatage = array(
				'Document' => array(
					'courrier_id' => $id,
					'size' => strlen($dossier['horodatage']),
					'main_doc' => false,
					'content' => null,
					'path' => $pathhorodatage,
					'name' => $name,
					'ext' => 'tsa',
					'mime' => 'application/octet-stream'
				)
			);
			$this->Document->create($documentHorodatage);
			$success = $this->Document->save() && $success;

			if( !empty( $dossier['mailseccontent'] ) ) {
				$sendmailDatas = [
					'Sendmail' => [
						'courrier_id' => $id,
						'email' => $dossier['email'],
						'message' => $dossier['mailseccontent']['message_horodate'],
						'created' => $dossier['mailseccontent']['date_horodatage'],
						'modified' => $dossier['mailseccontent']['date_horodatage'],
						'document_id' => $this->Document->id,
						'desktop_id' => '-3'
					]
				];
				$this->Sendmail->create($sendmailDatas);
				$success = $this->Sendmail->save() && $success;
			}
		}



		/*if( !empty( $dossier['getdossier']['autredocprinc'] )) {

			$autredocument = $this->Document->find('all', array(
				'fields' => array(
					'Document.id',
					'Document.name',
					'Document.courrier_id',
					'Document.main_doc',
					'Document.size',
					'Document.path'
				),
				'conditions' => array(
					'Document.courrier_id' => $id,
					'Document.asigner' => true
				),
				'contain' => false,
				'recursive' => -1
			));
			foreach ( $dossier['getdossier']['autrenomdocprinc'] as $key => $autrenom ){
				foreach ($autredocument as $annexe) {
					if ($annexe['Document']['name'] == $autrenom ) {
						@unlink($annexe['Document']['path']);
						$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'], true, 0777);
						$file = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . "{$annexe['Document']['name']}";
						file_put_contents($file, base64_decode($dossier['getdossier']['autredocprinc'][$key]));
						$path = $file;

						$this->Document->id = $annexe['Document']['id'];
						$this->Document->saveField('path', $path);
						$this->Document->saveField('signature', base64_decode($dossier['getdossier']['autredocprinc'][$key]));
						$this->Document->saveField('signee', true);
					}

				}
			}
		}*/
	}

	/**
	 * Envoi par mail d'un document lié à un flux
	 *
	 * @param integer $courrier_id identifiant du flux
	 * @param integer $user_id identifiant de l'utilisateur à notifier
	 * @param string $type notification à envoyer
	 * @return bool succès de l'envoi
	 */
	public function docParMail($mailDest, $doc, $sujet, $message, $mailCCDest, $mailCCiDest, $attachements) {

		$conn = CakeSession::read('Auth.Collectivite.conn');
		App::uses('CakeEmail', 'Network/Email');
		$config_mail = 'default';
		$this->Email = new CakeEmail($config_mail);

		$mailDest = explode('; ', $mailDest);
		foreach ($mailDest as $mails) {
			$this->Email->addTo($mails);
		}
		if (!empty($mailCCDest)) {
			$mailCCDest = explode('; ', $mailCCDest);
			foreach ($mailCCDest as $mailsCC) {
				$this->Email->addCc($mailsCC);
			}
		}
		if (!empty($mailCCiDest)) {
			$mailCCiDest = explode('; ', $mailCCiDest);
			foreach ($mailCCiDest as $mailsCCi) {
				$this->Email->addBcc($mailsCCi);
			}
		}

		if (empty($doc['Document'])) {
			$courrier_id = $doc['Ar']['courrier_id'];
		} else {
			$courrier_id = $doc['Document']['courrier_id'];
		}
		App::uses('Courrier', 'Model');
		$this->Courrier = new Courrier();
		$flux = $this->Courrier->find(
			'first',
			array(
				'recursive' => -1,
				'conditions' => array('Courrier.id' => $courrier_id),
				'fields' => array(
					'Courrier.id',
					'Courrier.objet',
					'Courrier.reference',
					'Courrier.parent_id',
					'Courrier.name',
					'Courrier.motifrefus',
					'Soustype.circuit_id'
				),
				'joins' => array(
					$this->Courrier->join('Soustype')
				)
			)
		);
		if (empty($sujet)) {
			$sujet = "Un nouveau document vous a été envoyé depuis web-GFC";
		}
		$type = "docparmail";

		// Si,le doc principal n'est pas de type PDF, alors on génère un pdf
		if( $doc['Document']['mime'] != 'application/pdf' && $doc['Document']['mime'] == 'application/vnd.oasis.opendocument.text' ) {
			$docNamePDf = substr( $doc['Document']['name'], 0, -4);
			$docNamePDf = $docNamePDf.'.pdf';
			require_once 'XML/RPC2/Client.php';
			$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
			$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$doc['Document']['name']}";
			$options = array(
				'uglyStructHack' => true
			);
			$url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
			$client = XML_RPC2_Client::create($url, $options);
			try {
				// On convertit en PDF
				App::uses('ConversionComponent', 'Controller/Component');
				$this->Conversion = new ConversionComponent;
				$docContent = $this->Conversion->convertirFlux(file_get_contents($file), 'odt', 'pdf');

				// On crée le fichier convertit sur le disque
				$sessionFluxIdDir = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'];
				$Folder = new Folder($sessionFluxIdDir, true, 0777);
				$generatedFile = file_put_contents($sessionFluxIdDir . DS . $docNamePDf, $docContent);

				// On stocke le document en BDD
				$fileToPdf = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$docNamePDf}";
				$document['Document']['name'] = $docNamePDf;
				$document['Document']['path'] = $fileToPdf;
				$document['Document']['size'] = strlen(base64_decode($docContent));
				$document['Document']['mime'] = 'application/pdf';
				$document['Document']['ext'] = 'pdf';
				$document['Document']['main_doc'] = true;
				$document['Document']['courrier_id'] = $courrier_id;
				// Seul les créateurs de doc peuvent les supprimer
				$document['Document']['desktop_creator_id'] = CakeSession::read('Auth.User.id');
				$docPrincipalFlux = $this->getMainDoc($courrier_id);
				// Si le doc ODT est en doc principal, on le passe en doc secondaire
				if (!empty($docPrincipalFlux)) {
					$this->Courrier->Document->updateAll(
						array('Document.main_doc' => false), array('Document.id' => $docPrincipalFlux['Document']['id'])
					);
				}

				$this->Document->create($document);
				$this->Document->save();

			} catch (XML_RPC2_FaultException $e) {
				$this->log('Exception #' . $e->getFaultCode() . ' : ' . $e->getFaultString(), 'debug');
				return false;
			}
			$docId = $this->Courrier->Document->id;
			$docName = $docNamePDf;
			$docPath = $document['Document']['path'];
		}
		else {

			if (empty($doc['Document'])) {
				$docId = $doc['Ar']['id'];
				$docName = $doc['Ar']['name'];
				$extension = pathinfo($docName, PATHINFO_EXTENSION);
				if (empty($extension)) {
					$docName = $docName . '.' . $extension;
				}

				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$docName}";
				$docContent = file_get_contents($file);
				$docPath = $file;
			} else {
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$doc['Document']['name']}";
				$isAR = strpos($doc['Document']['name'], 'AR');
				if ($isAR !== false && $doc['Document']['mime'] != 'application/pdf') {
					require_once 'XML/RPC2/Client.php';
					$options = array(
						'uglyStructHack' => true
					);
					$url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
					$client = XML_RPC2_Client::create($url, $options);
					try {
						$docContent = $client->convertFile(base64_encode(file_get_contents($file)), 'odt', 'pdf', false, true);
						$docContent = base64_decode($docContent);
					} catch (XML_RPC2_FaultException $e) {
						$this->log('Exception #' . $e->getFaultCode() . ' : ' . $e->getFaultString(), 'debug');
						return false;
					}
					$docId = $doc['Document']['id'];
					$docName = $doc['Document']['name'];
					$docPath = $doc['Document']['path'];
				} else {
					$docId = $doc['Document']['id'];
					$docName = $doc['Document']['name'];
					$docContent = file_get_contents($file);
					$docPath = $doc['Document']['path'];
				}
			}
		}

		// création du répertoire local
		$folder = WWW_ROOT . 'files/attachments/' . $docId . '/';
		if (!file_exists($folder)) {
			mkdir($folder, 0777, true);
		}
		// création du fichier
		$docName = str_replace( ' ', '_', replace_accents( $docName ) ) ;
		$docName = str_replace( '’', '_', $docName ) ;
		$filename = $docName;
		if (file_exists($file)) {
			$fileToSend = $file;
		}
		else if (!file_exists($folder . $filename)) {
			$fileToSend = fopen($folder . $filename, "x+");
			// écriture
			fputs($fileToSend, $docContent);
			// fermeture
			fclose($fileToSend);
		} else {
			$fileToSend = $folder . $filename;
		}


		if( !empty($attachements) ) {
			foreach($attachements as $d => $attachement) {
				$attachementName = str_replace( ' ', '_', replace_accents( $attachement['Document']['name'] ) ) ;
				$nameToSend = $attachementName;
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
				$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$attachement['Document']['name']}";
				$file = $attachement['Document']['path'];
				$docContentToSend = file_get_contents($file);

				if (!file_exists($folder . $nameToSend)) {
					$fileAttachedToSend = fopen($folder . $nameToSend, "x+");
				} else {
					$fileAttachedToSend = $folder . $nameToSend;
				}

				$fileAttachedContent = $docContentToSend;
				// écriture
				fputs($fileAttachedToSend, $fileAttachedContent);
				// fermeture
				fclose($fileAttachedToSend);
				$nameAttached[$nameToSend] = array(
					'file' => $folder . $nameToSend
				);
			}
		}

		if( !empty($nameAttached)) {
			$this->Email->attachments(
				array_merge(
					array($docName => array(
						'file' => $docPath
					) ),
					$nameAttached
				)
			);
		}
		else {
			$this->Email->attachments(
				array(
					$docName => array(
						'file' => $docPath
					)
				)
			);
		}

		$this->Email->emailFormat(CakeEmail::MESSAGE_HTML);

		$this->Email->subject($sujet);

		if( Configure::read('Mail.displayReplyTo') ) {
			$mailReplyTo = CakeSession::read('Auth.User.mail');
			$this->Email->replyTo($mailReplyTo);
		}

		$content = $this->_paramMails($type, $flux, $message);
		$content = str_replace("\r", "<br>", $content); // Pour conserver les retours à la ligne dans les mails
		$success = $this->Email->send($content);
		if ($success) {
			if( !empty($nameAttached ) ) {
				foreach( $nameAttached as $nameToUnlink => $data) {
					unlink($folder . $nameToUnlink);
				}
			}
			rmdir($folder);
		}

		return true;
	}

	/**
	 * Détermine le contenu du mail à envoyer en fonction du type de mail, le flux et l'utilisateur
	 * @param string $type
	 * @param array $flux
	 * @param array $user
	 * @return string
	 */
	function _paramMails($type, $flux, $message) {

		$searchReplace = array(
			"#IDENTIFIANT_FLUX#" => $flux['Courrier']['id'],
			"#NOM_FLUX#" => $flux['Courrier']['name'],
			"#OBJET_FLUX#" => $flux['Courrier']['objet'],
			"#LIBELLE_CIRCUIT#" => $this->Courrier->Traitement->Circuit->getLibelle($flux['Soustype']['circuit_id']),
			"#FLUX_ORIGINE#" => isset($flux['Courrier']['parent_id']) ? $flux['Courrier']['parent_id'] : null,
			"#REFERENCE_FLUX#" => isset($flux['Courrier']['reference']) ? $flux['Courrier']['reference'] : null
		);

		return str_replace(array_keys($searchReplace), array_values($searchReplace), $message);
	}

	/**
	 * Fonctionnalité de numérisation de masse (création de flux)
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant de la collectivité
	 * @param base64 $fichier_associe contenu du fichier lié
	 * @param string $username identifiant de connexion de l'utilisateur cible
	 * @return array
	 */
	public function remoteCreateMail($desktopmanagerId, $filename, $collId, $conn, $typeId = null, $soustypeId = null) {
		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Message' => 'Impossible de créer le flux.',
			'Severite' => 'important'
		);
//$this->Log(base64_decode($filename->bodyOtherHtml));
		$userId = CakeSession::read('Auth.User.id');
		if( isset($filename->body) && !empty($filename->body) ) {
			$bodyForFile = $filename->body;

			if( empty( base64_decode($filename->body, true) ) && ( strpos( $filename->bodyHtml, '<!DOCTYPE') !== false || strpos( $filename->bodyHtml, '<!doctype') !== false ) ){
				$bodyForFile = $filename->bodyHtml;
			}
			if( !empty( base64_decode($filename->body, true) ) && !empty( base64_decode($filename->bodyHtml, true ) ) )  {
				$bodyForFile = base64_decode($filename->bodyHtml );
			}

			if( !empty( base64_decode($filename->body, true) ) && !empty( base64_decode($filename->bodyHtml, true ) ) && !empty( base64_decode($filename->bodyOtherHtml, true) ) )  {
				$bodyForFile = base64_decode($filename->bodyOtherHtml );
			}

			if( !empty( base64_decode($filename->body) ) && !empty( base64_decode($filename->bodyHtml ) ) && !empty( base64_decode($filename->bodyOtherHtml) ) )  {
				if( !empty(base64_decode($filename->bodyOtherHtml, true)) ) {
					$bodyForFile = base64_decode($filename->bodyOtherHtml );
				}
				else if( !empty(base64_decode($filename->body, true)) ) {
					$bodyForFile = base64_decode($filename->body );
				}
			}

			if(  stripos( $filename->bodyHtml, '<html') === 0 || stripos( $filename->bodyHtml, '<HTML') === 0 ){
				if(strpos( base64_decode($filename->bodyHtml), '<?php') !== false ) {
					$bodyForFile = base64_decode($filename->body);
				}
				else {
					$bodyForFile = base64_decode($filename->bodyHtml);
				}
			}


			// le bodyHtml possède du doctype
			if( !empty( $filename->bodyHtml ) && ( stripos( $filename->bodyHtml, '<!DOCTYPE' ) === 0 || stripos( $filename->bodyHtml, '<!doctype' ) === 0 ) ) {
				$bodyForFile = $filename->bodyHtml;
			}

			// Si le bodyHtml n'est pas vide ET que décodage donne du html
			if( !empty( $filename->bodyHtml ) && ( stripos( base64_decode($filename->bodyHtml), '<html' ) === 0 || stripos( base64_decode($filename->bodyHtml), '<HTML' ) === 0 ) ) {
				$bodyForFile = base64_decode($filename->bodyHtml);
			}

			if( !empty( $filename->bodyHtml ) && !empty( stripos( base64_decode($filename->bodyHtml), '<html' ) ) && stripos( base64_decode($filename->bodyHtml), '<html' ) !== 0  && !empty( $filename->bodyOtherHtml )) {
				$bodyForFile = $filename->bodyOtherHtml;
			}

			if( strpos( $filename->bodyOtherHtml, '<!DOCTYPE' ) !== false || strpos( $filename->bodyOtherHtml, '<!doctype' ) !== false || strpos( $filename->bodyOtherHtml, '<meta' ) !== false) {
				$bodyForFile = $filename->bodyOtherHtml;
			}

			if(  stripos( $filename->bodyHtml, '<html') === 0 || stripos( $filename->bodyHtml, '<HTML') === 0 ){
				$bodyForFile = $filename->bodyHtml;
			}

			// Si le bodyHtml est vide et que le body est en base64
			if( empty( $filename->bodyHtml ) && !empty(base64_decode($filename->bodyOtherHtml, true) ) ){
				$bodyForFile = base64_decode($filename->bodyOtherHtml);
				$body = base64_decode($filename->bodyOtherHtml);
			}
		}
		$noRoleErrMsg = "Le rôle n'existe pas.";

		$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
		$desktopsIds[] = $this->Desktopmanager->getDesktops($desktopmanagerId);
		$desktops = array();
		$desktopCreatorId = [];
		foreach ($desktopsIds as $i => $desktopId) {
			$desktops = $this->Bancontenu->Desktop->find(
				'all', array(
					'conditions' => array(
						'Desktop.id' => $desktopsIds[$i]
					),
					'contain' => false
				)
			);
			if ($desktops[$i]['Desktop']['profil_id'] == DISP_GID) {
				$BAN = BAN_AIGUILLAGE;
				$desktopCreatorId[] = $desktops[$i]['Desktop']['id'];
			} else if ($desktops[$i]['Desktop']['profil_id'] == INIT_GID) {
				$BAN = BAN_DOCS;
				$desktopCreatorId[] = $desktops[$i]['Desktop']['id'];
			}
		}
		if (empty($desktopmanagerId)) {
			$return['CodeRetour'] = "KONODESKTOP";
			$return['Message'] .= $noRoleErrMsg;
		} else if (empty($BAN)) {
			$return['CodeRetour'] = "KONOBAN";
			$return['Message'] .= 'Aucun bannette renseignée';
		} else {

			// Si le contenu est en base 64 dans le body
			$contentType = get_string_between($filename->body, 'Content-Type: ', ';');
			if($contentType == 'text/plain') {
				$body64 = get_string_between($filename->body, 'Content-Transfer-Encoding: base64', '--');
				$bodyForFile = base64_decode(trim($body64));
			}

//$this->log(utf8_decode($bodyForFile));
//$this->log($filename->bodyOtherHtml);
			// Si le $bodyForFile n'est pas en html, ni le bodyOther HTML
			if( ( stripos( $bodyForFile, '<html' ) !== 0 || stripos( $bodyForFile, '<HTML' ) !== 0 ) && ( stripos( $filename->bodyOtherHtml, '<html' ) !== 0 || stripos( $filename->bodyOtherHtml, '<HTML' ) !== 0 ) ) {
				if( ( stripos( $bodyForFile, '<!DOCTYPE' ) !== 0 || stripos( $bodyForFile, '<!doctype' ) !== 0 ) ) {
					$body64 = get_string_between($filename->bodyOtherHtml, 'Content-Type: text/html; charset="utf-8"', '--');
					$body642 = get_string_between($filename->bodyOtherHtml, 'Content-Type: text/html; charset=utf-8', '--=');
					$body643 = get_string_between($filename->bodyOtherHtml, 'Content-Type: text/html; charset=UTF-8', '--b');
					$bodyIso = get_string_between($filename->bodyOtherHtml, 'Content-Type: text/html; charset="iso-8859-1"', '--');
					if( !empty( $body64 ) || !empty($body642) || !empty($body643) || !empty($bodyIso)) {
						if( !empty( $body64 ) ) {
							if( strpos($body64, 'base64') ) {
								$dataBase64ToDisplay = substr($body64, strlen('Content-Transfer-Encoding: base64') + 2);
							}
							else if( strpos($body64, 'quoted-printable') ) {
								$dataHtmlToDisplay = substr($body64, strlen('Content-Transfer-Encoding: quoted-printable') + 6);
							}
						}
						else if( !empty( $body642 ) ) {
							if( strpos($body642, 'base64') ) {
								$dataBase64ToDisplay = substr($body642, strlen('Content-Transfer-Encoding: base64') + 2);
							}
							else if( strpos($body642, 'quoted-printable') ) {
								$dataHtmlToDisplay = substr($body642, strlen('Content-Transfer-Encoding: quoted-printable') + 6);
							}
						}
						else if( !empty( $body643 ) ) {
							if( strpos($body643, 'base64') ) {
								$dataBase64ToDisplay = substr($body643, strlen('Content-Transfer-Encoding: base64') + 2);
							}
							else if( strpos($body643, 'quoted-printable') ) {
								$dataHtmlToDisplay = substr($body643, strlen('Content-Transfer-Encoding: quoted-printable') + 6);
							}
						}
						else if( !empty( $bodyIso ) ) {
							if( strpos($bodyIso, 'quoted-printable') ) {
								$dataHtmlToDisplay = substr($bodyIso, strlen('Content-Transfer-Encoding: quoted-printable') + 6);
							}
						}
						if( !empty($dataBase64ToDisplay) ) {
							$bodyForFile = base64_decode(trim($dataBase64ToDisplay));
						}
						else if(!empty($dataHtmlToDisplay)) {
							$bodyForFile = trim($dataHtmlToDisplay);
						}
					}
				}
			}

			// Si PGh0 , cela signifie que la balise de départ est <html (en base 64)
			if( strpos($filename->body, 'PGh0') !== false ) {
				$filename->body = base64_decode($filename->body);
				if( strpos( $filename->body, '<!DOCTYPE') !== false || strpos( $filename->body, '<html ') !== false || strpos( $filename->body, '--_000') !== false ) {
					$filename->body = strip_tags( $filename->body, "<style>" );
					$substring = substr($filename->body ,strpos($filename->body ,"<style"),strpos($filename->body ,"</style>")+2);
					$filename->body = str_replace($substring,"",$filename->body);
					$filename->body = trim($filename->body);
				}
			}

			if( strpos( $filename->body, '--_000') !== false ) {
				$filename->body = strip_tags( $filename->body, "<style>" );
				$substring = substr($filename->body ,strpos($filename->body ,"<style"),strpos($filename->body ,"</style>")+2);
				$filename->body = str_replace($substring,"",$filename->body);
				$filename->body = trim($filename->body);
			}

			if( strpos( $filename->body, '<style') !== false ) {
				$filename->body = strip_tags( $filename->body, "<style>" );
				$substring = substr($filename->body ,strpos($filename->body ,"<style"),strpos($filename->body ,"</style>")+2);
				$filename->body = str_replace($substring,"",$filename->body);
				$filename->body = trim($filename->body);
			}

			$referenceUnique = $this->genReferenceUnique();


			$pos = strpos($filename->subject, '=?UTF-8?B?');
			if ($pos !== false) {
				$filename->subject = mb_decode_mimeheader($filename->subject);
			}
			$pos = strpos($filename->subject, '=?utf-8?Q?');
			if ($pos !== false) {
				$filename->subject = mb_decode_mimeheader($filename->subject);
			}

			mb_internal_encoding('UTF-8');


			// On récupère la date du mail
			$dateOfTheMail = get_string_between($filename->mailHeader, 'Date: ', '+');
			$oldDate = strtotime($dateOfTheMail);
			$date = "Date: ".date('d/m/Y H:i:s', $oldDate);
			$dateForFile = "<b>Date:</b> ".date('d/m/Y H:i:s', $oldDate)."</br>";

			// On récupère chaque élément du header du mail
			$mailData = preg_split("/((\r?\n)|(\r\n?))/", $filename->mailHeader);
			$from = $fromForFile = $object = $objectForFile = $to = $toForFile = $replyTo = $replyToForFile = null;
			foreach( $mailData as $data ){

				$data = str_replace( '<', '(', $data);
				$data = str_replace( '>', ')', $data);
				if( strpos($data, '=?') !== false ) {
					$data = mb_decode_mimeheader($data);
				}
				if( stripos( $data, 'From:') === 0 ) {
					$from = str_replace( 'From', 'De', $data)."\r";
					$fromForFile = str_replace( 'From:', '<b>De:</b>', $data)."</br>";
				}
				if( stripos( $data, 'Subject:') === 0 ) {
					$object = str_replace( 'Subject', 'Objet', $data)."\r";
					$objectForFile = '<b>Objet: </b>'.replace_accents($filename->subject)."</br>";
				}
				if( stripos( $data, 'To:') === 0 ) {
					$to = str_replace( 'To', 'A', $data)."\r";
					$toForFile = str_replace( 'To:', '<b>A:</b>', $data)."</br>";
				}
				if( stripos( $data, 'Reply-To:') === 0 ) {
					$replyTo = str_replace( 'Reply-To', 'Répondre à', $data)."\r";
					$replyToForFile = str_replace( 'Reply-To:', '<b>Répondre à:</b>', $data)."</br>";
				}
			}

			$bodyFrom = $from;
			$bodyFrom .= $replyTo;
			$bodyFrom .= $to;
			$bodyFrom .= $object;
			$bodyFrom .= $date;

			// Ajout des informations d'entête du mail dans le corps du pdf généré
			$bodyFromForFile = '';
			$bodyFromForFile .= $fromForFile;
			$bodyFromForFile .= $replyToForFile;
			$bodyFromForFile .= $toForFile;
			$bodyFromForFile .= $objectForFile;
			$bodyFromForFile .= $dateForFile."</br>";
			$bodyForFile = nl2br($bodyFromForFile).$bodyForFile;

			$body = '';
			if( is_base64_string_s($filename->body) ){
				$filename->body = str_replace(' ', '', $filename->body);
				if(is_base64_encoded($filename->body)) {
					$body = base64_decode($filename->body);
				}
				else {
					$body = $filename->body;
				}
			}
			if(!Configure::read('Mail.Confspeciale')) {
				if (!empty($bodyForFile)) {
//					$body = $bodyForFile;
					// La partie body présente du html, div, doctype
					if( ( stripos( base64_decode($filename->body), '<html' ) === 0 || stripos( base64_decode($filename->body), '<HTML' ) === 0 ) || ( stripos( base64_decode($filename->body), '<div' ) === 0 || stripos( base64_decode($filename->body), '<DIV' ) === 0 ) || ( stripos( base64_decode($filename->body), '<!doctype' ) === 0 || stripos( base64_decode($filename->body), '<!DOCTYPE' ) === 0 ) ) {
						$body = $filename->body;
					}
					// La partie bodyForFile présente du html, div, doctype
					else if( (
						stripos( base64_decode($bodyForFile), '<html' ) === 0 || stripos( base64_decode($bodyForFile), '<HTML' ) === 0 )
						|| ( stripos( base64_decode($bodyForFile), '<div' ) === 0 || stripos( base64_decode($bodyForFile), '<DIV' ) === 0 )
						|| ( stripos( base64_decode($bodyForFile), '<!doctype' ) === 0 || stripos( base64_decode($bodyForFile), '<!DOCTYPE' ) === 0 )
					) {
						$body = $bodyForFile;
					}
					else if( empty(base64_decode($filename->body, true)) &&
						( stripos( $bodyForFile, '<html' ) === 0
						|| stripos( $bodyForFile, '<HTML' ) === 0
						||  stripos( $bodyForFile, '<!doctype' ) === 0
						||  stripos( $bodyForFile, '< html' ) === 0
						||  stripos( $bodyForFile, '<!DOCTYPE' ) === 0
						)
					) {
						$body = $filename->body;
					}
					else if( !empty( base64_decode($filename->body) ) && !empty( base64_decode($filename->bodyHtml ) ) && !empty( base64_decode($filename->bodyOtherHtml) ) )  {
						if(
							stripos( $filename->bodyHtml, '<div') === 0
							|| stripos( $filename->bodyHtml, '<DIV') === 0
							|| stripos( $filename->bodyHtml, '<!DOCTYPE') === 0
							|| stripos( $filename->bodyHtml, '<!doctype') === 0
							|| stripos( $filename->bodyHtml, '<html') === 0
							|| stripos( $filename->bodyHtml, '<HTML') === 0
							|| stripos( $filename->bodyHtml, '--') === 0
							|| strpos( $filename->bodyHtml, '<!DOCTYPE') !== false
							|| strpos( base64_decode($filename->bodyHtml), '<!DOCTYPE') === false
							|| strpos( base64_decode($filename->bodyHtml), '<!doctype') === false
							|| strpos( base64_decode($filename->bodyHtml), '<div') === false
							|| strpos( base64_decode($filename->bodyHtml), '<DIV') === false
							|| strpos( base64_decode($filename->bodyHtml), '<html') === false
							|| strpos( base64_decode($filename->bodyHtml), '<HTML') === false
						){
							if( stripos( $filename->bodyOtherHtml, '--') === 0 ) {
								$body = get_string_between( $filename->bodyOtherHtml, 'Content-Transfer-Encoding: quoted-printable', '--');
							}
							else if( strpos( base64_decode($filename->bodyHtml), 'PDF') !== false || strpos( base64_decode($filename->bodyHtml), 'JFIF') !== false) {
								if( !Configure::read('Mail.MultipleBase64Data') ) {
									$body = base64_decode($filename->body);
								}
								else {
									$body = $filename->body;
								}
							}
							else {
								$body = $filename->body;
							}
						}
						else {
							$body = base64_decode($filename->body); // Pour BREST les 10 derniers
						}
					}
					else if( empty($filename->bodyHtml ) && stripos( $filename->body, '<meta' ) !== 0 ) {
						$body = base64_decode($filename->body);
					}
					else {
						$body = $filename->body;
					}
				}
			}

			if( !Configure::read('Mail.MultipleBase64Data') ) {
				if (!empty(base64_decode($filename->body, true)) && !empty(base64_decode($filename->bodyHtml, true)) && !empty(base64_decode($filename->bodyOtherHtml, true))) {
					$body = base64_decode($filename->body);
				}
			}
//$this->Log($body);
//$this->Log($bodyForFile);
			$body = preg_replace( '/[: "]/', ' ', $body );

			if( isset($filename->pj) && !empty($filename->pj) && ( !empty($filename->pj[0]['filename']) || !empty($filename->pj[1]['filename']) ) ) {
				$nbPjs = count($filename->pj) - 1;
				$bodyPj = "\n " . "Nombre de pjs: " . $nbPjs;
				foreach ($filename->pj as $pj) {
					$subjectFormat = strpos($pj['filename'], '=?UTF-8?B?');
					if ($subjectFormat !== false) {
						$pj['filename'] = mb_decode_mimeheader($pj['filename']);
					}
					$pj['filename'] = preg_replace("/[: ']/", "_", $pj['filename']);
					$pj['filename'] = str_replace('/', '_', $pj['filename']);
					$pj['filename'] = replace_accents($pj['filename']);
					if ($pj['is_attachment']) {
						$bodyPj .= "\n " . "PJs: " . $pj['filename'];
					}

//					if( base64_decode($filename->bodyHtml) === $pj['attachment']) {
//						$filename->bodyHtml = '';
//						$bodyForFile = '';
//					}
				}
				$body = $bodyFrom . "\n" .  html_entity_decode(Sanitize::clean($bodyPj, array('encode', false))) . "\n\n". $body;
			}
			else {
				$body = $bodyFrom . "\n\n". html_entity_decode($body);
			}


			if(!mb_check_encoding( $body, 'utf-8' ))  {
				$body = utf8_encode( $body );
			}

			if( mb_detect_encoding($body) === 'ASCII' ) {
				$body = quoted_printable_decode($body);
			}

			if( strpos( $body, '=C3' ) !== false ) {
				$body = quoted_printable_decode($body);
			}

			$objet = (!empty($filename->body) ? ( $body ) : null);
			if( !Configure::read('ScanMail.GetObjet') ) {
				$objet = '';
			}

			$data = array(
				'Courrier' => array(
					'name' => '[MAIL] ' . preg_replace( '/[: "]/', "_", $filename->subject ),
					'objet' => $objet,
					'reference' => $referenceUnique,
					'type_id' => !empty($typeId) ? ( $typeId ) : null,
					'soustype_id' => !empty($soustypeId) ? $soustypeId : null,
					'desktop_creator_id' => !empty($desktopCreatorId[0]) ? $desktopCreatorId[0] : null
				)
			);

			$this->begin();
			$this->create($data);

			if ($this->save()) {
				$courrierId = $this->id;
				$return['CodeRetour'] = "CREATED";
				$return['Message'] = "Le flux a été créé";
				$return['Severite'] = "normal";
				$return['CourrierId'] = $courrierId;

				$this->commit();

				//ajout dans les bannette + notifications par mail
				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						//ajout dans la bannette de chacun des aiguilleurs
						if ($desktops[$i]['Desktop']['profil_id'] == DISP_GID) {
							$this->Bancontenu->add($desktopId[$i], BAN_AIGUILLAGE, $courrierId, false , $userId);
						} else if ($desktops[$i]['Desktop']['profil_id'] == INIT_GID) {
							$this->Bancontenu->add($desktopId[$i], BAN_DOCS, $courrierId, false , $userId);
						}

						// Notification par mail de la génération du flux
						$user_id = $this->User->userIdByDesktop($desktopId[$i]);
						$typeNotif = $this->User->find(
							'first', array(
								'fields' => array('User.typeabonnement'),
								'conditions' => array(
									'User.id' => $user_id,
									'User.active' => true
								),
								'contain' => false
							)
						);
						$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_xnew');
						for ($d = 0; $d < count($desktopId); $d++) {
							$notifyDesktops[$desktopId[$d]] = array($courrierId);
							$notified = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopId[$i], $notifyDesktops);
						}

						$notificationId = $this->Notifieddesktop->Notification->id;
						if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
							$this->User->saveNotif($courrierId, $user_id, $notificationId, 'aiguillage');
						} else {
							$this->User->notifier($courrierId, $user_id, $notificationId, 'aiguillage');
						}
					}
				}

				$filename->subject = replace_accents($filename->subject);
				$filename->subject = preg_replace( "/[: ']/", "_", $filename->subject);
				$filename->subject = str_replace( '°', '_', $filename->subject);
				$filename->subject = str_replace( '!', '_', $filename->subject);
				$filename->subject = str_replace( '/', '_', $filename->subject);
				$filename->subject = str_replace( '«', '_', $filename->subject);
				$filename->subject = str_replace( '»', '_', $filename->subject);
				$filename->subject = $filename->subject.'_sujet'; // afin que la PJ n'écrase pas le doc principal si les noms sont identiques
				if( mb_detect_encoding($filename->subject) == 'UTF-8' ) {
					$filename->subject = str_replace( '?', '_', utf8_decode($filename->subject));
				}
//$this->log( $bodyForFile );
				$htmlBodyTruncated = strstr( $filename->bodyHtml, '<html>' );
				if (isset($filename->pj) && !empty($filename->pj) && ( !empty($filename->pj[0]['filename']) || !empty($filename->pj[1]['filename']) ) ) {
					$this->_genFileAssocMail($filename->pj, $courrierId, $conn );
$this->log( 'avec PJ mais sans HTML' );
					if( !Configure::read('Mail.Html') && !empty( $bodyForFile ) && ( strpos($bodyForFile, '<!DOCTYPE') !== false || strpos($bodyForFile, '<!doctype') !== false  || strpos($bodyForFile, '<html') !== false || strpos($bodyForFile, '<HTML') !== false  || strpos($bodyForFile, '<div') !== false || strpos($bodyForFile, '<DIV') !== false) ) {
						$this->_genFileAssocMailWithoutpj($filename->subject, $bodyForFile, $courrierId, $conn, true, $filename);
					}
					else {
						if( !empty( $bodyForFile ) && ( strpos($bodyForFile, '<!DOCTYPE') !== false || strpos($bodyForFile, '<!doctype') !== false  || strpos($bodyForFile, '<html') !== false || strpos($bodyForFile, '<HTML' ) ) ) {
							$this->_genFileAssocMailWithoutpj($filename->subject, $bodyForFile, $courrierId, $conn, true, $filename);
						}
						else if( !empty( $htmlBodyTruncated ) ){
							$this->_genFileAssocMailWithoutpj($filename->subject, $htmlBodyTruncated, $courrierId, $conn, true, $filename);
						}
						else if(isset($filename->bodyHtml) && !empty($filename->bodyHtml)) {
							if (strpos($filename->bodyHtml, '<html>') !== false) {
								$body = strstr($filename->bodyHtml, '<html>');
								$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
							}
							else if(strpos( $filename->bodyHtml, '<div>') !== false) {
								$body = $bodyForFile;
								$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
							}
							else if (empty(base64_decode($filename->bodyHtml, true)) && strpos($filename->bodyHtml, 'base64') === false) { // cas des pj non présentent dans le bodyHtml, on envoie le bodyHtml
								$this->_genFileAssocMailWithoutpj($filename->subject, $filename->bodyHtml, $courrierId, $conn, true, $filename);
							} else if (!empty(base64_decode($filename->bodyHtml, true)) || strpos($filename->bodyHtml, 'base64') !== false) {// cas des pj présentent das le bodyHtml, on envoie le body{
								$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
							}
						}
						else {
							$this->_genFileAssocMailWithoutpj($filename->subject, (!empty($filename->body) ? ($body) : null), $courrierId, $conn, true, $filename);
						}
					}
				}
				else {
$this->log( 'sans PJ mais sans HTML' );
					if( !Configure::read('Mail.Html') && !empty( $bodyForFile ) && ( strpos($bodyForFile, '<!DOCTYPE') !== false || strpos($bodyForFile, '<!doctype') !== false  || strpos($bodyForFile, '<html') !== false || strpos($bodyForFile, '<HTML') !== false   || strpos($bodyForFile, '<div') !== false || strpos($bodyForFile, '<DIV') !== false) ) {
						$this->_genFileAssocMailWithoutpj($filename->subject, $bodyForFile, $courrierId, $conn, true, $filename);
					}
					else {
						if( !empty( $bodyForFile ) && ( strpos($bodyForFile, '<!DOCTYPE') !== false || strpos($bodyForFile, '<!doctype') !== false  || strpos($bodyForFile, '<html') !== false || strpos($bodyForFile, '<HTML' ) ) ) {
							$this->_genFileAssocMailWithoutpj($filename->subject, $bodyForFile, $courrierId, $conn, true, $filename);
						}
						else if( !empty( $htmlBodyTruncated ) ){
							$this->_genFileAssocMailWithoutpj($filename->subject, $htmlBodyTruncated, $courrierId, $conn, true, $filename);
						}
						else if(isset($filename->bodyHtml) && !empty($filename->bodyHtml)) {
							if(strpos( $filename->bodyHtml, '<html>') !== false) {
								$body = strstr( $filename->bodyHtml, '<html>' );
								$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
							}
							else if(strpos( $filename->bodyHtml, '<div') !== false) {
//								$body = $bodyForFile;
								$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
							}
							else if( empty(base64_decode($filename->bodyHtml, true) ) && strpos($filename->bodyHtml, 'base64') === false ){ // cas des pj non présentent dans le bodyHtml, on envoie le bodyHtml
								$this->_genFileAssocMailWithoutpj($filename->subject, $filename->bodyHtml, $courrierId, $conn, true, $filename);
							}
							else if( !empty(base64_decode($filename->bodyHtml, true)) || strpos($filename->bodyHtml, 'base64') !== false){ // cas des pj présentent das le bodyHtml, on envoie le body
								$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
							}
						}
						else {
							$this->_genFileAssocMailWithoutpj($filename->subject, $body, $courrierId, $conn, true, $filename);
						}
					}
				}
			} else {
				$this->rollback();
			}
		}

		return $return;
	}

	/**
	 * Génération d'un document lié (à partir du script lancé à la main)
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileAssocMail($fichiers_associes, $fluxId, $conn) {
		if( empty($conn) ) {
			$conn = CakeSession::read('Auth.Collectivite.conn');
		}

		foreach ($fichiers_associes as $i => $pj) {
			if ($pj['is_attachment'] == true) {
				$finfo = new finfo(FILEINFO_MIME_TYPE);

				$sql = "SELECT nextval('documents_name_seq'); /*" . mktime(true) . " " . $this->_lastSequenceDocument . "*/";
				$fileMimeType = $pj['subtype'];


				if ($fileMimeType == "application/pdf" || $fileMimeType == "PDF"  || $fileMimeType == "pdf" || $fileMimeType == "DOWNLOAD") {
					$fileExt = "pdf";
				} else if ($fileMimeType == "application/vnd.oasis.opendocument.text" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.TEXT" || $fileMimeType == "OCTET-STREAM") {
					$fileExt = "odt";
				} else if ($fileMimeType == "image/bmp") {
					$fileExt = "bmp";
				} else if ($fileMimeType == "image/tiff") {
					$fileExt = "tif";
				} else if ($fileMimeType == "text/plain") {
					$fileExt = "txt";
				} else if ($fileMimeType == "application/vnd.ms-office") {
					$fileExt = "doc";
				} else if ($fileMimeType == "application/msword") {
					$fileExt = "doc";
				} else if ($fileMimeType == "application/rtf") {
					$fileExt = "rtf";
				} else if ($fileMimeType == "image/png" || $fileMimeType == "PNG") {
					$fileExt = "png";
				} else if ($fileMimeType == "image/jpeg" || $fileMimeType == "JPEG") {
					$fileExt = "jpg";
				} else if ($fileMimeType == "application/vnd.oasis.opendocument.spreadsheet" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.SPREADSHEET") {
					$fileExt = "ods";
				} else if ($fileMimeType == "VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT" || $fileMimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
					$fileExt = "docx";
				} else if ($fileMimeType == "GIF" || $fileMimeType == "gif") {
					$fileExt = "gif";
				} else if ($fileMimeType == "application/zip" || $fileMimeType == "zip" || $fileMimeType == "ZIP") {
					$fileExt = "zip";
				} else if ($fileMimeType == '') {
					$fileMimeType = 'txt';
					$fileExt = "rtf";
				}

				if ($fileMimeType == "application/pdf" || $fileMimeType == "PDF"  || $fileMimeType == "pdf" || $fileMimeType == "DOWNLOAD") {
					$mime = "application/pdf";
				} else if ($fileMimeType == "VND.OASIS.OPENDOCUMENT.TEXT" || $fileMimeType == "OCTET-STREAM") {
					$mime = "application/vnd.oasis.opendocument.text";
				} else if ($fileMimeType == "application/vnd.oasis.opendocument.spreadsheet" || $fileMimeType == "VND.OASIS.OPENDOCUMENT.SPREADSHEET") {
					$mime = "application/vnd.oasis.opendocument.spreadsheet";
				} else if ($fileMimeType == "image/png" || $fileMimeType == "PNG") {
					$mime = "image/png";
				} else if ($fileMimeType == "image/jpeg" || $fileMimeType == "JPEG") {
					$mime = "image/jpeg";
				}

				// si, le contneu de la PJ présente du PDF en entête alors c'est un PDF
				if ( stripos( $pj['attachment'], '%PDF-1') === 0 ) {
					$mime = 'application/pdf';
					$fileExt = "pdf";
				}

				$numSeqDoc = $this->query($sql);
				if ($numSeqDoc === false) {
					return null; // FIXME: Throw exception
				}
				$this->_lastSequenceDocument = $numSeqDoc[0][0]['nextval'];

				$flux = $this->find(
					'first',
					array(
						'recursive' => -1,
						'conditions' => array('Courrier.id' => $fluxId),
						'contain' => false
					)
				);
				$subjectFormat = strpos($pj['filename'], '=?UTF-8?B?');
				if ($subjectFormat !== false) {
					$pj['filename'] = mb_decode_mimeheader($pj['filename']);
				}

				// chemin du stockage
				$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
				$pj['filename']  = preg_replace( "/[: ']/", "_", $pj['filename']  );
				$pj['filename'] = str_replace( '/', '_', $pj['filename'] );
				$pj['filename'] = replace_accents($pj['filename']);
				$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$pj['filename']}";
				file_put_contents( $file, $pj['attachment']);
				$path = $file;

				$doc[] = array(
					'Document' => array(
						'name' => isset( $pj['filename'] ) ? $pj['filename'] : 'mail.pdf',
						'courrier_id' => $fluxId,
						'size' => $pj['size'],
						'mime' => !empty($mime) ? $mime : $fileMimeType,
						'ext' => @$fileExt,
						'main_doc' => false,
						'path' => $path
					)
				);
			}
		}

		$this->setDataSource(Configure::read('conn'));
		$this->Document->setDataSource(Configure::read('conn'));
		$this->Document->create();
		if (!empty($doc)) {
//			$doc[0]['Document']['main_doc'] = true;
			$success = $this->Document->saveAll($doc, array('validate' => 'first', 'atomic' => false));

			if ($success) {
				echo 'La pièce jointe a été envoyée.';
			} else {
				echo "La pièce jointe n'a pas été envoyée.";
			}
		} else {
			echo "Aucune pièce jointe à insérer";
		}
	}

	public function notifier($courrier = array()) {

		$courrierId = $courrier['Courrier']['id'];
		$success = array();
		// Notification par mail de la génération du flux
		$destinataires = $this->Traitement->whoIsNext($courrierId, 'current');
		if (!empty($destinataires)) {
			foreach ($destinataires as $destinataire_id) {
				if ($destinataire_id != -1) {
					$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
					$desktopsIds = $this->Desktopmanager->getDesktops($destinataire_id);
					foreach ($desktopsIds as $deskId) {
						$userId = $this->User->userIdByDesktop($deskId);
						if (!empty($userId)) {
							$typeNotif = $this->User->find(
								'first', array(
									'fields' => array('User.typeabonnement'),
									'conditions' => array('User.id' => $userId),
									'contain' => false
								)
							);

							if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
								$success[] = $this->User->saveNotif($courrierId, $userId, null, 'retard_validation') && $success;
							} else {
								$success[] = $this->User->notifier($courrierId, $userId, null, 'retard_validation') && $success;
							}

							if (in_array(true, $success)) {
								$this->updateAll(
									array(
										'Courrier.mail_retard_envoye' => true
									), array(
										'Courrier.id' => $courrierId
									)
								);
							}
						} else {
							$success = false;
						}
					}
				} else {
					$success = false;
				}
			}
		}
	}

	/**
	 * Fonciton permettant de rechercher les flux de mon historique selon certains critères
	 *
	 * @param type $criteres
	 * @return array()
	 */
	public function search($criteres) {
		/// Conditions de base
		$conditions = array();
		// Critères sur le courrier
		if (isset($criteres['Courrier']['reference']) && !empty($criteres['Courrier']['reference'])) {
			$conditions[] = 'Courrier.reference ILIKE \'' . $this->wildcard('%' . $criteres['Courrier']['reference'] . '%') . '\'';
		}

		if (isset($criteres['Courrier']['name']) && !empty($criteres['Courrier']['name'])) {
			$conditions[] = 'NOACCENTS_UPPER(Courrier.name) LIKE \'' . $this->wildcard('%' . noaccents_upper( $criteres['Courrier']['name'] ) . '%') . '\'';
		}

		if (isset($criteres['Courrier']['datereception']) && !empty($criteres['Courrier']['datereception'])) {
			if (isset($criteres['Courrier']['datereceptionfin']) && !empty($criteres['Courrier']['datereceptionfin'])) {
				if( isset($criteres['Courrier']['datereception'])  && !empty($criteres['Courrier']['datereception']) ) {
					$dateReception = $criteres['Courrier']['datereception'];
					if( strpos( $dateReception,  '/'  ) != 0 ) {
						$debut = explode( '/', $dateReception );
						$day = $debut[0];
						$month = $debut[1];
						$year = $debut[2];
						$dateDebut = $year.'-'.$month.'-'.$day;
						$criteres['Courrier']['datereception'] = $dateDebut;
					}
				}
				if( isset($criteres['Courrier']['datereceptionfin'])  && !empty($criteres['Courrier']['datereceptionfin']) ) {
					$dateReceptionFin = $criteres['Courrier']['datereceptionfin'];
					if( strpos( $dateReceptionFin,  '/'  ) != 0 ) {
						$fin = explode( '/', $dateReceptionFin );
						$day = $fin[0];
						$month = $fin[1];
						$year = $fin[2];
						$dateFin = $year.'-'.$month.'-'.$day;
						$criteres['Courrier']['datereceptionfin'] = $dateFin;
					}
				}

				$conditions[] = array('AND' => array(array('Courrier.datereception >=' => $criteres['Courrier']['datereception']), array('Courrier.datereception <=' => $criteres['Courrier']['datereceptionfin'])));
			} else {
				$conditions[] = array('Courrier.datereception' => $criteres['Courrier']['datereception']);
			}
		}

		//critères Type
		if (!empty($criteres['Type']['Type'])) {
			$type_conditions = array();
			foreach ($criteres['Type']['Type'] as $type_id) {
				$type_conditions[] = array('Type.id' => $type_id);
			}
			$conditions[] = array('OR' => $type_conditions);
		}

		//critères Soustype
		if (!empty($criteres['Soustype']['Soustype'])) {
			$soustype_conditions = array();
			foreach ($criteres['Soustype']['Soustype'] as $soustype_id) {
				$soustype_conditions[] = array('Courrier.soustype_id' => $soustype_id);
			}
			$conditions[] = array('OR' => $soustype_conditions);
		}

		if ( isset($criteres['Courrier']['etat'] ) && ($criteres['Courrier']['etat'] != '' ) ) {
			$etat = $criteres['Courrier']['etat'];
			$sqBancontenu = $this->Bancontenu->sqDerniereBannette( 'Courrier.id', $etat );
			$conditions[] = array('Bancontenu.etat' => $criteres['Courrier']['etat']);
		}
		return $conditions;
	}

	/**
	 * Vérifie que les paramètres sont bons.
	 *
	 * @param mixed $checks
	 * @param mixed $references
	 * @return boolean
	 */
	protected function _checkParams($checks, $references) {
		return ( is_array($checks) && !empty($references) );
	}

	/**
	 * Compare la date en valeur par-rapport à la date de référence, suivant
	 * le comparateur.
	 *
	 * @param Model $Model
	 * @param array $check
	 * @param string $reference
	 * @param string $comparator Une valeur parmi: >, <, ==, <=, >=
	 * @return boolean
	 */
	public function compareDates($check, $reference, $comparator) {
		if (!$this->_checkParams($check, $reference)) {
			return false;
		}

		$check_value = strtotime(Hash::get($check, 'datereception'));
		$reference_value = strtotime(Hash::get($this->data, "{$this->alias}.{$reference}"));

		if (empty($reference_value) || empty($check_value)) {
			return true;
		}

		$seconds = $reference_value - $check_value;
		$days = floor($seconds / 3600 / 24);

		if ($days > 0) {
			return in_array($comparator, array('<', '<='), true);
		} else if ($days < 0) {
			return in_array($comparator, array('>', '>='), true);
		}

		return in_array($comparator, array('==', '<=', '>='), true);
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getMailsSent($id) {
		$qdMainDoc = array(
			'fields' => array_merge(
				$this->Sendmail->fields(), $this->Sendmail->Desktop->fields(), array(
					'Document.name'
				)
			),
			'conditions' => array(
				'Sendmail.courrier_id' => $id
			),
			'joins' => array(
				$this->Sendmail->join('Desktop'),
				$this->Sendmail->join('Document')
			),
			'contain' => false,
			'order' => 'Sendmail.created DESC'
		);

		$sendsmails = $this->Sendmail->find('all', $qdMainDoc);

		return $sendsmails;
	}

	/**
	 * Fonctionnalité de numérisation de masse (création de flux)
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $collectiviteId identifiant de la collectivité
	 * @param base64 $fichier_associe contenu du fichier lié
	 * @param string $username identifiant de connexion de l'utilisateur cible
	 * @return array
	 */
	public function remoteCreateDesktopmanager($fichier_associe, $username, $dispDesktop = true, $desktopId = null, $filename, $conn, $fluxName = null) {

		$return = array(
			'CodeRetour' => 'KO',
			'CourrierId' => -1,
			'Message' => 'Impossible de créer le flux.',
			'Severite' => 'important',
			'DocumentId' => ''
		);
		$userId = CakeSession::read('Auth.User.id');

		$noRoleErrMsg = "Le rôle n'existe pas.";

		if (empty($desktopId)) {
			$GID = $dispDesktop ? DISP_GID : INIT_GID;
			$BAN = $dispDesktop ? BAN_AIGUILLAGE : BAN_DOCS;
			$noRoleErrMsg = $dispDesktop ? "L'utilisateur n'a pas de rôle Aiguilleur." : "L'utilisateur n'a pas de rôle Initiateur.";
		} else {
			// On utilise ici le desktopmanager pour obtenir la liste des desktops associés
			$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
			$desktopsIds[] = $this->Desktopmanager->getDesktops($desktopId);
			foreach ($desktopsIds as $i => $desktopId) {
				$desktops = $this->Bancontenu->Desktop->find(
					'all', array(
						'conditions' => array(
							'Desktop.id' => $desktopsIds[$i]
						),
						'contain' => false
					)
				);

				if ($desktops[$i]['Desktop']['profil_id'] == DISP_GID) {
					$BAN = BAN_AIGUILLAGE;
				} else if ($desktops[$i]['Desktop']['profil_id'] == INIT_GID) {
					$BAN = BAN_DOCS;
				}
				$desktop = $this->Bancontenu->Desktop->find(
					'first', array(
						'conditions' => array(
							'Desktop.id' => $desktopsIds[$i]
						),
						'contain' => array(
							'User'
						)
					)
				);
			}
		}


		if (empty($desktopId)) {
			$return['CodeRetour'] = "KONODESKTOP";
			$return['Message'] .= $noRoleErrMsg;
		} else if (empty($BAN)) {
			$return['CodeRetour'] = "KONOBAN";
			$return['Message'] .= 'Aucun bannette renseignée';
		} else {
			if (!is_array($filename)) {
				$pos_point = strpos($filename, '.');
				$courrierName = substr($filename, 0, $pos_point);
			}
			$referenceUnique = $this->genReferenceUnique();

			$userConnected = !empty($desktop['User'][0]['id']) ? $desktop['User'][0]['id'] : null;
			if( Configure::read( 'Scan.UserCreatorEmpty') ) {
				$userConnected = null;
			}
			if ($fluxName != null) {
				$data = array(
					'Courrier' => array(
						'name' => $fluxName . '_' . date('Y-m-d_H:i:s'),
						'reference' => $referenceUnique,
						'user_creator_id' => $userConnected,
						'desktop_creator_id' => !empty($desktop['Desktop']['id']) ? $desktop['Desktop']['id'] : null
					)
				);
			} else {
				$data = array(
					'Courrier' => array(
						'name' => $courrierName . '_' . date('Y-m-d_H:i:s'),
						'reference' => $referenceUnique,
						'user_creator_id' => $userConnected,
						'desktop_creator_id' => !empty($desktop['Desktop']['id']) ? $desktop['Desktop']['id'] : null
					)
				);
			}
			$this->begin();
			$this->create($data);
			if ($this->save()) {
				$courrierId = $this->id;
				$return['CodeRetour'] = "CREATED";
				$return['Message'] = "Le flux a été créé";
				$return['Severite'] = "normal";
				$return['CourrierId'] = $courrierId;

				$this->commit();

				//ajout dans les bannette + notifications par mail
				foreach ($desktopsIds as $i => $desktopId) {
					for ($i = 0; $i < count($desktopId); $i++) {
						//ajout dans la bannette de chacun des aiguilleurs
						if ($desktops[$i]['Desktop']['profil_id'] == DISP_GID) {
							$this->Bancontenu->add($desktopId[$i], BAN_AIGUILLAGE, $courrierId, false , $userId);
						} else if ($desktops[$i]['Desktop']['profil_id'] == INIT_GID) {
							$this->Bancontenu->add($desktopId[$i], BAN_DOCS, $courrierId, false , $userId);
						}

						// Notification par mail de la génération du flux
						$user_id = $this->User->userIdByDesktop($desktopId[$i]);
						$typeNotif = $this->User->find(
							'first', array(
								'fields' => array('User.typeabonnement'),
								'conditions' => array('User.id' => $user_id),
								'contain' => false
							)
						);
						$notifInfos = $this->Notifieddesktop->Notification->setNotification('ban_xnew');
						for ($d = 0; $d < count($desktopId); $d++) {
							$notifyDesktops[$desktopId[$d]] = array($courrierId);
							$notified = $this->Notifieddesktop->Notification->add($notifInfos['name'], $notifInfos['description'], $notifInfos['typeNotif'], $desktopId[$i], $notifyDesktops);
						}
						$notificationId = $this->Notifieddesktop->Notification->id;
						if ($typeNotif['User']['typeabonnement'] == 'quotidien') {
							$this->User->saveNotif($courrierId, $user_id, $notificationId, 'aiguillage');
						} else {
							$this->User->notifier($courrierId, $user_id, $notificationId, 'aiguillage');
						}
					}
				}


				// Association des fichiers aux flux générés
				if (is_array($fichier_associe)) {
					for ($j = 0; $j < count($fichier_associe); $j++) {
						if ($j != 0) {
							$this->_genFileAssoc($fichier_associe[$j], $courrierId, $return, false, $filename, $conn);
						} else {
							$this->_genFileAssoc($fichier_associe[$j], $courrierId, $return, true, $filename, $conn);
						}
					}

					$codeRetour = str_split($return['CodeRetour'], 13);
					$return['CodeRetour'] = $codeRetour[0];
				} else {
					$this->_genFileAssoc($fichier_associe, $courrierId, $return, true, $filename, $conn);
				}
			} else {
				$this->rollback();
			}
		}
		return $return;
	}

	/**
	 * Fonction retournant l'année du dernier numéro de référence présent en BDD
	 * Ceci nous permet de savoir à quel moment la séquence courriers_reference_seq doit être remise à zéro
	 * @return type
	 */
	public function lastNumReference() {
		$subquery = $this->sq(
			array(
				'fields' => array(
					'courriers.reference'
				),
				'alias' => 'courriers',
				'conditions' => array(
					"courriers.reference IS NOT NULL"
				),
				'order' => array("courriers.reference DESC"),
				'limit' => 1
			)
		);
		// requête pour obtenir le résultat
		$lastNumReference = $this->query($subquery);
		// on extrait la valeur prise par le numéro de référence
		$valueLastReference = Hash::get($lastNumReference, '0.courriers.reference');
		// on ne conserve que les 4 premiers caractères 2016000001 => 2016
		$valueLastReferenceOnlyYear = substr($valueLastReference, 0, -6);
		// on retourne l'année du dernier flux référencé du coup
		return $valueLastReferenceOnlyYear;
	}

	/**
	 * Fonction remontant le service auquel le flux est lié
	 * @param type $id
	 */
	public function getMyService($id) {


		/* Liste des profils auxquels l'utilisateur est lié
          select id, name from desktops where id in (
          select desktop_id from desktops_users where user_id=5
          union
          select desktop_id from users where id=5
          );
          7, 26, 2
          Liste des services auxquels les bureaux de l'utyilsiateur connecté sont liés
          select service_id from desktops_services where desktop_id in (2,7,26);

          2, 19
         */
	}

	/**
	 * Génération d'un document lié (à partir du script lancé à la main)
	 *
	 * @access private
	 * @param base64 $fichier_associe contenu du document lié
	 * @param integer $fluxId identifiant du flux
	 * @param array $return valeur de retour
	 * @return void
	 */
	private function _genFileAssocMailWithoutpj($file_name, $file_body, $fluxId, $conn, $isHtml = false, $filename) {

		require_once 'XML/RPC2/Client.php';
		// initialisations
		$ret = array();
		$convertorType = Configure::read('CONVERSION_TYPE');
//$this->log( $file_body );
		if (empty($convertorType)) {
			$ret['resultat'] = false;
			$ret['info'] = __('Type du programme de conversion non déclaré dans le fichier de configuration de web-GFC', true);
			return $ret;
		}

		if( !Configure::read('Mail.MultipleBase64Data') ) {
			if (strpos($file_body, '=C3') !== false) {
				$file_body = quoted_printable_decode($file_body);
				$filename->bodyOtherHtml = quoted_printable_decode($filename->bodyOtherHtml);
			}
		}

		$options = array(
			'uglyStructHack' => true
		);

		// En cas d'encodage "connu"
		if (strpos($file_body, 'JVB') !== false) {
			$file_body = base64_decode($file_body);
		}

		if( !empty( base64_decode($file_body, true)) ) {
			$file_body = base64_decode($file_body);
		}

		if( strpos( $file_body, '%PDF') !== false ) {
			$pdfMail = $file_body;
		}
		else {

			// Autre informations issues des mails
			if (
				!empty($filename->bodyOtherHtml)  && (
					stripos( $filename->bodyOtherHtml, '--_00') === 0
					|| stripos( $filename->bodyOtherHtml, '<html') === 0
					|| stripos( $filename->bodyOtherHtml, '--00') === 0
				)
				&& !str_starts_with( $filename->bodyOtherHtml, '<html')
				&& !str_starts_with( $filename->bodyOtherHtml, '<!DOCTYPE')
				&& !str_starts_with( $filename->bodyOtherHtml, '<!doctype')
				&& !is_base64($filename->bodyOtherHtml)
			) {
				if( strpos( $filename->bodyOtherHtml, '<div') !== false ) {
					$file_body = '<div ' . get_string_between($filename->bodyOtherHtml, '<div', '--');
				}
			}


			// PJS
			$pjsdata = [];
			foreach( $filename->pj as $pj ) {
				if( $pj['is_attachment'] == 1 ) {
					$pj['filename'] = str_replace( ' ', '_', replace_accents( $pj['filename'] ) ) ;
					$pj['filename'] = str_replace( '’', '_', $pj['filename'] ) ;
					$pj['filename'] = str_replace( 'ee', 'e', $pj['filename'] ) ;
					$pjsdata[$pj['filename']] = [
						'name' => $pj['filename'],
						'attachment' => $pj['attachment']
					];
				}
			}

			$pjsOtherdata = [];
			$o = $filename->bodyOtherHtml;
			$o2 = $filename->bodyOtherHtml;

			preg_match_all('/Content-Description: [^.]+.[gif|png]+/i', $o2, $matchesName);
			preg_match_all('/Content-ID: <(.*)\s\s[^.]+--/', $o2, $matchesContent);
			$imageParsed = [];
			foreach ($matchesContent[0] as $dataImage) {
				$imageParsed[] = get_string_between($dataImage, '>', '--');
				$imageNameToParsed[] = get_string_between($dataImage, '<', '>');
			}
			foreach( $matchesName as $imageOtherHtml ) {
				foreach( $imageOtherHtml as $d => $todisplayOtherHtml ) {
					$fileName = substr($todisplayOtherHtml, strlen('Content-Description: '));

					$pjsOtherdata[$d]  = [
						'name' => $fileName,
						'content' => $imageParsed[$d],
						'nameToParsed' => $imageNameToParsed[$d]
					];
				}
			}
//$this->log($pjsOtherdata);
			// 	on supprime les guillemets double
			$file_name = str_replace('"', '', $file_name);
			$file_name = str_replace('/', '-', $file_name);
			if (strpos($file_body, '<!DOCTYPE') !== false || strpos($file_body, '<!doctype') !== false || strpos($file_body, '<HTML') !== false || strpos($file_body, '<html') !== false || strpos($file_body, '<DIV') !== false || strpos($file_body, '<div') !== false  || strpos($file_body, '<meta') !== false  ) {
				$htmTemp = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $fluxId.$conn.'_sortie.html';
				$file_body = quoted_printable_decode($file_body);
				// On regarde si le simages sont en Content-ID
				if( strpos($file_body, 'cid') !== false ) {
					$file_body = str_replace( ['src="cid:', 'src=3D"cid:'], 'src="', $file_body);

					if( !empty( $filename->bodyOtherHtml )  ) {
						$imageParsed = get_string_between($filename->bodyOtherHtml, 'Content-ID: <', '>');
						if( !empty($imageParsed)) {
							$imageBase64value = get_string_between($filename->bodyOtherHtml, 'X-Attachment-Id: ' . $imageParsed, '--');
							if( !empty($imageBase64value) ) {
								$imageFilename = get_string_between($filename->bodyOtherHtml, 'filename="', '"');
								$imagePath = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $imageFilename;
								file_put_contents($imagePath, base64_decode($imageBase64value));
							}
						}
//$this->Log($file_body);
						preg_match_all('/<img[^>]+>/i', $file_body, $resultOtherHtml);
						foreach( $resultOtherHtml as $imageOtherHtml ) {
							foreach( $imageOtherHtml as $todisplayOtherHtml ) {
								$parsedOtherHtml = get_string_between($todisplayOtherHtml, 'src="', '"');
								$parsedAltOtherHtml = get_string_between($todisplayOtherHtml, 'alt="', '"');
								$parsedAltOtherHtml = str_replace( ' ', '_', replace_accents($parsedAltOtherHtml) ) ;
								$parsedAltOtherHtml = str_replace( '’', '_', $parsedAltOtherHtml ) ;
								$parsedOtherHtml = str_replace('cid:', '', $parsedOtherHtml);
								if( strpos( $parsedOtherHtml, '/' )  === false ) {

									// filenbame->bodyOtherHtml
									$imageBase64valueOtherhtml = get_string_between($filename->bodyOtherHtml, 'Content-ID: <' . $parsedOtherHtml . '>', '--');
									if( !empty($imageBase64valueOtherhtml) ){
										$dataBase64ToDisplayOtherHtml = substr($imageBase64valueOtherhtml, strlen('Content-Transfer-Encoding: base64') + 2);
										if( empty($dataBase64ToDisplayOtherHtml) ) {
											$dataBase64ToDisplayOtherHtml = get_string_between($filename->bodyOtherHtml, 'X-Attachment-Id: ' . $parsedOtherHtml, '--');
										}

										if( !empty($dataBase64ToDisplayOtherHtml)) {
											if( Configure::read('Mail.MultipleBase64Data') ) {
												if (count($pjsOtherdata) == 1) {
													foreach ($pjsOtherdata as $pj) {
														if (strpos($pj['name'], "=?") !== FALSE) {
															$pj['name'] = $parsedOtherHtml;
														}
														$imagePathAlt = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $pj['name'];
														file_put_contents($imagePathAlt, base64_decode($imageBase64valueOtherhtml));
														$file_body = str_replace(['src="' . $parsedOtherHtml, 'src="' . $parsedOtherHtml], 'src="' . $pj['name'], $file_body);
													}
												} else {
													foreach ($pjsOtherdata as $pj) {
														if (strpos($pj['name'], "=?") !== FALSE) {
															$pj['name'] = $parsedOtherHtml;
														}
														$imagePathAlt = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $pj['name'];
														file_put_contents($imagePathAlt, base64_decode($pj['content']));
														$file_body = str_replace(['src="' . $pj['nameToParsed'], 'src="' . $pj['nameToParsed']], 'src="' . $pj['name'], $file_body);
													}
												}
											}
											$imagePathOtherHtml = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $parsedOtherHtml;
											file_put_contents($imagePathOtherHtml, base64_decode($dataBase64ToDisplayOtherHtml));
										}
										else if( !empty($parsedOtherHtml)) {
											foreach($pjsdata as $pjname => $data ) {
												$imagePathAltOtherHtml = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $pjname;
												file_put_contents($imagePathAltOtherHtml, $data['attachment']);
											}
											$file_body = str_replace( ['src="cid:'.$parsedOtherHtml, 'src=3D"cid:'.$parsedOtherHtml], 'src="'.$parsedOtherHtml, $file_body);
										}
									}
									else {
										$imageBase64value = get_string_between($file_body, 'Content-ID: <' . $parsedOtherHtml . '>', '--');
										if( empty($imageBase64value) ) {
											$imageBase64value = get_string_between($file_body, 'X-Attachment-Id: ' . $parsedOtherHtml, '--');
										}

										if( !empty($imageBase64value)) {
											$imagePath = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $parsedOtherHtml;
											file_put_contents($imagePath, base64_decode($imageBase64value));
										}
										else if( !empty($parsedOtherHtml)) {
											foreach($pjsdata as $pjname => $data ) {
												$imagePathAlt = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $pjname;
												file_put_contents($imagePathAlt, $data['attachment']);
											}
											if (($pos = strpos($parsedOtherHtml, "@")) !== FALSE) {
												$parsedAlt = substr($parsedOtherHtml, 0, $pos);
											}
											else {
												$parsedAlt = $parsedAltOtherHtml;
											}

											$file_body = str_replace( ['src="'.$parsedOtherHtml, 'src=3D"'.$parsedOtherHtml], 'src="'.$parsedAlt, $file_body);
										}
									}
								}
							}
						}
					}
				}
//$this->log($file_body);
				file_put_contents($htmTemp, $file_body);
				$fileoutput = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $fluxId.$conn.'_sortie.pdf';
				$urlHtmlTmp = APP . DS . WEBROOT_DIR . DS . 'files' . DS . $fluxId.$conn.'_sortie.html';
				$encoding = '--encoding utf-8';
				$bodyIso = get_string_between($filename->bodyOtherHtml, 'Content-Type: text/html; charset="iso-8859-1"', '--');
				if( !empty( $bodyIso) ) {
					$encoding = '';
				}
				$cmd = "/usr/local/bin/wkhtmltopdf $encoding --enable-external-links --enable-local-file-access " . $urlHtmlTmp . " " . $fileoutput;
				shell_exec(escapeshellcmd($cmd));

				$pdfMail = file_get_contents($fileoutput);
				$pdfMail = str_replace( '%253a', '%3A', $pdfMail);
				$pdfMail = str_replace( '%252f', '%2F', $pdfMail);

				unlink($htmTemp);
				unlink($fileoutput);
				// Nettoyage des fichiers récupérés pour les mails
				$dir = APP . DS . WEBROOT_DIR . DS . 'files/';
				array_map('unlink', glob("{$dir}*.jpg"));
				array_map('unlink', glob("{$dir}*.jpeg"));
				array_map('unlink', glob("{$dir}*.png"));
				array_map('unlink', glob("{$dir}*.pdf"));
				array_map('unlink', glob("{$dir}*.gif"));
				array_map('unlink', glob("{$dir}*@*"));


			} else {
				$url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
				$client = XML_RPC2_Client::create($url, $options);
				try {

//					$file_body = htmlspecialchars_decode($file_body);
//					if(Configure::read('Mail.Confspeciale')) {
//						$file_body = replace_accents($file_body);
//					}
					$file_body = iconv('UTF-8', 'ISO-8859-1//TRANSLIT//IGNORE', $file_body); // A tester
					$result = $client->convertFile(base64_encode($file_body), 'odt', 'pdf', false, true);
					$pdfMail = base64_decode($result);
				} catch (XML_RPC2_FaultException $e) {
					$this->log('Exception #' . $e->getFaultCode() . ' : ' . $e->getFaultString(), 'debug');
					return false;
				}
			}
		}
		$sql = "SELECT nextval('documents_name_seq'); /*" . mktime(true) . " " . $this->_lastSequenceDocument . "*/";


		$mime = "application/pdf";

		$numSeqDoc = $this->query($sql);
		if ($numSeqDoc === false) {
			return null;
		}
		$this->_lastSequenceDocument = $numSeqDoc[0][0]['nextval'];

		$file_name = preg_replace( "/[: ']/", "_", $file_name ). '.pdf';
		$file_name = str_replace( '/', '_', $file_name );
		$file_name = str_replace( '!', '_', $file_name );
		$file_name = str_replace( '°', '_', $file_name );
		$countDoc = $this->Document->find('count', array('conditions' => array('Document.courrier_id' => $fluxId), 'contain' => false));

		$flux = $this->find(
			'first',
			array(
				'recursive' => -1,
				'conditions' => array('Courrier.id' => $fluxId),
				'contain' => false
			)
		);
		// chemin du stockage
		$fileFolder = new Folder(WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'], true, 0777);
		$file = WORKSPACE_PATH . DS . $conn . DS . $flux['Courrier']['reference'] . DS . "{$file_name}";
		file_put_contents( $file, $pdfMail);
		$path = $file;

		$doc[] = array(
			'Document' => array(
				'name' => $file_name,
				'courrier_id' => $fluxId,
				'size' => strlen($pdfMail),
				'mime' => $mime,
				'ext' => 'pdf',
				'main_doc' => (($countDoc == 0)) ? true : false,
				'path' => $path
			)
		);

		$this->setDataSource(Configure::read('conn'));
		$this->Document->setDataSource(Configure::read('conn'));
		$this->Document->create();
		if (!empty($doc)) {
			$doc[0]['Document']['main_doc'] = true;
			$success = $this->Document->saveAll($doc, array('validate' => 'first', 'atomic' => false));
			if ($success) {
				echo 'Le fichier a été généré.';
			} else {
				echo "Le fichier n'a pas été généré.";
			}
		} else {
			echo "Aucun fichier à insérer";
		}
	}

	/**
	 * Récupération du schéma du circuit emprunté par le flux
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id
	 * @throws NotFoundException
	 * @return void
	 */
	public function getCircuitFromCourrier($fluxId) {
		$qdCircuit = array(
			'fields' => array(
				'Circuit.id',
				'Circuit.nom'
			),
			'conditions' => array(
				'Courrier.id' => $fluxId
			),
			'contain' => false,
			'joins' => array(
				$this->join($this->Soustype->alias),
				$this->Soustype->join($this->Soustype->Circuit->alias),
			)
		);
		$circuit = $this->find('first', $qdCircuit);
		$etapes = null;
		$visas = null;
		if (!empty($circuit)) {
			//definition de l identifiant du circuit
			$circuitId = $circuit['Circuit']['id'];
			//recuperation de la structure initiale du circuit
			$qdEtapes = array(
				'fields' => array(
					'Etape.nom',
					'Etape.type',
					'Etape.ordre',
					'Etape.description',
					'Etape.type_document',
					'Etape.inforequired_type_document'
				),
				'contain' => array(
					'Composition.type_validation',
					'Composition.trigger_id',
					'Composition.type_document',
					'Composition.inforequired_type_document',
					'Composition' => array(
						CAKEFLOW_TRIGGER_MODEL => array('fields' => CAKEFLOW_TRIGGER_FIELDS)
					)
				),
				'conditions' => array(
					'Etape.circuit_id' => $circuitId
				),
				'order' => 'Etape.ordre'
			);
			$etapes = $this->Traitement->Circuit->Etape->find('all', $qdEtapes);
		}
		$listOfDesktops = array();
		foreach ($etapes as $etape) {
			foreach ($etape['Composition'] as $composition) {
				$listOfDesktops[] = $composition['trigger_id'];
			}
		}
		return $listOfDesktops;
	}


	/**
	 * Focntion permettant à l'admin (uniquement) de modifier le bancontenu pour repositionner le flux sur un autre profil
	 * @param type $desktopId
	 */
	public function repositionneFlux( $id, $desktopId ) {
		$success = true;
		$userId = CakeSession::read('Auth.User.id');
		$sqBancontenu = $this->Bancontenu->sqDerniereBannette('Courrier.id');
		$courrier = $this->find(
			'first',
			array(
				'fields' => array_merge(
					$this->fields(),
					$this->Bancontenu->fields()
				),
				'conditions' => array(
					'Courrier.id' => $id
				),
				'contain' => false,
				'joins' => array(
					$this->join('Bancontenu', array('type' => 'INNER','conditions' => array("Bancontenu.id IN ( {$sqBancontenu} )") ) )
				)
			)
		);

		$group = $this->Desktop->find(
			'first',
			array(
				'conditions' => array(
					'Desktop.id' => $desktopId
				),
				'contain' => array(
					'Profil'
				),
				'recursive' => -1
			)
		);

		if( $group['Profil']['id'] == 7 ) {
			$banValue = BAN_AIGUILLAGE;
		}
		else if( in_array( $group['Profil']['id'], array(4, 5) ) ) {
			$banValue = BAN_FLUX;
		}
		else if( $group['Profil']['id'] == 3 ) {
			$banValue = BAN_DOCS;
		}

		// On passe le flux comme traité par les agents possédant le flux
		$success = $this->Bancontenu->updateAll(
				array(
					'Bancontenu.etat' => 0
				),
				array(
					'Bancontenu.courrier_id' => $courrier['Bancontenu']['courrier_id'],
					'Bancontenu.etat' => 1
				)
			) && $success;

		// On ajoute le flux dans l'environnement de l'agent cible
		$success = $this->Bancontenu->add($desktopId, $banValue, $courrier['Bancontenu']['courrier_id'], false , $userId) && $success;

		return $success;

	}


	/**
	 * Récupération du schéma du circuit emprunté par le flux
	 *
	 * @logical-group Flux
	 * @logical-group Context
	 * @user-profile User
	 *
	 * @access public
	 * @param integer $id
	 * @throws NotFoundException
	 * @return void
	 */
	public function getCircuitInfos($fluxId) {
		$qdCircuit = array(
			'fields' => array(
				'Circuit.id',
				'Circuit.nom'
			),
			'conditions' => array(
				'Courrier.id' => $fluxId
			),
			'contain' => false,
			'joins' => array(
				$this->join($this->Soustype->alias),
				$this->Soustype->join($this->Soustype->Circuit->alias),
			)
		);
		$circuit = $this->find('first', $qdCircuit);

		$etapes = null;
		$visas = null;
		if (!empty($circuit)) {
			//definition de l identifiant du circuit
			$circuitId = $circuit['Circuit']['id'];

			//recuperation de la structure initiale du circuit
			$qdEtapes = array(
				'fields' => array(
					'Etape.nom',
					'Etape.type',
					'Etape.ordre',
					'Etape.description'
				),
				'contain' => array(
					'Composition.type_validation',
					'Composition.trigger_id',
					'Composition.soustype',
					'Composition' => array(
						'Desktopmanager' => array('fields' => 'Desktopmanager.name')
					)
				),
				'conditions' => array(
					'Etape.circuit_id' => $circuitId
				),
				'order' => 'Etape.ordre'
			);
			$etapes = $this->Traitement->Circuit->Etape->find('all', $qdEtapes);
			//recuperation du traitement du circuit
			$qdVisas = array(
				'fields' => array(
					'Traitement.id',
					'Traitement.target_id',
					'Traitement.numero_traitement',
					'Traitement.treated',
					'Visa.etape_type',
					'Visa.etape_nom',
					'Visa.etape_id',
					'Visa.trigger_id',
					'Visa.action',
					'Visa.numero_traitement',
					'Etape.description',
					'Etape.id',
					CAKEFLOW_TRIGGER_MODEL . "." . CAKEFLOW_TRIGGER_FIELDS
				),
				'contain' => false,
				'joins' => array(
					array(
						'table' => 'wkf_visas',
						'alias' => 'Visa',
						'type' => 'right',
						'conditions' => array(
							'Visa.traitement_id = Traitement.id'
						)
					),
					array(
						'table' => 'wkf_etapes',
						'alias' => 'Etape',
						'type' => 'right',
						'conditions' => array(
							'Etape.id = Visa.etape_id'
						)
					),
					$this->Traitement->Circuit->Traitement->Visa->join(CAKEFLOW_TRIGGER_MODEL)
				),
				'conditions' => array(
					'Traitement.target_id' => $fluxId
				),
				'order' => array(
					'Traitement.target_id',
					'Visa.numero_traitement',
					'Visa.id'
				)
			);
			$visas = $this->Traitement->find('all', $qdVisas);
		}
//$this->log($visas);
		$compiledVisas = array();
		$compiledVisasRef = array();
		foreach ($visas as $kVisa => $visa) {
			if (in_array($visa['Visa']['numero_traitement'], $compiledVisasRef)) {
				$key = array_search($visa['Visa']['numero_traitement'], $compiledVisasRef);
				$compiledVisas[$key]['Composition'][] = array(
					CAKEFLOW_TRIGGER_MODEL => array(
						'name' => $visa[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
						'action' => $visa['Visa']['action']
					)
				);
			} else {
				$compiledVisasRef[$kVisa] = $visa['Visa']['numero_traitement'];
				$visa['Composition'] = array(
					array(
						CAKEFLOW_TRIGGER_MODEL => array(
							'name' => $visa[CAKEFLOW_TRIGGER_MODEL][CAKEFLOW_TRIGGER_FIELDS],
							'action' => $visa['Visa']['action']
						)
					)
				);
				$compiledVisas[$kVisa] = $visa;
			}
		}


		// On masque les descriptions des étapes du circuit ajoutées par la fonction d'envoi avec ou sans AR.
		if( count($visas) >= 2 ) {
			for( $v=1; $v <= count($visas)-1; $v++ ) {
				// si le flux est issu d'une étape précédente du type IL ou IP (envoi avec ou sans AR)
				if( isset($compiledVisas[$v]) && isset($compiledVisas[$v-1])) {
					if ( in_array( $compiledVisas[$v]['Visa']['action'],  array( 'RI', 'OK' ) ) &&  in_array( $compiledVisas[$v-1]['Visa']['action'], array( 'IL', 'IP' ) )  ){
						// on ne prend pas de description pour cette novuelle étape
						$compiledVisas[$v]['Etape']['description'] = '';
					}
				}

				// si le flux est issu d'une étape avec AR
				if( $v == count($visas)-2) {
					// si le flux est issu d'une étape précédente du type IL ou IP (envoi avec ou sans AR)
					if( isset($compiledVisas[$v]) && isset($compiledVisas[$v-1])) {
						if ( in_array( $compiledVisas[$v]['Visa']['action'],  array( 'RI', 'OK' ) )  &&  in_array( $compiledVisas[$v-1]['Visa']['action'], array('RI', 'OK') ) ) {
							// on ne prend pas de description pour cette novuelle étape
							$compiledVisas[$v]['Etape']['description'] = '';
						}
					}
				}
			}
		}



		return array(
			'circuit' => $circuit,
			'etapes' => $etapes,
			'visas' => $compiledVisas
		);
	}


	/**
	 *
	 * @param type $courrier_id
	 * @return string
	 */
	public function getCycle($courrier_id, $withPastell = true) {
		$bannettes = $this->Bancontenu->find(
			'all', array(
				'conditions' => array(
					'Bancontenu.courrier_id' => $courrier_id
				),
				'contain' => array(
					'Bannette',
					'Bancontenuuser',
					'Desktop' => array(
						'SecondaryUser',
						'User',
						'Profil',
						'Desktopmanager',
					)
				),
				'order' => array('Bancontenu.id ASC', 'Bancontenu.created ASC', 'Desktop.name ASC')
			)
		);

		$traitement = $this->Traitement->find(
			'first', array(
				'fields' => array(
					'Traitement.id',
					'Traitement.circuit_id',
					'Traitement.numero_traitement'
				),
				'conditions' => array(
					'Traitement.target_id' => $courrier_id
				),
				'contain' => false,
				'recursive' => -1
			)
		);
		$traitement_id = Hash::get($traitement, 'Traitement.id');


		if( !empty($traitement)) {
			$num_traitement = $traitement['Traitement']['numero_traitement'];
			$circuit_id = Hash::get($traitement, 'Traitement.circuit_id');
			if (!empty($traitement) && !empty($circuit_id)) {
				$etape = $this->Traitement->Circuit->Etape->find('first', array(
					'conditions' => array(
						'Etape.circuit_id' => $circuit_id,
						'Etape.ordre' => $traitement['Traitement']['numero_traitement']
					),
					'contain' => false
				));

				$visa = $this->Traitement->Visa->find('first', array(
					'conditions' => array(
						'Visa.traitement_id' => $traitement_id,
						'Visa.numero_traitement' => $traitement['Traitement']['numero_traitement']
					),
					'contain' => false
				));

				if (!empty($etape) && !empty($visa['Visa']['commentaire'])) {
					$descriptionEtape = $etape['Etape']['description'];
				} else {
					$descriptionEtape = '';
				}
			} else {
				$descriptionEtape = '';
			}
		}
		else {
			$descriptionEtape = '';
		}
		$visas = $this->Traitement->Visa->find(
			'all', array(
				'fields' => array_merge(
					$this->Traitement->Visa->fields(), $this->Traitement->Visa->Desktopmanager->fields()
				),
				'conditions' => array(
					'Visa.traitement_id' => $traitement_id,
//                'Visa.type_validation <>' => 'D'
				),
				'contain' => array(
					'Desktopmanager' => array(
						'Desktop'
					)
				),
				'recursive' => -1,
				'order' => 'Visa.numero_traitement ASC'
			)
		);
		$validated = array();
		$idsToBold = array();
		$rebond = array();
		$desktopsOrigines['rebond'] = '';
		$desktopsCibles['name'] = '';

		if( empty( $visas ) ) {
			foreach ($bannettes as $b => $bannette) {
				if( $bannette['Bancontenu']['bannette_id'] != BAN_COPY ) {
					$idsToBold[] = $bannette['Bancontenu']['desktop_id'];
				}
			}
		}
		else {
			foreach ($visas as $v => $visa) {
				$validated[$visa['Visa']['trigger_id']] = $visa['Visa']['date'];
				if( $visa['Visa']['numero_traitement'] == $num_traitement && empty( $visa['Visa']['date'] ) ) {
					if(isset($visa['Desktopmanager']['Desktop'][1]['id']) && !empty($visa['Desktopmanager']['Desktop'][1]['id'])) {
						$idsToBold = Hash::extract($visa, 'Desktopmanager.Desktop.{n}.id');
					}
					else {
						$idsToBold[] = $visa['Desktopmanager']['Desktop'][0]['id'];
					}
				}

				if( $visa['Visa']['action'] == 'IP' ) {
					$desktopsOrigines['rebond'] = $this->Desktop->Desktopmanager->getDesktops($visas[$v]['Visa']['trigger_id']);
					$desktopsCibles['name'] = $visas[$v+1]['Desktopmanager']['name'];
				}
			}
		}
		if( !empty($validated) ) {
			foreach ($validated as $id => $date) {
				$validationDesktopmanagerId[$id] = $this->Desktop->Desktopmanager->getDesktops($id);
			}
		}


		$etatsBannettes = array();
		foreach ($bannettes as $b => $bannette) {
			if (!empty($validated[$bannette['Bancontenu']['desktop_id']])) {
				$bannettes[$b]['Bancontenu']['validated'] = $validated[$bannette['Bancontenu']['desktop_id']];
			} else if (!empty($validationDesktopmanagerId)) {
				foreach ($bannette['Desktop']['Desktopmanager'] as $dKey => $desktopmanager) {
					if (isset($validated[$desktopmanager['id']])) {
						$bannettes[$b]['Bancontenu']['validated'] = $validated[$desktopmanager['id']];
					}
				}
			}

			if( $bannette['Bancontenu']['etat'] != 2 ) {
				if( !empty($idsToBold) ) {
					if( $bannette['Desktop']['id'] == $idsToBold && $bannette['Bancontenu']['etat'] == 1 ) {
						if( $bannette['Bancontenu']['bannette_id'] != BAN_COPY) {
							$bannettes[$b]['Bancontenu']['gras'] = true;
						}
					}
					else if( is_array($idsToBold) && in_array( $bannette['Desktop']['id'], $idsToBold) && $bannette['Bancontenu']['etat'] == 1 ) {
						if( $bannette['Bancontenu']['bannette_id'] != BAN_COPY) {
							$bannettes[$b]['Bancontenu']['gras'] = true;
						}
					}
				}
				else {
					if( $bannettes[count($bannettes)-1]['Bancontenu']['bannette_id'] != BAN_COPY ) {
						$bannettes[count($bannettes)-1]['Bancontenu']['gras'] = true;
					}
					else if( $bannettes[count($bannettes)-2]['Bancontenu']['bannette_id'] != BAN_COPY ) {
						$bannettes[count($bannettes)-2]['Bancontenu']['gras'] = true;
					}
					else if( $bannettes[count($bannettes)-3]['Bancontenu']['bannette_id'] != BAN_COPY ) {
						$bannettes[count($bannettes)-3]['Bancontenu']['gras'] = true;
					}
					else if( $bannettes[count($bannettes)-4]['Bancontenu']['bannette_id'] != BAN_COPY ) {
						$bannettes[count($bannettes)-4]['Bancontenu']['gras'] = true;
					}
				}
			}
			// Si le flux est clos (etat=2) alors on naffiche d'agent en gras
			$etatsBannettes[$b] = $bannette['Bancontenu']['etat'];
			if( in_array(2, $etatsBannettes ) ) {
				$bannettes[$b]['Bancontenu']['gras'] = false;
			}


			// Si le flux n'est pas lu, on n'affiche pas la date de modification, ni celle de validation
			if ($bannette['Bancontenu']['read'] == false) {
				$bannettes[$b]['Bancontenu']['validated'] = '';
				$bannettes[$b]['Bancontenu']['modified'] = '';
			}

			// Gestion de la cible du flux , à qui on adresse le flux

			for( $j = 0; $j < count($bannettes) - 1; $j++ ) {
				$cible = @$bannettes[$j+1]['Desktop']['Desktopmanager'][0]['name'];
				$bannettes[$j]['Bancontenu']['cible'] = $cible;
			}


			// Gestion des cas de rebond avec affichage du nom du bureau cible
			$rebonds = $desktopsOrigines['rebond'];
			if( !empty( $rebonds ) ) {
				if( in_array( $bannette['Bancontenu']['desktop_id'], $rebonds) ) {
					$bannettes[$b]['Bancontenu']['rebondir'] = 'Envoi '.Hash::get( $desktopsCibles, 'name' );
				}
				else {
					$bannettes[$b]['Bancontenu']['rebondir'] = '';
				}
			}
			else {
				$bannettes[$b]['Bancontenu']['rebondir'] = '';
			}

			// Si le flux est en copie, on l'indique
			if( $bannette['Bancontenu']['bannette_id'] == BAN_COPY ) {
				$bannettes[$b]['Bancontenu']['copie'] = 'Envoi pour copie';
			}
			else {
				$bannettes[$b]['Bancontenu']['copie'] = '';
			}

			// Action d'insertion dans un circuit
			if( count( $bannettes ) > 2 ) {
				if( @$bannettes[$b]['Bancontenu']['bannette_id'] == BAN_DOCS && @$bannettes[$b+1]['Bancontenu']['bannette_id'] == BAN_FLUX) {
					$bannettes[$b]['Bancontenu']['insertion'] = 'Insertion dans le circuit';
				}
			}
			// Action de validation dans un circuit
			for( $j=0; $j < count($bannettes) - 1; $j++) {
				if( $bannettes[$j]['Bancontenu']['bannette_id'] == BAN_FLUX && $bannettes[$j+1]['Bancontenu']['bannette_id'] == BAN_FLUX ) {
					$bannettes[$j]['Bancontenu']['validation'] = 'Validation';
				}
			}

			for( $j=0; $j < count($bannettes); $j++) {
				if( $j == count($bannettes) - 1) {
					// Action de clore dans un circuit si on est la dernière étape de type clore et bannette flux à traiter
					if( $bannettes[$j]['Bancontenu']['bannette_id'] == BAN_FLUX && $bannettes[$j]['Bancontenu']['etat'] == 2 && !empty( $bannettes[$j]['Bancontenu']['validated'] ) ) {
						$bannettes[$j]['Bancontenu']['clore'] = 'Clôture';
					}
					else if( $j > 1 ) {
						if( $bannettes[$j-1]['Bancontenu']['bannette_id'] == BAN_FLUX && $bannettes[$j-1]['Bancontenu']['etat'] == 2 && !empty( $bannettes[$j-1]['Bancontenu']['validated'] ) ) {
							$bannettes[$j-1]['Bancontenu']['clore'] = 'Clôture';
						}
					}
				}
			}


			$this->Connecteur = ClassRegistry::init('Connecteur');
			$parapheur = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Parapheur%',
						'Connecteur.use_signature'=> true,
						'Connecteur.signature_protocol'=> 'IPARAPHEUR'
					),
					'contain' => false
				)
			);
			$courrier = $this->find(
				'first',
				array(
					'conditions' => array(
						'Courrier.id' => $courrier_id
					),
					'contain' => false,
					'recursive' => -1
				)
			);
			$id_dossier = $courrier['Courrier']['parapheur_id'];

			$parapheurBureauName = '';
			if( !empty( $parapheur) ) {
				App::uses('IparapheurComponent', 'Controller/Component');
				$this->Parafwebservice = new IparapheurComponent;
				$histo = $this->Parafwebservice->getHistoDossierWebservice($id_dossier, $parapheur);
				if ($histo['messageretour']['coderetour'] == 'OK') {
					for ($i = 0; $i < count($histo['logdossier']); $i++) {
						$parapheurBureauName = $histo['logdossier'][$i]['annotation'];
					}

					if ($bannette['Bancontenu']['desktop_id'] == '-1') {
						$bannettes[$b]['Bancontenu']['parapheurname'] = $parapheurBureauName;
					}
				}
			}


			$hasPastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell'=> true
					),
					'contain' => false
				)
			);

			if( !empty( $hasPastellActif) && $withPastell ) {
				App::uses('NewPastellComponent', 'Controller/Component');
				$this->Pastellwebservice = new NewPastellComponent;
				$id_entity = $hasPastellActif['Connecteur']['id_entity'];
				if ($bannette['Bancontenu']['desktop_id'] == '-3' && !empty($bannette['Bancontenu']['pastell_id'] ) ) {
					$infos = $this->Pastellwebservice->detailDocument($id_entity, $bannette['Bancontenu']['pastell_id']);
					if($infos['last_action']['action'] == 'termine' ) {
						$bannettes[$b]['Bancontenu']['pastellname'] = 'Traitement terminé, le '.date('d/m/Y à H:i:s', strtotime($infos['last_action']['date']) );
					}
					else {
						$bannettes[$b]['Bancontenu']['pastellname'] = $infos['last_action']['message'].', le '.date('d/m/Y à H:i:s', strtotime($infos['last_action']['date']) ). '<br /> ('.$infos['last_action']['action'].')';
					}
				}
			}
		}
		$results = array( 'bannettes' => $bannettes, 'descriptionEtape' => $descriptionEtape );
		return $results;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getSon($id = null) {
		$return = null;
		if ($id != null) {
			$return = $this->find('first', array('conditions' => array('Courrier.parent_id' => $id), 'recursive' => -1, 'contain' => false));
		}
		return $return;
	}

	/**
	 * Champ virtuel pour récupérer les noms et valeurs des métadonnéees associées à un flux
	 * On crée un champ virtuel nommé "courrier.metadonnees" qui est un array que l'on trasnforme en string
	 * array(
	 *      'name' => '3 - Code quartier'
	 *      'valeur' => '301 - Vieil Angoulême - Plateau - Eperon '
	 * )
	 * reultat => 3 - Code quartier: 301 - Vieil Angoulême - Plateau - Eperon
	 * @param type $fieldName
	 * @param type $typemetadonnee_id
	 * @return type
	 */
	public function getVfMetaValues($fieldName, $typemetadonnee_id) {
		return "ARRAY_TO_STRING(
            ARRAY(
              SELECT metadonnees.name || ': ' ||
                -- courriers_metadonnees.valeur
                (CASE
                  WHEN metadonnees.typemetadonnee_id = {$typemetadonnee_id} THEN selectvaluesmetadonnees.name::TEXT
                  ELSE courriers_metadonnees.valeur::TEXT
                END)
                FROM courriers_metadonnees
                    INNER JOIN metadonnees ON (courriers_metadonnees.metadonnee_id = metadonnees.id)
                    LEFT OUTER JOIN selectvaluesmetadonnees ON (
                        selectvaluesmetadonnees.metadonnee_id = metadonnees.id
                        AND metadonnees.typemetadonnee_id = {$typemetadonnee_id}
                        AND courriers_metadonnees.valeur ~ '^[0-9]'
                        AND selectvaluesmetadonnees.id::TEXT = courriers_metadonnees.valeur
                    )
                WHERE courriers_metadonnees.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\"
            ),
           ' // '
          ) AS \"{$this->alias}__{$fieldName}\"";
	}


	/**
	 * Champ virtuel pour récupérer les noms et valeurs des métadonnéees associées à un flux
	 * On crée un champ virtuel nommé "courrier.arenvoye" pour la métadonnée 41 - Accusé de réception envoyé
	 * array(
	 *      'name' => '3 - Code quartier'
	 *      'valeur' => '301 - Vieil Angoulême - Plateau - Eperon '
	 * )
	 * reultat => 3 - Code quartier: 301 - Vieil Angoulême - Plateau - Eperon
	 * @param type $fieldName
	 * @param type $typemetadonnee_id
	 * @return type
	 */
	public function getVfMetaARenvoye($fieldName = 'arenvoye', $typemetadonnee_id = 5) {
		return "(
                SELECT courriers_metadonnees.valeur
                FROM courriers_metadonnees
                    INNER JOIN metadonnees ON (courriers_metadonnees.metadonnee_id = metadonnees.id AND metadonnees.id = {$typemetadonnee_id} )
                WHERE courriers_metadonnees.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\"
          ) AS \"{$this->alias}__{$fieldName}\"";
	}

	/**
	 * Champ virtuel pour récupérer les noms et valeurs des métadonnéees associées à un flux
	 * On crée un champ virtuel nommé selon le champ passé en paramètre pour une métadonnée donnée
	 * @param type $fieldName
	 * @param type $typemetadonnee_id
	 * @return type
	 */

	public function getVfMeta($fieldName, $typemetadonnee_id) {
		return "(
                SELECT courriers_metadonnees.valeur
                FROM courriers_metadonnees
                    INNER JOIN metadonnees ON (courriers_metadonnees.metadonnee_id = metadonnees.id AND metadonnees.id = {$typemetadonnee_id} )
                WHERE courriers_metadonnees.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\"
          ) AS \"{$this->alias}__{$fieldName}\"";
	}


	/**
	 * Fonction déclenchant l'envoi vers la GRC Localeo
	 * @param type $courrier_id
	 * @return type
	 */
	public function sendGrcAllowed($courrier_id) {
		$msg['message'] = '';
		$conn = CakeSession::read('Auth.Collectivite.conn');
		if( Configure::read('Webservice.GRC') ) {
			$flux = $this->find('first', array('conditions' => array('Courrier.id' => $courrier_id), 'contain' => array('Contact', 'Organisme'), 'recursive' => -1) );
			// On vérifie si la requête existe toujours dans la GRC
			$this->Connecteur = ClassRegistry::init('Connecteur');
			$hasGrcActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%GRC%',
						'Connecteur.use_grc' => true
					),
					'contain' => false
				)
			);

			if( !empty( $hasGrcActif )) {
				$localeo = new LocaleoComponent;
				/*$note = 'Le flux a été clos dans web-GFC';
                $this->updateAll(
                    array( 'Courrier.statutgrc' => '50'),
                    array( 'Courrier.id' => $courrier_id)
                );*/

				// D'abord le contact
				// Création du contact
				if (empty($flux['Courrier']['contactgrc_id'])) {
					$flux['Contact']['externalId'] = $flux['Courrier']['contact_id'];
					$retourContact = $localeo->createContactGRC( $flux );
					$resultContact = $retourContact;
//					$this->Jsonmsg->json['contactgrc_id'] = $resultContact->id;

					if( !empty($resultContact->id) ) {
						$this->Contact->updateAll(
							array( 'Contact.contactgrc_id' => $resultContact->id),
							array('Contact.id' => $flux['Courrier']['contact_id'])
						);

						$this->updateAll(
							array( 'Courrier.contactgrc_id' => $resultContact->id),
							array('Courrier.id' => $flux['Courrier']['id'])
						);

						$flux['Courrier']['contactgrc_id'] = $resultContact->id;
					}
				}
				$note = '';
				// Création du doc
				$doc = array();
				$path = null;
				$attachement = $this->Document->find('first', array('conditions' => array('Document.courrier_id' =>  $courrier_id, 'Document.main_doc' => true), 'contain' => false));
				if( !empty($attachement) ) {
					if( !empty($attachement['Document']['path']) ) {
						$path = $attachement['Document']['path'];
					}
					else {
						$file = WEBDAV_DIR . DS . $conn . DS .$courrier_id;
						$Folder = new Folder($file, true, 0777);
						file_put_contents($file . DS . $attachement['Document']['name'], $attachement['Document']['content']);
						$path = $file . DS . $attachement['Document']['name'];
					}
				}
				$retour = $localeo->sendDocGRC( $flux, $path, '50', $note );

				// SI le flux a déjà été envoyé à la GRC, alors on ne change pas le courriergrc_id sinon on écrase les valeurs
				if( !empty($flux['Courrier']['courriergrc_id'])) {
					$retour->id = $flux['Courrier']['courriergrc_id'];
				}


//				$this->Jsonmsg->json['courriergrc_id'] = $retour->id;
				if( !empty($retour->id) ) {
					$this->updateAll(
						array( 'Courrier.courriergrc_id' => $retour->id),
						array('Courrier.id' => $courrier_id)
					);
				}

			}
		}
		return json_encode( $retour );
	}


	/**
	 * Retourne la liste des valeurs de métadonnéees autorisant le versement vers la GRC Localeo
	 * @param type $courrier_id
	 */
	public function getMetaValuesAuthorizedSendToGRC( $courrier_id ) {
		$isSelectvaluemetaSendGRC = false;
		$flux = $this->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $courrier_id
				),
				'contain' => array(
					'Metadonnee'
				),
				'recursive' => -1
			)
		);

		$selectvalues = $this->Metadonnee->Selectvaluemetadonnee->find('list', array('conditions' => array('Selectvaluemetadonnee.sendgrc' => true), 'contain' => false));

		foreach( $flux['Metadonnee'] as $key => $metas )  {
			if( in_array( $metas['CourrierMetadonnee']['valeur'], array_keys( $selectvalues) ) ) {
				$isSelectvaluemetaSendGRC = true;
			}

		}
		return $isSelectvaluemetaSendGRC;
	}

	/**
	 * Focntion permettant de remonter la liste des agents possédant le flux actuellement
	 * @param $courrier_id
	 * @return string
	 */
	public function getAgentTraitant( $courrier_id, $desktopId ) {
//		$result = array();
//		$cycle = $this->getCycle($courrier_id);
//		if( isset($cycle['bannettes']) && !empty($cycle['bannettes']) ) {
//			foreach ($cycle['bannettes'] as $key => $bannette) {
//				if (isset($bannette['Bancontenu']['gras']) && $bannette['Bancontenu']['gras']) {
//					$result[] = $bannette['Desktop']['name'];
//				}
//			}
//		}
//		$agentstraitants = implode( " / ", $result);
//		return $agentstraitants;

	}


	/**
	 * Champ virtuel pour récupérer les noms des agents possédant un flux
	 * On crée un champ virtuel nommé selon le champ passé en paramètre pour un flux donné
	 * @param type $fieldName
	 * @return type
	 */

	public function getVfAgents($fieldName, $separator) {
		return "ARRAY_TO_STRING(
            ARRAY(
				SELECT desktops.name
				FROM desktops
					INNER JOIN bancontenus ON (bancontenus.desktop_id = desktops.id )
					INNER JOIN courriers ON (courriers.id = bancontenus.courrier_id)
					LEFT OUTER JOIN wkf_traitements ON (wkf_traitements.target_id = courriers.id)
					LEFT OUTER JOIN wkf_visas ON (wkf_visas.traitement_id = wkf_traitements.id)
				WHERE
					bancontenus.id IN ( SELECT id FROM bancontenus WHERE bancontenus.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\" AND bancontenus.etat = 1 AND bancontenus.bannette_id != 7 ORDER BY bancontenus.created DESC )
				 	AND (
						(
							wkf_visas.numero_traitement = wkf_traitements.numero_traitement
							AND wkf_visas.date IS NULL
						)
						OR
						(
							wkf_visas.id IS NULL
							AND wkf_traitements.id IS NULL
						)
					)
		 	),
           ' {$separator} '
          ) AS \"{$this->alias}__{$fieldName}\"";

	}

	/**
	 * Champ virtuel pour récupérer la date du refus
	 * On crée un champ virtuel nommé selon le champ passé en paramètre pour un flux donné
	 * @param type $fieldName
	 * @return type
	 */

	public function getDaterefus($fieldName) {
		return "(
				SELECT bancontenus.modified
				FROM bancontenus
					INNER JOIN courriers ON (bancontenus.courrier_id = courriers.id )
				WHERE
					bancontenus.id IN ( SELECT id FROM bancontenus WHERE bancontenus.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\" AND bancontenus.etat = -1 AND bancontenus.bannette_id IS NULL ORDER BY bancontenus.created DESC LIMIT 1 )
          ) AS \"{$this->alias}__{$fieldName}\"";

	}

	/**
	 * Champ virtuel pour récupérer la date de validation
	 * On crée un champ virtuel nommé selon le champ passé en paramètre pour un flux donné
	 * @param type $fieldName
	 * @return type
	 */

	public function getDateValidation($fieldName) {
		return "(
				SELECT bancontenus.modified
				FROM bancontenus
					INNER JOIN courriers ON (bancontenus.courrier_id = courriers.id )
				WHERE
					bancontenus.id IN ( SELECT id FROM bancontenus WHERE bancontenus.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\" AND bancontenus.etat = 1 AND bancontenus.bannette_id = 4 ORDER BY bancontenus.created DESC LIMIT 1 )
          ) AS \"{$this->alias}__{$fieldName}\"";

	}

	/**
	 * Champ virtuel pour récupérer la date de clôture
	 * On crée un champ virtuel nommé selon le champ passé en paramètre pour un flux donné
	 * @param type $fieldName
	 * @return type
	 */

	public function getDateCloture($fieldName) {
		return "(
				SELECT bancontenus.modified
				FROM bancontenus
					INNER JOIN courriers ON (bancontenus.courrier_id = courriers.id )
				WHERE
					bancontenus.id IN ( SELECT id FROM bancontenus WHERE bancontenus.courrier_id = \"{$this->alias}\".\"{$this->primaryKey}\" AND bancontenus.etat = 2 AND bancontenus.bannette_id = 4 ORDER BY bancontenus.created DESC LIMIT 1 )
          ) AS \"{$this->alias}__{$fieldName}\"";

	}
	/**
	 * Fonction retournant le nombre de jours avant que le flux soit en retard
	 * @param $courrierId
	 * @return false|float
	 */
	public function getRetardDelaiNbOfDays($courrierId) {
		$days = '';
		$nbDelai = 0;
		$unite = 'days';
		$courrier = $this->find(
			'first',
			array(
				'conditions' => array(
					'Courrier.id' => $courrierId
				),
				'contain' => false
			)
		);

		if (!empty($courrier['Courrier']['delai_nb']) && ( !empty($courrier['Courrier']['delai_unite']) || $courrier['Courrier']['delai_unite'] == 0) ) {
			if ($courrier['Courrier']['delai_unite'] == 0) { // 0 : jour
				$nbDelai = $courrier['Courrier']['delai_nb'];
			} else if ($courrier['Courrier']['delai_unite'] == 1) { // 1 : semaine
				$nbDelai = $courrier['Courrier']['delai_nb'] * 7;
			} else if ($courrier['Courrier']['delai_unite'] == 2) { // 2 : mois
				$nbDelai = $courrier['Courrier']['delai_nb'];
				$unite = 'month';
			}
		}
		else if (!empty($courrier['Courrier']['soustype_id']) ) {
			$soustype = $this->Soustype->find(
				'first',
				array(
					'conditions' => array(
						'Soustype.id' => $courrier['Courrier']['soustype_id']
					),
					'contain' => false
				)
			);

			if (!empty($soustype['Soustype']['delai_nb']) && ( !empty($soustype['Soustype']['delai_unite']) || $soustype['Soustype']['delai_unite'] == 0) ) {
				if ($soustype['Soustype']['delai_unite'] == 0) { // 0 : jour
					$nbDelai = $soustype['Soustype']['delai_nb'];
				} else if ($soustype['Soustype']['delai_unite'] == 1) { // 1 : semaine
					$nbDelai = $soustype['Soustype']['delai_nb'] * 7;
				} else if ($soustype['Soustype']['delai_unite'] == 2) { // 2 : mois
					$nbDelai = $courrier['Courrier']['delai_nb'];
					$unite = 'month';
				}
			}
		}

		if( !empty( $nbDelai ) ) {
			$dateToCheck = date('Y-m-d', strtotime($courrier['Courrier']['datereception'] . " + $nbDelai $unite"));
			if ($dateToCheck != $courrier['Courrier']['datereception']) {
				// Calcul du nombre de jours entre la date de validation et la date de réception du flux
				$operator = '+';
				if ($dateToCheck > date('Y-m-d')) {
					$operator = '-';
				}
				$date1_ts = strtotime(date('Y-m-d'));
				$date2_ts = strtotime($dateToCheck);
				$diff = $date2_ts - $date1_ts;
				$days = $operator . abs(round($diff / 86400));
			}
		}

		return $days;
	}

	/**
	 * Fonction peremttant de notifier l'administrateur
	 * @param array $courrier
	 */
	public function notifierAdmin($courrierId, $userToNotify = array()) {
		$success = array();
		$userToNotify = array_merge( $userToNotify['User'], $userToNotify['SecondaryUser']);
		if (!empty($userToNotify)) {
			foreach ($userToNotify as $user) {
				if ($user['id'] != -1) {
					$userId = $user['id'];
					if (!empty($userId)) {
						$typeNotif = $user['typeabonnement'];
						if ($typeNotif == 'quotidien') {
							$success[] = $this->User->saveNotif($courrierId, $userId, null, 'bloque_parapheur');
						} else {
							$success[] = $this->User->notifier($courrierId, $userId, null, 'bloque_parapheur');
						}
						if (in_array(true, $success)) {
							$this->updateAll(
								array(
									'Courrier.mail_bloque_parapheur' => true
								), array(
									'Courrier.id' => $courrierId
								)
							);
						}
					} else {
						$success = false;
					}
				} else {
					$success = false;
				}
			}
		}
	}


	/**
	 * @version 4.3
	 * @access public
	 * @param integer $courrier_id --> identifiant du projet à mettre à jour
	 * @return boolean --> true si le dossier à terminé son circuit
	 * @throws Exception
	 */
	public function majTraitementsPastell($courrier_id, $conn = null, $id_e, $forParapheur) {

		$before = $this->Traitement->whoIs($courrier_id, 'current');
		$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($before);

		$ret = $this->Traitement->majTraitementsPastell($courrier_id, $conn, $id_e, $forParapheur);

		$msg = 'Traitement en cours dans Pastell';
		if ($ret == 'TRAITEMENT_TERMINE_OK') {
			$parent = 0;
			$type_id = null;
			$insertAuto = 0;
			$desktop_id = @$desktopsIds[0];
			if (!empty($desktop_id)) {
				if ($this->cakeflowExecuteNext($desktop_id, $courrier_id, $parent, $type_id, $insertAuto)) {
					$msg = 'Flux récupéré et transféré à l\'étape suivante';
				} else {
					$msg = 'Echec de la transmission à l\'étape suivante';
				}
			} else {
				$msg = 'Aucun rôle destinataire défini';
			}
		}
		return $msg;
	}

	/**
	 * Conversion des PDFs en PNGs pour exploitation future
	 * @param type $file
	 * @return type
	 */
	private function _convertPdfToPng($pdfFile, $courrier, $conn) {
		$pos_point = strpos($pdfFile, '.');
		$filename = substr($pdfFile, 0, $pos_point);

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $conn)));
		$repo = $coll['Collectivite']['scan_local_path'];
		$repoTesseract = $repo . DS . 'tesseract';

		$this->repositoryTesseract = new Folder($repoTesseract);
		if (!isset($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'existe pas.');
		}
		$this->repository = new Folder($repo);
		if (!isset($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'existe pas.');
		}

		$pdf = $this->repository->path.'/'.$pdfFile;
		$png = $this->repositoryTesseract->path. DS .$filename.".png";
		$convert = "gs -sDEVICE=png16m -dTextAlphaBits=4 -r300 -o $png $pdf";

		$filenamePath = WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . $filename;
		$ocerisation = "tesseract $png $filenamePath";

		exec($convert);
		exec($ocerisation);
		// le fichier stocké en txt est enregistré comme PJ du flux
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$fileMimeType = $finfo->buffer($filename);
		$dataFile = file_get_contents($filenamePath.".txt");
		$doc = array(
			'Document' => array(
				'name' => $filename.".txt",
				'size' => strlen($png),
				'mime' => $fileMimeType,
				'ext' => 'txt',
				'courrier_id' => $courrier['Courrier']['id'],
				'main_doc' => false,
				'path' => $filenamePath.".txt"
			)
		);
		$this->Document->setDataSource($conn);
		$this->Document->create($doc);
		$this->Document->save();

		$repositoryTesseractOld = $repo . DS . 'tesseract/old';
		$this->repositoryTesseractOld = new Folder($repositoryTesseractOld);
		$tmpFile = new File($this->repositoryTesseract->path . DS . $filename.".png");
		if ($tmpFile->copy($this->repositoryTesseractOld->path . DS . $filename.".png" . '.webgfcold_' . date('Ymd'))) {
			$return = $tmpFile->delete();
		}
	}

	/**
	 *
	 * @param type $file
	 * @param type $invalid
	 * @return type
	 */
	private function _deleteTesseractTxtFiles($file, $courrier, $conn) {
		$return = false;
		$txtFile = new File( WORKSPACE_PATH . DS . $conn . DS . $courrier['Courrier']['reference'] . DS . $file);
		@unlink($txtFile->path);

		$docId = $this->Document->find(
			'first',
			array(
				'fields' => array('Document.id'),
				'conditions' => array(
					'Document.name' => $file,
					'Document.courrier_id' => $courrier['Courrier']['id']
				),
				'contain' => false,
				'recursive' => -1
			)
		);

		$return = $this->Document->delete($docId['Document']['id']);

		return $return;
	}

	/**
	 * Création du flux depuis l'API v1
	 * @param $post format json {"name":"test", ....}
	 * @param $docs format form-data [document_1, document_2 ....]
	 * @return array
	 */
	public function createCourrierApiV1($post, $docs)
	{
		$userId = CakeSession::read('Auth.User.id');
		$this->_docs = $docs;
		$this->_post = $post;
		try {

			$data = array(
				'Courrier' => array(
					'name' => $post['name'],
					'objet' => isset( $post['objet'] ) ? $post['objet'] : null,
					'user_creator_id' => isset( $post['user_creator_id'] ) ? $post['user_creator_id'] : Configure::read('Api.UserId'),
					'desktop_creator_id' => isset( $post['desktop_creator_id'] ) ? $post['desktop_creator_id'] : Configure::read('Api.DesktopId'),
					'soustype_id' => !empty($post['soustype_id']) ? $post['soustype_id'] : null,
					'datereception' => !empty($post['datereception']) ? $post['datereception'] : date('Y-m-d'),
					'date' => date('Y-m-d'),
					'reference' => $this->genReferenceUnique(),
					'contact_id' => isset( $post['contactId'] ) ? $post['contactId'] : Configure::read('Sanscontact.id'),
					'organisme_id' => isset( $post['organismeId'] ) ? $post['organismeId'] : Configure::read('Sansorganisme.id'),
					'courriergrc_id' => isset( $post['courrierGrcId'] ) ? $post['courrierGrcId'] : ''
				)
			);
			$this->create($data);
			$resultSaveCourrier = $this->save();
		} catch (Exception $e) {
			throw new BadRequestException($e->getMessage(), 400);
		}

		if ($resultSaveCourrier) {
			// Ajout dans les bonnes bannettes
			$desktopmanagerId = $post['desktopmanager_id'];
			$groupId = $post['profil_id'];
			if( empty($desktopmanagerId) ) {
				$desktopmanagerId = Configure::read('Desktopmanager.BureauCourrierId');
			}
			if( empty($groupId) ) {
				$groupId = Configure::read('Desktopmanager.BureauCourrierGroupId');
			}
			$desktopsIds = $this->Desktop->Desktopmanager->getDesktops($desktopmanagerId);
			foreach($desktopsIds as $desktopId) {
				if ($groupId == 7) {
					$this->Bancontenu->add($desktopId, BAN_AIGUILLAGE, $this->id, false , $userId);
				} else if ($groupId == 3) {
					$this->Bancontenu->add($desktopId, BAN_DOCS, $this->id, false , $userId);
				} else {
					return ['success' => false, 'errors' => "L'utilisateur n'a pas de rôle Aiguilleur ou Initiateur."];
				}
			}

			// Ajout des documents si possible
			$conn = CakeSession::read('Auth.Collectivite.conn');
			$return['CodeRetour'] = "CREATED";
			$return['Message'] = "Le flux a été créé";
			$return['Severite'] = "normal";
			$return['CourrierId'] = $this->id;
			$return['DocumentId'] = '';
			if( !empty($docs) ) {
				$nbDocs = count($docs);
				for ($j = 1; $j <= $nbDocs; $j++) {
					$fileContent = file_get_contents($docs["document_$j"]['tmp_name']);
					if ($j != 1) {
						$this->_genFileAssoc($fileContent, $this->id, $return, false, $docs["document_$j"]['name'], $conn);
					} else {
						$this->_genFileAssoc($fileContent, $this->id, $return, true, $docs["document_1"]['name'], $conn);
					}
				}
			}
			return [ 'success' => true, 'courrierId' => $this->id];
		}

		return ['success' => false, 'errors' => $this->validationErrors];
	}


	/**
	 * Création du flux depuis l'API v1
	 * @param $post
	 * @param $docs
	 * @return array
	 */
	public function createContactForCourrierApiV1($post)
	{
		try {
			$contactData = $this->_getContact($post);
		} catch (Exception $e) {
			throw new BadRequestException($e->getMessage(), 400);
		}

		if(!empty($contactData) ) {
			return [ 'success' => true, 'contactId' => $contactData['contactId'], 'organismeId' => $contactData['organismeId'] ];
		}
		return ['success' => false, 'errors' => $this->validationErrors];
	}


	/**
	 * Datas Contacts
	 * @param $contactData
	 * @return array
	 */
	private  function _getContact($contactData) {

		// Gestion de l'organisme
		$addressbooks = $this->Organisme->Addressbook->find('list', array('contain' => false));
		$dataOrganisme = array(
			'Organisme' => array(
				'addressbook_id' => array_keys($addressbooks)[0], //FICME: voir comment récupérer le bon carnet d'adresse cible
				'name' => $contactData['organismeName']
			)
		);
		$OrganismeInBase = $this->Organisme->find(
			'first',
			array(
				'conditions' => array(
					'Organisme.name' => $contactData['organismeName']
				),
				'contain' => false,
				'recursive' => -1
			)
		);

		if (!empty($OrganismeInBase)) {
			$OrganismeIdForCourrier = $OrganismeInBase['Organisme']['id'];
		} else {
			$this->Organisme->create($dataOrganisme);
			if ($this->Organisme->save()) {
				$OrganismeIdForCourrier = $this->Organisme->id;
				if (!empty($OrganismeIdForCourrier)) {
					$sansContact['Contact']['addressbook_id'] = $dataOrganisme['Organisme']['addressbook_id'];
					$sansContact['Contact']['organisme_id'] = $OrganismeIdForCourrier;
					$sansContact['Contact']['name'] = ' Sans contact';
					$sansContact['Contact']['nom'] = '0Sans contact';
					$this->Organisme->Contact->create($sansContact['Contact']);
					$this->Organisme->Contact->save();
					$contactIdForCourrier = $this->Organisme->Contact->id;
				}
			}
		}

		// Gestion du contact
		$contactInBase = $this->Contact->find(
			'first',
			array(
				'conditions' => array(
					'Contact.name' => $contactData['name'],
					'Contact.nom' => $contactData['nom'],
					'Contact.prenom' => $contactData['prenom']
				),
				'contain' => false,
				'recursive' => -1
			)
		);

		if (!empty($contactInBase)) {
			$contactIdForCourrier = $contactInBase['Contact']['id'];
			$OrganismeIdForCourrier = $contactInBase['Contact']['organisme_id'];
		} else {
			$contactData['organisme_id'] = $OrganismeIdForCourrier;
			$this->Contact->create($contactData);
			if ($this->Contact->save()) {
				$contactIdForCourrier = $this->Contact->id;
			}
		}

		return [ 'success' => true, 'contactId' => $contactIdForCourrier, 'organismeId' => $OrganismeIdForCourrier];
	}


	/**
	 * Vérification de l'état d'un flux
	 *
	 * @logical-group WebService
	 *
	 * @access public
	 * @param integer $courrierId identifiant du flux
	 * @return array
	 */
	public function getStatus($courrierId) {

		$courrier = $this->find('first', array('conditions' => array('Courrier.id' => $courrierId)));

		//si le flux existe
		if (!empty($courrier)) {
			//flux refuse
			if ($this->isRefused($courrier['Courrier']['id'])) {
				return [ 'success' => true, 'courrierId' => $courrierId, 'status' => 'REFUS', 'message' => 'Le flux a été refusé car '.$courrier['Courrier']['motifrefus']];
			} else if ($this->isTreated($courrier['Courrier']['id'])) {
				//on retourne le code OK
				return [ 'success' => true, 'courrierId' => $courrierId, 'status' => 'OK', 'message' => 'Le flux a été traité'];
			} else if ($this->isClosed($courrier['Courrier']['id'])) {
				//on retourne le code OK
				return [ 'success' => true, 'courrierId' => $courrierId, 'status' => 'TREATED', 'message' => 'Le flux est clos'];
			}
			else {
				//on retourne le code ENCOURS
				return [ 'success' => true, 'courrierId' => $courrierId, 'status' => 'ENCOURS', 'message' => 'Le flux est en cours de traitement'];
			}
		}
		return ['success' => false, 'courrierId' => $courrierId, 'errors' => "Le flux n'existe pas"];
	}

	/**
	 * Fonction permettant de retourner l'état courant d'un flux
	 * @param id $id du flux
	 * @return string
	 */

	public function getLast($id) {
		$sqBancontenu = $this->Bancontenu->sqDerniereBannette('Courrier.id');
		$courrier = $this->find(
			'first',
			array(
				'fields' => array_merge(
					$this->fields(),
					$this->Bancontenu->fields()
				),
				'conditions' => array(
					'Courrier.id' => $id
				),
				'contain' => false,
				'joins' => array(
					$this->join('Bancontenu', array('type' => 'INNER','conditions' => array("Bancontenu.id IN ( {$sqBancontenu} )") ) )
				)
			)
		);
		//si le flux existe
		if (!empty($courrier)) {
			return $courrier;
		}
	}

	/**
	 * Fonction permettant de retourner l'état courant d'un flux
	 * @param id $id du flux
	 * @return string
	 */

	public function getLastEtat($id) {
		$etat = '';
		$courrier = $this->getLast($id);
		if( !empty( $courrier) ) {
			$etat = $courrier['Bancontenu']['etat'];
		}
		return $etat;
	}

	/**
	 * Fonction permettant de retourner l'état courant d'un flux
	 * @param id $id du flux
	 * @return string
	 */

	public function getLastBannette($id) {
		$bannette = '';
		$courrier = $this->getLast($id);
		if( !empty( $courrier) ) {
			$bannette = $courrier['Bancontenu']['bannette_id'];
		}
		return $bannette;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getComments($id)
	{

		//querydata générique
		$qdComment = array(
			'fields' => array(
				'DISTINCT Comment.id',
				'Comment.created',
				'Comment.modified',
				'Comment.content',
				'Comment.objet',
				'Comment.private',
				'Comment.read',
				'Owner.id',
				'Owner.name',
				'User.prenom',
				'User.nom',
				'User.id'
			),
			'conditions' => array(
				'Comment.target_id' => $id
			),
			'order' => array('Comment.created desc'),
			'joins' => array(
				$this->Comment->join($this->Comment->Owner->alias),
				$this->Comment->Owner->join($this->Comment->Owner->User->alias),
				$this->Comment->Owner->join($this->Comment->Owner->DesktopsUser->alias)
			),
			'contain' => false,
			'recursive' => -1
		);

		//public
		$qdPublic = $qdComment;
		$qdPublic['conditions']['Comment.private'] = false;

		//private
		$qdPrivate = $qdComment;
		$qdPrivate['conditions']['Comment.private'] = true;

		//unread
		$qdUnread = $qdComment;
		$qdUnread['conditions']['Comment.read'] = false;

		if (!empty(CakeSession::read('Auth.User.Env.secondary'))) {
			$readerPrivates = array_keys(CakeSession::read('Auth.User.Env.secondary'));
			if (!empty($readerPrivates)) {
				array_push($readerPrivates, CakeSession::read('Auth.User.Desktop.id'));
			}
		}

		$sqParams = array(
			'alias' => 'comments_readers',
			'fields' => array(
				'comments_readers.comment_id'
			),
			'conditions' => array(
				'comments_readers.reader_id' => @$readerPrivates,
			),
			'contain' => false,
			'recursive' => -1
		);
		$qdPrivate['conditions'][] = 'Comment.id IN (' . $this->Comment->SCCommentReader->sq($sqParams) . ')';


		//written
		$qdWritten = $qdComment;
		$ownerIds = [];
		$sd = CakeSession::read('Auth.User.SecondaryDesktops');
		if (!empty($sd)) {
			$ownerIds = array_merge(array(CakeSession::read('Auth.User.Desktop.id')), Hash::extract($sd, '{n}.id'));
		} else {
			$ownerIds[] = CakeSession::read('Auth.User.Desktop.id');
		}

		$qdWritten['conditions']['Owner.id'] = $ownerIds;

		$qdAll = $qdComment;

		$return = array(
			'public' => $this->Comment->find('all', $qdPublic),
			'private' => $this->Comment->find('all', $qdPrivate),
			'written' => $this->Comment->find('all', $qdWritten),
			'unread' => $this->Comment->find('all', $qdUnread),
			'all' => $this->Comment->find('all', $qdAll),
			'owners' => $ownerIds
		);

		for ($i = 0; $i < count($return['written']); $i++) {
			$found = $this->Comment->find('first', array(
					'fields' => array(
						'Comment.id',
						'Comment.slug'
					),
					'conditions' => array(
						'Comment.id' => $return['written'][$i]['Comment']['id']
					),
					'contain' => array(
						'Reader' => array(
							'fields' => array(
								'Reader.name'
							),
							'User' => array(
								'fields' => array(
									'User.id',
									'User.prenom',
									'User.nom'
								)
							),
							'SecondaryUser' => array(
								'fields' => array(
									'SecondaryUser.id',
									'SecondaryUser.prenom',
									'SecondaryUser.nom'
								),
								'conditions' => array(
									'or' => array(
										'DesktopsUser.delegation' => false,
										'DesktopsUser.delegation' => null
									)
								)
							)
						)
					)
				)
			);

			$readers = array();
			foreach ($found['Reader'] as $reader) {
				$name = '';
				if (!empty($reader['User'][0]['nom']) && !empty($reader['User'][0]['prenom'])) {
					$name .= $reader['User'][0]['nom'] . ' ' . $reader['User'][0]['prenom'];
				} else if (!empty($reader['SecondaryUser'][0]['nom']) && !empty($reader['SecondaryUser'][0]['prenom'])) {
					$name .= $reader['SecondaryUser'][0]['nom'] . ' ' . $reader['SecondaryUser'][0]['prenom'];
				}
				$name .= (empty($name) ? '' : ' (') . $reader['name'] . (empty($name) ? '' : ')');
				$readers[] = array('id' => $reader['id'], 'name' => $name);
			}

			$return['written'][$i]['readers'] = $readers;
		}

		foreach($return['private'] as $p => $private ) {
			$readers = $this->Comment->SCCommentReader->find('all', array(
					'conditions' => array(
						'SCCommentReader.comment_id' => $private['Comment']['id']
					),
					'contain' => false
				)
			);
			$unreadReaders = [];
			foreach( $readers as $reader ) {
				if( empty($reader['SCCommentReader']['read'] ) ) {
					$unreadReaders[$reader['SCCommentReader']['comment_id']][] = $reader['SCCommentReader']['reader_id'];
//					if( CakeSession::read('Auth.User.Courrier.Desktop.id') != $reader['SCCommentReader']['reader_id'] ){
//						$return['private'][$p] = [];
//					}
				}
			}

			$return['unreadprivate'][$p]['unreadReaders'] = $unreadReaders;
		}

		foreach($return['public'] as $p => $all ) {
			if( in_array( $all['Owner']['id'], $ownerIds ) ) {
				$writerOwnerId = $ownerIds;
			}
			else {
				$writerOwnerId = $all['Owner']['id'];
			}
			$return['writerOwner'][$all['Comment']['id']] = $writerOwnerId;
		}

		return $return;
	}

	/**
	 * @param $id
	 */
	public function getTypeDocNamePastell( $data ) {
		$name = '';
		$typePastell = [];
		if( $data['Soustype']['envoi_signature'] ) {
			$name = 'Visa/Signature ';
		}
		if( $data['Soustype']['envoi_mailsec'] ) {
			$name .= '- Mail Sécurisé ';
		}
		if( $data['Soustype']['envoi_ged'] ) {
			$name .= '- GED ';
		}
		if( $data['Soustype']['envoi_sae'] ) {
			$name .= '- SAE';
		}

		if( empty($name) ) {
			$typePastell = Configure::read('Pastell.typesdoc');
		}
		else {
			$typePastell = [
				Configure::read('Pastell.fluxStudioName') => $name
			];
		}

		return $typePastell;
	}
}
?>
