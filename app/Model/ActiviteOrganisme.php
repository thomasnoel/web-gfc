<?php

App::uses('AppModel', 'Model');

/**
 * ActiviteOrganisme model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class ActiviteOrganisme extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'ActiviteOrganisme';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'activites_organismes';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'activite_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'organisme_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Organisme' => array(
			'className' => 'Organisme',
			'foreignKey' => 'organisme_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Activite' => array(
			'className' => 'Activite',
			'foreignKey' => 'activite_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
