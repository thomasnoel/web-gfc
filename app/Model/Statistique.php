<?php

App::uses('AppModel', 'Model');

/**
 * Statistique model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Statistique extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Statistique';

    /**
     * Ce modèle n'est lié à aucune table.
     *
     * @var boolean
     */
    public $useTable = false;

    /**
     *
     * @var type
     */
    public $validate = array(
        'dept' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        ),
        'commune' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            )
        )
    );

    /**
     * Retourne la requête de base utilisée dans les différents questionnaires.
     *
     * Les modèles utilisés sont: Dossier, Detaildroitrsa, Foyer, Situationdossierrsa,
     * Adressefoyer, Personne, Adresse, Prestation, Calculdroitrsa.
     *
     * @param array $search
     * @return array
     */
    protected function _getBaseQuery(array $search, $etat) {
        $Courrier = ClassRegistry::init('Courrier');
        $Bancontenu = ClassRegistry::init('Bancontenu');
        $annee = Hash::get($search, 'Statistique.annee');

        $origine = Hash::get($search, 'Statistique.origineflux_id');


        $conditions = array(
            "Courrier.created BETWEEN '{$annee}-01-01' AND '{$annee}-12-31'"
        );

        $agent = Hash::get($search, 'Agent.Agent');
        // On ajoute une condition sur l'orginie du flux
        if (isset($agent) && !empty($agent)) {
            $this->User = ClassRegistry::init('User');
            $userSelected = $this->User->find(
                    'all', array(
                'conditions' => array(
                    'User.id' => $agent
                ),
                'join' => array(
                    $this->User->join('DesktopsUser')
                )
                    )
            );
//debug($userSelected);
            foreach ($userSelected as $u => $user) {

                // desktop_id de l'utilisateur sélectionné
                $desktopsSelectedIds = array($user['Desktop']['id']);

                // on regarde les bureaux liés à l'utilsiateur sélectionné
                foreach ($user['SecondaryDesktop'] as $key => $secondaryDesktops) {
                    // si on a une délégation, on l'enlève
                    if ($secondaryDesktops['DesktopsUser']['delegation'] == true) {
                        unset($user['SecondaryDesktop'][$key]);
                    }
                }

                $secondariesDesktops = Hash::extract($user, 'SecondaryDesktop.{n}.id');
                $desktopsIds = array_merge($desktopsSelectedIds, $secondariesDesktops);
//debug($desktopsIds);
                $conditions = array_merge(
                        $conditions, array(
                    'Bancontenu.desktop_id' => $desktopsIds
                        )
                );
            }
        }


        // On ajoute une condition sur l'orginie du flux
        if (isset($origine) && !empty($origine)) {
            $conditions = array_merge(
                    $conditions, array(
                'Courrier.origineflux_id' => $origine
                    )
            );
        }

        $sq = $this->_sqDerniereBannette($annee, $etat);
        $query['joins'][] = $Courrier->join(
                'Bancontenu', array(
            'type' => 'INNER',
            'conditions' => array(
                "Bancontenu.id IN ( {$sq} )"
            )
                )
        );

        $query = array(
            'joins' => array_merge(
                    $query['joins']
            ),
            'contain' => false,
            'conditions' => $conditions,
        );
        return $query;
    }

    /**
     * Retourne une sous-requête permettant de cibler la dernière bannette
     * d'un courrier pour une année donnée.
     * * etat :
     *      2 terminé
     * @param type $annee
     * @return type
     */
    protected function _sqDerniereBannette($annee, $etat = null) {
        $Bancontenu = ClassRegistry::init('Bancontenu');
        $courrierIdFied = 'Courrier.id';

        $conditions = array(
            "banscontenus.courrier_id = {$courrierIdFied}",
            "banscontenus.created BETWEEN '{$annee}-01-01' AND '{$annee}-12-31'"
        );
        if (isset($etat)) {
            $conditions[] = array("banscontenus.etat = {$etat}");
        }

        return $Bancontenu->sq(
                        array(
                            'fields' => array(
                                'banscontenus.id'
                            ),
                            'alias' => 'banscontenus',
                            'conditions' => $conditions,
                            'order' => array('banscontenus.created DESC'),
                            'limit' => 1
                        )
        );
    }

    public function getIndicateursQuants(array $search) {
        $Courrier = ClassRegistry::init('Courrier');
        $Bancontenu = ClassRegistry::init('Bancontenu');
        $annee = Hash::get($search, 'Statistique.annee');
        $results = array();

        /*
         * Informations concernant les états du flux
         */
        // 0. Query de base
        $base = $this->_getBaseQuery($search, null);
        $base['fields'] = array('COUNT(DISTINCT(Courrier.id)) AS "count"');

        // 1. Nombre de courriers en cours de traitement
        $query = $base;
        $query['conditions'][] = array('Bancontenu.etat' => 1, 'Bancontenu.courrier_id IS NOT NULL');
        $results['Indicateurquant']['encours'] = Hash::get($Courrier->find('all', $query), '0.0.count');


        // 2. Nombre de courriers refusés
        $query = $base;
        $query['conditions'][] = array('Bancontenu.etat' => -1);
        $results['Indicateurquant']['refuse'] = Hash::get($Courrier->find('all', $query), '0.0.count');

        // 3. Nombre de courriers enregistrés
        $query = $base;
        $query['conditions'][] = array('Bancontenu.etat' => 0);
        $results['Indicateurquant']['valide'] = Hash::get($Courrier->find('all', $query), '0.0.count');

        // 4. Nombre de courriers clos
        $query = $base;
        $query['conditions'][] = array('Bancontenu.etat' => 2);
        $results['Indicateurquant']['cloture'] = Hash::get($Courrier->find('all', $query), '0.0.count');

        // 5. Nombre de courriers validés
        $query = $base;
        $results['Indicateurquant']['total'] = Hash::get($Courrier->find('all', $query), '0.0.count');

        return $results;
    }

    /**
     * Retourne les résultats de la partie "Statistiques",
     * Indicateurs quantitatifs avec sélection des métadonnées
     * au 31 décembre de l'année".
     *
     * @param array $search
     * @return array
     */
    public function getIndicateursMetas(array $search) {
        $Courrier = ClassRegistry::init('Courrier');
        $annee = Hash::get($search, 'Statistique.annee');
        $results = array();

        // 0. Query de base
        $base = $this->_getBaseQuery($search, null);
        $base['fields'] = array('COUNT(DISTINCT(Courrier.id)) AS "count"');




        // 1. Nombre total de flux associé à une métadonnée sélectionnée
        if (!empty($search['Metadonnee'])) {
            foreach ($search['Metadonnee'] as $metaId => $valueChecked) {

                $metaName = $Courrier->CourrierMetadonnee->Metadonnee->find(
                        'first', array(
                    'fields' => array('Metadonnee.name'),
                    'conditions' => array('Metadonnee.id' => $metaId),
                    'recursive' => -1
                        )
                );
                if ($valueChecked['checked'] === '1') {
                    $query = $base;
                    $query['joins'][] = $Courrier->join(
                            'CourrierMetadonnee', array(
                        'type' => 'LEFT OUTER'
                            )
                    );
                    $query['joins'][] = $Courrier->CourrierMetadonnee->join(
                            'Metadonnee', array(
                        'type' => 'INNER',
//                        'conditions' => array(
//                            'Metadonnee.typemetadonnee_id' => Configure::read('Selectvaluemetadonnee.id')
//                        )
                            )
                    );
                    $query['joins'][] = $Courrier->CourrierMetadonnee->Metadonnee->join(
                            'Selectvaluemetadonnee', array(
                        'type' => 'INNER'
                            )
                    );
                    $query['conditions'][] = array('Metadonnee.id' => $metaId);
                    //debug($metaName);
                    $results['Indicateurmeta'][$metaName['Metadonnee']['name']] = Hash::get($Courrier->find('all', $query), '0.0.count');
                }
            }

            // 2. Nombre total de flux associé à une métadonnée sélectionnée par valeur renseignée
            foreach ($search['Metadonnee'] as $metaId => $valueChecked) {

                $courrier_metadonnee = $Courrier->CourrierMetadonnee->find(
                        'all', array(
                    'conditions' => array(
                        'CourrierMetadonnee.metadonnee_id' => $metaId
                    ),
                    'recursive' => -1
                        )
                );
                $metadonnee = $Courrier->CourrierMetadonnee->Metadonnee->find(
                        'all', array(
                    'conditions' => array(
                        'Metadonnee.id' => $metaId
                    ),
                    'contain' => array(
                        'Selectvaluemetadonnee'
                    ),
                    'order' => 'Metadonnee.name ASC'
                        )
                );
                if( $valueChecked['typemetadonnee_id'] == Configure::read('Selectvaluemetadonnee.id') ) {
                    $listeValeur = $Courrier->CourrierMetadonnee->Metadonnee->Selectvaluemetadonnee->find(
                            'list', array(
                        'conditions' => array('Selectvaluemetadonnee.id' => $valueChecked['selectvaluemetadonnee_id'])
                            )
                    );
                }

                foreach ($metadonnee as $i => $meta) {
                    if( !empty( $meta['Selectvaluemetadonnee'] ) ) {
                        foreach ($meta['Selectvaluemetadonnee'] as $j => $selectvalue) {
                            if ($valueChecked['checked'] === '1') {

                                $query = $base;
                                $query['joins'][] = $Courrier->join(
                                        'CourrierMetadonnee', array(
                                    'type' => 'LEFT OUTER'
                                        )
                                );
                                $query['joins'][] = $Courrier->CourrierMetadonnee->join(
                                        'Metadonnee', array(
                                    'type' => 'INNER'
                                        )
                                );
                                $query['joins'][] = $Courrier->CourrierMetadonnee->Metadonnee->join(
                                        'Selectvaluemetadonnee', array(
                                    'type' => 'INNER'
                                        )
                                );

                                // On veut les valeurs pour
                                //  la métadonnée sélectionnée
                                // la métadonnée liée au courrier
                                // la valeur prise par cette métadonnée dans le flux en question
                                if (!empty($valueChecked['selectvaluemetadonnee_id']) && !is_null($valueChecked['selectvaluemetadonnee_id'])) {
                                    $query['conditions'][] = array(
                                        'Metadonnee.id' => $metaId,
                                        'CourrierMetadonnee.metadonnee_id' => $metaId,
                                        'CourrierMetadonnee.valeur' => $valueChecked['selectvaluemetadonnee_id']
                                    );

                                    if( $valueChecked['typemetadonnee_id'] == Configure::read('Selectvaluemetadonnee.id') ) {
                                        $metadonnee[$i]['ValueInSelect'][$valueChecked['selectvaluemetadonnee_id']] = $listeValeur[$valueChecked['selectvaluemetadonnee_id']];
                                    }
                                    else {
                                        $metadonnee[$i]['ValueInSelect'][$valueChecked['selectvaluemetadonnee_id']] = $valueChecked['selectvaluemetadonnee_id'];
                                    }

                                    $valueByMeta = $metadonnee[$i]['Metadonnee']['name'] . ' - ' . $metadonnee[$i]['ValueInSelect'][$valueChecked['selectvaluemetadonnee_id']];
                                    $results['Indicateurmeta'][$valueByMeta] = Hash::get($Courrier->find('all', $query), '0.0.count');
                                } else {
                                    // Si aucune valeur sélectionnée, on affiche toutes les données
                                    $query['conditions'][] = array(
                                        'Metadonnee.id' => $metaId,
                                        'CourrierMetadonnee.metadonnee_id' => $metaId,
                                        'CourrierMetadonnee.valeur' => $selectvalue['id']
                                    );

                                    $metadonnee[$i]['ValueInSelect'][$selectvalue['id']] = $selectvalue['name'];
                                    $valueByMeta = $metadonnee[$i]['Metadonnee']['name'] . ' - ' . $metadonnee[$i]['ValueInSelect'][$selectvalue['id']];
                                    $results['Indicateurmeta'][$valueByMeta] = Hash::get($Courrier->find('all', $query), '0.0.count');
                                }
                            }
                        }
                    }
                    else {
                        $query = $base;
                        $query['joins'][] = $Courrier->join(
                                'CourrierMetadonnee', array(
                            'type' => 'LEFT OUTER'
                                )
                        );
                        $query['joins'][] = $Courrier->CourrierMetadonnee->join(
                                'Metadonnee', array(
                            'type' => 'INNER'
                                )
                        );

                        if ($valueChecked['checked'] === '1') {
                            // On veut les valeurs pour
                            //  la métadonnée sélectionnée
                            // la métadonnée liée au courrier
                            // la valeur prise par cette métadonnée dans le flux en question
                            if (!empty($meta['Metadonnee']['typemetadonnee_id']) && in_array( $meta['Metadonnee']['typemetadonnee_id'], array( 3, 4 ) ) ) {
                                $query['conditions'][] = array(
                                    'Metadonnee.id' => $metaId,
                                    'CourrierMetadonnee.metadonnee_id' => $metaId,
                                    'CourrierMetadonnee.valeur' => $valueChecked['selectvaluemetadonnee_id']
                                );
                                $metadonnee[$i]['ValueInSelect'][$valueChecked['selectvaluemetadonnee_id']] = $valueChecked['selectvaluemetadonnee_id'];
                                $valueByMeta = $metadonnee[$i]['Metadonnee']['name'] . ' - ' . $metadonnee[$i]['ValueInSelect'][$valueChecked['selectvaluemetadonnee_id']];
                                $results['Indicateurmeta'][$valueByMeta] = Hash::get($Courrier->find('all', $query), '0.0.count');

                            }
                            if (!empty($meta['Metadonnee']['typemetadonnee_id']) && in_array( $meta['Metadonnee']['typemetadonnee_id'], array( 1, 2 ) ) ) {
                                $query['conditions'][] = array(
                                    'Metadonnee.id' => $metaId,
                                    'CourrierMetadonnee.metadonnee_id' => $metaId,
                                    'CourrierMetadonnee.valeur' => $valueChecked['value']
                                );
                                $metadonnee[$i]['ValueInSelect'][$valueChecked['value']] = $valueChecked['value'];
                                $valueByMeta = $metadonnee[$i]['Metadonnee']['name'] . ' - ' . $metadonnee[$i]['ValueInSelect'][$valueChecked['value']];
                                $results['Indicateurmeta'][$valueByMeta] = Hash::get($Courrier->find('all', $query), '0.0.count');

                            }
                        }
                    }
                }
            }
        }



        // Services
        if (!empty($search['Statistique']['service'])) {
            $searchServices = Hash::get($search, 'Statistique.service');
            $conditions['Service.id'] = $searchServices;
            $services = $Courrier->Bancontenu->Desktop->DesktopsService->Service->find(
                'list',
                array(
                    'conditions' => $conditions,
                    'order' => 'Service.name ASC'
                )
            );
            foreach ($services as $serviceId => $serviceName) {
                $query = $base;
                $query['joins'][] = $Courrier->Bancontenu->join(
                    'Desktop', array(
                    'type' => 'INNER'
                        )
                );
                $query['joins'][] = $Courrier->Bancontenu->Desktop->join(
                    'DesktopsService', array(
                    'type' => 'INNER'
                        )
                );
                $query['joins'][] = $Courrier->Bancontenu->Desktop->DesktopsService->join(
                    'Service', array(
                    'type' => 'INNER'
                        )
                );

                $query['conditions']['OR'][] = array('Service.id' => $serviceId);
                $results['Indicateurservice'][$serviceName] = Hash::get($Courrier->find('all', $query), '0.0.count');
            }
        }
        return $results;
    }

    /**
     * Indicateurs pour le pourcentage de réponses + délai
     * @param array $search
     * @return type
     */
    public function getIndicateursPourcents(array $search) {
        $Courrier = ClassRegistry::init('Courrier');
        $annee = Hash::get($search, 'Statistique.annee');
        //$services = Hash::get( $search, 'Statistique.service' );
        $results = array();
//debug($search);
        // 0. Query de base
        $base = $this->_getBaseQuery($search, null);
        $base['fields'] = array('COUNT(DISTINCT(Courrier.id)) AS "Service__count"');
        $base['joins'][] = $Courrier->Bancontenu->join(
                'Desktop', array(
            'type' => 'LEFT OUTER'
                )
        );
        $base['joins'][] = $Courrier->Bancontenu->Desktop->join(
                'DesktopsService', array(
            'type' => 'LEFT OUTER'
                )
        );
        $base['joins'][] = $Courrier->Bancontenu->Desktop->DesktopsService->join(
                'Service', array(
            'type' => 'LEFT OUTER'
                )
        );



        // Nombre de courriers au total
        $query = $base;
        $nbTotalFlux = Hash::get($Courrier->find('all', $query), '0.Service.count');
        if ($nbTotalFlux == '0') {
            $nbTotalFlux = 1;
        }
        $results['Indicateurpourcent']['nbtotal'] = $nbTotalFlux;

        // Nombre de courriers clos sans suite
        $query['conditions'][] = array('Bancontenu.etat' => 2);
        $results['Indicateurpourcent']['sanssuite'] = Hash::get($Courrier->find('all', $query), '0.Service.count') * 100 / $nbTotalFlux;
        $results['Indicateurpourcent']['nbsanssuite'] = Hash::get($Courrier->find('all', $query), '0.Service.count');

        // Nombre de courriers clos avec réponse attendue
        $query['conditions'][] = array('Courrier.parent_id IS NOT NULL');
        $results['Indicateurpourcent']['reponseattendue'] = Hash::get($Courrier->find('all', $query), '0.0.count') * 100 / $nbTotalFlux;
        $results['Indicateurpourcent']['nbreponseattendue'] = Hash::get($Courrier->find('all', $query), '0.0.count');

        // Nombre de courriers clos sans suite par mois
        $mois = array(
            '01' => 'Janvier',
            '02' => 'Février',
            '03' => 'Mars',
            '04' => 'Avril',
            '05' => 'Mai',
            '06' => 'Juin',
            '07' => 'Juillet',
            '08' => 'Août',
            '09' => 'Septembre',
            '10' => 'Octobre',
            '11' => 'Novembre',
            '12' => 'Décembre'
        );


        $sqls = array();

        $conditions = array();
        $services = Hash::get($search, 'Statistique.service');
        if (!empty($services)) {
            $conditions['Service.id'] = $services;
        }
        // FIXME: conditions venant des filtres
        $Service = ClassRegistry::init('Service');
        $services = $Service->find(
                'list', array(
            'conditions' => $conditions,
            'order' => 'Service.name ASC'
                )
        );

        foreach ($mois as $value => $name) {
            foreach ($services as $serviceId => $serviceName) {
                // 0. Nb total de flux
                $queryTotal = $base;

                $queryTotal['conditions'][] = array('Service.id' => $serviceId);
                $queryTotal['conditions'][] = array('EXTRACT(MONTH FROM Bancontenu.created) = \'' . $value . '\'');
                $sqNbTotal = $Courrier->sq($queryTotal);

                // 1. En cours de traitement
                $queryEct = $base;
                $queryEct['conditions'][] = array('Bancontenu.etat NOT' => array(-1, 0, 2));
                $queryEct['conditions'][] = array('Service.id' => $serviceId);
                $queryEct['conditions'][] = array('EXTRACT(MONTH FROM Bancontenu.created) = \'' . $value . '\'');
                $sqEct = $Courrier->sq($queryEct);


                // 2. Clos
                $query = $base;
                $query['conditions'][] = array('Bancontenu.etat' => 2);
                $query['conditions'][] = array('EXTRACT(MONTH FROM Bancontenu.created) = \'' . $value . '\'');
                $query['conditions'][] = array('Service.id' => $serviceId);
                $sqSansSuite = $Courrier->sq($query);

                // 3. Clos avec Réponse attendue
                $queryNew = $base;
                $queryNew['conditions'][] = array('EXTRACT(MONTH FROM Bancontenu.created) = \'' . $value . '\'');
                $queryNew['conditions'][] = array('Service.id' => $serviceId);
                $queryNew['conditions'][] = array('Courrier.parent_id IS NOT NULL');
                $queryNew['conditions'][] = array('Bancontenu.etat <>' => 2);
                $sqReponse = $Courrier->sq($queryNew);


                // 4. Réponse dans les délais
                $queryDelai = $base;
                // jointure
                $queryDelai['joins'][] = $Courrier->join(
                        'Soustype', array(
                    'type' => 'LEFT OUTER'
                        )
                );
                //conditions
                $queryDelai['conditions'][] = array(
                    'OR' => array(
                        array(
                            'Courrier.delai_unite' => '0',
                            'Bancontenu.created <= ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 day\' ) )'
                        ),
                        array(
                            'Courrier.delai_unite' => '1',
                            'Bancontenu.created <= ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 week\' ) )'
                        ),
                        array(
                            'Courrier.delai_unite' => '2',
                            'Bancontenu.created <= ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 month\' ) )'
                        )
                    )
                );
                $queryDelai['conditions'][] = array('EXTRACT(MONTH FROM Bancontenu.created) = \'' . $value . '\'');
                $queryDelai['conditions'][] = array('Service.id' => $serviceId);
                $queryDelai['conditions'][] = array('Courrier.soustype_id IS NOT NULL');
                $queryDelai['conditions'][] = array(
                    'OR' => array(
                        'Courrier.delai_nb IS NOT NULL',
                        'Soustype.delai_nb IS NOT NULL'
                    )
                );
                $queryDelai['conditions'][] = array('Bancontenu.etat' => 2);
                $sqDelai = $Courrier->sq($queryDelai);


                // 5. Courrier en retard
                $queryRetard = $base;
                // jointure
                $queryRetard['joins'][] = $Courrier->join(
                        'Soustype', array(
                    'type' => 'LEFT OUTER'
                        )
                );
                $queryRetard['conditions'][] = array(
                    'OR' => array(
                        array(
                            'Courrier.delai_unite' => '0',
                            'Bancontenu.created <= ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 day\' ) )'
                        ),
                        array(
                            'Courrier.delai_unite' => '1',
                            'Bancontenu.created <= ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 week\' ) )'
                        ),
                        array(
                            'Courrier.delai_unite' => '2',
                            'Bancontenu.created <= ( "Courrier"."datereception" + ( "Courrier"."delai_nb" * INTERVAL \'1 month\' ) )'
                        )
                    )
                );
                $queryRetard['conditions'][] = array('EXTRACT(MONTH FROM Bancontenu.created) = \'' . $value . '\'');
                $queryRetard['conditions'][] = array('Service.id' => $serviceId);
                $queryRetard['conditions'][] = array('Courrier.soustype_id IS NOT NULL');
                $queryRetard['conditions'][] = array(
                    'OR' => array(
                        'Courrier.delai_nb IS NOT NULL',
                        'Soustype.delai_nb IS NOT NULL'
                    )
                );
                $sqRetard = $Courrier->sq($queryRetard);

                //FIXME: échapper correctementpour Pg
                $serviceName = str_replace("'", "''", $serviceName);
                $sqls[] = "SELECT '{$serviceName}' AS \"Statistique__name\","
                        . "'{$value}' AS \"Statistique__mois\","
                        . "( {$sqNbTotal} ) AS \"Statistique__count_total\","
                        . "( {$sqEct} ) AS \"Statistique__count_ect\","
                        . "( {$sqSansSuite} ) AS \"Statistique__count_ss\","
                        . "( {$sqReponse} ) AS \"Statistique__count_ra\","
                        . "( {$sqDelai} ) AS \"Statistique__count_delai\","
                        . "( {$sqRetard} ) AS \"Statistique__count_retard\"";
            }
        }

        if (!empty($sqls)) {
            $results['Indicateur'] = $Courrier->query(implode(' UNION ', $sqls) . ' ORDER BY "Statistique__name", "Statistique__mois" ');
        }
//            debug( implode( ' UNION ', $sqls ) );
//            debug( $results );
        // Délai
        // Nombre de courriers au total
        $query = $base;
        $nbTotalFlux = Hash::get($Courrier->find('all', $query), '0.0.count');
        $results['Indicateurdelai']['nbtotal'] = $nbTotalFlux;

//            $typesdemandes = array();
//            foreach( $typesdemandes as $value => $typedemande) {
//                $query = $base;
//                $query['conditions'][] = array(); // délai à calculer selon le delai_nb + delai_unite selon le sous-type
//                $results['Indicateurpourcent']["nbtypedemande{$value}"] = Hash::get( $Courrier->find( 'all', $query ), '0.0.count' );
//                $results['Indicateurpourcent']["typedemande{$value}"] = Hash::get( $Courrier->find( 'all', $query ), '0.0.count' ) * 100 / $nbTotalFlux;
//
//            }

        return $results;
    }

    /**
     * Retourne les statistiques géographiques
     * @param array $search
     * @return type
     */
    public function getIndicateursGeos(array $search) {
        $Bancommune = ClassRegistry::init('Bancommune');
        $Courrier = ClassRegistry::init('Courrier');

        $bancommuneId = Hash::get($search, 'Statistique.commune');
        $adresseNomAfnor = Hash::get($search, 'Statistique.adresse');
        $resultatNonvide = Hash::get($search, 'Statistique.resultatNonvide');

        // 1. Liste des noms de rues "AFNOR"
        $adresses = $Bancommune->Banadresse->find(
                'all', array(
            'recursive' => -1,
            'fields' => array(
                'DISTINCT on ("Banadresse"."nom_afnor")  "Banadresse"."nom_afnor" ',
//                'DISTINCT on  (Banadresse.nom_afnor ) AS Banadresse__nom_afnor ',
                'Banadresse.nom_afnor',
                'Banadresse.lat',
                'Banadresse.lon',
            ),
            'conditions' => array(
                'Banadresse.bancommune_id' => $bancommuneId,
                array(
                    'Banadresse.nom_afnor IS NOT NULL',
                    'Banadresse.nom_afnor <>' => '',
                )
            ),
            'recursive' => -1,
            'order' => array('Banadresse.nom_afnor')
                )
        );
        $base = $this->_getBaseQuery($search, null);
        $base['fields'] = array('COUNT(DISTINCT(Courrier.id)) AS "count"');

        $results['Indicateurgeo']['nbTotalCourrier'] = Hash::get($Courrier->find('all', $base), '0.0.count');

        /**
         *
         */
        $conditions = array();
        if (!empty($bancommuneId)) {
            $conditions['Bancommune.id'] = $bancommuneId;
        }
        // FIXME: conditions venant des filtres
        $Bancommune = ClassRegistry::init('Bancommune');
        $bancommune = $Bancommune->find(
                'first', array(
            'conditions' => $conditions,
            'order' => 'Bancommune.name ASC',
            'recursive' => -1
                )
        );


        $nameCommune = $bancommune['Bancommune']['name'];
//            foreach( $banscommunes as $bancommuneId => $nameCommune) {
        $query = $base;

        $query['joins'][] = $Courrier->join('Contact', array('type' => 'INNER'));

        $query['conditions'][] = array('Courrier.contact_id IS NOT NULL');
        $query['conditions']['Contact.ville ILIKE'] = '%' . $nameCommune . '%';

        $results['Indicateurgeo']['commune'][$bancommuneId] = $nameCommune;
        $results['Indicateurgeo'][$nameCommune] = Hash::get($Courrier->find('all', $query), '0.0.count');

        $results['Indicateurgeo']['sansadresse'] = $results['Indicateurgeo']['nbTotalCourrier'] - $results['Indicateurgeo'][$nameCommune];
//debug($query['conditions']);
        $sqls = array();
        foreach ($adresses as $adresse) {
            $nameAfnor = str_replace("'", "''", $adresse['Banadresse']['nom_afnor']);
            $latAfnor = str_replace("'", "''", $adresse['Banadresse']['lat']);
            $lonAfnor = str_replace("'", "''", $adresse['Banadresse']['lon']);
            $query['fields'] = array(
                '\'' . $nameAfnor . '\' AS "Statistique__name"',
                '\'' . $latAfnor . '\' AS "Statistique__lat"',
                '\'' . $lonAfnor . '\' AS "Statistique__lon"',
                'COUNT(DISTINCT(Courrier.id)) AS "Statistique__count"'
            );
            $query['conditions']['Contact.adresse ILIKE'] = $this->wildcard('%' . $adresse['Banadresse']['nom_afnor'] . '%');
            $sqls[] = $Courrier->sq($query);
        }



//

        /** DEBUT ** */
        // 1. Nombre total de flux associé à une métadonnée sélectionnée
        /* if( !empty($search['Metadonnee'])) {

          // 2. Nombre total de flux associé à une métadonnée sélectionnée par valeur renseignée
          foreach( $search['Metadonnee'] as $metaId => $valueChecked ) {

          $courrier_metadonnee = $Courrier->CourrierMetadonnee->find(
          'all',
          array(
          'conditions' => array(
          'CourrierMetadonnee.metadonnee_id' => $metaId
          ),
          'recursive' => -1
          )
          );
          $metadonnee = $Courrier->CourrierMetadonnee->Metadonnee->find(
          'all',
          array(
          'conditions' => array(
          'Metadonnee.id' => $metaId
          ),
          'contain' => array(
          'Selectvaluemetadonnee'
          ),
          'order' => 'Metadonnee.name ASC'
          )
          );
          foreach( $metadonnee as $i => $meta ) {
          foreach( $meta['Selectvaluemetadonnee'] as $j => $selectvalue ) {
          if($valueChecked === '1' ) {

          $query = $base;
          $query['joins'][] = $Courrier->join( 'Contactinfo', array( 'type' => 'INNER' ) );

          $query['conditions'][] = array( 'Courrier.contactinfo_id IS NOT NULL' );
          $query['conditions']['Contactinfo.ville ILIKE'] = '%'.$nameCommune.'%';

          foreach( $adresses as $adresse ) {
          $query['conditions']['Contactinfo.adresse ILIKE'] = $this->wildcard( '%'.$adresse['Banadresse']['nom_afnor'].'%' );
          }

          $query['joins'][] = $Courrier->join(
          'CourrierMetadonnee',
          array(
          'type' => 'LEFT OUTER'
          )
          );
          $query['joins'][] = $Courrier->CourrierMetadonnee->join(
          'Metadonnee',
          array(
          'type' => 'INNER'
          )
          );
          $query['joins'][] = $Courrier->CourrierMetadonnee->Metadonnee->join(
          'Selectvaluemetadonnee',
          array(
          'type' => 'INNER'
          )
          );

          // On veut les valeurs pour
          //  la métadonnée sélectionnée
          // la métadonnée liée au courrier
          // la valeur prise par cette métadonnée dans le flux en question
          $query['conditions'][] = array(
          'Metadonnee.id' => $metaId,
          'CourrierMetadonnee.metadonnee_id' => $metaId,
          'CourrierMetadonnee.valeur' => $selectvalue['id']
          );
          $metadonnee[$i]['ValueInSelect'][$selectvalue['id']] = $selectvalue['name'];
          $valueByMeta = $metadonnee[$i]['Metadonnee']['name'].' - '.$metadonnee[$i]['ValueInSelect'][$selectvalue['id']];
          $results['Indicateurgeo']['meta'][$metadonnee[$i]['Metadonnee']['name']][$metadonnee[$i]['ValueInSelect'][$selectvalue['id']]] = Hash::get( $Courrier->find( 'all', $query ), '0.0.count' );
          }

          }
          }
          }
          } */
//                $sqls[] = $Courrier->sq( $query );

        /*         * *FIN */


//debug($sqls);

        $results['Indicateurgeo']['adresse'] = $Courrier->query(implode(' UNION ', $sqls) . ' ORDER BY "Statistique__name" ');
        //obtenir les langtitude et longtitude de adresse
//        debug(count($results['Indicateurgeo']['adresse']));
//        foreach ($results['Indicateurgeo']['adresse'] as $adresse){
//            debug($adresse['Statistique']['name']);
//        }
//        $adresse_ancien = null;
//        foreach ($adresses as $adresse){
//            $adresse_ancien = $adresse
//        }
        // si on coche la case Résultat non vide
        if ($resultatNonvide == '1') {
            foreach ($results['Indicateurgeo']['adresse'] as $result) {
                if ($result['Statistique']['count'] > 0) {
                    $results['Indicateurgeo']['adresse']['nonvide'][] = $result;
                }
            }
        }
//debug($adresseNomAfnor);
//debug($results['Indicateurgeo']['adresse']);
        // si on sélectionne une adresse
        if (isset($adresseNomAfnor) && !empty($adresseNomAfnor)) {
            foreach ($results['Indicateurgeo']['adresse'] as $result) {
                if (in_array(@$result['Statistique']['name'], $adresseNomAfnor)) {
                    $results['Indicateurgeo']['adresse']['selected'][] = $result;
                }
            }
        }

        // 5. Nombre de courriers validés
        $query = $base;
        $results['Indicateurgeo']['total'] = Hash::get($Courrier->find('all', $query), '0.0.count');

        return $results;
    }

    /**
     *
     * @param array $search
     * @return type
     */
    public function getIndicateursAgents(array $search) {
        $Courrier = ClassRegistry::init('Courrier');
        $annee = Hash::get($search, 'Statistique.annee');
        $results = array();

        $agents = Hash::extract($search, 'Agent.Agent.{n}');

        foreach ($agents as $agent) {
            $search = array(
                'Statistique' => array(
                    'annee' => $annee
                ),
                'Agent' => array(
                    'Agent' => $agent
                )
            );
//debug($search);
            /*
             * Informations concernant les états du flux
             */
            // 0. Query de base
            $base = $this->_getBaseQuery($search, null);
            $base['fields'] = array('COUNT(DISTINCT(Courrier.id)) AS "count"');

            // 1. Nombre de courriers en cours de traitement
            $query = $base;
            $query['conditions'][] = array('Bancontenu.etat' => 1, 'Bancontenu.courrier_id IS NOT NULL');
            $results['IndicateurAgent'][$agent]['encours'] = Hash::get($Courrier->find('all', $query), '0.0.count');


            // 2. Nombre de courriers refusés
            $query = $base;
            $query['conditions'][] = array('Bancontenu.etat' => -1);
            $results['IndicateurAgent'][$agent]['refuse'] = Hash::get($Courrier->find('all', $query), '0.0.count');

            // 3. Nombre de courriers enregistrés
            $query = $base;
            $query['conditions'][] = array('Bancontenu.etat' => 0);
            $results['IndicateurAgent'][$agent]['valide'] = Hash::get($Courrier->find('all', $query), '0.0.count');

            // 4. Nombre de courriers clos
            $query = $base;
            $query['conditions'][] = array('Bancontenu.etat' => 2);
            $results['IndicateurAgent'][$agent]['cloture'] = Hash::get($Courrier->find('all', $query), '0.0.count');

            // 5. Nombre de courriers validés
            $query = $base;
            $results['IndicateurAgent'][$agent]['total'] = Hash::get($Courrier->find('all', $query), '0.0.count');
        }
        return $results;
    }

}

?>
