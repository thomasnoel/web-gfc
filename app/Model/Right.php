<?php

App::uses('AppModel', 'Model');

/**
 * Right model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Right extends AppModel {

    /**
     *
     * @var type
     */
    private $_env = array(
        'administrateur' => array(
            'environnement' => array(
                'controllers/Checks' => 1,
                'controllers/Environnement' => 1,
                'controllers/Collectivites' => 1,
                'controllers/Workflows' => 1,
                'controllers/Profils' => 1,
                'controllers/Users' => 1,
                'controllers/Courriers' => 1,
                'controllers/Documents' => 1,
                'controllers/Armodels' => 1,
                'controllers/Rmodels' => 1,
                'controllers/Relements' => 1,
                'controllers/Ars' => 1,
                'controllers/Services' => 1,
                'controllers/Soustypes' => 1,
                'controllers/Types' => 1,
                'controllers/Cakeflow' => 1,
                'controllers/Taches' => 1,
                'controllers/Metadonnees' => 1,
                'controllers/Dossiers' => 1,
                'controllers/Affaires' => 1,
                'controllers/Recherches' => 1,
                'controllers/Keywords' => 1,
                'controllers/Keywordlists' => 1,
                'controllers/Keywordtypes' => 1,
                'controllers/Contacts' => 1,
                'controllers/Contactinfos' => 1,
                'controllers/Addressbook' => 1,
                'controllers/Desktops' => 1,
                'controllers/Selectvaluesmetadonnees' => 1,
                'controllers/Connecteurs' => 1,
                'controllers/Courriers/sendToGed' => 1,
                'controllers/Courriers/sendDocumentToGed' => 1,
                'controllers/Scanemails' => 1,
                'controllers/Originesflux' => 1,
                'controllers/Referentielsfantoir' => 1,
                'controllers/Bans' => 1,
                'controllers/Banscommunes' => 1,
                'controllers/Bansadresses' => 1,
                'controllers/Statistiques' => 1,
                'controllers/Addressbooks' => 1,
                'controllers/Consoles' => 1
            )
        ),
        'utilisateur' => array(
            'environnement' => array(
                'controllers/Environnement/index' => 1,
                'controllers/Environnement/historique' => 1,
                'controllers/Environnement/userenv' => 1,
                'controllers/Environnement/setSessionPagination' => 1,
                'controllers/Environnement/getNotifications' => 1,
                'controllers/Environnement/setNotificationRead' => 1,
                'controllers/Environnement/setAllNotificationRead' => 1,
                'controllers/Environnement/infosLogiciel' => 1,
                'controllers/Repertoires' => 1,
                'controllers/Recherches' => 1,
                'controllers/Taches' => 1,
                'controllers/Desktops/setDeleg' => 1,
                'controllers/Comments' => 1,
                'controllers/Courriers/getComments' => 1,
                'controllers/Courriers/setComments' => 1,
                'controllers/Courriers/historiqueGlobal' => 1,
                'controllers/Courriers/sendToGed' => 1,
                'controllers/Courriers/sendDocumentToGed' => 1,
                'controllers/Courriers/sendmail' => 1,
                'controllers/Courriers/isInTraite' => 1,
//                'controllers/Courriers/unlink' => 1,
                'controllers/Contacts' => 1,
                'controllers/Contactinfos' => 1,
                'controllers/Addressbook' => 1,
                'controllers/Statistiques' => 1,
                'controllers/Addressbooks' => 1
            ),
            'utilisateur' => array(
                'controllers/Users/editinfo' => 1,
                'controllers/Users/changemdp' => 1,
                'controllers/Users/changenotif' => 1,
                'controllers/Users/setInfosPersos' => 1,
                'controllers/Users/getInfosCompte' => 1,
                'controllers/Users/ajaxformvalid' => 1,
                'controllers/Users/isAway' => 1,
//				'controllers/Users/getRights' => 1,
//				'controllers/Desktops/getRights' => 1
            )
        ),
        'mes_services' => array('environnement' => array('controllers/Environnement/getBancontenusServices' => 1)),
        'recherches' => array('environnement' => array('controllers/Recherches' => 1)),
        'suppression_flux' => array('environnement' => array('controllers/Courriers/delete' => 1)),
        'carnet_adresse' => array('environnement' => array('controllers/Addressbooks' => 1)),
        'statistiques' => array('environnement' => array('controllers/Statistiques' => 1))
    );

    /**
     *
     * @var type
     */
    private $_flux = array(
        'create' => array('controllers/Courriers/add' => 1),
        'read' => array('controllers/Courriers/view' => 1),
        'update' => array(
            'controllers/Courriers/edit' => 1,
//			'controllers/Courriers/getListSoustypes' => 1
        ),
        'delete' => array('controllers/Courriers/delete' => 1)
    );

    /**
     *
     * @var type
     */
    private $_context = array(
        'full' => array(
            'controllers/Courriers/getContext' => 1,
            'controllers/Courriers/getCircuit' => 1,
            'controllers/Documents/upload' => 1,
            'controllers/Documents/delete' => 1,
            'controllers/Documents/download' => 1,
            'controllers/Documents/setMainDoc' => 1,
            'controllers/Documents/getPreview' => 1,
            'controllers/Documents/getFichierScane' => 1,
            'controllers/Documents/mergeFile' => 1,
            'controllers/Documents/sousRepertoireMerge' => 1,
            'controllers/Documents/nouveauFlux' => 1,
            'controllers/Documents/getFusionPreview' => 1,
            'controllers/Documents/getScanFilePreview' => 1,
            'controllers/Courriers/getDocuments' => 1,
            'controllers/Courriers/getArs' => 1,
            'controllers/Courriers/getRelements' => 1,
            'controllers/Relements' => 1,
            'controllers/Rmodels/getPreview' => 1,
            'controllers/Armodels/getPreview' => 1,
            'controllers/Ars/getPreview' => 1,
            'controllers/Rmodels/download' => 1,
            'controllers/Courriers/setArs' => 1,
            'controllers/Courriers/genResponse' => 1,
            'controllers/Courriers/sendmail' => 1,
            'controllers/Courriers/detache' => 1,
            'controllers/Courriers/detachelot' => 1,
            'controllers/Courriers/traitementlot' => 1,
            'controllers/Courriers/execTraitementlot' => 1,
            'controllers/Courriers/refusparlot' => 1,
            'controllers/Courriers/sendcopylot' => 1,
            'controllers/Courriers/deletelot' => 1,
            'controllers/Ars' => 1,
            'controllers/Armodels/download' => 1,
            'controllers/Courriers/getParent' => 1,
            'controllers/Statistiques' => 1,
            'controllers/Addressbooks' => 1
        ),
        'simple' => array(
            'controllers/Courriers/getContext' => 1,
            'controllers/Courriers/getCircuit' => 1,
            'controllers/Documents/upload' => 1,
            'controllers/Documents/delete' => 1,
            'controllers/Documents/download' => 1,
            'controllers/Documents/setMainDoc' => 1,
            'controllers/Documents/getPreview' => 1,
            'controllers/Documents/sendmail' => 1,
            'controllers/Documents/getFichierScane' => 1,
            'controllers/Documents/getScanFilePreview' => 1,
            'controllers/Documents/getFusionPreview' => 1,
            'controllers/Documents/mergeFile' => 1,
            'controllers/Documents/sousRepertoireMerge' => 1,
            'controllers/Documents/nouveauFlux' => 1,
            'controllers/Courriers/getDocuments' => 1,
            'controllers/Courriers/getArs' => 0,
            'controllers/Courriers/getRelements' => 0,
            'controllers/Relements' => 0,
            'controllers/Rmodels/getPreview' => 0,
            'controllers/Armodels/getPreview' => 0,
            'controllers/Ars/getPreview' => 0,
            'controllers/Rmodels/download' => 0,
            'controllers/Courriers/setArs' => 0,
            'controllers/Courriers/genResponse' => 0,
            'controllers/Ars' => 0,
            'controllers/Armodels/download' => 0,
            'controllers/Courriers/getParent' => 0,
            'controllers/Courriers/detache' => 0
        ),
        'document_upload' => array('controllers/Documents/upload' => 1),
        'document_suppr' => array('controllers/Documents/delete' => 1),
        'document_setasdefault' => array('controllers/Documents/setMainDoc' => 1),
        'relements' => array('controllers/Courriers/getRelements' => 1),
        'relements_upload' => array('controllers/Relements/add' => 1),
        'relements_suppr' => array('controllers/Relements/delete' => 1),
        'ar_gen' => array('controllers/Courriers/getArs' => 1)
    );

    /**
     *
     * @var type
     */
    private $_workflow = array(
        'dispatch' => array(
            'controllers/Courriers/dispsend' => 1,
            'controllers/Courriers/dispmultiplesend' => 1
        ),
        'consult_scan' => array(
            'controllers/Documents/getFichierScane' => 1,
            'controllers/Documents/getScanFilePreview' => 1,
            'controllers/Documents/mergeFile' => 1,
            'controllers/Documents/sousRepertoireMerge' => 1,
            'controllers/Documents/nouveauFlux' => 1
        ),
        'send_back_to_disp' => array('controllers/Courriers/sendBackToDisp' => 1),
        'envoi_cpy' => array('controllers/Courriers/sendcpy' => 1),
        'envoi_wkf' => array('controllers/Courriers/sendwkf' => 1),
        'envoi_ged' => array(
            'controllers/Courriers/sendToGed' => 1,
            'controllers/Courriers/sendDocumentToGed' => 1
        ),
        'lot' => array(
            'controllers/Courriers/traitementlot' => 1,
            'controllers/Courriers/execTraitementlot' => 1,
            'controllers/Courriers/refusparlot' => 1,
            'controllers/Courriers/sendcopylot' => 1,
            'controllers/Courriers/deletelot' => 1,
            'controllers/Courriers/detachelot' => 1
        ),
        'insert' => array(
            'controllers/Courriers/getFlowControls' => 1,
            'controllers/Courriers/getCircuit' => 1,
            'controllers/Courriers/valid' => 0,
            'controllers/Courriers/send' => 1,
            'controllers/Courriers/sendcpy' => 1,
            'controllers/Courriers/sendwkf' => 0,
            'controllers/Courriers/insert' => 1,
            'controllers/Courriers/getInsertEnd' => 1,
            'controllers/Courriers/refus' => 0,
            'controllers/Courriers/reponse' => 0
        ),
        'active' => array(
            'controllers/Courriers/getFlowControls' => 1,
            'controllers/Courriers/getCircuit' => 1,
            'controllers/Courriers/valid' => 1,
            'controllers/Courriers/send' => 1,
            'controllers/Courriers/sendcpy' => 1,
            'controllers/Courriers/sendwkf' => 1,
            'controllers/Courriers/insert' => 0,
            'controllers/Courriers/getInsertEnd' => 0,
            'controllers/Courriers/refus' => 1,
            'controllers/Courriers/reponse' => 1
        ),
        'inactive' => array(
            'controllers/Courriers/getFlowControls' => 0,
            'controllers/Courriers/getCircuit' => 0,
            'controllers/Courriers/valid' => 0,
            'controllers/Courriers/send' => 0,
            'controllers/Courriers/sendcpy' => 0,
            'controllers/Courriers/sendwkf' => 0,
            'controllers/Courriers/insert' => 0,
            'controllers/Courriers/getInsertEnd' => 0,
            'controllers/Courriers/refus' => 0,
            'controllers/Courriers/reponse' => 0
        ),
		'action_carnet_adresse_flux' => array(
			'controllers/Courriers/add_contact' => 1,
			'controllers/Courriers/edit_contact' => 1,
			'controllers/Courriers/add_organisme' => 1,
			'controllers/Courriers/edit_organisme' => 1
		)
    );

    /**
     *
     * @var type
     */
    private $_tabs = array(
        'infos' => array(
            'get' => array(
                'controllers/Courriers/getInfos' => 1,
                'controllers/Contacts/add' => 0,
                'controllers/Contacts/getHomonyms' => 0,
                'controllers/Contacts/search' => 0,
                'controllers/Contacts/searchContactinfo' => 0,
                'controllers/Contactinfos/add' => 0,
                'controllers/Contactinfos/getHomonyms' => 0,
//				'controllers/Courriers/getContacts' => 0,
                'controllers/Soustypes/getRefSimul' => 0
            ),
            'create' => array(
                'controllers/Courriers/createInfos' => 1,
                'controllers/Contacts/add' => 1,
                'controllers/Contacts/edit' => 1,
                'controllers/Contacts/getHomonyms' => 1,
                'controllers/Contacts/search' => 1,
                'controllers/Contacts/searchAdresse' => 1,
                'controllers/Contacts/searchAdresseSpecifique' => 1,
                'controllers/Contacts/searchAdresseNomvoie' => 1,
                'controllers/Contacts/searchContactinfo' => 1,
                'controllers/Contactinfos/add' => 1,
                'controllers/Contactinfos/edit' => 1,
                'controllers/Contactinfos/getHomonyms' => 1,
//				'controllers/Courriers/getContacts' => 1,
                'controllers/Soustypes/getRefSimul' => 1
            ),
            'set' => array(
                'controllers/Courriers/setInfos' => 1,
                'controllers/Contacts/add' => 1,
                'controllers/Contacts/edit' => 1,
                'controllers/Contacts/getHomonyms' => 1,
                'controllers/Contacts/search' => 1,
                'controllers/Contacts/searchContactinfo' => 1,
                'controllers/Contacts/searchAdresse' => 1,
                'controllers/Contacts/searchAdresseSpecifique' => 1,
                'controllers/Contacts/searchAdresseNomvoie' => 1,
                'controllers/Contactinfos/add' => 1,
                'controllers/Contactinfos/edit' => 1,
                'controllers/Contactinfos/getHomonyms' => 1,
//				'controllers/Courriers/getContacts' => 1,
                'controllers/Soustypes/getRefSimul' => 1
            )
        ),
        'meta' => array(
            'get' => array('controllers/Courriers/getMeta' => 1),
            'set' => array('controllers/Courriers/setMeta' => 1)
        ),
        'taches' => array(
            'get' => array('controllers/Courriers/getTache' => 1),
            'set' => array('controllers/Courriers/setTache' => 1)
        ),
        'affaire' => array(
            'get' => array(
                'controllers/Courriers/getAffaire' => 1,
                'controllers/Dossiers/getComment' => 1,
                'controllers/Affaires/getComment' => 1
            ),
            'set' => array(
                'controllers/Courriers/setAffaire' => 1,
                'controllers/Dossiers/getComment' => 1,
                'controllers/Affaires/getComment' => 1
            )
        )
    );

    /**
     *
     * @var type
     */
    private $_rightSets = array(
        'administration' => array(
            'env' => array('administrateur', 'recherches', 'carnet_adresse'),
            'flux' => array('create', 'read', 'update', 'delete'),
            'context' => array('full', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen'),
            'workflow' => array('insert', 'envoi_cpy', 'send_back_to_disp', 'envoi_ged', 'consult_scan', 'lot', 'action_carnet_adresse_flux'),
            'tabs' => array(
                'infos' => array('create', 'get', 'set'),
                'meta' => array('get', 'set'),
                'taches' => array('get', 'set')
            )
        ),
        'aiguillage' => array(
            'env' => array('utilisateur', 'recherches', 'suppression_flux', 'statistiques', 'carnet_adresse'),
            'flux' => array('create', 'update', 'read', 'delete'),
            'context' => array('full', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen'),
            'workflow' => array('dispatch', 'consult_scan', 'inactive', 'lot', 'action_carnet_adresse_flux'),
            'tabs' => array(
                'infos' => array('get', 'set'),
                'meta' => array('get', 'set'),
                'taches' => array('get', 'set')
            )
        ),
        'creation' => array(
            'env' => array('utilisateur', 'recherches', 'suppression_flux', 'statistiques', 'carnet_adresse'),
            'flux' => array('create', 'read', 'update', 'delete'),
            'context' => array('full', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen'),
            'workflow' => array('insert', 'envoi_cpy', 'send_back_to_disp', 'lot', 'action_carnet_adresse_flux'),
            'tabs' => array(
                'infos' => array('create', 'get', 'set'),
                'meta' => array('get', 'set'),
                'taches' => array('get', 'set')
            )
        ),
        'validation' => array(
            'env' => array('utilisateur', 'recherches', 'suppression_flux', 'mes_services', 'statistiques', 'carnet_adresse'),
            'flux' => array('read', 'update'),
            'context' => array('full', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen'),
            'workflow' => array('active', 'envoi_wkf', 'envoi_cpy', 'envoi_ged', 'lot', 'action_carnet_adresse_flux'),
            'tabs' => array(
                'infos' => array('get'),
                'meta' => array('get', 'set'),
                'taches' => array('get', 'set'),
                'affaire' => array('get')
            )
        ),
        'edition' => array(
            'env' => array('utilisateur'),
            'flux' => array('read', 'update', 'delete'),
            'context' => array('full', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen'),
            'workflow' => array('action_carnet_adresse_flux'),
            'tabs' => array(
                'infos' => array('get', 'set'),
                'meta' => array('get', 'set'),
                'taches' => array('get', 'set'),
                'affaire' => array('get')
            )
        ),
        'documentation' => array(
            'env' => array('utilisateur', 'recherches', 'suppression_flux', 'mes_services', 'statistiques', 'carnet_adresse'),
            'flux' => array('read', 'update', 'delete'),
            'context' => array('full', 'document_upload', 'document_suppr', 'document_setasdefault', 'relements', 'relements_upload', 'relements_suppr', 'ar_gen'),
            'workflow' => array('active', 'envoi_wkf', 'envoi_cpy', 'envoi_ged', 'lot', 'action_carnet_adresse_flux'),
            'tabs' => array(
                'infos' => array('get'),
                'meta' => array('get', 'set'),
                'taches' => array('get', 'set'),
                'affaire' => array('get', 'set')
            )
        )
    );

    /**
     *
     * @var type
     */
    private $_groupRightSets = array(
        'Initiateur' => array('creation'),
        'Valideur' => array('validation'),
        'Valideur Editeur' => array('validation', 'edition'),
        'Documentaliste' => array('documentation'),
        'Aiguilleur' => array('aiguillage'),
        'Admin' => array('administration')
    );

    /**
     *
     * @param type $groupName
     * @return type
     */
    public function getRightSetsFromProfilName($groupName) {
        return $this->_groupRightSets[$groupName];
    }

    /**
     *
     * @param type $computed
     * @param type $rights
     */
    private function _processRights(&$computed, $rights) {
        foreach ($rights as $alias => $right) {
            if (in_array($alias, array_keys($computed))) {
                $computed[$alias] = $computed[$alias] || $right;
            } else {
                $computed[$alias] = $right;
            }
        }
    }

    private function _applyRights($aro_id) {
        $right = $this->find('first', array('conditions' => array('Right.aro_id' => $aro_id)));
        $return = array_merge($this->_applyRightSets($right), $this->_applyEnvs($right), $this->_applyWorkflows($right), $this->_applyContexts($right), !$right['Right']['administration'] ? $this->_applyTabs($right) : array());
        return $return;
    }

    /**
     *
     * @param type $aro_id
     * @return type
     */
    public function getRightSets($aro_id) {
        return $this->_applyRights($aro_id);
    }

    /**
     *
     * @param type $aro_id
     * @return type
     */
    private function _applyRightSets($right) {
        $rightSets = array();
        if (!empty($right['Right'])) {
            foreach ($right['Right'] as $key => $val) {
                if (in_array($key, array_keys($this->_rightSets)) && $val) {
                    $rightSets[] = $key;
                }
            }
        }
        $return = $this->_computeRights($rightSets);
        return $return;
    }

    /**
     *
     * @access private
     * @param type $rightSets
     * @return array
     */
    private function _computeRights($rightSets) {
        $computed = array();
        foreach ($rightSets as $rightSet) {
            //env
            foreach ($this->_rightSets[$rightSet]['env'] as $env) {
                foreach ($this->_env[$env] as $val) {
                    $this->_processRights($computed, $val);
                }
            }

            //flux
            if (!empty($this->_rightSets[$rightSet]['flux'])) {
                foreach ($this->_rightSets[$rightSet]['flux'] as $action) {
                    $this->_processRights($computed, $this->_flux[$action]);
                }
            }
            //context
            if (!empty($this->_rightSets[$rightSet]['context'])) {
                foreach ($this->_rightSets[$rightSet]['context'] as $context) {
                    $this->_processRights($computed, $this->_context[$context]);
                }
            }
            //workflow
            if (!empty($this->_rightSets[$rightSet]['workflow'])) {
                foreach ($this->_rightSets[$rightSet]['workflow'] as $action) {
                    $this->_processRights($computed, $this->_workflow[$action]);
                }
            }
            //tabs
            if (!empty($this->_rightSets[$rightSet]['tabs'])) {
                foreach ($this->_rightSets[$rightSet]['tabs'] as $tab => $actions) {
                    foreach ($actions as $action) {
                        $this->_processRights($computed, $this->_tabs[$tab][$action]);
                    }
                }
            }
        }
        return $computed;
    }

    private function _applyEnvs($right) {
        $rights = array();
        if (!empty($right['Right'])) {
            foreach ($right['Right'] as $key => $val) {
                if (in_array($key, array_keys($this->_env))) {
                    foreach ($this->_env[$key]['environnement'] as $aco => $value) {
                        $rights[$aco] = $val;
                    }
                }
            }
        }
        $return = $rights;
//		debug($return);
        return $return;
    }

    private function _applyWorkflows($right) {
        $rights = array();
        if (!empty($right['Right'])) {
            foreach ($right['Right'] as $key => $val) {
                if (in_array($key, array_keys($this->_workflow))) {
                    foreach ($this->_workflow[$key] as $aco => $value) {
                        $rights[$aco] = $val;
                    }
                }
            }
        }
        $return = $rights;
//		debug($return);
        return $return;
    }

    private function _applyContexts($right) {
        $rights = array();
        if (!empty($right['Right'])) {
            foreach ($right['Right'] as $key => $val) {
                if (in_array($key, array_keys($this->_context))) {
                    foreach ($this->_context[$key] as $aco => $value) {
                        $rights[$aco] = $val;
                    }
                }
            }
        }
        $return = $rights;
//		debug($return);
        return $return;
    }

    private function _applyTabs($right) {
        $rights = array();
        if (!empty($right['Right'])) {
            foreach ($right['Right'] as $key => $val) {
                $tmp = preg_split('#_#', $key);
                if (count($tmp) >= 2) {
                    if (in_array($tmp[1], array_keys($this->_tabs))) { //filtre des infos pertinentes
                        $values = $this->_tabs[$tmp[1]][$tmp[0]];
                        if ($tmp[1] == 'infos') {
                            foreach ($values as $aco => $access) {
                                if (empty($rights[$aco])) {
                                    $rights[$aco] = $access && $val;
                                }
                            }
                        } else {
                            foreach ($values as $aco => $access) {
                                $rights[$aco] = $access && $val;
                            }
                        }
                    }
                }
            }
        }
        $return = $rights;
        return $return;
    }

    /**
     *
     * @param array $options
     * @return array
     */
    private function _prepareTabs($options, $editMode = false) {
        $right = array();
        if (empty($options['tabs']) && !$editMode) {
            $options['tabs'] = array();

            $tabsToSave = array();
            foreach ($options['rightSets'] as $rightSet) {
//				if ($rightSet != 'administration') {
                $options['tabs'] = array_merge($options['tabs'], $this->_rightSets[$rightSet]['tabs']);
//				}
            }
            foreach ($options['tabs'] as $tab => $rights) {
                if ($tab == 'infos') {
                    $tabsToSave['create_infos'] = in_array('create', $rights);
                    $tabsToSave['get_infos'] = in_array('get', $rights);
                    $tabsToSave['set_infos'] = in_array('set', $rights);
                } else if ($tab == 'meta') {
                    $tabsToSave['get_meta'] = in_array('get', $rights);
                    $tabsToSave['set_meta'] = in_array('set', $rights);
                } else if ($tab == 'taches') {
                    $tabsToSave['get_taches'] = in_array('get', $rights);
                    $tabsToSave['set_taches'] = in_array('set', $rights);
                } else if ($tab == 'affaire') {
                    $tabsToSave['get_affaire'] = in_array('get', $rights);
                    $tabsToSave['set_affaire'] = in_array('set', $rights);
                }
            }
            $right = array_merge($right, $tabsToSave);
        } else {
            foreach (array_keys($this->_tabs) as $key_part1) {
                foreach (array_keys($this->_tabs[$key_part1]) as $key_part2) {
                    $right[$key_part2 . '_' . $key_part1] = false;
                }
            }
            foreach ($options['tabs'] as $tab) {
                $right[$tab] = true;
            }
        }
        return $right;
    }

    /**
     *
     * @param array $options
     * @return array
     */
    private function _prepareEnv($options, $editMode = false) {
        $right = array();
        if (empty($options['env']) && !$editMode) {
            $options['env'] = array();
            $envsToSave = array();
            foreach ($options['rightSets'] as $rightSet) {
                //if ($rightSet != 'administration') {
                $options['env'] = array_merge($options['env'], $this->_rightSets[$rightSet]['env']);
                //}
            }
            foreach ($options['env'] as $rights) {
                if (in_array($rights, array_keys($this->_env))) {
                    $envsToSave[$rights] = true;
                }
            }
            $right = array_merge($right, $envsToSave);
        } else {
            foreach (array_keys($this->_env) as $key) {
                if ($key != 'administrateur' && $key != 'utilisateur') {
                    $right[$key] = false;
                }
            }
            foreach ($options['env'] as $env) {
                if ($env != 'administrateur' && $env != 'utilisateur') {
                    $right[$env] = true;
                }
            }
        }
        return $right;
    }

    /**
     *
     * @param array $options
     * @return array
     */
    private function _prepareContext($options, $editMode = false) {
        $right = array();
        if (empty($options['context']) && !$editMode) {
            $options['context'] = array();
            $contextsToSave = array();
            foreach ($options['rightSets'] as $rightSet) {
//				if ($rightSet != 'administration') {
                $options['context'] = array_merge($options['context'], $this->_rightSets[$rightSet]['context']);
//				}
            }
            foreach ($options['context'] as $rights) {
                if (in_array($rights, array_keys($this->_context))) {
                    $contextsToSave[$rights] = true;
                }
            }
            $right = array_merge($right, $contextsToSave);
        } else {
            foreach (array_keys($this->_context) as $key) {
                if ($key != 'full' && $key != 'simple') {
                    $right[$key] = false;
                }
            }
            foreach ($options['context'] as $context) {
                if ($context != 'simple' && $context != 'full') {
                    $right[$context] = true;
                }
            }
        }
        return $right;
    }

    /**
     *
     * @param array $options
     * @return array
     */
    private function _prepareWorkflow($options, $editMode = false) {
        $right = array();
        if (empty($options['workflow']) && !$editMode) {
            $options['workflow'] = array();
            $workflowsToSave = array();
            foreach ($options['rightSets'] as $rightSet) {
//				if ($rightSet != 'administration') {
                $options['workflow'] = array_merge($options['workflow'], $this->_rightSets[$rightSet]['workflow']);
//				}
            }
            foreach ($options['workflow'] as $rights) {
                if (in_array($rights, array_keys($this->_workflow))) {
                    $workflowsToSave[$rights] = true;
                }
            }
            $right = array_merge($right, $workflowsToSave);
        } else {
            foreach (array_keys($this->_workflow) as $key) {
                if ($key != 'full' && $key != 'simple') {
                    $right[$key] = false;
                }
            }
            foreach ($options['workflow'] as $workflow) {
                if ($workflow != 'dispatch' && $workflow != 'insert' && $workflow != 'active' && $workflow != 'inactive') {
                    $right[$workflow] = true;
                }
            }
        }
        return $right;
    }

    /**
     *
     * @access public
     * @param type $aro_id
     * @param type $lightRightSet
     */
    public function saveRightSets($aro_id, $options = array(), $editMode = false) {
        $aroModel = ClassRegistry::init("Aro");
        $aro = $aroModel->find('first', array('conditions' => array('Aro.id' => $aro_id)));

        $right = $this->find('first', array('conditions' => array('Right.aro_id' => $aro_id)));
        if (empty($right)) {
            $right = array(
                'Right' => array()
            );
        }
        $right['Right']['aro_id'] = $aro_id;
        $right['Right']['administration'] = false;
        $right['Right']['aiguillage'] = false;
        $right['Right']['creation'] = false;
        $right['Right']['validation'] = false;
        $right['Right']['edition'] = false;
        $right['Right']['documentation'] = false;
        $right['Right']['model'] = $aro['Aro']['model'];
        $right['Right']['foreign_key'] = $aro['Aro']['foreign_key'];

        foreach ($options['rightSets'] as $rightSet) {
            $right['Right'][$rightSet] = true;
        }

        $right['Right'] = array_merge($right['Right'], $this->_prepareTabs($options, $editMode), $this->_prepareEnv($options, $editMode), $this->_prepareWorkflow($options, $editMode), $this->_prepareContext($options, $editMode));



        $right['Right']['mode_classique'] = false;
        $this->create();
        $saved = $this->save($right);



        if (!empty($saved)) {
            return $this->_applyRights($aro_id);
        } else {
            return false;
        }
    }

    /**
     * Retourne les onglets définis pour chaque groupe de droits
     *
     * @access public
     * @return array
     */
    public function getTabsForSets() {
        $return = array();
        foreach ($this->_rightSets as $rightSet => $data) {
            $return[$rightSet] = !empty($data['tabs']) ? $data['tabs'] : array();
        }
        return $return;
    }

    /**
     * Retourne les fonctionnalités d'environnement définis pour chaque groupe de droits
     *
     * @access public
     * @return array
     */
    public function getEnvsForSets() {
        $return = array();
        foreach ($this->_rightSets as $rightSet => $data) {
            $return[$rightSet] = !empty($data['env']) ? $data['env'] : array();
        }
        return $return;
    }

    /**
     * Retourne les fonctionnalités du panneau contextuel définis pour chaque groupe de droits
     *
     * @access public
     * @return array
     */
    public function getCtxsForSets() {
        $return = array();
        foreach ($this->_rightSets as $rightSet => $data) {
            $return[$rightSet] = !empty($data['context']) ? $data['context'] : array();
        }
        return $return;
    }

    /**
     * Retourne les fonctionnalités de circuit de traitement définis pour chaque groupe de droits
     *
     * @access public
     * @return array
     */
    public function getWkfsForSets() {
        $return = array();
        foreach ($this->_rightSets as $rightSet => $data) {
            $return[$rightSet] = !empty($data['workflow']) ? $data['workflow'] : array();
        }
        return $return;
    }

}

?>
