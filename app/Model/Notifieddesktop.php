<?php

App::uses('AppModel', 'Model');

/**
 * Notifieddesktop model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Notifieddesktop extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Notifieddesktop';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'desktop_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => false,
			),
		),
		'notification_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => false,
			),
		),
		'courrier_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Desktop' => array(
			'className' => 'Desktop',
			'foreignKey' => 'desktop_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Notification' => array(
			'className' => 'Notification',
			'foreignKey' => 'notification_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'courrier_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

}

?>
