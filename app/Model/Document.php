<?php

App::uses('AppModel', 'Model');

/**
 * Document model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Document extends AppModel {

    /**
     * Display field
     *
     * @var string
     */
    public $displayField = 'name';
    protected $_lastSequenceDocument = null;

    /**
     *
     * @var type
     */
    public $actsAs = array('Linkeddocs');

    /**
     *
     * @var type
     */
    public $convertibleMimeTypes = array(
        "application/vnd.oasis.opendocument.text",
        "application/vnd.oasis.opendocument.spreadsheet",
        "image/bmp",
        "image/tiff",
        "text/plain",
        "application/msword",
        "application/x-octetstream",
        "application/msexcel",
        "application/xls",
        "application/vnd.ms-office",
        "application/vnd.ms-excel",
        "application/rtf",
        "application/binary",
		"application/xml",
        "OCTET-STREAM"
    );

    /**
     *
     * @var type
     */
    public $otherMimeTypes = array(
        'audio' => array(
            'audio/x-wav' => 'wav',
            'audio/mpeg' => 'mp3',
            'application/ogg' => 'ogg',
            'audio/ogg' => 'ogg'
        ),
        'video' => array(
            'video/x-msvideo' => 'avi',
            'video/mpeg' => 'mpg',
            'video/mp4' => 'mp4',
            'application/ogg' => 'ogg',
            'video/ogg' => 'ogg'
        ),
        'image' => array(
            'image/gif' => 'gif',
            'image/jpeg' => 'jpg',
            'image/png' => 'png'
        ),
        'file' => array(
            'application/vnd.oasis.opendocument.text' => 'odt',
            'OCTET-STREAM' => 'odt',
            'application/vnd.oasis.opendocument.spreadsheet' => 'ods',
            'text/plain' => 'txt',
            'text/xml' => 'xml',
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            'notBlank' => array(
                'rule' => array('notBlank'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'size' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'courrier_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'courrier_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany
     *
     * @var mixed
     * @access public
     */
    public $hasMany = array(
        'Sendmail' => array(
            'className' => 'Sendmail',
            'foreignKey' => 'document_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        )
    );

    /**
     *
     * @param type $id
     * @return type
     * @throws NotFoundException
     */
    public function generatePreview($id = null, $path = null) {
        if (!empty($id)) {
            $this->id = $id;
        }

        if (empty($this->id)) {
            throw new NotFoundException();
        }

        $pdfContent = '';
        $fileContent = '';
        $this->recursive = -1;
        $document = $this->read();
        $return = __d('document', 'Document.preview.error');

        $isPdf = ($document['Document']['mime'] == 'application/pdf' || $document['Document']['mime'] == 'PDF');
        $isAR = strpos( $document['Document']['name'], 'AR' );
        if( $isAR !== false ) {
            $isPdf = false;
        }
        if (!empty($path)) {
            require_once 'XML/RPC2/Client.php';

            $content = file_get_contents($path);
            $options = array(
                'uglyStructHack' => true
            );
            $url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
            $cli = XML_RPC2_Client::create($url, $options);

            $result = $cli->convertFile(base64_encode($content), 'odt', 'pdf', false, true);
            $pdfContent = base64_decode($result);

        } else {
            //Conversion en pdf via Gedooo
            if (in_array($document['Document']['mime'], $this->convertibleMimeTypes)) {
                require_once 'XML/RPC2/Client.php';

                $content = $document['Document']['content'];
                $options = array(
                    'uglyStructHack' => true
                );
                $url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
                $cli = XML_RPC2_Client::create($url, $options);

                $result = $cli->convertFile(base64_encode($content), 'odt', 'pdf', false, true);
                $pdfContent = base64_decode($result);



            } else if ( $isPdf ) {

                $pdfContent = $this->field('Document.content', array('Document.id' => $id));
            } else if ($document['Document']['mime'] == 'image/png' || $document['Document']['mime'] == 'image/jpeg') {
                // Si c'est une image, on retourne le contenu directement
                $return = $this->field('Document.content', array('Document.id' => $id));
            } else {
//                debug($document);
                require_once 'XML/RPC2/Client.php';
                $content = $document['Document']['content'];
                $options = array(
                    'uglyStructHack' => true
                );
                $url = 'http://' . Configure::read('CLOUDOOO_HOST') . ':' . Configure::read('CLOUDOOO_PORT');
                $cli = XML_RPC2_Client::create($url, $options);
                $result = $cli->convertFile(base64_encode($content), 'odt', 'pdf', false, true);
                $pdfContent = base64_decode($result);

            }
        }

        //Conversion en flash
        if (!empty($pdfContent)) {
            $return = $pdfContent;
        }

        return $return;
    }

    /**
     * Récupère toutes les pièces jointes (associées à 1 flux) ayant la propriété main_doc à false
     * @param null|int|array $flux_id
     * @return mixed tableau d'annexes
     */
    public function getPiecesJointes($flux_id = null) {
        $conditions = array('main_doc' => false);

        if (!empty($flux_id))
            $conditions['courrier_id'] = $flux_id;

        $piecesjointes = $this->find('all', array(
            'conditions' => $conditions,
            'order' => array('id' => 'ASC'),
            'recursive' => -1
        ));

        return $piecesjointes;
    }

    /**
     * Génération auto de la référence unique du courrier
     * @param type $id
     * @return string|null
     */
    public function genNameUnique($id = null, $data = null) {
        $filename = null;
        $sql = "SELECT nextval('documents_name_seq'); /*" . mktime(true) . " " . $this->_lastSequenceDocument . "*/";


        $numSeqDoc = $this->query($sql);
        if ($numSeqDoc === false) {
            return null; // FIXME: Throw exception
        }
        $this->_lastSequenceDocument = $numSeqDoc[0][0]['nextval'];
        $filename = "fichier_" . sprintf("%012s", $numSeqDoc[0][0]['nextval']);
        return $filename;
    }

	/**
	 * @param $id
	 */
    public function isValid( $id )  {
		$isValid = false;
    	$document = $this->find(
    		'first',
			array(
				'conditions' => [
					'Document.id' => $id
				],
				'contain' => false,
				'recursive' => -1
			)
		);
		if( !empty($document) ) {
			$documentPath = $document['Document']['path'];
			if (file_exists($documentPath)) {
				$isValid = true;
			}
		}
    	return $isValid;
	}
}
