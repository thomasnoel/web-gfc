<?php

App::uses('AppModel', 'Model');

/**
 * Dossier model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Dossier extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Dossier';

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'name' => array(
			'rule' => 'isUnique',
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false
			)
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Affaire' => array(
			'className' => 'Affaire',
			'foreignKey' => 'dossier_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

	/**
	 *
	 * @var type
	 */
//  public $belongsTo = array(
//      'Collectivite' => array(
//          'className' => 'Collectivite',
//          'foreignKey' => 'collectivite_id',
//          'type' => 'INNER',
//          'conditions' => null,
//          'fields' => null,
//          'order' => null
//      )
//  );

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'Recherche' => array(
			'className' => 'Recherche',
			'joinTable' => 'recherches_dossiers',
			'foreignKey' => 'dossier_id',
			'associationForeignKey' => 'recherche_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheDossier'
		)
	);

	/**
	 *
	 * @param type $collectivite_id
	 * @return array
	 */
	public function getDossiersAffairesTree() {
//		$dossiers = $this->find("all", array('order' => 'Dossier.name'));
		//on recupere les affaire non associées a un dossier
		$affaires_tmp = $this->Affaire->find("all", array('order' => 'Affaire.name', 'conditions' => array(array('Affaire.dossier_id' => ''))));
		$affaires = array();
		foreach ($affaires_tmp as $affaire) {
			$affaires[] = $affaire['Affaire'];
		}
		$dossiers[] = array('Dossier' => array('id' => '', 'name' => __d('affaire', 'affaires non classees')), 'Affaire' => $affaires);
		return $dossiers;
	}

}

?>
