<?php

App::uses('AppModel', 'Model');

/**
 * User model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class User extends AppModel {

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'User';

    /**
     * Model display field
     *
     * @access public
     */
    public $displayField = 'username';

    /**
     * Validation rules
     *
     * @access public
     */
    public $validate = array(
        'username' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
            array(
                'rule' => array('isunique')
            )
        ),
        'password' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        ),
        'nom' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        ),
        'prenom' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        ),
        'mail' => array(
//            array(
//                'rule' => array('notBlank'),
//                'allowEmpty' => false,
//            ),
            array(
                'rule' => array('email')
            )
        ),
        'desktop_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'foreignKey' => 'desktop_id',
            'type' => 'LEFT',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

    /**
     * hasAndBelongsToMany
     *
     * @access public
     */
    public $hasAndBelongsToMany = array(
//      'Tache' => array(
//          'className' => 'Tache',
//          'joinTable' => 'taches_users',
//          'foreignKey' => 'user_id',
//          'associationForeignKey' => 'tache_id',
//          'unique' => null,
//          'conditions' => null,
//          'fields' => null,
//          'order' => null,
//          'limit' => null,
//          'offset' => null,
//          'finderQuery' => null,
//          'deleteQuery' => null,
//          'insertQuery' => null,
//          'with' => 'TachesUser'
//      ),
        'SecondaryDesktop' => array(
            'className' => 'Desktop',
            'joinTable' => 'desktops_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'desktop_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'DesktopsUser'
        ),
        'Typenotif' => array(
            'className' => 'Typenotif',
            'joinTable' => 'typenotifs_users',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'typenotif_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'TypenotifUser'
        )
    );

    /**
     *
     * @var type
     */
    public $hasMany = array(
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'user_creator_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Notifquotidien' => array(
            'className' => 'Notifquotidien',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'exclusive' => null,
            'finderQuery' => null,
            'counterQuery' => null
        ),
		'Bancontenu' => array(
			'className' => 'Bancontenu',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
    );

    /**
     *
     * @var type
     */
    private $_defaultSaveMsg = array(
        'state' => 'add',
        'passwordConversion' => false,
        'userAdded' => false,
        'userAroAdded' => false,
        'userDaroAdded' => array(),
        'debug' => ''
    );

    /**
     *
     * @var type
     */
    public $saveMsg = array();

    /**
     *
     * @param type $id
     * @param type $table
     * @param type $ds
     */
    public function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
        if ($this->useDbConfig == 'default') {
            $this->hasMany = array();
            $this->hasAndBelongsToMany = array();
            $this->belongsTo = array();
            $this->validate = array(
                'username' => array(
                    array(
                        'rule' => array('notBlank'),
                        'allowEmpty' => false,
                    ),
                ),
                'password' => array(
                    array(
                        'rule' => array('notBlank'),
                        'allowEmpty' => false,
                    ),
                ),
                'nom' => array(
                    array(
                        'rule' => array('notBlank'),
                        'allowEmpty' => false,
                    ),
                ),
                'prenom' => array(
                    array(
                        'rule' => array('notBlank'),
                        'allowEmpty' => false,
                    ),
                ),
                'mail' => array(
                    array(
                        'rule' => array('email', false)
                    ),
                )
            );
        }
    }

    /**
     *
     * @return null
     */
//  public function parentNode() {
//    if (!$this->id && empty($this->data)) {
//      return null;
//    }
//    if (isset($this->data['User']['profil_id'])) {
//      $groupId = $this->data['User']['profil_id'];
//    } else {
//      $groupId = $this->field('profil_id');
//    }
//    if (!$groupId) {
//      return null;
//    } else {
//      return array('Profil' => array('id' => $groupId));
//    }
//  }

    /**
     *
     */
    private function _initSaveMsg() {
        $this->saveMsg = $this->_defaultSaveMsg;
    }

    /**
     *
     * @return boolean
     */
    public function beforeSave($options = array()) {
        $this->_initSaveMsg();
        if (isset($this->data['User']['password'])) {
//            $this->data['User']['password'] = Security::hash($this->data['User']['password'], 'sha256', true);
            $this->saveMsg['passwordConversion'] = true;
        }
        return true;
    }

    /**
     *
     * @return type
     */
    public function samePassword() {
        return ((!empty($this->data['User']['password'])) && ($this->data['User']['password'] == $this->data['User']['password2']));
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function validatesPassword($data) {
        return ((!empty($data['User']['password'])) && ($data['User']['password'] == $data['User']['password2']));
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function validOldPassword($data) {
        $oldPass = $this->find('first', array('conditions' => array('id' => $data['User']['id']), 'fields' => array('password'), 'recursive' => -1));
        return (md5($data['User']['oldpassword']) == $oldPass['User']['password']);
    }

    /**
     *
     * @param type $id
     * @param type $field
     * @return null
     */
    public function circuitDefaut($id = null, $field = '') {
        $circuitDefautId = 0;
        $user = $this->findById($id);
        // Circuit par défaut défini au niveau de l'utilisateur
        if (!empty($user['User']['circuit_defaut_id']))
            $circuitDefautId = $user['User']['circuit_defaut_id'];
        else {
            // Premier circuit par défaut défini pour les services de l'utilisateur
            foreach ($user['Service'] as $service) {
                if (!empty($service['circuit_defaut_id'])) {
                    $circuitDefautId = $service['circuit_defaut_id'];
                    break;
                }
            }
        }
        if ($circuitDefautId > 0) {
            $this->Circuit->recursive = -1;
            $circuit = $this->Composition->Etape->Circuit->findById($circuitDefautId);
            if (empty($field))
                return $circuit;
            else
                return $circuit['Circuit'][$field];
        } else
            return null;
    }

    /**
     *
     * @param type $user_id
     * @return type
     */
    public function getServicesPath($user_id) {
        $this->Service->recursive = -1;
        $user = $this->find('first', array('contain' => array('Service'), 'conditions' => array('User.id' => $user_id)));
        $userHierarchies = array();
        foreach ($user['Service'] as $serviceUser) {
            $userHierarchies[$serviceUser['name']] = array();
            $userHierarchies[$serviceUser['name']][] = $serviceUser['id'];
            $this->Service->id = $serviceUser['id'];
            do {
                $parent_id = $this->Service->field('parent_id');
                if (!empty($parent_id)) {
                    $userHierarchies[$serviceUser['name']][] = $parent_id;
                }
                $this->Service->id = $parent_id;
            } while (!empty($parent_id));
            $userHierarchies[$serviceUser['name']] = array_reverse($userHierarchies[$serviceUser['name']]);
        }
        return $userHierarchies;
    }

    /**
     *
     * @param type $hierarchies1
     * @param type $hierarchies2
     * @return boolean
     */
    public function compareServicesPath($hierarchies1, $hierarchies2) {
        $right = false;
        foreach ($hierarchies1 as $hierarchie1) {
            foreach ($hierarchies2 as $hierarchie2) {
                if ($this->_compareSingleServicesPath($hierarchie1, $hierarchie2)) {
                    $right = true;
                }
            }
        }
        return $right;
    }

    /**
     *
     * @param type $hierarchie1
     * @param type $hierarchie2
     * @return boolean
     */
    protected function _compareSingleServicesPath($hierarchie1, $hierarchie2) {
        $right = true;
        $count1 = count($hierarchie1);
        $count2 = count($hierarchie2);
        if ($count1 == $count2) {
            for ($cpt = 0; $cpt < $count1; $cpt++) {
                if ($hierarchie1[$cpt] != $hierarchie2[$cpt]) {
                    $right = false;
                }
            }
        } else {
            if ($count1 > $count2) {
                $right = false;
            } else {
                for ($cpt = 0; $cpt < $count1; $cpt++) {
                    if ($hierarchie1[$cpt] != $hierarchie2[$cpt]) {
                        $right = false;
                    }
                }
            }
        }
        return $right;
    }

    /**
     *
     * @param type $id
     * @param type $delegue_id
     * @return type
     */
    public function setAway($id, $delegue_id) {
        $data = array(
            'User' => array(
                'id' => $id,
                'delegue' => $delegue_id,
                'date_dep' => date("Y-m-d H:i:s")
            )
        );
        $this->create($data);
        return $this->save();
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function setPresent($id) {
        $data = array(
            'User' => array(
                'id' => $id,
                'delegue' => null,
                'date_ret' => date("Y-m-d H:i:s")
            )
        );
        $this->create($data);
        return $this->save();
    }

    /**
     *
     * @param type $array
     * @return int
     */
    private function _array_sum_boolean($array) {
        $cpt = 0;
        for ($i = 0; $i < count($array); $i++) {
            if ($array[$i] === true) {
                $cpt++;
            }
        }
        return $cpt;
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function isAway($id) {
        $desktops = $this->getDesktops($id);
        $return = 0;
        for ($i = 0; $i < count($desktops); $i++) {
            $tmp = $this->Desktop->DesktopsUser->find('count', array('conditions' => array('DesktopsUser.desktop_id' => $desktops[$i], 'DesktopsUser.delegation' => true)));
            $return += $tmp;
        }
        return $return;
    }

    /**
     *
     * @param type $data
     * @return type
     */
    public function convertFindAllToList($model, $data) {
        $ret = array();
        for ($i = 0; $i < count($data); $i++) {
            $ret[$data[$i]['User']['id']] = $data[$i]['User']['username'];
        }
        return $ret;
    }

    /**
     *
     * @param type $id
     * @return null
     */
    public function getDesktops($id = null) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }
        $querydata = array(
            'conditions' => array(
                'User.id' => $this->id
            ),
            'contain' => array(
                $this->Desktop->alias,
                $this->SecondaryDesktop->alias,
            )/* ,
                  'recursive' => -1 */
        );
        $user = $this->find('first', $querydata);
        $return = array($user['Desktop']['id']);
        foreach ($user['SecondaryDesktop'] as $desktop) {
            $return[] = $desktop['id'];
        }
        return $return;
    }

    /**
     *
     * @param type $id
     * @return null
     */
    public function getDelegatedDesktops($id = null) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }
        $querydata = array(
            'conditions' => array(
                'DesktopsUser.user_id' => $this->id,
                'DesktopsUser.delegation' => true
            ),
            'fields' => array(
                'DesktopsUser.id',
                'DesktopsUser.desktop_id'
            )
        );
        return $this->DesktopsUser->find('list', $querydata);
    }

    /**
     *
     * @param type $id
     * @return array
     */
    public function getDelegatedDesktopsContents($id = null) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }
        $contents = array();
        $desktops = $this->DesktopsUser->find('all', array(
            'conditions' => array(
                'DesktopsUser.user_id' => $id,
                'DesktopsUser.delegation' => true
            ),
            'fields' => array(
                'Desktop.id',
                'Desktop.name'
            ),
            'contain' => array(
                'Desktop' => array(
                    'DesktopsService' => array(
                        'Service'
                    )
                )
            ),
            'recursive' => -1
        ));
        for ($i = 0; $i < count($desktops); $i++) {
            $contents[$i]['desktopName'] = $desktops[$i]['Desktop']['name'];

            if ($desktops[$i]['Desktop']['name'] == 'Bureau administrateur') {
                $contents[$i]['serviceName'] = ' - ';
            } else {
                $service = $this->DesktopsUser->Desktop->DesktopsService->find('first', array(
                    'conditions' => array(
                        'DesktopsService.desktop_id' => $desktops[$i]['Desktop']['id']
                    ),
                    'fields' => array(
                        'Service.name'
                    ),
                    'contain' => array(
                        'Service'
                    ),
                    'recursive' => -1
                ));
                $contents[$i]['serviceName'] = isset($service['Service']['name']) ? $service['Service']['name'] : "";
            }
        }
        return $contents;
    }

    /**
     *
     * @param type $id
     * @return null
     */
    public function getDelegateToDesktops($id = null) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }

        $return = array();

        $desktops = $this->getDesktops($id);
        $delegatedDesktops = $this->getDelegatedDesktops($id);

        foreach ($desktops as $desktop) {
            if (!in_array($desktop, $delegatedDesktops)) {
                $querydata = array(
                    'conditions' => array(
                        'DesktopsUser.desktop_id' => $desktop,
                        'DesktopsUser.delegation' => true
                    ),
                    'fields' => array(
                        'DesktopsUser.user_id'
                    )
                );
                $usersList = $this->DesktopsUser->find('list', $querydata);
                if (!empty($usersList)) {
                    $return[$desktop] = $usersList;
                }
            }
        }

        return $return;
    }

    /**
     *
     * @param type $id
     * @return array
     */
    public function getDelegateToDesktopsContents($id = null) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }

        $contents = array();

        $desktops = $this->getDesktops($id);
        $delegatedDesktops = $this->getDelegatedDesktops($id);
        //for($i=0;$i<count($desktops);$i++){
        foreach ($desktops as $desktop) {
            if (!in_array($desktop, $delegatedDesktops)) {
                $querydata = array(
                    'conditions' => array(
                        'DesktopsUser.desktop_id' => $desktop,
                        'DesktopsUser.delegation' => true
                    ),
                    'fields' => array(
                        'Desktop.name',
                        'User.nom',
                        'User.prenom'
                    ),
                    'contain' => array(
                        'User',
                        'Desktop'
                    )
                );
                $infos = $this->DesktopsUser->find('all', $querydata);
                if (!empty($infos)) {
                    for ($i = 0; $i < count($infos); $i++) {
                        $contents[$i]['UserInfo'] = $infos[$i]['User']['nom'] . ' ' . $infos[$i]['User']['prenom'];
                        $contents[$i]['Role'] = $infos[$i]['Desktop']['name'];
                    }
                }
            }
        }

        return $contents;
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getServices($id = null) {
        $desktopList = $this->getDesktops($id);
        $return = array();
        foreach ($desktopList as $desktop) {
            $items = $this->Desktop->DesktopsService->find('list', array('conditions' => array('DesktopsService.desktop_id' => $desktop)));
            foreach ($items as $item) {
                if (!in_array($item, $return)) {
                    $return[] = $item;
                }
            }
        }
        return $return;
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getProfils($id = null) {
        $desktopList = $this->getDesktops($id);
        $return = array();
        foreach ($desktopList as $desktop) {
            $item = $this->Desktop->field('profil_id', array('Desktop.id' => $desktop));
            if (!in_array($item, $return)) {
                $return[] = $item;
            }
        }
        return $return;
    }

    /**
     *
     * @param type $id
     * @param type $alias
     * @return null
     */
    public function getDaros($id = null, $alias = false) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }
        $return = array();
        $desktops = $this->getDesktops();
        foreach ($desktops as $desktop) {
            $daros = $this->Desktop->getDaros($desktop);
            foreach ($daros as $daro) {
                if ($alias) {
                    $return[$daro['Daro']['id']] = $daro['Daro']['alias'];
                } else {
                    $return[] = $daro['Daro'];
                }
            }
        }
        return $return;
    }

    /**
     *
     * @param type $id
     * @return null
     */
    public function getInfos($id = null) {
        if ($id != null) {
            $this->id = $id;
        }
        if (!$this->id) {
            return null;
        }
        $querydata = array(
            'conditions' => array(
                'User.id' => $id
            ),
            'contain' => array(
                $this->Desktop->alias => array(
                    $this->Desktop->Profil->alias,
                    $this->Desktop->Service->alias
                ),
                $this->SecondaryDesktop->alias => array(
                    $this->SecondaryDesktop->Profil->alias,
                    $this->SecondaryDesktop->Service->alias
                )
            )
        );
        $return = $this->find('first', $querydata);
        return !empty($return) ? $return : null;
    }

    /**
     * Fonciton permettant de rechercher les utilisateurse selon certains critères
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres) {
        /// Conditions de base
        $conditions = array();
        // Critères sur l'utilisateur
        foreach (array('username', 'nom', 'prenom') as $critereUser) {
            if (isset($criteres['User'][$critereUser]) && !empty($criteres['User'][$critereUser])) {
                $conditions[] = 'User.' . $critereUser . ' ILIKE \'' . $this->wildcard('%' . $criteres['User'][$critereUser] . '%') . '\'';
                //$conditions[] = 'CONVERT("User.'.$critereUser.'" ,"utf8","utf8_general_ci")  ILIKE \''.$this->wildcard( '%'.$criteres['User'][$critereUser].'%' ).'\'';
            }
        }

        if (isset($criteres['User']['active'])) {
            $conditions[] = array('User.active' => $criteres['User']['active']);
        }

        $query = array(
            'joins' => array(
                $this->join('Desktop', array('type' => 'INNER')),
                $this->Desktop->join('Profil', array('type' => 'INNER'))
            ),
//            'limit' => 10,
            'fields' => array(
                'DISTINCT User.id', //FIXME
                'User.username',
                'User.desktop_id',
                'User.nom',
                'User.prenom',
                'User.created',
                'User.active',
                'Desktop.id',
                'Desktop.name',
                'Profil.id',
                'Profil.name'
            ),
            'order' => 'User.username',
            'conditions' => array(
                $conditions
            ),
            'contain' => false
        );
//        debug($criteres);
        // Filtre sur les services
        if (isset($criteres['User']['service_id']) && !empty($criteres['User']['service_id'])) {
            $query['joins'][] = $this->Desktop->join('DesktopsUser', array('type' => 'LEFT OUTER'));
            $query['joins'][] = $this->Desktop->join('DesktopsService', array('type' => 'LEFT OUTER'));

            // On cherche la liste des services associés aux différents profils
            // On retourne la liste des profils associés à l'utilisateur pour le service sélectionné
            $sqDesktopsService = $this->Desktop->DesktopsService->sq(
                    array(
                        'fields' => array('desktops_services.desktop_id'),
                        'alias' => 'desktops_services',
                        'conditions' => array(
                            'desktops_services.service_id' => $criteres['User']['service_id']
                        ),
                        'contain' => false
                    )
            );
            $conditionsUser[] = array("desktops_users.desktop_id IN ( {$sqDesktopsService} )");

            // Parmi la liste des profils récupérés pour le service sélectionné,
            // on retourne les utilisateurs possédant un des profils parmi ceux précédemment cherchés
            $sqDesktopsUser = $this->Desktop->DesktopsUser->sq(
                    array(
                        'fields' => array('desktops_users.user_id'),
                        'alias' => 'desktops_users',
                        'conditions' => array(
                            $conditionsUser
                        ),
                        'contain' => false
                    )
            );

            $conditionsNewUser[] = array("User.desktop_id IN ( {$sqDesktopsService} )");
            $sqDesktopIdFromUser = $this->sq(
                    array(
                        'fields' => array('users.desktop_id'),
                        'alias' => 'users',
                        'conditions' => array(
                            $conditionsNewUser
                        ),
                        'contain' => false
                    )
            );

            $conditionsServices[] = array("User.id IN ( {$sqDesktopsUser} ) OR User.desktop_id IN ( {$sqDesktopIdFromUser} )");

            $query['conditions'][] = array_merge(
                    $conditions, $conditionsServices
//                array(
//                    'DesktopsService.service_id' => $criteres['User']['service_id'],
//                )
            );
        }


        // Filtre sur les profils
        if (isset($criteres['User']['profil_id']) && !empty($criteres['User']['profil_id'])) {

            $secDesk = $this->Desktop->find(
                    'all', array(
                'conditions' => array(
                    'Desktop.profil_id' => $criteres['User']['profil_id']
                ),
                'contain' => false
                    )
            );
            $secDesktopsIds = Hash::extract($secDesk, '{n}.Desktop.id');

//            if ($criteres['User']['profil_id'] != DISP_GID) {
                if (empty($criteres['User']['service_id'])) {
                    $query['joins'][] = array_merge(
                            $this->join('DesktopsUser')
                    );
                }


                $query['conditions'][] = array(
                    'AND' => array_merge(
                            array(
                                'OR' => array(
                                    'Desktop.profil_id' => $criteres['User']['profil_id'],
                                    'DesktopsUser.desktop_id' => $secDesktopsIds
                                )
                            )
                    )
                );

//                $query['conditions'][] = array_merge(
//                        $conditions, array(
//                    'OR' => array(
//                        'Desktop.profil_id' => $criteres['User']['profil_id'],
//                        'DesktopsUser.desktop_id' => $secDesktopsIds
//                    )
//                        )
//                );
//            } else {
//                $query['conditions'][] = array(
//                    'AND' => array_merge(
//                            $conditions, array(
//                        'Desktop.profil_id' => $criteres['User']['profil_id']
//                            )
//                    )
//                );
//            }
        }

        return $query;
    }

    /**
     * Envoi une notification par mail à un utilisateur sur l'état d'un flux
     *
     * @param integer $courrier_id identifiant du flux
     * @param integer $user_id identifiant de l'utilisateur à notifier
     * @param string $type notification à envoyer
     * @return bool succès de l'envoi
     */
    public function notifier($courrier_id, $user_id, $notifId, $type, $desktopData = array()) {

        $user = $this->find('first', array(
            'recursive' => -1,
            'conditions' => array('User.id' => $user_id)
        ));

        // utilisateur existe et accepte les mails ?
        if (empty($user) || empty($user['User']['accept_notif']) || empty($user['User']['mail']) || empty($user['User']["mail_$type"])) {
            return false;
        }


        App::uses('CakeEmail', 'Network/Email');
        $config_mail = 'default';
        $this->Email = new CakeEmail($config_mail);
        $this->Email->to($user['User']['mail']);
		$this->Email->emailFormat(CakeEmail::MESSAGE_HTML);

        if (!empty($courrier_id)) {
            App::uses('Courrier', 'Model');
            $this->Courrier = new Courrier();
            $flux = $this->Courrier->find(
                    'first', array(
                'recursive' => -1,
                'conditions' => array('Courrier.id' => $courrier_id),
                'contain' => false
                    )
            );


            if (!empty($flux['Courrier']['affairesuiviepar_id'])) {
                $affairesuiviepar = $this->Desktop->find(
                        'first', array(
                    'conditions' => array(
                        'Desktop.id' => $flux['Courrier']['affairesuiviepar_id']
                    ),
                    'contain' => false,
                    'recursive' => -1
                        )
                );
                $user['User']['affairesuiviepar'] = $affairesuiviepar['Desktop']['name'];
            }


            App::uses('DataAclNode', 'DataAcl.Model');
            ClassRegistry::init(('Soustype'));
            $Soustype = new Soustype();
            $Soustype->setDataSource($this->useDbConfig);

            $soustype = $Soustype->find(
                    'first', array(
                'fields' => array('Soustype.circuit_id'),
                'recursive' => -1,
                'conditions' => array('Soustype.id' => $flux['Courrier']['soustype_id']),
                'contain' => false
                    )
            );
            if (!empty($soustype)) {
                $flux = Hash::merge($flux, $soustype);
            }

            App::uses('SCComment', 'Model');
            $this->SCComment = new SCComment();
            $comment = $this->SCComment->find(
                    'first', array(
                'fields' => array('SCComment.objet'),
                'contain' => false,
                'conditions' => array(
                    'SCComment.target_id' => $courrier_id
                )
                    )
            );
            if (!empty($comment)) {
                $flux = Hash::merge($flux, $comment);
            }
        }

        if (!empty($desktopData)) {
            if (!empty($desktopData['Plandelegation'])) {
                $desktop = $this->Desktop->find('first', array(
                    'recursive' => -1,
                    'conditions' => array('Desktop.id' => $desktopData['Plandelegation']['desktop_id'])
                ));

                $desktopData['Plandelegation']['name'] = $desktop['Desktop']['name'];
            } else if (!empty($desktopData['DesktopsUser'])) {
                $desktop = $this->Desktop->find('first', array(
                    'recursive' => -1,
                    'conditions' => array('Desktop.id' => $desktopData['DesktopsUser']['desktop_id'])
                ));
                $desktopData['DesktopsUser']['name'] = $desktop['Desktop']['name'];
            }
            $flux = array();
        } else {
            $desktop = array();
        }

        $this->Templatenotification = ClassRegistry::init('Templatenotification');
        $templatenotification = $this->Templatenotification->find(
                'first', array(
            'conditions' => array(
                'Templatenotification.name' => $type
            ),
            'contain' => false
                )
        );

        // Utilisation le paramtrage dans templatenotifications
        if (!empty($templatenotification)) {
            $subject = $templatenotification['Templatenotification']['subject'];
        }
        // sinon on fait normalement
        else {
            switch ($type) {
                case 'insertion':
                    $subject = "Vous allez recevoir le flux : " . $flux['Courrier']['name'];
                    break;
                case 'traitement':
                    $subject = "Vous avez le flux : " . $flux['Courrier']['name'] . " à traiter";
                    break;
                case 'refus':
                    $subject = "Le flux : " . $flux['Courrier']['name'] . " a été refusé";
                    break;
                case 'retard_validation':
                    $subject = "Retard sur le flux : " . $flux['Courrier']['name'];
                    break;
                case 'copy':
                    $subject = "Réception d'un flux en copie : " . $flux['Courrier']['name'];
                    break;
                case 'insertion_reponse':
                    $subject = "Réception d'un flux réponse : " . $flux['Courrier']['name'];
                    break;
                case 'aiguillage':
                    $subject = "Un flux vient de vous être aiguillé : " . $flux['Courrier']['name'];
                    break;
                case 'delegation':
                    $subject = "Un bureau vient de vous être délégué : " . $desktop['Desktop']['name'];
                    break;
				case 'bloque_parapheur':
                    $subject = "Un flux est présent dans le i-Parapheur depuis + d'1 semaine : " . $flux['Courrier']['reference'];
                    break;
                case 'clos':
                    $subject = "Le flux " . $flux['Courrier']['reference'] . "que vous avez inséré dans un circuit vient d'être clos";
                    break;
            }
        }


        $this->Email->subject($subject);
        $content = $this->_paramMails($type, $flux, $user['User'], $desktopData);
        $this->Email->send($content);

        return true;
    }

    /**
     * Détermine le contenu du mail à envoyer en fonction du type de mail, le flux et l'utilisateur
     * @param string $type
     * @param array $flux
     * @param array $user
     * @return string
     */
    function _paramMails($type, $flux, $user, $desktop) {

        $this->Templatenotification = ClassRegistry::init('Templatenotification');
        $templatenotification = $this->Templatenotification->find(
                'first', array(
            'conditions' => array(
                'Templatenotification.name' => $type
            ),
            'contain' => false
                )
        );

        if (!empty($templatenotification)) {
            $content = nl2br( $templatenotification['Templatenotification']['object'] );
        } else {
            $file = new File(APP . "/Config/emails/$type.txt", false);
            $content = $file->read();
            $file->close();
        }

        if (!empty($flux)) {
            if(Configure::read('Conf.SAERP') ) {
                $addrTraiter = Configure::read('WEBGFC_URL') . '/courriers/historiqueGlobal/' . $flux['Courrier']['id'];
                $addrView = Configure::read('WEBGFC_URL') . '/courriers/historiqueGlobal/' . $flux['Courrier']['id'];
                $addrEdit = Configure::read('WEBGFC_URL') . '/courriers/historiqueGlobal/' . $flux['Courrier']['id'] ;
            }
            else {
                $addrTraiter = Configure::read('WEBGFC_URL') . '/courriers/view/' . $flux['Courrier']['id'] . '/' . $user['desktop_id'];
                $addrView = Configure::read('WEBGFC_URL') . '/courriers/view/' . $flux['Courrier']['id'] . '/' . $user['desktop_id'];
                $addrEdit = Configure::read('WEBGFC_URL') . '/courriers/view/' . $flux['Courrier']['id'] . '/' . $user['desktop_id'];
            }

            $searchReplace = array(
                "#NOM#" => $user['nom'],
                "#PRENOM#" => $user['prenom'],
                "#IDENTIFIANT_FLUX#" => $flux['Courrier']['id'],
                "#NOM_FLUX#" => $flux['Courrier']['name'],
                "#OBJET_FLUX#" => $flux['Courrier']['objet'],
                "#COMMENTAIRE_FLUX#" => isset($flux['SCComment']['content']) ? $flux['SCComment']['content'] : null,
                "#LIBELLE_CIRCUIT#" => isset($flux['Soustype']['circuit_id']) ? $this->Courrier->Traitement->Circuit->getLibelle($flux['Soustype']['circuit_id']) : null,
                "#ADRESSE_A_TRAITER#" => $addrTraiter,
                "#ADRESSE_A_VISUALISER#" => $addrView,
                "#ADRESSE_A_MODIFIER#" => $addrEdit,
                "#FLUX_ORIGINE#" => isset($flux['Courrier']['parent_id']) ? $flux['Courrier']['parent_id'] : null,
                "#REFERENCE_FLUX#" => isset($flux['Courrier']['reference']) ? $flux['Courrier']['reference'] : null,
                "#DATERECEPTION_FLUX#" => isset($flux['Courrier']['datereception']) ? date('d/m/Y', strtotime($flux['Courrier']['datereception'])) : null,
                "#DATECREATION_FLUX#" => isset($flux['Courrier']['date']) ? date('d/m/Y', strtotime($flux['Courrier']['date'])) : null,
                "#AFFAIRESUIVIE_PAR#" => isset($user['affairesuiviepar']) ? $user['affairesuiviepar'] : null,
                "#MOTIF_REFUS#" => isset($flux['Courrier']['motifrefus']) ? $flux['Courrier']['motifrefus'] : null
            );
        }
        // Partie pour les mails de délégation
        else if (!empty($desktop)) {

            if (!empty($desktop['Plandelegation'])) {
                $searchReplace = array(
                    "#NOM#" => $user['nom'],
                    "#PRENOM#" => $user['prenom'],
                    "#NOM_BUREAU#" => $desktop['Plandelegation']['name'],
                    "#DEBUT_DELEGATION#" => isset($desktop['Plandelegation']['date_start']) ? $desktop['Plandelegation']['date_start'] : null,
                    "#FIN_DELEGATION#" => isset($desktop['Plandelegation']['date_end']) ? $desktop['Plandelegation']['date_end'] : null
                );
            } else {
                $searchReplace = array(
                    "#NOM#" => $user['nom'],
                    "#PRENOM#" => $user['prenom'],
                    "#NOM_BUREAU#" => $desktop['DesktopsUser']['name'],
                    "#DEBUT_DELEGATION#" => null,
                    "#FIN_DELEGATION#" => null
                );
            }
        }
        return str_replace(array_keys($searchReplace), array_values($searchReplace), $content);
    }

    /**
     * Fonction retournant l'ID de l'utilsiateur selon les IDs de bureaux passés en paramètre
     * Utilisée pour les notifications par mail
     * @param type $desktopsIds
     * @return integer
     */
    public function userIdByDesktop($desktopId, $name = false) {
        if (empty($desktopId)) {
            return false;
        }

        $user = $this->find(
        	'first',
			array(
				'recursive' => -1,
				'conditions' => array(
					'OR' => array(
						'User.desktop_id' => $desktopId,
						'SecondaryDesktop.id' => $desktopId,
					),
					'User.active' => true,
					'Desktop.active' => true
				),
				'joins' => array(
					$this->join('Desktop', array('type' => 'INNER')),
					$this->Desktop->join('Profil', array('type' => 'INNER')),
					$this->join('DesktopsUser', array('type' => 'LEFT OUTER')),
					alias_querydata(
							$this->DesktopsUser->join('Desktop', array('type' => 'LEFT OUTER')), array('Desktop' => 'SecondaryDesktop')
					),
					alias_querydata(
							$this->DesktopsUser->Desktop->join('Profil', array('type' => 'LEFT OUTER')), array('Desktop' => 'SecondaryDesktop', 'Profil' => 'SecondaryProfil')
					)
				),
				'contain' => false
			)
        );

		$user_id = '';
        if( !empty($user) ) {
			$user_id = $user['User']['id'];
			if($name) {
				$user_id = $user['User']['prenom'].' '.$user['User']['nom'];
			}
		}
        else if( $name && $desktopId == '-1') {
			$user_id = 'Parapheur';
		}
        return $user_id;
    }

    /**
     * Fonction permettant de retrouner l'ensemble des mails des users enregistrés en BDD
     * @return array
     */
    public function getUserMails() {

        $emails = array();

        $mails = $this->find(
                'all', array(
            'fields' => array('User.mail'),
            'conditions' => array(
                'User.active' => true
            ),
            'recursive' => -1,
            'order' => 'User.mail ASC'
                )
        );
        foreach ($mails as $i => $mail) {
            $emails[$mail['User']['mail']] = $mail['User']['mail'];
        }
//debug($emails);

        return $emails;
    }

    /**
     * Envoi une notification par mail à un utilisateur sur l'état d'un flux
     *
     * @param integer $courrier_id identifiant du flux
     * @param integer $user_id identifiant de l'utilisateur à notifier
     * @param string $type notification à envoyer
     * @return bool succès de l'envoi
     */
    public function saveNotif($courrier_id, $user_id, $notificationId, $type, $desktopData = array()) {
        $user = $this->find('first', array(
            'recursive' => -1,
            'conditions' => array('User.id' => $user_id)
        ));
        // utilisateur existe et accepte les mails ?
        if (empty($user) || empty($user['User']['accept_notif']) || empty($user['User']['mail']) || empty($user['User']["mail_$type"])
        )
            return false;
//debug($user);
//        App::uses('CakeEmail', 'Network/Email');
//
//        $this->Email = new CakeEmail($config_mail);
//        $this->Email->to($user['User']['mail']);
        if (!empty($courrier_id)) {
            App::uses('Courrier', 'Model');
            $this->Courrier = new Courrier();
            $flux = $this->Courrier->find(
                    'first', array(
                'recursive' => -1,
                'conditions' => array('Courrier.id' => $courrier_id),
                'contain' => false
                    )
            );
        }

        if (!empty($notificationId)) {
            $notification = $this->Notifquotidien->Notification->find('first', array(
                'recursive' => -1,
                'conditions' => array('Notification.id' => $notificationId)
            ));
        } else {
            if ($type == 'retard_validation') {
                $notification['Notification']['name'] = 'Flux en retard';
            } else if ($type == 'aiguillage') {
                $notification['Notification']['name'] = 'Flux à aiguiller';
            }
            else {
                $notification['Notification']['name'] = 'Flux à traiter';
            }
        }

        if (!empty($desktopData)) {
            if (!empty($desktopData['Plandelegation'])) {
                $desktop = $this->Desktop->find('first', array(
                    'recursive' => -1,
                    'conditions' => array('Desktop.id' => $desktopData['Plandelegation']['desktop_id'])
                ));

                $desktopData['Plandelegation']['name'] = $desktop['Desktop']['name'];
            } else if (!empty($desktopData['DesktopsUser'])) {
                $desktop = $this->Desktop->find('first', array(
                    'recursive' => -1,
                    'conditions' => array('Desktop.id' => $desktopData['DesktopsUser']['desktop_id'])
                ));
                $desktopData['DesktopsUser']['name'] = $desktop['Desktop']['name'];
            }
            $flux = array();
        } else {
            $desktop = array();
        }

        switch ($type) {
            case 'insertion':
                $subject = "Vous allez recevoir le flux : " . $flux['Courrier']['name'];
                break;
            case 'traitement':
                $subject = "Vous avez le flux : " . $flux['Courrier']['name'] . " à traiter";
                break;
            case 'refus':
                $subject = "Le flux : " . $flux['Courrier']['name'] . " a été refusé";
                break;
            case 'retard_validation':
                $subject = "Retard sur le flux : " . $flux['Courrier']['name'];
                break;
            case 'copy':
                $subject = "Réception d'un flux en copie : " . $flux['Courrier']['name'];
                break;
            case 'insertion_reponse':
                $subject = "Réception d'un flux réponse : " . $flux['Courrier']['name'];
                break;
            case 'aiguillage':
                $subject = "Un flux vient de vous être aiguillé : " . $flux['Courrier']['name'];
                break;
            case 'delegation':
                $subject = "Un bureau vient de vous être délégué : " . $desktop['Desktop']['name'];
                break;
        }

        $this->Notifquotidien->begin();
        $this->Notifquotidien->create();
        $data = array(
            'Notifquotidien' => array(
                'notification_id' => !empty($notificationId) ? $notificationId : null,
                'desktop_id' => @$desktop['Desktop']['id'],
                'name' => !empty( $notification['Notification']['name'] ) ? $notification['Notification']['name'] : $subject,
                'user_id' => $user_id,
                'courrier_id' => $courrier_id
            )
        );
        $saved = $this->Notifquotidien->save($data);
        if ($saved) {
            $this->Notifquotidien->commit();
        } else {
            $this->Notifquotidien->rollback();
        }
        return true;
    }

    /**
     * Fonction permettant de construire le mail envoyé quotidiennement
     * @param type $data
     */
    public function sendNotifQuotidien($user, $data = array()) {


        $userName = $user['User']['prenom'] . ' ' . $user['User']['prenom'];
//debug($userName);
        $this->Templatenotification = ClassRegistry::init('Templatenotification');
        $templatenotification = $this->Templatenotification->find(
                'first', array(
            'conditions' => array(
                'Templatenotification.name' => 'quotidien'
            ),
            'contain' => false
                )
        );
        App::uses('CakeEmail', 'Network/Email');
        $config_mail = 'default';
        $this->Email = new CakeEmail($config_mail);
        $this->Email->to($user['User']['mail']);
		$this->Email->emailFormat(CakeEmail::MESSAGE_HTML);
        // Utilisation le paramtrage dans templatenotifications
        if (!empty($templatenotification)) {
            $subject = $templatenotification['Templatenotification']['subject'];
        } else {
            $subject = "[web-GFC] Récapitulatif journalier pour l'agent : " . $userName;
        }
        $this->Email->subject($subject);
        $content = $this->_paramMailsQuot('quotidien', $user, $data);
//debug($content);
//die();
        $this->Email->send($content);
        return true;
    }

    /**
     * Détermine le contenu du mail à envoyer en fonction du type de mail, le flux et l'utilisateur
     * @param string $type
     * @param array $flux
     * @param array $user
     * @return string
     */
    function _paramMailsQuot($type, $user, $data) {
//$this->log($user);

        $this->Templatenotification = ClassRegistry::init('Templatenotification');
        $templatenotification = $this->Templatenotification->find(
                'first', array(
            'conditions' => array(
                'Templatenotification.name' => $type
            ),
            'contain' => false
                )
        );

        if (!empty($templatenotification)) {
			$content = nl2br( $templatenotification['Templatenotification']['object'] );
        } else {
            $file = new File(APP . "/Config/emails/$type.txt", false);
            $content = $file->read();
            $file->close();
        }
//debug($data);
        if (!empty($data)) {
            $refByTypeNotif = array();
            foreach ($data as $userId => $typeNotif) {
                foreach ($typeNotif as $i => $courrier) {
                    foreach ($courrier as $info) {
                        $numRef = $info['Courrier']['reference'];
                        $objet = $info['Courrier']['name'];
                        $name = $info['Notifquotidien']['name'];

                        $courrierid = $info['Courrier']['id'];
                        $addrView = Configure::read('WEBGFC_URL') . '/courriers/view/' . $courrierid . '/' . $user['User']['desktop_id'];

                        $infoFlux[$userId][$name][] = $numRef . ' : ' . $objet . ' - ( ' . $addrView . ' )';
                    }
                }
            }
            if( !empty($infoFlux)) {
                foreach ($infoFlux[$user['User']['id']] as $nomNotif => $flux) {
                    $refByTypeNotif[] = "\n" . $nomNotif . "\n" . ' ' . implode("\n", $flux);
                }
            }

            $searchReplace = array(
                "#NOM#" => $user['User']['nom'],
                "#PRENOM#" => $user['User']['prenom'],
//                "#REFERENCE_FLUX#" => implode( "\n\n", $refByTypeNotif ),
                "#REFERENCE_FLUX#" => implode("\n", $refByTypeNotif),
                "#DATERECEPTION_FLUX#" => date('d/m/Y', strtotime(date('Y-m-d'))),
                "#ADRESSE_A_VISUALISER#" => $addrView
            );
        }
        // Partie pour les mails de délégation
        /*else if (!empty($desktop)) {
            if (!empty($desktop['Plandelegation'])) {
                $searchReplace = array(
                    "#NOM#" => $user['nom'],
                    "#PRENOM#" => $user['prenom'],
                    "#NOM_BUREAU#" => $desktop['Plandelegation']['name'],
                    "#DEBUT_DELEGATION#" => isset($desktop['Plandelegation']['date_start']) ? date('d/m/Y', strtotime($desktop['Plandelegation']['date_start'])) : null,
                    "#FIN_DELEGATION#" => isset($desktop['Plandelegation']['date_end']) ? date('d/m/Y', strtotime($desktop['Plandelegation']['date_end'])) : null
                );
            } else {
                $searchReplace = array(
                    "#NOM#" => $user['nom'],
                    "#PRENOM#" => $user['prenom'],
                    "#NOM_BUREAU#" => $desktop['DesktopsUser']['name'],
                    "#DEBUT_DELEGATION#" => isset($desktop['DesktopsUser']['date_start']) ? date('d/m/Y', strtotime($desktop['DesktopsUser']['date_start'])) : null,
                    "#FIN_DELEGATION#" => isset($desktop['DesktopsUser']['date_end']) ? date('d/m/Y', strtotime($desktop['DesktopsUser']['date_end'])) : null
                );
            }
        }*/
        return str_replace(array_keys($searchReplace), array_values($searchReplace), $content);
    }

    public function getNbUsers() {
        return $this->find('count', array('conditions' => array('User.active' => true)));
    }



    /**
     *  Sauvegarde des comptes Users synchronisés depuis l'annuiare LDAP
     * @param type $data
     * @return boolean
     */

    public function saveLdapManagerLdap($data) {
//$this->log($data);
//die();
//        if (!isset($data['User']['civilite'])) {
//            $data['User']['civilite'] = 'Monsieur';
//        }
//        if (!isset($data['User']['nom'])) {
//            $data['User']['nom'] = 'Doe';
//        }
//        if (!isset($data['User']['prenom'])) {
//            $data['User']['prenom'] = 'John';
//        }
//        if (!isset($data['User']['mail'])) {
//            $data['User']['mail'] = $data['User']['prenom'].'@test.fr';
//        }

        if (!isset($data['User']['password'])) {
			$password = bin2hex(random_bytes(15));
            $data['User']['password'] = Security::hash($password, 'sha256', true);
        }


		$saved = true;
        if( !empty($data) && !empty($data['User']['nom']) && !empty($data['User']['prenom']) ) {
            $this->begin();
            $this->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $userAlreadyExist = $this->find(
                'first',
                array(
                    'conditions' => array(
                    	'OR' => array(
                    		array(
								'User.username' => $data['User']['username']
							),
							array(
								'User.nom' => $data['User']['nom'],
								'User.prenom' => $data['User']['prenom'],
								'User.mail' => @$data['User']['mail']
							)
						)
                    ),
                    'recursive' => -1
                )
            );
            // Profil
            Configure::write('Acl.database', Configure::read('LdapManager.Ldap.conn'));
            Configure::write('DataAcl.database', Configure::read('LdapManager.Ldap.conn'));
            $this->Desktop = ClassRegistry::init('Desktop');
            $this->Aro = ClassRegistry::init('Aro');
            $this->Service = ClassRegistry::init('Service');
            $this->DesktopsService = ClassRegistry::init('DesktopsService');
            $this->Desktopmanager = ClassRegistry::init('Desktopmanager');
            $this->DesktopDesktopmanager = ClassRegistry::init('DesktopDesktopmanager');
            $this->DesktopsUser = ClassRegistry::init('DesktopsUser');
            $this->Desktop->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->Aro->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->Service->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->DesktopsService->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->Desktopmanager->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->DesktopDesktopmanager->setDataSource(Configure::read('LdapManager.Ldap.conn'));
            $this->DesktopsUser->setDataSource(Configure::read('LdapManager.Ldap.conn'));

            if( empty( $userAlreadyExist ) ) {
				if($data['User']['profil_id'] == 3) {
					$desktopName = 'Initiateur';
				}
				else if($data['User']['profil_id'] == 4) {
					$desktopName = 'Valideur';
				}
				else if($data['User']['profil_id'] == 5) {
					$desktopName = 'Valideur Editeur';
				}
				else if( $data['User']['profil_id'] == 6) {
					$desktopName = 'Documentaliste';
				}
				else if( $data['User']['profil_id'] == 7) {
					$desktopName = 'Aiguilleur';
				}
				$desktopData['Desktop']['name'] = $desktopName.' '.$data['User']['prenom'].' '.$data['User']['nom'];
				$desktopData['Desktop']['profil_id'] = $data['User']['profil_id'];

				/// on crée le rôle associé
				$this->Desktop->create();
				$desktopAlreadyExist = $this->Desktop->find('first', array('conditions' => array(  'Desktop.name' => $desktopData['Desktop']['name']), 'contain' => false) );
				if( empty($desktopAlreadyExist) ) {
					$saved = $this->Desktop->save($desktopData) && $saved;
					$desktopId = $this->Desktop->id;
				}
				else {
					$desktopId = $desktopAlreadyExist['Desktop']['id'];
				}

				// on crée le bureau associé au rôle
				$this->Desktopmanager->create();
				$desktopmanagerData['Desktopmanager']['name'] = 'Bureau '.$desktopData['Desktop']['name'];
				$desktopmanagerData['Desktopmanager']['isautocreated'] = true;
				$desktopmanagerAlreadyExist = $this->Desktopmanager->find('first', array('conditions' => array(  'Desktopmanager.name' => $desktopmanagerData['Desktopmanager']['name']), 'contain' => false) );
				if( empty($desktopmanagerAlreadyExist) ) {
					$saved = $this->Desktopmanager->save($desktopmanagerData) && $saved;
					$desktopmanagerId = $this->Desktopmanager->id;
				}
				else {
					$desktopmanagerId = $desktopmanagerAlreadyExist['Desktopmanager']['id'];
				}

				// on crée l'association entre le bureau et le rôle
				$this->DesktopDesktopmanager->create();
				$desktopDesktopmanagerData['DesktopDesktopmanager']['desktop_id'] = $desktopId;
				$desktopDesktopmanagerData['DesktopDesktopmanager']['desktopmanager_id'] = $desktopmanagerId;
				$desktopDesktopmanagerAlreadyExist = $this->DesktopDesktopmanager->find('first', array('conditions' => array(  'DesktopDesktopmanager.desktop_id' => $desktopId, 'DesktopDesktopmanager.desktopmanager_id' => $desktopmanagerId), 'contain' => false) );
				if( empty($desktopDesktopmanagerAlreadyExist) ) {
					$saved = $this->DesktopDesktopmanager->save($desktopDesktopmanagerData) && $saved;
				}


				// on associz le profil au service principal
				$service = $this->Service->find('first', array('conditions' => array('Service.parent_id IS NULL'), 'contain' => false, 'order' => 'id ASC'));
				$this->DesktopsService->create();
				$desktopServiceData['DesktopsService']['desktop_id'] = $desktopId;
				$desktopServiceData['DesktopsService']['service_id'] = $service['Service']['id'];
				$saved = $this->DesktopsService->save($desktopServiceData) && $saved;

				// User
				$this->create();
				$username = $data['User']['username'];
				$userDATA['User'] = $data['User'];
				$userDATA['User']['username'] = $username;
				$userDATA['User']['password'] = Security::hash($username, 'sha256', true);
				$userDATA['User']['desktop_id'] = $desktopId;
				$saved = $this->save($userDATA) && $saved;
				if( $saved ) {
					$this->commit();
				}
				else {
					$this->rollback();
				}
            }
            else if( !empty($userAlreadyExist) ) {
                $desktopUserAlreadyExist = $this->Desktop->find(
                    'first',
                    array(
                        'conditions' => array(
                            'Desktop.id' => $userAlreadyExist['User']['desktop_id']
                        ),
                        'recursive' => -1,
                        'contain'=> false
                    )
                );
                if( !empty($desktopUserAlreadyExist) ) {
                    if($data['User']['profil_id'] == 3) {
                        $newDesktopName = 'Initiateur';
                    }
                    else if($data['User']['profil_id'] == 4) {
                        $newDesktopName = 'Valideur';
                    }
                    else if($data['User']['profil_id'] == 5) {
                        $newDesktopName = 'Valideur Editeur';
                    }
                    else if( $data['User']['profil_id'] == 6) {
                        $newDesktopName = 'Documentaliste';
                    }
                    else if( $data['User']['profil_id'] == 7) {
                        $newDesktopName = 'Aiguilleur';
                    }
                    $newDesktopData['Desktop']['name'] = $newDesktopName.' '.$data['User']['prenom'].' '.$data['User']['nom'];
                    $newDesktopData['Desktop']['profil_id'] = $data['User']['profil_id'];

                    /// on crée le rôle associé
                    $this->Desktop->create();
                    $newDesktopAlreadyExist = $this->Desktop->find('first', array('conditions' => array(  'Desktop.name' => $newDesktopData['Desktop']['name']), 'contain' => false) );
                    if( empty($newDesktopAlreadyExist) ) {
                        $saved = $this->Desktop->save($newDesktopData) && $saved;
                        $newdesktopId = $this->Desktop->id;
                    }

                    /// on crée le second rôle associé
                    if( !empty( $newdesktopId ) ) {
                        $this->DesktopsUser->create();
                        $newDesktopsUserData['DesktopsUser']['desktop_id'] = $newdesktopId;
                        $newDesktopsUserData['DesktopsUser']['user_id'] = $userAlreadyExist['User']['id'];
                        $newDesktopsUserAlreadyExist = $this->DesktopsUser->find('first', array('conditions' => array(  'DesktopsUser.desktop_id' => $newdesktopId, 'DesktopsUser.user_id' => $userAlreadyExist['User']['id']), 'contain' => false) );
                        if( empty($newDesktopsUserAlreadyExist)     ) {
                            $saved = $this->DesktopsUser->save($newDesktopsUserData) && $saved;
                        }

                        // on crée le bureau associé au rôle
                        $this->Desktopmanager->create();
                        $newdesktopmanagerData['Desktopmanager']['name'] = 'Bureau '.$newDesktopData['Desktop']['name'];
                        $newdesktopmanagerAlreadyExist = $this->Desktopmanager->find('first', array('conditions' => array(  'Desktopmanager.name' => $newdesktopmanagerData['Desktopmanager']['name']), 'contain' => false) );
                        if( empty($newdesktopmanagerAlreadyExist) ) {
                            $saved = $this->Desktopmanager->save($newdesktopmanagerData) && $saved;
                            $newdesktopmanagerId = $this->Desktopmanager->id;
                        }
                        else {
							$newdesktopmanagerId = $newdesktopmanagerAlreadyExist['Desktopmanager']['id'];
						}

                        // on crée l'association entre le bureau et le rôle
                        $this->DesktopDesktopmanager->create();
                        $newdesktopDesktopmanagerData['DesktopDesktopmanager']['desktop_id'] = $newdesktopId;
                        $newdesktopDesktopmanagerData['DesktopDesktopmanager']['desktopmanager_id'] = $newdesktopmanagerId;
                        $newdesktopDesktopmanagerAlreadyExist = $this->DesktopDesktopmanager->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'DesktopDesktopmanager.desktop_id' => $newdesktopDesktopmanagerData['DesktopDesktopmanager']['desktop_id'],
                                    'DesktopDesktopmanager.desktopmanager_id' => $newdesktopDesktopmanagerData['DesktopDesktopmanager']['desktopmanager_id']
                                ),
                                'contain' => false
                            )
                        );
                        if( empty($newdesktopDesktopmanagerAlreadyExist) ) {
                            $saved = $this->DesktopDesktopmanager->save($newdesktopDesktopmanagerData) && $saved;
                        }

                        // on associz le profil au service principal
                        $newservice = $this->Service->find('first', array('conditions' => array('Service.parent_id IS NULL'), 'contain' => false, 'order' => 'id ASC'));
                        $this->DesktopsService->create();
                        $newdesktopServiceData['DesktopsService']['desktop_id'] = $newdesktopId;
                        $newdesktopServiceData['DesktopsService']['service_id'] = $newservice['Service']['id'];
                        $saved = $this->DesktopsService->save($newdesktopServiceData) && $saved;
                    }

                    if( $saved ) {
                        $this->commit();
                    }
                    else {
                        $this->rollback();
                    }
                }

                if( $data['User']['username'] != $userAlreadyExist['User']['username'] ) {
                    $userDATA['User'] = $data['User'];
                    $saved = $this->updateAll(
                        array( 'User.username' => "'".$data['User']['username']."'" ),
                        array(
                            'User.nom' => $data['User']['nom'],
                            'User.prenom' => $data['User']['prenom'],
                            'User.mail' => $data['User']['mail']
                        )
                    ) && $saved;
                    if( $saved ) {
                        $this->commit();
                    }
                    else {
						$saved = false;
                        $this->rollback();
                    }
                }
                else {
                    $saved = false;
                    $this->rollback();
                }
            }
            else {
                $saved = false;
                $this->rollback();
            }
        }
        return $saved;
    }

}

?>
