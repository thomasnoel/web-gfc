<?php

App::uses('AppModel', 'Model');

/**
 * Repertoire model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Repertoire extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Repertoire';

	/**
	 *
	 * @var type
	 */
	public $actsAs = array('Tree');

	/**
	 *
	 * @var type
	 */
	public $validate = array(
		'user_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'parent_id' => array(
			'rule' => 'checkParadox',
			'on' => 'update',
			'message' => "Une catégorie ne peut pas devenir sa propre fille !"
		)
	);

	/**
	 *
	 * @var type
	 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasAndBelongsToMany = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'joinTable' => 'courriers_repertoires',
			'foreignKey' => 'repertoire_id',
			'associationForeignKey' => 'courrier_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'CourrierRepertoire'
		)
	);

	/**
	 *
	 * @param type $data
	 * @return boolean
	 */
	public function checkParadox($data) {
		if (isset($this->data[$this->alias]['id'])) {
			return $data['parent_id'] != $this->data[$this->alias]['id'];
		}
		return true;
	}

}

?>
