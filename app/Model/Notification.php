<?php

App::uses('AppModel', 'Model');

/**
 * Notification model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Notification extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Notification';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false,
			),
		),
		'desktop_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
		'typenotif_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		)
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Desktop' => array(
			'className' => 'Desktop',
			'foreignKey' => 'desktop_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		),
		'Typenotif' => array(
			'className' => 'Typenotif',
			'foreignKey' => 'typenotif_id',
			'type' => 'LEFT',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

	/**
	 *
	 * @var type
	 */
	public $hasMany = array(
		'Notifieddesktop' => array(
			'className' => 'Notifieddesktop',
			'foreignKey' => 'notification_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
        'Notifquotidien' => array(
			'className' => 'Notifquotidien',
			'foreignKey' => 'notification_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

	/**
	 *
	 * @param type $typeNotif
	 * @return type
	 */
	public function setNotification($typeNotif) {
		return array(
			'name' => __d('notification', $typeNotif . '.name'),
			'description' => __d('notification', $typeNotif . '.description'),
			'typeNotif' => $this->Typenotif->codes[$typeNotif]
		);
	}

	/**
	 *
	 * @param type $notificationId
	 * @param type $userId
	 * @return boolean
	 */
	private function _checkNotificationMailToSend($notificationId, $userId) {
		$return = false;


		return $return;
	}

	/**
	 *
	 * @param type $notificationId
	 * @param type $userId
	 * @return boolean
	 */
	private function _sendNotificationByMail($notificationId, $userId) {
		$return = false;


		return $return;
	}

	/**
	 *
	 * @param type $name
	 * @param type $description
	 * @param type $type
	 * @param type $desktopId
	 * @param type $desktops
	 * @return boolean
	 */
	public function add($name, $description, /* $type */ $typeId, $desktopId, $desktops = array()) {
		$return = null;

        if( !empty($desktopId) || $desktopId != -1 ){

            //initialisation du tableau de verification
            $verif = array(
                'notification' => false,
                'Notifieddesktop' => false
            );

             // On n'enregistre pas de notif si le biureau est de type parapheur
            foreach($desktops as $dId => $liste) {
                if($dId == -1 || $dId == -3) {
					$verif = array(
						'notification' => true,
						'Notifieddesktop' => true
					);
                    return $verif;
                }
            }
            //creation de la notification
            $this->begin();
            $notification = array(
                'Notification' => array(
                    'name' => $name,
                    'description' => $description,
                    'typenotif_id' => $typeId,
                    'desktop_id' => $desktopId,
                    'desktops' => $desktops
                )
            );

            $this->create($notification);

            if ($this->save()) {
                $verif['notification'] = true;
            } else {
                $verif['notification'] = false;
            }
            $createdNotifications = $this->id;



            //association de la notification aux flux et desktops
            $notificationId = $this->id;
            $verifnotified = array();
            foreach ($desktops as $dktId => $flux) {
    //            $flux = (array)$flux;
                if( !empty($flux ) && is_array($flux)) {
                    foreach ($flux as $fluxId) {
                        if(!empty($fluxId)) {
                            $Notifieddesktop = array(
                                'Notifieddesktop' => array(
                                    'notification_id' => $notificationId,
                                    'courrier_id' => $fluxId,
                                    'desktop_id' => $dktId,
                                    'read' => false
                                )
                            );
                            $this->Notifieddesktop->create($Notifieddesktop);

                            if ($this->Notifieddesktop->save()) {
                                $verifnotified[] = true;
                            } else {
                                $verifnotified[] = false;
                            }
                        }
                    }
                    //envoi de la notification par mail
                    $usersToMail = $this->Notifieddesktop->Desktop->getUsers($dktId);
                    foreach ($usersToMail as $userId) {
                        if ($this->_checkNotificationMailToSend($notificationId, $userId)) {
                            $verif['mail'] = $this->_sendNotificationByMail($notificationId, $userId);
                        }
                    }
                }
            }

            $verif['Notifieddesktop'] = !in_array(false, $verifnotified, true);

            if (!in_array(false, $verif, true)) {
                $this->commit();
                $return = $createdNotifications;
            } else {
                $this->rollback();
            }
        }
		return $return;
	}

	/**
	 *
	 * @param type $notificationId
	 * @param type $desktopId
	 * @param type $courrierId
	 * @return type
	 * @throws NotFoundException
	 */
	public function setRead($notificationId, $desktopId, $courrierId = null) {
		$querydata = array(
			'Notifieddesktop.notification_id' => $notificationId,
			'Notifieddesktop.desktop_id' => $desktopId,
			'Notifieddesktop.read' => false
		);
		if (!empty($courrierId)) {
			$querydata['Notifieddesktop.courrier_id'] = $courrierId;
		}


		$notifieddesktop = $this->Notifieddesktop->find('first', $querydata);

		if (empty($notifieddesktop)) {
			throw new NotFoundException();
		}

		$notifieddesktop['Notifieddesktop']['read'] = true;


		$this->debugLog($notifieddesktop);

		$this->Notifieddesktop->create($notifieddesktop);
		$return = $this->Notifieddesktop->save();
		return !empty($return);
	}

	/**
	 *
	 * @param type $desktopId
	 * @param type $courrierId
	 * @return boolean
	 */
	public function setReadByDesktopCourrier($desktopId, $courrierId) {
		$return = false;
		$valid = array();
		$this->Notifieddesktop->begin();
		$querydata = array(
			'conditions' => array(
				'Notifieddesktop.courrier_id' => $courrierId,
				'Notifieddesktop.desktop_id' => $desktopId,
				'Notifieddesktop.read' => false
			),
            'recursive' => -1
		);
		$notifieddesktops = $this->Notifieddesktop->find('all', $querydata);

		foreach ($notifieddesktops as $notifieddesktop) {
			$notifieddesktop['Notifieddesktop']['read'] = true;
			$this->Notifieddesktop->create($notifieddesktop);

			if ($this->Notifieddesktop->save()) {
				$valid[] = true;
			} else {
				$valid[] = false;
			}
		}

		if (!in_array(false, $valid, true)) {
			$this->Notifieddesktop->commit();
			$return = true;
		} else {
			$this->Notifieddesktop->rollback();
		}
		return $return;
	}

	/**
	 *
	 * @param type $desktopId
	 * @return boolean
	 */
	public function setAllReadByDesktop($desktopId) {
		$verif = array();
		$return = false;

		$notifieddesktops = $this->Notifieddesktop->find('all', array('recursive' => -1, 'conditions' => array('Notifieddesktop.desktop_id' => $desktopId, 'Notifieddesktop.read' => false)));
		$this->Notifieddesktop->begin();
		foreach ($notifieddesktops as $notifieddesktop) {
			$notifieddesktop['Notifieddesktop']['read'] = true;
			$this->Notifieddesktop->create($notifieddesktop);
			$verif[] = $this->Notifieddesktop->save();
		}

		if (!in_array(false, $verif, true)) {
			$this->Notifieddesktop->commit();
			$return = true;
		} else {
			$this->Notifieddesktop->rollback();
		}

		return $return;
	}

	/**
	 *
	 * @param type $courrierId
	 * @return boolean
	 */
	public function setAllReadByCourrier($courrierId) {
		$verif = array();
		$return = false;

		$notifieddesktops = $this->Notifieddesktop->find('all', array('recursive' => -1, 'conditions' => array('Notifieddesktop.courrier_id' => $courrierId, 'Notifieddesktop.read' => false)));
		$this->Notifieddesktop->begin();
		foreach ($notifieddesktops as $notifieddesktop) {
			$notifieddesktop['Notifieddesktop']['read'] = true;
			$this->Notifieddesktop->create($notifieddesktop);
			$verif[] = $this->Notifieddesktop->save();
		}

		if (!in_array(false, $verif, true)) {
			$this->Notifieddesktop->commit();
			$return = true;
		} else {
			$this->Notifieddesktop->rollback();
		}

		return $return;
	}

}

?>
