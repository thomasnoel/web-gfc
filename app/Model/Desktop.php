<?php

App::uses('AppModel', 'Model');

/**
 * Desktop model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Desktop extends AppModel {

    public $name = 'Desktop';

    /**
     *
     * @var type
     */
    public $actsAs = array(
        'Acl' => array('type' => 'requester'),
        'SimpleComment.SCOwner',
        'SimpleComment.SCReader'
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
            array(
                'rule' => array('isunique')
            )
        ),
        'profil_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Profil' => array(
            'className' => 'Profil',
            'foreignKey' => 'profil_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Composition' => array(
            'className' => 'Cakeflow.Composition',
            'foreignKey' => 'trigger_id'
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Courrier' => array(
            'className' => 'Courrier',
            'foreignKey' => 'desktop_creator_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Notifieddesktop' => array(
            'className' => 'Notifieddesktop',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Bancontenu' => array(
            'className' => 'Bancontenu',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Sendmail' => array(
            'className' => 'Sendmail',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Scanemail' => array(
            'className' => 'Scanemail',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Plandelegation' => array(
            'className' => 'Plandelegation',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Notifquotidien' => array(
			'className' => 'Notifquotidien',
			'foreignKey' => 'desktop_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		),
        'Notification' => array(
            'className' => 'Notification',
            'foreignKey' => 'desktop_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );

    /**
     * hasAndBelongsToMany associations
     *
     * @var array
     */
    public $hasAndBelongsToMany = array(
        'Tache' => array(
            'className' => 'Tache',
            'joinTable' => 'desktops_taches',
            'foreignKey' => 'desktop_id',
            'associationForeignKey' => 'tache_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'DesktopTache'
        ),
        'Service' => array(
            'className' => 'Service',
            'joinTable' => 'desktops_services',
            'foreignKey' => 'desktop_id',
            'associationForeignKey' => 'service_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'DesktopsService'
        ),
        'Initservice' => array(
            'className' => 'Service',
            'joinTable' => 'inits_services',
            'foreignKey' => 'desktop_id',
            'associationForeignKey' => 'service_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'InitsService'
        ),
        'SecondaryUser' => array(
            'className' => 'User',
            'joinTable' => 'desktops_users',
            'foreignKey' => 'desktop_id',
            'associationForeignKey' => 'user_id',
            'unique' => true,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
            'deleteQuery' => '',
            'insertQuery' => '',
            'with' => 'DesktopsUser'
        ),
        'Recherche' => array(
            'className' => 'Recherche',
            'joinTable' => 'recherches_desktops',
            'foreignKey' => 'desktop_id',
            'associationForeignKey' => 'recherche_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'RechercheDesktop'
        ),
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'joinTable' => 'desktops_desktopsmanagers',
            'foreignKey' => 'desktop_id',
            'associationForeignKey' => 'desktopmanager_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'DesktopDesktopmanager'
        )
    );

    /**
     *
     * @return null
     */
    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['Desktop']['profil_id'])) {
            $profilId = $this->data['Desktop']['profil_id'];
        } else {
            $profilId = $this->field('profil_id');
        }
        if (!$profilId) {
            return null;
        } else {
            return array('Profil' => array('id' => $profilId));
        }
    }

    /**
     * CakePHP callback afterSave
     * Mise à jour de l'alias des aros et daros
     *
     * @access public
     * @param type $created
     * @return void
     */
    public function afterSave($created, $options = Array()) {
        parent::afterSave($created);

        if (!empty($this->data['Desktop']['Service'])) {
            $this->data['Service'] = $this->data['Desktop']['Service'];
        }

        //mise à jour des AROs
        $aro = $this->Aro->find('first', array('conditions' => array('model' => 'Desktop', 'foreign_key' => $this->id)));
        $aro['Aro']['alias'] = $this->field('name');
        $this->Aro->create($aro);
        $this->Aro->save();

        //mise à jour des DAROs
        if (!empty($this->data['Service'])) {
            foreach ($this->data['Service']['Service'] as $item) {
                $DesktopsService = $this->DesktopsService->find('first', array('conditions' => array('DesktopsService.desktop_id' => $this->id, 'DesktopsService.service_id' => $item)));

                if (empty($DesktopsService)) {
                    $newDesktopsService = array(
                        'DesktopsService' => array(
                            'service_id' => $item,
                            'desktop_id' => $this->id
                        )
                    );
                    $this->DesktopsService->create($newDesktopsService);
                    $DesktopsService = $this->DesktopsService->save();
                }


                if (!empty($DesktopsService)) {
                    //TODO: find a way to launch DataAclBehavior aftersave callback from DesktopsService
                    //          $this->DesktopsService->id = $DesktopsService['DesktopsService']['id'];
                    //          $trigger_result = $this->DesktopsService->Behaviors->trigger('afterSave', array(&$this->DesktopsService, true, array('validate' => true, 'fieldList' => array(), 'callbacks' => true)));

                    $parent = $this->DesktopsService->Daro->find('first', array('conditions' => array('model' => 'Service', 'foreign_key' => $item)));
                    $data = array(
                        'parent_id' => isset($parent['Daro']['id']) ? $parent['Daro']['id'] : null,
                        'model' => $this->DesktopsService->name,
                        'foreign_key' => $DesktopsService['DesktopsService']['id']
                    );
                    $data['alias'] = $data['model'] . '.' . $data['foreign_key'] . (isset($parent['Daro']['id']) ? '.' . $parent['Daro']['id'] : '');

                    $existingDaro = $this->DesktopsService->Daro->find('first', array('conditions' => Set::flatten(array('Daro' => $data))));

                    if (!empty($existingDaro)) {
                        $data['id'] = $existingDaro['Daro']['id'];
                    }
                    $this->DesktopsService->Daro->create();
                    $retval = $this->DesktopsService->Daro->save(array('Daro' => $data));
                }
            }
        }
    }

    /**
     *
     * @param type $id
     * @return null
     */
    public function getDaros($id = null) {
        if ($id != null) {
            $this->id = $id;
        }

        if (!$this->id) {
            return null;
        }

        $return = array();
        $DesktopsServices = $this->DesktopsService->find('all', array('conditions' => array('DesktopsService.desktop_id' => $this->id)/* , 'recursive' => -1 */));
        foreach ($DesktopsServices as $DesktopsService) {
            $this->DesktopsService->id = $DesktopsService['DesktopsService']['id'];
            $node = $this->DesktopsService->node();
            $return[] = $node[0];
        }
        return $return;
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function getServicesList($id = null) {
        $services = array();
        if ($id != null) {
            $results = $this->DesktopsService->find(
                    'all', array(
                'fields' => array(
                    'Service.id',
                    'Service.name',
                ),
                'contain' => false,
                'joins' => array(
                    $this->DesktopsService->join('Service', array('type' => 'INNER'))
                ),
                'conditions' => array(
                    'DesktopsService.desktop_id' => $id
                ),
                'order' => array('Service.name ASC')
                    )
            );

            $services = Hash::combine($results, '{n}.Service.id', '{n}.Service.name');
        }

        return $services;
    }

    /**
     *
     * @param type $id
     * @return type
     */
    public function inDeletable($id = null) {
        $res = array(
            'deletable' => false,
            'message' => 'Non supprimable'
        );
        $inTache = $this->DesktopTache->find('count', array(
            'conditions' => array(
                'desktop_id' => $id
            ),
            'contain' => array(
                'Tache' => array(
                    'conditions' => array(
                        'statut !=' => 2
                    )
                )
            )
        ));


        // On réupère l'ID du bureau pour savoir s'il est dans un circuit pour autoriser la suppression
        $desktopmanager = $this->getDesktopManager($id);
        $inCircuit = $this->Composition->find('count', array(
            'conditions' => array(
                'trigger_id' => $desktopmanager
            )
        ));


        if ($inTache == 0 && $inCircuit == 0) {
            $res['deletable'] = true;
            $res['message'] = 'Profil supprimable';
        } else if ($inTache != 0) {
            $res = array(
                'deletable' => false,
                'message' => 'Profil non supprimable car il est associé à une tâche en cours de traitement'
            );
        } else if ($inCircuit != 0) {
            $res = array(
                'deletable' => false,
                'message' => 'Profil non supprimable car il est présent dans un circuit en cours de traitement'
            );
        }
        return $res;
    }

    /**
     *
     * @param type $id
     * @return type *
     */
    public function getUsers($id = null) {
        $return = array();
        $desktop = $this->find('first', array('conditions' => array('Desktop.id' => $id), 'contain' => array($this->User->alias, $this->SecondaryUser->alias)));
        if (!empty($desktop)) {
            foreach ($desktop['User'] as $user) {
                if (!in_array($user['id'], $return)) {
                    $return[] = $user['id'];
                }
            }
            foreach ($desktop['SecondaryUser'] as $user) {
                if (!in_array($user['id'], $return)) {
                    $return[] = $user['id'];
                }
            }
        }
        return $return;
    }

    /**
     *
     * @return type
     */
    public function getDesktopsByUser($mode = 'circuit') {

        $querydata = array(
            'contain' => array(
                'Desktop' => array(
                    'Service'
                ),
                'SecondaryDesktop'
            ),
            'conditions' => array(
                'Desktop.active' => true,
                'User.active' => true
            )
        );

        if ($mode == 'rebond') {

            $querydata['conditions']['Desktop.id <>'] = CakeSession::read('Auth.User.Desktop.id');

            $arraySecDesktop = array();
            foreach (CakeSession::read('Auth.User.SecondaryDesktops') as $desktop) {
                $arraySecDesktop[] = array('Desktop.id <>' => $desktop['id']);
            }
            if (!empty($arraySecDesktop)) {
                $querydata['conditions'][] = array('AND' => $arraySecDesktop);
            }
        }

        $users = $this->User->find('all', $querydata);
        $return = array();
        foreach ($users as $user) {
            $optgroupUserName = $user['User']['username'] . ' (' . $user['User']['prenom'] . ' ' . $user['User']['nom'] . ')';
            if ($mode == 'circuit' || $mode == 'rebond') {
                if ($user['Desktop']['profil_id'] != ADMIN_GID && $user['Desktop']['profil_id'] != DISP_GID && $user['Desktop']['profil_id'] != INIT_GID) {
                    $return[$optgroupUserName][$user['Desktop']['id']] = $user['Desktop']['name'];
                }
            } else if ($mode == 'init') {
                if ($user['Desktop']['profil_id'] == INIT_GID || $mode == "all") {
                    $return[$optgroupUserName][$user['Desktop']['id']] = $user['Desktop']['name'];
                }
            } else if ($mode == "all") {
                if ($user['Desktop']['profil_id'] != ADMIN_GID) {
                    $return[$optgroupUserName][$user['Desktop']['id']] = $user['Desktop']['name'];
                }
            }
            foreach ($user['SecondaryDesktop'] as $desktop) {
                if ($mode == 'circuit' || $mode == 'rebond') {
                    if ($desktop['profil_id'] != ADMIN_GID && $desktop['profil_id'] != DISP_GID && $desktop['profil_id'] != INIT_GID) {
                        $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
                    }
                } else if ($mode == 'init') {
                    if ($desktop['profil_id'] == INIT_GID) {
                        $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
                    }
                } else if ($mode == "all" && $desktop['profil_id'] != ADMIN_GID) {
                    $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
                }
            }
        }

        return $return;
    }

    /**
     * Liste des utilisateurs ayant le profil Initiateur
     * et qui sont Initiateur principal de leur service
     * @return type
     */
    /*
      public function getInitsByService() {
      $services = $this->Initservice->find('all', array('contain' => array('Init')));
      $return = array();
      foreach ($services as $service) {
      if (!empty($service['Init'])) {
      foreach ($service['Init'] as $init) {
      if ($init['active'] && $init['profil_id'] == INIT_GID) {
      $return[$service['Initservice']['name'] . " - Initiateurs"][$init['id']] = $init['name'];
      }
      }
      }
      }
      return $return;
      }
     */
    public function getDispsByService() {
        $services = $this->Service->find('all', array('contain' => array('Desktop' => array('conditions' => array('Desktop.profil_id' => DISP_GID, 'Desktop.active' => 1)))));
        $return = array();
        foreach ($services as $service) {
            if (!empty($service['Desktop'])) {
                foreach ($service['Desktop'] as $desktop) {
                    $return[$service['Service']['name'] . " - Aiguilleurs"][$desktop['id']] = $desktop['name'];
                }
            }
        }
        return $return;
    }

    //Retourne la liste des bureaux aiguilleurs et initiateurs (utilisé pour l'aiguillage multiple)
    public function getValEditByService() {
        $services = $this->Service->find('all', array(
            'contain' => array(
                'Desktop' => array(
                    'conditions' => array(
                        'Desktop.profil_id' => VALEDIT_GID,
                        'Desktop.active' => 1
                    )
                )
            )
        ));
        $return = array();
        foreach ($services as $service) {
            if (!empty($service['Desktop'])) {
                foreach ($service['Desktop'] as $desktop) {
                    $return[$service['Service']['name']][$desktop['id']] = $desktop['name'];
                }
            }
        }
        return $return;
    }

    //Retourne la liste des bureaux aiguilleurs et initiateurs (utilisé pour l'aiguillage multiple)
    public function getInitsDispsByService() {
        $services = $this->Service->find(
                'all', array(
            'contain' => array(
                'Desktop' => array(
                    'conditions' => array(
                        'Desktop.profil_id' => array(DISP_GID, INIT_GID),
                        'Desktop.active' => 1
                    )
                )
            )
                )
        );
        $return = array();
        foreach ($services as $service) {
            if (!empty($service['Desktop'])) {
                foreach ($service['Desktop'] as $desktop) {
                    $return[$service['Service']['name']][$desktop['id']] = $desktop['name'];
                }
            }
        }
        return $return;
    }

    /**
     * Liste des utilisateurs par service
     * afin de créer une liste déroulante comprenant les services et leur liste d'utilisateurs liés.
     * (envoi pour copie multiple pendant le traitement d'un flux en circuit)
     * @return type
     */
    /* public function getDesktopsByService() {
      $services = $this->Service->find(
      'all',
      array(
      'contain' => array(
      'Desktop' => array(
      'conditions' => array(
      'Desktop.active' => 1
      )
      )
      )
      )
      );
      $return = array();
      foreach ($services as $service) {
      if (!empty($service['Desktop'])) {
      foreach ($service['Desktop'] as $desktop) {
      $return[$service['Service']['name']][$desktop['id']] = $desktop['name'];
      }
      }
      }
      return $return;
      } */

    /**
     * Liste des utilisateurs par service
     * afin de créer une liste déroulante comprenant les services et leur liste d'utilisateurs liés.
     * (envoi pour copie multiple pendant le traitement d'un flux en circuit)
     * @return type
     */
    public function getDesktopsByService($mode = 'circuit', $profils = []) {

        if( Configure::read('Conf.SAERP')) {
            if( $mode == 'all' ) {
				$profils = array( INIT_GID,VALEDIT_GID, VAL_GID, ARCH_GID, DISP_GID);
            }
            else {
				$profils = array( VALEDIT_GID, VAL_GID, ARCH_GID );
            }
            $conditions = array(
                'Desktop.active' => true,
                'Desktop.profil_id' => $profils
            );
        }
        else {
        	if( isset($profils) && !empty($profils) ) {
				$conditions = array(
					'Desktop.active' => true,
					'Desktop.profil_id' => $profils
				);
			}
        	else {
				$conditions = array(
					'Desktop.active' => true
				);
			}
        }
        $querydata = array(
			'contain' => array(
				'Desktop' => array(
                    'conditions' => $conditions
                )
			),
            'order' => array('Service.name ASC')

		);

        if ($mode == 'rebond') {

            $querydata['Desktop']['conditions']['Desktop.id <>'] = CakeSession::read('Auth.User.Desktop.id');

            $arraySecDesktop = array();
            foreach (CakeSession::read('Auth.User.SecondaryDesktops') as $desktop) {
                $arraySecDesktop[] = array('Desktop.id <>' => $desktop['id']);
            }
            if (!empty($arraySecDesktop)) {
                $querydata['Desktop']['conditions'][] = array('AND' => $arraySecDesktop);
            }
        }
        $services = $this->Service->find('all', $querydata);

        $return = array();
        foreach ($services as $service) {
            if (!empty($service['Desktop'])) {
                foreach ($service['Desktop'] as $desktop) {
                    $optgroupUserName = $service['Service']['name'];
                    if ($mode == 'circuit' || $mode == 'rebond') {
                        if ($desktop['profil_id'] != ADMIN_GID && $desktop['profil_id'] != DISP_GID && $desktop['profil_id'] != INIT_GID) {
                            $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
                        }
                    } else if ($mode == 'init') {
                        if ($desktop['profil_id'] == INIT_GID || $mode == "all") {
                            $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
                        }
                    } else if ($mode == "all") {
                        if ($desktop['profil_id'] != ADMIN_GID) {
                            $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
                        }
                    }
//                    foreach ($user['SecondaryDesktop'] as $desktop) {
//                        if ($mode == 'circuit' || $mode == 'rebond') {
//                            if ($desktop['profil_id'] != ADMIN_GID && $desktop['profil_id'] != DISP_GID && $desktop['profil_id'] != INIT_GID) {
//                                $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
//                            }
//                        } else if ($mode == 'init') {
//                            if ($desktop['profil_id'] == INIT_GID) {
//                                $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
//                            }
//                        } else if ($mode == "all" && $desktop['profil_id'] != ADMIN_GID) {
//                            $return[$optgroupUserName][$desktop['id']] = $desktop['name'];
//                        }
//                    }
//					$return[$service['Service']['name']][$desktop['id']] = $desktop['name'];
                }
            }
        }
        return $return;
    }

    /**
     *
     * @param type $id Desktop.id
     * @return type
     */
    public function getDesktopManager($id = null) {
        $return = array();
        $desktop = $this->find(
		'first',
			array(
				'conditions' => array(
					'Desktop.id' => $id
				),
				'contain' => array(
					'Desktopmanager'
				)
			)
        );
//$this->Log( $id );
//$this->Log( $desktop );
//die();
        if( !empty( $desktop ) ) {
            foreach ($desktop['Desktopmanager'] as $desktopmanager) {
                if (!in_array($desktopmanager['id'], $return)) {
                    $return[] = $desktopmanager['id'];
                }
            }
        }
        return $return;
    }

}
