<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Scanemail
 *
 * @author aauzolat
 */
class Scanemail extends AppModel {
    //put your code here

    /**
     * Model name
     *
     * @access public
     */
    public $name = 'Scanemail';

    /**
     *
     * @param type $id
     * @param type $table
     * @param type $ds
     */
    function __construct($id = false, $table = null, $ds = null) {
        parent::__construct($id, $table, $ds);
//		$this->setDataSource('default');
    }

    /**
     * Validation rules
     *
     * @var mixed
     * @access public
     */
    public $validate = array(
        'username' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        ),
        'scriptname' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        ),
        'mail' => array(
            array(
                'rule' => array('email'),
                'allowEmpty' => true
            )
        ),
        'desktopmanager_id' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        ),
        'name' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        ),
        'password' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        ),
        'hostname' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        ),
        'port' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false
            )
        )
    );

    /**
     * belongsTo
     *
     * @access public
     */
    public $belongsTo = array(
        'Desktop' => array(
            'className' => 'Desktop',
            'foreignKey' => 'desktop_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Type' => array(
            'className' => 'Type',
            'foreignKey' => 'type_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Soustype' => array(
            'className' => 'Soustype',
            'foreignKey' => 'soustype_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        ),
        'Desktopmanager' => array(
            'className' => 'Desktopmanager',
            'foreignKey' => 'desktopmanager_id',
            'type' => 'LEFT OUTER',
            'conditions' => null,
            'fields' => null,
            'order' => null
        )
    );

 
    /**
     * Fonction permettant de déchiffrer en BDD les mots de passe utilisés pour les boîtes mail scannées
     * @param type $key
     * @param type $garble
     * @return openssl_decrypt
     */
    public function decrypt($key, $garble) {
        list($encrypted_data, $iv) = explode('::', base64_decode($garble), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $key, 0, $iv);
    }
}
