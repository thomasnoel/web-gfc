<?php

/**
 * Keywordtype model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 *
 * based on As@lae keywordlists / keywords (thesaurus)
 * @link https://adullact.net/projects/asalae/
 */
class Keywordtype extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Keywordtype';

	/**
	 *
	 * @var type
	 */
	public $displayField = 'code';

	/**
	 *
	 * @var type
	 */
	public $displayFields = array(
		'key' => 'code',
		'fields' => array('code', 'documentation'),
		'format' => '%s - %s',
		'order' => array('code')
	);

	/**
	 *
	 * @var type
	 */
	public $order = array('Keywordtype.code');

}

?>
