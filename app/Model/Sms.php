<?php

App::uses('AppModel', 'Model');

/**
 * Sms model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Sms extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Sms';

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'message' => array(
			array(
				'rule' => 'notBlank',
			),
		),
		'numero' => array(
			array(
				'rule' => 'notBlank',
			)
		)
	);

	public $belongsTo = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'foreignKey' => 'courrier_id',
			'type' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);
}

?>

