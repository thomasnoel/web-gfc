<?php

App::uses('Model', 'Model');
App::uses('Sanitize', 'Utility');

/**
 * Application model for Cake.
 *
 * This is a placeholder class.
 * Create the same file in app/app_model.php
 * Add your application-wide methods to the class, your models will inherit them.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class AppModel extends Model {

	/**
	 * Override of the Model's __construct method.
	 *
	 *
	 * @param mixed $id Set this ID for this model on startup, can also be an array of options, see above.
	 * @param string $table Name of database table to use.
	 * @param string $ds DataSource connection name.
	 * @link http://bakery.cakephp.org/articles/doze/2010/03/12/use-multiple-databases-in-one-app-based-on-requested-url
	 */
	public function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$conn = Configure::read('conn');
		if (!empty($conn)) {
			$this->setDataSource($conn);
		}
	}

	/**
	 * Model behaviors
	 *
	 * @var mixed
	 * @access public
	 */
	public $actsAs = array('DatabaseTable', 'Containable', 'ValidateTranslate');

	/**
	 * recursive
	 *
	 * @var mixed
	 * @access public
	 */
//  public $recursive = -1;
	//TODO: optimisation : passer le recursive a -1

	/**
	 * Remplace le caractère * par le caractère % pour les requêtes SQL
	 *
	 * @param type $value
	 * @return type
	 */
	public function wildcard($value) {
		return str_replace('*', '%', Sanitize::escape($value));
	}

	/**
	 *
	 * @param type $table
	 * @return type
	 */
	public function truncate($table = null) {
		if (empty($table)) {
			$table = $this->table;
		}
		$db = &ConnectionManager::getDataSource($this->useDbConfig);
		$res = $db->truncate($table);
		return $res;
	}

	/**
	 *
	 * @param type $model
	 * @param type $data
	 * @return type
	 */
	public function convertFindAllToList($model, $data) {
		$ret = array();
		for ($i = 0; $i < count($data); $i++) {
			$tmp[$data[$i][$model]['id']] = $data[$i][$model]['name'];
		}
		return $ret;
	}

	/**
	 *
	 * @param type $model
	 * @return type
	 */
	public function findListByName(&$model) {
		return $model->find('list', array('fields' => array($model->alias . "." . $model->displayField, $model->alias . ".id")));
	}

	/**
	 * On retourne un booléen true / false dans tous les cas.
	 *
	 * @param type $data
	 * @param type $options
	 * @return boolean
	 */
	public function saveAll($data = NULL, $options = array()) {
		$return = parent::saveAll($data, $options);
		if (is_array($return) && !empty($return)) {
			$return = Set::flatten($return);
			$return = ( array_sum($return) == count($return) );
		} else if (is_array($return) && empty($return)) {
			return false;
		}
		return $return;
	}

	/**
	 *
	 * Ajoute le nom et la valeur d'une variable dans le log debug
	 *
	 * @access protected
	 * @param mixed $var
	 * @return void
	 */
	protected function _logDebug($var) {
		$debugvar = array(
			'================================================================',
			'date' => date('Y-m-d H:i:s'),
			'trace' => Debugger::trace(),
			'var' => $var,
			'type' => gettype($var)
		);
		$this->log(var_export($debugvar, true), 'debug');
	}

	/**
	 *
	 * @param type $var
	 */
	public function ldebug($var) {
		$this->_logDebug($var);
	}

	/**
	 * Débute une transaction.
	 *
	 * @return boolean
	 */
	public function begin() {
		return $this->getDataSource()->begin();
	}

	/**
	 * Valide une transaction.
	 *
	 * @return boolean
	 */
	public function commit() {
		return $this->getDataSource()->commit();
	}

	/**
	 * Annule une transaction.
	 *
	 * @return boolean
	 */
	public function rollback() {
		return $this->getDataSource()->rollback();
	}

	/**
	 * Retourne les requêtes SQL effectuées
	 *
	 * @return array
	 */
	public function getQueries() {
		return $this->getDatasource()->getLog(false, false);
	}

}

?>
