<?php

App::uses('AppBehavior', 'Model/Behavior');

/**
 * Notification behavior class.
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		app
 * @subpackage		app.Model.Behavior
 */
class NotificationBehavior extends AppBehavior {

	/**
	 * Notification model
	 *
	 * @access public
	 * @var Model
	 */
	public $notification;

	/**
	 * Initalisation du modèle Notification
	 *
	 * @access public
	 * @return void
	 */
	public function setup() {
		$this->notification = ClassRegistry::init('Notification');
	}

	/**
	 * Ajouter une notification
	 *
	 * @access public
	 * @return void
	 */
	public function add() {
		$this->notification->add($this->args);
	}

	/**
	 * Marquer une notification comme lue
	 *
	 * @access public
	 * @return void
	 */
	public function read() {
		$this->notification->read($this->args);
	}

}

?>
