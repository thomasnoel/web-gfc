<?php

/**
 * Activite model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Demarchesimplifiee extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Demarchesimplifiee';

	/**
	 *
	 * @var type
	 */
	public $useTable = false;

	public $validate = array(
		'procedureId' => array(
			array(
				'rule' => 'notBlank',
				'message' => 'Ce champ ne peut être vide',
				'allowEmpty' => false
			)
		)
	);

}

?>
