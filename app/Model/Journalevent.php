<?php

App::uses('AppModel', 'Model');
App::uses('SCTarget', 'SimpleComment/Model/Behavior');

/**
 * Courrier model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Journalevent extends AppModel {

    /**
     * Model name
     *
     * @var string
     * @access public
     */
    public $name = 'Journalevent';

    /**
     * Validation rules
     *
     * @var mixed
     * @access public
     */
    public $validate = array(
        'user_id' => array(
            array(
                'rule' => array('notBlank'),
                'allowEmpty' => false,
            ),
        )
    );

    /**
     * Fonction permettant de stocker les données dans la table journalevents
     * @param type $datasSession Données en session
     * @param type $action Action d'où la donée doit être sauvegardée
     * @param type $msg Message a sauvegarder
     * @param type $level Niveau d'alerte
     * @param type $datasCourrier Données du courrier (si présentes)
     */
    public function saveDatas( $datasSession, $action, $msg, $level, $datasCourrier = array()) {

        $dataToSave['Journalevent']['user_id'] = $datasSession['id'];
        $dataToSave['Journalevent']['username'] = $datasSession['username'];
        if( $action != 'createInfos' ) {
            $dataToSave['Journalevent']['desktop_id'] = $datasSession['Courrier']['Desktop']['id'];

        } else if( $action == 'createInfos' && !empty($datasCourrier) ) {
            $dataToSave['Journalevent']['desktop_id'] = $datasCourrier['Courrier']['desktop_creator_id'];
        }
        else {
            $dataToSave['Journalevent']['desktop_id'] = $datasSession['desktop_id'];
        }
        $dataToSave['Journalevent']['date'] = date('Y-m-d H:i:s');
        if( !empty($datasCourrier['Courrier']) ) {
            $dataToSave['Journalevent']['courrier_id'] = $datasCourrier['Courrier']['id'];
            $dataToSave['Journalevent']['reference'] = $datasCourrier['Courrier']['reference'];
        }
        $dataToSave['Journalevent']['action'] = $action;
        $dataToSave['Journalevent']['level'] = $level;
        $dataToSave['Journalevent']['message'] = $msg;
//$this->log($datasSession);
//$this->log($datasCourrier);
//$this->log($dataToSave);
        if( !empty($dataToSave) ) {
            $this->create();
            $this->save( $dataToSave );
        }
    }
}
?>
