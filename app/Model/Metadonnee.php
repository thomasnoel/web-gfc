<?php

App::uses('AppModel', 'Model');

/**
 * Metadonnee model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Metadonnee extends AppModel {

	/**
	 * Model name
	 *
	 * @access public
	 */
	public $name = 'Metadonnee';

	/**
	 *
	 * @var type
	 */
	public $actsAs = array('DataAcl.DataAcl' => array('type' => 'controlled'));

	/**
	 * Validation rules
	 *
	 * @access public
	 */
	public $validate = array(
		'typemetadonnee_id' => array(
			array(
				'rule' => array('numeric'),
				'allowEmpty' => true,
			),
		),
	);

	/**
	 * belongsTo
	 *
	 * @access public
	 */
	public $belongsTo = array(
		'Typemetadonnee' => array(
			'className' => 'Typemetadonnee',
			'foreignKey' => 'typemetadonnee_id',
			'type' => 'INNER',
			'conditions' => null,
			'fields' => null,
			'order' => null
		)
	);

	/**
	 * hasAndBelongsToMany
	 *
	 * @access public
	 */
	public $hasAndBelongsToMany = array(
		'Courrier' => array(
			'className' => 'Courrier',
			'joinTable' => 'courriers_metadonnees',
			'foreignKey' => 'metadonnee_id',
			'associationForeignKey' => 'courrier_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'CourrierMetadonnee'
		),
		'Recherche' => array(
			'className' => 'Recherche',
			'joinTable' => 'recherches_metadonnees',
			'foreignKey' => 'metadonnee_id',
			'associationForeignKey' => 'recherche_id',
			'unique' => null,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'finderQuery' => null,
			'deleteQuery' => null,
			'insertQuery' => null,
			'with' => 'RechercheMetadonnee'
		),
        'Soustype' => array(
            'className' => 'Soustype',
            'joinTable' => 'metadonnees_soustypes',
            'foreignKey' => 'metadonnee_id',
            'associationForeignKey' => 'soustype_id',
            'unique' => null,
            'conditions' => null,
            'fields' => null,
            'order' => null,
            'limit' => null,
            'offset' => null,
            'finderQuery' => null,
            'deleteQuery' => null,
            'insertQuery' => null,
            'with' => 'MetadonneeSoustype'
        ),
	);


    /**
	 * hasMany
	 *
	 * @access public
	 */
	public $hasMany = array(
		'Selectvaluemetadonnee' => array(
			'className' => 'Selectvaluemetadonnee',
			'foreignKey' => 'metadonnee_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
	);

	/**
	 * parentNode override
	 *
	 * @return null
	 */
	function parentNode() {
		return null;
	}

	/**
	 *
	 * @param type $id
	 * @param type $alias
	 * @return null
	 */
	public function getDaco($id = null, $alias = false) {
		if ($id != null) {
			$this->id = $id;
		}

		if (!$this->id) {
			return null;
		}

		$daco = $this->Daco->node(array('model' => 'Metadonnee', 'foreign_key' => $this->id));

		if ($alias) {
			$return = $daco[0]['Daco']['alias'];
		} else {
			$return = $daco[0]['Daco'];
		}

		return $return;
	}

	/**
	 *
	 * @param type $alias
	 * @return type
	 */
	public function getDacos($alias = false) {
		$metadonnees = $this->find('list');
		$return = array();
		foreach ($metadonnees as $kmetadonnee => $metadonnee) {
			$this->id = $kmetadonnee;
			$return[] = $this->getDaco($kmetadonnee, $alias);
		}
		return !empty($return) ? $return : null;
	}

    /**
     * Fonciton permettant de rechercher les métadonnées selon certains critères
     *
     * @param type $criteres
     * @return array()
     */
    public function search($criteres) {
         /// Conditions de base
        if (empty($criteres)) {
            $conditions = array(
                'Metadonnee.active' => true
            );
        }

        if (!empty($criteres)) {
            if (isset($criteres['Metadonnee']['active'])) {
                $conditions[] = array('Metadonnee.active' => $criteres['Metadonnee']['active']);
            }
            if (isset($criteres['Metadonnee']['name']) && !empty($criteres['Metadonnee']['name'])) {
                $conditions[] = 'Metadonnee.name ILIKE \'' . $this->wildcard('%' . $criteres['Metadonnee']['name'] . '%') . '\'';
            }
        }

        $querydata = array(
            'contain' => false,
            'conditions' => $conditions,
            'order' => 'Metadonnee.name'
        );

        return $querydata;
    }
}

?>
