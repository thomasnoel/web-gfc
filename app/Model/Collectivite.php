<?php

App::uses('AppModel', 'Model');

/**
 * Collectivite model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Collectivite extends AppModel {

	/**
	 *
	 * @var type
	 */
	private $_deleteItems = array();

	/**
	 *
	 * @var type
	 */
	public $name = 'Collectivite';

	/**
	 *
	 * @param type $id
	 * @param type $table
	 * @param type $ds
	 */
	function __construct($id = false, $table = null, $ds = null) {
		parent::__construct($id, $table, $ds);
		$this->setDataSource('default');
	}

	/**
	 *
	 * @var type
	 */
	public $validate = array(
//		'config_id' => array(
//			array(
//				'rule' => array('numeric'),
//				'allowEmpty' => false
//			)
//		),
		'name' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false
			),
			array(
				'rule' => array('isunique'),
				'allowEmpty' => true
			)
		),
//		'adresse' => array(
//			array(
//				'rule' => array('notBlank'),
//				'allowEmpty' => false
//			)
//		),
//		'codepostal' => array(
//			array(
//				'rule' => array('notBlank'),
//				'allowEmpty' => false
//			)
//		),
//		'ville' => array(
//			array(
//				'rule' => array('notBlank'),
//				'allowEmpty' => false
//			)
//		),
//		'siren' => array(
//			array(
//				'rule' => array('numeric'),
//				'allowEmpty' => false,
//				'minLength' => '10'
//			)
//		),
//		'codeinsee' => array(
//			array(
//				'rule' => array('numeric'),
//				'allowEmpty' => false,
//				'minLength' => '10'
//			)
//		),
//		'telephone' => array(
//			array(
//				'rule' => array('numeric'),
//				'allowEmpty' => false,
//				'minLength' => '10'
//			)
//		),
		'conn' => array(
			array(
				'rule' => array('notBlank'),
				'allowEmpty' => false
			)
		),
		'login_suffix' => array(
			array(
				'rule' => array('isunique')
			)
		)
	);

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function getName($id) {
		$ret = false;
		$collectivite = $this->find('first', array('conditions' => array('Collectivite.id' => $id)));
		if (!empty($collectivite)) {
			$ret = $collectivite["Collectivite"]["name"];
		}
		return $ret;
	}

	/**
	 *
	 * @param type $id
	 * @return type
	 */
	public function isDeletable($id) {
		$conn = $this->field('Collectivite.conn', array('Collectivite.id' => $id));
		$courriers = ClassRegistry::init('Courrier');
		$courriers->setDataSource($conn);
		$courriers->recursive = -1;
		return ($courriers->find('count') == 0);
	}

}

?>
