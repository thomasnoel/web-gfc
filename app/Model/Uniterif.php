<?php


/**
 * Uniterif model class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud Auzolat
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 * @package		app
 * @subpackage		app.Model
 */
class Uniterif extends AppModel {

	/**
	 *
	 * @var type
	 */
	public $name = 'Uniterif';

	/**
	 *
	 * @var type
	 */
	public $useTable = 'uniterifs';


    public $hasMany = array(
		'Marche' => array(
			'className' => 'Marche',
			'foreignKey' => 'uniterif_id',
			'dependent' => false,
			'conditions' => null,
			'fields' => null,
			'order' => null,
			'limit' => null,
			'offset' => null,
			'exclusive' => null,
			'finderQuery' => null,
			'counterQuery' => null
		)
    );

}

?>
