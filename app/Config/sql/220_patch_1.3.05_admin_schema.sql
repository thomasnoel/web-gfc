-- 220_patch_1.3.05_admin_schema.sql script
-- Patch permettant la mise en place des notifications par mail des utilisateurs
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

-- SELECT add_missing_table_field ('public', 'collectivites', 'scanemail_id', 'INTEGER');


/* Table pour stocker les valeurs de emails à scruter pour la génération des flux */
/*DROP TABLE IF EXISTS scanemails CASCADE;
CREATE TABLE scanemails(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    collectivite_id integer not null references collectivites(id) on update cascade on delete cascade,
    mail VARCHAR(255) NOT NULL,
    hostname VARCHAR(255) NOT NULL,
    port VARCHAR(24) DEFAULT '143',
    password VARCHAR(255) NOT NULL,
    desktop_id integer not null,
    type_id integer,
    soustype_id integer,
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE scanemails IS 'Table permettant de stocker les emails et destinataires pour chaque collectivité';

DROP INDEX IF EXISTS scanemails_collectivite_id_idx;
CREATE INDEX scanemails_collectivite_id_idx ON scanemails( collectivite_id );

DROP INDEX IF EXISTS scanemails_desktop_id_idx;
CREATE INDEX scanemails_desktop_id_idx ON scanemails( desktop_id );

DROP INDEX IF EXISTS scanemails_collectivite_id_desktop_id_idx;
CREATE UNIQUE INDEX scanemails_collectivite_id_desktop_id_idx ON scanemails(collectivite_id,desktop_id);

DROP INDEX IF EXISTS scanemails_type_id_idx;
CREATE INDEX scanemails_type_id_idx ON scanemails( type_id );

DROP INDEX IF EXISTS scanemails_soustype_id_idx;
CREATE INDEX scanemails_soustype_id_idx ON scanemails( soustype_id ); */

COMMIT;
