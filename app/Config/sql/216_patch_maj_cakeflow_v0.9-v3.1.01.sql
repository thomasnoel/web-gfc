-- 216_patch_maj_cakeflow_v0.9-v3.1.01.sql script
-- Patch de montée de version de cakeflow de v0.9 vers v 3.1.01
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************
ALTER TABLE wkf_visas ALTER COLUMN commentaire TYPE TEXT;
ALTER TABLE wkf_etapes ADD COLUMN soustype integer DEFAULT NULL;
ALTER TABLE wkf_compositions ADD COLUMN soustype integer DEFAULT NULL;
ALTER TABLE wkf_compositions ADD COLUMN type_composition VARCHAR(20) DEFAULT 'USER';

ALTER TABLE wkf_visas ADD COLUMN etape_id INT REFERENCES wkf_etapes(id) DEFAULT NULL;
ALTER TABLE wkf_signatures ADD COLUMN visa_id INT REFERENCES wkf_visas(id);

ALTER TABLE wkf_etapes ADD COLUMN cpt_retard INT;
ALTER TABLE wkf_visas ADD COLUMN date_retard TIMESTAMP WITHOUT TIME ZONE;

COMMIT;
