--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: acos; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY acos (id, parent_id, model, foreign_key, alias, lft, rght) FROM stdin;
424	421	\N	\N	add	845	846
425	421	\N	\N	edit	847	848
426	421	\N	\N	getFlux	849	850
3	2	\N	\N	index	3	4
427	421	\N	\N	delete	851	852
428	421	\N	\N	linkGroup	853	854
4	2	\N	\N	get_addressbooks	5	6
429	421	\N	\N	link	855	856
5	2	\N	\N	add	7	8
430	421	\N	\N	unlinkGroup	857	858
431	421	\N	\N	unlink	859	860
6	2	\N	\N	edit	9	10
432	421	\N	\N	userAclCheck	861	862
7	2	\N	\N	delete	11	12
433	421	\N	\N	getSessionInitServices	863	864
434	421	\N	\N	getSessionAppDefinedDesktops	865	866
8	2	\N	\N	userAclCheck	13	14
435	421	\N	\N	getSessionInitDesktops	867	868
9	2	\N	\N	getSessionInitServices	15	16
436	421	\N	\N	getSessionValDesktops	869	870
437	421	\N	\N	getSessionValeditDesktops	871	872
10	2	\N	\N	getSessionAppDefinedDesktops	17	18
438	421	\N	\N	getSessionDispDesktops	873	874
11	2	\N	\N	getSessionInitDesktops	19	20
439	421	\N	\N	getSessionArchDesktops	875	876
440	421	\N	\N	getSessionAllDesktops	877	878
12	2	\N	\N	getSessionValDesktops	21	22
441	421	\N	\N	getEnv	879	880
13	2	\N	\N	getSessionValeditDesktops	23	24
442	421	\N	\N	getAddParams	881	882
443	421	\N	\N	ldebug	883	884
14	2	\N	\N	getSessionDispDesktops	25	26
15	2	\N	\N	getSessionArchDesktops	27	28
445	444	\N	\N	add	887	888
16	2	\N	\N	getSessionAllDesktops	29	30
446	444	\N	\N	delete	889	890
17	2	\N	\N	getEnv	31	32
447	444	\N	\N	download	891	892
18	2	\N	\N	getAddParams	33	34
448	444	\N	\N	getPreview	893	894
2	1	\N	\N	Addressbooks	2	37
19	2	\N	\N	ldebug	35	36
449	444	\N	\N	userAclCheck	895	896
450	444	\N	\N	getSessionInitServices	897	898
21	20	\N	\N	check	39	40
451	444	\N	\N	getSessionAppDefinedDesktops	899	900
22	20	\N	\N	userAclCheck	41	42
452	444	\N	\N	getSessionInitDesktops	901	902
23	20	\N	\N	getSessionInitServices	43	44
453	444	\N	\N	getSessionValDesktops	903	904
24	20	\N	\N	getSessionAppDefinedDesktops	45	46
454	444	\N	\N	getSessionValeditDesktops	905	906
25	20	\N	\N	getSessionInitDesktops	47	48
455	444	\N	\N	getSessionDispDesktops	907	908
26	20	\N	\N	getSessionValDesktops	49	50
456	444	\N	\N	getSessionArchDesktops	909	910
27	20	\N	\N	getSessionValeditDesktops	51	52
457	444	\N	\N	getSessionAllDesktops	911	912
28	20	\N	\N	getSessionDispDesktops	53	54
458	444	\N	\N	getEnv	913	914
29	20	\N	\N	getSessionArchDesktops	55	56
459	444	\N	\N	getAddParams	915	916
30	20	\N	\N	getSessionAllDesktops	57	58
444	1	\N	\N	Rmodels	886	919
460	444	\N	\N	ldebug	917	918
31	20	\N	\N	getEnv	59	60
32	20	\N	\N	getAddParams	61	62
462	461	\N	\N	index	921	922
20	1	\N	\N	Checks	38	65
33	20	\N	\N	ldebug	63	64
463	461	\N	\N	getServices	923	924
464	461	\N	\N	add	925	926
35	34	\N	\N	add	67	68
465	461	\N	\N	edit	927	928
36	34	\N	\N	edit	69	70
466	461	\N	\N	setInfos	929	930
37	34	\N	\N	delete	71	72
467	461	\N	\N	setDroitsData	931	932
38	34	\N	\N	userAclCheck	73	74
468	461	\N	\N	setDroitsMeta	933	934
39	34	\N	\N	getSessionInitServices	75	76
469	461	\N	\N	delete	935	936
40	34	\N	\N	getSessionAppDefinedDesktops	77	78
470	461	\N	\N	userAclCheck	937	938
41	34	\N	\N	getSessionInitDesktops	79	80
42	34	\N	\N	getSessionValDesktops	81	82
43	34	\N	\N	getSessionValeditDesktops	83	84
44	34	\N	\N	getSessionDispDesktops	85	86
461	1	\N	\N	Services	920	961
45	34	\N	\N	getSessionArchDesktops	87	88
46	34	\N	\N	getSessionAllDesktops	89	90
47	34	\N	\N	getEnv	91	92
48	34	\N	\N	getAddParams	93	94
34	1	\N	\N	Affaires	66	97
49	34	\N	\N	ldebug	95	96
51	50	\N	\N	add	99	100
52	50	\N	\N	delete	101	102
53	50	\N	\N	download	103	104
54	50	\N	\N	getPreview	105	106
55	50	\N	\N	userAclCheck	107	108
56	50	\N	\N	getSessionInitServices	109	110
57	50	\N	\N	getSessionAppDefinedDesktops	111	112
58	50	\N	\N	getSessionInitDesktops	113	114
59	50	\N	\N	getSessionValDesktops	115	116
60	50	\N	\N	getSessionValeditDesktops	117	118
61	50	\N	\N	getSessionDispDesktops	119	120
62	50	\N	\N	getSessionArchDesktops	121	122
63	50	\N	\N	getSessionAllDesktops	123	124
64	50	\N	\N	getEnv	125	126
65	50	\N	\N	getAddParams	127	128
50	1	\N	\N	Armodels	98	131
66	50	\N	\N	ldebug	129	130
68	67	\N	\N	select	133	134
69	67	\N	\N	delete	135	136
70	67	\N	\N	download	137	138
71	67	\N	\N	getPreview	139	140
72	67	\N	\N	userAclCheck	141	142
73	67	\N	\N	getSessionInitServices	143	144
74	67	\N	\N	getSessionAppDefinedDesktops	145	146
75	67	\N	\N	getSessionInitDesktops	147	148
471	461	\N	\N	getSessionInitServices	939	940
472	461	\N	\N	getSessionAppDefinedDesktops	941	942
76	67	\N	\N	getSessionValDesktops	149	150
473	461	\N	\N	getSessionInitDesktops	943	944
77	67	\N	\N	getSessionValeditDesktops	151	152
474	461	\N	\N	getSessionValDesktops	945	946
475	461	\N	\N	getSessionValeditDesktops	947	948
78	67	\N	\N	getSessionDispDesktops	153	154
476	461	\N	\N	getSessionDispDesktops	949	950
79	67	\N	\N	getSessionArchDesktops	155	156
477	461	\N	\N	getSessionArchDesktops	951	952
478	461	\N	\N	getSessionAllDesktops	953	954
80	67	\N	\N	getSessionAllDesktops	157	158
479	461	\N	\N	getEnv	955	956
81	67	\N	\N	getEnv	159	160
480	461	\N	\N	getAddParams	957	958
481	461	\N	\N	ldebug	959	960
82	67	\N	\N	getAddParams	161	162
67	1	\N	\N	Ars	132	165
83	67	\N	\N	ldebug	163	164
483	482	\N	\N	index	963	964
484	482	\N	\N	getTypesSoustypes	965	966
85	84	\N	\N	index	167	168
485	482	\N	\N	setInfos	967	968
86	84	\N	\N	getCollectivites	169	170
486	482	\N	\N	setArmodels	969	970
87	84	\N	\N	ajaxformvalid	171	172
487	482	\N	\N	setRmodels	971	972
88	84	\N	\N	getConnList	173	174
488	482	\N	\N	add	973	974
89	84	\N	\N	add	175	176
489	482	\N	\N	edit	975	976
90	84	\N	\N	edit	177	178
490	482	\N	\N	delete	977	978
91	84	\N	\N	delete	179	180
491	482	\N	\N	getRefSimul	979	980
92	84	\N	\N	upload	181	182
492	482	\N	\N	userAclCheck	981	982
93	84	\N	\N	getLogo	183	184
493	482	\N	\N	getSessionInitServices	983	984
94	84	\N	\N	admin	185	186
494	482	\N	\N	getSessionAppDefinedDesktops	985	986
95	84	\N	\N	userAclCheck	187	188
495	482	\N	\N	getSessionInitDesktops	987	988
96	84	\N	\N	getSessionInitServices	189	190
496	482	\N	\N	getSessionValDesktops	989	990
97	84	\N	\N	getSessionAppDefinedDesktops	191	192
497	482	\N	\N	getSessionValeditDesktops	991	992
98	84	\N	\N	getSessionInitDesktops	193	194
498	482	\N	\N	getSessionDispDesktops	993	994
99	84	\N	\N	getSessionValDesktops	195	196
499	482	\N	\N	getSessionArchDesktops	995	996
100	84	\N	\N	getSessionValeditDesktops	197	198
500	482	\N	\N	getSessionAllDesktops	997	998
101	84	\N	\N	getSessionDispDesktops	199	200
501	482	\N	\N	getEnv	999	1000
102	84	\N	\N	getSessionArchDesktops	201	202
502	482	\N	\N	getAddParams	1001	1002
103	84	\N	\N	getSessionAllDesktops	203	204
482	1	\N	\N	Soustypes	962	1005
503	482	\N	\N	ldebug	1003	1004
104	84	\N	\N	getEnv	205	206
105	84	\N	\N	getAddParams	207	208
505	504	\N	\N	add	1007	1008
84	1	\N	\N	Collectivites	166	211
106	84	\N	\N	ldebug	209	210
506	504	\N	\N	edit	1009	1010
507	504	\N	\N	delete	1011	1012
108	107	\N	\N	add	213	214
508	504	\N	\N	checkTache	1013	1014
109	107	\N	\N	edit	215	216
509	504	\N	\N	grabTache	1015	1016
110	107	\N	\N	delete	217	218
510	504	\N	\N	releaseTache	1017	1018
111	107	\N	\N	userAclCheck	219	220
511	504	\N	\N	getTaches	1019	1020
112	107	\N	\N	getSessionInitServices	221	222
512	504	\N	\N	setFromTask	1021	1022
113	107	\N	\N	getSessionAppDefinedDesktops	223	224
513	504	\N	\N	userAclCheck	1023	1024
114	107	\N	\N	getSessionInitDesktops	225	226
514	504	\N	\N	getSessionInitServices	1025	1026
115	107	\N	\N	getSessionValDesktops	227	228
515	504	\N	\N	getSessionAppDefinedDesktops	1027	1028
116	107	\N	\N	getSessionValeditDesktops	229	230
117	107	\N	\N	getSessionDispDesktops	231	232
118	107	\N	\N	getSessionArchDesktops	233	234
119	107	\N	\N	getSessionAllDesktops	235	236
120	107	\N	\N	getEnv	237	238
121	107	\N	\N	getAddParams	239	240
107	1	\N	\N	Comments	212	243
122	107	\N	\N	ldebug	241	242
124	123	\N	\N	userAclCheck	245	246
125	123	\N	\N	getSessionInitServices	247	248
126	123	\N	\N	getSessionAppDefinedDesktops	249	250
127	123	\N	\N	getSessionInitDesktops	251	252
128	123	\N	\N	getSessionValDesktops	253	254
129	123	\N	\N	getSessionValeditDesktops	255	256
130	123	\N	\N	getSessionDispDesktops	257	258
131	123	\N	\N	getSessionArchDesktops	259	260
132	123	\N	\N	getSessionAllDesktops	261	262
133	123	\N	\N	getEnv	263	264
134	123	\N	\N	getAddParams	265	266
123	1	\N	\N	Compteurs	244	269
135	123	\N	\N	ldebug	267	268
137	136	\N	\N	add	271	272
138	136	\N	\N	getHomonyms	273	274
139	136	\N	\N	edit	275	276
140	136	\N	\N	delete	277	278
141	136	\N	\N	userAclCheck	279	280
142	136	\N	\N	getSessionInitServices	281	282
143	136	\N	\N	getSessionAppDefinedDesktops	283	284
144	136	\N	\N	getSessionInitDesktops	285	286
145	136	\N	\N	getSessionValDesktops	287	288
146	136	\N	\N	getSessionValeditDesktops	289	290
147	136	\N	\N	getSessionDispDesktops	291	292
148	136	\N	\N	getSessionArchDesktops	293	294
149	136	\N	\N	getSessionAllDesktops	295	296
516	504	\N	\N	getSessionInitDesktops	1029	1030
150	136	\N	\N	getEnv	297	298
517	504	\N	\N	getSessionValDesktops	1031	1032
151	136	\N	\N	getAddParams	299	300
136	1	\N	\N	Contactinfos	270	303
152	136	\N	\N	ldebug	301	302
518	504	\N	\N	getSessionValeditDesktops	1033	1034
519	504	\N	\N	getSessionDispDesktops	1035	1036
154	153	\N	\N	get_contacts	305	306
520	504	\N	\N	getSessionArchDesktops	1037	1038
155	153	\N	\N	add	307	308
521	504	\N	\N	getSessionAllDesktops	1039	1040
156	153	\N	\N	edit	309	310
522	504	\N	\N	getEnv	1041	1042
157	153	\N	\N	delete	311	312
523	504	\N	\N	getAddParams	1043	1044
504	1	\N	\N	Taches	1006	1047
158	153	\N	\N	import	313	314
524	504	\N	\N	ldebug	1045	1046
159	153	\N	\N	search	315	316
526	525	\N	\N	add	1049	1050
160	153	\N	\N	searchContactinfo	317	318
527	525	\N	\N	edit	1051	1052
161	153	\N	\N	getHomonyms	319	320
528	525	\N	\N	delete	1053	1054
162	153	\N	\N	userAclCheck	321	322
529	525	\N	\N	userAclCheck	1055	1056
163	153	\N	\N	getSessionInitServices	323	324
530	525	\N	\N	getSessionInitServices	1057	1058
164	153	\N	\N	getSessionAppDefinedDesktops	325	326
531	525	\N	\N	getSessionAppDefinedDesktops	1059	1060
165	153	\N	\N	getSessionInitDesktops	327	328
532	525	\N	\N	getSessionInitDesktops	1061	1062
166	153	\N	\N	getSessionValDesktops	329	330
533	525	\N	\N	getSessionValDesktops	1063	1064
167	153	\N	\N	getSessionValeditDesktops	331	332
534	525	\N	\N	getSessionValeditDesktops	1065	1066
168	153	\N	\N	getSessionDispDesktops	333	334
535	525	\N	\N	getSessionDispDesktops	1067	1068
169	153	\N	\N	getSessionArchDesktops	335	336
536	525	\N	\N	getSessionArchDesktops	1069	1070
170	153	\N	\N	getSessionAllDesktops	337	338
537	525	\N	\N	getSessionAllDesktops	1071	1072
171	153	\N	\N	getEnv	339	340
538	525	\N	\N	getEnv	1073	1074
172	153	\N	\N	getAddParams	341	342
153	1	\N	\N	Contacts	304	345
173	153	\N	\N	ldebug	343	344
539	525	\N	\N	getAddParams	1075	1076
525	1	\N	\N	Types	1048	1079
540	525	\N	\N	ldebug	1077	1078
175	174	\N	\N	view	347	348
542	541	\N	\N	index	1081	1082
176	174	\N	\N	edit	349	350
543	541	\N	\N	getUsers	1083	1084
177	174	\N	\N	add	351	352
544	541	\N	\N	add	1085	1086
178	174	\N	\N	getInfos	353	354
545	541	\N	\N	ajaxformvalid	1087	1088
179	174	\N	\N	getCircuit	355	356
546	541	\N	\N	edit	1089	1090
180	174	\N	\N	getMeta	357	358
547	541	\N	\N	delete	1091	1092
181	174	\N	\N	getTache	359	360
548	541	\N	\N	setInfosPersos	1093	1094
182	174	\N	\N	getAffaire	361	362
549	541	\N	\N	getInfosCompte	1095	1096
183	174	\N	\N	createInfos	363	364
550	541	\N	\N	editinfo	1097	1098
184	174	\N	\N	setInfos	365	366
551	541	\N	\N	changemdp	1099	1100
185	174	\N	\N	setComments	367	368
552	541	\N	\N	rights	1101	1102
186	174	\N	\N	getComments	369	370
553	541	\N	\N	getRights	1103	1104
187	174	\N	\N	getContacts	371	372
554	541	\N	\N	login	1105	1106
188	174	\N	\N	getFlowControls	373	374
555	541	\N	\N	logout	1107	1108
189	174	\N	\N	setMeta	375	376
556	541	\N	\N	isAway	1109	1110
190	174	\N	\N	setTache	377	378
557	541	\N	\N	mdplost	1111	1112
191	174	\N	\N	setAffaire	379	380
558	541	\N	\N	userAclCheck	1113	1114
192	174	\N	\N	reponse	381	382
559	541	\N	\N	getSessionInitServices	1115	1116
193	174	\N	\N	valid	383	384
560	541	\N	\N	getSessionAppDefinedDesktops	1117	1118
194	174	\N	\N	insert	385	386
561	541	\N	\N	getSessionInitDesktops	1119	1120
195	174	\N	\N	refus	387	388
562	541	\N	\N	getSessionValDesktops	1121	1122
196	174	\N	\N	sendcpy	389	390
563	541	\N	\N	getSessionValeditDesktops	1123	1124
197	174	\N	\N	sendwkf	391	392
564	541	\N	\N	getSessionDispDesktops	1125	1126
198	174	\N	\N	send	393	394
199	174	\N	\N	dispsend	395	396
200	174	\N	\N	delete	397	398
201	174	\N	\N	getInsertEnd	399	400
202	174	\N	\N	getArs	401	402
203	174	\N	\N	setArs	403	404
204	174	\N	\N	genResponse	405	406
205	174	\N	\N	getContext	407	408
206	174	\N	\N	getRelements	409	410
207	174	\N	\N	getDocuments	411	412
208	174	\N	\N	getParent	413	414
209	174	\N	\N	acquisition	415	416
210	174	\N	\N	sendBackToDisp	417	418
211	174	\N	\N	userAclCheck	419	420
212	174	\N	\N	getSessionInitServices	421	422
213	174	\N	\N	getSessionAppDefinedDesktops	423	424
214	174	\N	\N	getSessionInitDesktops	425	426
215	174	\N	\N	getSessionValDesktops	427	428
216	174	\N	\N	getSessionValeditDesktops	429	430
217	174	\N	\N	getSessionDispDesktops	431	432
218	174	\N	\N	getSessionArchDesktops	433	434
219	174	\N	\N	getSessionAllDesktops	435	436
220	174	\N	\N	getEnv	437	438
221	174	\N	\N	getAddParams	439	440
174	1	\N	\N	Courriers	346	443
222	174	\N	\N	ldebug	441	442
565	541	\N	\N	getSessionArchDesktops	1127	1128
224	223	\N	\N	add	445	446
566	541	\N	\N	getSessionAllDesktops	1129	1130
225	223	\N	\N	edit	447	448
567	541	\N	\N	getEnv	1131	1132
226	223	\N	\N	setRights	449	450
568	541	\N	\N	getAddParams	1133	1134
541	1	\N	\N	Users	1080	1137
227	223	\N	\N	setDeleg	451	452
569	541	\N	\N	ldebug	1135	1136
228	223	\N	\N	getRights	453	454
571	570	\N	\N	chk	1139	1140
229	223	\N	\N	rights	455	456
572	570	\N	\N	service	1141	1142
230	223	\N	\N	delete	457	458
573	570	\N	\N	echotest	1143	1144
231	223	\N	\N	userAclCheck	459	460
574	570	\N	\N	getGFCCollectivites	1145	1146
232	223	\N	\N	getSessionInitServices	461	462
575	570	\N	\N	getGFCTypes	1147	1148
233	223	\N	\N	getSessionAppDefinedDesktops	463	464
576	570	\N	\N	getGFCSoustypes	1149	1150
234	223	\N	\N	getSessionInitDesktops	465	466
577	570	\N	\N	createCourrier	1151	1152
235	223	\N	\N	getSessionValDesktops	467	468
578	570	\N	\N	scanMass	1153	1154
236	223	\N	\N	getSessionValeditDesktops	469	470
579	570	\N	\N	getStatut	1155	1156
237	223	\N	\N	getSessionDispDesktops	471	472
580	570	\N	\N	userAclCheck	1157	1158
238	223	\N	\N	getSessionArchDesktops	473	474
581	570	\N	\N	getSessionInitServices	1159	1160
239	223	\N	\N	getSessionAllDesktops	475	476
582	570	\N	\N	getSessionAppDefinedDesktops	1161	1162
240	223	\N	\N	getEnv	477	478
583	570	\N	\N	getSessionInitDesktops	1163	1164
241	223	\N	\N	getAddParams	479	480
223	1	\N	\N	Desktops	444	483
242	223	\N	\N	ldebug	481	482
584	570	\N	\N	getSessionValDesktops	1165	1166
585	570	\N	\N	getSessionValeditDesktops	1167	1168
244	243	\N	\N	setMainDoc	485	486
586	570	\N	\N	getSessionDispDesktops	1169	1170
245	243	\N	\N	upload	487	488
587	570	\N	\N	getSessionArchDesktops	1171	1172
246	243	\N	\N	delete	489	490
588	570	\N	\N	getSessionAllDesktops	1173	1174
247	243	\N	\N	download	491	492
589	570	\N	\N	getEnv	1175	1176
248	243	\N	\N	getPreview	493	494
590	570	\N	\N	getAddParams	1177	1178
570	1	\N	\N	Webservices	1138	1181
249	243	\N	\N	userAclCheck	495	496
591	570	\N	\N	ldebug	1179	1180
250	243	\N	\N	getSessionInitServices	497	498
593	592	\N	\N	index	1183	1184
251	243	\N	\N	getSessionAppDefinedDesktops	499	500
594	592	\N	\N	getWorkflows	1185	1186
252	243	\N	\N	getSessionInitDesktops	501	502
595	592	\N	\N	add	1187	1188
253	243	\N	\N	getSessionValDesktops	503	504
596	592	\N	\N	edit	1189	1190
254	243	\N	\N	getSessionValeditDesktops	505	506
597	592	\N	\N	setSchema	1191	1192
255	243	\N	\N	getSessionDispDesktops	507	508
598	592	\N	\N	delete	1193	1194
256	243	\N	\N	getSessionArchDesktops	509	510
599	592	\N	\N	moveEtape	1195	1196
257	243	\N	\N	getSessionAllDesktops	511	512
600	592	\N	\N	userAclCheck	1197	1198
258	243	\N	\N	getEnv	513	514
601	592	\N	\N	getSessionInitServices	1199	1200
259	243	\N	\N	getAddParams	515	516
243	1	\N	\N	Documents	484	519
260	243	\N	\N	ldebug	517	518
602	592	\N	\N	getSessionAppDefinedDesktops	1201	1202
603	592	\N	\N	getSessionInitDesktops	1203	1204
262	261	\N	\N	index	521	522
604	592	\N	\N	getSessionValDesktops	1205	1206
263	261	\N	\N	add	523	524
605	592	\N	\N	getSessionValeditDesktops	1207	1208
264	261	\N	\N	edit	525	526
606	592	\N	\N	getSessionDispDesktops	1209	1210
265	261	\N	\N	delete	527	528
607	592	\N	\N	getSessionArchDesktops	1211	1212
266	261	\N	\N	getDossiers	529	530
608	592	\N	\N	getSessionAllDesktops	1213	1214
267	261	\N	\N	userAclCheck	531	532
609	592	\N	\N	getEnv	1215	1216
610	592	\N	\N	getAddParams	1217	1218
268	261	\N	\N	getSessionInitServices	533	534
269	261	\N	\N	getSessionAppDefinedDesktops	535	536
270	261	\N	\N	getSessionInitDesktops	537	538
271	261	\N	\N	getSessionValDesktops	539	540
272	261	\N	\N	getSessionValeditDesktops	541	542
273	261	\N	\N	getSessionDispDesktops	543	544
274	261	\N	\N	getSessionArchDesktops	545	546
275	261	\N	\N	getSessionAllDesktops	547	548
276	261	\N	\N	getEnv	549	550
277	261	\N	\N	getAddParams	551	552
261	1	\N	\N	Dossiers	520	555
278	261	\N	\N	ldebug	553	554
280	279	\N	\N	getBancontenusServices	557	558
281	279	\N	\N	index	559	560
282	279	\N	\N	superadminenv	561	562
283	279	\N	\N	adminenv	563	564
284	279	\N	\N	getNotifications	565	566
285	279	\N	\N	setNotificationRead	567	568
286	279	\N	\N	setAllNotificationRead	569	570
287	279	\N	\N	userenv	571	572
288	279	\N	\N	dispenv	573	574
289	279	\N	\N	historique	575	576
290	279	\N	\N	userAclCheck	577	578
291	279	\N	\N	getSessionInitServices	579	580
292	279	\N	\N	getSessionAppDefinedDesktops	581	582
293	279	\N	\N	getSessionInitDesktops	583	584
294	279	\N	\N	getSessionValDesktops	585	586
295	279	\N	\N	getSessionValeditDesktops	587	588
296	279	\N	\N	getSessionDispDesktops	589	590
592	1	\N	\N	Workflows	1182	1221
611	592	\N	\N	ldebug	1219	1220
297	279	\N	\N	getSessionArchDesktops	591	592
612	1	\N	\N	AclExtras	1222	1223
298	279	\N	\N	getSessionAllDesktops	593	594
613	1	\N	\N	Addressbook	1224	1225
299	279	\N	\N	getEnv	595	596
300	279	\N	\N	getAddParams	597	598
279	1	\N	\N	Environnement	556	601
301	279	\N	\N	ldebug	599	600
616	615	\N	\N	index	1228	1229
617	615	\N	\N	view	1230	1231
303	302	\N	\N	getGroups	603	604
304	302	\N	\N	view	605	606
618	615	\N	\N	add	1232	1233
305	302	\N	\N	add	607	608
619	615	\N	\N	edit	1234	1235
306	302	\N	\N	edit	609	610
620	615	\N	\N	delete	1236	1237
307	302	\N	\N	setInfos	611	612
308	302	\N	\N	setDroitsFunc	613	614
621	615	\N	\N	visuCircuit	1238	1239
309	302	\N	\N	getDroitsFunc	615	616
622	615	\N	\N	reorder	1240	1241
310	302	\N	\N	delete	617	618
623	615	\N	\N	setCreatedModifiedUser	1242	1243
311	302	\N	\N	ajaxformvalid	619	620
312	302	\N	\N	userAclCheck	621	622
624	615	\N	\N	formatUser	1244	1245
313	302	\N	\N	getSessionInitServices	623	624
625	615	\N	\N	formatLinkedModel	1246	1247
314	302	\N	\N	getSessionAppDefinedDesktops	625	626
626	615	\N	\N	listLinkedModel	1248	1249
315	302	\N	\N	getSessionInitDesktops	627	628
316	302	\N	\N	getSessionValDesktops	629	630
627	615	\N	\N	userAclCheck	1250	1251
317	302	\N	\N	getSessionValeditDesktops	631	632
628	615	\N	\N	getSessionInitServices	1252	1253
318	302	\N	\N	getSessionDispDesktops	633	634
629	615	\N	\N	getSessionAppDefinedDesktops	1254	1255
319	302	\N	\N	getSessionArchDesktops	635	636
320	302	\N	\N	getSessionAllDesktops	637	638
630	615	\N	\N	getSessionInitDesktops	1256	1257
321	302	\N	\N	getEnv	639	640
631	615	\N	\N	getSessionValDesktops	1258	1259
322	302	\N	\N	getAddParams	641	642
302	1	\N	\N	Groups	602	645
323	302	\N	\N	ldebug	643	644
632	615	\N	\N	getSessionValeditDesktops	1260	1261
633	615	\N	\N	getSessionDispDesktops	1262	1263
325	324	\N	\N	index	647	648
634	615	\N	\N	getSessionArchDesktops	1264	1265
326	324	\N	\N	get_keywordlists	649	650
327	324	\N	\N	edit	651	652
635	615	\N	\N	getSessionAllDesktops	1266	1267
328	324	\N	\N	view	653	654
636	615	\N	\N	getEnv	1268	1269
329	324	\N	\N	userAclCheck	655	656
330	324	\N	\N	getSessionInitServices	657	658
637	615	\N	\N	getAddParams	1270	1271
331	324	\N	\N	getSessionAppDefinedDesktops	659	660
615	614	\N	\N	Circuits	1227	1274
332	324	\N	\N	getSessionInitDesktops	661	662
638	615	\N	\N	ldebug	1272	1273
333	324	\N	\N	getSessionValDesktops	663	664
334	324	\N	\N	getSessionValeditDesktops	665	666
335	324	\N	\N	getSessionDispDesktops	667	668
640	639	\N	\N	index	1276	1277
336	324	\N	\N	getSessionArchDesktops	669	670
641	639	\N	\N	view	1278	1279
337	324	\N	\N	getSessionAllDesktops	671	672
338	324	\N	\N	getEnv	673	674
642	639	\N	\N	add	1280	1281
339	324	\N	\N	getAddParams	675	676
324	1	\N	\N	Keywordlists	646	679
340	324	\N	\N	ldebug	677	678
643	639	\N	\N	edit	1282	1283
342	341	\N	\N	userAclCheck	681	682
644	639	\N	\N	delete	1284	1285
343	341	\N	\N	getSessionInitServices	683	684
645	639	\N	\N	setCreatedModifiedUser	1286	1287
344	341	\N	\N	getSessionAppDefinedDesktops	685	686
345	341	\N	\N	getSessionInitDesktops	687	688
646	639	\N	\N	formatUser	1288	1289
346	341	\N	\N	getSessionValDesktops	689	690
347	341	\N	\N	getSessionValeditDesktops	691	692
647	639	\N	\N	formatLinkedModel	1290	1291
348	341	\N	\N	getSessionDispDesktops	693	694
648	639	\N	\N	listLinkedModel	1292	1293
349	341	\N	\N	getSessionArchDesktops	695	696
350	341	\N	\N	getSessionAllDesktops	697	698
649	639	\N	\N	userAclCheck	1294	1295
351	341	\N	\N	getEnv	699	700
352	341	\N	\N	getAddParams	701	702
650	639	\N	\N	getSessionInitServices	1296	1297
341	1	\N	\N	Keywords	680	705
353	341	\N	\N	ldebug	703	704
651	639	\N	\N	getSessionAppDefinedDesktops	1298	1299
355	354	\N	\N	userAclCheck	707	708
652	639	\N	\N	getSessionInitDesktops	1300	1301
356	354	\N	\N	getSessionInitServices	709	710
357	354	\N	\N	getSessionAppDefinedDesktops	711	712
653	639	\N	\N	getSessionValDesktops	1302	1303
358	354	\N	\N	getSessionInitDesktops	713	714
359	354	\N	\N	getSessionValDesktops	715	716
360	354	\N	\N	getSessionValeditDesktops	717	718
361	354	\N	\N	getSessionDispDesktops	719	720
362	354	\N	\N	getSessionArchDesktops	721	722
363	354	\N	\N	getSessionAllDesktops	723	724
364	354	\N	\N	getEnv	725	726
365	354	\N	\N	getAddParams	727	728
354	1	\N	\N	Keywordtypes	706	731
366	354	\N	\N	ldebug	729	730
368	367	\N	\N	index	733	734
369	367	\N	\N	getMetadonnees	735	736
423	421	\N	\N	getRepertoires	843	844
370	367	\N	\N	add	737	738
371	367	\N	\N	edit	739	740
372	367	\N	\N	delete	741	742
373	367	\N	\N	userAclCheck	743	744
374	367	\N	\N	getSessionInitServices	745	746
375	367	\N	\N	getSessionAppDefinedDesktops	747	748
376	367	\N	\N	getSessionInitDesktops	749	750
377	367	\N	\N	getSessionValDesktops	751	752
378	367	\N	\N	getSessionValeditDesktops	753	754
379	367	\N	\N	getSessionDispDesktops	755	756
380	367	\N	\N	getSessionArchDesktops	757	758
381	367	\N	\N	getSessionAllDesktops	759	760
382	367	\N	\N	getEnv	761	762
383	367	\N	\N	getAddParams	763	764
421	1	\N	\N	Repertoires	840	885
367	1	\N	\N	Metadonnees	732	767
384	367	\N	\N	ldebug	765	766
386	385	\N	\N	index	769	770
387	385	\N	\N	formulaire	771	772
388	385	\N	\N	getRecherches	773	774
389	385	\N	\N	delete	775	776
390	385	\N	\N	recherche	777	778
391	385	\N	\N	exportcsv	779	780
392	385	\N	\N	userAclCheck	781	782
393	385	\N	\N	getSessionInitServices	783	784
394	385	\N	\N	getSessionAppDefinedDesktops	785	786
395	385	\N	\N	getSessionInitDesktops	787	788
396	385	\N	\N	getSessionValDesktops	789	790
397	385	\N	\N	getSessionValeditDesktops	791	792
398	385	\N	\N	getSessionDispDesktops	793	794
399	385	\N	\N	getSessionArchDesktops	795	796
400	385	\N	\N	getSessionAllDesktops	797	798
401	385	\N	\N	getEnv	799	800
402	385	\N	\N	getAddParams	801	802
385	1	\N	\N	Recherches	768	805
403	385	\N	\N	ldebug	803	804
405	404	\N	\N	add	807	808
406	404	\N	\N	delete	809	810
407	404	\N	\N	download	811	812
408	404	\N	\N	getPreview	813	814
409	404	\N	\N	userAclCheck	815	816
410	404	\N	\N	getSessionInitServices	817	818
411	404	\N	\N	getSessionAppDefinedDesktops	819	820
412	404	\N	\N	getSessionInitDesktops	821	822
413	404	\N	\N	getSessionValDesktops	823	824
414	404	\N	\N	getSessionValeditDesktops	825	826
415	404	\N	\N	getSessionDispDesktops	827	828
416	404	\N	\N	getSessionArchDesktops	829	830
417	404	\N	\N	getSessionAllDesktops	831	832
418	404	\N	\N	getEnv	833	834
419	404	\N	\N	getAddParams	835	836
404	1	\N	\N	Relements	806	839
420	404	\N	\N	ldebug	837	838
422	421	\N	\N	index	841	842
654	639	\N	\N	getSessionValeditDesktops	1304	1305
655	639	\N	\N	getSessionDispDesktops	1306	1307
656	639	\N	\N	getSessionArchDesktops	1308	1309
657	639	\N	\N	getSessionAllDesktops	1310	1311
658	639	\N	\N	getEnv	1312	1313
659	639	\N	\N	getAddParams	1314	1315
639	614	\N	\N	Compositions	1275	1318
660	639	\N	\N	ldebug	1316	1317
662	661	\N	\N	index	1320	1321
663	661	\N	\N	view	1322	1323
664	661	\N	\N	add	1324	1325
665	661	\N	\N	edit	1326	1327
666	661	\N	\N	delete	1328	1329
667	661	\N	\N	moveUp	1330	1331
668	661	\N	\N	moveDown	1332	1333
669	661	\N	\N	setCreatedModifiedUser	1334	1335
670	661	\N	\N	formatUser	1336	1337
671	661	\N	\N	formatLinkedModel	1338	1339
672	661	\N	\N	listLinkedModel	1340	1341
673	661	\N	\N	userAclCheck	1342	1343
674	661	\N	\N	getSessionInitServices	1344	1345
675	661	\N	\N	getSessionAppDefinedDesktops	1346	1347
676	661	\N	\N	getSessionInitDesktops	1348	1349
677	661	\N	\N	getSessionValDesktops	1350	1351
678	661	\N	\N	getSessionValeditDesktops	1352	1353
679	661	\N	\N	getSessionDispDesktops	1354	1355
680	661	\N	\N	getSessionArchDesktops	1356	1357
681	661	\N	\N	getSessionAllDesktops	1358	1359
682	661	\N	\N	getEnv	1360	1361
683	661	\N	\N	getAddParams	1362	1363
661	614	\N	\N	Etapes	1319	1366
684	661	\N	\N	ldebug	1364	1365
686	685	\N	\N	visuTraitement	1368	1369
687	685	\N	\N	setCreatedModifiedUser	1370	1371
688	685	\N	\N	formatUser	1372	1373
689	685	\N	\N	formatLinkedModel	1374	1375
690	685	\N	\N	listLinkedModel	1376	1377
691	685	\N	\N	userAclCheck	1378	1379
692	685	\N	\N	getSessionInitServices	1380	1381
693	685	\N	\N	getSessionAppDefinedDesktops	1382	1383
694	685	\N	\N	getSessionInitDesktops	1384	1385
695	685	\N	\N	getSessionValDesktops	1386	1387
696	685	\N	\N	getSessionValeditDesktops	1388	1389
697	685	\N	\N	getSessionDispDesktops	1390	1391
698	685	\N	\N	getSessionArchDesktops	1392	1393
699	685	\N	\N	getSessionAllDesktops	1394	1395
700	685	\N	\N	getEnv	1396	1397
701	685	\N	\N	getAddParams	1398	1399
614	1	\N	\N	Cakeflow	1226	1403
685	614	\N	\N	Traitements	1367	1402
702	685	\N	\N	ldebug	1400	1401
703	1	\N	\N	DataAcl	1404	1405
706	705	\N	\N	history_state	1408	1409
707	705	\N	\N	sql_explain	1410	1411
708	705	\N	\N	userAclCheck	1412	1413
709	705	\N	\N	getSessionInitServices	1414	1415
710	705	\N	\N	getSessionAppDefinedDesktops	1416	1417
711	705	\N	\N	getSessionInitDesktops	1418	1419
712	705	\N	\N	getSessionValDesktops	1420	1421
713	705	\N	\N	getSessionValeditDesktops	1422	1423
714	705	\N	\N	getSessionDispDesktops	1424	1425
715	705	\N	\N	getSessionArchDesktops	1426	1427
716	705	\N	\N	getSessionAllDesktops	1428	1429
717	705	\N	\N	getEnv	1430	1431
718	705	\N	\N	getAddParams	1432	1433
704	1	\N	\N	DebugKit	1406	1437
705	704	\N	\N	ToolbarAccess	1407	1436
719	705	\N	\N	ldebug	1434	1435
722	721	\N	\N	index	1440	1441
723	721	\N	\N	retrieve	1442	1443
724	721	\N	\N	getMail	1444	1445
725	721	\N	\N	getMailPart	1446	1447
726	721	\N	\N	userAclCheck	1448	1449
727	721	\N	\N	getSessionInitServices	1450	1451
728	721	\N	\N	getSessionAppDefinedDesktops	1452	1453
729	721	\N	\N	getSessionInitDesktops	1454	1455
730	721	\N	\N	getSessionValDesktops	1456	1457
731	721	\N	\N	getSessionValeditDesktops	1458	1459
732	721	\N	\N	getSessionDispDesktops	1460	1461
733	721	\N	\N	getSessionArchDesktops	1462	1463
734	721	\N	\N	getSessionAllDesktops	1464	1465
735	721	\N	\N	getEnv	1466	1467
736	721	\N	\N	getAddParams	1468	1469
720	1	\N	\N	Imap	1438	1473
721	720	\N	\N	Imaps	1439	1472
737	721	\N	\N	ldebug	1470	1471
738	1	\N	\N	Jsonmsg	1474	1475
739	1	\N	\N	Pgsqlcake	1476	1477
1	\N	\N	\N	controllers	1	1480
740	1	\N	\N	SimpleComment	1478	1479
\.


--
-- Name: acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('acos_id_seq', 740, true);


--
-- Data for Name: addressbooks; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY addressbooks (id, name, description, created, modified, foreign_key, active) FROM stdin;
1	Carnet principal	\\x4361726e6574207072696e636970616c	2014-03-14 14:15:33.519019	2014-03-14 14:15:33.519019	\N	t
\.


--
-- Name: addressbooks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('addressbooks_id_seq', 2, true);


--
-- Data for Name: dossiers; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY dossiers (id, name, comment, created, modified) FROM stdin;
\.


--
-- Data for Name: affaires; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY affaires (id, name, comment, dossier_id, created, modified) FROM stdin;
\.


--
-- Name: affaires_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('affaires_id_seq', 1, true);


--
-- Data for Name: sequences; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY sequences (id, nom, commentaire, num_sequence, created, modified) FROM stdin;
\.


--
-- Data for Name: compteurs; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY compteurs (id, nom, commentaire, def_compteur, sequence_id, def_reinit, created, modified, val_reinit) FROM stdin;
\.


--
-- Data for Name: types; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY types (id, name, created, modified) FROM stdin;
\.


--
-- Data for Name: soustypes; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY soustypes (id, name, created, modified, type_id, circuit_id, entrant, delai_nb, delai_unite, lft, rght, parent_id, valeuremail, compteur_id) FROM stdin;
\.


--
-- Data for Name: armodels; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY armodels (id, name, gedpath, content, created, modified, soustype_id, sms_content, email_content, email_title, format, mime, ext, size, preview) FROM stdin;
\.


--
-- Name: armodels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('armodels_id_seq', 1, true);


--
-- Data for Name: aros; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY aros (id, parent_id, model, foreign_key, alias, lft, rght) FROM stdin;
1	\N	Group	1	Superadmin	1	2
2	\N	Group	2	Admin	3	6
3	\N	Group	3	Initiateur	7	8
4	\N	Group	4	Valideur	9	10
5	\N	Group	5	Valideur Editeur	11	12
6	\N	Group	6	Documentaliste	13	14
7	\N	Group	7	Aiguilleur	15	16
\.


--
-- Data for Name: aros_acos; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY aros_acos (id, aro_id, aco_id, _create, _read, _update, _delete) FROM stdin;
1	7	281	1	1	1	1
2	7	289	1	1	1	1
3	7	287	1	1	1	1
4	7	284	1	1	1	1
5	7	285	1	1	1	1
6	7	286	1	1	1	1
7	7	421	1	1	1	1
8	7	385	1	1	1	1
9	7	504	1	1	1	1
10	7	227	1	1	1	1
11	7	107	1	1	1	1
12	7	186	1	1	1	1
13	7	185	1	1	1	1
14	7	550	1	1	1	1
15	7	551	1	1	1	1
16	7	548	1	1	1	1
17	7	549	1	1	1	1
18	7	545	1	1	1	1
19	7	556	1	1	1	1
20	7	200	1	1	1	1
21	7	177	1	1	1	1
22	7	176	1	1	1	1
23	7	175	1	1	1	1
24	7	205	1	1	1	1
25	7	245	1	1	1	1
26	7	246	1	1	1	1
27	7	247	1	1	1	1
28	7	244	1	1	1	1
29	7	248	1	1	1	1
30	7	207	-1	-1	-1	-1
31	7	202	-1	-1	-1	-1
32	7	206	-1	-1	-1	-1
33	7	404	-1	-1	-1	-1
34	7	448	-1	-1	-1	-1
35	7	54	-1	-1	-1	-1
36	7	71	-1	-1	-1	-1
37	7	447	-1	-1	-1	-1
38	7	203	-1	-1	-1	-1
39	7	204	-1	-1	-1	-1
40	7	67	-1	-1	-1	-1
41	7	53	-1	-1	-1	-1
42	7	208	-1	-1	-1	-1
43	7	199	1	1	1	1
44	7	188	-1	-1	-1	-1
45	7	179	-1	-1	-1	-1
46	7	193	-1	-1	-1	-1
47	7	198	-1	-1	-1	-1
48	7	196	-1	-1	-1	-1
49	7	197	-1	-1	-1	-1
50	7	194	-1	-1	-1	-1
51	7	201	-1	-1	-1	-1
52	7	195	-1	-1	-1	-1
53	7	192	-1	-1	-1	-1
54	7	178	1	1	1	1
55	7	155	1	1	1	1
56	7	161	1	1	1	1
57	7	159	1	1	1	1
58	7	160	1	1	1	1
59	7	137	1	1	1	1
60	7	138	1	1	1	1
61	7	491	1	1	1	1
62	7	184	1	1	1	1
63	7	181	1	1	1	1
64	7	190	1	1	1	1
65	7	280	-1	-1	-1	-1
66	7	405	-1	-1	-1	-1
67	7	406	-1	-1	-1	-1
68	7	180	-1	-1	-1	-1
69	7	182	-1	-1	-1	-1
70	7	183	-1	-1	-1	-1
71	7	189	-1	-1	-1	-1
72	7	191	-1	-1	-1	-1
73	6	281	1	1	1	1
74	6	289	1	1	1	1
75	6	287	1	1	1	1
76	6	284	1	1	1	1
77	6	285	1	1	1	1
78	6	286	1	1	1	1
79	6	421	1	1	1	1
80	6	385	1	1	1	1
81	6	504	1	1	1	1
82	6	227	1	1	1	1
83	6	107	1	1	1	1
84	6	186	1	1	1	1
85	6	185	1	1	1	1
86	6	550	1	1	1	1
87	6	551	1	1	1	1
88	6	548	1	1	1	1
89	6	549	1	1	1	1
90	6	545	1	1	1	1
91	6	556	1	1	1	1
92	6	200	1	1	1	1
93	6	280	1	1	1	1
94	6	175	1	1	1	1
95	6	176	1	1	1	1
96	6	205	-1	-1	-1	-1
97	6	245	1	1	1	1
98	6	246	1	1	1	1
99	6	247	1	1	1	1
100	6	244	1	1	1	1
101	6	248	1	1	1	1
102	6	207	1	1	1	1
103	6	202	1	1	1	1
104	6	206	1	1	1	1
105	6	404	1	1	1	1
106	6	448	1	1	1	1
107	6	54	1	1	1	1
108	6	71	1	1	1	1
109	6	447	1	1	1	1
110	6	203	1	1	1	1
111	6	204	1	1	1	1
112	6	67	1	1	1	1
113	6	53	1	1	1	1
114	6	208	1	1	1	1
115	6	405	1	1	1	1
116	6	406	1	1	1	1
117	6	188	1	1	1	1
118	6	179	1	1	1	1
119	6	193	1	1	1	1
120	6	198	1	1	1	1
121	6	196	1	1	1	1
122	6	197	1	1	1	1
123	6	194	-1	-1	-1	-1
124	6	201	-1	-1	-1	-1
125	6	195	1	1	1	1
126	6	192	1	1	1	1
127	6	178	1	1	1	1
128	6	155	-1	-1	-1	-1
129	6	161	-1	-1	-1	-1
130	6	159	-1	-1	-1	-1
131	6	160	-1	-1	-1	-1
132	6	137	-1	-1	-1	-1
133	6	138	-1	-1	-1	-1
134	6	491	-1	-1	-1	-1
135	6	180	1	1	1	1
136	6	189	1	1	1	1
137	6	181	1	1	1	1
138	6	190	1	1	1	1
139	6	182	1	1	1	1
140	6	191	1	1	1	1
141	6	183	-1	-1	-1	-1
142	6	184	-1	-1	-1	-1
143	5	281	1	1	1	1
144	5	289	1	1	1	1
145	5	287	1	1	1	1
146	5	284	1	1	1	1
147	5	285	1	1	1	1
148	5	286	1	1	1	1
149	5	421	1	1	1	1
150	5	385	1	1	1	1
151	5	504	1	1	1	1
152	5	227	1	1	1	1
153	5	107	1	1	1	1
154	5	186	1	1	1	1
155	5	185	1	1	1	1
156	5	550	1	1	1	1
157	5	551	1	1	1	1
158	5	548	1	1	1	1
159	5	549	1	1	1	1
160	5	545	1	1	1	1
161	5	556	1	1	1	1
162	5	200	1	1	1	1
163	5	280	1	1	1	1
164	5	175	1	1	1	1
165	5	176	1	1	1	1
166	5	205	-1	-1	-1	-1
167	5	245	1	1	1	1
168	5	246	1	1	1	1
169	5	247	1	1	1	1
170	5	244	1	1	1	1
171	5	248	1	1	1	1
172	5	207	1	1	1	1
173	5	202	1	1	1	1
174	5	206	1	1	1	1
175	5	404	1	1	1	1
176	5	448	1	1	1	1
177	5	54	1	1	1	1
178	5	71	1	1	1	1
179	5	447	1	1	1	1
180	5	203	1	1	1	1
181	5	204	1	1	1	1
182	5	67	1	1	1	1
183	5	53	1	1	1	1
184	5	208	1	1	1	1
185	5	405	1	1	1	1
186	5	406	1	1	1	1
187	5	188	1	1	1	1
188	5	179	1	1	1	1
189	5	193	1	1	1	1
190	5	198	1	1	1	1
191	5	196	1	1	1	1
192	5	197	1	1	1	1
193	5	194	-1	-1	-1	-1
194	5	201	-1	-1	-1	-1
195	5	195	1	1	1	1
196	5	192	1	1	1	1
197	5	178	1	1	1	1
198	5	155	1	1	1	1
199	5	161	1	1	1	1
200	5	159	1	1	1	1
201	5	160	1	1	1	1
202	5	137	1	1	1	1
203	5	138	1	1	1	1
204	5	491	1	1	1	1
205	5	180	1	1	1	1
206	5	189	1	1	1	1
207	5	181	1	1	1	1
208	5	190	1	1	1	1
209	5	182	1	1	1	1
210	5	184	1	1	1	1
211	5	183	-1	-1	-1	-1
212	5	191	-1	-1	-1	-1
213	4	281	1	1	1	1
214	4	289	1	1	1	1
215	4	287	1	1	1	1
216	4	284	1	1	1	1
217	4	285	1	1	1	1
218	4	286	1	1	1	1
219	4	421	1	1	1	1
220	4	385	1	1	1	1
221	4	504	1	1	1	1
222	4	227	1	1	1	1
223	4	107	1	1	1	1
224	4	186	1	1	1	1
225	4	185	1	1	1	1
226	4	550	1	1	1	1
227	4	551	1	1	1	1
228	4	548	1	1	1	1
229	4	549	1	1	1	1
230	4	545	1	1	1	1
231	4	556	1	1	1	1
232	4	200	1	1	1	1
233	4	280	1	1	1	1
234	4	175	1	1	1	1
235	4	176	1	1	1	1
236	4	205	-1	-1	-1	-1
237	4	245	1	1	1	1
238	4	246	1	1	1	1
239	4	247	1	1	1	1
240	4	244	1	1	1	1
241	4	248	1	1	1	1
242	4	207	1	1	1	1
243	4	202	1	1	1	1
244	4	206	1	1	1	1
245	4	404	1	1	1	1
246	4	448	1	1	1	1
247	4	54	1	1	1	1
248	4	71	1	1	1	1
249	4	447	1	1	1	1
250	4	203	1	1	1	1
251	4	204	1	1	1	1
252	4	67	1	1	1	1
253	4	53	1	1	1	1
254	4	208	1	1	1	1
255	4	405	1	1	1	1
256	4	406	1	1	1	1
257	4	188	1	1	1	1
258	4	179	1	1	1	1
259	4	193	1	1	1	1
260	4	198	1	1	1	1
261	4	196	1	1	1	1
262	4	197	1	1	1	1
263	4	194	-1	-1	-1	-1
264	4	201	-1	-1	-1	-1
265	4	195	1	1	1	1
266	4	192	1	1	1	1
267	4	178	1	1	1	1
268	4	155	-1	-1	-1	-1
269	4	161	-1	-1	-1	-1
270	4	159	-1	-1	-1	-1
271	4	160	-1	-1	-1	-1
272	4	137	-1	-1	-1	-1
273	4	138	-1	-1	-1	-1
274	4	491	-1	-1	-1	-1
275	4	180	1	1	1	1
276	4	189	1	1	1	1
277	4	181	1	1	1	1
278	4	190	1	1	1	1
279	4	182	1	1	1	1
280	4	183	-1	-1	-1	-1
281	4	184	-1	-1	-1	-1
282	4	191	-1	-1	-1	-1
283	3	281	1	1	1	1
284	3	289	1	1	1	1
285	3	287	1	1	1	1
286	3	284	1	1	1	1
287	3	285	1	1	1	1
288	3	286	1	1	1	1
289	3	421	1	1	1	1
290	3	385	1	1	1	1
291	3	504	1	1	1	1
292	3	227	1	1	1	1
293	3	107	1	1	1	1
294	3	186	1	1	1	1
295	3	185	1	1	1	1
296	3	550	1	1	1	1
297	3	551	1	1	1	1
298	3	548	1	1	1	1
299	3	549	1	1	1	1
300	3	545	1	1	1	1
301	3	556	1	1	1	1
302	3	200	1	1	1	1
303	3	177	1	1	1	1
304	3	175	1	1	1	1
305	3	176	1	1	1	1
306	3	205	-1	-1	-1	-1
307	3	245	1	1	1	1
308	3	246	1	1	1	1
309	3	247	1	1	1	1
310	3	244	1	1	1	1
311	3	248	1	1	1	1
312	3	207	1	1	1	1
313	3	202	1	1	1	1
314	3	206	1	1	1	1
315	3	404	1	1	1	1
316	3	448	1	1	1	1
317	3	54	1	1	1	1
318	3	71	1	1	1	1
319	3	447	1	1	1	1
320	3	203	1	1	1	1
321	3	204	1	1	1	1
322	3	67	1	1	1	1
323	3	53	1	1	1	1
324	3	208	1	1	1	1
325	3	405	1	1	1	1
326	3	406	1	1	1	1
327	3	188	1	1	1	1
328	3	179	1	1	1	1
329	3	193	-1	-1	-1	-1
330	3	198	-1	-1	-1	-1
331	3	196	1	1	1	1
332	3	197	-1	-1	-1	-1
333	3	194	1	1	1	1
334	3	201	1	1	1	1
335	3	195	-1	-1	-1	-1
336	3	192	-1	-1	-1	-1
337	3	210	1	1	1	1
338	3	183	1	1	1	1
339	3	155	1	1	1	1
340	3	161	1	1	1	1
341	3	159	1	1	1	1
342	3	160	1	1	1	1
343	3	137	1	1	1	1
344	3	138	1	1	1	1
345	3	491	1	1	1	1
346	3	178	1	1	1	1
347	3	184	1	1	1	1
348	3	181	1	1	1	1
349	3	190	1	1	1	1
350	3	280	-1	-1	-1	-1
351	3	180	-1	-1	-1	-1
352	3	182	-1	-1	-1	-1
353	3	189	-1	-1	-1	-1
354	3	191	-1	-1	-1	-1
355	2	20	1	1	1	1
356	2	279	1	1	1	1
357	2	84	1	1	1	1
358	2	592	1	1	1	1
359	2	302	1	1	1	1
360	2	541	1	1	1	1
361	2	174	1	1	1	1
362	2	243	1	1	1	1
363	2	50	1	1	1	1
364	2	444	1	1	1	1
365	2	404	1	1	1	1
366	2	67	1	1	1	1
367	2	461	1	1	1	1
368	2	482	1	1	1	1
369	2	525	1	1	1	1
370	2	614	1	1	1	1
371	2	504	1	1	1	1
372	2	367	1	1	1	1
373	2	261	1	1	1	1
374	2	34	1	1	1	1
375	2	385	-1	-1	-1	-1
376	2	341	1	1	1	1
377	2	324	1	1	1	1
378	2	354	1	1	1	1
379	2	153	1	1	1	1
380	2	136	1	1	1	1
381	2	2	1	1	1	1
382	2	223	1	1	1	1
383	2	178	1	1	1	1
384	2	155	1	1	1	1
385	2	161	1	1	1	1
386	2	159	1	1	1	1
387	2	160	1	1	1	1
388	2	137	1	1	1	1
389	2	138	1	1	1	1
390	2	491	1	1	1	1
391	2	184	1	1	1	1
392	2	183	1	1	1	1
393	2	280	-1	-1	-1	-1
394	2	200	-1	-1	-1	-1
395	2	196	-1	-1	-1	-1
396	2	197	-1	-1	-1	-1
397	2	245	-1	-1	-1	-1
398	2	246	-1	-1	-1	-1
399	2	244	-1	-1	-1	-1
400	2	206	-1	-1	-1	-1
401	2	405	-1	-1	-1	-1
402	2	406	-1	-1	-1	-1
403	2	202	-1	-1	-1	-1
\.


--
-- Name: aros_acos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('aros_acos_id_seq', 403, true);


--
-- Name: aros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('aros_id_seq', 8, true);


--
-- Data for Name: contacts; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY contacts (id, name, civilite, nom, prenom, created, modified, addressbook_id, foreign_key, active, citoyen, slug) FROM stdin;
\.


--
-- Data for Name: contactinfos; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY contactinfos (id, name, civilite, nom, prenom, email, adresse, compl, cp, ville, region, pays, tel, role, organisation, active, created, modified, contact_id, slug, numvoie, typevoie, nomvoie) FROM stdin;
\.


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY groups (id, name, created, modified, readonly, active, group_id) FROM stdin;
1	Superadmin	2012-02-17 14:55:14.842819	2012-02-17 14:55:14.842819	t	t	\N
2	Admin	2012-02-17 15:15:43.236166	2012-02-17 15:15:43.236166	t	t	\N
3	Initiateur	2012-02-17 15:15:43.260401	2012-02-17 15:15:43.260401	t	t	\N
4	Valideur	2012-02-17 15:15:43.277671	2012-02-17 15:15:43.277671	t	t	\N
5	Valideur Editeur	2012-02-17 15:15:43.296607	2012-02-17 15:15:43.296607	t	t	\N
6	Documentaliste	2012-02-17 15:15:43.310943	2012-02-17 15:15:43.310943	t	t	\N
7	Aiguilleur	2012-07-06 11:51:30.769035	2012-07-06 11:51:30.769035	t	t	\N
\.


--
-- Data for Name: desktops; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY desktops (name, group_id, created, modified, id, active) FROM stdin;
\.


--
-- Data for Name: services; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY services (name, created, modified, parent_id, lft, rght, id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY users (id, username, password, nom, prenom, created, modified, mail, desktop_id, last_login, last_logout, active, pwdmodified, numtel) FROM stdin;
\.


--
-- Data for Name: courriers; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY courriers (id, name, fichier_associe, objet, ref_ancien, soustype_id, lft, rght, parent_id, affaire_id, adressemail, wav_associe, video_associe, numtelsms, messagesms, reference, fichier_genere, priorite, user_creator_id, desktop_creator_id, created, modified, date, datereception, direction, contactinfo_id, service_creator_id, delai_nb, delai_unite, affairesuiviepar_id) FROM stdin;
\.


--
-- Data for Name: ars; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY ars (id, name, gedpath, content, created, modified, courrier_id, format, sms_dest, sms_content, email_title, email_content, ext, mime, used, size, email_dest, preview) FROM stdin;
\.


--
-- Name: ars_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('ars_id_seq', 1, true);


--
-- Data for Name: bannettes; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY bannettes (id, name) FROM stdin;
1	docs
2	refus
3	reponses
4	flux
5	aiguillage
6	infos
7	copy
\.


--
-- Data for Name: bancontenus; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY bancontenus (id, bannette_id, courrier_id, created, modified, etat, desktop_id, read) FROM stdin;
\.


--
-- Name: bancontenus_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('bancontenus_id_seq', 1, true);


--
-- Name: bannettes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('bannettes_id_seq', 8, true);


--
-- Data for Name: comments; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY comments (id, slug, content, private, owner_id, target_id, created, modified) FROM stdin;
\.


--
-- Name: comments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('comments_id_seq', 1, true);


--
-- Data for Name: comments_readers; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY comments_readers (id, reader_id, comment_id, created, modified) FROM stdin;
\.


--
-- Name: comments_readers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('comments_readers_id_seq', 1, true);


--
-- Name: compteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('compteurs_id_seq', 1, true);


--
-- Data for Name: connecteurs; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY connecteurs (id, name, created, modified) FROM stdin;
1	Parapheur électronique (Signature)	2014-08-05 13:53:10.780508	2014-08-05 13:53:10.780508
\.


--
-- Name: connecteurs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('connecteurs_id_seq', 2, true);


--
-- Name: contactinfos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('contactinfos_id_seq', 1, true);


--
-- Name: contacts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('contacts_id_seq', 1, true);


--
-- Name: courriers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('courriers_id_seq', 1, true);


--
-- Data for Name: typemetadonnees; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY typemetadonnees (id, name, created, modified) FROM stdin;
1	date	2012-02-17 14:55:14.779645	2012-02-17 14:55:14.779645
2	texte	2012-02-17 14:55:14.811863	2012-02-17 14:55:14.811863
3	booléen	2012-02-17 14:55:14.825279	2012-02-17 14:55:14.825279
4	select	2014-05-16 10:06:15.491712	2014-05-16 10:06:15.491712
\.


--
-- Data for Name: metadonnees; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY metadonnees (id, name, typemetadonnee_id, created, modified) FROM stdin;
\.


--
-- Data for Name: courriers_metadonnees; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY courriers_metadonnees (id, valeur, created, modified, courrier_id, metadonnee_id) FROM stdin;
\.


--
-- Name: courriers_metadonnees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('courriers_metadonnees_id_seq', 1, true);


--
-- Name: courriers_reference_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('courriers_reference_seq', 45, true);


--
-- Data for Name: repertoires; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY repertoires (id, name, user_id, lft, rght, parent_id) FROM stdin;
\.


--
-- Data for Name: courriers_repertoires; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY courriers_repertoires (id, courrier_id, repertoire_id) FROM stdin;
\.


--
-- Name: courriers_repertoires_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('courriers_repertoires_id_seq', 1, true);


--
-- Data for Name: dacos; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY dacos (id, parent_id, model, foreign_key, alias, lft, rght) FROM stdin;
1	\N	\N	\N	data	1	2
\.


--
-- Name: dacos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('dacos_id_seq', 2, true);


--
-- Data for Name: daros; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY daros (id, parent_id, model, foreign_key, alias, lft, rght) FROM stdin;
\.


--
-- Data for Name: daros_dacos; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY daros_dacos (id, daro_id, daco_id, _create, _read, _update, _delete) FROM stdin;
\.


--
-- Name: daros_dacos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('daros_dacos_id_seq', 1, true);


--
-- Name: daros_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('daros_id_seq', 1, true);


--
-- Name: desktops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('desktops_id_seq', 1, true);


--
-- Data for Name: desktops_services; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY desktops_services (id, service_id, desktop_id, created, modified) FROM stdin;
\.


--
-- Name: desktops_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('desktops_services_id_seq', 1, true);


--
-- Data for Name: taches; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY taches (id, courrier_id, created, modified, name, delai_nb, delai_unite, statut, act_desktop_id) FROM stdin;
\.


--
-- Data for Name: desktops_taches; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY desktops_taches (id, desktop_id, tache_id, created, modified) FROM stdin;
\.


--
-- Name: desktops_taches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('desktops_taches_id_seq', 1, true);


--
-- Data for Name: desktops_users; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY desktops_users (id, user_id, desktop_id, delegation, created, modified) FROM stdin;
\.


--
-- Name: desktops_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('desktops_users_id_seq', 1, true);


--
-- Data for Name: documents; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY documents (id, name, gedpath, content, size, mime, preview, created, modified, courrier_id, ext, main_doc) FROM stdin;
\.


--
-- Name: documents_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('documents_id_seq', 1, true);


--
-- Name: dossiers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('dossiers_id_seq', 1, true);


--
-- Data for Name: formatsreponse; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY formatsreponse (id, name) FROM stdin;
1	Modèle de document
2	Fichier son (.wav)
3	Fichier vidéo (.mpeg)
4	SMS
5	Email
\.


--
-- Name: formatsreponse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('formatsreponse_id_seq', 6, true);


--
-- Data for Name: formatsreponse_soustypes; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY formatsreponse_soustypes (id, formatreponse_id, soustype_id, created, modified) FROM stdin;
\.


--
-- Name: formatsreponse_soustypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('formatsreponse_soustypes_id_seq', 1, true);


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('groups_id_seq', 8, true);


--
-- Data for Name: inits_services; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY inits_services (id, desktop_id, service_id, created, modified) FROM stdin;
\.


--
-- Name: inits_services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('inits_services_id_seq', 1, true);


--
-- Data for Name: keywordtypes; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY keywordtypes (id, code, documentation, created, modified) FROM stdin;
1	corpname	Collectivité	2012-02-28 11:38:20.054222	2012-02-28 11:38:20.054222
2	famname	Nom de famille	2012-02-28 11:38:20.088158	2012-02-28 11:38:20.088158
3	geogname	Nom géographique	2012-02-28 11:38:20.096135	2012-02-28 11:38:20.096135
4	name	Nom	2012-02-28 11:38:20.105176	2012-02-28 11:38:20.105176
5	occupation	Fonction	2012-02-28 11:38:20.113056	2012-02-28 11:38:20.113056
6	persname	Nom de personne	2012-02-28 11:38:20.12103	2012-02-28 11:38:20.12103
7	subject	Mot-matière	2012-02-28 11:38:20.129994	2012-02-28 11:38:20.129994
8	genreform	Type de document	2012-02-28 11:38:20.138126	2012-02-28 11:38:20.138126
9	function	Activité	2012-02-28 11:38:20.146106	2012-02-28 11:38:20.146106
\.


--
-- Data for Name: keywordlists; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY keywordlists (id, nom, description, version, active, keywordtype_id, scheme_id, scheme_name, scheme_agency_name, scheme_version_id, scheme_data_uri, scheme_uri, created_user_id, modified_user_id, created, modified) FROM stdin;
1	Thésaurus-matières	\\x5365727669636520696e7465726d696e697374c3a97269656c20646573204172636869766573206465204672616e6365203a206465736372697074696f6e20647520766f636162756c61697265	1	t	7	THESAURUS-MATIERES	Thésaurus-matières	Service interministériel des Archives de France	2011-04-19	http://www.archivesdefrance.culture.gouv.fr/thesaurus/data/Matiere?includeSchemes=true	http://www.archivesdefrance.culture.gouv.fr/thesaurus/resource/Matiere	1	1	2011-05-25 10:02:54	2011-05-25 10:02:54
3	Liste d'autorité « Contexte historique »	\\x5365727669636520696e7465726d696e697374c3a97269656c20646573204172636869766573206465204672616e6365203a20766f636162756c6169726520636f6e7472c3b46cc3a92064657320636f6e74657874657320686973746f726971756573	1	t	7	LISTE D'AUTORITE « CONTEXTE HISTORIQUE »	Liste d'autorité « Contexte historique »	Service interministériel des Archives de France	2010-04-21	http://www.archivesdefrance.culture.gouv.fr/thesaurus/data/Contexte?includeSchemes=true	http://www.archivesdefrance.culture.gouv.fr/thesaurus/resource/Contexte	1	1	2011-05-25 10:09:29	2011-05-25 10:09:29
4	Liste d'autorités « Typologie documentaire »	\\x5365727669636520696e7465726d696e697374c3a97269656c20646573204172636869766573206465204672616e6365203a20766f636162756c6169726520636f6e7472c3b46cc3a9206465206c61207479706f6c6f67696520646f63756d656e74616972652064616e73206c6573206172636869766573	1	t	7	LISTE D'AUTORITES « TYPOLOGIE DOCUMENTAIRE »	Liste d'autorités « Typologie documentaire »	Service interministériel des Archives de France	2011-04-18	http://www.archivesdefrance.culture.gouv.fr/thesaurus/data/Typologie?includeSchemes=true	http://www.archivesdefrance.culture.gouv.fr/thesaurus/resource/Typologie	1	1	2011-05-25 10:13:13	2011-05-25 10:13:13
2	Liste d'autorités « Actions »	\\x5365727669636520696e7465726d696e697374c3a97269656c20646573204172636869766573206465204672616e6365203a20766f636162756c6169726520636f6e7472c3b46cc3a92064657320616374696f6e732064616e73206c6573206172636869766573	1	t	7	LISTE D'AUTORITES « ACTIONS »	Liste d'autorités « Actions »	Service interministériel des Archives de France	2011-04-07	http://www.archivesdefrance.culture.gouv.fr/thesaurus/data/Actions?includeSchemes=true	http://www.archivesdefrance.culture.gouv.fr/thesaurus/resource/Actions	1	1	2011-05-25 10:06:25	2011-05-25 10:14:22
\.


--
-- Name: keywordlists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('keywordlists_id_seq', 5, true);


--
-- Data for Name: keywords; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY keywords (id, version, code, libelle, parent_id, lft, rght, keywordlist_id, created_user_id, modified_user_id, created, modified) FROM stdin;
2	1	T1-543	\\xc3a9636f6e6f6d6965	\N	3	4	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
1814	1	T3-224	\\x7461726966	\N	3627	3628	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1826	1	T3-221	\\x7374617475742064276173736f63696174696f6e	\N	3651	3652	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
297	1	T1-90	\\x66697363616c6974c3a92070c3a974726f6c69657265	\N	593	594	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
1827	1	T3-51	\\x636172746f6e206427696e7669746174696f6e	\N	3653	3654	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
424	1	T1-1219	\\x696d6d6575626c65206465206772616e64652068617574657572	\N	847	848	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
620	1	T1-1173	\\x6c6f69736972	\N	1239	1240	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
1126	1	T1-743	\\x76696520706173746f72616c65	\N	2251	2252	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1777	1	T3-150	\\x6f75767261676520696d7072696dc3a9	\N	3553	3554	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1746	1	T3-44	\\x6361727465206427c3a96c656374657572	\N	3491	3492	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1327	1	T1-1440	\\x766f6c61696c6c65	\N	2653	2654	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
546	1	T1-1241	\\x73616c6c6520646520737065637461636c6573	\N	1091	1092	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
82	1	T1-21	\\x63c3a972c3a96d6f6e6965207075626c69717565	\N	163	164	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
579	1	T1-237	\\x61756265726765206465206a65756e65737365	\N	1157	1158	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
372	1	T1-484	\\x70617472696d6f696e6520736369656e7469666971756520657420746563686e69717565	\N	743	744	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
250	1	T1-80	\\x6469736369706c696e652070c3a96e6974656e746961697265	\N	499	500	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
218	1	T1-284	\\x696e63656e646965	\N	435	436	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
219	1	T1-698	\\x636174617374726f706865206e61747572656c6c65	\N	437	438	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
701	1	T1-325	\\x666f72c3aa7420636f6d6d756e616c65	\N	1401	1402	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
889	1	T1-1012	\\x6173737572616e6365206d617465726e6974c3a9	\N	1777	1778	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
261	1	T1-85	\\x656d706c6f6920c3a02074656d7073207061727469656c	\N	521	522	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
302	1	T1-1315	\\xc3a96e65726769652072656e6f7576656c61626c65	\N	603	604	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
373	1	T1-226	\\x6f6575767265206427617274	\N	745	746	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
162	1	T1-892	\\x6a75737469636520636976696c65	\N	323	324	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
301	1	T1-92	\\xc3a96e65726769652067c3a96f746865726d69717565	\N	601	602	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
1569	1	T4-68	\\x6d6f7576656d656e7420646573204c756d69c3a8726573	\N	3137	3138	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
750	1	T1-357	\\x73747275637475726520636f6d6d756e616c652064276169646520736f6369616c65	\N	1499	1500	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
477	1	T1-1264	\\x73c3a96375726974c3a9206475207472617661696c	\N	953	954	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
486	1	T1-182	\\x64c3a963656e7472616c69736174696f6e20696e647573747269656c6c65	\N	971	972	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
970	1	T1-514	\\x617274206472616d617469717565	\N	1939	1940	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1212	1	T1-792	\\x696e737472756374696f6e2063697669717565	\N	2423	2424	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1114	1	T1-1120	\\x766f6965207069c3a9746f6e6e65	\N	2227	2228	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
240	1	T1-75	\\x666f726d6174696f6e206d75736963616c65	\N	479	480	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
165	1	T1-48	\\x6f757672616765206427617274	\N	329	330	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1816	1	T3-36	\\x62756c6c6574696e20646520766f7465	\N	3631	3632	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
263	1	T1-86	\\xc3a96475636174696f6e20706f70756c61697265	\N	525	526	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
288	1	T1-803	\\x6d6f7576656d656e74206175746f6e6f6d69737465	\N	575	576	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
682	1	T1-297	\\x636972717565	\N	1363	1364	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
1061	1	T1-708	\\x636972636f6e736372697074696f6e2066697363616c65	\N	2121	2122	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1084	1	T1-623	\\x64726f69747320646f6d616e69617578206427416e6369656e2052c3a967696d65	\N	2167	2168	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1110	1	T1-657	\\x6167656e74206e6f6e20746974756c61697265	\N	2219	2220	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
738	1	T1-476	\\xc389746174	\N	1475	1476	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
1288	1	T1-1342	\\x7465727261696e2064652063616d70696e67	\N	2575	2576	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1122	1	T1-1399	\\x707570696c6c65206465206c61206e6174696f6e	\N	2243	2244	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:12
1149	1	T1-701	\\x736f6369c3a974c3a9206427c3a9636f6e6f6d6965206d69787465	\N	2297	2298	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1678	1	T3-170	\\x70726f63c3a8732d76657262616c	\N	3355	3356	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1006	1	T1-627	\\x70696c6f7465	\N	2011	2012	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
711	1	T1-336	\\x617373697374616e636520c3a96475636174697665	\N	1421	1422	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
712	1	T1-824	\\x70726f74656374696f6e206a756469636961697265206465206c61206a65756e65737365	\N	1423	1424	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
723	1	T1-959	\\x626c6f637573	\N	1445	1446	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
725	1	T1-630	\\x706f6c696365206465732066726f6e7469c3a8726573	\N	1449	1450	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
749	1	T1-356	\\x70616c616973206465206a757374696365	\N	1497	1498	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
994	1	T1-1201	\\x6d617269616765	\N	1987	1988	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1140	1	T1-1288	\\x65617520706c757669616c65	\N	2279	2280	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
247	1	T1-78	\\x6d696c6974616e7420706f6c697469717565	\N	493	494	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1530	1	T4-128	\\x7472616974c3a9206465205665727361696c6c657320283139313929	\N	3059	3060	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1300	1	T1-1445	\\x73746174696f6e20726164696f20c3a96c65637472697175652070726976c3a965	\N	2599	2600	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1331	1	T1-1023	\\x67617a206e61747572656c	\N	2661	2662	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
292	1	T1-752	\\x696d7072696d657572	\N	583	584	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
769	1	T1-1283	\\x73c3bb726574c3a9206d6f62696c69c3a87265	\N	1537	1538	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:06
1539	1	T4-97	\\x63726973652070c3a974726f6c69c3a872652028313937332d3139373929	\N	3077	3078	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1812	1	T3-228	\\x74697472652064652063697263756c6174696f6e	\N	3623	3624	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
293	1	T1-1212	\\xc3a9646974657572	\N	585	586	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
1813	1	T3-12	\\x61726d6f7269616c	\N	3625	3626	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1219	1	T1-794	\\x68c3b474656c206465206c6120736f75732d7072c3a966656374757265	\N	2437	2438	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1367	1	T1-1161	\\x636f6d6d616e6465207075626c69717565	\N	2733	2734	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
751	1	T1-358	\\x7465727261696e206d696c697461697265	\N	1501	1502	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
320	1	T1-390	\\x6875697373696572206465206a757374696365	\N	639	640	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
996	1	T1-537	\\x65617520706f7461626c65	\N	1991	1992	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
334	1	T1-860	\\xc3a97472616e676572	\N	667	668	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
669	1	T1-982	\\x6d616c61646965206465732076c3a967c3a974617578	\N	1337	1338	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
823	1	T1-410	\\x64726f69747320646520646f75616e65	\N	1645	1646	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
1201	1	T1-775	\\xc3a96e657267696520736f6c61697265	\N	2401	2402	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1365	1	T1-1451	\\xc3a9746174732070726f76696e6369617578	\N	2729	2730	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1354	1	T1-1142	\\x6163636964656e742073636f6c61697265	\N	2707	2708	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1355	1	T1-1225	\\x62617461696c6c6f6e2073636f6c61697265	\N	2709	2710	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1358	1	T1-1125	\\x66c3aa7465206e6174696f6e616c65	\N	2715	2716	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1326	1	T1-1389	\\x616c696d656e742064752062c3a97461696c	\N	2651	2652	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1266	1	T1-893	\\x7072c3a9736964656e74206427c3a97461626c697373656d656e74207075626c696320646520636f6f70c3a9726174696f6e20696e746572636f6d6d756e616c65	\N	2531	2532	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1292	1	T1-916	\\x696e74657272757074696f6e20766f6c6f6e74616972652064652067726f737365737365	\N	2583	2584	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1376	1	T1-1401	\\x74616fc3af736d65	\N	2751	2752	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1356	1	T1-1122	\\x636f6e7365696c20636f6d6d756e61757461697265	\N	2711	2712	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1360	1	T1-1230	\\x63656e7472652076696c6c65	\N	2719	2720	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1202	1	T1-776	\\x6368616d62726520636f6e73756c61697265	\N	2403	2404	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
766	1	T1-1250	\\x7072c3a9736964656e7420647520636f6e7365696c2067c3a96ec3a972616c	\N	1531	1532	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:06
399	1	T1-135	\\x706f6964732d65742d6d657375726573	\N	797	798	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
1193	1	T1-765	\\x6cc3a96769736c6174696f6e	\N	2385	2386	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1364	1	T1-1156	\\x6661757820656e20c3a963726974757265	\N	2727	2728	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
763	1	T1-364	\\x666175782074c3a96d6f69676e616765	\N	1525	1526	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
999	1	T1-540	\\x61726368c3a96f6c6f6769652061c3a97269656e6e65	\N	1997	1998	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
950	1	T1-857	\\x656e736569676e656d656e74207072c3a9c3a96cc3a96d656e7461697265	\N	1899	1900	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1368	1	T1-1189	\\x746f7869636f6d616e65	\N	2735	2736	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1375	1	T1-1367	\\x70726f74657374616e7469736d65	\N	2749	2750	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1745	1	T3-178	\\x70726f66657373696f6e20646520666f69	\N	3489	3490	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1672	1	T3-208	\\x72c3a9706572746f69726520646573206f70c3a9726174696f6e7320646520646f75616e6573	\N	3343	3344	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1209	1	T1-1202	\\x6175746f726974c3a92064276f636375706174696f6e	\N	2417	2418	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1370	1	T1-1362	\\x6d616c616469652064657320616e696d617578	\N	2739	2740	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
670	1	T1-461	\\x616e696d616c206e75697369626c65	\N	1339	1340	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
671	1	T1-290	\\x6c6f67656d656e74	\N	1341	1342	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
478	1	T1-177	\\x63616d7061676e6520c3a96c6563746f72616c65	\N	955	956	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
1115	1	T1-1340	\\xc3a9636f6c65	\N	2229	2230	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
164	1	T1-47	\\x66697363616c6974c3a92064697265637465206427416e6369656e2052c3a967696d65	\N	327	328	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1377	1	T1-1180	\\x636f6d7061676e6965207468c3a9c3a27472616c65	\N	2753	2754	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1416	1	T2-1	\\x61626f6c6974696f6e	\N	2831	2832	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1417	1	T2-50	\\x696e64656d6e69736174696f6e	\N	2833	2834	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1488	1	T4-46	\\x323165207369c3a8636c65	\N	2975	2976	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1329	1	T1-1360	\\x62617474616765	\N	2657	2658	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1330	1	T1-1020	\\x6f657576726520646520677565727265	\N	2659	2660	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1531	1	T4-87	\\x6166666169726520447265796675732028313839342d3139303629	\N	3061	3062	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1323	1	T1-1006	\\xc3a9636f6c65206d696c697461697265	\N	2645	2646	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
248	1	T1-79	\\x66c3aa746520666f7261696e65	\N	495	496	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
824	1	T1-456	\\x636f6d6d6572636520657874c3a97269657572	\N	1647	1648	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
556	1	T1-220	\\x636f757274696572206465206d61726368616e6469736573	\N	1111	1112	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
557	1	T1-221	\\x6a657578206f6c796d706971756573	\N	1113	1114	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
1234	1	T1-821	\\x646973736964656e74	\N	2467	2468	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1205	1	T1-1198	\\x6dc3a963c3a96e6174	\N	2409	2410	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1206	1	T1-834	\\x6d616e69666573746174696f6e2063756c747572656c6c65	\N	2411	2412	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1207	1	T1-1231	\\x72c3a973697374616e6365	\N	2413	2414	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1141	1	T1-1276	\\x6e61707065206427656175	\N	2281	2282	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
264	1	T1-786	\\x63756c74757265	\N	527	528	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1418	1	T2-21	\\x636f6e737472756374696f6e	\N	2835	2836	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1328	1	T1-1026	\\x6173736f6c656d656e74	\N	2655	2656	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1529	1	T4-111	\\x6775657272652069737261c3a96c6f2d61726162652028313934382d2029	\N	3057	3058	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1290	1	T1-1119	\\x617273656e616c	\N	2579	2580	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
636	1	T1-268	\\x6d696e65726169	\N	1271	1272	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
637	1	T1-269	\\x6a756966	\N	1273	1274	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
1204	1	T1-941	\\x6d61726368c3a9206427696e74c3a972c3aa74206e6174696f6e616c	\N	2407	2408	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1128	1	T1-806	\\x736167652066656d6d65	\N	2255	2256	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1588	1	T4-17	\\x3965207369c3a8636c65	\N	3175	3176	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1131	1	T1-681	\\x6f70696e696f6e207075626c69717565	\N	2261	2262	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
992	1	T1-532	\\x756e69766572736974c3a9	\N	1983	1984	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1244	1	T1-1165	\\x66697363616c6974c3a920646972656374652072c3a9766f6c7574696f6e6e61697265	\N	2487	2488	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
993	1	T1-666	\\x6166666169726520636976696c65	\N	1985	1986	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1366	1	T1-1160	\\x68c3b474656c20636f6d6d756e61757461697265	\N	2731	2732	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1371	1	T1-1249	\\x6169646520736f6369616c6520666163756c746174697665	\N	2741	2742	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1359	1	T1-1258	\\x646f6d61696e65207075626c696320666c757669616c	\N	2717	2718	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
715	1	T1-1010	\\x6f66666963696e6520706861726d616365757469717565	\N	1429	1430	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
1361	1	T1-1205	\\x62617468796dc3a974726965	\N	2721	2722	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1362	1	T1-1147	\\x726f7574652064c3a970617274656d656e74616c65	\N	2723	2724	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1363	1	T1-1380	\\x766f696520696e746572636f6d6d756e616c65	\N	2725	2726	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1232	1	T1-818	\\x7365637465	\N	2463	2464	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
767	1	T1-721	\\x636f6e7365696c6c65722067c3a96ec3a972616c	\N	1533	1534	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:06
1236	1	T1-823	\\x63617365726e65	\N	2471	2472	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
768	1	T1-368	\\x61646d696e697374726174696f6e2063616e746f6e616c65206427c3a9706f7175652072c3a9766f6c7574696f6e6e61697265	\N	1535	1536	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:06
1828	1	T3-49	\\x63617274652064276964656e746974c3a92070726f66657373696f6e6e656c6c65	\N	3655	3656	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
794	1	T1-1091	\\x6368617373657572	\N	1587	1588	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
1829	1	T3-152	\\x706172746974696f6e206d75736963616c65	\N	3657	3658	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
316	1	T1-100	\\x707570696c6c65206465206c27c389746174	\N	631	632	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
658	1	T1-1179	\\x63756c7465	\N	1315	1316	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
1197	1	T1-769	\\x636f6e7365696c20642761646d696e697374726174696f6e	\N	2393	2394	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1198	1	T1-1280	\\x6d696c696365732070726f76696e6369616c6573	\N	2395	2396	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1658	1	T3-16	\\x617272c3aa74c3a9206475207072c3a9736964656e7420647520636f6e7365696c2072c3a967696f6e616c	\N	3315	3316	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1659	1	T3-27	\\x62696c616e2070c3a96461676f6769717565	\N	3317	3318	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1692	1	T3-7	\\x61666669636865	\N	3383	3384	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1593	1	T4-61	\\x677565727265206465206c61204c6967756520642741756773626f7572672028313638382d3136393729	\N	3185	3186	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1645	1	T3-156	\\x7065726d69732064652064c3a96d6f6c6972	\N	3289	3290	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1654	1	T3-52	\\x63617274756c61697265	\N	3307	3308	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1655	1	T3-132	\\x6d617472696365206427696d706f736974696f6e	\N	3309	3310	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1508	1	T4-83	\\x52c3a9766f6c7574696f6e20696e647573747269656c6c65	\N	3015	3016	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1270	1	T1-922	\\x617373656d626cc3a965206575726f70c3a9656e6e65	\N	2539	2540	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
604	1	T1-441	\\x7375727665696c6c616e6365206465732062c3a274696d656e7473	\N	1207	1208	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
995	1	T1-1233	\\x656e7472657072697365206465207472617661696c2074656d706f7261697265	\N	1989	1990	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1594	1	T4-60	\\x67756572726520646520486f6c6c616e64652028313637322d3136373829	\N	3187	3188	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
873	1	T1-451	\\x6175746f726f757465	\N	1745	1746	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
1741	1	T3-102	\\x657461742064652073656374696f6e	\N	3481	3482	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1666	1	T3-212	\\x72c3b46c65	\N	3331	3332	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1667	1	T3-129	\\x6d61726368c3a9207075626c6963	\N	3333	3334	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1665	1	T3-192	\\x7265676973747265206427617070656c	\N	3329	3330	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1171	1	T1-1185	\\x7a6f6e65206465206d6172616973	\N	2341	2342	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
300	1	T1-739	\\x657370616365206e61747572656c2073656e7369626c65	\N	599	600	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
635	1	T1-1141	\\xc3a9636f6c6520646520636f6e6475697465	\N	1269	1270	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
267	1	T1-110	\\x63656e747269736d65	\N	533	534	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
949	1	T1-952	\\x656e736569676e656d656e742070726976c3a9	\N	1897	1898	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1200	1	T1-774	\\x686162697461742070c3a9726975726261696e	\N	2399	2400	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
296	1	T1-672	\\x70726f66657373696f6e206dc3a9646963616c65	\N	591	592	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
951	1	T1-1349	\\x6f7269656e746174696f6e2073636f6c61697265	\N	1901	1902	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
765	1	T1-367	\\x636f6e7365696c2067c3a96ec3a972616c	\N	1529	1530	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:06
1235	1	T1-822	\\x6772c3a26365	\N	2469	2470	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1563	1	T4-30	\\x313865207369c3a8636c65	\N	3125	3126	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1567	1	T4-63	\\x677565727265206465205472656e746520416e732028313631382d3136343829	\N	3133	3134	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1566	1	T4-10	\\x416e746971756974c3a9206761756c6f697365	\N	3131	3132	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1568	1	T4-66	\\x6775657272652064652053756363657373696f6e20642741757472696368652028313734302d3137343829	\N	3135	3136	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1589	1	T4-22	\\x313465207369c3a8636c65	\N	3177	3178	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
979	1	T1-1190	\\x736f6369c3a974c3a920636f6d6d65726369616c65	\N	1957	1958	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1466	1	T2-47	\\x67657374696f6e20636f6d707461626c65	\N	2931	2932	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
747	1	T1-354	\\x66696e616e63657320696e746572636f6d6d756e616c6573	\N	1493	1494	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
1635	1	T3-214	\\x72c3b46c65206427696d706f736974696f6e	\N	3269	3270	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1752	1	T3-155	\\x7065726d697320646520636f6e73747275697265	\N	3503	3504	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1008	1	T1-1050	\\x63697263756c6174696f6e20726f757469c3a87265	\N	2015	2016	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1029	1	T1-1072	\\x6d696e6575722064c3a96c696e7175616e74	\N	2057	2058	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1035	1	T1-561	\\x6167656e7420736569676e65757269616c	\N	2069	2070	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1668	1	T3-231	\\x7472616374	\N	3335	3336	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1030	1	T1-848	\\x696e73657274696f6e2070726f66657373696f6e6e656c6c65	\N	2059	2060	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1669	1	T3-59	\\x6368726f6e6f	\N	3337	3338	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1199	1	T1-799	\\x63696d657469c3a87265206d696c697461697265	\N	2397	2398	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1127	1	T1-1032	\\x617578696c6961697265206dc3a9646963616c	\N	2253	2254	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
997	1	T1-565	\\x68796769c3a86e6520616c696d656e7461697265	\N	1993	1994	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
998	1	T1-539	\\x696e717569736974696f6e	\N	1995	1996	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
627	1	T1-261	\\x70617472696d6f696e652070726976c3a9	\N	1253	1254	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
295	1	T1-89	\\x6dc3a9646563696e	\N	589	590	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
1211	1	T1-791	\\x7265636f757672656d656e742066697363616c	\N	2421	2422	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1111	1	T1-659	\\x64c3a96c69742064277573616765	\N	2221	2222	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1664	1	T3-179	\\x70726f6772616d6d65	\N	3327	3328	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
265	1	T1-1407	\\x756e69766572736974c3a920706f70756c61697265	\N	529	530	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1210	1	T1-1274	\\x70726f706167616e6465	\N	2419	2420	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
771	1	T1-1294	\\x636f6d70746162696c6974c3a9206427656e7472657072697365	\N	1541	1542	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
1554	1	T4-33	\\x417373656d626cc3a965206cc3a96769736c61746976652028313739312d3137393229	\N	3107	3108	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1580	1	T4-56	\\x52c3a9666f726d652070726f74657374616e7465	\N	3159	3160	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
13	1	T1-764	\\x706f70756c6174696f6e20727572616c65	\N	25	26	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
158	1	T1-1299	\\x70726f74656374696f6e20736f6369616c65	\N	315	316	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
262	1	T1-696	\\x656d706c6f69	\N	523	524	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1590	1	T4-71	\\x63616d7061676e652064652052757373696520283138313229	\N	3179	3180	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1591	1	T4-70	\\x63616d7061676e65206465204672616e636520283138313429	\N	3181	3182	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1608	1	T3-203	\\x72c3a8676c656d656e74206427656e7472657072697365	\N	3215	3216	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1000	1	T1-648	\\x61726368c3a96f6c6f676965	\N	1999	2000	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1527	1	T4-118	\\x4f636375706174696f6e20616c6c656d616e64652028313934302d3139343429	\N	3053	3054	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1372	1	T1-1425	\\x6d6172c3a9636861757373c3a965	\N	2743	2744	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1373	1	T1-1200	\\x64697374696e6374696f6e20686f6e6f72696669717565	\N	2745	2746	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1374	1	T1-1175	\\x626f7564646869736d65	\N	2747	2748	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1208	1	T1-830	\\x636f6c6c61626f726174696f6e	\N	2415	2416	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
298	1	T1-647	\\x687964726f63617262757265	\N	595	596	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
722	1	T1-341	\\x66726f6e7469c3a87265	\N	1443	1444	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
463	1	T1-1321	\\x72c3a9717569736974696f6e73206d696c69746169726573	\N	925	926	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
515	1	T1-1096	\\x6173736f63696174696f6e20736f63696f2dc3a96475636174697665	\N	1029	1030	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
555	1	T1-219	\\x736f6369c3a974c3a92064652074c3a96cc3a9646966667573696f6e2070726976c3a965	\N	1109	1110	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
1231	1	T1-816	\\x75736167657220646573207472616e73706f727473	\N	2461	2462	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1233	1	T1-819	\\x64726f697420646573206f626c69676174696f6e73	\N	2465	2466	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1172	1	T1-740	\\x636174686f6c696369736d65	\N	2343	2344	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
299	1	T1-91	\\x64c3a9666f726573746174696f6e	\N	597	598	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
1237	1	T1-1444	\\xc3a96475636174696f6e207375727665696c6cc3a965	\N	2473	2474	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
770	1	T1-524	\\x6964656e746974c3a920636f6d6d65726369616c65	\N	1539	1540	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
1679	1	T3-209	\\x72c3a9706572746f6972652064276f66666963696572207075626c6963206d696e697374c3a97269656c	\N	3357	3358	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
815	1	T1-626	\\x666f796572	\N	1629	1630	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
1001	1	T1-541	\\x6d6f7274616c6974c3a920696e66616e74696c65	\N	2001	2002	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1190	1	T1-1108	\\x64c3a966656e736520636f6e747265206c61206d6572	\N	2379	2380	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1191	1	T1-951	\\x7369676e616c69736174696f6e206d61726974696d65	\N	2381	2382	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
294	1	T1-397	\\x6c69627261697265	\N	587	588	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
1656	1	T3-216	\\x736368c3a96d6120646972656374657572	\N	3311	3312	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
266	1	T1-919	\\x6661736369736d65	\N	531	532	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1189	1	T1-761	\\x72656c6174696f6e732061766563206c65732075736167657273	\N	2377	2378	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
631	1	T1-1082	\\x736572766963652066696e616e63696572206465206c6120706f737465	\N	1261	1262	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
249	1	T1-1437	\\x66c3aa7465	\N	497	498	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1203	1	T1-781	\\x726563656e73656d656e7420646520706f70756c6174696f6e	\N	2405	2406	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1269	1	T1-1063	\\xc3a970696365726965	\N	2537	2538	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
654	1	T1-279	\\x7472616e73667573696f6e2073616e6775696e65	\N	1307	1308	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
679	1	T1-1018	\\x707261746971756573206167726169726573	\N	1357	1358	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
1348	1	T1-1093	\\x706f6c6963652075726261696e65	\N	2695	2696	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1429	1	T2-55	\\x6f7267616e69736174696f6e	\N	2857	2858	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1509	1	T4-37	\\x4469726563746f6972652028313739352d3137393929	\N	3017	3018	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1581	1	T4-53	\\x4772616e6465732064c3a9636f75766572746573	\N	3161	3162	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1582	1	T4-52	\\xc3a9646974206465204e616e74657320283135393829	\N	3163	3164	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1660	1	T3-176	\\x70726f63c3a8732d76657262616c2064276578616d656e	\N	3319	3320	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1742	1	T3-134	\\x6dc3a96d6f697265	\N	3483	3484	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1822	1	T3-211	\\x726576756520646520707265737365	\N	3643	3644	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
3	1	T1-246	\\x6167726963756c74757265	\N	5	6	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
4	1	T1-1317	\\x61646d696e697374726174696f6e	\N	7	8	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
5	1	T1-307	\\x736f6369c3a974c3a9	\N	9	10	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
7	1	T1-1262	\\xc3a971756970656d656e74	\N	13	14	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
8	1	T1-1302	\\xc3a96475636174696f6e	\N	15	16	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
9	1	T1-483	\\x6a757374696365	\N	17	18	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
10	1	T1-906	\\x6f70696e696f6e	\N	19	20	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
11	1	T1-150	\\x657874c3a97269657572	\N	21	22	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
12	1	T1-1	\\x6861626974617420727572616c	\N	23	24	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
304	1	T1-94	\\x686f75696c6c65	\N	607	608	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
39	1	T1-643	\\x626f6973	\N	77	78	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
40	1	T1-1070	\\x70726f6475697420646f6d616e69616c	\N	79	80	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
41	1	T1-8	\\x6f70c3a9726174696f6e206d696c697461697265	\N	81	82	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
42	1	T1-250	\\x64c3a966656e7365206475207465727269746f697265	\N	83	84	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
43	1	T1-9	\\x6879647261756c697175652061677269636f6c65	\N	85	86	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
44	1	T1-1134	\\xc3a9636f6e6f6d696520727572616c65	\N	87	88	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
45	1	T1-242	\\x62617272616765206879647261756c69717565	\N	89	90	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
46	1	T1-133	\\x69727269676174696f6e	\N	91	92	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
47	1	T1-211	\\x647261696e616765	\N	93	94	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
48	1	T1-988	\\x616dc3a96e6167656d656e74206465732065617578	\N	95	96	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
49	1	T1-10	\\x696e647573747269652064752063756972	\N	97	98	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
50	1	T1-11	\\x70726f707269c3a974c3a9207075626c69717565	\N	99	100	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
51	1	T1-1222	\\x66696e616e636573207075626c6971756573	\N	101	102	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
52	1	T1-763	\\x646f6d61696e65206d6f62696c696572	\N	103	104	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
1195	1	T1-1366	\\x6d6167697374726174	\N	2389	2390	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
61	1	T1-383	\\x646f6d61696e6520696d6d6f62696c696572	\N	121	122	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
62	1	T1-377	\\x616c69c3a96e6174696f6e20646f6d616e69616c65	\N	123	124	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
63	1	T1-1113	\\x636f6e63657373696f6e20646f6d616e69616c65	\N	125	126	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
64	1	T1-838	\\x646f6e732d65742d6c656773	\N	127	128	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
65	1	T1-1095	\\x666f72c3aa7420646f6d616e69616c65	\N	129	130	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
66	1	T1-12	\\x67c3a96e6965206d696c697461697265	\N	131	132	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
67	1	T1-912	\\x696e667261737472756374757265206d696c697461697265	\N	133	134	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
68	1	T1-13	\\x76656e74652061757820656e6368c3a8726573	\N	135	136	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
69	1	T1-1248	\\x636f6d6d65726365	\N	137	138	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
70	1	T1-1438	\\x76656e7465207075626c6971756520696d6d6f62696c69c3a87265	\N	139	140	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
71	1	T1-1137	\\x636f6d6d697373616972652070726973657572	\N	141	142	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
72	1	T1-14	\\x706c616765	\N	143	144	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
73	1	T1-762	\\x6c6974746f72616c	\N	145	146	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
74	1	T1-342	\\x746f757269736d652062616c6ec3a961697265	\N	147	148	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
75	1	T1-15	\\x6672616e636f70686f6e6965	\N	149	150	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
76	1	T1-974	\\x72656c6174696f6e7320696e7465726e6174696f6e616c6573	\N	151	152	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
77	1	T1-16	\\x736f6369c3a974c3a92072c3a9766f6c7574696f6e6e61697265	\N	153	154	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
78	1	T1-17	\\x6d61696e7469656e206465206c276f72647265	\N	155	156	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
79	1	T1-471	\\x706f6c696365	\N	157	158	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
80	1	T1-926	\\x6d616e69666573746174696f6e2073706f7274697665	\N	159	160	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
83	1	T1-606	\\x766973697465206f6666696369656c6c65	\N	165	166	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
84	1	T1-796	\\x6d616e69666573746174696f6e2064652070726f746573746174696f6e	\N	167	168	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
603	1	T1-801	\\x7365727669636573	\N	1205	1206	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
1799	1	T3-144	\\x6e6f7465	\N	3597	3598	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
14	1	T1-40	\\x62c3a274696d656e742061677269636f6c65	\N	27	28	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
15	1	T1-1047	\\x616dc3a96e6167656d656e7420727572616c	\N	29	30	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
16	1	T1-366	\\x67c3ae746520727572616c	\N	31	32	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
17	1	T1-2	\\x766973697465757220646520707269736f6e	\N	33	34	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
18	1	T1-3	\\x72656368657263686520736369656e74696669717565	\N	35	36	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
19	1	T1-1356	\\x63756c7475726520657870c3a972696d656e74616c65	\N	37	38	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
20	1	T1-1413	\\x726563686572636865206170706c697175c3a965	\N	39	40	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
21	1	T1-618	\\x64c3a9636f75766572746520736369656e74696669717565	\N	41	42	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
22	1	T1-676	\\x72c3a9756e696f6e20736369656e74696669717565	\N	43	44	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
23	1	T1-1073	\\x736f6369c3a974c3a920736176616e7465	\N	45	46	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
24	1	T1-287	\\x6469736369706c696e6520736369656e74696669717565	\N	47	48	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
25	1	T1-1005	\\x736369656e6365732068756d61696e6573	\N	49	50	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
26	1	T1-319	\\x72656368657263686520666f6e64616d656e74616c65	\N	51	52	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
27	1	T1-981	\\x6f7267616e69736d6520646520726563686572636865	\N	53	54	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
1449	1	T2-15	\\x63657373696f6e	\N	2897	2898	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
846	1	T1-428	\\xc3a96cc3a87665	\N	1691	1692	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
1613	1	T3-18	\\x61746c6173	\N	3225	3226	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
85	1	T1-18	\\x6368616e736f6e2073756276657273697665	\N	169	170	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
86	1	T1-19	\\x6361727269c3a87265	\N	171	172	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
87	1	T1-481	\\x696e647573747269652065787472616374697665	\N	173	174	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
88	1	T1-20	\\x7472617661696c6c65757220736f6369616c	\N	175	176	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
28	1	T1-448	\\x636865726368657572	\N	55	56	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
29	1	T1-436	\\x6469736369706c696e6520756e6976657273697461697265	\N	57	58	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
30	1	T1-896	\\x76696520696e74656c6c65637475656c6c65	\N	59	60	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
31	1	T1-4	\\x647572c3a965206475207472617661696c	\N	61	62	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
37	1	T1-1079	\\x72c3a967696f6e	\N	73	74	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
1788	1	T3-220	\\x737461747574	\N	3575	3576	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
53	1	T1-1433	\\x6269656e7320696e746572636f6d6d756e617578	\N	105	106	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
54	1	T1-1051	\\x6269656e73206e6174696f6e617578	\N	107	108	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
55	1	T1-690	\\x6269656e7320636f6d6d756e617578	\N	109	110	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
56	1	T1-766	\\x6269656e2061646d696e69737472c3a9	\N	111	112	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
57	1	T1-333	\\x6163717569736974696f6e20646f6d616e69616c65	\N	113	114	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
58	1	T1-616	\\x6269656e732072c3a967696f6e617578	\N	115	116	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
59	1	T1-93	\\x636f6e63657373696f6e2066756ec3a97261697265	\N	117	118	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
33	1	T1-5	\\x726f757465206e6174696f6e616c65	\N	65	66	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
34	1	T1-1149	\\x72c3a97365617520726f7574696572	\N	67	68	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
35	1	T1-6	\\x66696e616e6365732072c3a967696f6e616c6573	\N	69	70	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
690	1	T1-312	\\x66656d6d65	\N	1379	1380	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
1457	1	T2-18	\\x636f6d6d756e69636174696f6e	\N	2913	2914	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
90	1	T1-350	\\xc3a96475636174657572	\N	179	180	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
91	1	T1-936	\\x656e7472c3a96520736f6c656e6e656c6c65	\N	181	182	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
93	1	T1-194	\\x72c3a963657074696f6e206f6666696369656c6c65	\N	185	186	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
94	1	T1-1335	\\x696e61756775726174696f6e	\N	187	188	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
95	1	T1-22	\\x76696520706f6c697469717565	\N	189	190	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
96	1	T1-1169	\\x706172746920706f6c697469717565	\N	191	192	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
97	1	T1-87	\\x6d6f7576656d656e7420706f6c697469717565	\N	193	194	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
98	1	T1-23	\\x636f6d7061676e6f6e6e616765	\N	195	196	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
99	1	T1-1025	\\x6f7267616e69736174696f6e2070726f66657373696f6e6e656c6c65	\N	197	198	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
100	1	T1-24	\\x6368656d696e2064652072616e646f6e6ec3a965	\N	199	200	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
101	1	T1-632	\\x746f757269736d65	\N	201	202	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
102	1	T1-908	\\x6368656d696e20727572616c	\N	203	204	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
103	1	T1-25	\\x696e63656e64696520766f6c6f6e7461697265	\N	205	206	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
104	1	T1-1403	\\x6372696d65732065742064c3a96c697473	\N	207	208	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
157	1	T1-45	\\x6578636c7573696f6e20736f6369616c65	\N	313	314	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
243	1	T1-76	\\x766f69652070726976c3a965	\N	485	486	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
112	1	T1-418	\\x64c3a974656e74696f6e20642761726d6573	\N	223	224	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
113	1	T1-204	\\x73c3a96a6f75722064657320c3a97472616e67657273	\N	225	226	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
115	1	T1-545	\\x7472616e73706f727420646520636f727073	\N	229	230	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
116	1	T1-518	\\x706967656f6e20766f796167657572	\N	231	232	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
117	1	T1-29	\\x70726976696cc3a86765732064657320636f6d6d756e617574c3a973	\N	233	234	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
118	1	T1-842	\\x72c3a967696d6520736569676e65757269616c	\N	235	236	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
119	1	T1-155	\\x636f7574756d6573	\N	237	238	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
120	1	T1-1333	\\x64726f6974732064277573616765	\N	239	240	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
121	1	T1-30	\\x766f69652066657272c3a9652070726976c3a965	\N	241	242	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
122	1	T1-31	\\x766163616e7473	\N	243	244	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
123	1	T1-32	\\x63697263756c6174696f6e2064657320706572736f6e6e6573	\N	245	246	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
124	1	T1-1269	\\x7375727665696c6c616e6365206475207465727269746f697265	\N	247	248	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
125	1	T1-33	\\x6d6f7576656d656e742064276964c3a96573	\N	249	250	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
126	1	T1-453	\\x746f6cc3a972616e6365	\N	251	252	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
1657	1	T3-105	\\xc3a974756465	\N	3313	3314	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
127	1	T1-1239	\\x64726f697473206465206c27686f6d6d65	\N	253	254	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
128	1	T1-1247	\\x617468c3a969736d65	\N	255	256	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
129	1	T1-143	\\x7068696c6f736f70686965	\N	257	258	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
130	1	T1-34	\\x62c3a274696d656e7420706f6c7976616c656e74	\N	259	260	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
131	1	T1-35	\\x646f6d61696e65207075626c6963206d61726974696d65	\N	261	262	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
132	1	T1-36	\\x64c3a96368657420726164696f6163746966	\N	263	264	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
133	1	T1-362	\\x7472616974656d656e74206465732064c3a96368657473	\N	265	266	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
134	1	T1-1443	\\x63656e7472616c65206e75636cc3a961697265	\N	267	268	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
135	1	T1-37	\\x746f757269736d6520666c757669616c	\N	269	270	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
136	1	T1-255	\\x6e617669676174696f6e20646520706c616973616e6365	\N	271	272	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
137	1	T1-38	\\x656e736569676e616e74	\N	273	274	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
138	1	T1-499	\\x6f7267616e69736174696f6e2073636f6c61697265	\N	275	276	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
139	1	T1-1029	\\x696e737469747574657572	\N	277	278	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
140	1	T1-1456	\\xc3a964756361746575722073706f72746966	\N	279	280	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
141	1	T1-614	\\x6d61c3ae74726520642761707072656e74697373616765	\N	281	282	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:55
142	1	T1-300	\\x70726f66657373657572	\N	283	284	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
143	1	T1-583	\\x666f726d6174657572	\N	285	286	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
144	1	T1-777	\\x6dc3a9646563696e20686f73706974616c696572	\N	287	288	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
145	1	T1-1056	\\x666f6e6374696f6e6e61697265206465206c27c389746174	\N	289	290	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
146	1	T1-39	\\xc3a9736f74657269736d65	\N	291	292	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
147	1	T1-172	\\x736369656e63657320706172616c6cc3a86c6573	\N	293	294	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1695	1	T3-21	\\x6176616e742d70726f6a6574	\N	3389	3390	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1784	1	T3-92	\\x646f63756d656e746174696f6e	\N	3567	3568	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
149	1	T1-42	\\x72656472657373656d656e74206a756469636961697265	\N	297	298	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
150	1	T1-460	\\x6166666169726520636f6d6d65726369616c65	\N	299	300	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
151	1	T1-724	\\x656e747265707269736520656e20646966666963756c74c3a9	\N	301	302	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
152	1	T1-224	\\x61646d696e697374726174657572206a756469636961697265	\N	303	304	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
153	1	T1-43	\\x64616e7365	\N	305	306	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
154	1	T1-608	\\x617274	\N	307	308	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
155	1	T1-1228	\\x656e736569676e656d656e742061727469737469717565	\N	309	310	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
156	1	T1-44	\\xc3a96d6967726174696f6e	\N	311	312	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
400	1	T1-136	\\x76696e	\N	799	800	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
105	1	T1-26	\\x70726f626174696f6e	\N	209	210	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
106	1	T1-996	\\x6170706c69636174696f6e20646573207065696e6573	\N	211	212	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
107	1	T1-27	\\x736f6369c3a974c3a920636976696c652070726f66657373696f6e6e656c6c65	\N	213	214	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
108	1	T1-885	\\x656e7472657072697365	\N	215	216	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
109	1	T1-369	\\x656e72656769737472656d656e74206a756469636961697265	\N	217	218	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
110	1	T1-28	\\x706f6c6963652061646d696e697374726174697665	\N	219	220	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
111	1	T1-962	\\x70726f66657373696f6e2072c3a9676c656d656e74c3a965	\N	221	222	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
159	1	T1-678	\\x696e646967656e74	\N	317	318	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
163	1	T1-432	\\x73c3a96375726974c3a920736f6369616c65	\N	325	326	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1789	1	T3-138	\\x6d696e757465206a75726964696374696f6e6e656c6c65	\N	3577	3578	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
714	1	T1-1028	\\x7375627374616e63652076c3a96ec3a96e65757365	\N	1427	1428	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
166	1	T1-1334	\\x766f696520646520636f6d6d756e69636174696f6e	\N	331	332	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
167	1	T1-277	\\x696e66726173747275637475726520706f72747561697265	\N	333	334	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
168	1	T1-1089	\\x6261727261676520687964726fc3a96c6563747269717565	\N	335	336	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
169	1	T1-49	\\x7669746963756c74757265	\N	337	338	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
170	1	T1-421	\\x70726f64756374696f6e2061677269636f6c65	\N	339	340	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
171	1	T1-50	\\x76c3a9686963756c65206175746f6d6f62696c65	\N	341	342	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
172	1	T1-548	\\x7472616e73706f7274	\N	343	344	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1630	1	T3-23	\\x6261696c	\N	3259	3260	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
181	1	T1-554	\\x616dc3a96e6167656d656e74206475207465727269746f697265	\N	361	362	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
182	1	T1-334	\\x6d6f6e7461676e65	\N	363	364	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
183	1	T1-53	\\x61696465206dc3a9646963616c65	\N	365	366	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
184	1	T1-1168	\\x6169646520736f6369616c65	\N	367	368	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
185	1	T1-54	\\x7072c3a9736964656e7420647520636f6e7365696c2072c3a967696f6e616c	\N	369	370	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
186	1	T1-868	\\xc3a96c75	\N	371	372	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
187	1	T1-1237	\\x636f6e7365696c2072c3a967696f6e616c	\N	373	374	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
188	1	T1-55	\\x72c3a967696f6e616c69736174696f6e	\N	375	376	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
189	1	T1-56	\\x6dc3a9646563696e652076c3a974c3a972696e61697265	\N	377	378	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
191	1	T1-57	\\x746572726520696e63756c7465	\N	381	382	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
192	1	T1-105	\\x64c3a96672696368656d656e74	\N	383	384	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
193	1	T1-58	\\x636f6e6672c3a9726965	\N	385	386	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
194	1	T1-74	\\x6173736f63696174696f6e2063756c7475656c6c65	\N	387	388	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
195	1	T1-59	\\x696e64757374726965206d61726974696d65	\N	389	390	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
196	1	T1-60	\\x6e6f7461697265	\N	391	392	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
197	1	T1-101	\\x6f66666963696572206d696e697374c3a97269656c	\N	393	394	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
198	1	T1-1215	\\x7075626c69636974c3a920666f6e6369c3a87265	\N	395	396	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
199	1	T1-61	\\x696e6672616374696f6e206d696c697461697265	\N	397	398	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
200	1	T1-872	\\x726563727574656d656e74206d696c697461697265	\N	399	400	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
201	1	T1-359	\\x736572766963652063697669717565	\N	401	402	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
202	1	T1-62	\\x636f6e67c3a9732073636f6c6169726573	\N	403	404	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
203	1	T1-1404	\\x727974686d652073636f6c61697265	\N	405	406	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
204	1	T1-684	\\x63656e747265206465206c6f6973697273	\N	407	408	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
205	1	T1-311	\\x63656e74726520646520766163616e636573	\N	409	410	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
206	1	T1-63	\\xc3a9636c616972616765207075626c6963	\N	411	412	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
207	1	T1-755	\\x72c3a97365617520646520646973747269627574696f6e	\N	413	414	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
208	1	T1-64	\\x706c616e74652074696e63746f7269616c65	\N	415	416	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
209	1	T1-1325	\\x63756c7475726520696e647573747269656c6c65	\N	417	418	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
210	1	T1-203	\\x696e647573747269652074657874696c65	\N	419	420	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
211	1	T1-65	\\x6d61676173696e2067c3a96ec3a972616c	\N	421	422	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
212	1	T1-66	\\x636972636f6e736372697074696f6e206563636cc3a9736961737469717565	\N	423	424	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
213	1	T1-793	\\x696e737469747574696f6e206563636cc3a9736961737469717565	\N	425	426	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
214	1	T1-67	\\x73696e6973747265	\N	427	428	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
215	1	T1-147	\\x70726f74656374696f6e20636976696c65	\N	429	430	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
216	1	T1-1393	\\x696e6f6e646174696f6e	\N	431	432	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
217	1	T1-1055	\\x636174617374726f70686520696e647573747269656c6c65	\N	433	434	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
221	1	T1-68	\\x70726f707269c3a974c3a920696e647573747269656c6c65	\N	441	442	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
222	1	T1-69	\\x696d7069c3a974c3a9	\N	443	444	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
223	1	T1-485	\\x6d616e69666573746174696f6e20616e746972656c69676965757365	\N	445	446	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
235	1	T1-317	\\x636f6d6d756e65	\N	469	470	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
479	1	T1-178	\\x666572726f7574616765	\N	957	958	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
736	1	T1-345	\\x62616e6e697373656d656e74	\N	1471	1472	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
1511	1	T4-96	\\x63726973652064652031393239	\N	3021	3022	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
224	1	T1-70	\\x6d6f7576656d656e74206c61c3af717565	\N	447	448	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
225	1	T1-1106	\\x636f6e666c69742073636f6c61697265	\N	449	450	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
226	1	T1-71	\\x696d70c3b47420737572206c65732062c3a96ec3a96669636573	\N	451	452	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
227	1	T1-669	\\x66697363616c6974c3a92070726f66657373696f6e6e656c6c65	\N	453	454	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
228	1	T1-72	\\x72c3a9717569736974696f6e206465206c6f67656d656e74	\N	455	456	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
229	1	T1-73	\\x636f6c6c65637469766974c3a9206c6f63616c65	\N	457	458	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
230	1	T1-1345	\\x61646d696e697374726174696f6e2067c3a96ec3a972616c65	\N	459	460	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
231	1	T1-594	\\x726567726f7570656d656e7420646520636f6d6d756e6573	\N	461	462	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
1268	1	T1-1178	\\x626f756c616e6765726965	\N	2535	2536	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
176	1	T1-628	\\x73c3a96375726974c3a920726f757469c3a87265	\N	351	352	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
177	1	T1-51	\\x7072756427686f6d6d65	\N	353	354	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
178	1	T1-767	\\x6a756765	\N	355	356	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
179	1	T1-1061	\\xc3a96c656374696f6e2070726f66657373696f6e6e656c6c65	\N	357	358	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
180	1	T1-52	\\x706f6c697469717565206465206c61206d6f6e7461676e65	\N	359	360	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
232	1	T1-1449	\\x64c3a970617274656d656e74	\N	463	464	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
233	1	T1-199	\\x636f6f70c3a9726174696f6e20696e74657272c3a967696f6e616c65	\N	465	466	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
1676	1	T3-67	\\x636f6e74726174	\N	3351	3352	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
89	1	T1-200	\\x616374696f6e20736f6369616c65	\N	177	178	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
324	1	T1-446	\\x6e6f626c65737365	\N	647	648	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
582	1	T1-239	\\x686f6d6d616765	\N	1163	1164	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
234	1	T1-1238	\\xc3a97461626c697373656d656e74207075626c696320646520636f6f70c3a9726174696f6e20696e746572636f6d6d756e616c65	\N	467	468	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
236	1	T1-804	\\x7669652072656c69676965757365	\N	471	472	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
237	1	T1-1391	\\x736f6369c3a974c3a920646520636861726974c3a9	\N	473	474	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
241	1	T1-1311	\\x656e747265707269736520646520737065637461636c65	\N	481	482	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
242	1	T1-562	\\x6c616e6775652072c3a967696f6e616c65	\N	483	484	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
244	1	T1-77	\\x6e61697373616e6365	\N	487	488	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
245	1	T1-1139	\\xc3a974617420636976696c	\N	489	490	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
246	1	T1-492	\\x706f70756c6174696f6e20686f73706974616c69c3a87265	\N	491	492	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
254	1	T1-946	\\x656e66616e74	\N	507	508	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
774	1	T1-782	\\x616c696d656e746174696f6e	\N	1547	1548	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
282	1	T1-1390	\\x7261646963616c69736d65	\N	563	564	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
251	1	T1-81	\\x696e6475737472696520c3a96c656374726f6e69717565	\N	501	502	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
252	1	T1-82	\\x656e66616e742068616e6469636170c3a9	\N	503	504	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
253	1	T1-677	\\xc3a96475636174696f6e207370c3a96369616c65	\N	505	506	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
255	1	T1-83	\\x6f70c3a9726174696f6e206427757262616e69736d65	\N	509	510	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
256	1	T1-209	\\x757262616e69736d65	\N	511	512	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
257	1	T1-475	\\x6c6f74697373656d656e74	\N	513	514	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
258	1	T1-1097	\\x726573747275637475726174696f6e2075726261696e65	\N	515	516	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
259	1	T1-124	\\x726573746175726174696f6e20696d6d6f62696c69c3a87265	\N	517	518	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
260	1	T1-84	\\x757262616e69737465	\N	519	520	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
575	1	T1-233	\\x73697465	\N	1149	1150	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
283	1	T1-1322	\\x65787472c3aa6d652064726f697465	\N	565	566	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
284	1	T1-1207	\\x65787472c3aa6d6520676175636865	\N	567	568	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
285	1	T1-229	\\x726f79616c69736d65	\N	569	570	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
286	1	T1-174	\\x626f6e617061727469736d65	\N	571	572	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
287	1	T1-1218	\\x6d6f7576656d656e742066c3a96d696e69737465	\N	573	574	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
289	1	T1-115	\\x6d6f7576656d656e7420686f6d6f73657875656c	\N	577	578	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
290	1	T1-318	\\x6d6f7576656d656e7420706163696669737465	\N	579	580	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
291	1	T1-88	\\x6dc3a97469657273206475206c69767265	\N	581	582	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:57
354	1	T1-114	\\x6d7573c3a965	\N	707	708	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
874	1	T1-496	\\x72616369736d65	\N	1747	1748	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
274	1	T1-538	\\x636f6c6f6e69616c69736d65	\N	547	548	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
275	1	T1-526	\\x636f6d6d756e69736d65	\N	549	550	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
276	1	T1-605	\\x6761756c6c69736d65	\N	551	552	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
277	1	T1-720	\\x6d6f7576656d656e742072c3a967696f6e616c69737465	\N	553	554	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
278	1	T1-1339	\\x6d6f7576656d656e7420c3a974756469616e74	\N	555	556	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
279	1	T1-197	\\x636cc3a9726963616c69736d65	\N	557	558	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
280	1	T1-391	\\x6d6f7576656d656e74206f757672696572	\N	559	560	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
281	1	T1-266	\\x6c6962c3a972616c69736d65	\N	561	562	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
303	1	T1-773	\\x63696d657469c3a87265	\N	605	606	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
305	1	T1-1326	\\x6d617469c3a87265207072656d69c3a87265	\N	609	610	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
306	1	T1-1441	\\x63656e7472616c6520746865726d69717565	\N	611	612	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
307	1	T1-95	\\x74c3a96cc3a96d617469717565	\N	613	614	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
308	1	T1-1327	\\x74c3a96cc3a9636f6d6d756e69636174696f6e73	\N	615	616	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
309	1	T1-96	\\x636f6e63657373696f6e20666f7265737469c3a87265	\N	617	618	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
310	1	T1-832	\\x6578706c6f6974616e7420666f72657374696572	\N	619	620	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
311	1	T1-97	\\x736f6369c3a974c3a9206d757475616c69737465	\N	621	622	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
312	1	T1-98	\\x6173736f63696174696f6e20646520636f6e736f6d6d617465757273	\N	623	624	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
313	1	T1-581	\\x636f6e736f6d6d6174696f6e	\N	625	626	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
314	1	T1-99	\\x656e66616e74207365636f757275	\N	627	628	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
315	1	T1-566	\\x6d696e657572207375727665696c6cc3a9	\N	629	630	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
317	1	T1-158	\\x6f7267616e69736174696f6e206a756469636961697265	\N	633	634	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
318	1	T1-1166	\\x61766f75c3a9	\N	635	636	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
321	1	T1-102	\\x706f6c69636520646573207472616e73706f727473	\N	641	642	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
322	1	T1-103	\\x636f6e646974696f6e20736f6369616c65	\N	643	644	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
323	1	T1-942	\\x636f6e646974696f6e2064657320706572736f6e6e657320657420646573206269656e73	\N	645	646	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
325	1	T1-1394	\\x6573636c6176616765	\N	649	650	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
326	1	T1-723	\\x70726f6cc3a9746172696174	\N	651	652	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
327	1	T1-1181	\\x73657276616765	\N	653	654	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
328	1	T1-151	\\x746965727320c3a9746174	\N	655	656	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
329	1	T1-184	\\x626f757267656f69736965	\N	657	658	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
330	1	T1-104	\\x636f6e7365696c206a7572696469717565	\N	659	660	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
331	1	T1-487	\\x617578696c6961697265206465206a757374696365	\N	661	662	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
332	1	T1-400	\\x73796c766963756c74757265	\N	663	664	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
333	1	T1-106	\\x72c3a966756769c3a9	\N	665	666	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
335	1	T1-1243	\\x72c3a966756769c3a920646520677565727265	\N	669	670	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
336	1	T1-107	\\xc3a96c656374696f6e20706f6c697469717565	\N	671	672	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
173	1	T1-1221	\\x6163636964656e7420646573207472616e73706f727473	\N	345	346	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
174	1	T1-126	\\x617070617265696c20c3a0207072657373696f6e20646520766170657572	\N	347	348	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
175	1	T1-1374	\\x636f6e737472756374696f6e206175746f6d6f62696c65	\N	349	350	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
337	1	T1-1152	\\xc3a96c656374696f6e	\N	673	674	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
339	1	T1-1081	\\xc3a96c656374696f6e20726567696f6e616c65	\N	677	678	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
340	1	T1-624	\\xc3a96c656374696f6e20707265736964656e7469656c6c65	\N	679	680	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
341	1	T1-1383	\\xc3a96c656374696f6e2073c3a96e61746f7269616c65	\N	681	682	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
342	1	T1-923	\\xc3a96c656374696f6e2063616e746f6e616c65	\N	683	684	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
343	1	T1-321	\\x72c3a966c3a972656e64756d	\N	685	686	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
773	1	T1-372	\\x6a75676520636f6e73756c61697265	\N	1545	1546	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
344	1	T1-401	\\xc3a96c656374696f6e20617520636f6e7365696c2064276172726f6e64697373656d656e74	\N	687	688	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
345	1	T1-171	\\xc3a96c656374696f6e206d756e69636970616c65	\N	689	690	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
346	1	T1-1254	\\xc3a96c656374696f6e206cc3a96769736c6174697665	\N	691	692	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
347	1	T1-108	\\x61726d7572696572	\N	693	694	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
348	1	T1-109	\\x6dc3a964696174657572206465206a757374696365	\N	695	696	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
349	1	T1-111	\\x696e74c3a972657373656d656e7420646573207472617661696c6c65757273	\N	697	698	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
350	1	T1-536	\\x7472617661696c	\N	699	700	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
351	1	T1-112	\\x72656365747465206e6f6e2066697363616c65	\N	701	702	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:59
352	1	T1-113	\\x636f6e737469747574696f6e	\N	703	704	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:59
353	1	T1-1259	\\x64726f6974207075626c6963	\N	705	706	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
355	1	T1-213	\\xc3a971756970656d656e7420736f63696f2063756c747572656c	\N	709	710	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
356	1	T1-116	\\xc3a9636f6e6f6d6965206d6f6e7461676e61726465	\N	711	712	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
357	1	T1-148	\\x616374696f6e20c3a9636f6e6f6d69717565	\N	713	714	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
358	1	T1-1013	\\xc3a96c6576616765	\N	715	716	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
359	1	T1-117	\\x666f6e6473206575726f70c3a9656e	\N	717	718	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
360	1	T1-118	\\x6d61676173696e20636f6c6c6563746966	\N	719	720	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
361	1	T1-1158	\\x666f6e647320646520636f6d6d65726365	\N	721	722	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
363	1	T1-119	\\x616c70686162c3a97469736174696f6e	\N	725	726	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
364	1	T1-544	\\x616374696f6e2068756d616e697461697265	\N	727	728	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
365	1	T1-881	\\x656e736569676e656d656e74	\N	729	730	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
366	1	T1-120	\\x626f6d62617264656d656e74	\N	731	732	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
367	1	T1-1341	\\x677565727265	\N	733	734	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
368	1	T1-121	\\x726176697461696c6c656d656e74206d696c697461697265	\N	735	736	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
369	1	T1-122	\\x6d6172696e65	\N	737	738	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
370	1	T1-551	\\x61726dc3a965	\N	739	740	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
371	1	T1-123	\\x70617472696d6f696e652063756c747572656c	\N	741	742	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
374	1	T1-889	\\x70617472696d6f696e65206e756dc3a97269717565	\N	747	748	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
375	1	T1-995	\\x6d6f6e756d656e7420686973746f7269717565	\N	749	750	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
376	1	T1-1343	\\x70617472696d6f696e6520617564696f76697375656c	\N	751	752	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
377	1	T1-991	\\x70617472696d6f696e65206574686e6f6c6f6769717565	\N	753	754	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
378	1	T1-1337	\\x70617472696d6f696e6520c3a963726974	\N	755	756	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
379	1	T1-658	\\xc3a964696669636520636c617373c3a9	\N	757	758	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
380	1	T1-825	\\x6f626a6574206427617274	\N	759	760	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
381	1	T1-1223	\\x70617472696d6f696e652069636f6e6f677261706869717565	\N	761	762	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
1739	1	T3-50	\\x636172746520706f7374616c65	\N	3477	3478	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
268	1	T1-170	\\x72c3a97075626c6963616e69736d65	\N	535	536	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
269	1	T1-1021	\\x736f6369616c69736d65	\N	537	538	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
270	1	T1-288	\\x616e74696361706974616c69736d65	\N	539	540	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
271	1	T1-634	\\x616e6172636869736d65	\N	541	542	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
272	1	T1-1278	\\x6e6174696f6e616c69736d65	\N	543	544	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
273	1	T1-1177	\\x6d6f7576656d656e7420c3a9636f6c6f6769737465	\N	545	546	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
385	1	T1-127	\\x6f757672696572	\N	769	770	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
386	1	T1-128	\\x70726174697175652072656c69676965757365	\N	771	772	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
387	1	T1-129	\\x706c616e696669636174696f6e	\N	773	774	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
388	1	T1-130	\\x657370c3a863652070726f74c3a967c3a965	\N	775	776	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
389	1	T1-1395	\\x70c3aa636865206d61726974696d65	\N	777	778	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
390	1	T1-389	\\x636861737365	\N	779	780	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
391	1	T1-1182	\\x70c3aa63686520c3a0206c61206c69676e65	\N	781	782	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
392	1	T1-425	\\x70c3aa63686520656e2065617520646f756365	\N	783	784	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
394	1	T1-435	\\x7374727563747572652061646d696e697374726174697665	\N	787	788	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
395	1	T1-132	\\xc3a976616c756174696f6e20666f6e6369c3a87265	\N	789	790	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
396	1	T1-944	\\x7072697365206427656175	\N	791	792	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
397	1	T1-134	\\x7075c3a9726963756c7472696365	\N	793	794	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
401	1	T1-603	\\x626f6973736f6e20616c636f6f6c6973c3a965	\N	801	802	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
402	1	T1-137	\\x6368c3b46d616765207061727469656c	\N	803	804	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
403	1	T1-862	\\x6368c3b46d616765	\N	805	806	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
405	1	T1-866	\\x6173736f63696174696f6e2073796e646963616c652064652070726f707269c3a9746169726573	\N	809	810	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
406	1	T1-139	\\x6c69676e65206465206368656d696e20646520666572	\N	811	812	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
407	1	T1-164	\\x7472616e73706f727420666572726f766961697265	\N	813	814	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
459	1	T1-1019	\\x62696f2d636172627572616e74	\N	917	918	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
408	1	T1-140	\\x707265737365	\N	815	816	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
409	1	T1-813	\\x64c3a970c3b474206cc3a967616c	\N	817	818	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
410	1	T1-1186	\\x6a6f75726e616c69737465	\N	819	820	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
411	1	T1-968	\\x7075626c69636174696f6e20696e7465726e65	\N	821	822	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
412	1	T1-576	\\x7265706f727465722070686f746f677261706865	\N	823	824	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
413	1	T1-1358	\\x7072657373652072c3a967696f6e616c65	\N	825	826	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
414	1	T1-141	\\x646973747269627574696f6e206465732070726978	\N	827	828	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
415	1	T1-142	\\x616772657373696f6e2073657875656c6c65	\N	829	830	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
439	1	T1-584	\\x6d6172696e2070c3aa6368657572	\N	877	878	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
440	1	T1-157	\\x706f6c6c7574696f6e2076697375656c6c65	\N	879	880	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
441	1	T1-286	\\x706f6c6c7574696f6e	\N	881	882	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
442	1	T1-1132	\\x6a75726964696374696f6e	\N	883	884	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
443	1	T1-159	\\x636f6e7365696c206d756e69636970616c	\N	885	886	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
444	1	T1-1157	\\x6f7267616e652064c3a96c6962c3a972616e74	\N	887	888	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
445	1	T1-1251	\\x6d61697265	\N	889	890	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
446	1	T1-1188	\\x636f6e7365696c6c6572206d756e69636970616c	\N	891	892	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
447	1	T1-160	\\x617263686974656374757265	\N	893	894	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
1289	1	T1-910	\\x6861726b69	\N	2577	2578	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1616	1	T3-8	\\x6167656e6461	\N	3231	3232	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
452	1	T1-163	\\x73796e646963206465206661696c6c697465	\N	903	904	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
454	1	T1-493	\\x6c696169736f6e2066657272c3a96520696e7465726e6174696f6e616c65	\N	907	908	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
455	1	T1-165	\\x6d697261636c65	\N	909	910	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
456	1	T1-1210	\\x7669652073706972697475656c6c65	\N	911	912	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
457	1	T1-166	\\xc3a96e6572676965	\N	913	914	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
460	1	T1-1373	\\xc3a96e6572676965206e75636cc3a961697265	\N	919	920	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
461	1	T1-452	\\xc3a9636f6e6f6d6965206427c3a96e6572676965	\N	921	922	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
462	1	T1-744	\\xc3a971756970656d656e74206d696c697461697265	\N	923	924	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
1740	1	T3-108	\\x66696368696572	\N	3479	3480	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
425	1	T1-887	\\x696e7374616c6c6174696f6e20636c617373c3a965	\N	849	850	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
426	1	T1-1000	\\x7365636f757273	\N	851	852	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
427	1	T1-511	\\x64c3a976656c6f7070656d656e742064757261626c65	\N	853	854	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
428	1	T1-920	\\x61696465207075626c697175652061757820656e747265707269736573	\N	855	856	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
429	1	T1-939	\\x706f6c69636520c3a9636f6e6f6d69717565	\N	857	858	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
430	1	T1-1234	\\x6c6162656c206465207175616c6974c3a9	\N	859	860	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
431	1	T1-568	\\x70726978	\N	861	862	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
432	1	T1-149	\\x65617520736f757465727261696e65	\N	863	864	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
1343	1	T1-1318	\\x6d6572	\N	2685	2686	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
464	1	T1-506	\\xc3a97461742d6d616a6f72	\N	927	928	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
465	1	T1-905	\\x736572766963652073616e697461697265206d696c697461697265	\N	929	930	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
466	1	T1-168	\\x64c3a9636f726174696f6e2075726261696e65	\N	931	932	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
467	1	T1-169	\\x636f6e74656e746965757820c3a96c6563746f72616c	\N	933	934	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
468	1	T1-1154	\\x636f6e74656e74696575782061646d696e6973747261746966	\N	935	936	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
469	1	T1-1439	\\x63726f79616e63657320657420736369656e63657320706172616c6cc3a86c6573	\N	937	938	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
470	1	T1-1111	\\x617374726f6c6f676965	\N	939	940	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
471	1	T1-784	\\x616c6368696d6965	\N	941	942	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
472	1	T1-386	\\x67726170686f6c6f676965	\N	943	944	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
473	1	T1-901	\\x73706972697469736d65	\N	945	946	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
474	1	T1-173	\\x726563686572636865206d696e69c3a87265	\N	947	948	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
475	1	T1-175	\\x617373656d626cc3a965206e6174696f6e616c65	\N	949	950	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
476	1	T1-902	\\x7061726c656d656e7461697265	\N	951	952	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
634	1	T1-267	\\x6175746f6d6f62696c69737465	\N	1267	1268	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
480	1	T1-745	\\x7472616e73706f7274206d756c74696d6f64616c	\N	959	960	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
481	1	T1-179	\\x73616c617269c3a92061677269636f6c65	\N	961	962	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:01
482	1	T1-419	\\x6167726963756c74657572	\N	963	964	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
483	1	T1-667	\\x6d6967726174696f6e20736169736f6e6e69c3a87265	\N	965	966	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
484	1	T1-180	\\x68616c7465206761726465726965	\N	967	968	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
485	1	T1-181	\\x70657273c3a9637574696f6e2072656c69676965757365	\N	969	970	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
487	1	T1-183	\\x61637469766974c3a9206dc3a96e6167c3a87265	\N	973	974	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
488	1	T1-185	\\xc3a97461626c697373656d656e742064652073616e74c3a9	\N	975	976	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
489	1	T1-344	\\x73616e74c3a9	\N	977	978	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
490	1	T1-1457	\\xc3a97461626c697373656d656e742073616e6974616972652070726976c3a9	\N	979	980	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
764	1	T1-365	\\x706861726d616369656e	\N	1527	1528	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:06
382	1	T1-125	\\x726566756765206465206d6f6e7461676e65	\N	763	764	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
383	1	T1-909	\\xc3a971756970656d656e7420746f757269737469717565	\N	765	766	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
384	1	T1-176	\\x6d616368696e652064616e67657265757365	\N	767	768	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
491	1	T1-950	\\x636f6e737472756374696f6e20686f73706974616c69c3a87265	\N	981	982	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
495	1	T1-894	\\x73616e61746f7269756d	\N	989	990	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
496	1	T1-560	\\xc3a971756970656d656e74206dc3a96469636f20636869727572676963616c	\N	991	992	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
497	1	T1-651	\\x656e736569676e656d656e7420686f73706974616c696572	\N	993	994	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
498	1	T1-1414	\\x666f6e6374696f6e6e6169726520686f73706974616c696572	\N	995	996	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
1369	1	T1-1164	\\x766f6c	\N	2737	2738	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
513	1	T1-1090	\\x7472616e73706f7274206d61726974696d65	\N	1025	1026	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
514	1	T1-198	\\x63617274652073636f6c61697265	\N	1027	1028	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
516	1	T1-406	\\x6173736f63696174696f6e2066616d696c69616c65	\N	1031	1032	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
517	1	T1-201	\\x706f726e6f67726170686965	\N	1033	1034	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
518	1	T1-1396	\\x706f6c69636520646573206d6f65757273	\N	1035	1036	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
519	1	T1-1110	\\x63696ec3a96d61	\N	1037	1038	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
520	1	T1-202	\\x63616c616d6974c3a92061677269636f6c65	\N	1039	1040	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
521	1	T1-309	\\x73c3a96368657265737365	\N	1041	1042	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
522	1	T1-854	\\x67656cc3a965	\N	1043	1044	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
523	1	T1-683	\\x6772c3aa6c65	\N	1045	1046	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
524	1	T1-663	\\x6dc3a974c3a96f726f6c6f676965	\N	1047	1048	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
525	1	T1-447	\\x706c616e74652074657874696c65	\N	1049	1050	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
526	1	T1-205	\\x7461786520737572206c6520636869666672652064276166666169726573	\N	1051	1052	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
527	1	T1-206	\\x64c3a970656e7365206427696e76657374697373656d656e74	\N	1053	1054	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
528	1	T1-207	\\xc3a96d616e6369706174696f6e	\N	1055	1056	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
529	1	T1-489	\\x64726f6974206465206c612066616d696c6c65	\N	1057	1058	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
619	1	T1-1060	\\x6a6575782d65742d7061726973	\N	1237	1238	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
530	1	T1-208	\\xc3a96475636174696f6e2073657875656c6c65	\N	1059	1060	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
531	1	T1-940	\\x70726f6772616d6d652073636f6c61697265	\N	1061	1062	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
532	1	T1-1131	\\x6d6f62696c6965722075726261696e	\N	1063	1064	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
533	1	T1-1246	\\xc3a974616c656d656e742075726261696e	\N	1065	1066	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
534	1	T1-444	\\x7265636f6e737472756374696f6e	\N	1067	1068	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
535	1	T1-1105	\\x7365637465757220736175766567617264c3a9	\N	1069	1070	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
536	1	T1-778	\\x666c657572697373656d656e74	\N	1071	1072	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
537	1	T1-210	\\x656d706c6f79c3a9206465206d6169736f6e	\N	1073	1074	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
538	1	T1-480	\\x70726f66657373696f6e20706172746963756c69c3a87265	\N	1075	1076	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
539	1	T1-212	\\x6a757374696365206d756e69636970616c65	\N	1077	1078	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
540	1	T1-1115	\\x64726f69747320736569676e65757269617578	\N	1079	1080	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
541	1	T1-578	\\x6dc3a96469617468c3a8717565	\N	1081	1082	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
542	1	T1-693	\\x6b696f7371756520c3a0206d757369717565	\N	1083	1084	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
543	1	T1-254	\\x6172636869766573	\N	1085	1086	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
544	1	T1-871	\\x6d6169736f6e20646573206a65756e6573	\N	1087	1088	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
545	1	T1-305	\\x73616c6c65206465732066c3aa746573	\N	1089	1090	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
547	1	T1-704	\\x636f6e7365727661746f697265	\N	1093	1094	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
549	1	T1-332	\\x666f79657220727572616c	\N	1097	1098	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
416	1	T1-144	\\x656d706c6f6920646573206a65756e6573	\N	831	832	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
417	1	T1-558	\\x6a65756e65	\N	833	834	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
418	1	T1-145	\\x636c617373652064652064c3a9636f757665727465	\N	835	836	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
419	1	T1-146	\\x726176697461696c6c656d656e74	\N	837	838	1	1	1	2011-05-25 10:03:00	2012-03-07 09:41:00
420	1	T1-1150	\\x726174696f6e6e656d656e74	\N	839	840	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
421	1	T1-756	\\x6d61726368c3a9206e6f6972	\N	841	842	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
422	1	T1-1450	\\x73756273697374616e636573	\N	843	844	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
423	1	T1-1151	\\x7472616e73706f7274206465206d617469c3a87265732064616e6765726575736573	\N	845	846	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
438	1	T1-231	\\x706f7274	\N	875	876	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
1662	1	T3-135	\\x6d656e75	\N	3323	3324	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
499	1	T1-186	\\xc3a97461626c697373656d656e7420646520666f726d6174696f6e20646573206d61c3ae74726573	\N	997	998	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
500	1	T1-662	\\xc3a97461626c697373656d656e74206427656e736569676e656d656e74	\N	999	1000	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
501	1	T1-188	\\x696d70c3b47420737572206c6120666f7274756e65	\N	1001	1002	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
502	1	T1-1376	\\x66697363616c6974c3a92064657320706572736f6e6e6573	\N	1003	1004	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
503	1	T1-189	\\x7472617661696c2070726f74c3a967c3a9	\N	1005	1006	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
504	1	T1-1027	\\x7472617661696c6c6575722068616e6469636170c3a9	\N	1007	1008	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
505	1	T1-190	\\x6372c3a8636865	\N	1009	1010	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
506	1	T1-191	\\x73756963696465	\N	1011	1012	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
779	1	T1-900	\\x6d6f62696c696572	\N	1557	1558	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
584	1	T1-240	\\x6772616e646520c3a9636f6c65	\N	1167	1168	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
510	1	T1-195	\\x666f6e6374696f6e6e61697265206465206c27556e696f6e206575726f70c3a9656e6e65	\N	1019	1020	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
511	1	T1-805	\\x706572736f6e6e656c	\N	1021	1022	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
512	1	T1-196	\\x6c69676e65206d61726974696d65	\N	1023	1024	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
448	1	T1-738	\\x61726368697465637465	\N	895	896	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
449	1	T1-161	\\x7265706f7320686562646f6d616461697265	\N	897	898	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
450	1	T1-162	\\x6672756974	\N	899	900	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
451	1	T1-631	\\x686f72746963756c74757265	\N	901	902	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
453	1	T1-1232	\\x6661696c6c697465	\N	905	906	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
436	1	T1-154	\\x6173736f63696174696f6e20646520746f757269736d65	\N	871	872	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
437	1	T1-156	\\x706f72742064652070c3aa636865	\N	873	874	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
492	1	T1-1263	\\x6cc3a970726f7365726965	\N	983	984	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
493	1	T1-1062	\\x68c3b4706974616c2070737963686961747269717565	\N	985	986	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
578	1	T1-525	\\x6a7573746963652070c3a96e616c65	\N	1155	1156	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
494	1	T1-329	\\xc3a97461626c697373656d656e74207075626c6963206427686f73706974616c69736174696f6e	\N	987	988	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
548	1	T1-1459	\\x6269626c696f7468c3a8717565	\N	1095	1096	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
550	1	T1-1034	\\x63656e74726520646520646f63756d656e746174696f6e	\N	1099	1100	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
551	1	T1-214	\\x6d616c616469652073657875656c6c656d656e74207472616e736d69737369626c65	\N	1101	1102	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
552	1	T1-215	\\x6162616e646f6e2064652066616d696c6c65	\N	1103	1104	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
553	1	T1-217	\\x62617465617520646520706c616973616e6365	\N	1105	1106	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:02
554	1	T1-218	\\x6167656e742064652064726f69742070726976c3a9	\N	1107	1108	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
558	1	T1-639	\\x73706f7274	\N	1115	1116	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
559	1	T1-222	\\x766f796167652073636f6c61697265	\N	1117	1118	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
560	1	T1-223	\\x6561752d64652d766965	\N	1119	1120	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
561	1	T1-225	\\x68c3b474656c20646520746f757269736d65	\N	1121	1122	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
562	1	T1-227	\\x656e74726570726973652064752062c3a274696d656e74	\N	1123	1124	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
563	1	T1-915	\\x6d657373616765726965	\N	1125	1126	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
564	1	T1-1350	\\x7265646576616e6365207061726166697363616c65	\N	1127	1128	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
565	1	T1-230	\\x6f7267616e69736d652064652073c3a96375726974c3a920736f6369616c65	\N	1129	1130	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
566	1	T1-1214	\\xc3a96c656374696f6e20736f6369616c65	\N	1131	1132	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
567	1	T1-597	\\x706f7274206d61726974696d65	\N	1133	1134	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
568	1	T1-1434	\\x646f636b6572	\N	1135	1136	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
569	1	T1-567	\\x706f727420646520636f6d6d65726365	\N	1137	1138	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
570	1	T1-948	\\x706f727420666c757669616c	\N	1139	1140	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
571	1	T1-1253	\\x706f727420646520706c616973616e6365	\N	1141	1142	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
572	1	T1-928	\\x6f6666696369657220646520706f7274	\N	1143	1144	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
573	1	T1-1206	\\x766f69652066657272c3a96520646573207175616973	\N	1145	1146	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
698	1	T1-322	\\x6cc3a8707265	\N	1395	1396	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
574	1	T1-232	\\x656e736569676e656d656e742072656c696769657578	\N	1147	1148	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
576	1	T1-234	\\x6f66667265206427656d706c6f69	\N	1151	1152	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
577	1	T1-235	\\x6372696d696e616c6974c3a9	\N	1153	1154	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
580	1	T1-1412	\\x746f757269736d6520736f6369616c	\N	1159	1160	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
581	1	T1-238	\\x76657220c3a020736f6965	\N	1161	1162	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
583	1	T1-718	\\x66c3a96f64616c6974c3a9	\N	1165	1166	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
585	1	T1-241	\\x696d70c3b47473206c6f63617578	\N	1169	1170	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:02
586	1	T1-1316	\\x6f6374726f69	\N	1171	1172	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
587	1	T1-1016	\\x696d70c3b474732070726f76696e6369617578206427416e6369656e2052c3a967696d65	\N	1173	1174	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
588	1	T1-641	\\x64726f697420646520706c616365	\N	1175	1176	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
589	1	T1-298	\\x64726f6974206465732070617576726573	\N	1177	1178	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
590	1	T1-1002	\\x746178652064652073c3a96a6f7572	\N	1179	1180	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
591	1	T1-1298	\\x7461786520642768616269746174696f6e	\N	1181	1182	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
592	1	T1-243	\\x6368656e616c	\N	1183	1184	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
593	1	T1-244	\\x696e64757374726965206167726f20616c696d656e7461697265	\N	1185	1186	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
594	1	T1-869	\\x636f6d6d6572636520616c696d656e7461697265	\N	1187	1188	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
595	1	T1-1092	\\x61626174746f6972	\N	1189	1190	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
596	1	T1-245	\\x7472616e73706f72742073636f6c61697265	\N	1191	1192	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
848	1	T1-429	\\x61637175697474656d656e74	\N	1695	1696	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
1775	1	T3-42	\\x6361727465	\N	3549	3550	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
611	1	T1-251	\\x656e736569676e656d656e7420737570c3a97269657572	\N	1221	1222	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
612	1	T1-294	\\x64726f6974732064652073636f6c61726974c3a9	\N	1223	1224	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
613	1	T1-586	\\x746974726520756e6976657273697461697265	\N	1225	1226	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
614	1	T1-976	\\x74726f75626c6520756e6976657273697461697265	\N	1227	1228	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
615	1	T1-252	\\x64726f697473206427757361676520666f7265737469657273	\N	1229	1230	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
616	1	T1-1324	\\x7573616765732061677269636f6c6573206c6f63617578	\N	1231	1232	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
617	1	T1-637	\\x6c6f7420646520636861737365	\N	1233	1234	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
618	1	T1-253	\\x706f6c69636520646573206a657578	\N	1235	1236	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
1188	1	T1-1379	\\x666f6e646174696f6e	\N	2375	2376	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
597	1	T1-1296	\\x6f6575767265732073636f6c6169726573	\N	1193	1194	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
598	1	T1-1273	\\x7472616e73706f727420656e20636f6d6d756e	\N	1195	1196	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
599	1	T1-1405	\\x656e736569676e656d656e742061677269636f6c65	\N	1197	1198	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
600	1	T1-247	\\x6f66666963696572206d696c697461697265	\N	1199	1200	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
601	1	T1-1103	\\x6d696c697461697265	\N	1201	1202	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
602	1	T1-248	\\x6167656e742064652073c3a96375726974c3a9	\N	1203	1204	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
605	1	T1-249	\\x70726f706f7320737562766572736966	\N	1209	1210	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
606	1	T1-770	\\x6d696c69636573	\N	1211	1212	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
607	1	T1-1293	\\x6d6f62696c69736174696f6e	\N	1213	1214	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
608	1	T1-966	\\x706f6c696365206d696c697461697265	\N	1215	1216	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
684	1	T1-303	\\x68696e646f7569736d65	\N	1367	1368	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
661	1	T1-285	\\x7265636f6e6e61697373616e636573	\N	1321	1322	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
662	1	T1-1074	\\x656e7669726f6e6e656d656e74	\N	1323	1324	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
663	1	T1-714	\\x706f6c6c7574696f6e2061746d6f737068c3a97269717565	\N	1325	1326	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
664	1	T1-1420	\\x706f6c6c7574696f6e206465732065617578	\N	1327	1328	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
665	1	T1-375	\\x706f6c6c7574696f6e20736f6e6f7265	\N	1329	1330	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
666	1	T1-327	\\x706f6c6c7574696f6e206465206c61206d6572	\N	1331	1332	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
667	1	T1-1145	\\x696e64757374726965206368696d69717565	\N	1333	1334	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
668	1	T1-289	\\x70726f74656374696f6e206465732076c3a967c3a974617578	\N	1335	1336	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
433	1	T1-695	\\x656175	\N	865	866	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
434	1	T1-152	\\x63616e636572	\N	867	868	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
609	1	T1-1135	\\x67656e6461726d65726965	\N	1217	1218	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
610	1	T1-301	\\x61747465696e746520c3a0206c612073c3bb726574c3a9206465206c27c389746174	\N	1219	1220	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
639	1	T1-1297	\\x616e746973c3a96d697469736d65	\N	1277	1278	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
640	1	T1-270	\\x72c3a9636c7573696f6e206372696d696e656c6c65	\N	1279	1280	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
641	1	T1-615	\\x7065696e65	\N	1281	1282	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
642	1	T1-271	\\x676172646520c3a020767565	\N	1283	1284	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
643	1	T1-468	\\x706f6c696365206a756469636961697265	\N	1285	1286	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
644	1	T1-272	\\x636f6e737472756374696f6e2061c3a9726f6e61757469717565	\N	1287	1288	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
645	1	T1-849	\\x696e64757374726965206dc3a963616e69717565	\N	1289	1290	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
646	1	T1-1406	\\x61c3a9726f6e6566	\N	1291	1292	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
647	1	T1-273	\\x68c3b474656c2064752064c3a970617274656d656e74	\N	1293	1294	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
983	1	T1-1381	\\x636f6e74726176656e74696f6e	\N	1965	1966	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
507	1	T1-192	\\x6173737572616e636520766965696c6c65737365	\N	1013	1014	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
508	1	T1-404	\\x706572736f6e6e6520c3a267c3a965	\N	1015	1016	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
509	1	T1-193	\\x636f6e74656e74696575782066697363616c	\N	1017	1018	1	1	1	2011-05-25 10:03:02	2012-03-07 09:41:01
621	1	T1-256	\\x626f75727365206427c3a97475646573	\N	1241	1242	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
622	1	T1-257	\\x70726573627974c3a87265	\N	1243	1244	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
623	1	T1-258	\\x73796e64696361742070726f66657373696f6e6e656c	\N	1245	1246	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
624	1	T1-847	\\x72656c6174696f6e73206465207472617661696c	\N	1247	1248	1	1	1	2011-05-25 10:03:03	2012-03-07 09:41:03
625	1	T1-259	\\x6368697275726769656e2064656e7469737465	\N	1249	1250	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
626	1	T1-260	\\x61646d696e697374726174696f6e2064c3a970617274656d656e74616c65	\N	1251	1252	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
672	1	T1-1130	\\x6c6f79657220696d6d6f62696c696572	\N	1343	1344	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
673	1	T1-656	\\x6c6f67656d656e7420696e646976696475656c	\N	1345	1346	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
674	1	T1-772	\\x6c6f67656d656e7420636f6c6c6563746966	\N	1347	1348	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
675	1	T1-302	\\x657870756c73696f6e206c6f636174697665	\N	1349	1350	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
676	1	T1-291	\\x6173737572616e63652064c3a963c3a873	\N	1351	1352	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
677	1	T1-292	\\x736f6e646167652067c3a96f6c6f6769717565	\N	1353	1354	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
678	1	T1-293	\\xc3a9636f6275616765	\N	1355	1356	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
680	1	T1-295	\\x6672617564652066697363616c65	\N	1359	1360	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
681	1	T1-296	\\x657363726f717565726965	\N	1361	1362	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
683	1	T1-299	\\x726573736f7274206a756469636961697265	\N	1365	1366	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
628	1	T1-262	\\x636f7276c3a96520726f79616c65	\N	1255	1256	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
629	1	T1-263	\\x636169737365206427c3a9706172676e65	\N	1257	1258	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
959	1	T1-549	\\x61697265206465206a657578	\N	1917	1918	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
630	1	T1-314	\\xc3a97461626c697373656d656e7420646520637265646974	\N	1259	1260	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
632	1	T1-264	\\x6a75727920642761737369736573	\N	1263	1264	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
633	1	T1-265	\\xc3a9636865632073636f6c61697265	\N	1265	1266	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
638	1	T1-281	\\x6a756461c3af736d65	\N	1275	1276	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:03
691	1	T1-313	\\x6a7567652061646d696e6973747261746966	\N	1381	1382	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
692	1	T1-622	\\x7072c3aa74657572	\N	1383	1384	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
693	1	T1-1386	\\x62616e717565206d757475616c69737465	\N	1385	1386	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
694	1	T1-315	\\x62c3a274696d656e7420696e647573747269656c	\N	1387	1388	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
695	1	T1-316	\\x636f7272757074696f6e	\N	1389	1390	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
696	1	T1-888	\\x66696e616e63657320636f6d6d756e616c6573	\N	1391	1392	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
697	1	T1-320	\\x746572726f7269736d65	\N	1393	1394	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
699	1	T1-323	\\x6167726963756c747572652062696f6c6f6769717565	\N	1397	1398	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
1601	1	T3-53	\\x636174616c6f677565	\N	3201	3202	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
648	1	T1-274	\\x6d61726368c3a92064652064657461696c	\N	1295	1296	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
649	1	T1-1385	\\x636f6c706f7274657572	\N	1297	1298	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
650	1	T1-1078	\\x6d61726368616e6420666f7261696e	\N	1299	1300	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
651	1	T1-275	\\xc3a9746174732067c3a96ec3a972617578	\N	1301	1302	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
652	1	T1-276	\\x64c3a96c6f63616c69736174696f6e	\N	1303	1304	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
653	1	T1-278	\\x706f6c696365207365636f757273	\N	1305	1306	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
655	1	T1-1423	\\x6f7267616e69736174696f6e2073616e697461697265	\N	1309	1310	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
656	1	T1-280	\\x656e736569676e656d656e7420636f6d6d65726369616c	\N	1311	1312	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
657	1	T1-1331	\\x656e736569676e656d656e742070726f66657373696f6e6e656c	\N	1313	1314	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
659	1	T1-282	\\x64726f69747320686f6e6f7269666971756573	\N	1317	1318	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
660	1	T1-283	\\x6f6cc3a96167696e657578	\N	1319	1320	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
685	1	T1-304	\\x63656e74726520646520666f726d6174696f6e	\N	1369	1370	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
686	1	T1-1196	\\x666f726d6174696f6e2070726f66657373696f6e6e656c6c65	\N	1371	1372	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
687	1	T1-306	\\x656e72656769737472656d656e74	\N	1373	1374	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
688	1	T1-308	\\x636f6d6d756e69636174696f6e20696e737469747574696f6e6e656c6c65	\N	1375	1376	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
689	1	T1-310	\\x726570726f64756374696f6e20616e696d616c65	\N	1377	1378	1	1	1	2011-05-25 10:03:04	2012-03-07 09:41:04
700	1	T1-324	\\x63616d70206427696e7465726e656d656e74	\N	1399	1400	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
728	1	T1-748	\\x68796769c3a86e65	\N	1455	1456	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
1072	1	T1-611	\\x6c6963656e6369656d656e74	\N	2143	2144	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
704	1	T1-679	\\x7472616e73706f727420666c757669616c	\N	1407	1408	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
705	1	T1-328	\\x6e6176697265	\N	1409	1410	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
706	1	T1-330	\\x6372c3a96174696f6e20617564696f76697375656c6c65	\N	1411	1412	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
795	1	T1-1357	\\x6661756e652073617576616765	\N	1589	1590	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
707	1	T1-331	\\x7175616c696669636174696f6e20636f7272656374696f6e6e656c6c65	\N	1413	1414	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
708	1	T1-1004	\\x726573746175726174696f6e20646573207465727261696e7320656e206d6f6e7461676e65	\N	1415	1416	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
709	1	T1-335	\\x6469736369706c696e652073706f7274697665	\N	1417	1418	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
710	1	T1-687	\\x746f757269736d65206465206d6f6e7461676e65	\N	1419	1420	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
729	1	T1-1170	\\x616dc3a96c696f726174696f6e206465206c2768616269746174	\N	1457	1458	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
730	1	T1-500	\\x6d616c616469652070726f66657373696f6e6e656c6c65	\N	1459	1460	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
731	1	T1-1163	\\x68796769c3a86e6520736f6369616c65	\N	1461	1462	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
732	1	T1-702	\\x6dc3a9646563696e652070c3a96e6974656e746961697265	\N	1463	1464	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
733	1	T1-841	\\x6dc3a9646563696e65206475207472617661696c	\N	1465	1466	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
734	1	T1-998	\\x6dc3a9646563696e652073636f6c61697265	\N	1467	1468	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
735	1	T1-1330	\\x6173737572616e6365206d616c61646965	\N	1469	1470	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
737	1	T1-346	\\x6d6f6e617263686965	\N	1473	1474	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
739	1	T1-1083	\\x64c3a963656e7472616c69736174696f6e	\N	1477	1478	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
740	1	T1-1452	\\x64c3a9636f6e63656e74726174696f6e	\N	1479	1480	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
741	1	T1-348	\\x6c6f7465726965	\N	1481	1482	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
742	1	T1-349	\\x6c696d697465207465727269746f7269616c65	\N	1483	1484	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
743	1	T1-351	\\x636974c3a92061646d696e697374726174697665	\N	1485	1486	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
744	1	T1-352	\\x616e6369656e20636f6d62617474616e74	\N	1487	1488	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
745	1	T1-1312	\\x656d706c6f692072c3a973657276c3a9	\N	1489	1490	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
746	1	T1-353	\\x666f6e6374696f6e6e61697265207465727269746f7269616c	\N	1491	1492	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
702	1	T1-326	\\x766f6965206e6176696761626c65	\N	1403	1404	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
713	1	T1-337	\\x706861726d61636965	\N	1425	1426	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
716	1	T1-728	\\x70726f6475697420636f736dc3a97469717565	\N	1431	1432	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
717	1	T1-625	\\x7370c3a96369616c6974c3a920706861726d616365757469717565	\N	1433	1434	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
718	1	T1-338	\\x616374696f6e2063756c747572656c6c65	\N	1435	1436	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
719	1	T1-339	\\x6e6f6d616465	\N	1437	1438	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
720	1	T1-1128	\\x616972652064652073746174696f6e6e656d656e74	\N	1439	1440	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
721	1	T1-340	\\x6c6f742064652070c3aa636865	\N	1441	1442	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
1301	1	T1-947	\\x736f6c646573	\N	2601	2602	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
778	1	T1-416	\\x65737061636520646f6d65737469717565	\N	1555	1556	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
780	1	T1-374	\\x72c3a96d756ec3a9726174696f6e	\N	1559	1560	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
782	1	T1-379	\\x656e736569676e656d656e7420c3a96cc3a96d656e7461697265	\N	1563	1564	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
783	1	T1-380	\\x62616c207075626c6963	\N	1565	1566	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
784	1	T1-381	\\x70c3aa6368657572	\N	1567	1568	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
785	1	T1-382	\\x636c616e64657374696e6974c3a9	\N	1569	1570	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
786	1	T1-1193	\\x72c3a9736572766520666f6e6369c3a87265	\N	1571	1572	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
787	1	T1-384	\\x636f6c6f6e69736174696f6e	\N	1573	1574	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
788	1	T1-1069	\\x636f6c6f6e6965	\N	1575	1576	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
789	1	T1-385	\\x696e737469747574696f6e73	\N	1577	1578	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
790	1	T1-387	\\x6f7267616e69736d6520706172617075626c6963	\N	1579	1580	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
791	1	T1-760	\\x6173736f63696174696f6e	\N	1581	1582	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
792	1	T1-388	\\x616d656e6465	\N	1583	1584	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
796	1	T1-1114	\\x706f6c696365206465206c6120636861737365	\N	1591	1592	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
797	1	T1-886	\\x636f6e666c6974206475207472617661696c	\N	1593	1594	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
798	1	T1-392	\\x72c3a9686162696c69746174696f6e	\N	1595	1596	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
799	1	T1-393	\\x746175726f6d6163686965	\N	1597	1598	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
800	1	T1-394	\\x696e6672616374696f6e206d61726974696d65	\N	1599	1600	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
801	1	T1-395	\\x766f696520636f6d6d756e616c65	\N	1601	1602	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
802	1	T1-595	\\x6368616e74696572	\N	1603	1604	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
804	1	T1-654	\\x736f6369c3a974c3a920646520636f6e737472756374696f6e	\N	1607	1608	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
805	1	T1-1346	\\x6f7576726965722064752062c3a274696d656e74	\N	1609	1610	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
806	1	T1-975	\\x6d6f6e6f706f6c65732066697363617578	\N	1611	1612	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
807	1	T1-1086	\\x64726f6974732064652063697263756c6174696f6e	\N	1613	1614	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
808	1	T1-399	\\x616dc3a96e6167656d656e74206475206c6974746f72616c	\N	1615	1616	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
816	1	T1-405	\\x636f6e67c3a97320706179c3a973	\N	1631	1632	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
726	1	T1-1268	\\x746865726d6f636c696d617469736d65	\N	1451	1452	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
727	1	T1-343	\\x616d69616e7465	\N	1453	1454	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
756	1	T1-378	\\x64c3a96368657420616e696d616c	\N	1511	1512	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
757	1	T1-442	\\x64c3a96368657420696e647573747269656c	\N	1513	1514	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
758	1	T1-1129	\\x6f726475726573206dc3a96e6167c3a8726573	\N	1515	1516	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
759	1	T1-800	\\x64c3a96368657420686f73706974616c696572	\N	1517	1518	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
760	1	T1-722	\\x7573696e65206427696e63696ec3a9726174696f6e	\N	1519	1520	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
761	1	T1-1159	\\x64c3a9636861726765207075626c69717565	\N	1521	1522	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
762	1	T1-363	\\x636f6e6772c3a87320706f6c697469717565	\N	1523	1524	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
752	1	T1-360	\\x6c697474c3a9726174757265	\N	1503	1504	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
753	1	T1-361	\\x6672616e63206d61c3a76f6e	\N	1505	1506	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
754	1	T1-370	\\x736f6369c3a974c3a92073656372c3a87465	\N	1507	1508	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
755	1	T1-879	\\x76696374696d6520646520677565727265	\N	1509	1510	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
817	1	T1-411	\\x66616d696c6c65	\N	1633	1634	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
818	1	T1-407	\\x617373656d626cc3a9652067c3a96ec3a972616c6520647520636c657267c3a9	\N	1635	1636	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
819	1	T1-408	\\x616d6e6973746965	\N	1637	1638	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
820	1	T1-409	\\x6f66666963696572206465206a757374696365206427416e6369656e2052c3a967696d65	\N	1639	1640	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
821	1	T1-1417	\\x6167656e74206465206c2761646d696e697374726174696f6e20726f79616c65	\N	1641	1642	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
822	1	T1-780	\\x6f666669636573	\N	1643	1644	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
908	1	T1-477	\\x6578747261646974696f6e	\N	1815	1816	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
772	1	T1-371	\\x61696465206a756469636961697265	\N	1543	1544	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
775	1	T1-1042	\\x616e696d616c20646520636f6d7061676e6965	\N	1549	1550	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
776	1	T1-1252	\\x76c3aa74656d656e74	\N	1551	1552	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
777	1	T1-686	\\x636f6d70746162696c6974c3a92070726976c3a965	\N	1553	1554	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
809	1	T1-1213	\\x7265626f6973656d656e74	\N	1617	1618	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
810	1	T1-874	\\x636f6e7365696c2064276172726f6e64697373656d656e74	\N	1619	1620	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
811	1	T1-402	\\x666f7263657320616c6c69c3a96573	\N	1621	1622	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
812	1	T1-403	\\x6573706163652076657274	\N	1623	1624	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
813	1	T1-1057	\\x70617263206e61747572656c	\N	1625	1626	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
814	1	T1-591	\\x706c6163656d656e742066616d696c69616c	\N	1627	1628	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
834	1	T1-546	\\xc3a96c6576657572	\N	1667	1668	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
1691	1	T3-147	\\x6f626a6574	\N	3381	3382	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
825	1	T1-1415	\\x6d6169736f6e2066616d696c69616c6520646520766163616e636573	\N	1649	1650	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
826	1	T1-533	\\x616666616972652066616d696c69616c65	\N	1651	1652	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
827	1	T1-790	\\x70726573746174696f6e2066616d696c69616c65	\N	1653	1654	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
828	1	T1-412	\\x616c636f6f6c69717565	\N	1655	1656	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
829	1	T1-413	\\x72c3a973656175206427696e666f726d6174696f6e	\N	1657	1658	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
830	1	T1-414	\\x6578616d656e2070726f66657373696f6e6e656c	\N	1659	1660	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
831	1	T1-415	\\x61706963756c74757265	\N	1661	1662	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
832	1	T1-417	\\x636c616e64657374696e	\N	1663	1664	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
835	1	T1-449	\\x6a65756e65206167726963756c74657572	\N	1669	1670	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
836	1	T1-1209	\\x6578706c6f6974616e742061677269636f6c65	\N	1671	1672	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
837	1	T1-1107	\\x626572676572	\N	1673	1674	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
838	1	T1-986	\\x6171756163756c74757265	\N	1675	1676	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
839	1	T1-653	\\x70c3aa636865	\N	1677	1678	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
840	1	T1-785	\\x6d61726368c3a92061677269636f6c65	\N	1679	1680	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
842	1	T1-423	\\x6f636375706174696f6e2074656d706f726169726520647520646f6d61696e65207075626c6963	\N	1683	1684	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
843	1	T1-424	\\x6d696e69737472652064752063756c7465	\N	1685	1686	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
844	1	T1-426	\\x73616e7320646f6d6963696c652066697865	\N	1687	1688	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
845	1	T1-427	\\x666f7274696669636174696f6e	\N	1689	1690	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
847	1	T1-559	\\x706f70756c6174696f6e2073636f6c61697265	\N	1693	1694	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
849	1	T1-1242	\\x64c3a9636973696f6e206465206a757374696365	\N	1697	1698	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
850	1	T1-430	\\xc3a96e6572676965206879647261756c69717565	\N	1699	1700	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
851	1	T1-431	\\x636f6d6d657263652064652064c3a97461696c	\N	1701	1702	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
1450	1	T2-29	\\x636f6f70c3a9726174696f6e	\N	2899	2900	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
852	1	T1-433	\\x6c616e67756520c3a97472616e67c3a87265	\N	1703	1704	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
853	1	T1-434	\\x65786f6ec3a9726174696f6e2066697363616c65	\N	1705	1706	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
854	1	T1-1109	\\xc3a97461626c697373656d656e74207075626c6963206e6174696f6e616c	\N	1707	1708	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
855	1	T1-771	\\x6f7267616e69736d652061646d696e6973747261746966206575726f70c3a9656e	\N	1709	1710	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
856	1	T1-1044	\\x61646d696e697374726174696f6e2063656e7472616c65	\N	1711	1712	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
857	1	T1-575	\\x7365727669636520657874c3a97269657572	\N	1713	1714	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
858	1	T1-1422	\\xc3a97461626c697373656d656e74207075626c6963206c6f63616c	\N	1715	1716	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
860	1	T1-1040	\\x61646d696e697374726174696f6e206c6f63616c65206427416e6369656e2052c3a967696d65	\N	1719	1720	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
861	1	T1-1088	\\x736572766963652064c3a9636f6e63656e7472c3a9	\N	1721	1722	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
862	1	T1-621	\\x61646d696e697374726174696f6e207072c3a9666563746f72616c65	\N	1723	1724	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
863	1	T1-437	\\x6167656e7420696d6d6f62696c696572	\N	1725	1726	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
864	1	T1-706	\\x6d61726368c3a920696d6d6f62696c696572	\N	1727	1728	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
865	1	T1-438	\\x636f6e7365696c6c65722064276172726f6e64697373656d656e74	\N	1729	1730	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
866	1	T1-439	\\x706861726d616369652076c3a974c3a972696e61697265	\N	1731	1732	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
867	1	T1-440	\\x7472616e73706f727420c3a020646f73206427686f6d6d65	\N	1733	1734	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
868	1	T1-443	\\x766f69652066657272c3a965206427696e74c3a972c3aa74206c6f63616c	\N	1735	1736	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
1041	1	T1-579	\\x64656d692d736f6c6465	\N	2081	2082	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
869	1	T1-1453	\\x646f6d6d6167657320646520677565727265	\N	1737	1738	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
870	1	T1-450	\\xc3a96c656374657572	\N	1739	1740	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
875	1	T1-454	\\x736569676e6575726965	\N	1749	1750	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
876	1	T1-455	\\x6cc3a967696f6e20c3a97472616e67c3a87265	\N	1751	1752	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
877	1	T1-937	\\x646973747269627574696f6e20c3a96c6563747269717565	\N	1753	1754	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
878	1	T1-458	\\x6173736f63696174696f6e20646520646566656e7365206465206c27656e7669726f6e6e656d656e74	\N	1755	1756	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
879	1	T1-459	\\x64c3a9636f757061676520c3a96c6563746f72616c	\N	1757	1758	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
880	1	T1-645	\\x63616e746f6e	\N	1759	1760	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
954	1	T1-502	\\x7069737465206379636c61626c65	\N	1907	1908	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1042	1	T1-580	\\x6c7963c3a965	\N	2083	2084	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
881	1	T1-1338	\\x6c69657574656e616e74206465206c6f7576657465726965	\N	1761	1762	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
882	1	T1-462	\\x746178652065787472616f7264696e61697265206427416e6369656e2052c3a967696d65	\N	1763	1764	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
883	1	T1-463	\\x6361646173747265	\N	1765	1766	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
884	1	T1-1143	\\x636172746f67726170686965	\N	1767	1768	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
885	1	T1-464	\\x64c3a970656e736520646520666f6e6374696f6e6e656d656e74	\N	1769	1770	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
886	1	T1-465	\\x6368616d7020646520666f697265	\N	1771	1772	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
887	1	T1-1354	\\x6173737572616e636520696e76616c69646974c3a9	\N	1773	1774	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
888	1	T1-779	\\x6173737572616e6365206368c3b46d616765	\N	1775	1776	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
1045	1	T1-588	\\x6162616e646f6e206427656e66616e74	\N	2089	2090	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
890	1	T1-673	\\x6173737572616e63652076657576616765	\N	1779	1780	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
891	1	T1-467	\\x656e66616e7420656e206761726465	\N	1781	1782	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
892	1	T1-1054	\\x696e7465726469742064652073c3a96a6f7572	\N	1783	1784	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
893	1	T1-652	\\x696e646976696475207265636865726368c3a9	\N	1785	1786	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
894	1	T1-1411	\\x6172726573746174696f6e	\N	1787	1788	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
896	1	T1-629	\\x6167656e7420646520706f6c696365206a756469636961697265	\N	1791	1792	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
897	1	T1-1162	\\x6c6962c3a9726174696f6e20636f6e646974696f6e6e656c6c65	\N	1793	1794	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
898	1	T1-469	\\x66726963686520696e647573747269656c6c65	\N	1795	1796	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
1007	1	T1-1022	\\x61c3a9726f64726f6d65	\N	2013	2014	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
841	1	T1-422	\\x61646d696e697374726174696f6e2072c3a967696f6e616c65	\N	1681	1682	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
907	1	T1-710	\\x72c3a97075626c69717565	\N	1813	1814	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
909	1	T1-789	\\x70617263206465206c6f6973697273	\N	1817	1818	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
910	1	T1-585	\\x696e7374616c6c6174696f6e2073706f7274697665	\N	1819	1820	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
911	1	T1-826	\\x64c3a962697420646520626f6973736f6e73	\N	1821	1822	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
912	1	T1-933	\\x636173696e6f	\N	1823	1824	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
913	1	T1-1155	\\x6772616e6465207375726661636520636f6d6d65726369616c65	\N	1825	1826	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
914	1	T1-820	\\xc3a96469666963652063756c7475656c	\N	1827	1828	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
915	1	T1-479	\\x7365727669747564652061c3a9726f6e61757469717565	\N	1829	1830	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
919	1	T1-1052	\\x6d696e65757220646520666f6e64	\N	1837	1838	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
920	1	T1-642	\\x6d6172696e20646520636f6d6d65726365	\N	1839	1840	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
921	1	T1-963	\\x61727469737465	\N	1841	1842	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
922	1	T1-1307	\\x617373697374616e7465206d617465726e656c6c65	\N	1843	1844	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
924	1	T1-535	\\x70726f7370656374696f6e2070c3a974726f6c69c3a87265	\N	1847	1848	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
925	1	T1-1455	\\x6578706c6f69746174696f6e206d696e69c3a87265	\N	1849	1850	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
926	1	T1-482	\\x6c6f67696369656c20696e666f726d617469717565	\N	1851	1852	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
927	1	T1-903	\\x73797374c3a86d65206427696e666f726d6174696f6e	\N	1853	1854	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
928	1	T1-486	\\x74c3a96cc3a9677261706865	\N	1855	1856	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
929	1	T1-1323	\\x636f6e63696c696174657572	\N	1857	1858	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
930	1	T1-994	\\x61766f636174	\N	1859	1860	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
931	1	T1-882	\\x6772656666696572	\N	1861	1862	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
932	1	T1-534	\\x657870657274	\N	1863	1864	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
933	1	T1-488	\\x7472616e73706f72742070617220616e696d616c	\N	1865	1866	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
934	1	T1-573	\\x616e696d616c2064652062c3a274	\N	1867	1868	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
1451	1	T2-44	\\x66696e616e63656d656e74	\N	2901	2902	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
935	1	T1-955	\\x76c3a9686963756c6520c3a0207472616374696f6e20616e696d616c65	\N	1869	1870	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
936	1	T1-1284	\\x656e66616e74206e61747572656c	\N	1871	1872	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
937	1	T1-934	\\x72c3a967696d65206d617472696d6f6e69616c	\N	1873	1874	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
938	1	T1-490	\\x7075626c69636974c3a9	\N	1875	1876	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
939	1	T1-491	\\x636f6f70c3a97261746976652061677269636f6c65	\N	1877	1878	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
940	1	T1-1099	\\x6578706c6f69746174696f6e2061677269636f6c65	\N	1879	1880	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
941	1	T1-817	\\x736f6369c3a974c3a920636f6f70c3a9726174697665	\N	1881	1882	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
942	1	T1-1348	\\x64c3a963c3a873	\N	1883	1884	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
943	1	T1-494	\\xc3a97075726174696f6e	\N	1885	1886	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
944	1	T1-497	\\x696e6475737472696520647520626f6973	\N	1887	1888	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
945	1	T1-498	\\x706172656e74206427c3a96cc3a87665	\N	1889	1890	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
946	1	T1-954	\\x656e736569676e656d656e74207075626c6963	\N	1891	1892	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
947	1	T1-1306	\\x656e736569676e656d656e74207365636f6e6461697265	\N	1893	1894	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
948	1	T1-1077	\\x6578616d656e	\N	1895	1896	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1109	1	T1-655	\\x70726f64756974206c616974696572	\N	2217	2218	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
952	1	T1-574	\\x656e736569676e656d656e7420c3a02064697374616e6365	\N	1903	1904	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
953	1	T1-501	\\xc3a9706964c3a96d6965	\N	1905	1906	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
955	1	T1-1257	\\x7665686963756c6520c3a0206465757820726f756573	\N	1909	1910	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
956	1	T1-504	\\x6173736f63696174696f6e206465206d616c6661697465757273	\N	1911	1912	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
957	1	T1-505	\\x64c3a96cc3a96775c3a920636f6e73756c61697265	\N	1913	1914	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
958	1	T1-507	\\x636f6e7365696c206465206469736369706c696e65	\N	1915	1916	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
960	1	T1-1216	\\x706c616365207075626c69717565	\N	1919	1920	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
961	1	T1-960	\\x68616c6c65	\N	1921	1922	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1216	1	T1-918	\\x706170617574c3a9	\N	2431	2432	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1217	1	T1-1291	\\x6d697373696f6e73	\N	2433	2434	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
962	1	T1-884	\\x7075697473	\N	1923	1924	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
963	1	T1-1320	\\x6c61766f6972	\N	1925	1926	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
964	1	T1-570	\\x686f726c6f6765207075626c69717565	\N	1927	1928	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
965	1	T1-835	\\x666f6e7461696e65	\N	1929	1930	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
966	1	T1-921	\\x636f6e737472756374696f6e2073636f6c61697265	\N	1931	1932	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
967	1	T1-509	\\x666f726d6174696f6e207175616c696669616e7465	\N	1933	1934	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
968	1	T1-510	\\x6368617566666167652075726261696e	\N	1935	1936	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
969	1	T1-512	\\xc3a963686576696e	\N	1937	1938	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
1229	1	T1-1204	\\x656e74656e7465	\N	2457	2458	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1033	1	T1-1377	\\x61707072656e7469	\N	2065	2066	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
902	1	T1-1172	\\x6167656e74206465206c6120666f726365207075626c69717565	\N	1803	1804	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
903	1	T1-522	\\x706f6c696365206465732065617578	\N	1805	1806	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
904	1	T1-472	\\x72656d656d6272656d656e7420727572616c	\N	1807	1808	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
905	1	T1-473	\\x706572736f6e6e616c6974c3a9	\N	1809	1810	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
906	1	T1-474	\\x70726f6a657420c3a964756361746966	\N	1811	1812	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
971	1	T1-515	\\x636f7374756d65206f6666696369656c	\N	1941	1942	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
972	1	T1-516	\\x6167656e74206465206368616e6765	\N	1943	1944	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
973	1	T1-519	\\x6761726465206e6174696f6e616c65	\N	1945	1946	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
974	1	T1-520	\\x63756d756c206427656d706c6f69	\N	1947	1948	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
975	1	T1-809	\\x696e6672616374696f6e20c3a9636f6e6f6d69717565	\N	1949	1950	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
976	1	T1-521	\\x636f6d6d6572c3a7616e7420c3a97472616e676572	\N	1951	1952	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
977	1	T1-1038	\\x63686566206427656e7472657072697365	\N	1953	1954	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
978	1	T1-523	\\x6861626974617420696e73616c75627265	\N	1955	1956	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:09
980	1	T1-1014	\\x76696374696d65206427696e6672616374696f6e	\N	1959	1960	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
981	1	T1-1458	\\x64c3a96c696e7175616e6365	\N	1961	1962	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1067	1	T1-897	\\x6172747320706c6173746971756573	\N	2133	2134	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
982	1	T1-1084	\\x7175616c696669636174696f6e206372696d696e656c6c65	\N	1963	1964	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1310	1	T1-1049	\\x7461626163	\N	2619	2620	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
984	1	T1-1116	\\x696e64757374726965207370617469616c65	\N	1967	1968	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
985	1	T1-873	\\x696e64757374726965206475207665727265	\N	1969	1970	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
986	1	T1-1267	\\x696e6475737472696520647520666575	\N	1971	1972	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
987	1	T1-913	\\x6dc3a974616c6c7572676965	\N	1973	1974	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
988	1	T1-528	\\x63656e73757265	\N	1975	1976	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
989	1	T1-529	\\x696e737469747574696f6e206d6f6e61737469717565	\N	1977	1978	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
990	1	T1-530	\\x6d797374696369736d65	\N	1979	1980	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
991	1	T1-531	\\x706973636963756c74757265	\N	1981	1982	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:09
1406	1	T2-65	\\x726563727574656d656e74	\N	2811	2812	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
916	1	T1-1008	\\x73657276697475646520726164696f20c3a96c6563747269717565	\N	1831	1832	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
918	1	T1-907	\\x7472617661696c6c65757220c3a020646f6d6963696c65	\N	1835	1836	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
1182	1	T1-753	\\x6a756d656c616765	\N	2363	2364	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1002	1	T1-1171	\\x636f6c6c65637465207075626c69717565	\N	2003	2004	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1003	1	T1-547	\\x646973747269627574696f6e20706f7374616c65	\N	2005	2006	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1004	1	T1-883	\\x7472616e73706f727420726f7574696572	\N	2007	2008	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1005	1	T1-617	\\x6368617566666575722064652074617869	\N	2009	2010	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1009	1	T1-1427	\\x63697263756c6174696f6e2075726261696e65	\N	2017	2018	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1010	1	T1-1428	\\x67617265	\N	2019	2020	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1011	1	T1-697	\\x6167656e636520646520766f7961676573	\N	2021	2022	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1012	1	T1-550	\\x656e747265707269736520696e647573747269656c6c65	\N	2023	2024	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1013	1	T1-977	\\x61726dc3a965206465207465727265	\N	2025	2026	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1014	1	T1-1352	\\x61726dc3a965206465206c27616972	\N	2027	2028	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1015	1	T1-552	\\x73706972697475616c6974c3a9	\N	2029	2030	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1016	1	T1-553	\\x7365726d656e7420646520666964c3a96c6974c3a9	\N	2031	2032	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1017	1	T1-1446	\\x76696c6c65206e6f7576656c6c65	\N	2033	2034	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1018	1	T1-961	\\x7a6f6e6520642761637469766974c3a973	\N	2035	2036	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1698	1	T3-154	\\x7065726d6973	\N	3395	3396	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1119	1	T1-1361	\\x657870726f7072696174696f6e	\N	2237	2238	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:12
1040	1	T1-571	\\x6d696c6963657320626f757267656f69736573	\N	2079	2080	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1043	1	T1-582	\\x616dc3a96c696f726174696f6e2064657320736f6c73	\N	2085	2086	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1044	1	T1-587	\\x636f696666657572	\N	2087	2088	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1046	1	T1-589	\\x7472616974656d656e74206465732065617578207573c3a96573	\N	2091	2092	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1047	1	T1-590	\\x63c3a972c3a9616c65	\N	2093	2094	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1048	1	T1-592	\\x726164696f646966667573696f6e	\N	2095	2096	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1049	1	T1-593	\\x747261666963206465206d61696e2064276f6575767265	\N	2097	2098	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1050	1	T1-596	\\xc3a97461626c697373656d656e74206172746973616e616c	\N	2099	2100	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1051	1	T1-598	\\x70726f737469747574696f6e	\N	2101	2102	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1052	1	T1-599	\\x72c3a97365727665206e61747572656c6c65	\N	2103	2104	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1832	1	T3-235	\\x64c3a9636973696f6e	\N	3663	3664	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1489	1	T4-95	\\x636f6e66c3a972656e63652064652059616c746120283139343529	\N	2977	2978	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1019	1	T1-1192	\\x706f6c697469717565206465206c612076696c6c65	\N	2037	2038	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1020	1	T1-1431	\\x7a6f6e6520696e647573747269656c6c65	\N	2039	2040	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1021	1	T1-555	\\x617373656d626cc3a9652067c3a96ec3a972616c65	\N	2041	2042	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1022	1	T1-1133	\\x686162697461742075726261696e	\N	2043	2044	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1023	1	T1-1045	\\x73796e64696320646520636f70726f707269c3a974c3a9	\N	2045	2046	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1024	1	T1-671	\\x64c3a96d6f6c6974696f6e	\N	2047	2048	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1025	1	T1-1001	\\x6c6f67656d656e7420736f6369616c	\N	2049	2050	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1026	1	T1-1448	\\x6c6f67656d656e7420646520666f6e6374696f6e	\N	2051	2052	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1392	1	T1-1308	\\x73696461	\N	2783	2784	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1185	1	T1-757	\\x64726170656175	\N	2369	2370	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
899	1	T1-470	\\x6d616e69666573746174696f6e20636f6d6d65726369616c65	\N	1797	1798	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
900	1	T1-735	\\x706f6c696365206465206c612070c3aa636865	\N	1799	1800	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
901	1	T1-863	\\x706f6c696365206465732063756c746573	\N	1801	1802	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
1054	1	T1-1064	\\x6172726f6e64697373656d656e74	\N	2107	2108	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1055	1	T1-1328	\\x64697374726963742072c3a9766f6c7574696f6e6e61697265	\N	2109	2110	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1056	1	T1-1043	\\x646f6d61696e6520726f79616c	\N	2111	2112	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1057	1	T1-1035	\\x6167676c6f6dc3a9726174696f6e2075726261696e65	\N	2113	2114	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1058	1	T1-812	\\x73c3a96ec3a9636861757373c3a965	\N	2115	2116	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1059	1	T1-1372	\\x70617973	\N	2117	2118	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1060	1	T1-638	\\x67c3a96ec3a972616c6974c3a9	\N	2119	2120	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1062	1	T1-999	\\x70726f76696e6365	\N	2123	2124	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1063	1	T1-602	\\x616273656e74c3a969736d652073636f6c61697265	\N	2125	2126	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1064	1	T1-604	\\x666c657572	\N	2127	2128	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1065	1	T1-607	\\x636f757273206427656175	\N	2129	2130	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1066	1	T1-731	\\x6d757369717565	\N	2131	2132	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1068	1	T1-609	\\x656e736569676e656d656e742070c3a96e6974656e746961697265	\N	2135	2136	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1069	1	T1-610	\\x70c3a96461676f676965	\N	2137	2138	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1070	1	T1-787	\\x6269626c696f7468c3a87175652073636f6c61697265	\N	2139	2140	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1071	1	T1-1203	\\x6d6174c3a97269656c2070c3a96461676f6769717565	\N	2141	2142	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1073	1	T1-839	\\x61666661697265207072756427686f6d616c65	\N	2145	2146	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1074	1	T1-612	\\x6f7267616e69736174696f6e20696e7465726e6174696f6e616c65	\N	2147	2148	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1075	1	T1-613	\\x67616cc3a8726573	\N	2149	2150	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1076	1	T1-810	\\x6172746973616e	\N	2151	2152	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1077	1	T1-758	\\x7472617661757820666f7263c3a973	\N	2153	2154	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1078	1	T1-746	\\x7065696e6520646520737562737469747574696f6e	\N	2155	2156	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1079	1	T1-1009	\\x7065696e65206361706974616c65	\N	2157	2158	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1080	1	T1-1447	\\x7065696e6520636f72706f72656c6c65	\N	2159	2160	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1081	1	T1-1432	\\x61737369676e6174696f6e20c3a02072c3a9736964656e6365	\N	2161	2162	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1082	1	T1-619	\\x6372696d6520646520677565727265	\N	2163	2164	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1083	1	T1-620	\\x62c3a97461696c	\N	2165	2166	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1085	1	T1-815	\\x696d6d6967726174696f6e	\N	2169	2170	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1086	1	T1-1220	\\x70726f64756974206d617261c3ae63686572	\N	2171	2172	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1087	1	T1-1409	\\x6a617264696e2066616d696c69616c	\N	2173	2174	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1088	1	T1-1068	\\x677569646520696e7465727072c3a87465	\N	2175	2176	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1089	1	T1-1290	\\x63616d70696e67206361726176616e696e67	\N	2177	2178	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1090	1	T1-1144	\\x746f757269736d6520727572616c	\N	2179	2180	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1550	1	T4-89	\\x42656c6c6520c389706f717565	\N	3099	3100	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1091	1	T1-827	\\x746f757269736d652075726261696e	\N	2181	2182	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1092	1	T1-1102	\\x6369726375697420746f757269737469717565	\N	2183	2184	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1093	1	T1-1378	\\x6f7267616e69736d65206c6f63616c20646520746f757269736d65	\N	2185	2186	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1094	1	T1-1272	\\x6379636c6f746f757269736d65	\N	2187	2188	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1095	1	T1-635	\\x7472616e73706f72742073616e697461697265	\N	2189	2190	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1096	1	T1-636	\\x69736c616d	\N	2191	2192	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1097	1	T1-850	\\x68616e646973706f7274	\N	2193	2194	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1098	1	T1-1368	\\x6173736f63696174696f6e2073706f7274697665	\N	2195	2196	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1099	1	T1-798	\\x656475636174696f6e2073706f7274697665	\N	2197	2198	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1100	1	T1-640	\\x746178652070726f66657373696f6e6e656c6c65	\N	2199	2200	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1161	1	T1-726	\\x6d6f6e6e616965	\N	2321	2322	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1592	1	T4-15	\\x3765207369c3a8636c65	\N	3183	3184	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1130	1	T1-680	\\x64c3a9706f7274c3a9	\N	2259	2260	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
917	1	T1-517	\\x636f6e636965726765	\N	1833	1834	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
1129	1	T1-674	\\xc3a97461626c697373656d656e742070c3a96e6974656e746961697265	\N	2257	2258	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1038	1	T1-1199	\\x726573746175726174696f6e2073636f6c61697265	\N	2075	2076	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1039	1	T1-569	\\x6a75737469636520726f79616c65	\N	2077	2078	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1053	1	T1-600	\\x72c3a9736964656e636520646520746f757269736d65	\N	2105	2106	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:10
1311	1	T1-978	\\x6163636f756368656d656e74	\N	2621	2622	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1394	1	T1-1418	\\x7375637265	\N	2787	2788	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1102	1	T1-644	\\x626f75636865726965	\N	2203	2204	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1103	1	T1-1410	\\x7669616e6465	\N	2205	2206	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1104	1	T1-646	\\x61696465206dc3a9646963616c6520757267656e7465	\N	2207	2208	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1105	1	T1-664	\\x61726368c3a96f6c6f67696520737562617175617469717565	\N	2209	2210	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1106	1	T1-1017	\\x61726368c3a96f6c6f67696520736f75732d6d6172696e65	\N	2211	2212	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1107	1	T1-650	\\x64c3a96cc3a96775c3a920647520706572736f6e6e656c	\N	2213	2214	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1108	1	T1-1067	\\x706f6973736f6e	\N	2215	2216	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1112	1	T1-660	\\x68796d6e65206e6174696f6e616c	\N	2223	2224	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1113	1	T1-754	\\x70726f6d656e61646520706c616e74c3a965	\N	2225	2226	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1116	1	T1-715	\\x636f6c6cc3a86765	\N	2231	2232	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1117	1	T1-1287	\\xc3a9636f6c652063656e7472616c65	\N	2233	2234	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1118	1	T1-665	\\x666f75726e69747572652073636f6c61697265	\N	2235	2236	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:12
1120	1	T1-844	\\x6261757820636f6d6d65726369617578	\N	2239	2240	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:12
1121	1	T1-1235	\\x6261757820727572617578	\N	2241	2242	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:12
1123	1	T1-668	\\x706f6c696365206d756e69636970616c65	\N	2245	2246	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1124	1	T1-1136	\\x696d70c3b47420737572206c657320736f6369c3a974c3a973	\N	2247	2248	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1125	1	T1-1031	\\x7461786520642761707072656e74697373616765	\N	2249	2250	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1153	1	T1-932	\\x66726175646520636f6d6d65726369616c65	\N	2305	2306	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1154	1	T1-712	\\x68c3b474656c2064652076696c6c65	\N	2307	2308	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1155	1	T1-716	\\x6368616d616e69736d65	\N	2309	2310	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1156	1	T1-717	\\x747574656c6c65206175782070726573746174696f6e7320736f6369616c6573	\N	2311	2312	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1157	1	T1-1176	\\x747574656c6c65206a756469636961697265	\N	2313	2314	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1158	1	T1-1313	\\x64c3a96e6f6d6272656d656e7420736569676e65757269616c	\N	2315	2316	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1159	1	T1-719	\\x696e76616c696465206475207472617661696c	\N	2317	2318	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1160	1	T1-725	\\x6269656e2073706f6c69c3a9	\N	2319	2320	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1162	1	T1-729	\\xc3a96372697420737562766572736966	\N	2323	2324	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1470	1	T4-1	\\x5072c3a9686973746f697265	\N	2939	2940	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:17
38	1	T1-7	\\x636f75706520646520626f6973	\N	75	76	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
1027	1	T1-557	\\x626967616d6965	\N	2053	2054	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1028	1	T1-709	\\x6d6f7576656d656e74206465206a65756e65737365	\N	2055	2056	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1031	1	T1-699	\\x6368616e74696572206465206c61206a65756e65737365	\N	2061	2062	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1032	1	T1-833	\\xc3a974756469616e74	\N	2063	2064	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1034	1	T1-1244	\\x696e7465726e65	\N	2067	2068	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1036	1	T1-563	\\x707269736f6e6e69657220646520677565727265	\N	2071	2072	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1037	1	T1-564	\\x66616d696e65	\N	2073	2074	1	1	1	2011-05-25 10:03:09	2012-03-07 09:41:10
1218	1	T1-1183	\\x636f6e63696c65	\N	2435	2436	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1132	1	T1-682	\\x6163636964656e74206475207472617661696c	\N	2263	2264	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1133	1	T1-685	\\x696e74657276656e74696f6e206427c3a96c75	\N	2265	2266	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1134	1	T1-688	\\x616c6c6575	\N	2267	2268	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1135	1	T1-689	\\x636f6d7061676e69652063686f72c3a9677261706869717565	\N	2269	2270	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1136	1	T1-691	\\x656e7472657072697365207075626c69717565	\N	2271	2272	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1137	1	T1-692	\\x617070617265696c20726164696f20c3a96c6563747269717565	\N	2273	2274	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1138	1	T1-945	\\x7365727669636520726164696f20c3a96c6563747269717565	\N	2275	2276	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1139	1	T1-694	\\x676172646520666f72657374696572	\N	2277	2278	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1605	1	T3-56	\\x6368616e736f6e	\N	3209	3210	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1163	1	T1-730	\\x696e7374616c6c6174696f6e20726164696f6c6f6769717565	\N	2325	2326	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1164	1	T1-732	\\x6d616c616465206d656e74616c	\N	2327	2328	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1165	1	T1-733	\\x64c3a9746563746976652070726976c3a9	\N	2329	2330	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1166	1	T1-734	\\x726f75746520666f7265737469c3a87265	\N	2331	2332	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1167	1	T1-985	\\x766f69652066657272c3a965206427696e74c3a972c3aa74206e6174696f6e616c	\N	2333	2334	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1168	1	T1-737	\\x706f70756c6174696f6e2070c3a96e6974656e746961697265	\N	2335	2336	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1169	1	T1-855	\\x696e63617263c3a9726174696f6e	\N	2337	2338	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1170	1	T1-1275	\\xc3a9766173696f6e	\N	2339	2340	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1101	1	T1-1112	\\x63686172626f6e20646520626f6973	\N	2201	2202	1	1	1	2011-05-25 10:03:10	2012-03-07 09:41:11
1223	1	T1-802	\\x736572766974756465	\N	2445	2446	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1224	1	T1-864	\\x74656d706f72656c206563636cc3a9736961737469717565	\N	2447	2448	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1225	1	T1-1369	\\x68c3a972c3a9736965	\N	2449	2450	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1226	1	T1-1071	\\x636f6e636f7572732061646d696e6973747261746966	\N	2451	2452	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1227	1	T1-807	\\x7461786520666f6e6369c3a87265	\N	2453	2454	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1228	1	T1-808	\\x696e64c3a970656e64616e6365	\N	2455	2456	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1230	1	T1-814	\\x6a65752d636f6e636f757273	\N	2459	2460	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1145	1	T1-929	\\x72657472616974c3a9	\N	2289	2290	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1146	1	T1-713	\\x706f70756c6174696f6e20616374697665	\N	2291	2292	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1147	1	T1-783	\\x6174656c69657220646520636861726974c3a9	\N	2293	2294	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1148	1	T1-700	\\x747562657263756c6f7365	\N	2295	2296	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1150	1	T1-703	\\x636f75727365206869707069717565	\N	2299	2300	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1151	1	T1-705	\\x6c696169736f6e20726f757469c3a8726520696e7465726e6174696f6e616c65	\N	2301	2302	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1152	1	T1-711	\\x7075626c69636974c3a9206d656e736f6e67c3a87265	\N	2303	2304	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1459	1	T2-3	\\x6163717569736974696f6e	\N	2917	2918	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1239	1	T1-935	\\x72657374617572616e7420646520746f757269736d65	\N	2477	2478	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1325	1	T1-1011	\\x6f657566	\N	2649	2650	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1577	1	T4-18	\\x313065207369c3a8636c65	\N	3153	3154	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1240	1	T1-1347	\\xc3a97461626c697373656d656e7420696e74657264697420617578206d696e65757273	\N	2479	2480	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1241	1	T1-1270	\\x616c636f6f6c69736d65	\N	2481	2482	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1242	1	T1-828	\\x617070656c	\N	2483	2484	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1176	1	T1-993	\\x6465747465207075626c69717565	\N	2351	2352	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1177	1	T1-1245	\\x726563657474652066697363616c65	\N	2353	2354	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1179	1	T1-749	\\x676172646520706172746963756c696572	\N	2357	2358	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1180	1	T1-1036	\\x616dc3a96e6167656d656e7420666f72657374696572	\N	2359	2360	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1181	1	T1-751	\\x6f75747261676520617578206d6f65757273	\N	2361	2362	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1183	1	T1-856	\\x63616e616c69736174696f6e	\N	2365	2366	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1184	1	T1-1295	\\x646973747269627574696f6e2064652067617a	\N	2367	2368	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1186	1	T1-759	\\x66617578206d6f6e6e6179616765	\N	2371	2372	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1187	1	T1-997	\\x6173736f63696174696f6e207265636f6e6e75652064277574696c6974c3a9207075626c69717565	\N	2373	2374	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1192	1	T1-1454	\\x6d6967726174696f6e20727572616c65	\N	2383	2384	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1194	1	T1-795	\\x6a7567652064652070616978	\N	2387	2388	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:13
1196	1	T1-768	\\x74c3a96c6578	\N	2391	2392	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1213	1	T1-811	\\x6661627269717565206427c3a9676c697365	\N	2425	2426	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1214	1	T1-1053	\\x61646d696e697374726174696f6e2064696f63c3a97361696e65	\N	2427	2428	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1215	1	T1-967	\\x73c3a96d696e616972652072656c696769657578	\N	2429	2430	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1220	1	T1-797	\\x6d616c6164696520c3a02064c3a9636c61726174696f6e206f626c696761746f697265	\N	2439	2440	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1221	1	T1-1363	\\x6167656e742064276173737572616e636573	\N	2441	2442	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1222	1	T1-949	\\x736f6369c3a974c3a9206465207365727669636573	\N	2443	2444	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1246	1	T1-831	\\x72657072c3a973656e746174696f6e206469706c6f6d617469717565	\N	2491	2492	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1247	1	T1-836	\\x736f7263656c6c65726965	\N	2493	2494	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1248	1	T1-1364	\\x6d61676965	\N	2495	2496	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1249	1	T1-840	\\x636f6d6d6973736169726520656e7175c3aa74657572	\N	2497	2498	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1250	1	T1-843	\\x636f6e7365696c2064652066616d696c6c65	\N	2499	2500	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1794	1	T3-40	\\x63617269636174757265	\N	3587	3588	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
36	1	T1-376	\\x66696e616e636573206c6f63616c6573	\N	71	72	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
1251	1	T1-846	\\x636f6d6d6572c3a7616e74	\N	2501	2502	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1252	1	T1-1208	\\x636f6d6974c3a9206427656e7472657072697365	\N	2503	2504	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1253	1	T1-987	\\x696e73657274696f6e20736f6369616c65	\N	2505	2506	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1254	1	T1-925	\\x6d6174c3a97269656c2061677269636f6c65	\N	2507	2508	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1255	1	T1-1217	\\x6f727068656c696e6174	\N	2509	2510	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1256	1	T1-1281	\\x61646f7074696f6e	\N	2511	2512	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1257	1	T1-858	\\x7a6f6e65206427616dc3a96e6167656d656e74	\N	2513	2514	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1258	1	T1-859	\\x6e617669676174696f6e2061c3a97269656e6e65	\N	2515	2516	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1259	1	T1-1240	\\x6e61747572616c69736174696f6e	\N	2517	2518	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1260	1	T1-1140	\\x7472617661696c206465206e756974	\N	2519	2520	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1261	1	T1-898	\\x686f7261697265206465207472617661696c	\N	2521	2522	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1262	1	T1-1100	\\x656d706c6f692064657320656e66616e7473	\N	2523	2524	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1263	1	T1-1194	\\x64656d616e64657572206427656d706c6f69	\N	2525	2526	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1264	1	T1-865	\\x7265736964656e636520756e6976657273697461697265	\N	2527	2528	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1265	1	T1-867	\\x7265636f6e647569746520c3a0206c612066726f6e7469c3a87265	\N	2529	2530	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1267	1	T1-1382	\\x636f6e7365696c6c65722072c3a967696f6e616c	\N	2533	2534	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1271	1	T1-1408	\\x636f6e736372697074696f6e	\N	2541	2542	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1272	1	T1-1039	\\x656e72c3b46c656d656e74	\N	2543	2544	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1357	1	T1-1124	\\x62617074c3aa6d6520636976696c	\N	2713	2714	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1273	1	T1-875	\\x67726f7570656d656e742064652070726f6475637465757273	\N	2545	2546	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1274	1	T1-876	\\x6173736f63696174696f6e2064652070c3aa636865	\N	2547	2548	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1275	1	T1-877	\\x61747465696e746520c3a0206c61206469676e6974c3a92064657320706572736f6e6e6573	\N	2549	2550	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1238	1	T1-1351	\\x62726f63616e74657572	\N	2475	2476	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:13
1280	1	T1-880	\\x6e6f6e2d6c696575	\N	2559	2560	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1281	1	T1-1003	\\x706f696473206c6f757264	\N	2561	2562	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1282	1	T1-938	\\x6372c3a96174696f6e206427656e7472657072697365	\N	2563	2564	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1283	1	T1-890	\\x636f6e6368796c6963756c74757265	\N	2565	2566	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1284	1	T1-1153	\\x6d6f7576656d656e7420706f70756c61697265	\N	2567	2568	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1285	1	T1-895	\\x6163637565696c2070c3a9726973636f6c61697265	\N	2569	2570	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1286	1	T1-1426	\\x6d6174c3a97269656c20696e666f726d617469717565	\N	2571	2572	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1287	1	T1-904	\\x7365727669636520c3a964756361746966	\N	2573	2574	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1243	1	T1-1387	\\x74696d6272652066697363616c	\N	2485	2486	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1245	1	T1-1123	\\x636f6e747269627561626c65	\N	2489	2490	1	1	1	2011-05-25 10:03:12	2012-03-07 09:41:14
1276	1	T1-969	\\x766575766520646520677565727265	\N	2551	2552	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1277	1	T1-1424	\\x676974616e	\N	2553	2554	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1278	1	T1-1104	\\x696e76616c69646520646520677565727265	\N	2555	2556	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1279	1	T1-1436	\\x70726973652064276f74616765	\N	2557	2558	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1304	1	T1-958	\\x61707072656e74697373616765	\N	2607	2608	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1305	1	T1-1384	\\x64c3a9626974206465207461626163	\N	2609	2610	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1306	1	T1-965	\\x64726f697420636f6d6d756e61757461697265	\N	2611	2612	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1307	1	T1-970	\\x6a75737469636520736569676e65757269616c65	\N	2613	2614	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1308	1	T1-972	\\x636f6e74726163657074696f6e	\N	2615	2616	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1309	1	T1-973	\\xc3a96e6572676965206465206c61206d6572	\N	2617	2618	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1312	1	T1-979	\\x73657276696365206475207472617661696c206f626c696761746f697265	\N	2623	2624	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1313	1	T1-980	\\x72656c6967696f6e206f7274686f646f7865	\N	2625	2626	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1314	1	T1-984	\\x746162616769736d65	\N	2627	2628	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1315	1	T1-989	\\x64c3a966656e73652070617373697665	\N	2629	2630	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1316	1	T1-990	\\x6c7574746520636f6e747265206c27c3a9726f73696f6e	\N	2631	2632	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1317	1	T1-1015	\\x63726f79616e63657320706f70756c6169726573	\N	2633	2634	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1424	1	T2-10	\\x6173736965747465	\N	2847	2848	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1142	1	T1-1419	\\x7472617661696c6c6575722066726f6e74616c696572	\N	2283	2284	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1143	1	T1-1344	\\x7472617661696c20636c616e64657374696e	\N	2285	2286	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1144	1	T1-1146	\\x6361727269c3a872652070726f66657373696f6e6e656c6c65	\N	2287	2288	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1332	1	T1-1098	\\x636f72706f726174696f6e	\N	2663	2664	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1333	1	T1-1037	\\x636c617373656d656e742073616e73207375697465	\N	2665	2666	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1334	1	T1-1041	\\x656e736569676e656d656e7420746563686e69717565	\N	2667	2668	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1335	1	T1-1048	\\x61747465696e746520c3a02070617069657273207075626c696373	\N	2669	2670	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1336	1	T1-1076	\\x7369676e616c69736174696f6e20726f757469c3a87265	\N	2671	2672	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1337	1	T1-1059	\\x68c3b474656c2064652072c3a967696f6e	\N	2673	2674	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1338	1	T1-1304	\\x6a657520646520636172746573	\N	2675	2676	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1339	1	T1-1309	\\x617070617265696c206465206a6575	\N	2677	2678	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1340	1	T1-1271	\\x706f70756c6174696f6e2075726261696e65	\N	2679	2680	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1341	1	T1-1314	\\x64c3a96d6f67726170686965	\N	2681	2682	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1342	1	T1-1191	\\x72617061747269c3a9	\N	2683	2684	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1173	1	T1-741	\\x666f72c3aa742070726976c3a965	\N	2345	2346	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1174	1	T1-742	\\x6169646520736f6369616c65206cc3a967616c65	\N	2347	2348	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1175	1	T1-1075	\\x7472616e7368756d616e6365	\N	2349	2350	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1318	1	T1-992	\\x6465677261646174696f6e206465206269656e73	\N	2635	2636	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1319	1	T1-1353	\\x70737963686f70c3a96461676f676965	\N	2637	2638	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1320	1	T1-1033	\\x73617576657461676520656e206d6572	\N	2639	2640	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1321	1	T1-1430	\\x73617065757220706f6d70696572	\N	2641	2642	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1322	1	T1-1065	\\x68616269746174696f6e20c3a0206c6f796572206d6f64c3a972c3a9	\N	2643	2644	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
160	1	T1-420	\\x6d656e6469636974c3a9	\N	319	320	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1291	1	T1-1397	\\x626173652061c3a97269656e6e65	\N	2581	2582	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1293	1	T1-924	\\x736169736965	\N	2585	2586	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1294	1	T1-927	\\x6f7267616e69736d6520646520666f726d6174696f6e	\N	2587	2588	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1295	1	T1-930	\\x696d70c3b47420736f6369616c	\N	2589	2590	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:14
1296	1	T1-1024	\\x636f6e63656e74726174696f6e	\N	2591	2592	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1297	1	T1-1398	\\x656e736569676e656d656e74206dc3a96e61676572	\N	2593	2594	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1298	1	T1-1260	\\x7472617661696c206d616e75656c	\N	2595	2596	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1299	1	T1-1429	\\x72c3a9666f726d6174696f6e	\N	2597	2598	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1344	1	T1-1080	\\x6d657572747265	\N	2687	2688	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1345	1	T1-1085	\\x76616363696e6174696f6e	\N	2689	2690	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1346	1	T1-1087	\\x74c3a96cc3a97068c3a97269717565	\N	2691	2692	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1350	1	T1-1286	\\x67656e7320646520677565727265	\N	2699	2700	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1351	1	T1-1148	\\x7472617661696c2070c3a96e6974656e746961697265	\N	2701	2702	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1352	1	T1-1118	\\x696e63617061626c65206d616a657572	\N	2703	2704	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1353	1	T1-1282	\\x72656e7472c3a9652073636f6c61697265	\N	2705	2706	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1378	1	T1-1416	\\x74726920706f7374616c	\N	2755	2756	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1379	1	T1-1187	\\x74c3a96cc3a970686f6e65	\N	2757	2758	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1386	1	T1-1229	\\x64c3a96d6f637261746965	\N	2771	2772	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1387	1	T1-1236	\\x6c69676e652061c3a97269656e6e65	\N	2773	2774	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1642	1	T3-202	\\x72c3a8676c656d656e74	\N	3283	3284	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1388	1	T1-1285	\\x76656e7465206a756469636961697265	\N	2775	2776	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1389	1	T1-1255	\\x6269656e2073696e69737472c3a9	\N	2777	2778	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1390	1	T1-1265	\\x6c61626f7261746f697265206427616e616c797365	\N	2779	2780	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1391	1	T1-1303	\\x62696f6469766572736974c3a9	\N	2781	2782	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1393	1	T1-1355	\\xc3a96e657267696520c3a96f6c69656e6e65	\N	2785	2786	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1395	1	T1-1375	\\x696d70c3b47420737572206c6520726576656e75	\N	2789	2790	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1396	1	T1-1392	\\x636f75707320657420626c65737375726573	\N	2791	2792	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1397	1	T1-1400	\\x6361726e6176616c	\N	2793	2794	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1398	1	T1-1421	\\x61766f7274656d656e74	\N	2795	2796	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1399	1	T2-38	\\x656e7472657469656e	\N	2797	2798	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1400	1	T2-17	\\x636f6d6d697373696f6e6e656d656e74	\N	2799	2800	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1401	1	T2-42	\\x6578c3a9637574696f6e2062756467c3a97461697265	\N	2801	2802	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1402	1	T2-40	\\xc3a9766163756174696f6e	\N	2803	2804	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1403	1	T2-62	\\x726563656e73656d656e74	\N	2805	2806	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1404	1	T2-20	\\x636f6e63696c696174696f6e	\N	2807	2808	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1405	1	T2-53	\\x6c6f636174696f6e	\N	2809	2810	2	1	1	2011-05-25 10:06:25	2012-03-07 09:41:16
1407	1	T2-51	\\x696e666f726d617469736174696f6e	\N	2813	2814	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1408	1	T2-31	\\x64c3a96cc3a9676174696f6e	\N	2815	2816	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1409	1	T2-59	\\x70726f6772616d6d6174696f6e	\N	2817	2818	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1410	1	T2-28	\\x636f6e7472c3b46c652073616e697461697265	\N	2819	2820	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1411	1	T2-43	\\x6578706c6f69746174696f6e20636f6d6d65726369616c65	\N	2821	2822	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1412	1	T2-68	\\x726573746175726174696f6e	\N	2823	2824	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1414	1	T2-16	\\x636c617373656d656e74	\N	2827	2828	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1415	1	T2-39	\\xc3a971756970656d656e74206d6174c3a97269656c	\N	2829	2830	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1420	1	T2-6	\\x616772c3a96d656e74	\N	2839	2840	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1421	1	T2-19	\\x636f6e63657373696f6e	\N	2841	2842	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1422	1	T2-4	\\x616468c3a973696f6e	\N	2843	2844	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1423	1	T2-67	\\x72c3a9717569736974696f6e	\N	2845	2846	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1425	1	T2-63	\\x7265636f757273206869c3a9726172636869717565	\N	2849	2850	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1426	1	T2-26	\\x636f6e7472c3b46c652064652073c3a96375726974c3a9	\N	2851	2852	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1427	1	T2-33	\\x64c3a97369676e6174696f6e	\N	2853	2854	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1430	1	T2-48	\\x67657374696f6e20647520706572736f6e6e656c	\N	2859	2860	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1431	1	T2-2	\\x61627374656e74696f6e	\N	2861	2862	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1432	1	T2-64	\\x7265636f757672656d656e74	\N	2863	2864	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1433	1	T2-9	\\x617270656e74616765	\N	2865	2866	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1434	1	T2-69	\\x746172696669636174696f6e	\N	2867	2868	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1435	1	T2-27	\\x636f6e7472c3b46c652066697363616c	\N	2869	2870	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1436	1	T2-35	\\x646973736f6c7574696f6e	\N	2871	2872	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1437	1	T2-24	\\x636f6e7472c3b46c652064652067657374696f6e	\N	2873	2874	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1419	1	T2-45	\\x666f6e6374696f6e6e656d656e74	\N	2837	2838	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1446	1	T2-56	\\x706c6163656d656e74	\N	2891	2892	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1447	1	T2-32	\\x64c3a96e6f6d696e6174696f6e	\N	2893	2894	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1448	1	T2-66	\\x72c3a9676c656d656e746174696f6e	\N	2895	2896	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1452	1	T2-22	\\x636f6e7472c3b46c65	\N	2903	2904	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1453	1	T2-11	\\x61737369676e6174696f6e	\N	2905	2906	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1454	1	T2-60	\\x70726f6d6f74696f6e	\N	2907	2908	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1455	1	T2-7	\\x616dc3a96e6167656d656e74	\N	2909	2910	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1456	1	T2-37	\\x656e7175c3aa7465207075626c69717565	\N	2911	2912	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1458	1	T2-58	\\x7072c3a976656e74696f6e	\N	2915	2916	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1460	1	T2-14	\\x63617574696f6e	\N	2919	2920	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1461	1	T2-41	\\xc3a976616c756174696f6e	\N	2921	2922	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1462	1	T2-52	\\x696e7370656374696f6e	\N	2923	2924	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1463	1	T2-70	\\x747574656c6c652061646d696e697374726174697665	\N	2925	2926	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1464	1	T2-30	\\x636f6f7264696e6174696f6e	\N	2927	2928	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1465	1	T2-13	\\x6175746f7269736174696f6e	\N	2929	2930	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1467	1	T2-8	\\x617070656c2064276f6666726573	\N	2933	2934	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1468	1	T2-25	\\x636f6e7472c3b46c65206465206cc3a967616c6974c3a9	\N	2935	2936	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1469	1	T2-36	\\x656e7175c3aa7465	\N	2937	2938	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1471	1	T4-8	\\xc3a9706f71756520636f6e74656d706f7261696e65	\N	2941	2942	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:17
1472	1	T4-3	\\x4d6f79656e20c3826765	\N	2943	2944	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:17
1473	1	T4-2	\\x416e746971756974c3a9	\N	2945	2946	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:17
1474	1	T4-5	\\x52c3a9766f6c7574696f6e2064652031373839	\N	2947	2948	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1475	1	T4-7	\\x5072656d69657220456d706972652028313830342d3138313529	\N	2949	2950	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1476	1	T4-4	\\xc3a9706f717565206d6f6465726e65	\N	2951	2952	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1477	1	T4-6	\\x436f6e73756c61742028313739392d3138303429	\N	2953	2954	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1478	1	T4-26	\\x6775657272652064652043656e7420416e732028313333372d3134353329	\N	2955	2956	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1479	1	T4-25	\\x657870616e73696f6e206465206c2769736c616d20283632322d3134393229	\N	2957	2958	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1480	1	T4-58	\\x46726f6e64652028313634382d3136353229	\N	2959	2960	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1481	1	T4-29	\\x313765207369c3a8636c65	\N	2961	2962	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1482	1	T4-80	\\x526573746175726174696f6e2028313831352d3138333029	\N	2963	2964	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1483	1	T4-44	\\x313965207369c3a8636c65	\N	2965	2966	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1484	1	T4-36	\\x436f6e76656e74696f6e206e6174696f6e616c652028313739322d3137393529	\N	2967	2968	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1485	1	T4-47	\\x43696e717569c3a86d652052c3a97075626c697175652028313935382d2029	\N	2969	2970	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1486	1	T4-72	\\x61626f6c6974696f6e206465206c276573636c617661676520283138343829	\N	2971	2972	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1487	1	T4-84	\\x5365636f6e6420456d706972652028313835322d3138373029	\N	2973	2974	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1661	1	T3-206	\\x72c3a9706572746f69726520636976696c	\N	3321	3322	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1440	1	T2-12	\\x6173737572616e6365	\N	2879	2880	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1586	1	T4-14	\\x3665207369c3a8636c65	\N	3171	3172	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1490	1	T4-45	\\x323065207369c3a8636c65	\N	2979	2980	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1382	1	T1-1305	\\x6c69767265207361696e74	\N	2763	2764	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1383	1	T1-1211	\\x666c6f72652073617576616765	\N	2765	2766	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1384	1	T1-1224	\\x6162757320646520666f6e6374696f6e	\N	2767	2768	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1439	1	T2-5	\\x61646a756469636174696f6e	\N	2877	2878	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1441	1	T2-46	\\x676172616e746965206427656d7072756e74	\N	2881	2882	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1442	1	T2-49	\\x696d6d6174726963756c6174696f6e	\N	2883	2884	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1443	1	T2-34	\\x6465737472756374696f6e	\N	2885	2886	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1444	1	T2-54	\\x6e756dc3a97269736174696f6e	\N	2887	2888	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1445	1	T2-61	\\x70726f74656374696f6e	\N	2889	2890	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1491	1	T4-24	\\x63726f6973616465732028313039352d3132393129	\N	2981	2982	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1492	1	T4-35	\\x63616d7061676e652064274974616c69652028313739362d3137393729	\N	2983	2984	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1493	1	T4-57	\\x52656e61697373616e6365	\N	2985	2986	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1494	1	T4-28	\\x313665207369c3a8636c65	\N	2987	2988	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1495	1	T4-27	\\x696e766173696f6e73207363616e64696e6176657320283739302d3130333029	\N	2989	2990	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1496	1	T4-94	\\x636f6e66c3a972656e6365206465204d756e69636820283139333829	\N	2991	2992	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1497	1	T4-76	\\x677565727265206465204372696dc3a9652028313835332d3138353629	\N	2993	2994	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1498	1	T4-74	\\x636f7570206427c38974617420647520322064c3a963656d6272652031383531	\N	2995	2996	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1499	1	T4-73	\\x436f6d6d756e6520646520506172697320283138373129	\N	2997	2998	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1500	1	T4-82	\\x52c3a9766f6c7574696f6e2064652031383438	\N	2999	3000	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1501	1	T4-81	\\x52c3a9766f6c7574696f6e2064652031383330	\N	3001	3002	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1502	1	T4-85	\\x756e6974c3a920616c6c656d616e646520283138373129	\N	3003	3004	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1503	1	T4-86	\\x756e6974c3a9206974616c69656e6e652028313965207369c3a8636c6529	\N	3005	3006	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1504	1	T4-77	\\x6775657272652064652053c3a963657373696f6e2028313836312d3138363529	\N	3007	3008	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1505	1	T4-78	\\x6775657272652064652031383730	\N	3009	3010	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1821	1	T3-189	\\x726170706f7274206465207374616765	\N	3641	3642	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1518	1	T4-114	\\x67756572726520313933392d31393435	\N	3035	3036	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1519	1	T4-125	\\x52c3a9766f6c7574696f6e20727573736520283139313729	\N	3037	3038	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1520	1	T4-99	\\xc3a976c3a96e656d656e7473206465206d61692031393638	\N	3039	3040	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1521	1	T4-105	\\x677565727265206427496e646f6368696e652028313934362d3139353429	\N	3041	3042	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1699	1	T3-58	\\x6368726f6e69717565	\N	3397	3398	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1522	1	T4-127	\\x7472616974c3a9206465206c2741746c616e7469717565204e6f726420283139343929	\N	3043	3044	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1523	1	T4-110	\\x6775657272652066726f6964652028313934352d3139383929	\N	3045	3046	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1524	1	T4-88	\\x61666661697265206465205375657a20283139353629	\N	3047	3048	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1525	1	T4-121	\\x7072696e74656d70732064652050726167756520283139363829	\N	3049	3050	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1526	1	T4-116	\\x4c6962c3a9726174696f6e2028313934342d3139343529	\N	3051	3052	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1528	1	T4-107	\\x67756572726520646573204d616c6f75696e657320283139383229	\N	3055	3056	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1622	1	T3-74	\\x636f757075726520646520707265737365	\N	3243	3244	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1532	1	T4-109	\\x67756572726520647520566965746e616d2028313936322d3139373329	\N	3063	3064	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1533	1	T4-92	\\x636f6e63696c65205661746963616e20322028313936322d3139363529	\N	3065	3066	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1534	1	T4-108	\\x67756572726520647520476f6c66652028313939302d3139393129	\N	3067	3068	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1535	1	T4-119	\\x7061637465206765726d616e6f2d736f7669c3a9746971756520283139333929	\N	3069	3070	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1536	1	T4-122	\\x517561747269c3a86d652052c3a97075626c697175652028313934362d3139353829	\N	3071	3072	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1537	1	T4-101	\\x676f757665726e656d656e742064652056696368792028313934302d3139343429	\N	3073	3074	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1538	1	T4-112	\\x67756572726520727573736f2d6a61706f6e616973652028313930342d3139303529	\N	3075	3076	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1380	1	T1-1292	\\x737461676961697265	\N	2759	2760	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1381	1	T1-1197	\\x7468c3a96f6c6f676965	\N	2761	2762	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1542	1	T4-120	\\x706c616e204d61727368616c6c20283139343729	\N	3083	3084	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1543	1	T4-102	\\x476f757665726e656d656e742070726f7669736f6972652028313934342d3139343629	\N	3085	3086	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1728	1	T3-141	\\x6d6f74696f6e	\N	3455	3456	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1544	1	T4-123	\\x52c3a97075626c69717565206465205765696d61722028313931392d3139333329	\N	3087	3088	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1545	1	T4-113	\\x67756572726520313931342d31393138	\N	3089	3090	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1546	1	T4-104	\\x67756572726520642745737061676e652028313933362d3139333929	\N	3091	3092	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1719	1	T3-41	\\x6361726e65742064652073616e74c3a9	\N	3437	3438	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1547	1	T4-115	\\x696e73757272656374696f6e20686f6e67726f69736520283139353629	\N	3093	3094	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1632	1	T3-125	\\x6d61696e20636f7572616e7465	\N	3263	3264	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1548	1	T4-126	\\x73c3a97061726174696f6e2064657320c389676c69736573206574206465206c27c38974617420283139303529	\N	3095	3096	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1549	1	T4-124	\\x72c3a9756e696669636174696f6e20616c6c656d616e64652028313938392d3139393029	\N	3097	3098	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1551	1	T4-38	\\x67756572726573206465206c612052c3a9766f6c7574696f6e2028313739322d3137393929	\N	3101	3102	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1552	1	T4-39	\\x677565727265732064652056656e64c3a9652028313739332d3137393929	\N	3103	3104	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1553	1	T4-34	\\x63616d7061676e65206427c38967797074652028313739382d3138303129	\N	3105	3106	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1737	1	T3-124	\\x6c6976726574206f757672696572	\N	3473	3474	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1555	1	T4-32	\\x417373656d626cc3a96520636f6e7374697475616e74652028313738392d3137393129	\N	3109	3110	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1556	1	T4-40	\\x546572726575722028313739332d3137393429	\N	3111	3112	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1557	1	T4-49	\\x54726f697369c3a86d652052c3a97075626c697175652028313837302d3139343029	\N	3113	3114	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1558	1	T4-11	\\x4261732d456d70697265	\N	3115	3116	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1559	1	T4-48	\\x636f6e737472756374696f6e206575726f70c3a9656e6e65	\N	3117	3118	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1800	1	T3-11	\\x616e6e7561697265	\N	3599	3600	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1506	1	T4-75	\\x4465757869c3a86d652052c3a97075626c697175652028313834382d3138353229	\N	3011	3012	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1507	1	T4-79	\\x6d6f6e617263686965206465204a75696c6c65742028313833302d3138343829	\N	3013	3014	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1510	1	T4-59	\\x6775657272652064652044c3a9766f6c7574696f6e2028313636372d3136363829	\N	3019	3020	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1512	1	T4-91	\\x436f6c6c61626f726174696f6e2028313934302d3139343429	\N	3023	3024	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1513	1	T4-98	\\x656e7472652d646575782d67756572726573	\N	3025	3026	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1514	1	T4-90	\\x626c6f637573206465204265726c696e2028313934382d3139343929	\N	3027	3028	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1515	1	T4-103	\\x677565727265206427416c67c3a97269652028313935342d3139363229	\N	3029	3030	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1516	1	T4-117	\\x6d7572206465204265726c696e2028313936312d3139383929	\N	3031	3032	3	1	1	2011-05-25 10:09:29	2012-03-07 09:41:18
1517	1	T4-106	\\x67756572726520646520436f72c3a9652028313935302d3139353329	\N	3033	3034	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:18
1673	1	T3-148	\\x6f72647265206465206d697373696f6e	\N	3345	3346	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1576	1	T4-42	\\x636f6e6772c3a873206465205669656e6e6520283138313529	\N	3151	3152	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1578	1	T4-54	\\x677565727265732064274974616c69652028313439342d3135353929	\N	3155	3156	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1579	1	T4-55	\\x677565727265732064652072656c6967696f6e2028313536322d3135393829	\N	3157	3158	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1583	1	T4-51	\\x436f6e7472652d52c3a9666f726d65	\N	3165	3166	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1584	1	T4-13	\\x3565207369c3a8636c65	\N	3167	3168	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1585	1	T4-19	\\x313165207369c3a8636c65	\N	3169	3170	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1587	1	T4-9	\\x416e746971756974c3a92067616c6c6f2d726f6d61696e65	\N	3173	3174	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1705	1	T3-30	\\x62726f6368757265	\N	3409	3410	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1571	1	T4-65	\\x677565727265206465205365707420416e732028313735362d3137363329	\N	3141	3142	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1572	1	T4-62	\\x6775657272652064652053756363657373696f6e20642745737061676e652028313730312d3137313429	\N	3143	3144	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1573	1	T4-69	\\x52c3a967656e63652028313731352d3137323329	\N	3145	3146	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1574	1	T4-41	\\x43656e74206a6f75727320283138313529	\N	3147	3148	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1575	1	T4-43	\\x67756572726573206e61706f6cc3a96f6e69656e6e65732028313830302d3138313529	\N	3149	3150	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1596	1	T4-21	\\x313365207369c3a8636c65	\N	3191	3192	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1597	1	T4-16	\\x3865207369c3a8636c65	\N	3193	3194	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1598	1	T4-23	\\x313565207369c3a8636c65	\N	3195	3196	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1599	1	T4-50	\\x63726f697361646520636f6e747265206c657320416c626967656f69732028313230382d3132323929	\N	3197	3198	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1623	1	T3-26	\\x62696c616e20636f6d707461626c65	\N	3245	3246	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1624	1	T3-96	\\x646f73736965722064652070656e73696f6e	\N	3247	3248	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1625	1	T3-197	\\x726567697374726520647520636f6d6d657263652065742064657320736f6369c3a974c3a973	\N	3249	3250	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1626	1	T3-94	\\x646f7373696572206465206361727269c3a87265	\N	3251	3252	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1627	1	T3-168	\\x706f727472616974	\N	3253	3254	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1628	1	T3-180	\\x70726f74c3aa74	\N	3255	3256	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1629	1	T3-146	\\x6e6f7469636520696e646976696475656c6c65	\N	3257	3258	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1631	1	T3-13	\\x617272c3aa74c3a9206427616c69676e656d656e74	\N	3261	3262	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1633	1	T3-158	\\x70c3a9746974696f6e	\N	3265	3266	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1634	1	T3-112	\\x696e76656e7461697265	\N	3267	3268	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1540	1	T4-100	\\x46726f6e7420706f70756c616972652028313933362d3139333829	\N	3079	3080	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1541	1	T4-93	\\x636f6e66c3a972656e63652064652042616e64756e6720283139353529	\N	3081	3082	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1610	1	T3-199	\\x7265676973747265206d6174726963756c65	\N	3219	3220	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1611	1	T3-196	\\x72656769737472652064657320656e7472c3a965732065742064657320736f7274696573	\N	3221	3222	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1612	1	T3-76	\\x64c3a9636973696f6e2062756467c3a97461697265206d6f64696669636174697665	\N	3223	3224	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1710	1	T3-128	\\x6d61717565747465	\N	3419	3420	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1712	1	T3-142	\\x6e6f6d656e636c6174757265	\N	3423	3424	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1614	1	T3-217	\\x736368c3a96d6120646972656374657572206427616dc3a96e6167656d656e74206574206427757262616e69736d65	\N	3227	3228	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1711	1	T3-191	\\x7265676973747265	\N	3421	3422	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1615	1	T3-81	\\x64c3a9636c61726174696f6e2064652073756363657373696f6e	\N	3229	3230	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1617	1	T3-77	\\x64c3a9636973696f6e20646520706169656d656e74	\N	3233	3234	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1302	1	T1-953	\\x636f6d6dc3a96d6f726174696f6e	\N	2603	2604	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1303	1	T1-957	\\x72656c6174696f6e73206575726f70c3a9656e6e6573	\N	2605	2606	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1560	1	T4-130	\\x7472616974c3a920737572206c27756e696f6e206575726f70c3a9656e6e6520283139393229	\N	3119	3120	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1561	1	T4-129	\\x7472616974c3a920646520526f6d6520283139353729	\N	3121	3122	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1562	1	T4-31	\\x6d6f6e617263686965206427416e6369656e2052c3a967696d65	\N	3123	3124	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1564	1	T4-12	\\x67756572726520646573204761756c6573	\N	3127	3128	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1565	1	T4-64	\\x72c3a9766f636174696f6e206465206c27c3a9646974206465204e616e74657320283136383529	\N	3129	3130	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1618	1	T3-64	\\x636f6d7074652064276578706c6f69746174696f6e	\N	3235	3236	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1619	1	T3-99	\\x646f737369657220646573206f75767261676573206578c3a9637574c3a973	\N	3237	3238	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1620	1	T3-4	\\x6163746520646520636f6e6772c3a873	\N	3239	3240	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1621	1	T3-116	\\x6c6973746520c3a96c6563746f72616c65	\N	3241	3242	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1649	1	T3-19	\\x61746c61732074657272696572	\N	3297	3298	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1650	1	T3-111	\\x696e736372697074696f6e206879706f7468c3a96361697265	\N	3299	3300	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1651	1	T3-25	\\x6269626c696f67726170686965	\N	3301	3302	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1652	1	T3-122	\\x6c69767265206c697475726769717565	\N	3303	3304	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1653	1	T3-127	\\x6d616e696665737465206465206e6176697265	\N	3305	3306	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1663	1	T3-9	\\x616c62756d	\N	3325	3326	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1636	1	T3-151	\\x70616e6e6561752064276578706f736974696f6e	\N	3271	3272	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1637	1	T3-183	\\x7175657374696f6e6e61697265206427656e7175c3aa7465	\N	3273	3274	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1638	1	T3-159	\\x70686f746f67726170686965	\N	3275	3276	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1639	1	T3-60	\\x63697263756c61697265	\N	3277	3278	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1640	1	T3-33	\\x62756467657420737570706cc3a96d656e7461697265	\N	3279	3280	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1641	1	T3-115	\\x6c69737465206427c3a96d617267656d656e74	\N	3281	3282	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1643	1	T3-204	\\x72c3a8676c656d656e7420696e74657269657572	\N	3285	3286	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1644	1	T3-57	\\x636861727465	\N	3287	3288	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1600	1	T3-34	\\x62756c6c6520706f6e7469666963616c65	\N	3199	3200	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1602	1	T3-198	\\x726567697374726520647520636f757272696572	\N	3203	3204	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1603	1	T3-83	\\x64c3a9636c61726174696f6e206427696e74656e74696f6e206427616c69c3a96e6572	\N	3205	3206	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1604	1	T3-157	\\x7065726d697320646520726563686572636865	\N	3207	3208	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1606	1	T3-118	\\x6c6976726520636f6d707461626c65	\N	3211	3212	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1607	1	T3-185	\\x726170706f727420642761637469766974c3a9	\N	3213	3214	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1609	1	T3-32	\\x627564676574207072696d69746966	\N	3217	3218	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:20
1684	1	T3-145	\\x6e6f74652064652073657276696365	\N	3367	3368	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1685	1	T3-22	\\x61766973	\N	3369	3370	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1724	1	T3-161	\\x706c616e	\N	3447	3448	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1595	1	T4-20	\\x313265207369c3a8636c65	\N	3189	3190	3	1	1	2011-05-25 10:09:31	2012-03-07 09:41:20
1570	1	T4-67	\\x696e64c3a970656e64616e63652064657320c389746174732d556e69732028313737352d3137383229	\N	3139	3140	3	1	1	2011-05-25 10:09:30	2012-03-07 09:41:19
1675	1	T3-140	\\x6d6f64c3a86c65206465206661627269717565	\N	3349	3350	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1677	1	T3-61	\\x636f6d6d756e697175c3a920646520707265737365	\N	3353	3354	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1680	1	T3-1	\\x6163636f7264206465207472617661696c	\N	3359	3360	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1670	1	T3-17	\\x617272c3aa74c3a9207072c3a9666563746f72616c	\N	3339	3340	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1703	1	T3-164	\\x706c616e206465207365636f757273	\N	3405	3406	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1704	1	T3-153	\\x7061737365706f7274	\N	3407	3408	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1706	1	T3-232	\\x7472616e736372697074696f6e206879706f7468c3a96361697265	\N	3411	3412	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1707	1	T3-205	\\x72c3a9706572746f697265	\N	3413	3414	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1708	1	T3-233	\\x77617272616e74	\N	3415	3416	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1709	1	T3-5	\\x6163746520646520736f6369c3a974c3a9	\N	3417	3418	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1713	1	T3-174	\\x70726f63c3a8732d76657262616c2064652072c3a9756e696f6e	\N	3425	3426	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1714	1	T3-38	\\x636168696572206465732063686172676573	\N	3427	3428	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1715	1	T3-139	\\x6d696e757465206e6f74617269616c65	\N	3429	3430	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1716	1	T3-222	\\x73756a65742064276578616d656e	\N	3431	3432	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1717	1	T3-20	\\x6175746f7269736174696f6e206427757262616e69736d65	\N	3433	3434	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1718	1	T3-113	\\x6c61697373657a2d706173736572	\N	3435	3436	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1720	1	T3-227	\\x74696d627265	\N	3439	3440	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1721	1	T3-39	\\x63616c656e6472696572	\N	3441	3442	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1722	1	T3-28	\\x62696c616e20736f6369616c	\N	3443	3444	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1723	1	T3-182	\\x7075626c69636174696f6e2070c3a972696f6469717565	\N	3445	3446	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1725	1	T3-62	\\x636f6d7074652061646d696e6973747261746966	\N	3449	3450	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1726	1	T3-186	\\x726170706f72742064652067656e6461726d65726965	\N	3451	3452	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1727	1	T3-63	\\x636f6d7074652064652067657374696f6e	\N	3453	3454	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1729	1	T3-143	\\x6e6f726d65	\N	3457	3458	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1730	1	T3-171	\\x70726f63c3a8732d76657262616c206427616dc3a96e6167656d656e7420666f72657374696572	\N	3459	3460	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1731	1	T3-225	\\x74c3a96cc3a96772616d6d65	\N	3461	3462	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1732	1	T3-69	\\x636f6e76656e74696f6e	\N	3463	3464	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1733	1	T3-121	\\x6c6976726520666f6e63696572	\N	3465	3466	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1734	1	T3-48	\\x63617274652064276964656e746974c3a9	\N	3467	3468	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1735	1	T3-165	\\x706c616e2064276f636375706174696f6e2064657320736f6c73	\N	3469	3470	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1736	1	T3-85	\\x64c3a9636c61726174696f6e2066697363616c65	\N	3471	3472	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1738	1	T3-72	\\x636f7069652064276578616d656e	\N	3475	3476	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1743	1	T3-169	\\x70726573736520696e737469747574696f6e6e656c6c65	\N	3485	3486	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1744	1	T3-230	\\x74697472652064652073c3a96a6f7572	\N	3487	3488	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:22
1747	1	T3-226	\\x7465787465206f6666696369656c	\N	3493	3494	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1748	1	T3-35	\\x62756c6c6574696e2064652073616c61697265	\N	3495	3496	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1749	1	T3-114	\\x6c69737465	\N	3497	3498	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1750	1	T3-73	\\x636f72726573706f6e64616e6365	\N	3499	3500	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1751	1	T3-47	\\x636172746520646520766f6575	\N	3501	3502	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1753	1	T3-166	\\x706c616e206427757262616e69736d65	\N	3505	3506	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1754	1	T3-10	\\x616e6e6f6e6365206f6666696369656c6c65206574206cc3a967616c65	\N	3507	3508	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1755	1	T3-218	\\x736f6d6d69657220646573206d61726368616e6469736573	\N	3509	3510	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1646	1	T3-84	\\x64c3a9636c61726174696f6e2064277574696c6974c3a9207075626c69717565	\N	3291	3292	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1647	1	T3-3	\\x6163746520636976696c207075626c6963	\N	3293	3294	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1648	1	T3-89	\\x646f63756d656e7420617564696f76697375656c	\N	3295	3296	4	1	1	2011-05-25 10:13:14	2012-03-07 09:41:21
1671	1	T3-162	\\x706c616e2063616461737472616c	\N	3341	3342	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1674	1	T3-126	\\x6d616e69666573746520642761c3a9726f6e6566	\N	3347	3348	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1681	1	T3-103	\\xc3a9746174206475206d6f6e74616e742064657320726f6c6573	\N	3361	3362	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1682	1	T3-131	\\x6d6174726963652063616461737472616c65	\N	3363	3364	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1683	1	T3-45	\\x6361727465206465206d656d627265	\N	3365	3366	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1686	1	T3-136	\\x6d657263757269616c65	\N	3371	3372	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1687	1	T3-29	\\x627265766574206427696e76656e74696f6e	\N	3373	3374	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1776	1	T3-31	\\x627564676574	\N	3551	3552	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1688	1	T3-55	\\x63657274696669636174206427757262616e69736d65	\N	3375	3376	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1689	1	T3-2	\\x616374652061757468656e7469717565	\N	3377	3378	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:21
1690	1	T3-137	\\x6d6963726f666f726d65	\N	3379	3380	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1693	1	T3-167	\\x706fc3a86d65	\N	3385	3386	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1694	1	T3-14	\\x617272c3aa74c3a9206475206d61697265	\N	3387	3388	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1696	1	T3-207	\\x72c3a9706572746f69726520646573206dc3a97469657273	\N	3391	3392	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1697	1	T3-71	\\x636f6e76656e74696f6e20646520666f726d6174696f6e	\N	3393	3394	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1700	1	T3-194	\\x726567697374726520646520636174686f6c69636974c3a9	\N	3399	3400	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1701	1	T3-130	\\x6d6172717565206465206661627269717565	\N	3401	3402	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1702	1	T3-43	\\x636172746520c3a0206a6f756572	\N	3403	3404	4	1	1	2011-05-25 10:13:15	2012-03-07 09:41:22
1756	1	T3-54	\\x63657274696669636174	\N	3511	3512	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1757	1	T3-88	\\x646973636f757273	\N	3513	3514	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1758	1	T3-210	\\x72c3a9706572746f6972652067c3a96ec3a972616c2064657320616666616972657320636976696c6573	\N	3515	3516	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1759	1	T3-97	\\x646f737369657220646520707265737365	\N	3517	3518	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1760	1	T3-15	\\x617272c3aa74c3a9206475207072c3a9736964656e7420647520636f6e7365696c2067c3a96ec3a972616c	\N	3519	3520	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1761	1	T3-149	\\x6f7267616e696772616d6d65	\N	3521	3522	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1762	1	T3-181	\\x7075626c69636174696f6e206f6666696369656c6c65	\N	3523	3524	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1763	1	T3-229	\\x74697472652064652070726f707269c3a974c3a9	\N	3525	3526	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1764	1	T3-80	\\x64c3a9636c61726174696f6e20646520726576656e7573	\N	3527	3528	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1765	1	T3-91	\\x646f63756d656e74206669677572c3a9	\N	3529	3530	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1766	1	T3-120	\\x6c697672652064276f72	\N	3531	3532	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1767	1	T3-184	\\x726170706f7274	\N	3533	3534	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1768	1	T3-107	\\x66616972652d70617274	\N	3535	3536	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1769	1	T3-119	\\x6c6976726520646520726169736f6e	\N	3537	3538	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1770	1	T3-223	\\x7461626c65	\N	3539	3540	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1771	1	T3-100	\\x646f737369657220696e646976696475656c	\N	3541	3542	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1772	1	T3-6	\\x6163746520736f7573207365696e672070726976c3a9	\N	3543	3544	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1773	1	T3-104	\\xc3a97461742070617263656c6c61697265	\N	3545	3546	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1774	1	T3-219	\\x7374617469737469717565	\N	3547	3548	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1778	1	T3-172	\\x70726f63c3a8732d76657262616c206427c3a96c656374696f6e	\N	3555	3556	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1779	1	T3-187	\\x726170706f7274206465206d6572	\N	3557	3558	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1780	1	T3-98	\\x646f73736965722064652070726f63c3a964757265	\N	3559	3560	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1781	1	T3-79	\\x64c3a9636c61726174696f6e20646520646f75616e65	\N	3561	3562	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1782	1	T3-82	\\x64c3a9636c61726174696f6e2064652074726176617578	\N	3563	3564	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1783	1	T3-193	\\x726567697374726520642761756469656e6365	\N	3565	3566	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1785	1	T3-109	\\x6669636869657220696d6d6f62696c696572	\N	3569	3570	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1786	1	T3-70	\\x636f6e76656e74696f6e20636f6c6c656374697665	\N	3571	3572	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1787	1	T3-160	\\x7069c3a8636520636f6d707461626c65	\N	3573	3574	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1790	1	T3-200	\\x7265676973747265207061726f69737369616c	\N	3579	3580	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1178	1	T1-956	\\x64c3a973696e66656374696f6e	\N	2355	2356	1	1	1	2011-05-25 10:03:11	2012-03-07 09:41:12
1791	1	T3-163	\\x706c616e206427616c69676e656d656e74	\N	3581	3582	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1792	1	T3-86	\\x64c3a96c6962c3a9726174696f6e	\N	3583	3584	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1793	1	T3-190	\\x726170706f7274206427657870657274697365	\N	3585	3586	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1795	1	T3-65	\\x636f6d7074652066696e616e63696572	\N	3589	3590	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1796	1	T3-175	\\x70726f63c3a8732d76657262616c206465207363656c6cc3a973	\N	3591	3592	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1797	1	T3-201	\\x72656769737472652070726f74657374616e74	\N	3593	3594	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1798	1	T3-188	\\x726170706f727420646520706f6c696365	\N	3595	3596	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1802	1	T3-24	\\x6261736520646520646f6e6ec3a96573	\N	3603	3604	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1830	1	T3-106	\\x6578706c6f69742064276875697373696572	\N	3659	3660	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1831	1	T3-117	\\x6c69737465206e6f6d696e6174697665	\N	3661	3662	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1	1	T1-503	\\x636f6d6d756e69636174696f6e73	\N	1	2	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
60	1	T1-495	\\x6269656e732064c3a970617274656d656e74617578	\N	119	120	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:53
81	1	T1-891	\\x72c3a9756e696f6e207075626c69717565	\N	161	162	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:54
92	1	T1-1310	\\x707269c3a87265207075626c69717565	\N	183	184	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
114	1	T1-1300	\\x7265636865726368652064616e73206c27696e74c3a972c3aa74206465732066616d696c6c6573	\N	227	228	1	1	1	2011-05-25 10:02:56	2012-03-07 09:40:54
148	1	T1-41	\\x61696465207075626c69717565206175206c6f67656d656e74	\N	295	296	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
1803	1	T3-123	\\x6c6976726574206d696c697461697265	\N	3605	3606	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1804	1	T3-173	\\x70726f63c3a8732d76657262616c206427656e7175c3aa7465	\N	3607	3608	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1805	1	T3-78	\\x64c3a9636c61726174696f6e	\N	3609	3610	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
1806	1	T3-95	\\x646f737369657220646520636f6e73756c746174696f6e2064657320656e747265707269736573	\N	3611	3612	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:23
1807	1	T3-195	\\x7265676973747265206427c3a963726f75	\N	3613	3614	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1808	1	T3-87	\\x6469706cc3b46d65	\N	3615	3616	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1809	1	T3-46	\\x6361727465206465207472617661696c	\N	3617	3618	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1810	1	T3-215	\\x7363656175	\N	3619	3620	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1811	1	T3-213	\\x72c3b46c65206427c3a971756970616765	\N	3621	3622	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
703	1	T1-1256	\\x626174656175206465206e617669676174696f6e20696e74c3a9726965757265	\N	1405	1406	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:04
724	1	T1-577	\\x616e6e6578696f6e206465207465727269746f697265	\N	1447	1448	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
748	1	T1-355	\\xc3a97461626c697373656d656e74206dc3a96469636f20736f6369616c	\N	1495	1496	1	1	1	2011-05-25 10:03:05	2012-03-07 09:41:05
781	1	T1-1138	\\x66696e616e6365732064c3a970617274656d656e74616c6573	\N	1561	1562	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
793	1	T1-1371	\\x6173736f63696174696f6e20646520636861737365	\N	1585	1586	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
404	1	T1-138	\\x6173736f63696174696f6e20666f6e6369c3a872652075726261696e65	\N	807	808	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
435	1	T1-1319	\\x616374696f6e2073616e697461697265	\N	869	870	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
458	1	T1-457	\\x70726f64756374696f6e20c3a96c6563747269717565	\N	915	916	1	1	1	2011-05-25 10:03:01	2012-03-07 09:41:00
803	1	T1-914	\\x70617274696369706174696f6e2064657320656d706c6f7965757273	\N	1605	1606	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:06
833	1	T1-707	\\x61747465696e746520c3a0206c276f72647265207075626c6963	\N	1665	1666	1	1	1	2011-05-25 10:03:06	2012-03-07 09:41:07
859	1	T1-1435	\\x61646d696e697374726174696f6e20636f6d6d756e616c65	\N	1717	1718	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
871	1	T1-911	\\x64726f697473206369766971756573	\N	1741	1742	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
872	1	T1-675	\\x636f6c6cc3a8676520c3a96c6563746f72616c	\N	1743	1744	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:07
895	1	T1-1226	\\x6f6666696369657220646520706f6c696365206a756469636961697265	\N	1789	1790	1	1	1	2011-05-25 10:03:07	2012-03-07 09:41:08
923	1	T1-851	\\x766f7961676575722072657072c3a973656e74616e7420706c6163696572	\N	1845	1846	1	1	1	2011-05-25 10:03:08	2012-03-07 09:41:08
1324	1	T1-1007	\\x6f7267616e69736d652067c3a96ec3a974697175656d656e74206d6f64696669c3a9	\N	2647	2648	1	1	1	2011-05-25 10:03:13	2012-03-07 09:41:15
1347	1	T1-1332	\\x61726d656d656e74206d61726974696d65	\N	2693	2694	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1349	1	T1-1261	\\x736f6369c3a974c3a9206427696e74c3a972c3aa7420636f6c6c65637469662061677269636f6c65	\N	2697	2698	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:15
1385	1	T1-1289	\\x68c3b474656c206465206c61207072c3a966656374757265	\N	2769	2770	1	1	1	2011-05-25 10:03:14	2012-03-07 09:41:16
1413	1	T2-23	\\x636f6e7472c3b46c652062756467c3a97461697265	\N	2825	2826	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:16
1428	1	T2-71	\\x747574656c6c652066696e616e6369c3a87265	\N	2855	2856	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1438	1	T2-57	\\x7072c3a97061726174696f6e2062756467c3a97461697265	\N	2875	2876	2	1	1	2011-05-25 10:06:26	2012-03-07 09:41:17
1801	1	T3-68	\\x636f6e7472617420642761707072656e74697373616765	\N	3601	3602	4	1	1	2011-05-25 10:13:16	2012-03-07 09:41:23
6	1	T1-1359	\\x74656d7073206c6962726520657420736f63696162696c6974c3a9	\N	11	12	1	1	1	2011-05-25 10:02:54	2012-03-07 09:40:52
32	1	T1-861	\\x636f6e646974696f6e73206475207472617661696c	\N	63	64	1	1	1	2011-05-25 10:02:55	2012-03-07 09:40:52
1817	1	T3-177	\\x70726f63c3a8732d76657262616c206427696e6672616374696f6e	\N	3633	3634	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1818	1	T3-133	\\x6dc3a96461696c6c65	\N	3635	3636	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1819	1	T3-101	\\x646f7373696572206dc3a9646963616c	\N	3637	3638	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1820	1	T3-110	\\x6879706f7468c3a8717565206d61726974696d65	\N	3639	3640	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1823	1	T3-66	\\x636f6d7074652072656e6475	\N	3645	3646	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1824	1	T3-90	\\x646f63756d656e742064652073c3a9616e6365	\N	3647	3648	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
1825	1	T3-37	\\x63616869657220646520646f6cc3a9616e6365	\N	3649	3650	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
319	1	T1-878	\\x677265666669657220646520636f6d6d65726365	\N	637	638	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
338	1	T1-870	\\xc3a96c656374696f6e206575726f70c3a9656e6e65	\N	675	676	1	1	1	2011-05-25 10:02:59	2012-03-07 09:40:58
362	1	T1-478	\\xc3a97461626c697373656d656e74207265636576616e74206475207075626c6963	\N	723	724	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
393	1	T1-131	\\x6f7267616e69736d6520636f6e73756c7461746966	\N	785	786	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
398	1	T1-1094	\\x70726f74656374696f6e206d617465726e656c6c6520657420696e66616e74696c65	\N	795	796	1	1	1	2011-05-25 10:03:00	2012-03-07 09:40:59
1815	1	T3-93	\\x646f73736965722064652063616e6469646174757265	\N	3629	3630	4	1	1	2011-05-25 10:13:17	2012-03-07 09:41:24
161	1	T1-46	\\x636f6e74656e7469657578206465206c612073c3a96375726974c3a920736f6369616c65	\N	321	322	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:55
190	1	T1-1167	\\x70726f74656374696f6e2073616e697461697265206475206368657074656c	\N	379	380	1	1	1	2011-05-25 10:02:57	2012-03-07 09:40:56
220	1	T1-513	\\x636f6d7061676e69652064276173737572616e636573	\N	439	440	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:56
238	1	T1-917	\\x74696572732d6f72647265	\N	475	476	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
239	1	T1-633	\\x636f6e6772c3a9676174696f6e	\N	477	478	1	1	1	2011-05-25 10:02:58	2012-03-07 09:40:57
\.


--
-- Name: keywords_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('keywords_id_seq', 1833, false);


--
-- Name: keywordtypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('keywordtypes_id_seq', 10, true);


--
-- Name: metadonnees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('metadonnees_id_seq', 1, true);


--
-- Data for Name: typenotifs; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY typenotifs (id, name) FROM stdin;
1	wkf_new
2	wkf_wait
3	wkf_done
4	ban_xnew
5	ban_cancel
6	ban_response
7	ban_info
8	sys_rights
9	dkt_delegto
10	wkf_jmp
11	wkf_jbk
12	ban_copy
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY notifications (id, name, description, typenotif_id, desktop_id, created, modified) FROM stdin;
\.


--
-- Name: notifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('notifications_id_seq', 1, true);


--
-- Data for Name: notifieddesktops; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY notifieddesktops (id, courrier_id, notification_id, desktop_id, read, created, modified) FROM stdin;
\.


--
-- Name: notifieddesktops_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('notifieddesktops_id_seq', 1, true);


--
-- Data for Name: plandelegations; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY plandelegations (id, user_id, desktop_id, date_start, date_end, created, modified) FROM stdin;
\.


--
-- Name: plandelegations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('plandelegations_id_seq', 1, true);


--
-- Data for Name: recherches; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches (id, name, cname, cdate, cdatereception, user_id, created, modified, cdatefin, cdatereceptionfin, contactname) FROM stdin;
\.


--
-- Data for Name: recheche_contactinfos; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recheche_contactinfos (id, recherche_id, contactinfo_id, created, modified) FROM stdin;
\.


--
-- Name: recheche_contactinfos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recheche_contactinfos_id_seq', 1, true);


--
-- Data for Name: recherches_addressbooks; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_addressbooks (id, addressbook_id, recherche_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_addressbooks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_addressbooks_id_seq', 1, true);


--
-- Data for Name: recherches_affaires; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_affaires (id, affaire_id, created, modified, recherche_id) FROM stdin;
\.


--
-- Name: recherches_affaires_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_affaires_id_seq', 1, true);


--
-- Data for Name: wkf_circuits; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY wkf_circuits (id, nom, description, actif, defaut, created_user_id, modified_user_id, created, modified) FROM stdin;
\.


--
-- Data for Name: recherches_circuits; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_circuits (id, recherche_id, circuit_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_circuits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_circuits_id_seq', 1, true);


--
-- Data for Name: recherches_contacts; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_contacts (id, contact_id, recherche_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_contacts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_contacts_id_seq', 1, true);


--
-- Data for Name: recherches_dossiers; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_dossiers (id, dossier_id, recherche_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_dossiers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_dossiers_id_seq', 1, true);


--
-- Name: recherches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_id_seq', 1, true);


--
-- Data for Name: recherches_metadonnees; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_metadonnees (id, recherche_id, metadonnee_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_metadonnees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_metadonnees_id_seq', 1, true);


--
-- Data for Name: recherches_soustypes; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_soustypes (id, soustype_id, recherche_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_soustypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_soustypes_id_seq', 1, true);


--
-- Data for Name: recherches_taches; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_taches (id, tache_id, created, modified, recherche_id) FROM stdin;
\.


--
-- Name: recherches_taches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_taches_id_seq', 1, true);


--
-- Data for Name: recherches_types; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY recherches_types (id, type_id, recherche_id, created, modified) FROM stdin;
\.


--
-- Name: recherches_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('recherches_types_id_seq', 1, true);


--
-- Data for Name: relements; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY relements (id, name, created, modified, courrier_id) FROM stdin;
\.


--
-- Name: relements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('relements_id_seq', 1, true);


--
-- Name: repertoires_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('repertoires_id_seq', 1, true);


--
-- Data for Name: rights; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY rights (id, get_infos, get_meta, get_taches, get_affaire, create_infos, set_infos, set_meta, set_taches, set_affaire, administration, aiguillage, creation, validation, edition, documentation, created, modified, mode_classique, aro_id, envoi_cpy, envoi_wkf, document_upload, document_suppr, document_setasdefault, relements, relements_upload, relements_suppr, ar_gen, mes_services, recherches, suppression_flux, model, foreign_key) FROM stdin;
1	t	f	t	f	f	t	f	t	f	f	t	f	f	f	f	2013-04-02 11:37:43.523222	2013-04-02 11:37:43.523222	f	7	f	f	t	t	t	f	f	f	f	f	t	t	Group	7
2	t	t	t	t	f	f	t	t	t	f	f	f	f	f	t	2013-04-02 11:37:45.624083	2013-04-02 11:37:45.624083	f	6	t	t	t	t	t	t	t	t	t	t	t	t	Group	6
3	t	t	t	t	f	t	t	t	f	f	f	f	t	t	f	2013-04-02 11:37:48.130788	2013-04-02 11:37:48.130788	f	5	t	t	t	t	t	t	t	t	t	t	t	t	Group	5
4	t	t	t	t	f	f	t	t	f	f	f	f	t	f	f	2013-04-02 11:37:50.466315	2013-04-02 11:37:50.466315	f	4	t	t	t	t	t	t	t	t	t	t	t	t	Group	4
5	t	f	t	f	t	t	f	t	f	f	f	t	f	f	f	2013-04-02 11:37:52.88356	2013-04-02 11:37:52.88356	f	3	t	f	t	t	t	t	t	t	t	f	t	t	Group	3
6	f	f	f	f	f	f	f	f	f	t	f	f	f	f	f	2013-04-02 11:37:55.024301	2013-04-02 11:37:55.024301	f	2	f	f	f	f	f	f	f	f	f	f	f	f	Group	2
\.


--
-- Name: rights_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('rights_id_seq', 7, true);


--
-- Data for Name: rmodels; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY rmodels (id, name, gedpath, content, size, created, modified, soustype_id, mime, preview, ext) FROM stdin;
\.


--
-- Name: rmodels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('rmodels_id_seq', 1, true);


--
-- Data for Name: selectvaluesmetadonnees; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY selectvaluesmetadonnees (id, metadonnee_id, name, created, modified) FROM stdin;
\.


--
-- Name: selectvaluesmetadonnees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('selectvaluesmetadonnees_id_seq', 1, true);


--
-- Name: sequences_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('sequences_id_seq', 1, true);


--
-- Name: services_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('services_id_seq', 1, true);


--
-- Name: soustypes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('soustypes_id_seq', 1, true);


--
-- Name: taches_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('taches_id_seq', 1, true);


--
-- Name: typemetadonnees_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('typemetadonnees_id_seq', 5, true);


--
-- Name: typenotifs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('typenotifs_id_seq', 13, false);


--
-- Data for Name: typenotifs_users; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY typenotifs_users (id, typenotif_id, user_id, created, modified) FROM stdin;
\.


--
-- Name: typenotifs_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('typenotifs_users_id_seq', 1, true);


--
-- Name: types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('types_id_seq', 1, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('users_id_seq', 1, true);


--
-- Name: wkf_circuits_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('wkf_circuits_id_seq', 1, true);


--
-- Data for Name: wkf_etapes; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY wkf_etapes (id, circuit_id, nom, description, type, ordre, created_user_id, modified_user_id, created, modified) FROM stdin;
\.


--
-- Data for Name: wkf_compositions; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY wkf_compositions (id, etape_id, type_validation, created_user_id, modified_user_id, created, modified, trigger_id) FROM stdin;
\.


--
-- Name: wkf_compositions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('wkf_compositions_id_seq', 1, true);


--
-- Name: wkf_etapes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('wkf_etapes_id_seq', 1, true);


--
-- Data for Name: wkf_signatures; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY wkf_signatures (id, type_signature, signature) FROM stdin;
\.


--
-- Name: wkf_signatures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('wkf_signatures_id_seq', 1, true);


--
-- Data for Name: wkf_traitements; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY wkf_traitements (id, circuit_id, target_id, numero_traitement, created_user_id, modified_user_id, created, modified, treated) FROM stdin;
\.


--
-- Name: wkf_traitements_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('wkf_traitements_id_seq', 1, true);


--
-- Data for Name: wkf_visas; Type: TABLE DATA; Schema: public; Owner: webgfc
--

COPY wkf_visas (id, traitement_id, trigger_id, signature_id, etape_nom, etape_type, action, commentaire, date, type_validation, numero_traitement) FROM stdin;
\.


--
-- Name: wkf_visas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: webgfc
--

SELECT pg_catalog.setval('wkf_visas_id_seq', 1, true);


--
-- PostgreSQL database dump complete
--

