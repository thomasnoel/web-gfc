-- 200_patch_1.0.1-1.0.2.sql script
-- Patch de montée de version 1.0.1 vers 1.0.2 pour les bases collectivité
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

create table rights(
	id serial not null primary key,

	get_infos boolean not null default false,
	get_meta boolean not null default false,
	get_taches boolean not null default false,
	get_affaire boolean not null default false,

	create_infos boolean not null default false,
	set_infos boolean not null default false,
	set_meta boolean not null default false,
	set_taches boolean not null default false,
	set_affaire boolean not null default false,

	administration boolean not null default false,
	aiguillage boolean not null default false,
	creation boolean not null default false,
	validation boolean not null default false,
	edition boolean not null default false,
	documentation boolean not null default false,

	envoi_cpy boolean not null default false,
	envoi_wkf boolean not null default false,

	document_upload boolean not null default false,
	document_suppr boolean not null default false,
	document_setasdefault boolean not null default false,
	relements boolean not null default false,
	relements_upload boolean not null default false,
	relements_suppr boolean not null default false,
	ar_gen boolean not null default false,

	mes_services boolean not null default false,
	recherches boolean not null default false,
	suppression_flux boolean not null default false,

	created timestamp without time zone,
	modified timestamp without time zone not null default now(),

	mode_classique boolean not null default false,

	aro_id integer unique not null references aros(id) on update cascade on delete cascade
);

-- *****************************************************************************
-- table users ajout de contrainte d unicité pour username et mail
-- *****************************************************************************
alter table users add constraint username_unique unique (username);
alter table users add constraint mail_unique unique (mail);


-- *****************************************************************************
COMMIT;
-- *****************************************************************************
