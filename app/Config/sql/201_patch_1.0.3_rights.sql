-- 200_patch_1.0.1-1.0.2.sql script
-- Patch de montée de version 1.0.1 vers 1.0.2 pour les bases collectivité
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Stéphane Sampaio
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

alter table rights add column model character varying(255);
alter table rights add column foreign_key integer;

alter table rights drop constraint if exists rights_aro_id_fkey;
alter table rights drop constraint if exists rights_aro_id_key;
alter table rights alter column aro_id drop not null;

alter table groups add column group_id integer references groups(id) on update cascade on delete set null;

-- *****************************************************************************
COMMIT;
-- *****************************************************************************
