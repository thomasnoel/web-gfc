-- 227_patch_admin_schema.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- /!\ CE PATCH DOIT ÊTRE BASÉ SUR LA BASE DE DONNÉES GÉNÉRALE CONTENANT TOUTES LES COLLECTIVITÉS  /!\ ---

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

DROP TABLE IF EXISTS authentifications CASCADE;
CREATE TABLE authentifications(
    id SERIAL NOT NULL PRIMARY KEY,
    host VARCHAR(255) NOT NULL,
    port VARCHAR(5) NOT NULL,
    contexte VARCHAR(255) NOT NULL,
    autcert BYTEA NOT NULL,
    proxy TEXT,
    logpath TEXT NOT NULL,
    use_cas BOOLEAN NOT NULL DEFAULT 'true',
    nom_cert VARCHAR(255),
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE authentifications IS 'Table permettant de stocker les paramètres du serveur d''authentification(CAS)';


COMMIT;
