-- 219_patch_1.3.04.sql script
-- Patch permettant la mise en place des notifications par mail des utilisateurs
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************
DELETE FROM dacos WHERE model='Type' AND foreign_key NOT IN (
	SELECT id FROM types
);



DROP TABLE IF EXISTS metadonnees_soustypes CASCADE;
CREATE TABLE metadonnees_soustypes(
    id SERIAL NOT NULL PRIMARY KEY,
    metadonnee_id integer not null references metadonnees(id) on update cascade on delete cascade,
    soustype_id integer not null references soustypes(id) on update cascade on delete cascade,
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE metadonnees_soustypes IS 'Table de liaison entre les métadonnées et les sous types';

DROP INDEX IF EXISTS metadonnees_soustypes_metadonnee_id_idx;
CREATE INDEX metadonnees_soustypes_metadonnee_id_idx ON metadonnees_soustypes( metadonnee_id );

DROP INDEX IF EXISTS metadonnees_soustypes_soustype_id_idx;
CREATE INDEX metadonnees_soustypes_soustype_id_idx ON metadonnees_soustypes( soustype_id );

DROP INDEX IF EXISTS metadonnees_soustypes_metadonnee_id_soustype_id_idx;
CREATE UNIQUE INDEX metadonnees_soustypes_metadonnee_id_soustype_id_idx ON metadonnees_soustypes(metadonnee_id,soustype_id);


/* Ajout de champs supplémentaires dnas la table recherches */

SELECT add_missing_table_field ('public', 'recherches', 'cobjet', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'creference', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'ccomment', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'recherches', 'contactname', 'INTEGER');



-- INFO: voir http://postgresql.developpez.com/sources/?page=chaines
CREATE OR REPLACE FUNCTION "public"."noaccents_upper" (text) RETURNS text AS
$body$
    DECLARE
        st text;

    BEGIN
        -- On transforme les caractèes accentués et on passe en majuscule
        st:=translate($1,'aàäâeéèêëiïîoôöuùûücçñAÀÄÂEÉÈÊËIÏÎOÔÖUÙÛÜCÇÑ','AAAAEEEEEIIIOOOUUUUCCNAAAAEEEEEIIIOOOUUUUCCN');
        st:=upper(st);

        return st;
    END;
$body$
LANGUAGE 'plpgsql' IMMUTABLE RETURNS NULL ON NULL INPUT SECURITY INVOKER;



DROP TABLE IF EXISTS recherches_desktops CASCADE;
CREATE TABLE recherches_desktops(
    id SERIAL NOT NULL PRIMARY KEY,
    recherche_id integer not null references recherches(id) on update cascade on delete cascade,
    desktop_id integer not null references desktops(id) on update cascade on delete cascade,
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE recherches_desktops IS 'Table de liaison entre les recherches et les desktops';

DROP INDEX IF EXISTS recherches_desktops_recherche_id_idx;
CREATE INDEX recherches_desktops_recherche_id_idx ON recherches_desktops( recherche_id );

DROP INDEX IF EXISTS recherches_desktops_desktop_id_idx;
CREATE INDEX recherches_desktops_desktop_id_idx ON recherches_desktops( desktop_id );

DROP INDEX IF EXISTS recherches_desktops_recherche_id_desktop_id_idx;
CREATE UNIQUE INDEX recherches_desktops_recherche_id_desktop_id_idx ON recherches_desktops(recherche_id,desktop_id);


CREATE INDEX comments_slug_noaccents_upper ON comments( NOACCENTS_UPPER( slug ) );

/* Table pour stocker les valeurs de métadonnées enregistrées dans la recherche */
DROP TABLE IF EXISTS recherches_selectvaluesmetadonnees CASCADE;
CREATE TABLE recherches_selectvaluesmetadonnees(
    id SERIAL NOT NULL PRIMARY KEY,
    recherche_id integer not null references recherches(id) on update cascade on delete cascade,
    selectvaluemetadonnee_id integer references selectvaluesmetadonnees(id) on update cascade on delete cascade,
    metadonnee_id integer references metadonnees(id) on update cascade on delete cascade,
    valeur VARCHAR(255),
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE recherches_selectvaluesmetadonnees IS 'Table de liaison entre les recherches et les selectvaluesmetadonnees';

DROP INDEX IF EXISTS recherches_selectvaluesmetadonnees_recherche_id_idx;
CREATE INDEX recherches_selectvaluesmetadonnees_recherche_id_idx ON recherches_selectvaluesmetadonnees( recherche_id );

DROP INDEX IF EXISTS recherches_selectvaluesmetadonnees_recherche_id_selectvaluemetadonnee_id_idx;
CREATE UNIQUE INDEX recherches_selectvaluesmetadonnees_recherche_id_selectvaluemetadonnee_id_idx ON recherches_selectvaluesmetadonnees(recherche_id,selectvaluemetadonnee_id);

ALTER TABLE contactinfos ALTER COLUMN numvoie TYPE VARCHAR(20);


SELECT add_missing_table_field ('public', 'documents', 'desktop_creator_id', 'INTEGER');


-- Mise à jour des droits sur toute l'applciation pour l'administrateur
UPDATE aros_acos SET (_create, _read, _update, _delete) = (1,1,1,1) WHERE aro_id = '2';


-- Création d'une séquence pour les documents scannés

CREATE SEQUENCE documents_name_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.documents_name_seq OWNER TO webgfc;
COMMIT;
