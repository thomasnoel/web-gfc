-- 230_patch_1.7.2.sql script
/**
 * Created: 14 déc. 2016
 */
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

INSERT INTO contacts (organisme_id, name, nom, addressbook_id, numvoie, nomvoie,  adressecomplete, cp, ville, ban_id, bancommune_id, banadresse, compl)
SELECT id, ' Sans contact', '0Sans contact', addressbook_id, numvoie, nomvoie, adressecomplete, cp, ville, ban_id, bancommune_id, banadresse, compl FROM organismes;


SELECT add_missing_table_field ('public', 'relements', 'mime', 'VARCHAR(255)');
UPDATE relements SET mime = 'application/vnd.oasis.opendocument.text';

SELECT add_missing_table_field ('public', 'relements', 'preview', 'BYTEA');
SELECT add_missing_table_field ('public', 'relements', 'content', 'BYTEA');
COMMIT;
