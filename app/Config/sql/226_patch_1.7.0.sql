-- 225_patch_1.7.0.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

SELECT add_missing_table_field ('public', 'rights', 'carnet_adresse', 'BOOLEAN');
UPDATE rights SET carnet_adresse = false;
ALTER TABLE rights ALTER COLUMN carnet_adresse SET NOT NULL;
ALTER TABLE rights ALTER COLUMN carnet_adresse SET DEFAULT false;

SELECT add_missing_table_field ('public', 'rights', 'statistiques', 'BOOLEAN');
UPDATE rights SET statistiques = false;
ALTER TABLE rights ALTER COLUMN statistiques SET NOT NULL;
ALTER TABLE rights ALTER COLUMN statistiques SET DEFAULT false;

SELECT add_missing_table_field ('public', 'bancontenus', 'desktopmanager_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'bancontenus', 'bancontenus_desktopmanager_id_fkey', 'desktopsmanagers', 'desktopmanager_id', false );


-- Mise en place de nouvelles entrées en BDD pour stocker les connecteurs
-- Ajout des champs pour stocket les informations des parapheurs individuellement par collectivité
SELECT add_missing_table_field ('public', 'connecteurs', 'use_signature', 'BOOLEAN'); -- USE_PARAPHEUR
SELECT add_missing_table_field ('public', 'connecteurs', 'signature_protocol', 'VARCHAR(30)'); -- PARAPHEUR
SELECT add_missing_table_field ('public', 'connecteurs', 'use_signature', 'BOOLEAN'); -- USE_IPARAPHEUR
SELECT add_missing_table_field ('public', 'connecteurs', 'host', 'VARCHAR(255)'); -- IPARAPHEUR_HOST
SELECT add_missing_table_field ('public', 'connecteurs', 'login', 'VARCHAR(255)'); -- IPARAPHEUR_LOGIN
SELECT add_missing_table_field ('public', 'connecteurs', 'pwd', 'VARCHAR(255)'); -- IPARAPHEUR_PWD
SELECT add_missing_table_field ('public', 'connecteurs', 'type', 'VARCHAR(255)'); -- IPARAPHEUR_TYPE
SELECT add_missing_table_field ('public', 'connecteurs', 'visibility', 'VARCHAR(255)'); -- IPARAPHEUR_VISIBILITY
SELECT add_missing_table_field ('public', 'connecteurs', 'wsdl', 'VARCHAR(255)'); -- IPARAPHEUR_WSDL
SELECT add_missing_table_field ('public', 'connecteurs', 'cacert', 'BYTEA'); -- IPARAPHEUR_CACERT
SELECT add_missing_table_field ('public', 'connecteurs', 'clientcert', 'BYTEA'); -- IPARAPHEUR_CLIENTCERT
SELECT add_missing_table_field ('public', 'connecteurs', 'cert', 'BYTEA'); -- IPARAPHEUR_CERT
SELECT add_missing_table_field ('public', 'connecteurs', 'certpwd', 'BYTEA'); -- IPARAPHEUR_CERTPWD
SELECT add_missing_table_field ('public', 'connecteurs', 'nom_cert', 'TEXT'); -- IPARAPHEUR_CERTPWD
--
-- -- Ajout des champs pour stocket les informations des GED individuellement par collectivité
SELECT add_missing_table_field ('public', 'connecteurs', 'use_ged', 'BOOLEAN' );
SELECT add_missing_table_field ('public', 'connecteurs', 'ged_url', 'VARCHAR(255)' );
SELECT add_missing_table_field ('public', 'connecteurs', 'ged_login', 'VARCHAR(255)' );
SELECT add_missing_table_field ('public', 'connecteurs', 'ged_passwd', 'VARCHAR(255)' );
SELECT add_missing_table_field ('public', 'connecteurs', 'ged_repo', 'VARCHAR(255)' );
SELECT add_missing_table_field ('public', 'connecteurs', 'ged_xml_version', 'VARCHAR(1)' );

-- Ajout d'une table permettant de paramétrer des mails type
DROP TABLE IF EXISTS templatemails CASCADE;
CREATE TABLE templatemails(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    subject VARCHAR(255) NOT NULL,
    object TEXT NOT NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE templatemails IS 'Table permettant de définir des mails-type à envoyer';

SELECT add_missing_table_field ('public', 'sendsmails', 'templatemail_id', 'INTEGER' );
SELECT add_missing_constraint ( 'public', 'sendsmails', 'sendsmails_templatemail_id_fkey', 'templatemails', 'templatemail_id', false );

-- Ajout d'un champ pour accepter l'envoi d'un mail en cas de délégations
-- SELECT add_missing_table_field ('public', 'users', 'mail_messervices', 'BOOLEAN');

-- Mise à jour de la table scanemails afin de pouvoir scruter plusieurs adresses mails et donc plusieurs dossiers INBOX
SELECT add_missing_table_field ('public', 'scanemails', 'scriptname', 'VARCHAR(255)');
UPDATE scanemails SET scriptname = name WHERE scanemails.name IS NOT NULL;
ALTER TABLE scanemails ALTER COLUMN scriptname SET NOT NULL;


-- Ajout d'une table permettant de paramétrer des mails type
DROP TABLE IF EXISTS templatenotifications CASCADE;
CREATE TABLE templatenotifications(
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    subject VARCHAR(255) NOT NULL,
    object TEXT NOT NULL,
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE templatenotifications IS 'Table permettant de définir les mails adressés par notifications';


INSERT INTO templatenotifications VALUES (1, 'aiguillage', 'Un flux vient de vous être aiguillé', 'Bonjour #PRENOM# #NOM#,

Un flux vient de vous être aiguillé.

Objet : #OBJET_FLUX#
Référence : #REFERENCE_FLUX#
Commentaire: #COMMENTAIRE_FLUX#

Le flux peut être consulté à cette adresse : #ADRESSE_A_MODIFIER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );


INSERT INTO templatenotifications VALUES (2, 'copy', 'Une copie vient de vous être adressée', 'Bonjour #PRENOM# #NOM#,

Un flux vous a été envoyé pour copie.

Objet : #OBJET_FLUX#
Référence : #REFERENCE_FLUX#
Circuit : #LIBELLE_CIRCUIT#
Commentaire: #COMMENTAIRE_FLUX#
Affaire suivie par: #AFFAIRESUIVIE_PAR#

Le flux peut être consulté à cette adresse : #ADRESSE_A_MODIFIER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

INSERT INTO templatenotifications VALUES (3, 'delegation', 'Un bureau vien de vous être délégué', 'Bonjour #PRENOM# #NOM#,

Un bureau vient de vous être délégué.

Bureau : #NOM_BUREAU#
Date de début: #DEBUT_DELEGATION#
Date de fin: #FIN_DELEGATION#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

INSERT INTO templatenotifications VALUES (4, 'insertion', 'Vous allez recevoir un nouveau flux', 'Bonjour #PRENOM# #NOM#,

Un flux vient d''être inséré dans un circuit dont vous êtes valideur.

Id : #IDENTIFIANT_FLUX#
Référence : #REFERENCE_FLUX#
Objet : #OBJET_FLUX#
Circuit : #LIBELLE_CIRCUIT#

Vous pouvez le consulter à l''adresse : #ADRESSE_A_VISUALISER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

INSERT INTO templatenotifications VALUES (5, 'insertion_reponse', 'Réception d''un flux réponse', 'Bonjour #PRENOM# #NOM#,

Un flux en réponse au flux #FLUX_ORIGINE# vient d''être ajouté dans votre bannette "Mes flux réponse".

Id : #IDENTIFIANT_FLUX#
Référence : #REFERENCE_FLUX#
Objet : #OBJET_FLUX#
Circuit : #LIBELLE_CIRCUIT#

Vous pouvez le consulter à l''adresse : #ADRESSE_A_VISUALISER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );


INSERT INTO templatenotifications VALUES (6, 'refus', 'Un flux a été refusé', 'Bonjour #PRENOM# #NOM#,

Un flux dont vous êtes l''initiateur a été refusé.

Objet : #OBJET_FLUX#
Référence : #REFERENCE_FLUX#
Nouvel identifiant : #IDENTIFIANT_FLUX#

Le flux peut être consulté à cette adresse : #ADRESSE_A_MODIFIER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

INSERT INTO templatenotifications VALUES (7, 'retard_validation', 'Un flux est en retard', 'Bonjour #PRENOM# #NOM#,

Un flux dans votre bannette "Mes flux à traiter" est en retard et attend votre validation.

Id : #IDENTIFIANT_FLUX#
Référence : #REFERENCE_FLUX#
Objet : #OBJET_FLUX#
Circuit : #LIBELLE_CIRCUIT#

Vous pouvez consulter ce flux à l''adresse : #ADRESSE_A_TRAITER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

INSERT INTO templatenotifications VALUES (8, 'traitement', 'Vous avez un flux à traiter', 'Bonjour #PRENOM# #NOM#,

Un flux est arrivé dans votre bannette "Mes flux à traiter".

Id : #IDENTIFIANT_FLUX#
Référence : #REFERENCE_FLUX#
Objet : #OBJET_FLUX#
Circuit : #LIBELLE_CIRCUIT#

Vous pouvez le consulter à l''adresse : #ADRESSE_A_TRAITER#

Très cordialement.

web-GFC

Ce mail de notification a été envoyé par web-GFC pour votre information. Merci de ne pas répondre.', true, NOW(), NOW() );

SELECT pg_catalog.setval('templatenotifications_id_seq', 8, true);

SELECT add_missing_table_field ('public', 'connecteurs', 'ged_cmis_version', 'VARCHAR(10)' );
UPDATE connecteurs SET ged_cmis_version='1.0' WHERE name ILIKE '%GED%';

-- ajout d'une valeur par défaut pour le contact
INSERT INTO contacts (name, nom, organisme_id ) VALUES ( 'Sans contact', 'Sans contact', (SELECT id FROM organismes WHERE name ILIKE '%Sans organisme%') );

SELECT add_missing_table_field ('public', 'contacts', 'pays', 'VARCHAR(50)' );
ALTER TABLE contacts ALTER COLUMN pays SET DEFAULT 'FRANCE';

SELECT add_missing_table_field ('public', 'organismes', 'pays', 'VARCHAR(50)' );
ALTER TABLE organismes ALTER COLUMN pays SET DEFAULT 'FRANCE';

SELECT alter_table_drop_column_if_exists('public', 'users', 'mail_messervices');

SELECT add_missing_table_field ('public', 'soustypes', 'information', 'TEXT' );



DROP TABLE IF EXISTS recherches_services CASCADE;
CREATE TABLE recherches_services(
    id SERIAL NOT NULL PRIMARY KEY,
    recherche_id integer not null references recherches(id) on update cascade on delete cascade,
    service_id integer not null references services(id) on update cascade on delete cascade,
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE recherches_services IS 'Table de liaison entre les recherches et les services';

DROP INDEX IF EXISTS recherches_services_recherche_id_idx;
CREATE INDEX recherches_services_recherche_id_idx ON recherches_services( recherche_id );

DROP INDEX IF EXISTS recherches_services_service_id_idx;
CREATE INDEX recherches_services_service_id_idx ON recherches_services( service_id );

DROP INDEX IF EXISTS recherches_services_recherche_id_service_id_idx;
CREATE UNIQUE INDEX recherches_services_recherche_id_service_id_idx ON recherches_services(recherche_id,service_id);
COMMIT;
