-- 211_patch_1.2-reference_unique_courrier.sql script
-- Patch de montée de version 1.0.4 vers 1.2
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************
DROP SEQUENCE IF EXISTS courriers_reference_seq;
CREATE SEQUENCE courriers_reference_seq START 1;

UPDATE courriers
    SET reference = regexp_replace( reference, '^(.{2}).*$', '' ) || DATE_PART( 'year', NOW() )::TEXT || LPAD( nextval('courriers_reference_seq')::TEXT, 6, '0' )
    WHERE reference IS NOT NULL;

--------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION public.alter_table_drop_constraint_if_exists( text, text, text ) RETURNS bool as
$$
	DECLARE
		p_namespace			alias for $1;
		p_table				alias for $2;
		p_constraintname    alias for $3;
		v_row				record;
		v_query				text;
	BEGIN
		SELECT
				1 INTO v_row
			FROM pg_constraint
				INNER JOIN pg_namespace ON ( pg_constraint.connamespace = pg_namespace.oid )
				INNER JOIN pg_class ON ( pg_constraint.conrelid = pg_class.oid )
			WHERE
				pg_namespace.nspname = p_namespace
				AND pg_class.relname = p_table
				AND pg_constraint.conname = p_constraintname;
		IF FOUND THEN
			RAISE NOTICE 'Alter table %.% - drop constraint %', p_namespace, p_table, p_constraintname;
			v_query := 'ALTER TABLE ' || p_namespace || '.' || p_table || ' DROP constraint ' || p_constraintname || ';';
			EXECUTE v_query;
			RETURN 't';
		ELSE
			RETURN 'f';
		END IF;
	END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.alter_table_drop_constraint_if_exists( text, text, text ) IS
	'Équivalent de la fonctionnalité ALTER TABLE <table> DROP CONSTRAINT <name> disponible à partir de PostgreSQl 9.1';

--------------------------------------------------------------------------------

-- INFO: http://archives.postgresql.org/pgsql-sql/2005-09/msg00266.php
CREATE OR REPLACE FUNCTION public.add_missing_table_field (text, text, text, text)
RETURNS bool as '
DECLARE
  p_namespace alias for $1;
  p_table     alias for $2;
  p_field     alias for $3;
  p_type      alias for $4;
  v_row       record;
  v_query     text;
BEGIN
  select 1 into v_row from pg_namespace n, pg_class c, pg_attribute a
     where
         --public.slon_quote_brute(n.nspname) = p_namespace and
         n.nspname = p_namespace and
         c.relnamespace = n.oid and
         --public.slon_quote_brute(c.relname) = p_table and
         c.relname = p_table and
         a.attrelid = c.oid and
         --public.slon_quote_brute(a.attname) = p_field;
         a.attname = p_field;
  if not found then
    raise notice ''Upgrade table %.% - add field %'', p_namespace, p_table, p_field;
    v_query := ''alter table '' || p_namespace || ''.'' || p_table || '' add column '';
    v_query := v_query || p_field || '' '' || p_type || '';'';
    execute v_query;
    return ''t'';
  else
    return ''f'';
  end if;
END;' language plpgsql;

COMMENT ON FUNCTION public.add_missing_table_field (text, text, text, text) IS 'Add a column of a given type to a table if it is missing';

-- *****************************************************************************

CREATE OR REPLACE FUNCTION public.alter_table_drop_column_if_exists( text, text, text ) RETURNS bool as
$$
	DECLARE
		p_namespace alias for $1;
		p_table     alias for $2;
		p_field     alias for $3;
		v_row       record;
		v_query     text;
	BEGIN
		SELECT 1 INTO v_row FROM pg_namespace n, pg_class c, pg_attribute a
			WHERE
				n.nspname = p_namespace
				AND c.relnamespace = n.oid
				AND c.relname = p_table
				AND a.attrelid = c.oid
				AND a.attname = p_field;
		IF FOUND THEN
			RAISE NOTICE 'Upgrade table %.% - drop field %', p_namespace, p_table, p_field;
			v_query := 'ALTER TABLE ' || p_namespace || '.' || p_table || ' DROP column ' || p_field || ';';
			EXECUTE v_query;
			RETURN 't';
		ELSE
			RETURN 'f';
		END IF;
	END;
$$
LANGUAGE plpgsql;

COMMENT ON FUNCTION public.add_missing_table_field (text, text, text, text) IS 'Drops a column from a table if it exists.';

-------------------------------------------------------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION cakephp_validate_in_list( text, text[] ) RETURNS boolean AS
$$
	SELECT $1 IS NULL OR ( ARRAY[CAST($1 AS TEXT)] <@ CAST($2 AS TEXT[]) );
$$
LANGUAGE sql IMMUTABLE;

COMMENT ON FUNCTION cakephp_validate_in_list( text, text[] ) IS
	'@see http://api.cakephp.org/class/validation#method-ValidationinList';
-------------------------------------------------------------------------------------------------------------------------

-- Ajout de champs supplémentaires dans la table contactinfos pour séparer les champs d'adresse
SELECT add_missing_table_field ('public', 'contactinfos', 'numvoie', 'VARCHAR(6)');
SELECT add_missing_table_field ('public', 'contactinfos', 'typevoie', 'VARCHAR(4)');
SELECT add_missing_table_field ('public', 'contactinfos', 'nomvoie', 'VARCHAR(25)');
-- *****************************************************************************
COMMIT;
-- *****************************************************************************
