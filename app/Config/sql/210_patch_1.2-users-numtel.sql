-- 210_patch_1.2-users-numtel.sql script
-- Patch de montée de version 1.0.4 vers 1.2
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

--------------------------------------------------------------------------------
-- 20140310: Ajout du champ N° de téléphonae pour els utilisateurs de l'application
--------------------------------------------------------------------------------
ALTER TABLE users ADD COLUMN numtel VARCHAR(15);
-- *****************************************************************************
COMMIT;
-- *****************************************************************************

