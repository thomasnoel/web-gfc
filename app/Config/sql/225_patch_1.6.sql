-- 224_patch_1.6.sql script
-- Patch permettant la mise en place des référentiels FANTOIR
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

/**
    Nouvelle fiche-contact
*/
-- table dédiée aux organismes
DROP TABLE IF EXISTS organismes CASCADE;
CREATE TABLE organismes (
    id SERIAL NOT NULL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    addressbook_id INTEGER NOT NULL REFERENCES addressbooks(id) ON UPDATE CASCADE ON DELETE SET NULL,
    ban_id INTEGER REFERENCES bans(id) ON UPDATE CASCADE ON DELETE SET NULL,
    bancommune_id INTEGER REFERENCES banscommunes(id) ON UPDATE CASCADE ON DELETE SET NULL,
    banadresse VARCHAR(255),
    numvoie VARCHAR(255),
    adressecomplete VARCHAR(255),
    compl VARCHAR(255),
    cp VARCHAR(5),
    email VARCHAR(255),
    tel VARCHAR(20),
    portable VARCHAR(20),
    fax VARCHAR(20),
    slug VARCHAR(255),
    active BOOLEAN NOT NULL DEFAULT 'true',
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
DROP INDEX IF EXISTS organismes_addressbook_id_idx;
CREATE INDEX organismes_addressbook_id_idx ON organismes(addressbook_id);
DROP INDEX IF EXISTS organismes_ban_id_idx;
CREATE INDEX organismes_ban_id_idx ON organismes(ban_id);
DROP INDEX IF EXISTS organismes_bancommune_id_idx;
CREATE INDEX organismes_bancommune_id_idx ON organismes(bancommune_id);

ALTER TABLE organismes OWNER TO webgfc;

INSERT INTO organismes (name, addressbook_id ) VALUES ( 'Sans organisme', (SELECT min(addressbooks.id) FROM addressbooks) );

SELECT add_missing_table_field ('public', 'contacts', 'organisme_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'contacts', 'contacts_organisme_id_fkey', 'organismes', 'organisme_id', false );

UPDATE contacts SET organisme_id = ( SELECT id FROM organismes WHERE name = 'Sans organisme' ) WHERE organisme_id IS NULL;
ALTER TABLE contacts ALTER COLUMN organisme_id SET NOT NULL;

SELECT add_missing_table_field ('public', 'courriers', 'typecontact', 'VARCHAR(20)');
SELECT add_missing_table_field ('public', 'courriers', 'contact_id', 'INTEGER');
SELECT add_missing_table_field ('public', 'courriers', 'contact', 'INTEGER');
SELECT add_missing_table_field ('public', 'courriers', 'organisme_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'courriers', 'courriers_organisme_id_fkey', 'organismes', 'organisme_id', false );


SELECT add_missing_table_field ('public', 'contactinfos', 'ban_id', 'INTEGER');
SELECT add_missing_table_field ('public', 'contactinfos', 'bancommune_id', 'INTEGER');
SELECT add_missing_table_field ('public', 'contactinfos', 'banadresse', 'VARCHAR(250)');


-- Mise à jour de la table courriers en injectant les données de contactinfo_id dans le champ contact
-- UPDATE courriers set contact = contactinfo_id where contactinfo_id is not null;


ALTER TABLE users DROP CONSTRAINT IF EXISTS mail_unique;

SELECT add_missing_table_field ('public', 'desktopsmanagers', 'isdispatch', 'BOOLEAN');

--  On supprime les entrées sur les droits de l'initiateur sur la partie send (envoi des flux)
DELETE FROM aros_acos WHERE aro_id IN ( SELECT id FROM aros WHERE parent_id = 3) AND aco_id = (SELECT id FROM acos WHERE alias = 'send');
--  On injecte la possibilité  pour l'initiateur d'avoir accès à l'envoi des flux pour copie (send)
INSERT INTO aros_acos (aro_id, aco_id, _create, _read, _update, _delete) (
SELECT id, (SELECT id FROM acos WHERE alias = 'send'), 1, 1, 1, 1 FROM aros WHERE parent_id=3 );


-- ALTER SEQUENCE courriers_reference_seq RESTART WITH 1 ;
SELECT pg_catalog.setval('desktopsmanagers_id_seq', ( SELECT max(desktopsmanagers.id) + 1 FROM desktopsmanagers ), false);
-- On insère dans la table desktopsmanagers les rôles aiguilleurs et initiateurs qui avaient été exclus
INSERT INTO desktopsmanagers ( id, name, active )  (SELECT id+200, concat( 'Bureau ', name), active FROM desktops where group_id IN (3,7));
-- On associe le rôle de l'utilisateur avec le bureau nouvellement créé
INSERT INTO desktops_desktopsmanagers (desktopmanager_id, desktop_id ) ( SELECT id, id-200 FROM desktopsmanagers WHERE id > 200);

SELECT pg_catalog.setval('desktopsmanagers_id_seq', ( SELECT max(desktopsmanagers.id) + 1 FROM desktopsmanagers ), false);
SELECT pg_catalog.setval('desktops_desktopsmanagers_id_seq', ( SELECT max(desktops_desktopsmanagers.id) + 1 FROM desktops_desktopsmanagers ), false);
-- On récupère les organisations créées en les intégrant à la table organisme
INSERT INTO organismes (name, addressbook_id, email) (SELECT organisation, (SELECT addressbook_id FROM contacts WHERE contactinfos.contact_id = contacts.id), email FROM contactinfos WHERE organisation IS NOT NULL AND organisation != '');

-- o ajoute les droits d'accès au carnet d'adresse à tout le monde
-- DELETE FROM aros_acos WHERE aco_id IN (SELECT id FROM acos WHERE alias = 'Addressbooks');
INSERT INTO aros_acos (aro_id, aco_id, _create, _read, _update, _delete) (
SELECT id, (SELECT id FROM acos WHERE alias = 'Addressbooks'), 1, 1, 1, 1 FROM aros where id != 2 );

SELECT add_missing_table_field ('public', 'documents', 'path', 'TEXT');

-- Ajout d'un champ pour accepter l'envoi d'un mail en cas de délégations
SELECT add_missing_table_field ('public', 'users', 'mail_delegation', 'BOOLEAN');

UPDATE contacts SET civilite='Madame' WHERE civilite ='Mademoiselle';
UPDATE contactinfos SET civilite='Madame' WHERE civilite ='Mademoiselle';


SELECT add_missing_table_field ('public', 'contacts', 'email', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'adresse', 'TEXT');
SELECT add_missing_table_field ('public', 'contacts', 'tel', 'VARCHAR(20)');
SELECT add_missing_table_field ('public', 'contacts', 'portable', 'VARCHAR(20)');
SELECT add_missing_table_field ('public', 'contacts', 'role', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'organismes', 'infoscontact', 'BOOLEAN');

ALTER TABLE contacts ALTER COLUMN addressbook_id DROP NOT NULL;
SELECT add_missing_table_field ('public', 'contacts', 'ville', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'ban_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'contacts', 'contacts_ban_id_fkey', 'bans', 'ban_id', false );
SELECT add_missing_table_field ('public', 'contacts', 'bancommune_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'contacts', 'contacts_bancommune_id_fkey', 'banscommunes', 'bancommune_id', false );
SELECT add_missing_table_field ('public', 'contacts', 'banadresse', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'numvoie', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'compl', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'cp', 'VARCHAR(5)');
SELECT add_missing_table_field ('public', 'contacts', 'nomvoie', 'VARCHAR(255)');

-- Récupération des données de la table contactinfos vers la table contacts
UPDATE contacts SET ville = (SELECT ville FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE ville IS NULL;
UPDATE contacts SET compl = (SELECT compl FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE compl IS NULL;
UPDATE contacts SET cp = (SELECT cp FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE cp IS NULL;
UPDATE contacts SET numvoie = (SELECT numvoie FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE numvoie IS NULL;
UPDATE contacts SET email = (SELECT email FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE email IS NULL;
UPDATE contacts SET adresse = (SELECT adresse FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE adresse IS NULL;
UPDATE contacts SET tel = (SELECT tel FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE tel IS NULL;
UPDATE contacts SET portable = (SELECT portable FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE portable IS NULL;
UPDATE contacts SET role = (SELECT role FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE role IS NULL;
UPDATE contacts SET nomvoie = (SELECT nomvoie FROM contactinfos WHERE contacts.id = contactinfos.contact_id LIMIT 1) WHERE nomvoie IS NULL;

UPDATE courriers SET contact_id =  (SELECT contact_id FROM contactinfos WHERE courriers.contactinfo_id = contactinfos.id LIMIT 1) WHERE contact_id IS NULL;
UPDATE courriers SET organisme_id = ( SELECT id FROM organismes WHERE name = 'Sans organisme' ) WHERE contact_id IS NOT NULL AND organisme_id IS NULL;

SELECT add_missing_table_field ('public', 'organismes', 'ville', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'contacts', 'adressecomplete', 'VARCHAR(255)');
SELECT add_missing_table_field ('public', 'organismes', 'nomvoie', 'VARCHAR(255)');

SELECT add_missing_table_field ('public', 'scanemails', 'desktopmanager_id', 'INTEGER');
SELECT add_missing_constraint ( 'public', 'scanemails', 'scanemails_desktopmanager_id_fkey', 'desktopsmanagers', 'desktopmanager_id', false );


UPDATE scanemails SET desktopmanager_id = ( SELECT desktopmanager_id FROM desktops_desktopsmanagers WHERE desktop_id IN (SELECT desktop_id FROM scanemails) LIMIT 1);
update aros_acos set (_read,_create,_update,_delete) = (1,1,1,1) where  aco_id in (SELECT id FROM acos WHERE alias = 'Addressbooks');


DROP TABLE IF EXISTS recherches_organismes CASCADE;
CREATE TABLE recherches_organismes(
    id SERIAL NOT NULL PRIMARY KEY,
    recherche_id integer not null references recherches(id) on update cascade on delete cascade,
    organisme_id integer not null references organismes(id) on update cascade on delete cascade,
    created timestamp without time zone not null default now(),
    modified timestamp without time zone not null default now()
);
COMMENT ON TABLE recherches_organismes IS 'Table de liaison entre les recherches et les organismes';

DROP INDEX IF EXISTS recherches_organismes_recherche_id_idx;
CREATE INDEX recherches_organismes_recherche_id_idx ON recherches_organismes( recherche_id );

DROP INDEX IF EXISTS recherches_organismes_organisme_id_idx;
CREATE INDEX recherches_organismes_organisme_id_idx ON recherches_organismes( organisme_id );

DROP INDEX IF EXISTS recherches_organismes_recherche_id_organisme_id_idx;
CREATE UNIQUE INDEX recherches_organismes_recherche_id_organisme_id_idx ON recherches_organismes(recherche_id,organisme_id);

SELECT add_missing_table_field ('public', 'recherches', 'cretard', 'VARCHAR(255)');
COMMIT;
