-- 303_patch_2.0.3.sql script
--
-- web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
--
-- @author Arnaud AUZOLAT
-- @copyright Initié par ADULLACT - Développé par ADULLACT Projet
-- @link http://adullact.org/
-- @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3

SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

-- *****************************************************************************
BEGIN;
-- *****************************************************************************

UPDATE courriers SET contact_id = NULL WHERE contact_id NOT IN (SELECT id FROM contacts);
ALTER TABLE courriers ADD CONSTRAINT courriers_contact_id_fkey FOREIGN KEY (contact_id) REFERENCES contacts(id) ON UPDATE CASCADE ON DELETE SET NULL;


ALTER TABLE ordresservices ALTER COLUMN numero DROP NOT NULL;
ALTER TABLE ordresservices ALTER COLUMN name DROP NOT NULL;

-- Table stockant les différentes actions
DROP TABLE IF EXISTS journalevents CASCADE;
CREATE TABLE journalevents (
    id SERIAL NOT NULL PRIMARY KEY,
    user_id integer NOT NULL references users(id) on update cascade on delete cascade,
    username VARCHAR(255) NOT NULL ,
    date timestamp without time zone DEFAULT now() NOT NULL,
    desktop_id integer NOT NULL references desktops(id) on update cascade on delete cascade,
    courrier_id integer,
    reference VARCHAR(255),
    action VARCHAR(255) NOT NULL,
    level VARCHAR(255) NOT NULL,
    message TEXT NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    modified timestamp without time zone DEFAULT now() NOT NULL
);
COMMENT ON TABLE journalevents IS 'Table permettant de stocker les logs utilisateurs par actions';

UPDATE desktopsmanagers SET isdispatch = false WHERE isdispatch IS NULL;
COMMIT;
