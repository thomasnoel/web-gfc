<?php

/**
 * This file is loaded automatically by the app/webroot/index.php file after core.php
 *
 * This file should load/create any application wide configuration settings, such as
 * Caching, Logging, loading additional configuration files.
 *
 * You should also use this file to include any files that provide global functions/constants
 * that your application uses.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
// Setup a 'default' cache configuration for use in the application.
Cache::config('default', array('engine' => 'File'));

/**
 * The settings below can be used to set additional paths to models, views and controllers.
 *
 * App::build(array(
 *     'Plugin' => array('/full/path/to/plugins/', '/next/full/path/to/plugins/'),
 *     'Model' =>  array('/full/path/to/models/', '/next/full/path/to/models/'),
 *     'View' => array('/full/path/to/views/', '/next/full/path/to/views/'),
 *     'Controller' => array('/full/path/to/controllers/', '/next/full/path/to/controllers/'),
 *     'Model/Datasource' => array('/full/path/to/datasources/', '/next/full/path/to/datasources/'),
 *     'Model/Behavior' => array('/full/path/to/behaviors/', '/next/full/path/to/behaviors/'),
 *     'Controller/Component' => array('/full/path/to/components/', '/next/full/path/to/components/'),
 *     'View/Helper' => array('/full/path/to/helpers/', '/next/full/path/to/helpers/'),
 *     'Vendor' => array('/full/path/to/vendors/', '/next/full/path/to/vendors/'),
 *     'Console/Command' => array('/full/path/to/shells/', '/next/full/path/to/shells/'),
 *     'locales' => array('/full/path/to/locale/', '/next/full/path/to/locale/')
 * ));
 *
 */
/**
 * Custom Inflector rules, can be set to correctly pluralize or singularize table, model, controller names or whatever other
 * string is passed to the inflection functions
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 *
 */
Inflector::rules('plural', array(
    'irregular' => array(
        'circuit_user' => 'circuits_users',
        'service_user' => 'services_users',
        'desktop_tache' => 'desktops_taches',
        'courrier_soustype' => 'courriers_soustypes',
        'courrier_user' => 'courriers_users',
        'courrier_metadonnee' => 'courriers_metadonnees',
        'carnetadresse' => 'carnetadresses',
        'typenotif_user' => 'typenotifs_users',
        'courrier_repertoire' => 'courriers_repertoires',
        'courrier_affaire' => 'courriers_affaires',
        'repertoire' => 'repertoires',
        'recherche' => 'recherches',
        'recherche_metadonnee' => 'recherches_metadonnees',
        'recherche_dossier' => 'recherches_dossiers',
        'recherche_affaire' => 'recherches_affaires',
        'recherche_carnetadresse' => 'recherches_carnetadresses',
        'recherche_circuit' => 'recherches_circuits',
        'recherche_type' => 'recherches_types',
        'recherche_soustype' => 'recherches_soustypes',
        'recherche_tache' => 'recherches_taches',
        'recherche_addressbook' => 'recherches_addressbooks',
        'recherche_contact' => 'recherches_contacts',
        'recherche_service' => 'recherches_services',
        'tache' => 'taches',
        'environnement' => 'environnement',
        'formatreponse' => 'formatsreponse',
        'formatreponse_soustype' => 'formatsreponse_soustypes',
        'collectivite_keywordlist' => 'collectivites_keywordlists',
        'selectvaluemetadonnee' => 'selectvaluesmetadonnees',
        'metadonnee_soustype' => 'metadonnees_soustypes',
        'referentielfantoir' => 'referentielsfantoir',
        'referentielfantoirdir' => 'referentielsfantoirdir',
        'referentielfantoircom' => 'referentielsfantoircom',
        'referentielfantoirvoie' => 'referentielsfantoirvoie',
        'scanemail' => 'scanemails',
        'origineflux' => 'originesflux',
        'ban' => 'bans',
        'bancommune' => 'banscommunes',
        'banadresse' => 'bansadresses',
        'desktopmanager' => 'desktopsmanagers',
        'desktop_desktopmanager' => 'desktops_desktopsmanagers',
        'sendmail' => 'sendsmails',
        'rmodelgabarit' => 'rmodelgabarits',
        'contact_organisme' => 'contacts_organismes',
        'desktopmanager_operation' => 'desktopsmanagers_operations',
        'intituleagent' => 'intitulesagents',
        'desktopmanager_intituleagent' => 'desktopsmanagers_intitulesagents',
        'ordreservice' => 'ordresservices',
        'pliconsultatif' => 'plisconsultatifs',
        'desktopmanager_sousoperation' => 'desktopsmanagers_sousoperations',
        'gabaritdocument' => 'gabaritsdocuments',
        'contact_event' => 'contacts_events',
        'contact_operation' => 'contacts_operations',
        'contact_organisme' => 'contacts_organismes',
        'activite_organisme' => 'activites_organismes',
        'soustype_soustypecible' => 'soustypes_soustypescibles',
        'sms' => 'sms'
    )
));

// Enable the Dispatcher filters for plugin assets, and
// CacheHelper.
Configure::write('Dispatcher.filters', array(
    'AssetDispatcher',
    'CacheDispatcher'
));

// Add logging configuration.
CakeLog::config('debug', array(
    'engine' => 'FileLog',
    'types' => array('notice', 'info', 'debug'),
    'file' => 'debug',
));
CakeLog::config('error', array(
    'engine' => 'FileLog',
    'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
    'file' => 'error',
));

Configure::write('Config.language', 'fra');
setlocale(LC_ALL, array('fr_FR', 'fr_FR@euro', 'fr', 'fr_FR.UTF8'));
/**
 * Plugins need to be loaded manually, you can either load them one by one or all of them in a single call
 * Uncomment one of the lines below, as you need. make sure you read the documentation on CakePlugin to use more
 * advanced ways of loading plugins
 *
 * CakePlugin::loadAll(); // Loads all plugins at once
 * CakePlugin::load('DebugKit'); //Loads a single plugin named DebugKit
 *
 */
//CakePlugin::loadAll();
CakePlugin::load('DebugKit');
CakePlugin::load('Addressbook');
CakePlugin::load('Cakeflow');
require_once( App::pluginPath('Cakeflow') . DS . 'Config' . DS . 'cakeflow.conf.php');
CakePlugin::load('DataAcl');
CakePlugin::load('AclExtras');
CakePlugin::load('Pgsqlcake');
CakePlugin::load('Imap');
CakePlugin::load('Jsonmsg');


CakePlugin::load('SimpleComment', array('bootstrap' => false, 'routes' => false));

CakePlugin::load('AuthManager', array('bootstrap' => false, 'routes' => false));
//Configuration du plugin AuthManager
Configure::write('AuthManager.ignorePlugins', array(
    'Database',
    'DebugKit',
    'LdapManager',
    'AuthManager'
));

Configure::write('AuthManager.ignoreActions', array('isAuthorized'));

Configure::write('AuthManager.ignoreControllers', array(
    'Dossiers',
    'Environnement',
    'Allos',
    'Annexes',
    'Commentaires',
    'Webdav',
    'Infosups',
    'Models',
    'Natures',
    'Cakeflow.Traitements'
        )
);
Configure::write('AuthManager.models', array('Profil', 'Desktop', 'Desktopmanager', 'Cakeflow.Circuit', 'Service'));
Configure::write('AuthManager.aros', array('Desktop', 'Profil'));
///Affichage des droits
Configure::write(
        'AuthManager.configCRUD', array(
    'CRUD' => array(
        'controllers' => array('group' => true),
        'Environnement' => array(),
        'index' => array('group' => true),
        'Courriers' => array(),
        'getPreview' => array('group' => true),
        'view' => array('group' => true),
        'setInfos' => array('group' => true),
        'getInfos' => array('group' => true),
        'getFlowControls' => array('group' => true),
        'Documents' => array('group' => true),
        'getFichierScane' => array('group' => true, 'role' => array('admin')),
        'getFusionPreview' => array('group' => true),
        'getFileScannePreview' => array('group' => true),
        'sendToParapheur' => array('group' => true),
        'goNext' => array('group' => true),
        'retour' => array('group' => true),
        'rebond' => array('group' => true),
        'Cakeflow' => array('group' => true, 'role' => array('admin', 'manager')),
        'Circuits' => array('role' => array('admin', 'manager')),
        'Etapes' => array('role' => array('admin', 'manager')),
        'Compositions' => array('role' => array('admin', 'manager')),
        'Seances' => array('group' => true),
        'Users' => array('role' => array('admin', 'manager')),
        'updateDroits' => array('group' => true, 'role' => array('admin')),
        'changeUserMdp' => array('group' => true),
        'changeFormatSortie' => array('group' => true),
        'Desktops' => array('role' => array('admin')),
        //            'Collectivites' => array('role' => array('admin')),
        //            'Sequences' => array('role' => array('admin')),
        'Groups' => array('role' => array('admin')),
        'Services' => array('role' => array('admin', 'manager')),
        'Soustypes' => array('role' => array('admin')),
        'Types' => array('role' => array('admin')),
        'Connecteurs' => array('group' => true),
        //            'Historiques' => array('group' => true, 'role' => array('admin')),
        'Connecteurs' => array('role' => array('admin')),
    ),
    'options' => array('order' => true)
        )
);



if (!function_exists('preg_replace_array')) {

    /**
     * Effectue des remplacements d'expressions réulières d'un array, de
     * manière récursive.
     *
     * @param array $array
     * @param array $replacements Clés regexpes, valeurs chaînes de remplacement
     * @return array
     */
    function preg_replace_array(array $array, array $replacements) {
        $newArray = array();
        foreach ($array as $key => $value) {
            foreach ($replacements as $pattern => $replacement) {
                $key = preg_replace($pattern, $replacement, $key);
            }

            if (is_array($value)) {
                $value = preg_replace_array($value, $replacements);
            } else {
                foreach ($replacements as $pattern => $replacement) {
                    $value = preg_replace($pattern, $replacement, $value);
                }
            }
            $newArray[$key] = $value;
        }
        return $newArray;
    }

}

if (!function_exists('alias_querydata')) {

    /**
     * Remplace des mots par d'autres dans un querydata ou une partie de
     * celui-ci.
     *
     * Exemple:
     * 	$subject = array( 'Foo.id' => array( 'Bar' => 1 ), 'Foobar' => array( 'Foo.bar = Bar.foo' ) );
     * 	$replacement = array( 'Foo' => 'Baz' );
     * 	Résultat: array( 'Baz.id' => array( 'Bar' => 1 ), 'Foobar' => array( 'Baz.bar = Bar.foo' ) );
     *
     * @param array $subject
     * @param array $replacement
     * @return array
     */
    function alias_querydata(array $subject, array $replacement) {
        $regexes = array();
        foreach ($replacement as $key => $value) {
            $key = "/(?<!\.)(?<!\w)({$key})(?!\w)/";
            $regexes[$key] = $value;
        }
        return preg_replace_array($subject, $regexes);
    }

}

/**
 * Remplace les caractères accentués par des caractères non accentués dans
 * une chaîne de caractères.
 *
 * @info il faut utiliser les fonctions mb_internal_encoding et mb_regex_encoding
 *    pour que le système sache quels encodages il traite, afin que le remplacement
 *  d'accents se passe bien.
 *
 * @param string $string
 * @return string
 */
function replace_accents($string) {
    $accents = array(
        '[ÂÀ]',
        '[âà]',
        '[Ç]',
        '[ç]',
        '[ÉÊÈË]',
        '[ééềëè]',
        '[ÎÏ]',
        '[îï]',
        '[ÔÖ]',
        '[ôö]',
        '[ÛÙ]',
        '[ûùü]',
        '[ÿŷ]',
		'[œ]',
		'[æ]'
    );

    $replace = array(
        'A',
        'a',
        'C',
        'c',
        'E',
        'e',
        'I',
        'i',
        'O',
        'o',
        'U',
        'u',
        'y',
		'oe',
		'ae'
    );

    foreach ($accents as $key => $accent) {
        $string = mb_ereg_replace($accent, $replace[$key], $string);
//        $string = preg_replace($accent, $replace[$key], $string);
    }

    return $string;
}

/**
 * Remplace les caractères accentués par des caractères non accentués et met
 * en majuscules dans une chaîne de caractères.
 *
 * @see replace_accents
 *
 * @param string $string
 * @return string
 */
function noaccents_upper($string) {
    return strtoupper(replace_accents($string));
}

/**
 * Echapement des caractères spéciaux pour une utilisation de la chaîne en
 * javascript:
 *      - " devient \"
 *      - \n devient \\n
 *
 * Exemple:
 * <pre>
 *      Bonjour Monsieur "Auzolat"
 *      vous devez vous présenter ...</pre>
 * donne
 * <pre>Bonjour Monsieur \"Auzolat\" \\nvous devez vous présenter ...</pre>
 * @param string $value
 * @return string
 */
function js_escape($value) {
    $value = str_replace('"', '\\"', $value);
    $value = str_replace(array("\r\n", "\r", "\n"), "\\n", $value);
    return $value;
}

function is_JSON() {
	call_user_func_array('json_decode',func_get_args());
	return (json_last_error()===JSON_ERROR_NONE);
}


/**
 * Remplace les caractères accentués par des caractères non accentués et met
 * en majuscules dans une chaîne de caractères.
 *
 * @see replace_accents
 *
 * @param string $string
 * @return string
 */
function translate_calendar($string) {
	$calendar = [
		'days' => 'jour(s)',
		'months' => 'mois',
		'years' => 'année(s)'
	] ;
	$translation = $string[$calendar];
	return $translation;
}

function is_base64_string_s($str, $enc=array('UTF-8', 'ASCII')) {
	return !(($b = base64_decode($str, TRUE)) === FALSE) && in_array(mb_detect_encoding($b), $enc);
}

function is_base64_encoded($data)
{
	if (preg_match('%^[a-zA-Z0-9/+]*={0,2}$%', $data)) {
		return TRUE;
	} else {
		return FALSE;
	}
}


function is_base64($data)
{
	return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $data);
}

function get_string_between($string, $start, $end){
	$string = ' ' . $string;
	$ini = strpos($string, $start);
	if ($ini == 0) return '';
	$ini += strlen($start);
	$len = strpos($string, $end, $ini) - $ini;
	return substr($string, $ini, $len);
}

CakePlugin::load('LdapManager', array('bootstrap' => true, 'routes' => false));

require APP . 'Vendor/autoload.php';

// Retire et réajoute l'autoloader de CakePHP puisque Composer pense que
// c'est le plus important.
// See http://goo.gl/kKVJO7
spl_autoload_unregister(array('App', 'load'));
spl_autoload_register(array('App', 'load'), true, true);
