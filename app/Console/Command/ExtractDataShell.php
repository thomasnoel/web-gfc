<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

App::uses('Cache', 'Cache');

App::uses('AppController', 'Controller');
App::uses('ComponentCollection', 'Controller');

App::uses('AuthComponent', 'Controller/Component');
App::uses('AclComponent', 'Controller/Component');

App::uses('DbAcl', 'Model');
App::uses('AclNode', 'Model');
App::uses('Aco', 'Model');
App::uses('Aro', 'Model');

App::uses('ConnectionManager', 'Model');

App::uses('AclSync', 'AuthManager.Lib');


/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class ExtractDataShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;


	/**
	 *
	 * @var type
	 */
	public $Courrier;
	public $Comment;



    public $headers = array(
        "Référence du flux",
        "Objet du flux",
        "Origine du flux",
        "Date d'entrée dans la collectivité",
        "Date de création du flux",
        "Identité de l'expéditeur",
        "Intitulé du flux",
        "Type",
        "Sous-type",
        "Accusé de réception",
        "Date AR",
        "Commentaires publics",
        "Commentaires privés",
        "Statut"
    );


	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];

            ClassRegistry::init(('Courrier'));
            $this->Courrier = new Courrier();
            $this->Courrier->setDataSource($this->_conn);

		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));
		Configure::write('conn', $this->params['connection']);

	}


	/**
	 *
	 */
	public function main() {

        $this->out('<info>Acquisition des données en cours ...</info>');
        $this->XProgressBar->start(1);

        $etat = array();
        $success = $this->getData( $this->params['etat'] );

        $this->hr();
        if ($success) {
            $this->out('<success>Opération terminée avec succès.</success>');
        } else {
            $this->out('<error>Opération terminée avec erreur(s).</error>');
        }
        $this->hr();
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
        $optionParser->addOption('etat', array(
			'short' => 'e',
            'help' => 'Valeur 0 pour les états Non Clos. Valuer 2 pour les états Clos'
		));


		return $optionParser;
	}

    /**
     *
     * @param type $filename
     * @param type $marche_id
     * @param type $foreign_key
     * @return boolean
     */
    public function getData( $etat ) {
        $Courrier = ClassRegistry::init('Courrier');
        $Comment = ClassRegistry::init('Comment');
        $CourrierMetadonnee = ClassRegistry::init('CourrierMetadonnee');
        $Daco = ClassRegistry::init('Daco');
        $Daro = ClassRegistry::init('Daro');

        $conn = $this->_conn;
        if ($conn != 'default') {
            $Courrier->useDbConfig = $conn;
            $Comment->useDbConfig = $conn;
            $CourrierMetadonnee->useDbConfig = $conn;
            $Daro->useDbConfig = $conn;
            $Daco->useDbConfig = $conn;
        }
        $return = array();

        if( $etat == 0 ) {
            $sqBancontenu = $this->Courrier->Bancontenu->sqDerniereBannette('Courrier.id', array( -1, 0, 1, 3));
//            $conditions[] = array('Bancontenu.etat <>' => array( -1, 0, 1));
            $conditions['OR'] = array(
                'Bancontenu.etat <>' => 2,
                'Courrier.id NOT IN ( SELECT courrier_id FROM bancontenus INNER JOIN courriers ON courriers.id = bancontenus.courrier_id )'
            );
        }
        else if( $etat == 2 ) {
            $sqBancontenu = $this->Courrier->Bancontenu->sqDerniereBannette('Courrier.id', $etat);
            $conditions[] = array('Bancontenu.etat' => $etat);
        }

//$conditions[] = array('Courrier.id'=> 545);
        $courriers = $this->Courrier->find(
            'all',
            array(
                'fields' => array_merge(
                    $this->Courrier->fields(),
                    $this->Courrier->Bancontenu->fields(),
                    array(
                        'Origineflux.name',
//                        'Typologieflux.name',
                        'Soustype.name',
                        'Type.name',
                        'Contact.id',
                        'Contact.name',
                        'Contact.nom',
                        'Contact.prenom'
                    )
                ),
                'conditions' => $conditions,
                'contain' => false,
                'joins' => array(
                    $this->Courrier->join('Bancontenu', array('type' => 'LEFT OUTER', 'conditions' => array("Bancontenu.id IN ( {$sqBancontenu} )") ) ),
                    $this->Courrier->join('Origineflux', array('type' => 'LEFT OUTER' ) ),
//                    $this->Courrier->join('Typologieflux', array('type' => 'LEFT OUTER' ) ),
                    $this->Courrier->join('Organisme', array('type' => 'LEFT OUTER' )),
                    $this->Courrier->join('Contact', array('type' => 'LEFT OUTER' )),
                    $this->Courrier->join('Soustype', array('type' => 'LEFT OUTER' )),
                    $this->Courrier->Soustype->join('Type', array('type' => 'LEFT OUTER' ))
                ),
                'recursive' => -1,
                'order' => array('Courrier.reference ASC')
            )
        );

        $this->XProgressBar->start(count($courriers));
        $this->out('<info>Récupération des données...</info>');

        $fp = fopen(TMP.'file.csv', 'w');
        fputcsv($fp, $this->headers);
        $etat = array(
            '-1' => 'Refusé',
            '0' => 'Traité',
            '1' => 'En cours de traitement',
            '2' => 'Clos'
        );
        foreach( $courriers as $courrier ) {
            $comments = $Comment->find(
                'all',
                array(
                    'conditions' => array(
                        'Comment.target_id' => $courrier['Courrier']['id']
                    ),
                    'contain' => false
                )
            );

            $value = array();
            foreach( $comments as $comment ) {
                if($comment['Comment']['private']) {
                    $courrier['Comment']['private'][] = $comment['Comment']['content'];
                }
                else {
                    $courrier['Comment']['public'][] = $comment['Comment']['content'];
                }
            }
//            $valueComment= implode(" / ", Hash::extract($comments, '{n}.Comment.content'));
//            $valuePrivate= implode(" / ", Hash::extract($comments, '{n}.Comment.private'));

//            $public

//debug($ublic);
//debug($valuePrivate);
//            $valuePrivate = Hash::extract($comments, '{n}.Comment[private=true].content');
//            $private = str_replace(array("\r","\n")," ",$valuePrivate);
            if( isset($courrier['Comment']['public']) && !empty($courrier['Comment']['public']) ) {
                $valuePublic = implode(" / ", @$courrier['Comment']['public']);
                $public = str_replace(array("\r","\n")," ",$valuePublic);
            }
            else {
                $public = '';
            }
            if( isset($courrier['Comment']['private']) && !empty($courrier['Comment']['private'] ) ) {
                $valuePrivate = implode(" / ", $courrier['Comment']['private']);
                $private = str_replace(array("\r","\n")," ",$valuePrivate);
            }
            else {
                $private = '';
            }

            $objet = str_replace(array("\n", ',', '|', ';', "\r"), " ", ($courrier['Courrier']['objet']));
            $intitule = str_replace(array(";", ",", " "), " ", ($courrier['Courrier']['name']));

            $metaAr = $CourrierMetadonnee->find(
                'first',
                array(
                    'conditions' => array(
                        'CourrierMetadonnee.courrier_id' => $courrier['Courrier']['id'],
                        'CourrierMetadonnee.metadonnee_id' => 2
                    ),
                    'contain' => false
                )
            );
            if( !empty( $metaAr) ) {
                $numAR = $metaAr['CourrierMetadonnee']['valeur'];
            }

            $dataValueAr = $CourrierMetadonnee->find(
                'first',
                array(
                    'conditions' => array(
                        'CourrierMetadonnee.courrier_id' => $courrier['Courrier']['id'],
                        'CourrierMetadonnee.metadonnee_id' => 3
                    ),
                    'contain' => false
                )
            );
            if( !empty( $dataValueAr ) ) {
                $dateAR = $dataValueAr['CourrierMetadonnee']['valeur'];
            }


            $row = array(
                Hash::get( $courrier, 'Courrier.reference' ),
                $objet,
//                Hash::get( $courrier, 'Typologieflux.name' ),
                Hash::get( $courrier, 'Origineflux.name' ),
                Hash::get( $courrier, 'Courrier.datereception' ),
                Hash::get( $courrier, 'Courrier.date' ),
                Hash::get( $courrier, 'Contact.name' ),
                $intitule,
                Hash::get( $courrier, 'Type.name' ),
                Hash::get( $courrier, 'Soustype.name' ),
                @$numAR,
                @$dateAR,
                @$public,
                @$private,
                @$etat[$courrier['Bancontenu']['etat']]
            );
            fputcsv($fp, $row, ';');
        }
        fclose($fp);

        $return[] = true;

        $this->XProgressBar->finish();
        return !in_array(false, $return, true);

    }


}
