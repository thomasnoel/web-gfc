<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * Utilisation :
 *  Seuls les organismes sont alimentés , a = ID du carnet adresse et le ban_id est en "dur" dans le code
    *  sudo ./lib/Cake/Console/cake --app app Contact -c webgfc_angouleme_backup -f app/tmp/exemple.csv -a 10 -o true
 *
 * CEtte commande est surotut utilisée par la SAERP et ocnerne les contacts principalement
    * sudo ./lib/Cake/Console/cake --app app Contact -c webgfc_angouleme_backup -f app/tmp/exemple.csv -a 10
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class ContactShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Contact;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Contact = ClassRegistry::init('Contact');
        $this->Contact->useDbConfig = $this->_conn;

	}


	/**
	 *
	 */
	public function main() {

        if( !empty( $this->params['file']) /*&& !empty($this->params['addressbook'])*/ ){
            $createdContact = array();
            $this->out('<info>Acquisition des contacts en cours ...</info>');
            $this->XProgressBar->start(1);

            $this->importContactCsv($this->params['file'], $this->params['addressbook'], $this->params['withorganisme']);

            $this->hr();
            if (!in_array(false, $createdContact, true)) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $this->Contact = ClassRegistry::init('Contact');

        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
        $optionParser->addOption('addressbook', array(
			'short' => 'a'
		));
        $optionParser->addOption('withorganisme', array(
			'short' => 'o',
			'help' => 'withorganisme'
		));
        $optionParser->addOption('file', array(
			'short' => 'f',
            'help' => 'Nom du fichier Contact (format CSV) à intégrer'
		));


		return $optionParser;
	}



    /*
     * Fonction permettant d'intégrer le fichier CSV d'une base contact
     *
     *
     */

    private function _processCsv($contact, $withorganisme) {

        $civilite = array('Monsieur' => 1, 'Madame' => 2, 'Monsieur et Madame' => 3, '' => '');
        $civiliteValue = $civilite[$contact['CIVILITE']];
        if( $withorganisme ) {
            $contacts = array(
                'civilite' => $civiliteValue,
                'nom' => $contact['NOM'],
                'prenom' => $contact['PRENOM'],
                'fonction_id' => $contact['FONCTION'],
                'tel' => $contact['TELEPHONE'],
                'portable' => $contact['PORTABLE'],
                'numvoie' => $contact['NUMVOIE'],
                'nomvoie' => $contact['NOMVOIE'],
                'email' => $contact['MAIL'],
                'adresse' => $contact['ADRESSE'],
                'compl' => $contact['COMPL ADRESSE'],
                'cp' => $contact['CODE POSTAL'],
                'ville' => $contact['VILLE'],
                'active' => 1
            );

            $orgs = array(
                'name' => $contact['ORGANISME'],
                'numvoie' => $contact['NUMVOIEORG'],
                'nomvoie' => $contact['NOMVOIEORG'],
				'adressecomplete' => $contact['ADRESSECOMPLETEORG'],
				'compl' => $contact["COMPLADRORG"],
                'cp' => $contact['CPORG'],
                'ville' => $contact['VILLEORG'],
                'telephone' => $contact['TELEPHONEORG'],
                'portable' => $contact['PORTABLEORG'],
                'email' => $contact['MAILORG'],
                'fax' => $contact["FAX"],
                'siret' => $contact["SIRET"]
            );
        }
        else {
            $contacts = array(
                'civilite' => $civiliteValue,
                'nom' => $contact['NOM'],
                'prenom' => $contact['PRENOM'],
                'role' => $contact['FONCTION'],
                'tel' => $contact['TELEPHONE'],
                'numvoie' => $contact['NUMVOIE'],
                'nomvoie' => $contact['NOMVOIE'],
                'portable' => $contact['PORTABLE'],
                'email' => $contact['MAIL'],
                'adresse' => $contact['ADRESSE'],
                'compl' => $contact['COMPL ADRESSE'],
                'cp' => $contact['CODE POSTAL'],
                'ville' => $contact['VILLE'],
//                    'observations' => $contact['OBSERVATIONS'],
                'active' => 1
            );
        }

		if( $withorganisme ) {
			$res = array(
				'contacts' => $contacts,
				'orgs' => $orgs
			);
		}
		else {
			$res = array(
				'contacts' => $contacts
			);
		}
        return $res;
    }



     /*
     * Fonction permettant d'intégrer les organismes
     *
     *
     */
    private function _processOrgCsv($datas) {
        $orgs = array(
            'name' => trim( $datas['Organisme.name'] ),
            'numvoie' => @$datas['Organisme.numvoie'],
            'nomvoie' => @$datas['Organisme.nomvoie'],
            'cp' => @$datas['Organisme.cp'],
            'ville' => @$datas['Organisme.ville'],
            'pays' => @$datas['Organisme.pays'],
            'tel' => @$datas["Organisme.tel"],
            'portable' => @$datas["Organisme.portable"],
            'compl' => @$datas['Organisme.compl'],
            'email' => @$datas["Organisme.email"],
            'fax' => @$datas["Organisme.fax"],
            'antenne' => @$datas["Organisme.antenne"],
            'siret' => @$datas["Organisme.siret"],
            'adressecomplete' => @$datas["Organisme.adressecomplete"]
        );
        $res = array('orgs' => $orgs );
        return $res;
    }

    /**
     *
     * @param type $filename
     * @param type $contact_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importContactCsv($filename = null, $addressbookId = null, $withorganisme = null) {
        $this->Contact->useDbConfig = $this->_conn;

//ini_set( 'memory_limit', '3000M');
        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {

                $contacts = array();
                $header = NULL;

                while (($row = fgetcsv($handle, 1000000, ';')) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $contacts[] = $tmp;
                    }
                }
                fclose($handle);

               if( isset($withorganisme) && !empty($withorganisme) && $withorganisme == 'true' ) {
                    $with = true;
                }
                else {
                    $with = false;
                }


                $this->XProgressBar->start(count($contacts));
                $this->out('<info>Enregistrement des contacts...</info>');
                $saved = true;
                foreach ($contacts as $contact) {

                    if( isset( $contact['Organisme.name'] )  ) {
                        $contactINFO = $this->_processOrgCsv($contact);
                    }
                    else {
                        $contactINFO = $this->_processCsv($contact, $with);
                    }

                    // On sauvegarde d'abord l'organisme
                    if( !empty( $contactINFO['orgs'] ) ){
                        $orgINFO['Organisme'] = $contactINFO['orgs'];
                        $orgINFO['Organisme']['addressbook_id'] = $addressbookId;
                        if( !empty($contactINFO['orgs']['ADRESSECOMPLETEORG']) ) {
                            $orgINFO['Organisme']['ADRESSECOMPLETEORG'] = str_replace(array("<p>", "</p>", "<br />"), "\t", $contactINFO['orgs']['adressecomplete']);
                        }
						$organismeCsv = $this->Contact->Organisme->find(
							'first',
							array(
								'conditions' => array(
									'Organisme.name' => $orgINFO['Organisme']['name']
								),
								'recursive' => -1
							)
						);
                        // Si l'organisme existe en BDD, on note son ID
                        if( !empty($organismeCsv) ) {
							$OrgId = $organismeCsv['Organisme']['id'];
                        }
                        else if( $orgINFO['Organisme']['name'] == 'Sans organisme' ) {
                           $OrganismeSans = $this->Contact->Organisme->find(
                                'first',
                                array(
                                    'conditions' => array(
                                        'Organisme.addressbook_id' => $addressbookId,
                                        'Organisme.name' => 'Sans organisme'
                                    ),
                                    'recursive' => -1,
                                    'contain' => false
                                )
                            );
                            if( !empty($OrganismeSans) ) {
                                $OrgId = $OrganismeSans['Organisme']['id'];
                            }
                            else {
                                $this->Contact->Organisme->create();
                                $sansOrg = array(
                                    'name' => 'Sans organisme',
                                    'addressbook_id' => $addressbookId
                                );
                                $saved = $this->Contact->Organisme->save($sansOrg) && $saved;
                                $OrgId = $this->Contact->Organisme->id;
                            }
                        }
                        // sinon, on crée une entrée en BDD
                        else {
                            $this->Contact->Organisme->create();
                            $saved = $this->Contact->Organisme->save($orgINFO) && $saved;
                            $OrgId = $this->Contact->Organisme->id;
                        }

                    }
                    // FIn création ORGANISME
					// CREATION DU CONTACT
                    if( empty($contactINFO['contacts']['nom']) ) {
                        $this->Contact->create();
                        $contactsEmptyDATA = array(
                            'modified' => date('Y-m-d'),
                            'civilite' => null,
                            'nom' => '0Sans contact',
                            'name' => ' Sans contact',
                            'prenom' => null,
                            'active' => 1,
                            'organisme_id' => $OrgId
                        );
                        $saved = $this->Contact->save($contactsEmptyDATA) && $saved;
                    }
                    // ensuite on sauvegarde le contact
                    else if( !empty($contactINFO['contacts']) ) {
                        $this->Contact->begin();
                        $contactValue = $contactINFO['contacts'];
                        $contactAlreadyExist = $this->Contact->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Contact.name' => $contactValue['prenom'].' '.$contactValue['nom'],
                                    'Contact.nom' => $contactValue['nom'],
                                    'Contact.prenom' => $contactValue['prenom'],
                                    'Contact.civilite' => $contactValue['civilite'],
                                    'Contact.organisme_id' => $OrgId
                                ),
                                'recursive' => -1
                            )
                        );


						if( !empty( $contactINFO['contacts']['fonction_id'] ) ){
							$fonctionINFO = array();
							$fonctionINFO['Fonction']['name'] = $contactINFO['contacts']['fonction_id'];
							$fonction = $this->Contact->Fonction->find(
								'first',
								array(
									'conditions' => array(
										'Fonction.name' => $fonctionINFO['Fonction']['name']
									),
									'recursive' => -1
								)
							);
							// Si la fonction existe en BDD, on note son ID
							if( !empty($fonction) ) {
								$FonctionId = $fonction['Fonction']['id'];
							}
							else {
								// sinon on crée l'entrée en BDD
								$this->Contact->Fonction->create();
								$saved = $this->Contact->Fonction->save($fonctionINFO) && $saved;
								$FonctionId = $this->Contact->Fonction->id;
							}
							$contactINFO['contacts']['fonction_id'] = $FonctionId;
						}

//debug($contactAlreadyExist);
                        if( empty( $contactAlreadyExist ) ) {
                            $this->Contact->create();
                            $contactDATA['Contact'] = $contactINFO['contacts'];
                            $contactDATA['Contact']['name'] = $contactINFO['contacts']['prenom'].' '.$contactINFO['contacts']['nom'];
                            $contactDATA['Contact']['organisme_id'] = $OrgId;
                            $contactDATA['Contact']['addressbook_id'] = $addressbookId;

                            $saved = $this->Contact->save($contactDATA) && $saved;
                            $contactId = $this->Contact->id;
                        }
                        else {
                            $contactId = $contactAlreadyExist['Contact']['id'];
                            $saved = true;
                        }

                    }

                    if ($saved) {
                        $return[] = true;
                        $this->Contact->commit();

                    } else {
                        $return[] = false;
                        $this->Contact->rollback();
                    }
                    $this->XProgressBar->next();
                }
                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }


}
