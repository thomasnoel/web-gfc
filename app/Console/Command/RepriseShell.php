<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * Reprise shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * Utilisation :
    *  sudo ./lib/Cake/Console/cake --app app Reprise -c webgfc_coll -f app/tmp/exemple.csv
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class RepriseShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Courrier;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Courrier = ClassRegistry::init('Courrier');
        $this->Courrier->useDbConfig = $this->_conn;
		$this->Comment = ClassRegistry::init('Comment');
        $this->Comment->useDbConfig = $this->_conn;
		$this->Contact = ClassRegistry::init('Contact');
        $this->Contact->useDbConfig = $this->_conn;
		$this->Organisme = ClassRegistry::init('Organisme');
        $this->Organisme->useDbConfig = $this->_conn;
		$this->Bancontenu = ClassRegistry::init('Bancontenu');
        $this->Bancontenu->useDbConfig = $this->_conn;
        $this->Desktop = ClassRegistry::init('Desktop');
        $this->Desktop->useDbConfig = $this->_conn;

	}


	/**
	 *
	 */
	public function main() {

        if( !empty( $this->params['file']) ){
            $createdCourrier = array();
            $this->out('<info>Insertion des courriers en cours ...</info>');
            $this->XProgressBar->start(1);

            $this->importCourrierCsv($this->params['file'], $this->params['addressbook']);

            $this->hr();
            if (!in_array(false, $createdCourrier, true)) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $this->Courrier = ClassRegistry::init('Courrier');

        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
        $optionParser->addOption('file', array(
			'short' => 'f',
            'help' => 'Nom du fichier Reprise (format CSV) à intégrer'
		));
        $optionParser->addOption('addressbook', array(
			'short' => 'a'
		));


		return $optionParser;
	}



    /*
     * Fonction permettant de réaliser la correspondance entre les colonnes du fichier et celles de la table courriers
     *
     */

    private function _processCsv($courrier) {

        // Pour les courriers
        $numero = $courrier['Numero'];
        if( strlen( $numero ) === 2 ) {
            $numero = '0000'.$courrier['Numero'];
        }
        elseif( strlen( $numero ) === 3 ) {
            $numero = '000'.$courrier['Numero'];
        }
        elseif( strlen( $numero ) === 4 ) {
            $numero = '00'.$courrier['Numero'];
        }
        if( strlen( $numero ) === 5 ) {
            $numero = '0'.$courrier['Numero'];
        }

        $objet = $courrier['Objet'];
        if( !empty($courrier['Objet2']) ){
            $objet = $courrier['Objet'].', '.$courrier['Objet2'];
        }
        $courriers = array(
            'name' => $objet,
            'reference' => substr( $this->params['file'], 23, -4 ).$numero,
            'date' => $courrier['CreeLe'],
        );

        // Pour les remarques
        $comments = array(
            'objet' => $courrier['TexteRemarques']
        );

        // Pour les contacts
        $nom = 'Sans contact';
        $prenom = '';
        if( !empty($courrier['NomContact']) ) {
            $contact = explode(' ', $courrier['NomContact'] );
            $nom = $contact[0];
            $prenom = @$contact[1];
        }
        $contacts = array(
            'name' => $courrier['NomContact'],
            'nom' => $nom,
            'prenom' => $prenom,
            'active' => 1
        );

        // Pour les organismes
        $orgs = array(
            'name' => $courrier['Entreprise']
        );


        $res = array(
            'courriers' => $courriers,
            'contacts' => $contacts,
            'orgs' => $orgs,
            'comments' => $comments
        );
        return $res;
    }


    /**
     *
     * @param type $filename
     * @param type $courrier_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importCourrierCsv($filename = null, $addressbookId = null) {
        $this->Courrier->useDbConfig = $this->_conn;

        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {

                $courriers = array();
                $header = NULL;

                while (($row = fgetcsv($handle, 1000000, ';')) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $courriers[] = $tmp;
                    }
                }
                fclose($handle);

                $this->XProgressBar->start(count($courriers));
                $this->out('<info>Enregistrement des courriers...</info>');
                $saved = true;
                foreach ($courriers as $courrier) {

                    $courrierINFO = $this->_processCsv($courrier);

                    if( !empty($courrierINFO['courriers']) ) {
                        $this->Courrier->begin();
                        $courrierValue = $courrierINFO['courriers'];
                        $courrierAlreadyExist = $this->Courrier->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'Courrier.reference' => $courrierValue['reference'],
                                    'Courrier.name' => $courrierValue['name']
                                ),
                                'recursive' => -1
                            )
                        );

                        if( !empty( $courrierAlreadyExist ) ) {
                            $courrierId = $courrierAlreadyExist['Courrier']['id'];
                        }
                        else {
                            // Partie Organismes
                            if( !empty( $courrierINFO['orgs'] )) {
                                $orgINFO['Organisme'] = $courrierINFO['orgs'];
                                $orgINFO['Organisme']['addressbook_id'] = $addressbookId;
                                $orgINFO['Organisme']['name'] = $courrierINFO['orgs']['name'];

                                $organisme = $this->Contact->Organisme->find(
                                    'first',
                                    array(
                                        'conditions' => array(
                                            'Organisme.name' => $courrierINFO['orgs']['name']
                                        ),
                                        'recursive' => -1
                                    )
                                );

                                // Si l'organisme existe en BDD, on note son ID
                                if( !empty($organisme) ) {
                                    $OrgId = $organisme['Organisme']['id'];
                                }
                                // sinon, on crée une entrée en BDD
                                else if(!empty($orgINFO['Organisme']['name'])) {
                                    $this->Organisme->create();
                                    $saved = $this->Organisme->save($orgINFO) && $saved;
                                    $OrgId = $this->Organisme->id;
                                }
                                else {
                                    $OrganismeSans = $this->Contact->Organisme->find(
                                        'first',
                                        array(
                                            'conditions' => array(
                                                'Organisme.addressbook_id' => $addressbookId,
                                                'Organisme.name' => 'Sans organisme'
                                            ),
                                            'recursive' => -1,
                                            'contain' => false
                                        )
                                    );
                                    if( !empty($OrganismeSans) ) {
                                        $OrgId = $OrganismeSans['Organisme']['id'];
                                    }
                                    else {
                                        $this->Contact->Organisme->create();
                                        $sansOrg = array(
                                            'name' => 'Sans organisme',
                                            'addressbook_id' => $addressbookId
                                        );
                                        $saved = $this->Contact->Organisme->save($sansOrg) && $saved;
                                        $OrgId = $this->Contact->Organisme->id;
                                    }
                                }
                            }
                            else {
                                $OrganismeSans = $this->Contact->Organisme->find(
                                    'first',
                                    array(
                                        'conditions' => array(
                                            'Organisme.addressbook_id' => $addressbookId,
                                            'Organisme.name' => 'Sans organisme'
                                        ),
                                        'recursive' => -1,
                                        'contain' => false
                                    )
                                );
                                if( !empty($OrganismeSans) ) {
                                    $OrgId = $OrganismeSans['Organisme']['id'];
                                }
                                else {
                                    $this->Contact->Organisme->create();
                                    $sansOrg = array(
                                        'name' => 'Sans organisme',
                                        'addressbook_id' => $addressbookId
                                    );
                                    $saved = $this->Contact->Organisme->save($sansOrg) && $saved;
                                    $OrgId = $this->Contact->Organisme->id;
                                }
                            }

                            // Partie Contacts
                            if( !empty($courrierINFO['contacts']['name']) ) {
                                $contactValue = $courrierINFO['contacts'];
                                $contactAlreadyExist = $this->Contact->find(
                                    'first',
                                    array(
                                        'conditions' => array(
                                            'Contact.name' => $contactValue['name'],
                                            'Contact.nom' => $contactValue['nom'],
                                            'Contact.prenom' => $contactValue['prenom']
                                        ),
                                        'recursive' => -1
                                    )
                                );
                                if( empty( $contactAlreadyExist ) ) {
                                    $contactInfo['Contact'] = $courrierINFO['contacts'];
                                    $contactInfo['Contact']['addressbook_id'] = $addressbookId;
                                    $contactInfo['Contact']['name'] = $courrierINFO['contacts']['name'];
                                    $contactInfo['Contact']['nom'] = $courrierINFO['contacts']['nom'];
                                    $contactInfo['Contact']['prenom'] = $courrierINFO['contacts']['prenom'];
                                    $contactInfo['Contact']['organisme_id'] = $OrgId;

                                    $this->Contact->create();
                                    $saved = $this->Contact->save($contactInfo) && $saved;
                                    $contactId = $this->Contact->id;
                                }
                                else {
                                    $contactId = $contactAlreadyExist['Contact']['id'];
                                    $saved = true;
                                }
                            }
                            else {
                                $contactsEmptyDATA['Contact'] = array(
                                    'name' => ' Sans contact',
                                    'nom' => '0Sans contact',
                                    'prenom' => null,
                                    'active' => 1,
                                    'organisme_id' => $OrgId,
                                    'addressbook_id' => $addressbookId
                                );
                                $contactAlreadyExist = $this->Contact->find(
                                    'first',
                                    array(
                                        'conditions' => $contactsEmptyDATA['Contact'],
                                        'recursive' => -1
                                    )
                                );
                                if( empty( $contactAlreadyExist ) ) {
                                    $this->Contact->create();
                                    $saved = $this->Contact->save($contactsEmptyDATA) && $saved;
                                    $contactId = $this->Contact->id;
                                }
                                else {
                                    $contactId = $contactAlreadyExist['Contact']['id'];
                                    $saved = true;
                                }
                            }

                            $this->Courrier->create();
                            $courrierDATA['Courrier'] = $courrierINFO['courriers'];
                            $courrierDATA['Courrier']['name'] = $courrierINFO['courriers']['name'];
                            $courrierDATA['Courrier']['reference'] = $courrierINFO['courriers']['reference'];
                            $courrierDATA['Courrier']['datereception'] = $courrierINFO['courriers']['date'];
                            $courrierDATA['Courrier']['date'] = date('Y-m-d');
                            $courrierDATA['Courrier']['contact_id'] = $contactId;
                            $courrierDATA['Courrier']['organisme_id'] = $OrgId;

                            $saved = $this->Courrier->save($courrierDATA) && $saved;
                            $courrierId = $this->Courrier->id;

                            // Partie Commentaires
                            if( !empty( $courrierINFO['comments']['objet'] )) {
                                $this->Comment->create();
                                $commentDATA['Comment'] = $courrierINFO['comments'];
                                $commentDATA['Comment']['target_id'] = $courrierId;
                                $commentDATA['Comment']['owner_id'] = 1; //FIXME
                                $commentDATA['Comment']['objet'] = $courrierINFO['comments']['objet'];

                                $saved = $this->Comment->save($commentDATA) && $saved;
                            }

                            // On stocke les données dans la bannette de l'administrateur pour pouvoir consulter les flux
                            $querydata = array(
                                'conditions' => array(
                                    'Desktop.profil_id' => ADMIN_GID
                                ),
                                'contain' => false,
                                'recursive' => -1
                            );
                            $desktop = $this->Desktop->find('first', $querydata);
                            // Partie Bannettes
                            $this->Bancontenu->create();
                            $bancontenuDATA['Bancontenu'] = array(
                                'bannette_id' => BAN_DOCS,
                                'courrier_id' => $courrierId,
                                'etat' => 2,
                                'desktop_id' => $desktop['Desktop']['id']
                            );
                            $this->Bancontenu->save($bancontenuDATA);


                            if ($saved) {
                                $return[] = true;
                                $this->Courrier->commit();

                            } else {
                                $return[] = false;
                                $this->Courrier->rollback();
                            }
                            $this->XProgressBar->next();
                        }
                    }
                }
                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }


}
