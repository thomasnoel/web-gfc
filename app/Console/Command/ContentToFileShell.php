<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * ContentToFile shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class ContentToFileShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Document;

	/**
	 *
	 * @var type
	 */
	public $Courrier;

    public $headers = array(
        "Référence du flux",
        "Nom du document",
        "Document principal ?"
    );

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Document = ClassRegistry::init('Document');
        $this->Document->useDbConfig = $this->_conn;
		$this->Courrier = ClassRegistry::init('Courrier');
        $this->Courrier->useDbConfig = $this->_conn;

	}


	/**
	 *
	 */
	public function main() {

        if( !empty( $this->params['folder']) ){
            $this->out('<info>Acquisition des documents en cours ...</info>');
            $this->XProgressBar->start(1);
            $success = $this->listeDocs( $this->params['folder']);
            $this->hr();
            if ($success) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
        else {
            $this->out('<info>Acquisition des documents en cours ...</info>');
            $this->XProgressBar->start(1);

            $success = $this->exportContentToFile();

            $this->hr();
            if ($success) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();

        }
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $this->Document = ClassRegistry::init('Document');

        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
        $optionParser->addOption('folder', array(
			'short' => 'F',
            'help' => 'Chemin du répertoire destinataire de la reprise.'
		));


		return $optionParser;
	}

    /**
     *
     * @param type $filename
     * @param type $marche_id
     * @param type $foreign_key
     * @return boolean
     * sudo ./lib/Cake/Console/cake --app app ContentToFile -c webgfc_angouleme_backup
     */
    public function exportContentToFile() {
        $this->Document->useDbConfig = $this->_conn;
        $this->Courrier->useDbConfig = $this->_conn;

        $return = array();
        $query = array(
            'fields' => array(
                'Document.id',
                'Document.content',
                'Document.name',
                'Courrier.reference'
            ),
            'joins' => array(
                $this->Document->join( 'Courrier', array('type' => 'INNER' ) )
            ),
            'recursive' => -1,
            'order' => array(
                'Courrier.reference',
                'Document.id'
            )
        );
        $count = $this->Document->find('count', $query);

        $this->XProgressBar->start($count);
        $this->out('<info>Création des documents...</info>');
        $folder = new Folder(WORKSPACE_PATH, true, 0777);

        $query['limit'] = 100;
        $steps = ceil($count/$query['limit']);

        for($i = 0 ; $i < $steps ; $i++) {
            $query['offset'] = $i * $query['limit'];
            $documents = $this->Document->find('all', $query);

            $this->Document->begin();
            foreach( $documents as $document ) {
                if( !empty( $document['Document']['content'] ) ) {
                    // On stocke les fichiers sur le disque dur
                    $fileFolder = new Folder(WORKSPACE_PATH . DS . $this->_conn . DS . $document['Courrier']['reference'], true, 0777);
                    $name = str_replace( "/", "_",$document['Document']['name'] );
                    $name = str_replace( "'", "_", $name );
                    $file = WORKSPACE_PATH . DS . $this->_conn . DS . $document['Courrier']['reference'] . DS . "{$name}";
                    $content = file_put_contents( $file, $document['Document']['content']);


                    // On stocke le chemin du document en base
                    $return[] = $this->Document->updateAll(
                        array(
                            'Document.path' => "'".$file."'",
                            'Document.content' => null
                        ),
                        array( 'Document.id' => $document['Document']['id'] )
                    );
                    $this->XProgressBar->next();
                }
            }

            if( !in_array(false, $return, true) ) {
                $this->Document->commit();
            }
            else {
                $this->Document->rollback();
            }
        }
        $this->XProgressBar->finish();

        /*$conns = ConnectionManager::enumConnectionObjects();
		putenv("PGPASSWORD={$conns['default']['password']}");
        $Contact = ClassRegistry::init('Contact');
		$Contact->setDataSource($this->_conn);
		$cmd = 'psql -U ' . $conns['default']['login'] . ' -p ' . $conns['default']['port'] . ' -h ' . $conns['default']['host'] . ' ' . $conns['default']['database'] ;
		$this->hr(1);
        $this->out($cmd);
        passthru(escapeshellcmd($cmd));
        $cmd2 = 'VACUUM FULL;';
        $this->out($cmd2);
        passthru(escapeshellcmd($cmd2));
		$this->out('<warning>Résultat de la commande</warning>');*/

        return !in_array(false, $return, true);
    }

    /**
     *
     * @param type $filename
     * @param type $marche_id
     * @param type $foreign_key
     * @return boolean
     * sudo ./lib/Cake/Console/cake --app app ContentToFile -c webgfc_angouleme_backup -F docs
     */
    public function listeDocs($folderPath) {

        $conn = $this->_conn;
        if ($conn != 'default') {
            $this->Courrier->useDbConfig = $conn;
            $this->Document->useDbConfig = $conn;
        }
        $documents = $this->Document->find(
            'all',
            array(
                'fields' => array(
                    'Document.name',
                    'Document.main_doc',
                    'Courrier.reference'
                ),
                'joins' => array(
                    $this->Document->join('Courrier')
                ),
                'recursive' => -1,
                'order' => array('Courrier.reference ASC')
            )
        );


        $this->XProgressBar->start(count($documents));
        $this->out('<info>Récupération des données...</info>');

        $fp = fopen( TMP . 'fichiers.csv', 'w');
        fputcsv($fp, $this->headers);
        foreach( $documents as $document ) {
            $row = array(
                Hash::get( $document, 'Courrier.reference' ),
                Hash::get( $document, 'Document.name' ),
                ( Hash::get( $document, 'Document.main_doc' ) == true ) ? 'Oui' : 'Non'
            );
            fputcsv($fp, $row, ';');
        }
        fclose($fp);

        $return[] = true;

        $this->XProgressBar->finish();
        return !in_array(false, $return, true);
    }
}
