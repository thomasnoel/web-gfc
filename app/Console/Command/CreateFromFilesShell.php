<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 *
 * ex:
 *  Lecture de répertoire basic : sudo ./lib/Cake/Console/cake --app app CreateFromFiles process -c collectivite
 *  Lecture d'une boîte mail : sudo ./lib/Cake/Console/cake --app app CreateFromFiles process -m boitemail -c collectivite
 *  Lecture d'un répertoire pour un service donné : sudo ./lib/Cake/Console/cake --app app CreateFromFiles process -s test -c collectivite
 *
 *
 */
class CreateFromFilesShell extends XShell {

	/**
	 * Contains database source to use
	 *
	 * @var string
	 * @access public
	 */
//	public $uses = array('User', 'Courrier');

	/**
	 *
	 * @var type
	 */
	public $repository;

	/**
	 *
	 * @var type
	 */
	public $repositoryOld;

	/**
	 *
	 * @var type
	 */
	public $repositoryInvalid;

	/**
	 *
	 * @var type
	 */
	public $repositoryMerge;
	public $repoMerge;

	/**
	 *
	 * @var type
	 */
	public $repositorySingle;

	/**
	 *
	 * @var type
	 */
	public $repositoryTesseract;

	/**
	 *
	 * @var type
	 */
	public $repositoryTesseractOld;

	/**
	 *
	 * @var type
	 */
	public $repositoryTesseractInvalid;

	/**
	 *
	 * @var type
	 */
	public $repositoryInprogress;

	/**
	 *
	 * @var type
	 */
	public $files = array();

	/**
	 *
	 * @var type
	 */
	public $oldFiles = array();

	/**
	 *
	 * @var type
	 */
	public $invalidFiles = array();

	/**
	 *
	 * @var type
	 */
	public $purgeableFiles = array();

	/**
	 *
	 * @var type
	 */
	public $purgeDate;

	/**
	 *
	 * @var type
	 */
	public $destDesktopId;

	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Courrier;

	/**
	 *
	 * @var type
	 */
	public $mailBoxes;

	/**
	 *
	 * @var type
	 */
	public $Scanemail;

	public $scanTypeId;
	public $scanSoustypeId;

	/**
	 *
	 * @var type
	 */
	public $Document;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();
		$repo = null;
		$repoOld = null;
		$repoInvalid = null;
		$repoMerge = null;
		$repoSingle = null;
		$repoInprogress = null;

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

//		Configure::write('conn', $this->params['connection']);

		ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Courrier = ClassRegistry::init('Courrier');

		$this->Scanemail = ClassRegistry::init('Scanemail');
		$this->Scanemail->useDbConfig = $this->_conn;

		$this->Service = ClassRegistry::init('Service');
		$this->Service->useDbConfig = $this->_conn;

		$this->Document = ClassRegistry::init('Document');
		$this->Document->useDbConfig = $this->_conn;

//                verification et récupération des paramètres de scan (type local)
		if( empty($this->params['mailbox'])) {
			// Chaque service se voit ajouter la possiblité de scanner un répertoire donné
			if( !empty($this->params['service']) ) {
				$service = $this->Service->find(
					'first',
					array(
						'conditions' => array(
							'Service.scan_service_name' => $this->params['service'],
							'Service.scan_active' => true
						),
						'contain' => false
					)
				);
				$repo = $service['Service']['scan_local_path'];
				$repoMerge = $repo . DS . 'merge';
				$repoSingle = $repo . DS . 'single';
				$repoOld = $repo . DS . 'old';
				$repoInvalid = $repoOld . DS . 'invalid';
				$repoInprogress = $repo . DS . 'inprogress';
				$this->destDesktopId = $service['Service']['scan_desktopmanager_id'];
				$this->_checkRepositories($repo, $repoOld, $repoInvalid, $repoMerge, $repoSingle, @$repoInprogress);
				$this->purgeDate = CakeTime::format('Ymd', '-'.$service['Service']['scan_purge_delay'].' days');
				$this->files = $this->_getFiles();
				$this->oldFiles = $this->_getOldFiles();
				$this->purgeableFiles = $this->_getOldFiles(true);
				$this->invalidFiles = $this->_getInvalidFiles();

				// Pour OCérisation
				$repoTesseract = $repo . DS . 'tesseract';
				$repoTesseractOld = $repoTesseract . DS . 'old';
				$repoTesseractInvalid = $repoTesseract . DS . 'invalid';
				$this->_checkRepositoriesTesseract($repo, $repoTesseract, $repoTesseractOld, $repoTesseractInvalid);
				$this->convertedFiles = $this->_getConvertedFiles();
			}
			else {
				if( $coll['Collectivite']['scan_active'] ){

					if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
						$repo = $coll['Collectivite']['scan_local_path'];
						$repoMerge = $repo . DS . 'merge';
						$repoSingle = $repo . DS . 'single';
						$repoOld = $repo . DS . 'old';
						$repoInvalid = $repoOld . DS . 'invalid';
						$repoInprogress = $repo . DS . 'inprogress';

						// Pour OCérisation
						$repoTesseract = $repo . DS . 'tesseract';
						$repoTesseractOld = $repoTesseract . DS . 'old';
						$repoTesseractInvalid = $repoTesseract . DS . 'invalid';
					}


					//verfication et récupération du role destinataire
					if (!empty($coll) && is_int($coll['Collectivite']['scan_desktop_id']) && $coll['Collectivite']['scan_desktop_id'] > 0) {
						$this->destDesktopId = $coll['Collectivite']['scan_desktop_id'];
					}

					if (!is_int($this->destDesktopId) || $this->destDesktopId <= 0) {
						throw new FatalErrorException('Le profil destinataire n\'est pas défini');
					}

					$this->_checkRepositories($repo, $repoOld, $repoInvalid, $repoMerge, $repoSingle, @$repoInprogress);
					$this->purgeDate = CakeTime::format('Ymd', '-1 week');
					$this->files = $this->_getFiles();
					$this->oldFiles = $this->_getOldFiles();
					$this->purgeableFiles = $this->_getOldFiles(true);
					$this->invalidFiles = $this->_getInvalidFiles();

					// Pour OCérisation
					$this->_checkRepositoriesTesseract($repo, $repoTesseract, $repoTesseractOld, $repoTesseractInvalid);
					$this->convertedFiles = $this->_getConvertedFiles();
				}
				else {
					$this->out('<error>Numérisation désactivée.</error>');
					exit;
				}
			}

		}else{
			if (!empty($coll) && $coll['Collectivite']['scan_access_type'] == 'local' && !empty($coll['Collectivite']['scan_local_path'])) {
				$repo = $coll['Collectivite']['scan_local_path'];
				$repoMerge = $repo . DS . 'merge';
				$repoSingle = $repo . DS . 'single';
				$repoOld = $repo . DS . 'old';
				$repoInvalid = $repoOld . DS . 'invalid';
				$repoInprogress = $repo . DS . 'inprogress';

				// Pour OCérisation
				$repoTesseract = $repo . DS . 'tesseract';
				$repoTesseractOld = $repoTesseract . DS . 'old';
				$repoTesseractInvalid = $repoTesseract . DS . 'invalid';
			}
			$this->_checkRepositories($repo, $repoOld, $repoInvalid, $repoMerge, $repoSingle, @$repoInprogress);

			// Pour OCérisation
			$this->_checkRepositoriesTesseract($repo, $repoTesseract, $repoTesseractOld, $repoTesseractInvalid);
			$this->convertedFiles = $this->_getConvertedFiles();

			$scan = $this->Scanemail->find('first',array(
				'conditions' => array(
					'Scanemail.scriptname' => $this->params['mailbox']
				),
				'recursive' => -1
			));
			if (empty($scan) ) {
				throw new FatalErrorException('Le paramètre '.$this->params['mailbox'].' n\'est pas défini dans la configuration de la boîte mail');
			}
			$this->destDesktopId = $scan['Scanemail']['desktop_id'];
			$this->destDesktopmanagerId = $scan['Scanemail']['desktopmanager_id'];
			$this->scanTypeId = isset( $scan['Scanemail']['type_id'] ) ? $scan['Scanemail']['type_id'] : null;
			$this->scanSoustypeId = isset( $scan['Scanemail']['soustype_id'] ) ? $scan['Scanemail']['soustype_id'] : null;
			/*if (!is_int($this->destDesktopId) || $this->destDesktopId <= 0) {
					throw new FatalErrorException('Le profil destinataire de la boîte mail n\'est pas défini');
			}*/
			if (!is_int($this->destDesktopmanagerId) || $this->destDesktopmanagerId <= 0) {
				throw new FatalErrorException('Le bureau destinataire de la boîte mail n\'est pas défini');
			}
			$this->collId = $coll['Collectivite']['id'];
			$this->files = $this->_getMailByBox($this->params['mailbox']);
		}
	}

	/**
	 *
	 * @param type $repo
	 * @param type $repoOld
	 * @param type $repoInvalid
	 * @throws FatalErrorException
	 */
	private function _checkRepositories($repo, $repoOld, $repoInvalid, $repoMerge, $repoSingle, $repoInprogress = null) {
		//Initialisation des répertoires à surveiller
		$this->repository = new Folder($repo);
		if (!isset($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'existe pas.');
		}

		if (!is_readable($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'est pas accessible en écriture.');
		}


		$this->repositoryOld = new Folder($repoOld);
		if (!isset($this->repositoryOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoOld . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoOld . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoOld . ' n\'est pas accessible en écriture.');
		}

		$this->repositoryInvalid = new Folder($repoInvalid);
		if (!isset($this->repositoryInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoInvalid . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoInvalid . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoInvalid . ' n\'est pas accessible en écriture.');
		}


		$this->repositoryMerge = new Folder($repoMerge);
		if (!isset($this->repositoryMerge->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoMerge . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryMerge->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoMerge . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryMerge->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoMerge . ' n\'est pas accessible en écriture.');
		}

		$this->repositorySingle = new Folder($repoSingle);
		if (!isset($this->repositorySingle->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoSingle . ' n\'existe pas.');
		}

		if (!is_readable($this->repositorySingle->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoSingle . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositorySingle->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoSingle . ' n\'est pas accessible en écriture.');
		}

		if( Configure::read('Scan.UseFolderInprogress') ) {
			$this->repositoryInprogress = new Folder($repoInprogress);
			if (!isset($this->repositoryInprogress->path)) {
				throw new FatalErrorException('Le répertoire ' . $repoInprogress . ' n\'existe pas.');
			}
			if (!is_readable($this->repositoryInprogress->path)) {
				throw new FatalErrorException('Le répertoire ' . $repoInprogress . ' n\'est pas accessible en lecture.');
			}

			if (!is_writable($this->repositoryInprogress->path)) {
				throw new FatalErrorException('Le répertoire ' . $repoInprogress . ' n\'est pas accessible en écriture.');
			}
		}
	}

	/**
	 *
	 * @param type $repo
	 * @param type $repoTesseract
	 * @param type $repoTesseractOld
	 * @param type $repoTesseractInvalid
	 * @throws FatalErrorException
	 */
	private function _checkRepositoriesTesseract($repo, $repoTesseract, $repoTesseractOld, $repoTesseractInvalid) {
		//Initialisation des répertoires à surveiller
		$this->repository = new Folder($repo);
		if (!isset($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'existe pas.');
		}

		if (!is_readable($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'est pas accessible en écriture.');
		}

		//Répertoire pour les PDFs convertis en PNGs
		$this->repositoryTesseract = new Folder($repoTesseract);
		if (!isset($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'est pas accessible en écriture.');
		}


		$this->repositoryTesseractOld = new Folder($repoTesseractOld);
		if (!isset($this->repositoryTesseractOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractOld . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryTesseractOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractOld . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryTesseractOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractOld . ' n\'est pas accessible en écriture.');
		}

		$this->repositoryTesseractInvalid = new Folder($repoTesseractInvalid);
		if (!isset($this->repositoryTesseractInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractInvalid . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryTesseractInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractInvalid . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryTesseractInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractInvalid . ' n\'est pas accessible en écriture.');
		}
	}
	/**
	 *
	 * @param type $label
	 * @param type $files
	 */
	private function _printListFiles($label, $files = array()) {
		$this->out('<info>' . $label . ' : </info>');
		if (empty($files)) {
			$this->out('<important> Aucun fichier</important>');
		} else {
			foreach ($files as $file) {
				if( !empty( $file->subject) ) {
					$this->out('<important> - ' . $file->subject . '</important>');
				}
				else {
					$this->out('<important> - ' . $file . '</important>');
				}
			}
		}
	}

	/**
	 *
	 */
	public function status() {
		$this->out('<info>Repertoire des fichiers à traiter (dépôt) : </info><important>' . $this->repository->path . '</important>');
		$this->out('<info>Repertoire des fichiers traités : </info><important>' . $this->repositoryOld->path . '</important>');
		$this->out('<info>Repertoire des PDFs convertis en PNGs : </info><important>' . $this->repositoryTesseract->path . '</important>');
		$this->out('<info>Repertoire des PDFS convertis en PNGs traités : </info><important>' . $this->repositoryTesseractOld->path . '</important>');
		$this->_printListFiles('Fichiers à traiter', $this->files);
		$this->_printListFiles('Fichiers à convertir', $this->convertedFiles);
		$this->_printListFiles('Fichiers traités', $this->oldFiles);
		$this->_printListFiles('Fichiers à supprimer', $this->purgeableFiles);
		$this->_printListFiles('Fichiers en erreur', $this->invalidFiles);
	}

	/**
	 *
	 * @return type
	 */
	private function _getFiles() {
		return $this->repository->find('.*\.pdf', true);
	}


	/**
	 *
	 * @return type
	 */
	private function _getConvertedFiles() {
		return $this->repositoryTesseract->find('.*\.png', true);
	}
	/**
	 *
	 * @return type
	 */
	private function _getInvalidFiles() {
		return $this->repositoryInvalid->find('.*', true);
	}

	/**
	 *
	 * @param type $purgeable
	 * @return type
	 */
	private function _getOldFiles($purgeable = false) {
		$return = array();
		$files = $this->repositoryOld->find('.*', true);
		foreach ($files as $file) {
			$matches = array();
			preg_match('#.*\.webgfcold_([0-9]*)#', $file, $matches);
			if (count($matches) > 1) {
				if ($purgeable) {
					if (intval($matches[1]) <= intval($this->purgeDate)) {
						$return[] = $file;
					}
				} else {
					if (intval($matches[1]) > intval($this->purgeDate)) {
						$return[] = $file;
					}
				}
			}
		}

		return $return;
	}

	/**
	 *
	 * @param type $file
	 * @return type
	 */
	private function _create($file, $conn, $fluxName = null) {


		if( !empty( $this->params['mailbox']) ) {
			$remoteReturn = $this->Courrier->remoteCreateMail($this->destDesktopmanagerId, $file, $this->collId, $conn, $this->scanTypeId, $this->scanSoustypeId);
		}else{

			if($fluxName!=null){
				$arrayFileContents = array();
				foreach($file as $fichier){
					$arrayFileContents[] = file_get_contents($this->repositoryMerge->path . DS .$fluxName . DS . $fichier);
				}
				$remoteReturn = $this->Courrier->remoteCreateDesktopmanager($arrayFileContents, '', true, $this->destDesktopId, $file, $conn, $fluxName);
			}else{
				if( Configure::read('Scan.UseFolderInprogress') ) {
					$remoteReturn = $this->Courrier->remoteCreateDesktopmanager(file_get_contents($this->repositoryInprogress->path . DS . $file), '', true, $this->destDesktopId, $file, $conn);
				}
				else {
					$remoteReturn = $this->Courrier->remoteCreateDesktopmanager(file_get_contents($this->repository->path . DS . $file), '', true, $this->destDesktopId, $file, $conn);
				}
			}
		}


		// LAD / RAD : conversion + OCR
		if( Configure::read('Ocerisation.active') ) {
			if( !empty($remoteReturn['CourrierId']) ) {
				$courrier = $this->Courrier->find(
					'first',
					array(
						'conditions' => array('Courrier.id' => $remoteReturn['CourrierId']),
						'contain' => false,
						'recursive' => -1
					)
				);
				if( empty( $this->params['mailbox']) ) {
					$this->_convertPdfToPng($file, $courrier);
				}
			}

			if( !empty($remoteReturn['DocumentId']) ) {
				$folderPath= WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference'];
				$folderToLook = new Folder($folderPath);
				$txtFiles = $folderToLook->find( '.*\.txt', true);
				foreach ($txtFiles as $txtFile) {
					$dataFile = file_get_contents( WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference']  . DS . $txtFile);
					$this->Courrier->Document->updateAll(
						array('Document.ocr_data' => "'" . pg_escape_bytea($dataFile) . "'"),
						array(
							'Document.id' => $remoteReturn['DocumentId'],
							'Document.courrier_id' => $remoteReturn['CourrierId']
						)
					);

					$this->_deleteTesseractTxtFiles($txtFile, $courrier);


					// Extraction de(s) l'email(s) s'il(s) existe(nt) dans l'ocr_data
					$pattern = '/([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';
					preg_match_all($pattern, $dataFile, $matches);
					$emailDetected = $matches[0];

					// Extraction de(s) n° de téléphone s'il(s) existe(nt) dans l'ocr_data
					preg_match_all('/\b[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\s*[-. ]{1}\s*[0-9]{2}\b/',$dataFile,$number);
					$numberTelDetected = $number;

					// On met le nom du courrier à jour avec la valeur présente dans le doc océrisé
					preg_match('/Objet :(.*)/', $dataFile, $objetvalue);
					preg_match('/Tel:(.*)/', $dataFile, $telvalue);
					preg_match('/Tél :(.*)/', $dataFile, $telvalue2);
					preg_match('/Mail :(.*)/', $dataFile, $mailvalue);
					$objetvalueDetected = '';
					$telvalueDetected = '';
					$mailvalueDetected = '';
					if( !empty($objetvalue[1]) ){
						$objetvalueDetected = $objetvalue[1];
					}
					if( !empty($telvalue[1]) ){
						$telvalueDetected = $telvalue[1];
					}
					if( !empty($mailvalue[1]) ){
						$mailvalueDetected = $mailvalue[1];
					}

					if( isset($objetvalueDetected) && !empty($objetvalueDetected) ) {
						$this->Courrier->updateAll(
							array(
								'Courrier.name' => "'" . $objetvalueDetected . "'"
							),
							array(
								'Courrier.id' => $remoteReturn['CourrierId']
							)
						);
					}
				}
			}
		}

		$return = false;
		if ($remoteReturn['CodeRetour'] == 'CREATEDFILEOK' && empty( $this->params['mailbox'] ) ) {
			if($fluxName!=null){
				$return = $this->_setAsOld($fluxName,false,true);
			}else{
				$return = $this->_setAsOld($file);
			}
		}else if($remoteReturn['CodeRetour'] == 'CREATED' && !empty( $this->params['mailbox'] ) ) {
			$return = true;
		}else{
			// Suppression du flux généré si aucun fichier associé n'a pu être créé
			$this->Desktopmanager = ClassRegistry::init('Desktopmanager');
			$desktopsIds[] = $this->Desktopmanager->getDesktops($this->destDesktopId);
			foreach( $desktopsIds as $i => $desktopId) {
				$banscontenus = $this->Courrier->Bancontenu->find(
					'all',
					array(
						'conditions' => array(
							'Bancontenu.courrier_id' => $remoteReturn['CourrierId'],
							'Bancontenu.desktop_id' => $desktopsIds[$i]
						),
						'recursive' => -1
					)
				);

				if (!empty($banscontenus)) {
					foreach( $banscontenus as $b => $bancontenu ) {
						$this->Courrier->Bancontenu->delete($bancontenu['Bancontenu']['id'], true);
					}
					$this->Courrier->delete($remoteReturn['CourrierId']);
				}
			}
			if(empty( $this->params['mailbox'] )) {
				$this->_setAsOld($file, true);
			}
		}
		return $return;
	}

	/**
	 *
	 * @param type $file
	 * @param type $invalid
	 * @return type
	 */
	private function _setAsOld($file, $invalid = false, $merge = false) {
		$return = false;
		if($merge==true){
			$filePath= $this->repositoryMerge->path . DS . $file;
			$repertoire = new Folder($filePath);
			$fichiers=$repertoire->find('.*\.pdf',true);
			foreach($fichiers as $fichier){
				$tmpFile = new File($repertoire->path . DS . $fichier);
				//deplacer et supprimer les fichiers
				if ($tmpFile->copy(($invalid ? $this->repositoryInvalid->path : $this->repositoryOld->path) . DS . $fichier . '.webgfcold_' . date('Ymd'))) {
					$return = $tmpFile->delete();
				}
			}
			//supprimer répertoire
			$return = $repertoire->delete();

		}else{
			$tmpFile = new File($this->repository->path . DS . $file);
			if( file_exists($this->repository->path . DS . $file)) {
				if ($tmpFile->copy(($invalid ? $this->repositoryInvalid->path : $this->repositoryOld->path) . DS . $file . '.webgfcold_' . date('Ymd'))) {
					$return = $tmpFile->delete();
				}
			}
			else {
				$tmpFileInprogress = new File($this->repositoryInprogress->path . DS . $file);
				if( file_exists($this->repositoryInprogress->path . DS . $file)) {
					if ($tmpFileInprogress->copy($this->repositoryOld->path . DS . $file . '.webgfcold_' . date('Ymd'))) {
						$return = $tmpFileInprogress->delete();
					}
				}
			}
		}

		return $return;
	}

	/**
	 *
	 */
	public function purge() {
		$this->_purge(true);
	}

	/**
	 *
	 * @param type $withInvalid
	 * @return type
	 */
	private function _purge($withInvalid = false) {
		$return = array();
		foreach ($this->purgeableFiles as $file) {
			$tmpFile = new File($this->repositoryOld->path . DS . $file);
			$return[$tmpFile->name] = $tmpFile->delete();

		}
		if ($withInvalid) {
			foreach ($this->invalidFiles as $file) {
				$tmpFile = new File($this->repositoryInvalid->path . DS . $file);
				$return[$tmpFile->name] = $tmpFile->delete();
			}
		}
		$this->out('<info>Fichiers supprimés :</info>', 1, Shell::QUIET);
		if (!empty($return)) {
			foreach ($return as $key => $val) {
				$this->out('<important> - ' . $key . ' : </important>' . ($val ? '<success>Ok</success>' : '<error>Error</error>'), 1, Shell::QUIET);
			}
		} else {
			$this->out('<important> aucun fichier</important>', 1, Shell::QUIET);
		}
		return $return;
	}

	/**
	 *
	 */
	public function process() {
		if (!empty($this->files) || is_dir($this->repositoryMerge->path)) {

			if( empty( $this->params['mailbox'] ) ) {
				$createdFlux = array();
				$this->out('<info>Acquisition des fichiers en cours ...</info>');
				$this->XProgressBar->start(count($this->files));
				$arrayRepertoire = array();

				if(is_dir($this->repositoryMerge->path)){
					$handle = opendir($this->repositoryMerge->path);
					while ( ($dossier = readdir($handle)) !== false ){
						if ( $dossier != ".." && $dossier != "." ) {
							if ( is_dir($this->repositoryMerge->path . "/" . $dossier) ) {
								$arrayFile = array();
								$arrayRepertoire[] = $dossier;
								$merge = $this->repositoryMerge->path . DS . $dossier;
								$doossierSousMerge = new Folder($merge);
								$arrayFile = $doossierSousMerge->find('.*\.pdf', true);
								$createdFlux[] = $this->_create($arrayFile,$this->_conn, $dossier);
								$this->XProgressBar->next();
							}
						}
					}
				}


				if( !empty($this->files) ) {
					if( Configure::read('Scan.UseFolderInprogress') ) {
						foreach ($this->files as $file) {
							$fileToCopy = new File($this->repository->path . DS . $file);
							//On déplace tout vers inprogress afin de lancer le traitement uniquement sur les fichiers réellement présents dans le répertoire de dépôt des scans
							if ($fileToCopy->copy($this->repositoryInprogress->path . DS . $file)) {
								$fileToCopy->delete();
								$createdFlux[] = $this->_create($file, $this->_conn);
							}
							$this->XProgressBar->next();
						}
					}
					else {
						foreach ($this->files as $file) {
							$createdFlux[] = $this->_create($file,$this->_conn);
							$this->XProgressBar->next();
						}
					}
				}

				if( Configure::read('Scan.UseFolderInprogress') ) {
					$checkIfFilesAlreadyThere = $this->repositoryInprogress->find('.*\.pdf', true);
					if (!empty($checkIfFilesAlreadyThere)) {
						foreach ($checkIfFilesAlreadyThere as $fileToTreat) {
							$createdFlux[] = $this->_create($fileToTreat, $this->_conn);
						}
					}
				}
				$this->XProgressBar->finish();
				$this->hr();
				if (!in_array(false, $createdFlux, true)) {
					$this->out('<success>Opération terminée avec succès.</success>');
				} else {
					$this->out('<error>Opération terminée avec erreur(s).</error>');
					$this->out('<info>Utilisez la commande "status" pour connaître l\'état des fichiers.</info>');
					$this->out('<info>Utilisez la commande "purge" pour supprimer les fichiers traités ayant dépassé la date de conservation et les fichiers en erreurs</info>');
				}
				$this->hr();
			}else{
				$createdFlux = array();
				$this->out('<info>Acquisition des mails en cours ...</info>');
				$this->XProgressBar->start(count($this->files));

				foreach ($this->files as $file) {
					if( $file->seen == '0' ) {
						$this->_create($file,$this->_conn);
						$this->out('<info>Acquisition en cours ...</info>');
						$this->XProgressBar->next();
					}else{
						$this->out('<info>Acquisition déjà réalisée</info>');
					}
				}
				$this->XProgressBar->finish();

				$this->hr();
				if (!in_array(false, $createdFlux, true)) {
					$this->out('<success>Opération terminée avec succès.</success>');
				} else {
					$this->out('<error>Opération terminée avec erreur(s).</error>');
					$this->out('<info>Utilisez la commande "status" pour connaître l\'état des fichiers.</info>');
					$this->out('<info>Utilisez la commande "purge" pour supprimer les fichiers traités ayant dépassé la date de conservation et les fichiers en erreurs</info>');
				}
				$this->hr();
			}
		}
		$this->_purge(true);
	}

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
		$optionParser = parent::getOptionParser();
		$optionParser->addOption('mailbox', array(
			'short' => 'm',
			'help' => 'Nom de la boîte mail définie en paramétrage'
		));
		$optionParser->addOption('service', array(
			'short' => 's',
			'help' => 'Nom du service devant être analysé'
		));
		$optionParser->description(__("Outil de gestion des scans"))
			->addSubcommand('status', array(
				'help' => __('Vérifications du contenu des fichiers du répertoire de dépot et des correspondances en base de données.')
			))
			->addSubcommand('process', array(
				'help' => __('Lecture, creation et purge des fichiers du répertoire de dépot (acquisitions précédentes)')
			))
			->addSubcommand('purge', array(
				'help' => __('Purge des fichiers du répertoire de dépot.')
			));

		return $optionParser;
	}

	function getBody($uid, $imap)
	{
		$body = $this->get_part($imap, $uid, "TEXT/HTML");
		// if HTML body is empty, try getting text body
		if ($body == "") {
			$body = $this->get_part($imap, $uid, "TEXT/PLAIN");
		}
		return $body;
	}

	function get_part($imap, $uid, $mimetype, $structure = false, $partNumber = false)
	{
		if (!$structure) {
			$structure = imap_fetchstructure($imap, $uid, FT_UID);
		}
		if ($structure) {
			if ($mimetype == $this->get_mime_type($structure)) {
				if (!$partNumber) {
					$partNumber = 1;
				}
				$text = imap_fetchbody($imap, $uid, $partNumber, FT_UID);
				switch ($structure->encoding) {
					case 3:
						return imap_base64($text);
					case 4:
						return imap_qprint($text);
					default:
						return $text;
				}
			}

			// multipart
			if ($structure->type == 1) {
				foreach ($structure->parts as $index => $subStruct) {
					$prefix = "";
					if ($partNumber) {
						$prefix = $partNumber . ".";
					}
					$data = $this->get_part($imap, $uid, $mimetype, $subStruct, $prefix . ($index + 1));
					if ($data) {
						return $data;
					}
				}
			}
		}
		return false;
	}

	function get_mime_type($structure)
	{
		$primaryMimetype = ["TEXT", "MULTIPART", "MESSAGE", "APPLICATION", "AUDIO", "IMAGE", "VIDEO", "OTHER"];

		if ($structure->subtype) {
			return $primaryMimetype[(int)$structure->type] . "/" . $structure->subtype;
		}
		return "TEXT/PLAIN";
	}

	private function _getMailByBox( $data ) {
		$this->Scanemail = ClassRegistry::init('Scanemail');
//            $this->Scanemail->useDbConfig = 'default';

		$scan = $this->Scanemail->find('first',array(
			'conditions' => array(
				'Scanemail.scriptname' => $data
			),
			'recursive' => -1
		));

		$boiteMail = $scan['Scanemail']['hostname'];
		$folder = $scan['Scanemail']['name'];
		$configuration = $scan['Scanemail']['configuration'];

		$port = $scan['Scanemail']['port'];
		$login = $scan['Scanemail']['username'];
		$motDePasse = $scan['Scanemail']['password'];
		$hostname = '{'.$boiteMail.':'.$port.''.$configuration.'}';

		$mbox = @imap_open($hostname.$folder, $login, $motDePasse, NULL, 1, array( 'DISABLE_AUTHENTICATOR' => 'GSSAPI' ) );
		// pour récupérer les alertes et erreurs imap
		imap_errors();
		imap_alerts();
		// Si la onnexion ne s'est pas faite, on regarde si les mots de passe ne sont pas chiffrés et on ré-essaie
		if (FALSE === $mbox ) {
			// On décode le mot de passe en BDD
			if( Configure::read('Crypt.Password') ) {
				$cle = "MaCleEstIncassable";
				$motDePasse = $this->Scanemail->decrypt( $cle, $scan['Scanemail']['password'] );
			}
			$mbox = @imap_open($hostname.$folder, $login, $motDePasse, NULL, 1, array( 'DISABLE_AUTHENTICATOR' => 'GSSAPI' ) ) or die( 'Connexion au serveur de messagerie impossible : '. imap_last_error() );
		}

		$mails = array();
		if (FALSE === $mbox) {
			$err = 'La connexion a échoué. Vérifiez vos paramètres!';
		}else{
			$info = imap_check($mbox);

			if (FALSE !== $info) {
				$nbMessages = $info->Nmsgs;
				$mails = imap_fetch_overview($mbox, '1:'.$nbMessages, 0);

				// On regarde si les messages sont lus ou non
				$status = imap_status($mbox, $hostname.$folder, SA_ALL);

				$count = $this->CountUnreadMail($hostname.$folder, $login, $motDePasse);

				if( !empty($count) && $count != 0) {
					$nb = 0;

					for( $j=1;$j<=$nbMessages;$j++) {
//debug($mails[$nb]);
//$this->log( $mails );
						$mails[$nb]->from = quoted_printable_decode( imap_utf8($mails[$nb]->from));
						$mails[$nb]->subject = quoted_printable_decode( imap_utf8(@$mails[$nb]->subject));
						$mails[$nb]->body = imap_fetchbody($mbox,$j, "1.1"); // FIXME 1 pour Angouleme
						$mails[$nb]->body = str_replace( array( "<p>", "</p>", "<br />" ), "\t", quoted_printable_decode($mails[$nb]->body) );
//                            $mails[$nb]->body = nl2br( quoted_printable_decode($mails[$nb]->body) );
						if( empty($mails[$nb]->body) || $mails[$nb]->body == '') {
							$mails[$nb]->body = imap_fetchbody($mbox,$j, "1");

							$mails[$nb]->body = str_replace( array( "<p>", "</p>", "<br />" ), "\t", ( quoted_printable_decode($mails[$nb]->body) ) );
						}
						// Pour les mails en HTML
						$mails[$nb]->bodyHtml = imap_fetchbody($mbox,$j, "2");
						$mails[$nb]->bodyOtherHtml = imap_fetchbody($mbox,$j, "1");
						$mails[$nb]->mailHeader = imap_fetchbody($mbox,$j, "0");
//$this->log( $mails[$nb]->bodyHtml );
//$this->log( $mails[$nb]->body );
						// Pour les pièces jointes
						$structure = imap_fetchstructure($mbox, $j);


						/*if( strpos( $mails[$nb]->body, '<!DOCTYPE') !== false || strpos( $mails[$nb]->body, '<html ') !== false || strpos( $mails[$nb]->body, '--_000') !== false ) {
							$mails[$nb]->body = strip_tags( $mails[$nb]->body, "<style>" );
							$substring = substr($mails[$nb]->body ,strpos($mails[$nb]->body ,"<style"),strpos($mails[$nb]->body ,"</style>")+2);
							$mails[$nb]->body = str_replace($substring,"",$mails[$nb]->body);
							$mails[$nb]->body = trim($mails[$nb]->body);
						}*/
						$attachments = array();
						if(isset($structure->parts) && count($structure->parts)) {
							for($i = 0; $i < count($structure->parts); $i++) {
								$attachments[$i] = array(
									'is_attachment' => false,
									'filename' => '',
									'attachment' => '',
									'content' => '',
									'size' => '',
									'subtype' => ''
								);

								if($structure->parts[$i]->ifdparameters) {
									foreach($structure->parts[$i]->dparameters as $object) {
										if(strtolower($object->attribute) == 'filename') {
											$attachments[$i]['is_attachment'] = true;
											$attachments[$i]['filename'] = imap_utf8( $object->value );
											$attachments[$i]['subtype'] = $structure->parts[$i]->subtype;
											$attachments[$i]['size'] = $structure->parts[$i]->bytes;
										}
									}
								}

								if($structure->parts[$i]->ifparameters) {
									foreach($structure->parts[$i]->parameters as $object) {
										if(strtolower($object->attribute) == 'name') {
											$attachments[$i]['is_attachment'] = true;
											$attachments[$i]['filename'] = imap_utf8( $object->value );
											$attachments[$i]['subtype'] = $structure->parts[$i]->subtype;
											$attachments[$i]['size'] = $structure->parts[$i]->bytes;
										}
									}
								}

								/// Cas des sous-niveaux de PJ ???
								if(isset( $structure->parts[$i]->parts) ){

									for( $y=0; $y < count($structure->parts[$i]->parts);$y++) {
										if($structure->parts[$i]->parts[$y]->ifdparameters) {

											foreach($structure->parts[$i]->parts[$y]->dparameters as $object) {
												if(strtolower($object->attribute) == 'filename') {
													$attachments[$i]['is_attachment'] = true;
													$attachments[$i]['filename'] = imap_utf8( $object->value );
													$attachments[$i]['subtype'] = $structure->parts[$i]->parts[$y]->subtype;
													$attachments[$i]['size'] = $structure->parts[$i]->parts[$y]->bytes;
												}
											}

											if($attachments[$i]['is_attachment']) {
												$attachments[$i]['attachment'] = imap_fetchbody($mbox, $j, '2.2');
												/* 3 = BASE64 encoding */
												if($structure->parts[$i]->parts[$y]->encoding == 3) {
													$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
												}
												/* 4 = QUOTED-PRINTABLE encoding */
												else if($structure->parts[$i]->parts[$y]->encoding == 4) {
													$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
												}
											}
										}
									}
								}
								else {

									if($attachments[$i]['is_attachment']) {
										$attachments[$i]['attachment'] = imap_fetchbody($mbox, $j, $i+1);

										/* 3 = BASE64 encoding */
										if($structure->parts[$i]->encoding == 3) {
											$attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
										}
										/* 4 = QUOTED-PRINTABLE encoding */
										else if($structure->parts[$i]->encoding == 4) {
											$attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
										}
									}
								}
							}
						}
						$mails[$nb]->pj = $attachments;
						$nb++;
					}

					// Marquer tous les mails traités en lu
					$result = imap_search($mbox, 'UNSEEN');
					if( !empty( $result ) ) {
						foreach ($result as $mail) {
							$status = imap_setflag_full($mbox, $mail, "\\Seen \\Flagged");
						}
					}
					$result = imap_search($mbox, 'UNSEEN');
					echo "<br>Resultats: ";
					if($result==false)
						debug( "Il n'y a plus de mails non lus" );
					else{
						debug("Il reste des mails non lus" );

					}
//                        }

				}
			}else{
				debug(imap_errors());
				$err = 'Impossible de lire le contenu de la boite mail';
			}
			imap_close($mbox);
		}

		if (FALSE === $mails) {
			echo $err;
		}

		return $mails;
	}

	public function CountUnreadMail($host, $login, $passwd) {
		$mbox = imap_open($host, $login, $passwd);
		$count = 0;
		if (!$mbox) {
			echo "Error";
		} else {
			$headers = imap_headers($mbox);
			foreach ($headers as $mail) {
				$flags = substr($mail, 0, 4);
				$isunr = (strpos($flags, "U") !== false);
				if ($isunr)
					$count++;
			}
		}

		imap_close($mbox);
		return $count;
	}


	/**
	 * Conversion des PDFs en PNGs pour exploitation future
	 * @param type $file
	 * @return type
	 */
	private function _convertPdfToPng($pdfFile, $courrier) {
		$pos_point = strpos($pdfFile, '.');
		$filename = substr($pdfFile, 0, $pos_point);

		$pdf = $this->repository->path.'/'.$pdfFile;
		$png = $this->repositoryTesseract->path. DS .$filename.".png";
		$convert = "gs -sDEVICE=png16m -dTextAlphaBits=4 -r300 -o $png $pdf";

		$filenamePath = WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference'] . DS . $filename;
		$ocerisation = "tesseract $png $filenamePath";

		exec($convert);
		exec($ocerisation);

		// le fichier stocké en txt est enregistré comme PJ du flux
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$fileMimeType = $finfo->buffer($filename);
		$dataFile = file_get_contents($filenamePath.".txt");
		$doc = array(
			'Document' => array(
				'name' => $filename.".txt",
				'size' => strlen($png),
				'mime' => $fileMimeType,
				'ext' => 'txt',
				'courrier_id' => $courrier['Courrier']['id'],
				'main_doc' => false,
				'path' => $filenamePath.".txt"
			)
		);
		$this->Document->setDataSource($this->_conn);
		$this->Document->create($doc);
		$this->Document->save();

		$this->_setAsTesseractOld($filename);
	}

	/**
	 *
	 * @param type $file
	 * @param type $invalid
	 * @return type
	 */
	private function _setAsTesseractOld($file) {
		$return = false;
		$tmpFile = new File($this->repositoryTesseract->path . DS . $file.".png");

		if ($tmpFile->copy($this->repositoryTesseractOld->path . DS . $file.".png" . '.webgfcold_' . date('Ymd'))) {
			$return = $tmpFile->delete();
		}

		return $return;
	}

	/**
	 *
	 * @param type $file
	 * @param type $invalid
	 * @return type
	 */
	private function _deleteTesseractTxtFiles($file, $courrier) {
		$return = false;
		$txtFile = new File( WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference'] . DS . $file);
		$return = $txtFile->delete();

		$docId = $this->Courrier->Document->find(
			'first',
			array(
				'fields' => array('Document.id'),
				'conditions' => array(
					'Document.name' => $file
				),
				'contain' => false,
				'recursive' => -1
			)
		);

		$return = $this->Courrier->Document->delete($docId['Document']['id']);

		return $return;
	}
}

