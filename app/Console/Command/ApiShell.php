<?php

App::uses('Cache', 'Cache');

App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
App::uses('ComponentCollection', 'Controller');

App::uses('AuthComponent', 'Controller/Component');
App::uses('AclComponent', 'Controller/Component');
App::uses('XShell', 'Console/Command');
App::uses('Model', 'AppModel');
App::uses('DbAcl', 'Model');
App::uses('AclNode', 'Model');
App::uses('Aco', 'Model');
App::uses('Aro', 'Model');

App::uses('ConnectionManager', 'Model');

App::uses('AclSync', 'AuthManager.Lib');

/**
 *
 * AdminTools shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class ApiShell extends XShell {

	/**
	 *
	 * @var type
	 */
	private $User;

	/**
	 *
	 * @var type
	 */
	private $Collectivite;

	/**
	 *
	 * @var type
	 */
	private $Profil;

	/**
	 *
	 * @var type
	 */
	private $Desktop;

	/**
	 *
	 * @var type
	 */
	private $_showHelp;

	/**
	 *
	 * @var type
	 */
	private $_conn;

	const SUCCESS = 0;

	/**
	 *
	 * @var type
	 */
	private $Courrier;
	/**
	 *
	 */
	public function startup() {
		parent::startup();

		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		if ($this->_conn != 'default') {

			ClassRegistry::init(('User'));
			$this->User = new User();
			$this->User->setDataSource($this->_conn);

			ClassRegistry::init(('Desktop'));
			$this->Desktop = new Desktop();
			$this->Desktop->setDataSource($this->_conn);

			ClassRegistry::init(('Profil'));
			$this->Profil = new Profil();
			$this->Profil->setDataSource($this->_conn);

			$collection = new ComponentCollection();
			$this->Acl = new AclComponent($collection);
			$this->Acl->startup(new AppController());

			$this->Aco = $this->Acl->Aco;
			$this->Aco->setDataSource($this->_conn);

			$this->Aro = $this->Acl->Aro;
			$this->Aro->setDataSource($this->_conn);


			ClassRegistry::init(('Courrier'));
			$this->Courrier = new Courrier();
			$this->Courrier->setDataSource($this->_conn);

			ClassRegistry::init(('Connecteur'));
			$this->Connecteur = new Connecteur();
			$this->Connecteur->setDataSource($this->_conn);

			ClassRegistry::init(('Journalevent'));
			$this->Journalevent = new Journalevent();
			$this->Journalevent->setDataSource($this->_conn);
		} else {
			ClassRegistry::init(('Collectivite'));
			$this->Collectivite = new Collectivite();
			$this->Collectivite->setDataSource($this->_conn);
		}
	}


	public function process() {
		if (!empty($this->params['file']) ) {
			$this->out('<info>Création de la collectivité en cours ...</info>');
			$this->XProgressBar->start(1);

			$createdOrNot = $this->_apicalled("/collectivites", $this->params['file']);

			$this->hr();
			if (isset($createdOrNot->collectiviteId) && !empty($createdOrNot->collectiviteId)) {
				$this->out('<success>La collectivité ' . $createdOrNot->collectiviteId . ' a bien été créée.</success>');
			} else if( isset($createdOrNot) && !empty($createdOrNot) ) {
				$this->out('<error>La collectivité n\'a pas été créée pour le motif suivant " ' . $createdOrNot . ' "</error>');
			} else {
				$this->out('<warning>La collectivité est déjà présente.</warning>');
			}

			$this->hr();
		}
	}



	/**
	 *
	 * @return optionParser
	 */
	public function getOptionParser() {
		$actions = array(
			"apicalled" => "Appel de l'API"
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}
		$optionParser->addOption('file', array(
			'short' => 'f',
			'help' => 'Fichier au format json contenant les données de la nouvelle collectivité'
		));
		return $optionParser;
	}

	/**

	 * Script permettant la création d'une collectivité sur webgfc.
	 * Il prend en paramètre le chemin vers le fichier json contenant les informations
	 * attendue par l'API de création de collectivité
	 */
	/** fonction utilisant curl pour appeler une API webgfc

	 * Elle prend en paramètre l'URL de l'API à appeler, le token à mettre dans le header pour s'authentifier et le json attendue par l'API
	 * Elle retourne rien mais affiche en verbeux les échanges curl et le retour de l'API
	 **/
	function callAPI($url, $token, $data){
		$ch = curl_init();
		// Paramétrage des options curl
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, @$data );
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		curl_setopt($ch, CURLOPT_NOPROGRESS, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLINFO_SSL_VERIFYRESULT, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Authorization: '.$token,
			'Content-Type: application/json',
			'Cookie: WebGFCProd=2ql544gg3eqo9sg06ot7ajkckl',
		));
		$curl_return = curl_exec($ch);

		$messageRetour = '';
		if( isset(json_decode($curl_return)->message) ) {
			$messageRetour = json_decode($curl_return)->message;
		}

		if($messageRetour === 'Bad api token') {
			$this->responseMessageStr = $messageRetour;
		} else {
			$this->responseMessageStr = json_decode ( $curl_return );
		}

		curl_close($ch);

		return $this->responseMessageStr;
	}


	/**
	 * @param $jsonFile
	 */
	function _apicalled($action, $jsonFile) {
		$url = Configure::read('WEBGFC_URL').'/api/v1';
		$api = $url.$action;

		ClassRegistry::init(('User'));
		$this->User = new User();
		$this->User->setDataSource('default');
		$user = $this->User->find('first');
		$token = $user['User']['apitoken'];

		$data = file_get_contents($jsonFile);
		return $this->callAPI($api,$token,$data);
	}


}
