<?php

App::uses('Controller', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');

/**
 *
 * ContentToFile shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class OcrShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $repositoryTesseract;

	/**
	 *
	 * @var type
	 */
	public $repositoryTesseractOld;

	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $Document;

	/**
	 *
	 * @var type
	 */
	public $Courrier;

	public $headers = array(
		"Référence du flux",
		"Nom du document",
		"Document principal ?"
	);

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();
		$repo = null;
		$repoOld = null;
		$repoInvalid = null;
		$repoMerge = null;
		$repoSingle = null;

		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';
		$coll = $this->Collectivite->find('first', array('conditions' => array('Collectivite.conn' => $this->params['connection'])));


		Configure::write('conn', $this->params['connection']);

		ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->Document = ClassRegistry::init('Document');
		$this->Document->useDbConfig = $this->_conn;
		$this->Courrier = ClassRegistry::init('Courrier');
		$this->Courrier->useDbConfig = $this->_conn;


		$repo = $coll['Collectivite']['scan_local_path'];
		$repoMerge = $repo . DS . 'merge';
		$repoSingle = $repo . DS . 'single';
		$repoOld = $repo . DS . 'old';
		$repoInvalid = $repoOld . DS . 'invalid';

		// Pour OCérisation
		$repoTesseract = $repo . DS . 'tesseract';
		$repoTesseractOld = $repoTesseract . DS . 'old';
		$repoTesseractInvalid = $repoTesseract . DS . 'invalid';

		$this->_checkRepositoriesTesseract($repo, $repoTesseract, $repoTesseractOld, $repoTesseractInvalid);
	}


	/**
	 *
	 * @param type $repo
	 * @param type $repoTesseract
	 * @param type $repoTesseractOld
	 * @param type $repoTesseractInvalid
	 * @throws FatalErrorException
	 */
	private function _checkRepositoriesTesseract($repo, $repoTesseract, $repoTesseractOld, $repoTesseractInvalid) {
		//Initialisation des répertoires à surveiller
		$this->repository = new Folder($repo);
		if (!isset($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'existe pas.');
		}

		if (!is_readable($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repository->path)) {
			throw new FatalErrorException('Le répertoire ' . $repo . ' n\'est pas accessible en écriture.');
		}

		//Répertoire pour les PDFs convertis en PNGs
		$this->repositoryTesseract = new Folder($repoTesseract);
		if (!isset($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryTesseract->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseract . ' n\'est pas accessible en écriture.');
		}


		$this->repositoryTesseractOld = new Folder($repoTesseractOld);
		if (!isset($this->repositoryTesseractOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractOld . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryTesseractOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractOld . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryTesseractOld->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractOld . ' n\'est pas accessible en écriture.');
		}

		$this->repositoryTesseractInvalid = new Folder($repoTesseractInvalid);
		if (!isset($this->repositoryTesseractInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractInvalid . ' n\'existe pas.');
		}

		if (!is_readable($this->repositoryTesseractInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractInvalid . ' n\'est pas accessible en lecture.');
		}

		if (!is_writable($this->repositoryTesseractInvalid->path)) {
			throw new FatalErrorException('Le répertoire ' . $repoTesseractInvalid . ' n\'est pas accessible en écriture.');
		}
	}

	/**
	 *
	 */
	public function main() {

		$this->out('<info>Acquisition des documents en cours ...</info>');
		$this->XProgressBar->start(1);

		$success = $this->ocrFile();

		$this->hr();
		if ($success) {
			$this->out('<success>Opération terminée avec succès.</success>');
		} else {
			$this->out('<error>Opération terminée avec erreur(s).</error>');
		}
		$this->hr();
	}

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
		$this->Document = ClassRegistry::init('Document');

		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));


		return $optionParser;
	}

	/**
	 *
	 * @param type $filename
	 * @param type $marche_id
	 * @param type $foreign_key
	 * @return boolean
	 * sudo ./lib/Cake/Console/cake --app app Ocr -c webgfc_coll
	 */
	public function ocrFile() {
		$this->Document->useDbConfig = $this->_conn;
		$this->Courrier->useDbConfig = $this->_conn;

		$return = array();
		$query = array(
			'fields' => array(
				'Document.id',
				'Document.path',
				'Document.size',
				'Document.mime',
				'Document.ext',
				'Document.courrier_id',
				'Document.ocr_data',
				'Document.name',
				'Courrier.id',
				'Courrier.reference'
			),
			'joins' => array(
				$this->Document->join( 'Courrier', array('type' => 'INNER' ) )
			),
			'recursive' => -1,
			'order' => array(
				'Courrier.reference',
				'Document.id'
			),
			'conditions' => array(
				'Document.path IS NOT NULL',
				'Document.ocr_data IS NULL',
				'Document.mime' => array('application/pdf', 'PDF', 'pdf')
			)
		);
		$count = $this->Document->find('count', $query);
		$this->XProgressBar->start($count);
		$this->out('<info>Activation des documents...</info>');

		$query['limit'] = 100;
		$steps = ceil($count/$query['limit']);

		for($i = 0 ; $i < $steps ; $i++) {
			$query['offset'] = $i * $query['limit'];
			$documents = $this->Document->find('all', $query);

			$this->Document->begin();
			foreach( $documents as $document ) {
				if( empty( $document['Document']['ocr_data'] ) && file_exists($document['Document']['path'] ) ) {

					$courrier = $this->Courrier->find(
						'first',
						array(
							'conditions' => array('Courrier.id' => $document['Document']['courrier_id']),
							'contain' => false,
							'recursive' => -1
						)
					);

					$this->_convertPdfToPng($document['Document']['path'], $courrier);

					$folderPath= WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference'];
					$folderToLook = new Folder($folderPath);
					$txtFiles = $folderToLook->find( '.*\.txt', true);
					foreach ($txtFiles as $txtFile) {
						$dataFile = file_get_contents( WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference']  . DS . $txtFile);
						$this->Courrier->Document->updateAll(
							array('Document.ocr_data' => "'" . pg_escape_bytea($dataFile) . "'"),
							array(
								'Document.id' => $document['Document']['id'],
								'Document.courrier_id' => $document['Document']['courrier_id']
							)
						);

						$this->_deleteTesseractTxtFiles($txtFile, $courrier);
					}
				}
				$this->XProgressBar->next();
			}

			if( !in_array(false, $return, true) ) {
				$this->Document->commit();
			}
			else {
				$this->Document->rollback();
			}
		}
		$this->XProgressBar->finish();


		return !in_array(false, $return, true);
	}


	/**
	 * Conversion des PDFs en PNGs pour exploitation future
	 * @param type $file
	 * @return type
	 */
	private function _convertPdfToPng($pdfFile, $courrier) {
		$this->Document->useDbConfig = $this->_conn;

		$filename = 'test'; // On force le nom du fichier txt généré car cela ne peut marcher si le fichier comporte des caractères spéciaux
		$png = $this->repositoryTesseract->path. DS .$filename.".png";

		$pdfFile = "'".$pdfFile."'";
		$convert = "/usr/bin/gs -sDEVICE=png16m -dTextAlphaBits=4 -r300 -o $png $pdfFile";

		$filenamePath = WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference'] . DS . $filename;
		$ocerisation = "/usr/bin/tesseract $png $filenamePath";

		shell_exec(escapeshellcmd($convert));
		shell_exec(escapeshellcmd($ocerisation));

		// le fichier stocké en txt est enregistré comme PJ du flux
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$fileMimeType = $finfo->buffer($filename);

		$doc = array(
			'Document' => array(
				'name' => $filename.".txt",
				'size' => strlen($pdfFile),
				'mime' => $fileMimeType,
				'ext' => 'txt',
				'courrier_id' => $courrier['Courrier']['id'],
				'main_doc' => false,
				'path' => $filenamePath.".txt"
			)
		);

		$this->Document->setDataSource($this->_conn);
		$this->Document->create($doc);
		$this->Document->save();

		$this->_setAsTesseractOld($filename);
	}

	/**
	 *
	 * @param type $file
	 * @param type $invalid
	 * @return type
	 */
	private function _setAsTesseractOld($file) {
		$return = false;
		$tmpFile = new File($this->repositoryTesseract->path . DS . $file.".png");

		if ($tmpFile->copy($this->repositoryTesseractOld->path . DS . $file.".png" . '.webgfcold_' . date('Ymd'))) {
			$return = $tmpFile->delete();
		}

		return $return;
	}

	/**
	 *
	 * @param type $file
	 * @param type $invalid
	 * @return type
	 */
	private function _deleteTesseractTxtFiles($file, $courrier) {
		$this->Courrier->useDbConfig = $this->_conn;
		$txtFile = new File( WORKSPACE_PATH . DS . $this->_conn . DS . $courrier['Courrier']['reference'] . DS . $file);
		$txtFile->delete();

		$docId = $this->Courrier->Document->find(
			'first',
			array(
				'fields' => array('Document.id'),
				'conditions' => array(
					'Document.name' => $file
				),
				'contain' => false,
				'recursive' => -1
			)
		);
		$this->Courrier->Document->delete($docId['Document']['id']);
	}

}
