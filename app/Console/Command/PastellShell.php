<?php

App::import(array('Model', 'AppModel', 'File'));
App::import(array('Model', 'Courrier', 'File'));
App::import(array('Model', 'Comment', 'File'));
App::uses('Model', 'AppModel');
App::uses('ConnectionManager', 'Model');


App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

App::uses('AppShell', 'Console/Command');
App::uses('ComponentCollection', 'Controller');
App::uses('NewPastellComponent', 'Controller/Component');

App::uses('CakeflowAppModel', 'Cakeflow.Model');
App::uses('SimpleCommentAppModel', 'SimpleComment.Model');
/**
 * Class PastellShell
 */
class PastellShell extends Shell {

	/**
	 *
	 * @var type
	 */
	private $_conn;

	/**
	 *
	 * @var type
	 */
	public $Courrier;


	var $uses = array('Courrier', 'Comment');

	public function startup() {
		parent::startup();

		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		if ($this->_conn != 'default') {
			ClassRegistry::init('Courrier');
			$this->Courrier = new Courrier();
			$this->Courrier->setDataSource($this->_conn);

			$collection = new ComponentCollection();
			$this->Pastellwebservice = new NewPastellComponent($collection);

		}
	}

	function main() {
		$Courrier = ClassRegistry::init('Courrier');
		$Connecteur = ClassRegistry::init('Connecteur');
		$conditions = [];
		$conn = $this->_conn;
		if ($conn != 'default') {
			$Courrier->useDbConfig = $conn;

			$pastell = $Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell'=> true
					),
					'contain' => false
				)
			);

			$courriers = [];
			// Controle de l'avancement des flux dans le parapheur
			if( $pastell['Connecteur']['use_pastell'] ) {
				$conditions['pastell_id !='] = null;
				$conditions['signee'] = (null | false);
				$conditions['pastell_etat'] = 1;


				$courriers = $Courrier->find(
					'all',
					array(
						'conditions' => $conditions,
						'recursive' => -1,
						'contain' => false,
						'fields' => array('id', 'name', 'pastell_id')
					)
				);
			}

			foreach ($courriers as $courrier) {
				$this->_checkEtatPastell($courrier['Courrier']['id'], $pastell['Connecteur']['id_entity'], $courrier['Courrier']['name']);
			}
		}
	}

	function _checkEtatPastell($courrier_id, $id_e, $name) {

		$collection = new ComponentCollection();
		$this->Pastellwebservice = new NewPastellComponent($collection);

		$Courrier = ClassRegistry::init('Courrier');
		$Traitement = ClassRegistry::init('Cakeflow.Traitement');
		$Desktopmanager = ClassRegistry::init('Desktopmanager');

		$Connecteur = ClassRegistry::init('Connecteur');
		$Comment = ClassRegistry::init('Comment');
		$CommentReader = ClassRegistry::init('SimpleComment.SCCommentReader');

		$conn = $this->_conn;
		if ($conn != 'default') {
			$Courrier->useDbConfig = $conn;
			$Traitement->useDbConfig = $conn;
			$Desktopmanager->useDbConfig = $conn;
			$Comment->useDbConfig = $conn;
			$CommentReader->useDbConfig = $conn;
			$Courrier->id = $courrier_id;
			$courrier = $Courrier->find(
				'first',
				array(
					'conditions' => array(
						"Courrier.id" => $courrier_id,
						"Courrier.pastell_id IS NOT NULL",
						"Courrier.pastell_etat" => 1
					),
					'contain' => array(
						'Document',
						'Bancontenu' => array(
							'order' => array('Bancontenu.id DESC')
						) )
				)
			);

			$traitements = $Courrier->Traitement->find(
				'first',
				array(
					'conditions' => array(
						'Traitement.target_id' => $courrier_id
					),
					'contain' => false
				)
			);

			if( !empty($traitements) ) {
				if( !empty($courrier['Courrier']['pastell_id']) ) {

					// TEste
					if( $courrier['Bancontenu'][0]['desktop_id'] == -'3' ){
						$Courrier->Bancontenu->updateAll(
							array(
								'Bancontenu.pastell_id' => "'".$courrier['Courrier']['pastell_id']."'"
							),
							array(
								'Bancontenu.id' => $courrier['Bancontenu'][0]['id']
							)
						);
					}

					$forParapheur = false;
					if($courrier['Courrier']['parapheur_etat'] == 1) {
						$forParapheur = true;
					}
					// Déclenchement de la suite des traitements
					return $Courrier->majTraitementsPastell($courrier_id, $conn, $id_e, $forParapheur);
				}
			}
		}
		return false;
	}



	/**
	 *
	 * @return optionParser
	 */
	public function getOptionParser() {
		$actions = array(
			'repair_addressbook_slugs' => 'Recalcule les informations pour la recherche d\'homonymie des contacts.'
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}
		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));

		return $optionParser;
	}

}
