<?php
/**
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 *
 *
 */
App::uses('Model', 'AppModel');
App::uses('ConnectionManager', 'Model');


App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

App::uses('AppShell', 'Console/Command');
App::uses('ComponentCollection', 'Controller');
App::uses('LocaleoComponent', 'Controller/Component');

App::uses('CakeflowAppModel', 'Cakeflow.Model');
App::uses('SimpleCommentAppModel', 'SimpleComment.Model');

/**
 * Class LocaleoShell
 */
class LocaleoShell extends Shell {

    /**
	 *
	 * @var type
	 */
	private $_conn;


    public function startup() {
        parent::startup();


		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

        if ($this->_conn != 'default') {
			ClassRegistry::init('Courrier');
			$this->Courrier = new Courrier();
			$this->Courrier->setDataSource($this->_conn);

            $collection = new ComponentCollection();
			$this->Localeowebservice = new LocaleoComponent($collection);

        }
    }

    /**
     * Focniton principale exécutant les différentes actions disponibles
     */
    function main() {
        $this->_showHelp = false;

		list($usec, $sec) = explode(' ', microtime());
		$script_start = (float) $sec + (float) $usec;



        if( Configure::read('Webservice.GRC') ) {
			ClassRegistry::init('Connecteur');
            $this->Connecteur = new Connecteur();
            $this->Connecteur->setDataSource($this->_conn);
            $hasGrcActif = $this->Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%GRC%',
                        'Connecteur.use_grc' => true
                    ),
                    'contain' => false
                )
            );
            if($hasGrcActif) {
                if (!empty($this->args[0])) {
                    if ($this->args[0] == "create") {
                        $this->_createGRCCourrier();
                    } else if ($this->args[0] == "update") {
                        $this->_updateGRCCourrier();
                    } else if ($this->args[0] == "check") {
                        $this->_checkTest();
                    } else if ($this->args[0] == "createagain") {
                        $this->_createGRCCourrierAgain();
                    }
                    else {
                        $this->_showHelp = true;
                    }
                }
                else {
                    $this->_showHelp = true;
                }
            }
            else {
                $this->out(__( "Le connecteur GRC Localeo n'est pas actif. Activez-le pour pouvoir utiliser cette fonctionnalité." ) );
            }
        }
        else {
            $this->out(__( 'Le paramètre Webservice.GRC retourne false. Aucun connecteur GRC disponible.' ) );
        }


        if ($this->_showHelp) {
			$this->_displayHelp('');
		} else {
			list($usec, $sec) = explode(' ', microtime());
			$script_end = (float) $sec + (float) $usec;

			$elapsed_time = round($script_end - $script_start, 5);
			$this->out('Elapsed time : ' . $elapsed_time . 's');
			$this->hr();
		}
    }

    /**
     * Fonction permettant de savoir la connexion avec la GRC est opérationnelle
     *
        @params : sudo ./lib/Cake/Console/cake --app app Localeo -c databasename  check
        @return : si un flux existe côté GRC, alors on le met à jour côté GRC
        @return : sinon, on ne fait rien
     */
    private function _checkTest() {
        $test = $this->Localeowebservice->echoTest();
        $this->out(__( 'La GRC retourne : ' . $test->statut->label ));
    }

	/**
	 *
	 * @return optionParser
	 */
	public function getOptionParser() {

        $actions = array(
			'create' => 'Création des requêtes dans la GRC depuis web-GFC',
			'update' => 'Mise à jour du statut des flux de web-GFC dans la GRC'
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}

		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));

		return $optionParser;
	}

    /**
     * Fonction permettant de créer les requêtes dans la GRC pour les flux non encore transmis à la GRC
     * Ce sont les flux ne possédant aucun courriergrc_id dans web-GFC et donc aucune entrée dans la GRC
     * mais dont la connexin avec la GRC est active
     *
     * @info: Si le système se voit bloqué entre web-GFC et la GRC, le cron s'exécutera et pourra créer les requêtes manquantes dans la GRC
     *
        @params : sudo ./lib/Cake/Console/cake --app app Localeo -c databasename create
        @return : si un flux n'existe pas côté GRC, alors on le crée
        @return : sinon, on ne fait rien
     */
    private function _createGRCCourrier() {
        $courriers = $this->Courrier->find(
            'all',
            array(
                'conditions' => array(
                    'Courrier.courriergrc_id IS NULL',
//                    'Courrier.created >=' => '2019-04-01 00:00:00'
                    'Courrier.created >=' => '2019-03-29 00:00:00'
                ),
                'contain' => false
            )
        );
        $success = array();
        if( !empty($courriers) ) {
            foreach( $courriers as $courrier ) {
                $courriergrcId = $courrier['Courrier']['courriergrc_id'];
                if( empty($courriergrcId) ) {
                    $retour = $this->Localeowebservice->getStatusGRC( $courriergrcId );
                    if( isset( $retour->err_msg ) && !empty($retour->err_msg)) {
                        if($retour->err_msg == "La demande n'existe pas") {
                            $success = $this->Localeowebservice->createRequestGRC( $courrier );
                            if( !empty($success->id) ) {
                                $this->Courrier->updateAll(
                                    array(
                                        'Courrier.courriergrc_id' => $success->id,
                                        'Courrier.statutgrc' => '2'
                                    ),
                                    array('Courrier.id' => $courrier['Courrier']['id'])
                                );
                            }
                        }
                    }
                    else {
                        $this->out(__( ' ==== Aucun flux à créer. La requête existe avec le statut : '.$retour->statut->label .' ==== '));
                    }
                }
            }
        }
        else {
            $this->out(__( ' ==== Aucun flux à créer côté GRC ==== '));
        }
        return $success;
    }

    /**
     * Fonction permettant de mettre à jour les états des requêtes dans la GRC
     *
     * @info: Si le système se voit bloqué entre web-GFC et la GRC, le cron s'exécutera et pourra mettre à jour les statuts dans la GRC
     *
        @params : sudo ./lib/Cake/Console/cake --app app Localeo -c databasename  update
        @return : si un flux existe côté GRC, alors on le met à jour côté GRC
        @return : sinon, on ne fait rien
     */
    private function _updateGRCCourrier() {
        $courriers = $this->Courrier->find(
            'all',
            array(
                'conditions' => array(
                    'Courrier.courriergrc_id IS NOT NULL',
                    'OR' => array(
                        'Courrier.statutgrc <>' => array(50, 29, 7, 6, 5),
                        'Courrier.statutgrc IS NULL'
                    )
                ),
                'contain' => false
            )
        );
        $success = array();
        foreach( $courriers as $courrier ) {
            $courrierId = $courrier['Courrier']['id'];
            $reference = $courrier['Courrier']['reference'];
            $courriergrcId = $courrier['Courrier']['courriergrc_id'];
            if( !empty($courriergrcId) ) {
                $retour = $this->Localeowebservice->getStatusGRC( $courriergrcId );
                if( isset($retour->statut) ) {
                    $statutId = $retour->statut->id;
                    $labelGRC = $retour->statut->label;
                    if( in_array($statutId, array('5', '6', '50'))) {
                        $note = 'Le flux est au statut " '. $labelGRC .' " dans la GRC';

                        $etats= array(
                            'refus' => $this->Courrier->isRefused($courrierId),
                            'traitement' => $this->Courrier->isTreated($courrierId),
                            'clos' => $this->Courrier->isClosed($courrierId)
                        );
                        foreach( $etats as $etat => $value ) {
                            if( $value == 1 ) {
                                if( $etat == 'refus' ) {
                                    $note = 'Le flux a été refusé dans web-GFC';
                                    $statutId = '6';
                                }
                                else if( $etat == 'traitement' ) {
                                    $note = 'Le flux a été traité dans web-GFC';
                                    $statutId = '5';
                                }
                                else if( $etat == 'clos' ) {
                                    $note = 'Le flux a été clos dans web-GFC';
                                    $statutId = '50';
                                }
                            }
                        }
                        $update = $this->Localeowebservice->updateStatusGRC( $courriergrcId,  $statutId, $note );
                        if( empty($update->err_msg) ) {
                            $statutsGrc = array(
                                '1' => 'Lu',
                                '2' => 'Non lu',
                                '5' => 'Satisfait',
                                '6' => 'Non satisfait',
                                '7' => 'Sans suite',
                                '20' => 'En cours de traitement',
                                '29' => 'Refusé',
                                '50' => 'Traité'
                            );
                            $return = $statutsGrc[$retour->statut->id];
                            $this->out(__( 'Requête ' . $courriergrcId . ' ( N° référence : '.$reference.' ) , La GRC retourne comme statut : ' . $return ));
                        }
                        else {
                            $return = $update->err_msg;
                            $this->out(__( 'Requête ' . $courriergrcId . ' ( N° référence : '.$reference.' ) , La GRC retourne : ' . $return ));
                        }
                    }
                }
                else {
                    $return = $retour->err_msg;
                    $this->out(__( 'Requête ' . $courriergrcId . ' ( N° référence : '.$reference.' ) , La GRC retourne : ' . $return ));
                }
            }
        }
        return $success;
    }

    /**
     * Fonction permettant de créer les requêtes dans la GRC pour les flux
     * possédant un courriergrc_id dans web-GFC mais aucune entrée dans la GRC
     *
     * @info: Si le système se voit bloqué entre web-GFC et la GRC, le cron s'exécutera et pourra créer les requêtes manquantes dans la GRC
     *
        @params : sudo ./lib/Cake/Console/cake --app app Localeo -c databasename create
        @return : si un flux n'exsite pas côté GRC, alors on le crée
        @return : sinon, on ne fait rien
     */
    private function _createGRCCourrierAgain() {
        $courriers = $this->Courrier->find(
            'all',
            array(
                'conditions' => array(
                    'Courrier.courriergrc_id IS NOT NULL',
//                    'Courrier.created >=' => '2019-04-01 00:00:00'
                ),
                'contain' => false
            )
        );
        $success = array();
        if( !empty($courriers) ) {
            foreach( $courriers as $courrier ) {
                $courriergrcId = $courrier['Courrier']['courriergrc_id'];
                if( !empty($courriergrcId) ) {
                    $retour = $this->Localeowebservice->getStatusGRC( $courriergrcId );
                    if( isset( $retour->err_msg ) && !empty($retour->err_msg)) {
                        if($retour->err_msg == "La demande n'existe pas") {
                            $success = $this->Localeowebservice->createRequestGRC( $courrier );
                            if( !empty($success->id) ) {
                                $this->Courrier->updateAll(
                                    array(
                                        'Courrier.courriergrc_id' => $success->id,
                                        'Courrier.statutgrc' => '2'
                                    ),
                                    array('Courrier.id' => $courrier['Courrier']['id'])
                                );
                            }
                        }
                    }
                    else {
                        $this->out(__( ' ==== Aucun flux à créer. La requête existe avec le statut : '.$retour->statut->label .' ==== '));
                    }
                }
            }
        }
        return $success;
    }


}
