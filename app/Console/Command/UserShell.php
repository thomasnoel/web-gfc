<?php

App::uses('Controller', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('XShell', 'Console/Command');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('CakeTime', 'Utility');
App::uses('AuthComponent', 'Controller/Component');
App::uses('Model', 'AppModel');
App::uses('DbAcl', 'Model');
App::uses('AclNode', 'Model');
App::uses('Aco', 'Model');
App::uses('Aro', 'Model');
/**
 *
 * CreateFromFiles shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Arnaud AUZOLAT
 * @copyright Initié par ADULLACT - Développé par Libriciel SCOP
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class UserShell extends XShell {


	/**
	 *
	 * @var type
	 */
	public $files = array();


	/**
	 *
	 * @var type
	 */
	public $Collectivite;

	/**
	 *
	 * @var type
	 */
	public $User;

	/**
	 *
	 * @var type
	 */
	public $Desktop;

	/**
	 *
	 * @throws FatalErrorException
	 */
	public function startup() {
		parent::startup();

        $this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		$this->Collectivite = ClassRegistry::init('Collectivite');
		$this->Collectivite->useDbConfig = 'default';

		Configure::write('conn', $this->params['connection']);

        ClassRegistry::config(array( 'ds' => $this->params['connection']));
		$this->User = ClassRegistry::init('User');
        $this->User->useDbConfig = $this->_conn;

	}


	/**
	 *
	 */
	public function main() {

        if( !empty( $this->params['file']) ){
            $createdUser = array();
            $this->out('<info>Acquisition des utilisateurs en cours ...</info>');
            $this->XProgressBar->start(1);

            $this->importUserCsv($this->params['file']);

            $this->hr();
            if (!in_array(false, $createdUser, true)) {
                $this->out('<success>Opération terminée avec succès.</success>');
            } else {
                $this->out('<error>Opération terminée avec erreur(s).</error>');
            }
            $this->hr();
        }
    }

	/**
	 *
	 * @return type
	 */
	public function getOptionParser() {
        $this->User = ClassRegistry::init('User');
        $this->Desktop = ClassRegistry::init('Desktop');

        $optionParser = parent::getOptionParser();
        $optionParser->description(__("Outils d'administration"));
        $optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));
        $optionParser->addOption('file', array(
			'short' => 'f',
            'help' => 'Nom du fichier User (format CSV) à intégrer'
		));


		return $optionParser;
	}



    /*
     * Fonction permettant d'intégrer le fichier BAN d'un département
     *
     *
     */

    private function _processCsv($user) {

        $users = array(
            'nom' => trim( $user['Nom'] ),
            'prenom' => trim( $user['Prénom'] ),
            'numtel' => trim( $user['Téléphone'] ),
            'mail' => trim( $user['Mail'] ),
            'role' => trim( $user['Profil'] ),
            'service' => trim( $user['Service'] ),
            'active' => 1
        );
//debug($user);
//die();
        $res = array(
            'users' => $users
        );
        return $res;
    }


    /**
     *
     * @param type $filename
     * @param type $user_id
     * @param type $foreign_key
     * @return boolean
     */
    public function importUserCsv($filename = null) {

//ini_set( 'memory_limit', '3000M');
        $return = array();
        if ($filename != null && is_file($filename) && is_readable($filename)) {
            if (($handle = fopen($filename, 'r')) !== FALSE) {

                $users = array();
                $header = NULL;

                while (($row = fgetcsv($handle, 1000000, ';')) !== FALSE) {
                    if (!$header) {
                        $header = $row;
                    } else {
                        $tmp = array();
                        for ($i = 0; $i < count($header); $i++) {
                            $tmp[$header[$i]] = $row[$i];
                        }
                        $users[] = $tmp;
                    }
                }
                fclose($handle);

//debug($users);
                $this->XProgressBar->start(count($users));
                $this->out('<info>Enregistrement des utilisateurs...</info>');
                $saved = true;
                foreach ($users as $user) {
                    $userINFO = $this->_processCsv($user);
                    // ensuite on sauvegarde le utilisateur
                    if( !empty($userINFO['users']) ) {
                        $this->User->begin();
                        $userValue = $userINFO['users'];
                        $userAlreadyExist = $this->User->find(
                            'first',
                            array(
                                'conditions' => array(
                                    'User.nom' => $userValue['nom'],
                                    'User.prenom' => $userValue['prenom'],
                                    'User.mail' => $userValue['mail']
                                ),
                                'recursive' => -1
                            )
                        );
                        if( empty( $userAlreadyExist ) ) {
                            if( $saved ) {
                                // Profil
                                Configure::write('Acl.database', $this->_conn);
                                Configure::write('DataAcl.database', $this->_conn);
                                $this->loadModel('Desktop');
                                $this->loadModel('Aro');
                                $this->loadModel('Service');
                                $this->loadModel('DesktopsService');
                                $this->loadModel('Desktopmanager');
                                $this->loadModel('DesktopDesktopmanager');
                                $this->Desktop->setDataSource($this->_conn);
                                $this->Aro->setDataSource($this->_conn);
                                $this->Service->setDataSource($this->_conn);
                                $this->DesktopsService->setDataSource($this->_conn);
                                $this->Desktopmanager->setDataSource($this->_conn);
                                $this->DesktopDesktopmanager->setDataSource($this->_conn);

                                $this->Desktop->create();
                                if($userINFO['users']['role'] == 3) {
                                    $desktopName = 'Initiateur';
                                }
                                else if($userINFO['users']['role'] == 4) {
                                    $desktopName = 'Valideur';
                                }
                                else if($userINFO['users']['role'] == 5) {
                                    $desktopName = 'Valideur Editeur';
                                }
                                else if( $userINFO['users']['role'] == 6) {
                                    $desktopName = 'Documentaliste';
                                }
                                else if( $userINFO['users']['role'] == 7) {
                                    $desktopName = 'Aiguilleur';
                                }
                                $desktopData['Desktop']['name'] = $desktopName.' '.$userINFO['users']['prenom'].' '.$userINFO['users']['nom'];
                                $desktopData['Desktop']['profil_id'] = $userINFO['users']['role'];
                                $saved = $this->Desktop->save($desktopData) && $saved;
                                $desktopId = $this->Desktop->id;


                                $this->Desktopmanager->create();
                                $desktopmanagerData['Desktopmanager']['name'] = 'Bureau '.$desktopData['Desktop']['name'];
                                $desktopmanagerData['Desktopmanager']['isautocreated'] = true;
                                $saved = $this->Desktopmanager->save($desktopmanagerData) && $saved;
                                $desktopmanagerId = $this->Desktopmanager->id;

                                $this->DesktopDesktopmanager->create();
                                $desktopDesktopmanagerData['DesktopDesktopmanager']['desktop_id'] = $desktopId;
                                $desktopDesktopmanagerData['DesktopDesktopmanager']['desktopmanager_id'] = $desktopmanagerId;
                                $saved = $this->DesktopDesktopmanager->save($desktopDesktopmanagerData) && $saved;
                                $desktopDesktopmanagerId = $this->DesktopDesktopmanager->id;

                                // Partie Service
                                $serviceData['Service']['name'] = $userINFO['users']['service'];
                                $serviceAlreadyExist = $this->Service->find(
                                    'first',
                                    array(
                                        'conditions' => array(
                                            'Service.name' => $userINFO['users']['service']
                                        ),
                                        'recursive' => -1
                                    )
                                );
                                $serviceDATA = array();
                                if( empty( $serviceAlreadyExist ) ) {
                                    $this->Service->create();
                                    $serviceDATA['Service']['name'] = $userINFO['users']['service'];
                                    $saved = $this->Service->save($serviceDATA) && $saved;
                                    $serviceId = $this->Service->id;
                                }
                                else {
                                    $serviceId = $serviceAlreadyExist['Service']['id'];
                                }

                                // desktopsService
                                $this->DesktopsService->create();
                                $desktopsserviceDATA['DesktopsService']['desktop_id'] = $desktopId;
                                $desktopsserviceDATA['DesktopsService']['service_id'] = $serviceId;
                                $saved = $this->DesktopsService->save($desktopsserviceDATA) && $saved;

                                // User
                                $this->User->create();
                                $username = replace_accents( mb_substr( mb_strtolower( $userINFO['users']['prenom'] ), 0, 1 ).''.mb_strtolower( trim( $userINFO['users']['nom'] ) ) );

                                $userDATA['User'] = $userINFO['users'];
                                $userDATA['User']['username'] = $username;
                                $userDATA['User']['password'] = $username;
                                $userDATA['User']['desktop_id'] = $desktopId;
                                $saved = $this->User->save($userDATA) && $saved;

                            }
                        }

                        // on récupère les valeurs des événements et on les stocke par utilisateur
                    }

                    if ($saved) {
                        $return[] = true;
                        $this->User->commit();

                    } else {
                        $return[] = false;
                        $this->User->rollback();
                    }
                    $this->XProgressBar->next();
                }
                $this->XProgressBar->finish();
            }
        }
        return !in_array(false, $return, true);
    }


}
