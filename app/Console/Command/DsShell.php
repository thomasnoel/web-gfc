<?php
/**
 * @author Arnaud AUZOLAT <arnaud.auzolat@libriciel.coop>
 * @editor Libriciel SCOP
 *
 * @created 19 sept 2018
 *
 *
 */
App::uses('Model', 'AppModel');
App::uses('ConnectionManager', 'Model');


App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

App::uses('AppShell', 'Console/Command');
App::uses('ComponentCollection', 'Controller');
App::uses('DsComponent', 'Controller/Component');

/**
 * Class DsShell
 */
class DsShell extends Shell {

	/**
	 *
	 * @var type
	 */
	private $_conn;


	public function startup() {
		parent::startup();


		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

		if ($this->_conn != 'default') {
			ClassRegistry::init('Courrier');
			$this->Courrier = new Courrier();
			$this->Courrier->setDataSource($this->_conn);

			ClassRegistry::init('Connecteur');
			$this->Connecteur = new Connecteur();
			$this->Connecteur->setDataSource($this->_conn);

			$collection = new ComponentCollection();
			$this->Dswebservice = new DsComponent($collection);

		}
	}

	/**
	 * Focniton principale exécutant les différentes actions disponibles
	 */
	function main() {
		$this->_showHelp = false;

		list($usec, $sec) = explode(' ', microtime());
		$script_start = (float) $sec + (float) $usec;

		ClassRegistry::init('Connecteur');
		$this->Connecteur = new Connecteur();
		$this->Connecteur->setDataSource($this->_conn);
		$hasDsActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Démarches%',
					'Connecteur.use_ds' => true
				),
				'contain' => false
			)
		);
		if($hasDsActif) {
			if (!empty($this->args[0])) {
				if ($this->args[0] == "check") {
					$this->_checkTest();
				}
			}
			else {
				$this->_showHelp = true;
			}
		}
		else {
			$this->out(__( "Le connecteur GRC Localeo n'est pas actif. Activez-le pour pouvoir utiliser cette fonctionnalité." ) );
		}


		if ($this->_showHelp) {
			$this->_displayHelp('');
		} else {
			list($usec, $sec) = explode(' ', microtime());
			$script_end = (float) $sec + (float) $usec;

			$elapsed_time = round($script_end - $script_start, 5);
			$this->out('Elapsed time : ' . $elapsed_time . 's');
			$this->hr();
		}
	}

	/**
	 * Fonction permettant de savoir la connexion avec la GRC est opérationnelle
	 *
	@params : sudo ./lib/Cake/Console/cake --app app Ds -c databasename  check
	@return : si un flux existe côté Démarches-Simplifiées, alors on retourne OK
	@return : sinon, on retourne un message d'erreur
	 */
	private function _checkTest() {
		$test = $this->Dswebservice->echoTest();
		return $test;
	}


	/**
	 *
	 * @return optionParser
	 */
	public function getOptionParser() {

		$actions = array(
			'create' => 'Création des requêtes dans la GRC depuis web-GFC',
			'update' => 'Mise à jour du statut des flux de web-GFC dans la GRC'
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}

		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));

		return $optionParser;
	}

}
