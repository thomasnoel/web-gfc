<?php

App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');
App::uses('Model', 'AppModel');
App::uses('ComponentCollection', 'Controller');
App::uses('AclComponent', 'Controller/Component');
App::uses('AuthComponent', 'Controller/Component');
App::uses('DbAcl', 'Model');

/**
 *
 * DbTools shell class
 *
 * web-GFC : Gestion de Flux Citoyens (https://adullact.net/projects/webgfc)
 *
 * PHP version 7
 * @author Stéphane Sampaio
 * @copyright Initié par ADULLACT - Développé par ADULLACT Projet
 * @link http://adullact.org/
 * @license https://choosealicense.com/licenses/agpl-3.0/ AGPL v3
 *
 *
 * @package		cake.app
 * @subpackage	cake.app.shell
 */
class DbToolsShell extends Shell {

	/**
	 * Contains database source to use
	 *
	 * @var string
	 * @access public
	 */
	public $uses = array('Desktop', 'Profil');

	/**
	 * Start up And load Acl Component / Aco model
	 *
	 * @return void
	 * */
	public function startup() {
		parent::startup();
		$collection = new ComponentCollection();
		$this->Acl = new AclComponent($collection);
		$this->Acl->startup(new AppController());
		$this->Aco = $this->Acl->Aco;
		$this->Aro = $this->Acl->Aro;
	}

	/**
	 * Delete ACL Table's content (MySQL: autoincrement reseted, PostgreSQL: serial not reseted)
	 *
	 * @return void
	 * */
	public function delete_db() {
		$this->Aro->query("TRUNCATE table acos CASCADE;");
		$this->Aro->query("ALTER SEQUENCE acos_id_seq RESTART WITH 1;");
		$this->Aro->query("TRUNCATE table rights CASCADE;");
		$this->Aro->query("ALTER SEQUENCE rights_id_seq RESTART WITH 1;");
		$this->Aro->query("TRUNCATE table aros CASCADE;");
		$this->Aro->query("ALTER SEQUENCE aros_id_seq RESTART WITH 1;");
		$this->Aro->query("TRUNCATE table aros_acos CASCADE;");
		$this->Aro->query("ALTER SEQUENCE aros_acos_id_seq RESTART WITH 1;");
	}

	/**
	 *  Check if aro is not set
	 *
	 * @param array $aro Aro to check
	 * @return boolean
	 * */
	private function _aro_not_set($aro = null) {
		$retval = false;
		if ($aro != null) {
			if (!empty($aro['Aro']['model']) && !empty($aro['Aro']['foreign_key'])) {
				$conditions = array(
					'model' => $aro['Aro']['model'],
					'foreign_key' => $aro['Aro']['foreign_key']
				);
				if (!empty($aro['Aro']['alias'])) {
					$conditions['alias'] = $aro['Aro']['alias'];
				}
				if (!empty($aro['Aro']['parent_id'])) {
					$conditions['parent_id'] = $aro['Aro']['parent_id'];
				}
				$exists = $this->Aro->find('list', array('conditions' => $conditions));
				if (count($exists) == 0) {
					$retval = true;
				}
			}
		}
		return $retval;
	}

	/**
	 *  Create an Aro
	 *
	 * @param array $aro Aro to create
	 * @return boolean
	 * */
	private function _aro_create($aro = null) {
		$retval = false;
		if ($aro != null) {
			if ($this->_aro_not_set($aro)) {
				$this->Aro->create($aro);
				if ($this->Aro->save()) {
					$this->out($aro['Aro']['model'] . ' ' . $aro['Aro']['alias'] . ' -> <success>ok</success>');
					$retval = true;
				} else {
					$this->out($aro['Aro']['model'] . ' ' . $aro['Aro']['alias'] . ' -> <error>error</error>');
				}
			}
		}
		return $retval;
	}

	/**
	 *  Synchronisation between Aro table and Profil table (here: Role)
	 *
	 * @return boolean
	 * */
	private function _aro_sync_group() {
		$retval = true;
		$groups = $this->Profil->find('list');
		foreach ($groups as $key => $group) {
			$aro = array('Aro' => array());
			$aro['Aro']['model'] = 'Profil';
			$aro['Aro']['foreign_key'] = $key;
			$aro['Aro']['alias'] = $group;
			$this->_aro_create($aro);
		}
		return $retval;
	}

	/**
	 *  Synchronisation between Aro table and User table (here: Utilisateur)
	 *
	 * @return boolean
	 * */
	private function _aro_sync_desktop() {
		$retval = true;
		$desktops = $this->Desktop->find('all');
		foreach ($desktops as $desktop) {
			$aro = array('Aro' => array());
			$aro['Aro']['model'] = 'Desktop';
			$aro['Aro']['foreign_key'] = $desktop['Desktop']['id'];
			$aro['Aro']['alias'] = $desktop['Desktop']['name'];
			$aroParent = $this->Aro->find('first', array('conditions' => array('model' => 'Profil', 'foreign_key' => $desktop['Desktop']['profil_id'])));
			$aro['Aro']['parent_id'] = $aroParent['Aro']['id'];
			$this->_aro_create($aro);
		}
		return $retval;
	}

	/**
	 *  Synchronisation between Aro table and User/Profil tables
	 *
	 * @return void
	 * */
	public function aro_sync() {
		$this->_aro_sync_group();
//    $this->_aro_sync_user();
		$this->_aro_sync_desktop();
	}

	/**
	 * ACL Rights initialisation
	 *
	 * @return void
	 * */
	public function init_rights() {
		$this->out('Init function rights starting ...');
		$this->initArosAcos();
		$this->out('Init function rights done.');
		$this->out(__('<success>Init rights: ok</success>'));
	}

	/**
	 *
	 */
	private function initArosAcos() {
		$access_rights = array(
			"Superadmin" => array(),
			"Admin" => array(),
			"Valideur Editeur" => array(),
			"Initiateur" => array(),
			"Service Courrier" => array(),
			"Valideur" => array()
		);

		$this->out("***** initArosAcos *****");

		/*
		 * Superadmin
		 */

		$access_rights["Superadmin"][] = 'controllers';

		/*
		 * Administrateur
		 */

		$access_rights["Admin"][] = 'controllers/Checks';
		$access_rights["Admin"][] = 'controllers/Environnement';
		$access_rights["Admin"][] = 'controllers/Collectivites';
		$access_rights["Admin"][] = 'controllers/Workflows';
		$access_rights["Admin"][] = 'controllers/Profils';
		$access_rights["Admin"][] = 'controllers/Users';
		$access_rights["Admin"][] = 'controllers/Courriers';
		$access_rights["Admin"][] = 'controllers/Documents';
		$access_rights["Admin"][] = 'controllers/Armodels';
		$access_rights["Admin"][] = 'controllers/Rmodels';
		$access_rights["Admin"][] = 'controllers/Relements';
		$access_rights["Admin"][] = 'controllers/Ars';
		$access_rights["Admin"][] = 'controllers/Services';
		$access_rights["Admin"][] = 'controllers/Soustypes';
		$access_rights["Admin"][] = 'controllers/Types';
		$access_rights["Admin"][] = 'controllers/Cakeflow';
		$access_rights["Admin"][] = 'controllers/Taches';
		$access_rights["Admin"][] = 'controllers/Metadonnees';
		$access_rights["Admin"][] = 'controllers/Dossiers';
		$access_rights["Admin"][] = 'controllers/Affaires';
		$access_rights["Admin"][] = 'controllers/Recherches';
		$access_rights["Admin"][] = 'controllers/Keywords';
		$access_rights["Admin"][] = 'controllers/Keywordlists';
		$access_rights["Admin"][] = 'controllers/Keywordtypes';
		$access_rights["Admin"][] = 'controllers/Contacts';
		$access_rights["Admin"][] = 'controllers/Contactinfos';
		$access_rights["Admin"][] = 'controllers/Addressbooks';
		$access_rights["Admin"][] = 'controllers/Desktops';

		/*
		 * Initiateur
		 */

		//environnement
		$access_rights["Initiateur"][] = 'controllers/Environnement/index';
		$access_rights["Initiateur"][] = 'controllers/Environnement/historique';
		$access_rights["Initiateur"][] = 'controllers/Courriers/historiqueGlobal';
		$access_rights["Initiateur"][] = 'controllers/Environnement/userenv';
		$access_rights["Initiateur"][] = 'controllers/Environnement/getNotifications';
		$access_rights["Initiateur"][] = 'controllers/Environnement/setNotificationRead';
		$access_rights["Initiateur"][] = 'controllers/Environnement/setAllNotificationRead';
		$access_rights["Initiateur"][] = 'controllers/Repertoires';
		$access_rights["Initiateur"][] = 'controllers/Recherches';
		$access_rights["Initiateur"][] = 'controllers/Taches';
		$access_rights["Initiateur"][] = 'controllers/Desktops/setDeleg';

		//utilisateur
		$access_rights["Initiateur"][] = 'controllers/Users/editinfo';
		$access_rights["Initiateur"][] = 'controllers/Users/changemdp';
		$access_rights["Initiateur"][] = 'controllers/Users/changenotif';
		$access_rights["Initiateur"][] = 'controllers/Users/setInfosPersos';
		$access_rights["Initiateur"][] = 'controllers/Users/getInfosCompte';
		$access_rights["Initiateur"][] = 'controllers/Users/ajaxformvalid';
		$access_rights["Initiateur"][] = 'controllers/Users/isAway';

		//actions courrier
		$access_rights["Initiateur"][] = 'controllers/Courriers/add';
		$access_rights["Initiateur"][] = 'controllers/Courriers/edit';
		$access_rights["Initiateur"][] = 'controllers/Courriers/view';
		$access_rights["Initiateur"][] = 'controllers/Courriers/delete';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/dispsend';
		//tools courrier
		$access_rights["Initiateur"][] = 'controllers/Courriers/getListSoustypes';


		//tools circuit
		$access_rights["Initiateur"][] = 'controllers/Courriers/getFlowControls';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getCircuit';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/valid';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/send';
		$access_rights["Initiateur"][] = 'controllers/Courriers/insert';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getInsertEnd';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/refus';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/reponse';
		//tabs view courrier
		$access_rights["Initiateur"][] = 'controllers/Courriers/getInfos';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getQualification';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getTache';

//    $access_rights["Initiateur"][] = 'controllers/Courriers/getMeta';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/getAffaire';
		//tabs create courrier
		$access_rights["Initiateur"][] = 'controllers/Courriers/createInfos';

		//tabs edit courrier
		$access_rights["Initiateur"][] = 'controllers/Courriers/setInfos';
		$access_rights["Initiateur"][] = 'controllers/Contacts/add';
		$access_rights["Initiateur"][] = 'controllers/Contacts/search';
		$access_rights["Initiateur"][] = 'controllers/Contacts/searchContactinfo';
		$access_rights["Initiateur"][] = 'controllers/Contactinfos/add';
		$access_rights["Initiateur"][] = 'controllers/Courriers/setQualification';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getContacts';
		$access_rights["Initiateur"][] = 'controllers/Soustypes/getRefSimul';
//		$access_rights["Initiateur"][] = 'controllers/Courriers/getContext';
		$access_rights["Initiateur"][] = 'controllers/Documents';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getDocuments';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getArs';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getRelements';
		$access_rights["Initiateur"][] = 'controllers/Relements';
		$access_rights["Initiateur"][] = 'controllers/Rmodels/getPreview';
		$access_rights["Initiateur"][] = 'controllers/Armodels/getPreview';
		$access_rights["Initiateur"][] = 'controllers/Ars/getPreview';
		$access_rights["Initiateur"][] = 'controllers/Rmodels/download';
		$access_rights["Initiateur"][] = 'controllers/Courriers/setArs';
		$access_rights["Initiateur"][] = 'controllers/Courriers/genResponse';
		$access_rights["Initiateur"][] = 'controllers/Ars';
		$access_rights["Initiateur"][] = 'controllers/Armodels/download';
		$access_rights["Initiateur"][] = 'controllers/Courriers/getParent';
		$access_rights["Initiateur"][] = 'controllers/Courriers/setTache';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/setMeta';
//    $access_rights["Initiateur"][] = 'controllers/Courriers/setAffaire';

		/*
		 * Valideur
		 */

		//environnement
		$access_rights["Valideur"][] = 'controllers/Environnement/index';
		$access_rights["Valideur"][] = 'controllers/Environnement/historique';
		$access_rights["Valideur"][] = 'controllers/Courriers/historiqueGlobal';
		$access_rights["Valideur"][] = 'controllers/Environnement/userenv';
		$access_rights["Valideur"][] = 'controllers/Environnement/getNotifications';
		$access_rights["Valideur"][] = 'controllers/Environnement/setNotificationRead';
		$access_rights["Valideur"][] = 'controllers/Environnement/setAllNotificationRead';
		$access_rights["Valideur"][] = 'controllers/Repertoires';
		$access_rights["Valideur"][] = 'controllers/Recherches';
		$access_rights["Valideur"][] = 'controllers/Taches';
		$access_rights["Valideur"][] = 'controllers/Desktops/setDeleg';

		//utilisateur
		$access_rights["Valideur"][] = 'controllers/Users/editinfo';
		$access_rights["Valideur"][] = 'controllers/Users/changemdp';
		$access_rights["Valideur"][] = 'controllers/Users/changenotif';
		$access_rights["Valideur"][] = 'controllers/Users/setInfosPersos';
		$access_rights["Valideur"][] = 'controllers/Users/getInfosCompte';
		$access_rights["Valideur"][] = 'controllers/Users/ajaxformvalid';
		$access_rights["Valideur"][] = 'controllers/Users/isAway';

		//actions courrier
//    $access_rights["Valideur"][] = 'controllers/Courriers/add';
		$access_rights["Valideur"][] = 'controllers/Courriers/edit';
		$access_rights["Valideur"][] = 'controllers/Courriers/view';
//    $access_rights["Valideur"][] = 'controllers/Courriers/delete';
//    $access_rights["Valideur"][] = 'controllers/Courriers/dispsend';
		//tools courrier

		$access_rights["Valideur"][] = 'controllers/Courriers/getListSoustypes';


		//tools circuit
		$access_rights["Valideur"][] = 'controllers/Courriers/getFlowControls';
		$access_rights["Valideur"][] = 'controllers/Courriers/getCircuit';
		$access_rights["Valideur"][] = 'controllers/Courriers/valid';
		$access_rights["Valideur"][] = 'controllers/Courriers/send';
//    $access_rights["Valideur"][] = 'controllers/Courriers/insert';
//    $access_rights["Valideur"][] = 'controllers/Courriers/getInsertEnd';
		$access_rights["Valideur"][] = 'controllers/Courriers/refus';
		$access_rights["Valideur"][] = 'controllers/Courriers/reponse';


		//tabs view courrier
		$access_rights["Valideur"][] = 'controllers/Courriers/getInfos';
		$access_rights["Valideur"][] = 'controllers/Courriers/getQualification';
		$access_rights["Valideur"][] = 'controllers/Courriers/getMeta';
		$access_rights["Valideur"][] = 'controllers/Courriers/getTache';
		$access_rights["Valideur"][] = 'controllers/Courriers/getAffaire';
//		$access_rights["Valideur"][] = 'controllers/Courriers/getContext';
		$access_rights["Valideur"][] = 'controllers/Documents';

		$access_rights["Valideur"][] = 'controllers/Courriers/getDocuments';
		$access_rights["Valideur"][] = 'controllers/Courriers/getArs';
		$access_rights["Valideur"][] = 'controllers/Courriers/getRelements';
		$access_rights["Valideur"][] = 'controllers/Relements';
		$access_rights["Valideur"][] = 'controllers/Rmodels/getPreview';
		$access_rights["Valideur"][] = 'controllers/Armodels/getPreview';
		$access_rights["Valideur"][] = 'controllers/Ars/getPreview';
		$access_rights["Valideur"][] = 'controllers/Rmodels/download';
		$access_rights["Valideur"][] = 'controllers/Courriers/setArs';
		$access_rights["Valideur"][] = 'controllers/Courriers/genResponse';
		$access_rights["Valideur"][] = 'controllers/Ars';
		$access_rights["Valideur"][] = 'controllers/Armodels/download';
		$access_rights["Valideur"][] = 'controllers/Courriers/getParent';

		//tabs edit courrier
//    $access_rights["Valideur"][] = 'controllers/Courriers/setInfos';
//    $access_rights["Valideur"][] = 'controllers/Contacts/add';
//    $access_rights["Valideur"][] = 'controllers/Contacts/search';
//    $access_rights["Valideur"][] = 'controllers/Contacts/searchContactinfo';
//    $access_rights["Valideur"][] = 'controllers/Contactinfos/add';
//    $access_rights["Valideur"][] = 'controllers/Courriers/setQualification';
//    $access_rights["Valideur"][] = 'controllers/Courriers/getContacts';
//    $access_rights["Valideur"][] = 'controllers/Soustypes/getRefSimul';
		$access_rights["Valideur"][] = 'controllers/Courriers/setMeta';
		$access_rights["Valideur"][] = 'controllers/Courriers/setTache';
//    $access_rights["Valideur"][] = 'controllers/Courriers/setAffaire';

		/*
		 * Valideur Editeur
		 */

		//environnement
		$access_rights["Valideur Editeur"][] = 'controllers/Environnement/index';
		$access_rights["Valideur Editeur"][] = 'controllers/Environnement/historique';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/historiqueGlobal';
		$access_rights["Valideur Editeur"][] = 'controllers/Environnement/userenv';
		$access_rights["Valideur Editeur"][] = 'controllers/Environnement/getNotifications';
		$access_rights["Valideur Editeur"][] = 'controllers/Environnement/setNotificationRead';
		$access_rights["Valideur Editeur"][] = 'controllers/Environnement/setAllNotificationRead';
		$access_rights["Valideur Editeur"][] = 'controllers/Repertoires';
		$access_rights["Valideur Editeur"][] = 'controllers/Recherches';
		$access_rights["Valideur Editeur"][] = 'controllers/Taches';
		$access_rights["Valideur Editeur"][] = 'controllers/Desktops/setDeleg';

		//utilisateur
		$access_rights["Valideur Editeur"][] = 'controllers/Users/editinfo';
		$access_rights["Valideur Editeur"][] = 'controllers/Users/changemdp';
		$access_rights["Valideur Editeur"][] = 'controllers/Users/changenotif';
		$access_rights["Valideur Editeur"][] = 'controllers/Users/setInfosPersos';
		$access_rights["Valideur Editeur"][] = 'controllers/Users/getInfosCompte';
		$access_rights["Valideur Editeur"][] = 'controllers/Users/ajaxformvalid';
		$access_rights["Valideur Editeur"][] = 'controllers/Users/isAway';

		//actions courrier
//    $access_rights["Valideur Editeur"][] = 'controllers/Courriers/add';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/edit';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/view';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/delete';
//    $access_rights["Valideur Editeur"][] = 'controllers/Courriers/dispsend';
		//tools courrier
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getListSoustypes';


		//tools circuit
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getFlowControls';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getCircuit';
//    $access_rights["Valideur Editeur"][] = 'controllers/Courriers/insert';
//    $access_rights["Valideur Editeur"][] = 'controllers/Courriers/getInsertEnd';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/valid';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/send';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/refus';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/reponse';


		//tabs view courrier
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getInfos';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getQualification';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getMeta';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getTache';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getAffaire';

		//tabs edit courrier
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/setInfos';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/setQualification';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getContacts';
		$access_rights["Valideur Editeur"][] = 'controllers/Contacts/add';
		$access_rights["Valideur Editeur"][] = 'controllers/Contacts/search';
		$access_rights["Valideur Editeur"][] = 'controllers/Contacts/searchContactinfo';
		$access_rights["Valideur Editeur"][] = 'controllers/Contactinfos/add';
		$access_rights["Valideur Editeur"][] = 'controllers/Soustypes/getRefSimul';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/setMeta';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/setTache';
//		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getContext';
		$access_rights["Valideur Editeur"][] = 'controllers/Documents';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getDocuments';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getArs';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getRelements';
		$access_rights["Valideur Editeur"][] = 'controllers/Relements';
		$access_rights["Valideur Editeur"][] = 'controllers/Rmodels/getPreview';
		$access_rights["Valideur Editeur"][] = 'controllers/Armodels/getPreview';
		$access_rights["Valideur Editeur"][] = 'controllers/Ars/getPreview';
		$access_rights["Valideur Editeur"][] = 'controllers/Rmodels/download';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/setArs';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/genResponse';
		$access_rights["Valideur Editeur"][] = 'controllers/Ars';
		$access_rights["Valideur Editeur"][] = 'controllers/Armodels/download';
		$access_rights["Valideur Editeur"][] = 'controllers/Courriers/getParent';
//    $access_rights["Valideur Editeur"][] = 'controllers/Courriers/setAffaire';

		/*
		 * Documentaliste / Archiviste
		 */

		//environnement
		$access_rights["Documentaliste"][] = 'controllers/Environnement/index';
		$access_rights["Documentaliste"][] = 'controllers/Environnement/historique';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/historiqueGlobal';
		$access_rights["Documentaliste"][] = 'controllers/Environnement/userenv';
		$access_rights["Documentaliste"][] = 'controllers/Environnement/getNotifications';
		$access_rights["Documentaliste"][] = 'controllers/Environnement/setNotificationRead';
		$access_rights["Documentaliste"][] = 'controllers/Environnement/setAllNotificationRead';
		$access_rights["Documentaliste"][] = 'controllers/Repertoires';
		$access_rights["Documentaliste"][] = 'controllers/Recherches';
		$access_rights["Documentaliste"][] = 'controllers/Taches';
		$access_rights["Documentaliste"][] = 'controllers/Desktops/setDeleg';

		//utilisateurs
		$access_rights["Documentaliste"][] = 'controllers/Users/changemdp';
		$access_rights["Documentaliste"][] = 'controllers/Users/changenotif';
		$access_rights["Documentaliste"][] = 'controllers/Users/editinfo';
		$access_rights["Documentaliste"][] = 'controllers/Users/getInfosCompte';
		$access_rights["Documentaliste"][] = 'controllers/Users/ajaxformvalid';
		$access_rights["Documentaliste"][] = 'controllers/Users/isAway';
		$access_rights["Documentaliste"][] = 'controllers/Users/setInfosPersos';

		//actions courrier
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/add';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/edit';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/view';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/delete';
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/dispsend';
		//tools courriers
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getListSoustypes';

		//tools circuit
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getFlowControls';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getCircuit';
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/getInsertEnd';
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/insert';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/refus';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/valid';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/send';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/reponse';
		//tabs view courrier
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getInfos';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getQualification';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getMeta';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getTache';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getAffaire';

		//tabs edit courrier
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/setInfos';
//    $access_rights["Documentaliste"][] = 'controllers/Contacts/add';
//    $access_rights["Documentaliste"][] = 'controllers/Contactinfos/add';
//    $access_rights["Documentaliste"][] = 'controllers/Contacts/search';
//    $access_rights["Documentaliste"][] = 'controllers/Contacts/searchContactinfo';
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/setQualification';
//    $access_rights["Documentaliste"][] = 'controllers/Courriers/setContacts';
//    $access_rights["Documentaliste"][] = 'controllers/Soustypes/getRefSimul';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/setMeta';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/setTache';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/setAffaire';
//		$access_rights["Documentaliste"][] = 'controllers/Courriers/getContext';
		$access_rights["Documentaliste"][] = 'controllers/Documents';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getDocuments';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getArs';
		$access_rights["Documentaliste"][] = 'controllers/Relements';
		$access_rights["Documentaliste"][] = 'controllers/Rmodels/getPreview';
		$access_rights["Documentaliste"][] = 'controllers/Armodels/getPreview';
		$access_rights["Documentaliste"][] = 'controllers/Ars/getPreview';
		$access_rights["Documentaliste"][] = 'controllers/Rmodels/download';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/setArs';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/genResponse';
		$access_rights["Documentaliste"][] = 'controllers/Ars';
		$access_rights["Documentaliste"][] = 'controllers/Armodels/download';
		$access_rights["Documentaliste"][] = 'controllers/Courriers/getParent';



		/*
		 * Dispatcheur
		 */

		//environnement
		$access_rights["Aiguilleur"][] = 'controllers/Environnement/index';
		$access_rights["Aiguilleur"][] = 'controllers/Environnement/historique';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/historiqueGlobal';
		$access_rights["Aiguilleur"][] = 'controllers/Environnement/userenv';
		$access_rights["Aiguilleur"][] = 'controllers/Environnement/getNotifications';
		$access_rights["Aiguilleur"][] = 'controllers/Environnement/setNotificationRead';
		$access_rights["Aiguilleur"][] = 'controllers/Environnement/setAllNotificationRead';
		$access_rights["Aiguilleur"][] = 'controllers/Repertoires';
		$access_rights["Aiguilleur"][] = 'controllers/Recherches';
		$access_rights["Aiguilleur"][] = 'controllers/Taches';
		$access_rights["Aiguilleur"][] = 'controllers/Desktops/setDeleg';

		//utilisateurs
		$access_rights["Aiguilleur"][] = 'controllers/Users/changemdp';
		$access_rights["Aiguilleur"][] = 'controllers/Users/changenotif';
		$access_rights["Aiguilleur"][] = 'controllers/Users/editinfo';
		$access_rights["Aiguilleur"][] = 'controllers/Users/getInfosCompte';
		$access_rights["Aiguilleur"][] = 'controllers/Users/ajaxformvalid';
		$access_rights["Aiguilleur"][] = 'controllers/Users/isAway';
		$access_rights["Aiguilleur"][] = 'controllers/Users/setInfosPersos';

		//actions courrier
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/add';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/edit';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/view';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/delete';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/dispsend';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/dispmultiplesend';

		//tools courriers
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getListSoustypes';
		//tools circuit
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getFlowControls';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getCircuit';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getInsertEnd';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/insert';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/refus';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/valid';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/send';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/reponse';
		//tabs view courrier
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/getInfos';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getQualification';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getMeta';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/getTache';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/getAffaire';
		//tabs edit courrier
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/setInfos';
//    $access_rights["Aiguilleur"][] = 'controllers/Contacts/add';
//    $access_rights["Aiguilleur"][] = 'controllers/Contacts/search';
//    $access_rights["Aiguilleur"][] = 'controllers/Contacts/searchContactinfo';
//    $access_rights["Aiguilleur"][] = 'controllers/Contactinfos/add';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/setQualification';
//    $access_rights["Aiguilleur"][] = 'controllers/Soustypes/getRefSimul';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/setMeta';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/setTache';
//    $access_rights["Aiguilleur"][] = 'controllers/Courriers/setAffaire';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/getContext';
		$access_rights["Aiguilleur"][] = 'controllers/Courriers/getDocuments';
//	  $access_rights["Aiguilleur"][] = 'controllers/Documents';
//	  $access_rights["Aiguilleur"][] = 'controllers/Courriers/getArs';
//	  $access_rights["Aiguilleur"][] = 'controllers/Courriers/getRelements';
//	  $access_rights["Aiguilleur"][] = 'controllers/Relements';
//	  $access_rights["Aiguilleur"][] = 'controllers/Rmodels/getPreview';
//	  $access_rights["Aiguilleur"][] = 'controllers/Armodels/getPreview';
//	  $access_rights["Aiguilleur"][] = 'controllers/Ars/getPreview';
//	  $access_rights["Aiguilleur"][] = 'controllers/Rmodels/download';
//	  $access_rights["Aiguilleur"][] = 'controllers/Courriers/setArs';
//	  $access_rights["Aiguilleur"][] = 'controllers/Courriers/genResponse';
//	  $access_rights["Aiguilleur"][] = 'controllers/Ars';
//	  $access_rights["Aiguilleur"][] = 'controllers/Armodels/download';
//	  $access_rights["Aiguilleur"][] = 'controllers/Courriers/getParent';
		//application des droits initiaux
		foreach ($access_rights as $key => $val) {
			foreach ($val as $key2 => $val2) {
				$valid = $this->Acl->allow($key, $val2);
				$this->out($key . " -> " . $val2 . ' : ' . ($valid ? '<success>ok</success>' : '<error>error</error>'));
			}
		}

		$this->set_light_rights();
	}

	/**
	 *
	 */
	public function set_light_rights() {
		$Right = ClassRegistry::init('Right');
		$groupAros = $this->Acl->Aro->find('all', array('recursive' => -1, 'conditions' => array('model' => 'Profil', 'id > ' => 1, 'id < ' => 8)));
		foreach ($groupAros as $aro) {
			$rights = $Right->saveRightSets($aro['Aro']['id'], $Right->getRightSetsFromProfilName($aro['Aro']['alias']));
			$this->out($aro['Aro']['alias']);
			$this->out('  Save lightGrid -> ' . ($rights ? '<success>ok</success>' : '<error>error</error>'));
			$valid = array();
			foreach ($rights as $right => $val){
				if ($val == 1){
					$valid[] = $this->Acl->allow($aro['Aro']['alias'], $right);
				} else {
					$valid[] = $this->Acl->deny($aro['Aro']['alias'], $right);
				}
			}
			$this->out('  Apply rights -> ' . (!in_array(false, $valid, true) ? '<success>ok</success>' : '<error>error</error>'));
			$this->out();
		}
	}

	/**
	 * get the option parser.
	 *
	 * @return void
	 */
	public function getOptionParser() {
		return parent::getOptionParser()
						->description(__("Outils d'administration"))
						->addSubcommand('gen_roles', array(
							'help' => __('Création des entrées de la table Roles. ')
						))
						->addSubcommand('delete_db', array(
							'help' => __('Suppression des tables concernant les ACLs. ')
						))
						->addSubcommand('aro_sync', array(
							'help' => __('Création des entrées de la table ARO correspondant à la table Roles. ')
						))
						->addSubcommand('init_rights', array(
							'help' => __('Génération des droits entre la table ARO et la table ACO')
						))
						->addSubcommand('set_light_rights', array(
							'help' => __('Génération des droits simplifiés')
						));
	}

}
