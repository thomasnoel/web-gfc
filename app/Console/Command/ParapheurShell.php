<?php

App::import(array('Model', 'AppModel', 'File'));
App::import(array('Model', 'Courrier', 'File'));
App::import(array('Model', 'Comment', 'File'));
App::uses('Model', 'AppModel');
App::uses('ConnectionManager', 'Model');


App::uses('Controller', 'Controller');
App::uses('AppController', 'Controller');

App::uses('AppShell', 'Console/Command');
App::uses('ComponentCollection', 'Controller');
App::uses('IparapheurComponent', 'Controller/Component');
App::uses('NewPastellComponent', 'Controller/Component');

App::uses('CakeflowAppModel', 'Cakeflow.Model');
App::uses('SimpleCommentAppModel', 'SimpleComment.Model');
/**
 * Class ParapheurShell
 */
class ParapheurShell extends Shell {

    /**
	 *
	 * @var type
	 */
	private $_conn;

    /**
	 *
	 * @var type
	 */
	public $Courrier;


    var $uses = array('Courrier', 'Comment');

    public function startup() {
        parent::startup();

		$this->_conn = 'default';
		if (!empty($this->params['connection'])) {
			Configure::write('conn', $this->params['connection']);
			$this->_conn = $this->params['connection'];
		}

        if ($this->_conn != 'default') {
			ClassRegistry::init('Courrier');
			$this->Courrier = new Courrier();
			$this->Courrier->setDataSource($this->_conn);

            $collection = new ComponentCollection();
			$this->Parafwebservice = new IparapheurComponent($collection);
			$this->Pastellwebservice = new NewPastellComponent($collection);

        }
    }

    function main() {
        $Courrier = ClassRegistry::init('Courrier');
        $Connecteur = ClassRegistry::init('Connecteur');
		$conditions = [];
        $conn = $this->_conn;
        if ($conn != 'default') {
            $Courrier->useDbConfig = $conn;

            //Si service désactivé ==> quitter
            $parapheur = $Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%Parapheur%',
						'Connecteur.use_signature'=> true
                    ),
                    'contain' => false
                )
            );
//            if (!Configure::read('USE_PARAPHEUR')) {
            if (!empty($parapheur) ) {
				if (!$parapheur['Connecteur']['use_signature']) {
					$this->out("Service i-Parapheur désactivé");
					exit;
				}
			}
            $collection = new ComponentCollection();
            $this->Parafwebservice = new IparapheurComponent($collection);

			$pastell = $Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell'=> true
					),
					'contain' => false
				)
			);

            // Controle de l'avancement des flux dans le parapheur
			if( !empty($pastell)) {
				if ($pastell['Connecteur']['use_pastell'] && $parapheur['Connecteur']['signature_protocol'] == 'PASTELL') {
					$conditions['pastell_id !='] = null;
				}
			}
            $conditions['parapheur_etat'] = 1;
            $conditions['parapheur_id !='] = null;
            $conditions['signee'] = ( null | false );
            $courriers = $Courrier->find(
                'all',
                array(
                    'conditions' => $conditions,
                    'recursive' => -1,
                    'contain' => false,
                    'fields' => array('id', 'name', 'pastell_id')
                )
            );

            foreach ($courriers as $courrier) {
                //FIXME : pourquoi aller chercher compteur_id ?
                $compteur_id = 0;
                $this->_checkEtatParapheur($courrier['Courrier']['id'], $courrier['Courrier']['name'], false, $compteur_id);
            }
        }
    }

    function _checkEtatParapheur($courrier_id, $name, $tdt = false, $compteur_id = 0) {

        $collection = new ComponentCollection();
        $this->Parafwebservice = new IparapheurComponent($collection);

        $this->Pastellwebservice = new NewPastellComponent($collection);

        $Courrier = ClassRegistry::init('Courrier');
        $Comment = ClassRegistry::init('Comment');
        $CommentReader = ClassRegistry::init('SimpleComment.SCCommentReader');
        $Traitement = ClassRegistry::init('Cakeflow.Traitement');
        $Desktopmanager = ClassRegistry::init('Desktopmanager');

        $Connecteur = ClassRegistry::init('Connecteur');

        $conn = $this->_conn;
        if ($conn != 'default') {
            $Courrier->useDbConfig = $conn;
            $Comment->useDbConfig = $conn;
            $CommentReader->useDbConfig = $conn;
            $Traitement->useDbConfig = $conn;
            $Desktopmanager->useDbConfig = $conn;
            $Courrier->id = $courrier_id;
            $courrier = $Courrier->find(
				'first',
				array(
					'conditions' => array("Courrier.id" => $courrier_id),
					'contain' => array( 'Document' )
				)
			);

			$id_dossier = $courrier['Courrier']['parapheur_id'];

            // Recup données parapheur
            //Si service désactivé ==> quitter
            $parapheur = $Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%Parapheur%',
						'Connecteur.use_signature'=> true
                    ),
                    'contain' => false
                )
            );
            $pastell = $Connecteur->find(
                'first',
                array(
                    'conditions' => array(
                        'Connecteur.name ILIKE' => '%Pastell%',
                        'Connecteur.use_pastell' => true
                    ),
                    'contain' => false
                )
            );

            $traitements = $Courrier->Traitement->find(
                'first',
                array(
                    'conditions' => array(
                        'Traitement.target_id' => $courrier_id
                    ),
                    'contain' => false
                )
            );

            if( !empty($traitements) ) {

            	if( !empty($pastell) && $parapheur['Connecteur']['signature_protocol'] == 'PASTELL' ) {
            		if( !empty($courrier['Courrier']['pastell_id']) ) {


						App::uses('Signature', 'Lib');
						$Signature = new Signature();
						// Déclenchement de la,mise à jour côté PASTELL

						$Signature->updateInfosPastell($courrier['Courrier']['pastell_id']);
						// Déclenchement de la suite des traitements
						return $Courrier->majTraitementsParapheur($courrier_id, $conn);
					}
				}
            	else {
					$histo = $this->Parafwebservice->getHistoDossierWebservice($id_dossier, $parapheur);
				}
//$this->log($histo);
//$this->log($courrier['Courrier']['id']);
//$this->log($courrier['Courrier']['reference']);
    // debug( $Courrier->majTraitementsParapheur(276) );
    //die();
                if (isset($histo['logdossier'])){
                    for ($i = 0; $i < count($histo['logdossier']); $i++) {
                        if (!$tdt) {
							if ( ( /*$histo['logdossier'][$i]['status'] == 'Signe' || */ $histo['logdossier'][$i]['status'] == 'Archive' ) && ( isset($histo['logdossier'][$i+1]) ? !in_array( $histo['logdossier'][$i+1]['status'], array( 'NonLu', 'EnCoursVisa' ) ) : true ) ) {
								$dossier = $this->Parafwebservice->GetDossierWebservice($id_dossier, $parapheur);

								if (!empty($dossier['getdossier']['annotpub'])) {
									$Comment->create();
									$commpub['Comment']['target_id'] = $courrier_id;
									$commpub['Comment']['owner_id'] = -1;
									//                                    $commpub['Comment']['content'] = $histo['logdossier'][$i]['nom'] . " : " . $dossier['getdossier']['annotpub'];
									$commpub['Comment']['objet'] = $histo['logdossier'][$i]['nom'] . " : " . preg_replace("/[\n\r]/", " ", $dossier['getdossier']['annotpub']);
									$commpub['Comment']['private'] = false;
									$Comment->save($commpub['Comment']);
								}
								if (!empty($dossier['getdossier']['annotpriv'])) {
									$Comment->create();
									$commpriv['Comment']['target_id'] = $courrier_id;
									$commpriv['Comment']['owner_id'] = -1;
									//                                    $commpriv['Comment']['content'] = $histo['logdossier'][$i]['nom'] . " : " . $dossier['getdossier']['annotpriv'];
									$commpriv['Comment']['objet'] = $histo['logdossier'][$i]['nom'] . " : " . preg_replace("/[\n\r]/", " ", $dossier['getdossier']['annotpriv']);
									$commpriv['Comment']['private'] = true;
									$Comment->save($commpriv['Comment']);

									// On stocke les personnes autorisées à visualiser ce commentaire privé
									$CommentReader->create();
									$next = $Traitement->whoIs($courrier_id, 'after');
									$desktopmanager_id = $next[0];
									$desktops = $Desktopmanager->getDesktops($desktopmanager_id);
									foreach ($desktops as $dId) {
										$commprivReaders['CommentReader']['reader_id'] = $dId;
										$commprivReaders['CommentReader']['comment_id'] = $Comment->id;
										$CommentReader->saveAll($commprivReaders['CommentReader']);
									}
								}

								if ( Configure::read('Parapheur.CommentaireVide') && empty($dossier['getdossier']['annotpriv']) && empty($dossier['getdossier']['annotpub'])) {
									$Comment->create();
									$comm ['Comment']['target_id'] = $courrier_id;
									$comm ['Comment']['owner_id'] = -1;
									//                                    $comm ['Comment']['content'] = $histo['logdossier'][$i]['nom'] . " : " . $histo['logdossier'][$i]['annotation'];
									$comm ['Comment']['objet'] = $histo['logdossier'][$i]['nom'] . " : " . preg_replace("/[\n\r]/", " ", $histo['logdossier'][$i]['annotation']);
									//                                $comm ['Comment']['private'] = false;
									$Comment->save($comm['Comment']);
								}

								if ($courrier['Courrier']['parapheur_etat'] == 1) {
									if ($histo['logdossier'][$i]['status'] == 'Signe' || $histo['logdossier'][$i]['status'] == 'CachetOK' || $histo['logdossier'][$i]['status'] == 'Archive' || $histo['logdossier'][$i]['status'] == 'Vise') {
										$dossier = $this->Parafwebservice->GetDossierWebservice($id_dossier, $parapheur);
										$Courrier->majDocument($courrier_id, $dossier, $conn);
										if ($histo['logdossier'][$i]['status'] == 'Signe') {
											$Courrier->saveField('signee', true);
										}
										//                                        $Courrier->saveField('parapheur_etat', 2);
									}
									//                                debug($histo);
									if (Configure::read('Parapheur.Archive')) {
										if ($histo['logdossier'][$i]['status'] == 'Archive') {
											$Courrier->saveField('parapheur_etat', 2);
											$this->Parafwebservice->archiverDossierWebservice($id_dossier, "EFFACER");
										}
									}
								}

							}
							elseif ($histo['logdossier'][$i]['status'] == 'RejetSignataire'
								|| $histo['logdossier'][$i]['status'] == 'RejetVisa'
								|| $histo['logdossier'][$i]['status'] == 'RejetTransmission'
								|| $histo['logdossier'][$i]['status'] == 'RejetMailSecPastell'
							) { // Cas de refus dans le parapheur

								$dossier = $this->Parafwebservice->GetDossierWebservice($id_dossier, $parapheur);
								// Supprimer le dossier du parapheur
								$supDossier = $this->Parafwebservice->effacerDossierRejeteWebservice($id_dossier, $parapheur);
//								$this->log($dossier);
								if( !empty($dossier['getdossier']['annotpub']) ) {
									$Comment->create();
									$commpub['Comment']['target_id'] = $courrier_id;
									$commpub['Comment']['owner_id'] = -1;
//                                    $commpub['Comment']['content'] = $histo['logdossier'][$i]['nom'] . " : " . $dossier['getdossier']['annotpub'];
									$commpub['Comment']['objet'] = $histo['logdossier'][$i]['nom'] . " : " . preg_replace("/[\n\r]/", " ", $dossier['getdossier']['annotpub'] );
									$commpub['Comment']['private'] = false;
									$Comment->save($commpub['Comment']);
								}
								if( !empty($dossier['getdossier']['annotpriv']) ) {
									$Comment->create();
									$commpriv['Comment']['target_id'] = $courrier_id;
									$commpriv['Comment']['owner_id'] = -1;
//                                    $commpriv['Comment']['content'] = $histo['logdossier'][$i]['nom'] . " : " . $dossier['getdossier']['annotpriv'];
									$commpriv['Comment']['objet'] = $histo['logdossier'][$i]['nom'] . " : " . preg_replace("/[\n\r]/", " ", $dossier['getdossier']['annotpriv'] );
									$commpriv['Comment']['private'] = true;
									$Comment->save($commpriv['Comment']);

									// On stocke les personnes autorisées à visualiser ce commentaire privé
									$CommentReader->create();
									$next = $Traitement->whoIs($courrier_id, 'after');
									$desktopmanager_id = $next[0];
									$desktops = $Desktopmanager->getDesktops( $desktopmanager_id );
									foreach( $desktops as $dId ) {
										$commprivReaders['CommentReader']['reader_id'] = $dId;
										$commprivReaders['CommentReader']['comment_id'] = $Comment->id;
										$CommentReader->saveAll($commprivReaders['CommentReader']);
									}
								}

								if( empty($dossier['getdossier']['annotpriv']) && empty($dossier['getdossier']['annotpub']) ) {
									$Comment->create();
									$comm ['Comment']['target_id'] = $courrier_id;
									$comm ['Comment']['owner_id'] = -1;
//                                    $comm ['Comment']['content'] = $histo['logdossier'][$i]['nom'] . " : " . $histo['logdossier'][$i]['annotation'];
									$comm ['Comment']['objet'] = $histo['logdossier'][$i]['nom'] . " : " . preg_replace("/[\n\r]/", " ", $histo['logdossier'][$i]['annotation'] );
//                                $comm ['Comment']['private'] = false;
									$Comment->save($comm['Comment']);
								}
								// Supprimer le dossier du parapheur
//                            $this->Parafwebservice->effacerDossierRejeteWebservice($id_dossier, $parapheur);
								$Courrier->createRefusParapheur($courrier,$courrier['Courrier']['desktop_creator_id'], $histo['logdossier'][$i]['annotation']);
							} else {
								if ($histo['logdossier'][$i]['status'] == 'EnCoursTransmission')
									return true;
							}
						}
                    }
                    //FIXME Arnaud TEST
                    $Courrier->majTraitementsParapheur($courrier_id, $conn);
                }
            }
        }
        return false;
    }



	/**
	 *
	 * @return optionParser
	 */
	public function getOptionParser() {
		$actions = array(
			'repair_addressbook_slugs' => 'Recalcule les informations pour la recherche d\'homonymie des contacts.'
		);
		ksort($actions);
		$optionParser = parent::getOptionParser();
		$optionParser->description(__("Outils d'administration"));
		foreach ($actions as $action => $description) {
			$optionParser->addSubcommand($action, array('help' => $description));
		}
		$optionParser->addOption('connection', array(
			'short' => 'c',
			'help' => 'connection',
			'default' => 'default',
			'choices' => array_keys(ConnectionManager::enumConnectionObjects())
		));

		return $optionParser;
	}

}
