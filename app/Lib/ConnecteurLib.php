<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
App::uses('ComponentCollection', 'Controller');
App::uses('Component', 'Controller');
App::uses('PastellComponent', 'Controller/Component');
App::uses('NewPastellComponent', 'Controller/Component');
App::uses('Collectivite', 'Model');
App::uses('Courrier', 'Model'); // WebGFC
/**
 * Description of Connecteur
 *
 * @author splaza
 */
class Connecteurlib {

    /**
     * @var string Protocole de signature (pastell|iparapheur)
     */
    private $connecteur;

    /**
     * @var bool Indique si le connecteur est activé
     */
    protected $active=true;

    /**
     * @var Composant PastellComponent
     */
    protected $Pastell;

	/**
	 * @var Composant NewPastellComponent
	 */
	protected $NewPastell;

    /**
     * @var string type pastell
     */
    protected $pastell_type;

    /**
     * @var int|string $id_e
     */
    protected $id_e;

    /**
     * @var Model Deliberation
     */
//    protected $Deliberation;
    protected $Courrier;

    /**
     * @var int|string collectivite
     */
    protected $Collectivite;

    /**
     * @var Model Deliberation
     */
    protected $collection;

    /**
     * @var array configuration
     */
    protected $config;

    function __construct($connecteur) {
        $this->collection = new ComponentCollection();

        if(!empty($connecteur))
            $this->connecteur = $connecteur;
        else $this->connecteur = NULL;

		$this->NewPastell = new NewPastellComponent($this->collection);
		//Enregistrement de la collectivité (pour pastell)
		$this->Connecteur = ClassRegistry::init('Connecteur');
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Pastell%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);
		$this->id_e = '';
		if( !empty($hasPastellActif['Connecteur']['id_entity']) ) {
			$this->id_e = $hasPastellActif['Connecteur']['id_entity'];
		}

		$hasParapheurActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true,
					'Connecteur.signature_protocol'=> 'IPARAPHEUR'
				),
				'contain' => false
			)
		);
		$this->pastell_type = '';
		if( !empty($hasParapheurActif) ) {
			$this->pastell_type = $hasParapheurActif['Connecteur']['type_doc_pastell'];
		}
        $this->Courrier = new Courrier;
    }

    /**
     * Fonction appelée à chaque appel de fonction non connu
     * et redirige vers la bonne fonction selon le protocol
     *
     * @param string $name nom de la fonction appelée
     * @param array $arguments tableau d'arguments indexés
     * @return mixed
     * @throws Exception
     */
    public function __call($name, $arguments) {
        if(!empty($this->connecteur) && $this->active)
            $suffix = ucfirst(strtolower($this->connecteur));
        else return NULL; //Retoun NULL pour les appels de fonction sans connecteur

        if (method_exists($this, $name . $suffix)) {
            return call_user_func_array(array($this, $name . $suffix), $arguments);
        }
        else {
            throw new Exception(sprintf('The required method "%s" does not exist for %s', $name . $suffix, get_class()));
        }
    }

    public function getType()
    {
      return !empty($this->connecteur)?$this->connecteur:false;
    }

     /**
     * @param array $acte
     * @param array $annexes (content, filename)
     * @return bool|int false si echec sinon identifiant pastell
     */
//     protected function createPastell($courrier, $document = null, $annexes = array()) {
//
//        $this->Courrier->id = $courrier['Courrier']['id'];
////        $id_d = $this->Pastell->createDocument($this->id_e, $this->pastell_type);
//        $id_d = $this->NewPastell->createDocument($this->id_e, $this->pastell_type);
//        $data=array();
//        //Gestion dans Pastell des signatures manuscrites parapheur_etat==0 OR IS NULL
//        if(empty($courrier['Courrier']['parapheur_etat']))
//            $data[$this->config['field']['envoi_signature']] = false;
//        try {
//            $this->Pastell->editTransmission($this->id_e, $id_d, $data);
////            if ($this->Pastell->modifDocument($this->id_e, $id_d, $data, file_get_content($courrier['Document']['path']), $annexes)) {
//            if ($this->NewPastell->modifDocument($this->id_e, $id_d, $data, file_get_content($courrier['Document']['path']), $annexes)) {
//                $this->Courrier->saveField('pastell_id', $id_d);
//                return $id_d;
//            }
//            else
//            {
//                throw new Exception();
//            }
//        } catch (Exception $e) {
////            $this->Pastell->action($this->id_e, $id_d, $this->config['action']['supression']);
//            $this->NewPastell->action($this->id_e, $id_d, $this->config['action']['supression']);
//            $this->log($e->getMessage(), 'error');
//            return false;
//        }
//    }
}
