<?php

/**
 * @author Florian Ajir <florian.ajir@adullact.org>
 * @editor Adullact
 *
 * @created 21 janvier 2014
 *
 * Interface controllant les actions de signature électronique
 * Utilise les connecteurs/components PastellComponent et IparapheurComponent
 *
 */
App::uses('IparapheurComponent', 'Controller/Component');
App::uses('ConnecteurLib', 'Lib');

class Signature extends ConnecteurLib  {

    /**
     * @var Composant IparapheurComponent
     */
    private $Iparapheur;

    /**
     * @var string type technique dans le parapheur
     */
    private $parapheur_type;

    /**
     * @var string visibilité du document parapheur
     */
    private $visibility;


    /**
     * Appelée lors de l'initialisation de la librairie
     * Charge le bon protocol de signature et initialise le composant correspondant
     */
    public function __construct() {
        $this->Connecteur = ClassRegistry::init('Connecteur');
        $hasParapheurActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true,
					'Connecteur.signature_protocol'=> 'IPARAPHEUR'
                ),
                'contain' => false
            )
        );
		$hasPastellActif = $this->Connecteur->find(
			'first',
			array(
				'conditions' => array(
					'Connecteur.name ILIKE' => '%Pastell%',
					'Connecteur.use_pastell'=> true
				),
				'contain' => false
			)
		);

		$signatureProtocol = 'IPARAPHEUR';
		if( !empty($hasPastellActif) ) {
			$signatureProtocol = 'PASTELL';
		}

        //FIXME cas où vide => exception levée lors de la tache planifiée
        parent::__construct($signatureProtocol);

        if ($hasParapheurActif['Connecteur']['use_signature']) {
            if (!$this->getType())
                throw new Exception("Aucun parapheur désigné");

            if ($this->getType() == 'IPARAPHEUR') {
                $this->Iparapheur = new IparapheurComponent($this->collection);
                $this->visibility = $hasParapheurActif['Connecteur']['visibility'];
            }
            $this->parapheur_type = $hasParapheurActif['Connecteur']['type'];
        }
		else if(!empty($hasPastellActif) ) {
			$this->NewPastell = new NewPastellComponent($this->collection);
			$idCe = $this->NewPastell->getIdCeByEntiteAndConnecteur($hasPastellActif['Connecteur']['id_entity'], 'signature');
			$infos = $this->NewPastell->getOneConnector($hasPastellActif['Connecteur']['id_entity'], $idCe);
			$this->parapheur_type = $infos->data->data['iparapheur_type'];
		}
        else
            $this->active = false;
    }

    /**
     * @param array $flux
     * @param int|string $circuit_id
     * @param string $document contenu du fichier du document principal
     * @param array $annexes (content, filename, mimetype)
     * @return bool|int false si echec sinon identifiant iparapheur
     */
    public function sendIparapheur($flux, $circuit_id, $document, $documentSupp = array(), $annexes = array()) {

    	$this->Connecteur = ClassRegistry::init('Connecteur');
        $hasParapheurActif = $this->Connecteur->find(
            'first',
            array(
                'conditions' => array(
                    'Connecteur.name ILIKE' => '%Parapheur%',
					'Connecteur.use_signature'=> true,
					'Connecteur.signature_protocol'=> 'IPARAPHEUR'
                ),
                'contain' => false
            )
        );

        if (is_numeric($circuit_id)) {
            $circuits = $this->listCircuitsIparapheur();
            $libelleSousType = $circuits[$circuit_id];
        } else {
            $libelleSousType = $circuit_id;
        }
CakeLog::write( 'error', $libelleSousType );
//CakeLog::write( 'error', utf8_encode($libelleSousType) );

//        $targetName = $flux[CAKEFLOW_TARGET_MODEL]['name'];
        $targetName = $flux[CAKEFLOW_TARGET_MODEL]['reference'] . ' ' . $flux[CAKEFLOW_TARGET_MODEL]['name'];
        $date_limite = null;

        $ret = $this->Iparapheur->creerDossierWebservice(
            $targetName,
            $this->parapheur_type,
            $libelleSousType,
            $this->visibility,
            $document,
            $documentSupp, //v3.0 de web-GFC compatible iParapheur v4.3
            $annexes,
            $date_limite,
            $hasParapheurActif
        );
//debug($ret);
//CakeLog::write( 'error', $ret );
//        if ($ret['messageretour']['coderetour'] == 'OK') {
//            return $ret['dossierID'];
            return $ret;
//        } else {
//            return $ret['messageretour']['message'];
//            CakeLog::write($ret['messageretour']['message'], 'parapheur');
//            return false;
//        }
    }

    /**
     * @param array $delib
     * @param int|string $circuit_id
     * @param string $document contenu du fichier du document principal
     * @param array $annexes (content, filename)
     * @return bool|int false si echec sinon identifiant pastell
     */
    public function sendPastell($data, $circuit_id, $document = null, $documentSupp = array(), $annexes = array()) {

        try {

			if( isset( $data['Courrier']['pastell_id']) && !empty( $data['Courrier']['pastell_id'])) {
				$id_d = $data['Courrier']['pastell_id'];
			}
			else {
				// On crée le document sur PASTELL
				$document = $this->NewPastell->createDocument($this->id_e, $this->pastell_type);
				$id_d = $document->getIdD();
				if (isset($data['Courrier'])) {
					$this->Courrier = ClassRegistry::init('Courrier');
					$this->Courrier->updateAll(
						array('Courrier.pastell_id' => "'" . $id_d . "'"),
						array('Courrier.id' => $data['Courrier']['id'])
					);
					$this->Courrier->saveField('pastell_etat' , 1);
					$this->Courrier->saveField('pastell_typedoc' , $this->pastell_type);
				}
			}

			// On récupère le circuit qui va bien
			if (is_numeric($circuit_id)) {
				$circuits = $this->NewPastell->getInfosField($this->id_e, $id_d, 'iparapheur_sous_type');
				$sousType = $circuits[$circuit_id];
			} else {
				$sousType = $circuit_id;
			}
			$this->NewPastell->selectCircuit($this->id_e, $id_d, $sousType);

			// On modifie pour associer les documents à envoyer à la signature
			$success = $this->NewPastell->modifDocumentDocumentASigner($this->id_e, $id_d, $data, $document, $documentSupp, $annexes);
            if (!$success)
                throw new Exception('Error modifDocument');

            // On déclenche l'envoi au IP
			if ($this->NewPastell->action($this->id_e, $id_d, 'send-iparapheur'))
                return $id_d;
            else
                throw new Exception('Error action');

        } catch (Exception $e) {
            $this->NewPastell->action($this->id_e, $id_d, 'supression');
            throw new Exception($e->getMessage());

        }

        return false;
    }

    public function getEtatIparapheur($data) {

		$resp = $this->Iparapheur->getListeTypesWebservice($data);
    }

    /**
     * @param int $id_d
     * @param bool $get_details
     * @return array(
     * 'action' => 'rejet-iparapheur',
     * 'message' => 'Le document a été rejeté dans le parapheur : 23/01/2014 16:54:52 : [RejetSignataire] vu',
     * 'date' => '2014-01-23 16:55:07'
     * )
     */
    public function getLastActionPastell($id_d, $get_details = false) {
        $details = $this->NewPastell->detailDocument($this->id_e, $id_d);
        if ($get_details)
            return $details['last_action'];
        $last_action = $details['last_action']['action'];
        return $last_action;
    }

    /**
     * @param int $id_d
	 * @return array
     */
    public function getBordereauPastell($id_d) {
        return $this->NewPastell->getFile($this->id_e, $id_d, 'bordereau');
    }

	/**
	 * @param int $id_d
	 * @return array
	 */
	public function getBordereauSignaturePastell($id_d) {
		return $this->NewPastell->getFile($this->id_e, $id_d, 'bordereau_signature');
	}

    /**
     * @param int $id_d
	 * @return array
     */
    public function getDocumentSignePastell($id_d) {
        return $this->NewPastell->getFile($this->id_e, $id_d, 'document');
    }

	/**
	 * @param int $id_d
	 * @return array
	 */
	public function getHistoriquePastell($id_d) {
		return $this->NewPastell->getFile($this->id_e, $id_d, 'iparapheur_historique');
	}
	/**
	 * @param int $id_d
	 * @return array
	 */
	public function getSignatureZipPastell($id_d) {
		return $this->NewPastell->getFile($this->id_e, $id_d, 'signature');
	}

    /**
     * @param int $id_d
     * @return array
     */
    public function getDetailsPastell($id_d) {
        return $this->NewPastell->detailDocument($this->id_e, $id_d);
    }

    /**
     * @param int $id_d
     * @return bool
     */
    public function isSignePastell($id_d) {
        $details = $this->NewPastell->detailDocument($this->id_e, $id_d);
        return !empty($details['data'][$this->config['field']['has_signature']]);
    }

    /**
     * @param int $id
     */
    public function deleteIparapheur($id) {
        $this->Iparapheur->effacerDossierRejeteWebservice($id);
    }

    /**
     * @param int $id
     */
    public function archiveIparapheur($id) {
        $this->Iparapheur->archiverDossierWebservice($id, 'EFFACER');
    }


    /**
     * @link listCircuits()
     * @return array
     * @throws Exception
     */
    public function listCircuitsIparapheur($data) {
        $resp = $this->Iparapheur->getListeSousTypesWebservice($this->parapheur_type, $data);
        if( $resp == 'Connexion impossible' ) {
            return $resp;
        }
        else if( isset($resp['messageretour']) && $resp['messageretour']['coderetour'] == '-1') {
            $resp['soustype'] = array();
            return $resp['soustype'];
        }
        else if( $resp == null ) {
            $resp['soustype'] = array();
            return $resp['soustype'];
        }
        else if (is_array($resp) && array_key_exists('soustype', $resp)) {
			$resp = array(
				'soustype' => $resp['soustype'],
				'soustypeold' => $resp['soustypeold']
			);
            return $resp;
        }
		else if(  !empty( $resp) ) {
			return $resp;
		}
		else {
            throw new Exception($resp['messageretour']['message']);
        }
    }

    /**
     * @link listCircuits()
     * @return array
     */
    public function listCircuitsPastell($data) {
		if( $data['Connecteur']['signature_protocol'] == 'PASTELL') {
			$this->Connecteur = ClassRegistry::init('Connecteur');
			$hasPastellActif = $this->Connecteur->find(
				'first',
				array(
					'conditions' => array(
						'Connecteur.name ILIKE' => '%Pastell%',
						'Connecteur.use_pastell'=> true
					),
					'contain' => false
				)
			);
			$data = $hasPastellActif;
		}
		$soustypes = $this->NewPastell->getCircuits($this->id_e, $data['Connecteur']['type_doc_pastell']);
        return $soustypes;
    }

    /**
     * @return array
     */
    public function printCircuits() {
        $circuits = array('Standard' => array('-1' => 'Signature manuscrite'));
        try {
            $circuits_parapheur = $this->listCircuits();
            if (!empty($circuits_parapheur))
                $circuits['Parapheur'] = $circuits_parapheur;
        } catch (Exception $e) {
        } //Si erreur de connexion au parapheur

        return $circuits;
    }

    /**
     * @link updateAll()
     * @return array
     */
    public function updateAllIparapheur() {
        return $this->Courrier->majActesParapheur();
    }

    /**
     * @link updateAll()
     * @return array
     */
    public function updateAllPastell() {
        return $this->Courrier->majSignaturesPastell();
    }

    /**
	 * Mise à jour côté PASTELL
	 * On déclenche u verif-iparapheur pour savoir où en est le dossier dans IP
     * @param $id_d
     * @return array
     */
    public function updateInfosPastell($id_d) {
        $details=$this->getDetailsPastell($id_d);
        if (!empty($details['last_action']['action']) && $details['last_action']['action'] === 'recu-iparapheur' || !empty($details['last_action']['action']) && $details['last_action']['action'] === 'termine') {
            return $details;
        } else {
            return $this->NewPastell->action($this->id_e, $id_d, 'verif-iparapheur');
        }
    }
}
