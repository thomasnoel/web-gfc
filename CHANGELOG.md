# Change Log

Toutes les modifications apportées au projet seront documentées dans ce fichier.

Le format est basé sur le modèle [Keep a Changelog](http://keepachangelog.com/)
et adhère aux principes du [Semantic Versioning](http://semver.org/).

## [3.1.10] - 2022-09-13
### Corrections
- Le filtre de recherche "Agent étant intervenu sur le flux" retourne désormais bien tous les flux sur lesquels un agent est intervenu (même s'il ne le possède plus)
- [LDAP] La synchronisation LDAP ne fonctionne pas si la base DN est ajoutée au nom de domaine fourni (targetDn), au moment de la recherche LDAP (ldap_search)
- La civilité du contact n'était pas reprise lors de l'import via fichier CSV
- En cas d'un trop grand nombre de flux, le refus ne marche pas car l'identifiant de référence de l'ancien flux est de trop grande taille (> 32767 taille limite du smallint)
- Les flux en copie ne sont plus aiguillables depuis l'environnement de l'aiguilleur, seulement adressables en copie
- Les flux en copie ne doivent pas être ré-aiguillables
- Au moment du ré-aiguillage, certaines bannettes n'étaient pas vidées car seul le bureau principal de l'initiateur ré-aiguillant était pris en compte
- Dans l'historique, la traduction du mot "days" n'était pas faite
- Suite à la mise à jour de LibreOffice en version 7.3.3.2, les caractères spéciaux et accentués posent soucis dans les noms des modèles de documents lors de l'utilisation du webdav.
  - Les ARs voient leur préfixe "[AR]_" remplacés par "AR_"
- La délégation posait souci sur les profils secondaires. Désormais un administrateur peut supprimer toutes délégations, quelque soit le profil.
- L'export CSV des bureaux ne fonctionnaient pas correctement
- Lors de l'édition d'un bureau, la liste déroulante permettant de définir le profil était parfois vide car aucun rôle n'était précédemment associé donc on ne récupérait aucune valeur
- Si l'utilisateur est abonné aux mails de notifications de façon quotidienne, si aucune notification n'est créée, l'enregistrement quotidien ne se fait pas

## [3.1.9] - 2022-06-23
### Corrections
- Mise à jour des droits dans le template SQL
- La fonction getAdresses permettant de charger les adresses de la BAN n'a pas besoin d'être placée dans les droits
- A la génération d'une réponse, prise en compte des caractères spéciaux dans les initiales de la personne enregistrée dans la liste déroulante "Affaire suivie par"
- Prise en compte des images stockées en base64 de façon multiples à l'intérieur du mail
- Mise en paramétrage du nombre de secondes avant déclenchement de la sauvegarde automatique
- Lors de l'envoi d'un document par mail, ajout d'une action permettant de charger automatiquement l'adresse mail du contact
- Mise en paramétrage du choix permettant de définir si on souhaite afficher les bureaux créés automatiquement dans les différentes listes déroulantes disponibles
- Mise en paramétrage du choix permettant de définir si on souhaite utiliser ou non la sauvegarde automatique du formulaire du flux
- Mise en paramétrage du choix permettant de définir si on souhaite récupérer le contenu du champ objet lors du scan des mails
- Les retours à la ligne étaient mal interprétés dans l'affichage des commentaires
- Au moment de l'envoi à Pastell, le json reprenant les métadonnées ne se créait pas côté PASTELL si le document visé n'était pas nommé "gfc-dossier"
- L'URL est encodée proprement pour pallier les soucis d'affichage des documents dans la visionneuse

## [3.1.8] - 2022-05-13
### Corrections
- Rectificatif pour la valeur du champ objet lors de l'accès à l'environnement de l'agent.
  - On remplace les accents APRES avoir réduit le champ objet à 10 caractères
- [MAIL] Lors de la scrutation des mails, le document reprenant le contenu du mail ne se créait pas si le sujet présente des caractères spéciaux, du type « ou »
- Lors de l'envoi d'un document par mail, si le sujet possède une variable du type #NOM_FLUX" ou autre, cette variable n'était pas remplacée car non prise en compte
- [CAS] Pour que la connexion au CAS fonctionne (sans LemonLdap ou tout autre application), des fonctionnalités ont dû être corrigées et améliorées
- Les documents associés à un flux supprimés via le traitement par lot (Outils de l'administrateur) sont supprimés du disque
- Au moment du refus, l'id de la notification n'était pas stocké et donc la notification non envoyée et l'enregistrement du refus bloqué.
- Dans le cas où la dernière étape possède une étape concurrente (OU), le flux est désormais traité pour tout le monde lorsque la clôture est déclenchée
- Les commentaires privés apparaissaient chez certains agents alors qu'il ne fallait pas
- Le problème de perte de droits lors de l'association d'un utilisateur à un nouveau service (depuis l'écran du service) a été corrigé
- Lors de l'envoi pour copie, l'envoi dans un circuit ou le ré-aiguillage, les champs obligatoires non renseignés ne bloquaient pas le formulaire et cela générait une erreur
- Les messages ont été corrigés lors du test de connexion au connecteur i-Parapheur

## [3.1.7] - 2022-04-20
### Corrections
- Les fichiers en extension et au format .htm bloquaient l'envoi au i-Parapheur (comme les zip)

## [3.1.6] - 2022-03-24
### Corrections
- [Pastell] Si plusieurs connecteurs de type signature sont présents du côté de PAstell, le type sélectionné était le 1er trouvé ... sans regarder les associations de connecteur.
  - Désormais, on regarde uniquement le connecteur de signature associé au flux gfc-dossier
- [Pastell] Les données envoyées en json vers Pastell n'étaient reprises dans le champ "metadonnees"
- Lors d'un flux réponse, s'il est refusé, il est perdu car aucun initiateur n'est récupéré
- Si les commentaires présentent des "line break" (retour à la ligne), l'affichage ne se fait pas et le javascript "casse"
- [MAIL] Les images intégrées directement dans les mails ou en pièces jointes n'étaient pas reprises dans les documents créés pour les flux générés
- [MAIL] Le contenu HTML des mails récupérés a été corrigé car ne se faisait pas pour chaque mail
- [MAIL] Le header dans le corps du pdf ne reprenait pas tous les éléments requis à l'affichage

## [3.1.5] - 2022-03-09
### Modifications
- Création de la base de données webgfc_template lors du script AdminTools install afin que chef puisse faire son travail

## [3.1.4] - 2022-03-07
### Corrections
- Le modèle Connecteur n'était pas chargé lorsque l'on modifie un flux ce qui générait une erreur
- A l'enregistrement d'une modification de bureau, les valeurs des profils déjà enregistrés n'étaient pas prises en compte.
- Lors de l'envoi d'un document par mail, les retours à la ligne étaient perdus pour les mail-types


## [3.1.3] - 2022-03-01
### Corrections
- A la création d'un flux (via scan de documents, de mail ou de script), le profil du créateur n'est pas tout le temps renseigné ce qui bloque les refus
- Le refus est par moment adressé au mauvais profil de l'agent ce qui entraîne un blocage.
  - Le desktop_creator_id prend par moment le profil_id du valideur et non pas celui de l'initiateur car les valeurs stockées en session ne sont pas prises en compte au bon moment


## [3.1.2] - 2022-02-14
### Corrections
- GRC Localeo: lorsque la GRC Localeo crée un nouveau flux, elle envoie parfois un contactId = -1.
  - Du coup, les flux se voient créés temporairement et leur n° de référence utilisé puis supprimé ce qui crée un décalage dans les IDs et les n° de référence par la suite
- L'administrateur ne pouvait se déléguer un profil vers son propre environnement
- Lors de l'envoi de documents par mail, les PJs ne sont pas toujours prises en compte si les noms présentent des caractères spéciaux
- Le script de mise à jour des BAN adresses ne mettait pas correctement à jour les entrées en base, le format des données fourni ayant évolué

## [3.1.1] - 2022-01-24
### Corrections
- Une fois les documents scannés, on déplace désormais les fichiers dans un répertoire "tampon" nommé **inprogress** pour s'assurer que chaque flux généré possède bien un document associé sinon des flux se retrouvent "vides"
  - Uniquement si la variable Configure::write('Scan.UseFolderInprogress', false); est à true
- Les sous-types désactivés ne sont plus sélectionnables pour l'action "Clore et répondre", ni pour la sélection des sous-types de référence
- Le délai de traitement ne se mettait pas à jour lorsque l'on changeait le sous-type d'un flux
- Le sens des flux (entrant/sortant) ne se mettait pas à jour lorsque l'on changeait le sous-type d'un flux
- Le composant utilisé pour Pastell ne doit aps être chargé si le connecteur n'est pas, utilisé.
- À la récupération des informations du circuit, en cas de suppression d'une étape du circuit, on continue d'afficher l'information pour les flux en cours de traitement pour savoir où se trouve le flux
- Plus aucun fichier zip n'est adressé au IP lors de l'envoi d'un document et ses pièces jointes
- DirectMairie: Lors de la recherche d'une demande, si le n° n'existe pas du côte DM, une page d'erreur apparaissait

## [3.1.0] - 2022-01-07
### Ajouts
- Ajout de la possibilité d'envoyer à PASTELL directement depuis la gestion du flux. IL faut pour cela avoir défini un flux studio du côté Pastell
- Lors d'un envoi vers Pastell, les cheminements possibles sont :
    - Visa/Signature -> on définit un sous-type IP dans le circuit
    - Visa/Signature - Mail Sécurisé -> on définit un sous-type IP dans le circuit
    - Cheminement défini dans le sous-type -> on coche les cases dédiées au cheminement dans le sous-type, et on y définit le sous-type IP
    - Etape à la volée :
        - Si on n'a pas défini de cheminement personnalisé : tous les choix sont laissés à l'utilisateur, qui devra renseigner un sous-type IP si l'étape est de type "Visa/Signature"
        - Si on a défini un cheminement personnalisé : seul ce cheminement est disponible, et s'il y a une étape IP, elle a été définie par l'administrateur dans le sous-type
- Ajout dans le cycle de vie du flux de l'état des documents dans Pastell sur la ligne dédiée à Pastell
- Il est désormais possible de définir plusieurs superadmin sur une instance de web-GFC (environnement super-administrateur)
- Une API pour tester l'impression des ODT a été ajoutée
- Au niveau des métadonnées, on pourra définir si on souhaite répercuter automatiquement la valeur de cette métadonnée d'un flux entrant vers son flux sortant associé
- Au niveau de la délégation, on pourra désormais savoir quel utilisateur a réalisé l'action à une étape donnée
- Lorsqu'un commentaire vient d'être créé dans un flux, l'utilisateur accédant au flux verra une pop-up s'afficher, indiquant qu'un nouveau commentaire est disponible
- Un nouveau paramètre est ajouté pour que l'administrateur puisse choisir la limite pour l'affichage du nombre de flux dans l'historique (par défaut -1 mois)
- Ajout dans l'historique d'un message indiquant que les flux affichés pour chaque profil ne sont que ceux datant de moins de 1 mois
- Ajout de la possibilité de ré-aiguiller un flux directement depuis le formulaire du flux (uniquement pour les initiateurs)
- Ajout du replyTo lors de l'envoi d'un document par mail
- Ajout dans le menu Outils de l'administrateur de la possibilité de supprimer des flux: par n° de référence ou sur une plage de dates donnée
- [RGPD] Ajout de la notion de consentement au sein même du journal d'événements lorsque l'agent coche/décoche la case d'acceptation des notifications.


### Modifications
- Depuis le profil aiguilleur, l'action d'insertion dans un circuit ne sera pas visible dans la liste des actions mais elle sera remplacée par la possibilité d'aiguiller directement depuis la gestion du flux.
- Lorsque l'on fait un export CSV en recherche, la boite de dialogue se ferme désormais dès lors que le fichier d'export est retourné
- Il est désormais possible de définir des variables pour les mails-types envoyés depuis la gestion du flux, de même que pour les notifications par mail
- Dans la gestion des bureaux, on masque désormais les bureaux créés de façon automatique à la création d'un agent (notamment via la synchronisation LDAP) afin de ne pas être "pollué"
- Lorsque l'on consulte un flux et qu'on clique sur détacher de la bannette des flux en copie, l'agent est redirigé vers l'environnement principal de l'utilisateur
- Lors d'un envoi à la volée vers Pastell, on fait comme pour IP, on ne force pas le retour, sauf si c'est une dernière étape
- A l'activation du connecteur Pastell, on active automatiquement le connecteur IP en le positionnant sur le protocole PASTELL
- Il n'y a plus de connecteur Mail Sécurisé, la connexion se fait désormais directement dans le connecteur Pastell.
- Le connecteur IP garde le choix de la connexion (direct ou Pastell) mais aucune autre information n'est à renseigner si on choisit Pastell.
- Le connecteur Pastell se voit ajouter les infos du type de doc pour IP + le choix d'activer ou non l'envoi via le mailsec.
- Enfin, le type IP est directement récupéré depuis Pastell
- Avant envoi au mail sécurisé, si le contact ne possède pas de mail, l'action de validation est grisée.  Au moment de la sélection du contact ou de sa modification afin de li associe run mail, on vérifie et on active l'action si tout est ok
- Les bureaux Pastell et Parapheur (fournis par défaut dans l'application) ne sont visibles que si les connecteurs associés sont activés.
    - A noter également que si le connecteur Pastell est activé, le bureau Parapheur est automatiquement masqué.

### Corrections
- Renommage des fichiers envoyés par mail dans le cas où des caractères spéciaux sont présents dans le nom de ces fichiers.
- Amélioration du temps de traitement, avec notamment la mise en cache des données issues de Pastell.
- Amélioration de la récupération des contenus et pièces jointes des mails.
- Certains formulaires ne présentaient pas de blocages sur les champs obligatoires
- Intégration des BANs, la taille du champ "ident" de la table _bansadresses_ a été modifié car la BAN a vu sa valeur augmentée.
- Au moment où on crée un commentaire privé, la liste des agents affichés reprenait les profils alors qu'il faut que ce soit le nom des agents
- Le clic sur les lignes des bannettes ne "répondait" plus car une action (location.reload()) faisait obstruction au traitement
- Lorsque l'on recherche les adresses associées à une commune, la fonction envoyant les données ne gérait pas correctement la présence d'un proxy


## [3.0.14] - 2021-12-17
### Corrections
- Lors de l'envoi d'un mail, le replyTo est activé par défaut alors qu'il faut pouvoir le conditionner selon la collectivité.

## [3.0.13] - 2021-11-16
### Corrections
- Lors de la scrutation par mail, la récupération des documents posait soucis à cause des caractères accentués
- L'envoi par mail d'un document PDF avec des accents ou caractères spéciaux ne se faisait pas
- L'envoi d'un AR généré en ODT se fait en PDF mais ce dernier était encore vu comme un ODT ce qui empêchait son ouverture dans le mail adressé
- Lors de l'ajout d'un flux, les PJs passaient en doc principal sans raison valable

## [3.0.12] - 2021-08-06
### Corrections
- [API] Les limites et offset n'étaient pas pris en compte lors de la requête en GET
- [API] L'action PUT n'était pas prise en compte pour les collectivités
- [API] Les retours des appels sur les rôles et les services étaient erronés (text/html au lieu de application/json)
- [API] La suppression d'un profil directement lié à un utilisateur (profil principal) n'est pas possible et un message d'alerte est remonté
- [API] Un utilisateur possédant le même username/nom/prénom ne peut être créé deux fois


## [3.0.11] - 2021-06-??
### Corrections
- Un warning apparaissait lors du déclenchement de la création du courrier depuis la GRC car l'envoi du json n'est pas possible ici
- Lors de la synchronisation entre la GRC et webGFC, le script ne trouvait pas les tables liées à la collectivité cible
- Si aucun document n'est présent lors de l'envoi depuis la GRC, une erreur est remontée vers la GRC ce qui entraine un blocage
- Le script de notification de flux envoyés au IP, mais non encore traité depuis 7 jours, a été corrigé car les notifications n'étaient pas bien adressées
- Lors de l'accès aux bannettes, si le flux comporte un objet avec des caractères spéciaux, l'affichage est cassé
- Lors de la recherche par valeurs de métadonnées, la combinaison de plusieurs valeur des métadonnées retournait des résultats erronés
- L'enregistrement du service à la création du flux ne retournait pas de résultats dans certains cas

## [3.0.10] - 2021-05-28
### Corrections
- Les quote étaient mal encodées lors de l'affichage de l'objet dans les bannettes des agents
- Lors de l'édition via webdav d'un AR, la variable $conn n'était pas envoyée à la vue pour la fonction getArs donc l'édition ne fonctionnait pas
- Lors des échanges avec IP, suppression du fsockopen sur le port 80 qui n'a plus lieu d'être et surtout qui bloque les échanges si le port 80 est bloqué
- La variable $desktopIdUse n'était pas déclarée par défaut ce qui générait un undefined index
- Le script de mise à jour des sous-types issus du i-Parapheur ne prenait pas en compte la table wkf_visas ce qui bloquait les traitements des flux passés/en cours (pas les nouveaux)

## [3.0.9] - 2021-05-21
### Corrections
- [API] Une limite à 10 persistait sur les profils et services ce qui limitait les résultats retournés
- [API] Le component API ne reprenait pas la bonne clé d'API (globale à l'application) dans le cas du superadmin
- Lors de la recherche par sens du flux + avec ou sans réponse, la requête était erronée
- L'océrisation via cron était bloquée dans le cas où la variable Ocerisation.active n'était pas à true mais cela ralentissait le système donc le cron se voit supprimer cette variable par souci d'efficacité
- Les actions disponibles depuis l'écran d'historique étaient mal gérées. Désormais, si on vient de l'historique, on regarde le profil présent en session pour passage à l'étape suivante. De plus, les actions ne sont affichées que si l'acteur est bien celui en cours d'action sur le flux (bancontenus.etat=1)
- Afin de pouvoir valider un flux après l'avoir édité depuis l'écran d'historique, il faut ajouter l'ID du profil réalisant l'action sinon il ne se passe rien
- L'appel sur le port 80 ne doit plus être utilisé lors de la connexion entre web-GFC et le i-Parapheur

## [3.0.8] - 2021-05-03
### Corrections
- La description de l'étape n'était plus reprise, ni affichée dans la gestion du flux
- Le paramètre prenant en compte le proxy pour les requêtes Curl n'était pas correctement défini
- La visionneuse était décalée vers le bas lors de l'accès au flux, ce qui oblige l'agent a scrollé pour voir le nom du document
- L'Océrisation ne marchait pas tout le temps, notamment pour les documents dont le nom possède des espaces et caractères spéciaux
- La redirection ne se faisait pas tout le temps lors de la génération d'un flux depuis Démarches Simplifiées ou Direct Mairie
- L'utilisateur et le rôle administrateur ne sont plus modifiables via l'API


## [3.0.7] - 2021-04-16
### Corrections
- Les flux récupérés de i-Parapheur peuvent avoir des noms avec des apostrophes ou autres caractères spéciaux, donc on les gère désormais en connaissance de cause
- Lors de la scrutation par mails, la fonction wkhtmltopdf ne se lançait pas correctement car le chemin de la librairie n'était pas le bon, cela ne marchait que lancé manuellement.
- Lors de la scrutation par mails, si le mail possède un bodyHTML présentant un charset base64 alors qu'il n'est pas encodé en base64 alors la pièce jointe reprenant le contenu du mail n'était pas générée
- Les actions possibles sur le flux, visibles depuis l'historique, ne le sont désormais que pour les admins ou les agents ayant le droit d'intervenir sur le flux (acteurs du circuit du flux) car elles apparaissaient tout le temps.
- Océrisation: le script a été corrigé afin d'être positionné en cron et exécuté la nuit car trop gourmand en performance
- LDAP: le prénom devait être encodé en UTF-8 parfois mais cela générait des doublons lors de la synchrnisation
- Lors de l'exécution par lots, l'action cakeflowExecuteSend ne reprenait pas la bonne valeur pour le profil passé en paramètre (desktopId) car on se basait sur l'URL en cours (ce qu'il n' pas lieu d'être pour le traitement par lots)
- Lors de l'export CSV de la recherche, le service et la direction n'était pas remontés et le champ objet était mal formaté pour les mails
- Lors de l'export CSV de la recherche, les dates de validation et de clôture étaient vides

## [3.0.6] - 2021-04-02
### Corrections
- Lors de la recherche, on ne prenait pas en compte les courriers dont le sous-type n'était pas encore défini
- Lorsque le i-Parapheur interrogé est inaccessible, le système est bloqué jusqu'au timeout défini. Du coup, un contrôle est ajouté pour rectifier cette erreur
- Lors de la recherche par n° de référence, la recherche "cassait" en cas d'agent connecté ne possédant pas de profil admin
- Dans les bannettes de l'environnement utilisateur, la colonne Bureau a été corrigée car elle n'affichait pas les bonnes informations
- Si l'adresse ou le nom de voie du contact/organisme possède une simple quote, (ex: rue de l'église) alors une erreur apparaît. Du coup, un sanitize::clean permet d'éviter cette erreur
- L'export CSV de la recherche ne retournait pas le nom du service, ni l'état du flux alors que les colonnes sont bien présentes
- L'API gérant les profils ne présentait pas certains champs indispensables à la modification depuis l'extérieur
- Lors de la récupération des mails, ceux dont le format n'est pas forcément en HTML ne remontaient aps correctement
- A la création du flux, si le service créateur n'est pas présent, on ne le met pas à jour, désormais on prend celui de l'initiateur agissant dessus
- Les actions de traitement du flux (validation, refus, envoi) n'apparaissaient plus dans l'historique du flux


## [3.0.5] - 2021-03-10
### Corrections
- Le bouton d'Envoi à la GRC apparaît alors que le connecteur est inactif
- Lorsqu'un agent est associé à plusieurs services, seul le premier retourné est disponible pour la variable service_email lors de la génération de la réponse
- Lors de l'édition en ligne du fichier ODT, la visionneuse n'est pas mise à jour car le contenu du fichier (désormais stocké sur le disque dans /data/workspace) n'était pas mis à jour par le webdav
- Lors d'une réponse à un flux, la date stockée en base (date réelle pour la prise en compte des retards) reprenait la date du flux d'origine ce qui posait souci en terme de cohérence (contrôle) entre la date d'entrée et la date d'enregistrement, la date d'entrée ne pouvant être postérieure à la date d'enregistrement
- La variable pour la concaténation des PDFs était par défaut en read au lieu de write
- Lors de l'appel des webservices du i-Parapheur, la vérification des certifications SSL (CURLOPT_SSL_VERIFYHOST) ne se faisait pas correctement
- Suite au stockage des fichiers sur le disque et du mode multi collectivités, il manquait le nom de la collectivité dans le lien webdav qui permet d'éditer le document
- Le script bloqueparapheur, permettant de notifier l'admin en cas de dossier inactif dans le i-Parapheur depuis + de 7 jours a été corrigé pour prendre en compte plus précisément les jours renseignés et non pas une date erronée
- Le template SQL posait souci sur la table acos car la séquence ne repartait pas après la dernière valeur utilisée (à savoir 1873) mais à 1093 ce qui entrainait des doublons d'enregistrement et cassait la script de mis eà jour des droits des nouvelles collectivités créées (update_rights)
- Au moment de la récupération des mails, si l'objet ou le nom du mail possède un " alors l'affichage est cassé, du coup on les remplace par des espaces  ou des _
- Une faille dans l'API a été corrigée. L'API se voit récupérer le token depuis la BDD sans possibilité de lecture depuis l'extérieur
- Le nom de la bannette n'était plus récupérable dans le cycle de vie du flux
- Le champ objet a été enlevé des colonnes de la bannette des flux à aiguiller car était cause d'erreur alors qu'il n'est pas affiché
- Correction sur la recherche qui était beaucoup trop lente

## [3.0.4] - 2021-02-18
### Modifications
- Lorsqu'on accède à la visionneuse, il est désormais possible de concaténer automatiquement les PDFs présents dans le flux (pièce principale + PJs) si la variable _Concatenation.Pdf_ est activée.
- Lorsqu'on sélectionne le sens du sous-type, on affiche le sous-type de référence uniquement quand le flux est sortant. De plus, le message indiquant le sens du flux dans le nom du sous-type a été enlevé.
- Suppression du champ obligatoire sur le délai de traitement d'une étape de circuit car non utilisé
- Les pré-requis se voient ajouter un contrôle sur les répertoires de stockage des documents, par collectivités, ceci afin de s'assurer que les répertoires cibles existent bien
- Dans le cycle de vie du flux, l'agent ayant réalisé le refus est désormais matérialisé par une ligne en bleu et italique
- Chaque flux refusé voit son motif de refus stocké en tant que commentaire public, ceci afin de conserver l'historique des différents refus

### Corrections
- Le formulaire d'édition des sous-types ne s'affichait pas correctement
- Lors de la délégation d'un agent depuis son propre compte, une erreur apparaît car les contrôles de contraintes étaient mal gérés depuis cet écran
- La récupération des informations des organismes et des contacts prenait trop de temps lors de l'accès au flux, du coup la recherche des informations a été améliorée
- Lorsqu'on utilise la fonctionnalité de concaténation des fichiers pdfs dans un seul et unique PDF, si un fichier autre que PDF est présent, cela bloquait le traitement


## [3.0.3] - 2021-01-27
### Modifications
- Passage en 2021 en pied de page
- Le script permettant l'import des contacts a été modifié pour être plus générique
- Les fichiers de gabarits, liés aux sous-types, se voient remplacer les espaces vides dans leur nom par des underscores
- La colonne reprenant le SIRET a été corrigé dans le script d'import des contacts
- Lors de la création d'un nouveau flux, le panneau latéral permettant d'associer un document, ne s'affiche qu'à partir de l'étape 2

### Corrections
- Le calcul des retards affiche J +18651 si le nombre dans le délai de retard du flux est laissé à vide
- Mise à jour du template SQL pour les habilitations du profil Valideur
- Si l'objet du flux présente la balise <html> en premier, on passe l'objet à null pour l'affichage des bannettes
- Le chemin permettant d'utiliser les documents à éditer via la webdav a été corrigé avec l'ajout de la connexion utilisée
- Les fichiers de gabarits, liés aux sous-types, se voient ajouter automatiquement l'extension .odt si la valeur n'est pas présente

## [3.0.2] - 2021-01-15
### Modifications
- Recherche: Le filtre sur les flux possédant ou non une réponse a été amélioré
- LDAP - lors de la rcherche dn d'un groupe, on passe le dn en encodage utf-8 pour ne pas se retrouver bloqué lors de la récupération des groupes

### Corrections
- Recherche: Le filtre sur le sens du flux a été corrigé
- Lors du traitement par lot, on recharge désormais la page car la redirection ne se fait pas
- L'ajour d'un Dossier/Affaire s'appuyait sur la table compteurs, ce qui n'est désormais plus le cas
- Lors de la sélection du profil dans la gestion des bureaux, une erreur apparaissait si la liste était vide

## [3.0.1] - 2021-01-08
### Ajouts
- L'API v2 se voit ajouter la possibilité de créer une collectivité avec sa BDD et son compte administrateur

### Modifications
- L'API v2 a été améliorée, notamment pour la création des profils utilisateurs
- Le champ objet est limité aux 90 premiers caractères pour ne pas "casser" l'affichage des bannettes
- Remplacement du terme Rôle par Bureau
- Amélioration des conditions pour les appels au I-Parapheur, notamment pour le check et ajout de la condition use_signature = true pour s'assurer que le Parapheur est actif ET utilisé
- Le script d'installation peut être lancé autant de fois que l'on souhaite, la première fois la collectivité est créée, ensuite il ne se passe rien

### Corrections
- Sous Firefox, les icônes font-awesome apparaissaient en bleue au lieu de blanc
- Dans le cas où l'utilisation du I-Parapheur est désactivée, la variable $soustype ne prend pas comme valeur $soustype['soustype']
- Lors de l'intégration des mails, suppression de la condition sur le contenu HTML du mail comme pour le cas où aucune PJ n'est présente sinon cela ne retourne pas les données correctement
- Lors de l'OCérisation, on fait en sorte que le nom du fichier ne comporte pas d'espace, que l'on remplace par des _
- Lorsque l'on clique sur le lien de l'application fourni dans les notifications par mail, si l'utilisateur n'est pas connecté, alors une erreur apparaît (page blanche en debug = 0). Désormais, on est redirigé vers la page de connexion


## [3.0] - 2020-12-11
### Ajouts
- Ajout: passage sous Serveur Ubuntu 20.04, PHP 7.4 et CakePHP 2.10.22
- Ajout: possibilité d'utiliser l'OCérisation des fichiers scannés avant intégration dans web-GFC
- Ajout: possibilité d'envoyer des mails sécurisés en passant par la plateforme PASTELL
- Ajout: les documents liés aux flux sont désormais stockés sur l'espace disque et non plus en base de données
- Ajout: interconnexion de web-GFC avec le service Démarches-simplifiées fournies par la DINUM (https://www.demarches-simplifiees.fr/)
- Ajout: interconnexion de web-GFC avec le service Direct-Mairie fournies par l'ADULLACT (https://directmairie.adullact.org)
- Ajout: possibilité d'envoyer plusieurs documents principaux (pouvant être signés) au i-Parapheur. (Ceci est en lien avec les dossiers multi documents principaux disponibles depuis la version 4.3 du i-Parapheur)
- Ajout: l'administrateur se voit désormais offrir la possiblité de "déclore" un flux, i.e., ajouter/modifier une métadonnée, un commentaire, un dossier/affaire
- Ajout: la liste des acteurs possédant le flux actuellement est ajoutée dans les bannettes de recherche (et l'export CSV) et de l'historique
- Ajout: un nouveau menu Outils est ajouté permettant à l'administrateur :
    - de retrouver les flux non clos présents chez des agents inactifs
    - de retrouver un flux non clos qui est à l'étape i-Parapheur et qui doit être ré-injecté dans le i-Parapheur (car perdu ou mal aiguillé)
    - de retrouver les flux en cours de traitement qui sont toujours dans la bannette des flux à insérer des initiateurs (alors qu'ils sont bien dans un circuit)
- Ajout: récupération du nom et du mail du service associé à la personne définie dans la liste déroulante "affaire suivie par"
- Ajout: possibilité d'envoyer des SMS depuis la gestion du flux
- Ajout: en lien avec le RGPD, ajout d'une page de politique de confidentialité, paramétrable par collectivités
- Ajout: prise en compte des ActiveDirectory en LDAPs, suite à la préconisation du pôle système et la mise en oeuvre sous Windows du LDAPs
- Ajout: de nouveaux tests unitaires sont pris en charge
- Ajout: dans l'export des types/sous-types, ajout d'une colonne reprenant le nom du circuit associé au sous-type

### Modifications
- Modification: le script d'intégration des données de la BAN (Base d'Adresse Nationale) se voit ajouter la possibilité de mettre à jour les données intégrées
- Modification: la recherche se voit ajouter le filtre "Sens du flux"
- Modification: ajout du nombre de jours de retard, ou du nombre de jours avant retard si un délai de traitement est défini, sur toutes les bannettes des agents (en bleu, le nombre de jours avant retard, en rouge le nombre de jours en retard)
- Modification: lors de la consultation d'un flux, le panneau latéral de droite est fixé à la page, ceci afin que l'on continue de pouvoir consulter le document au fur et à mesure que l'on descend dans la page.
- Modification: lors de la génération documentaire, le nom et l'e_mail repris pour le service concerné par le flux sont ceux du service associé à la personne sélectionnée dans la liste déroulante "Affaire suivie par", si cette dernière valeur est renseignée.
- Modificaion: un flux ne peut plus être inséré dans un circuit désactivé
- Modification: lors de l'envoi du document principal dans l'i-Parapheur, le numéro de référence du flux est ajouté dans le nom du dossier créé côté i-Parapheur

### Corrections
- Correction: la recherche par valeur de métadonnées n'était pas correctement définie
- Correction: lors de la synchronisation LDAP, le cas des associations bureaux/profils n'était pas prise en compte et une erreur de contrainte unique "cassait" le script de mise à jour LDAP. Les nouveaux utilisateurs n'étaient du coup pas redescendus
- Correction: [GRC Localeo] lorsque le contact doit être créé dans la GRC depuis web-GFC, on vérifie que les données sont bien présentes avant envoi vers la GRC
- Correction: lors de la notification aux utilisateurs, si parmi les profils cibles, l'un d'entre eux est inactif, alors on ne le notifie plus
- Correction: lors de l'envoi au i-Parapheur, si le document de réponse vient tout juste d'être généré, il n'est pas pris en compte par l'interface ce qui bloque l'envoi. Désormais, on stocke en session l'id du document qui vient d'être généré, si la page n'est pas rechargée avant l'envoi

## [2.2.2] - 2020-02-18
### Corrections
- Correction: Lors du ré-aiguillage, le flux ne partait pas toujours des bannettes des flux à insérer de tous les agents présents dans le même bureau.
- Correction: la variable autorisant la suppression des flux en tant qu'aiguilleur n'était aps écrite correctement
- Correction: le refus depuis le i-Parapheur ne permettait pas de remonter le flux dans web-gFC dans le cas d'un nouvel envoi au i-Parpaheur
- Correction: lors de l'ajout d'un contact, le nom de voie de l'organisme n'était pas repris si aucune BAN n'est sélectionnée
- Correction: suppression d'un die() empêchant l'envoi des notifications quotidiennes
- Correction: une transaction ne se fermait pas dans la synchronisation LDAP, empêchant la création de nouveaux utilisateurs
- Correction: lorsqu'un dossier présent dans le i-Parapheur se trouve dans un bureau inaccessible, le script "casse". Désormais, on ajoute des conditions sur les dossiers NonLu et EnCoursVisa

### Modifications
- Modification: les webservices avec la GRC Localeo ont été revus afin de mieux correspondre aux besoins attendus
- Modification: La visionneuse PDF ne permettait pas d'afficher les signatures dans les documents car une fonction de base l'empêchait.
- Modification: Le script d'intégration des contacts (issus d'un ancien outil) a été revu pour une meilleure stabilité.
- Modification: Le fichier email.php a été modifié afin de prendre en compte les infos fournies dans la partie "from" lors de l'envoi d'un mail.
- Modification: L'action d'ajout de commentaires a été renommée pour "coller" avec le reste des actions déjà en place (ajout de tâches, ajout de dossiers /affaires)
- Modification: les listes déroulantes ont été passées en "multiple" afin de pouvoir sélectionner plus facilement les différents champs de recherche
- Modification: la connexion avec le i-Parapheur se voit ajouter la  prise en compte du cachet serveur. De plus, si le document présent en document principal est un odt, alors on le convertit en PDF avant envoi au i-Parapheur
- Modification: lorsque l'agent consulte les flux dans la bannette des flux à traiter, l'action de Ré-aiguillage est enlevée (car non possible dans ce cas)

## [2.2.1] - 2019-12-30
### Corrections
- Correction: suppression d'un debug - die() qui empêchait la suppression d'une étape de circuit
- Correction: la connexion avec la GRC Localeo a été corrigée suite aux remontées de la version 2.2 (envoi du n° de référence, création de contact + organisme ...)
- Correction: report du champ adresse dans le contact "Sans contact" à la création de l'organisme
- Correction: après fusion d'un document de réponse, l'utilisateur n'était pas redirigé vers la visionneuse
- Correction: lorsque l'aiguilleur détache un flux, il est redirigé vers un autre profil (initiateur la majorité du temps)

## [2.2] - 2019-10-02
### Ajouts
- Ajout de la mise en session des données de l'agent lors de la connexion
- Ajout de la possibilité de définir **par service** un répertoire de documents scannés à scruter
- Ajout d'un connecteur direct avec la GRC Localeo développée par Docapost
- Ajout d'une icône permettant d'afficher/masquer le mot de passe de l'agent sur l'écran de connexion
- Ajout d'une variable permettant d'acter le fait que l'on masque le lien Mot de passe oublié ? dès lors qu'une collectivité est connectée à un annuaire LDAP.
- Ajout des métadonnées et de leurs valeurs lors de l'export des résultats d'une recherche
- Ajout d'une variable permettant de définir si web-GFC est utilisée en mode multi-collectivités ou non
- Ajout d'une variable permettant de définir si on peut utiliser le traitement par lots alors que les flux à traiter n'ont pas été lus (par défaut on ne peut pas)
- Ajout d'une fonction de chiffrage en base de données des mots de passe des boîtes mail paramétrées
- Ajout de la variable OBJET_FLUX dans les mails de notifications
- Ajout dans le traitement par lots de l'action "Envoyer à" afin de pouvoir ajouter par lots une étape dans un circuit

### Corrections
- Correction: les informations du contact sont désormais reprises sur tous les écrans du flux (ajout/édition/consultation)
- Correction: lors de l'envoi par l'aiguilleur vers les initiateurs, une duplication de flux s'opérait
- Correction: lors du refus d'un flux, ce denier ne disparaissait pas tout le temps des bannettes des agents ayant refusé
- Correction: lors de l'Envoi à (ajout d'une étape dans un circuit), si l'agent cible ne possède qu'un seul profil, alors une erreur apparaissait
- correction: les boutons Ajouter un gabarit et Ajouter un modèle du formulaire de sous-types ne fonctionnaient pas
- Correction: lors de la récupération des mails, des soucis d'encodage d'objet du mail ou de pièce jointes se présentaient encore
- Correction: la recherche sur les métadonnées présentait quelques soucis.
- Correction: lorsque l'on a dissocié un agent de tous les bureaux auxquels il était associé, il n'était pas possible de supprimer ses profils
- Correction: lorsque l'on supprimait un agent, on supprimait également tous les bureaux auxquels il était lié. Désormais, on ne le fait plus.

### Évolutions
- Modification: les listes déroulantes de sélection d'organisme et contact dans la gestion du flux sont remplacées par une actin de sélection via un formulaire de recherche par souci de  facilité et amélioration de performances
- Modification: les notifications affichées à la connexion sont désormais limitées à celles datant du dernier mois uniquement, afin d'améliorer les temps de traitement
- Modification: l'écran de sélection des filtres de recherche se présente sous la forme de 3 colonnes désormais
- Modification: lorsque l'on recherche un flux, s'il possède un flux réponse, alors on affiche les 2 flux. Inversement, un flux réponse se verra afficher son flux parent si on le recherche également.

## [2.1.2] - 2019-03-04
### Ajouts
- RAS

### Corrections
- Correction: il manquait le filtre "Nom de la commune" dans le formulaire de recherche par organisme
- Correction: lors de la modification du profil principal d'un agent, une erreur dans le code empêchait cette modification
- Correction: malgré la confirmation du changement de son mot de passe, à la connexion, l'agent se retrouvait en permanence redirigé vers la page de changement de mot de passe
- Correction: lors de l'envoi d'un document par mail, si ce dernier possède des caractères spéciaux dans son intitulé alors l'envoi ne se fait pas car la génération du mail est "cassée" par ces caractères spéciaux.
- Correction: l'action de création d'un nouveau flux depuis l'icône située en bas à droite de l'environnement de l'utilisateur ne fonctionnait plus

### Évolutions
- Lors de la création d'un flux, si l'agent ne possède qu'un seul profil et n'est associé qu'à un seul service, alors on ne lui propose pas le formulaire de sélection de profil et service, on le redirige directement vers le formulaire de création de flux.


## [2.1.1] - 2019-02-19
### Ajouts
- RAS

### Corrections
- Correction: le traitement par lots posait souci. Les flux étaient traités mais ne disparaissaient pas de l'environnement du valideur
- Correction : lors de l'insertion d'un flux dans un circuit, si la première étape est de type Parapheur, un souci se présentait.
- Correction: lors du "repositionnement" d'un flux, si le flux est issu d'une bannette d'aiguilleur, le flux n'était pas détaché de cette bannette
- Correction: lorsque l'on propage l'adresse de l'organisme vers ses contacts et que le champ numéro de voie présente une quote ('), une erreur d'enregistrement se produit
- Correction: lors de la modification d'un sous-type et de la suppression de son association avec son (ou ses) sous-type(s) de référence, l’enregistrement ne prenait pas en compte la suppression de l'association
- Correction: lors de la validation d'une étape collaborative (ET), la mise à jour de la table banscontenus se faisait de façon globale (pour tous les agents) au lieu de simplement valider l'action de l'agent étant intervenu à l'étape

### Évolutions
- Un export CSV est disponible pour les circuits reprenant les noms des circuits, les différentes étapes et les compositions d'étapes
- Amélioration de l'import CSV des contacts
- Le champ canton a été ajouté dans la base d'adresse nationale du CD81
- Un formulaire de recherche pour les types/sous-types a été ajouté

## [2.1] - 2018-09-30
### Ajouts
- Ajout de la synchronisation LDAP : les agents seront récupérés depuis l'annuaire et créés dans web-GFC
- Le lancement de la synchronisation LDAP peut se faire depuis la gestion des connecteurs
- Ajout de la possibilité d'associer des utilisateurs (les profils des utilisateurs) à un service lors de la création de ce dernier ou de son édition
- Ajout de la possibilité d'exporter au format CSV la liste des types/sous-types
- Ajout d'un indicateur dans le tableau de cycle de vie du flux (affichage en gras), indiquant chez quel(s) utilisateur(s) se situe le flux
- Ajout de la possibilité de détacher le flux directement depuis le formulaire du flux, si ce dernier est présent dans la bannette des flux en copie
- Ajout de la possibilité d'adresser un flux dynamiquement au i-Parapheur. Il faudra sélectionner le sous-type concerné
- Ajout des profils et services qui me sont délégués lors de la création d'un flux
- Ajout d'une console d'administration (consultation des logs + espace disque) dans l'environnement superadmin

### Corrections
- Correction: si on modifie la valeur d'une métadonnée déjà renseignée pour un flux donné, une erreur se produit car le système tente d'enregistrer plutôt que de mettre à jour. Désormais on fait un update pour pallier ce souci
- Correction: lorsque l'on modifie un type/sous-type (ajout ou déplacement), les modifications ne sont pas immédiatement impactées dans la gestion des flux, contrairement aux éléments dans la recherche. On fait donc comme pour la recherche.
- Correction: lors de l'édition d'un profil, la redirection renvoyait sur une URL erronnée
- Correction: sous Firefox, suite à une recherche, le fait de cliquer sur l'icone en forme d’œil ouvrait 2 onglets au lieu d'un seul
- Correction: lors de l'édition des habilitations d'un type, si on le décoche, les sous-types associés n'étaient pas décochés ce qui faisait que la modification n'était pas pris en compte.

### Évolutions
- Modification du footer avec remplacement des dates 2012-2017 par 2006 - 2018
- Modification: dans le cas où on désactive des types/Sous-types, si un flux est en cours de rédaction sur un type désactivé, on ne le retrouvait pas. Désormais on peut le retrouver
- Modification: lorsqu'un flux est refusé, on retourne le flux vers le bureau de l'initiateur qui a inséré le flux. Du coup, si plusieurs agents sont présents dans ce bureau, alors chacun des participants se trouvera à traiter le flux refusé
- Modification: lors de l'ajout d'un organisme, que l'on associe un contact ou non, on crée et on associe un contact de type _Sans contact_
- Modification: classement par ordre alphabétique des sous-types sélectionnables lorsque l'on clôt et répond pour un flux entrant
- Modification: suite à une recherche, si le flux est une réponse à un précédent flux, ajout de l'icône après le nom du flux, permettant de visualiser les informations du flux d'origine
- Modification: lors de la recherche par numéro de référence, il faut désormais indiquer le n° exact et complet pour que le flux soit retourné
- Modification : refonte ergonomique des Dossiers/Affaires dans la gestion du flux avec ajout de l'action d'édition d'un dossier ou une affaire

## [2.0.3] - 2018-07-31
### Ajouts
- Ajout pour l'administrateur (uniquement) de la possibilité de ré-affecter le flux à un autre agent tant qu'il n'est pas inséré dans un circuit.
- Ajout du champ Pays pour les formulaires de contacts et d'organismes
- Ajout d'une table de journalisation permettant de stocker les actions des utilisateurs. Consultation, édition, ajout, suppression sont stockées dans une table journalevents où le nom de l'utilisateur, l'action, le n° de référence, l'Id du courrier ... sont stockés
- Dans le cas où l'on refuse un flux, si on souhaite que le mail de refus ne soit adressé qu'à l'initiateur, il faudra désormais utiliser la variable Configure::write('Refus.InitiateurOnly', false); pour dire si on adresse seulement l'initiateur ou tous les agents concernés par le flux

### Corrections
- Ajout d'un nouveau patch SQL 2.0.3 qui corrige des soucis de la version 2.0.2
- Correction des filtres sur les champs _date_ qui cassaient lors du passage de page en page.
- Correction de la recherche disponible au niveau des bannettes d'historique
- Correction du bloc des commentaires qui disparaissait lorsque des -retour chariot_ étaient présents dans le texte
- Correction (SAERP) le calcul des OS entrants n'est plus pris en compte pur les marchés
- Correction lors de l'envoi d'un document (notamment l'AR) par mail depuis la partie Documents, une erreur apparaissait car la librairie XML_RPC2CLient n'était pas chargée
- Correction de la génération de l'AR
- Correction de la modification d'un organisme depuis la gestion du flux

### Évolutions
- Si un Aiguilleur possède également le profil Initiateur, à l'accès au flux, on l'autorise à insérer le flux dans le circuit lorsqu'il choisit le type/sous-type, s'il le souhaite. S'il ne possède pas de profil Initiateur, il ne peut toujours pas insérer dans le circuit
- Classement par ordre alphabétique des contacts suite à une recherche
- Seuls les administrateurs peuvent supprimer les carnets d'adresse, les organismes et les contacts
- Lorsque l'on clique sur les étoiles présentes sur chacun des intitulés de blocs présents dans la gestion du flux, on masque/affiche les informations présentes. Cela permettra aux agents de réduire les données à l'affichage
- Suite au ticket #2014830: ajout de la possibilité d'activer/désactiver un contact (+ les organismes)
- L'administrateur a désormais la possibilité de pousser un flux 'bloqué' à une étape donnée depuis la gestion du flux. Via la recherche, on trouve le flux, on le consulte, on l'édite et les actions d'insertion et/ou envoi à une étape suivante sont disponibles et effectives.
- Le nom du sous-type sélectionné côté i-Parapheur est désormais affcihé à l'étape i-Parapheur
- Dans le paramétrage des bureaux, ajout d'une colonne reprenant la liste des profils associés au bureau en question
- Amélioration du temps de chargement des informations du flux en édition avec déplacement des fonctions permettant de vérifier si les dates présentes sont bonnes

## [2.0.2] - 2017-11-28
### Corrections
- Correction du versement en GED depuis la gestion du flux
- Ajout d'un nouveau patch SQL 2.0.2 qui corrige des soucis de la version 2.0.1 (renommage de séquence et contrainte)
- Correction de l'envoi par mails des ARs
- Correction si le flux ne possède pas de type/sous-type, il ne ressortait pas dans la recherche
- Correction (SAERP) le format de la date du premier mois dans les marchés n'était pas bon
- Correction pour l'ajout d'un nouveau profil qui posait souci à cause de l'intitulé Rôle automatiquement chargé

### Évolutions
- Redimensionnement du menu du haut selon la taille 1600*900
- Modification du niveau de détection des homonymes avec abaissement du niveau à < en lieu et place de <= car il y avait des faux-positifs
- Sauvegarde automatique pour les éléments des méta-données d'un flux
- Suppression de la visualisation d'un contact car non utilisée


## [2.0.1] - 2017-10-31
### Corrections
- Correction de l'action de suppression d'un organisme
- Correction sur le champ objet de la partie commentaire "cassait" le javascript ce qui bloquait l'affichage de la liste des commentaires
- Correction pour la SAERP, les OS entrants étaient pris en compte dans le calcul du nombre d'OS pour un marché, ce qui n'est plus le cas
- Correction de la mise en place d'une délégation planifiée
- Correction de l'écran de modification d'un type/sous-type
- Correction du script d'intégration des contacts dans les carnets d'adresse
- Correction des exports CSV issus de la recherche (par commentaires, par types/sous-types, par contacts ...)

### Évolutions
- Amélioration de la gestion des erreurs lors d'une connexion au i-Parapheur
- Ajout de l'affichage du sous-type défini dans le i-Parapheur pour l'étape i-Parapheur du circuit de traitement d'un flux
- Changement des dates dans le footer par 2006-2018
- Ajout du nom de l'organisme lorsque l'on accède à la liste des contacts liés à un organisme
- Suppression de l'action _Supprimer_ pour les connecteurs d'une collectivité
- Remplacement des intitulés Clôturer/Clôturé(s) en Clore/Clos


## [2.0] - 2017-08-30
### Ajouts
- Ajout d'un outil statistiques externe complet
- Ajout d'un système d'abonnement aux notifications de façon quotidienne ou à chaque évènement
- Ajout de l'enregistrement automatique des informations dans la gestion du flux
- Ajout de la possibilité de naviguer d'un flux à un autre directement depuis la gestion d'un flux
- Ajout pour TOUS les modèles ODT d'une action d'édition (via webdav)
- Ajout de la possibilité d'activer/désactiver un service, un type et un sous-type
- Ajout du champ e-mail pour les services
- Ajout des adresses mails des contacts lors de l'envoi d'un document par mail
- Ajout de la possibilité d'adresser plusieurs documents (document principal et/ou ses pièces jointes) lors de l'envoi d'un mail

### Évolutions
- Evolution pour prendre compte la distribution Linux Ubuntu LTS 16.04 de base
- Evolution prise en charge de php en version 7
- Evolution prise en charge de postgresql en version 9.5
- Evolution de l'ergonomie de l'application avec la mise en œuvre de Bootstrap v3.0
- Evolution de la prévisualisation des documents avec un passage sous Pdf.js
- Evolution du versement en GED qui est désormais complet
- Evolution pour l'aiguilleur qui peut désormais consulter les documents scannés directement depuis son environnement
- Evolution du formulaire de recherche

## [2.0-beta2] - 2017-05-31
### Ajouts
- Début de la mise en place de la charte graphique Libriciel SCOP
- Système d'abonnement aux notifications (Résumé journalier ou à chaque événement)
- Possibilité d'envoyer plusieurs documents par mail
- Ajout d'un email pour les services de la collectivité

### Corrections
- Chargement des types et sous-types
- Le CSS et le javascript ont été changés suite à la mise ne oeuvre de la charte graphique Libriciel SCOP

### Évolutions
- Tableau de suivi du cycle de vie du flux: mise ne place d'icône pour les entêtes de colonnes
- Déplacement des chargements des listes déroulantes dans le controller plutôt que dnas la vue (organismes, contacts, types/sous-types)


## [2.0-beta] - 2017-05-02
### Ajouts
- Navigation entre les différents flux depuis la gestion d'un flux

### Corrections
- Lors de la génération de réponse, si + d'1 document webdav est utilisé, la fusion reprend les données de chacun des fichiers renseignés
- Les observations sur les contacts possédant des caractères spéciaux (`\, /, "` ...) faisaient "casser" le javascript

### Évolutions
- La visionneuse Flash est remplacée par une visionneuse sous PDF.js


## [2.0-alpha] - 2016-09-01 / 2017-05-01
### Ajouts
- Refonte totale de l'ergonomique de web-GFC



[2.0]: https://gitlab.libriciel.fr/web-GFC/webgfc
