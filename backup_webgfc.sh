#!/bin/sh
###################################################
# script de sauvegarde des bases postgreSQL 	  #
#                                                 #
# v1.0                                            #
# systeme@libriciel.coop
# FM                                              #
#                                                 #
###################################################

##############################
#Initialisation des variables#
##############################
#Date du jour
DATE=`date +%d-%m-%Y`
FULLDATE=`date +%Hh-%M--%d-%m-%Y`
#Repertoires de destination des sauvegardes
DIR_BACKUP="/var/backupsql"
BACKUP_J=$DIR_BACKUP/$DATE
ROTATEDIR=$DIR_BACKUP
#Nom de la machine
HOSTNAME=`hostname -f`
#Contact mail
CONTACT="contact@____.org"
#Rapport de backup
RAPPORT="/tmp/rapport-bkp-sql-$DATE.txt"
#bases de données
#BDDPROD=<%= node['webgfc']['database']['dbname'] %>
#USERPROD=<%= node['webgfc']['database']['user'] %>
BDDPROD='webgfc_coll'
USERPROD='webgfc'
NBR=4
HER=`pwd`
CMD="$0"
##################################

### test avant lancement du script
#if [ $UID -ne 0 ]; then
#	echo "$CMD: necessite des droits root"
#	exit 1
#fi


if [ ! -e $BACKUP_J ]
then
	mkdir -p $BACKUP_J

fi
chown -R www-data: $BACKUP_J
cd $BACKUP_J

echo "" > $RAPPORT
echo "#-----------------------------------------------------------------#" >> $RAPPORT
echo "#-----------------------BACKUP SQL -------------------------------#" >> $RAPPORT
echo "#-----------------------------------------------------------------#" >> $RAPPORT
echo "# sur $HOSTNAME le $DATE a `date +%Hh-%Mmn-%Ss` #" >> $RAPPORT
echo "" >> $RAPPORT

echo "####################################################" >> $RAPPORT
echo "#debut de sauvegarde de la base de production      #" >> $RAPPORT
echo "a `date +%Hh-%Mmn-%Ss`" >> $RAPPORT
echo "####################################################" >> $RAPPORT
echo "" >> $RAPPORT

        su  postgres -c "pg_dump --oids --no-privileges --no-owner --no-reconnect -Upostgres  $BDDPROD > $BACKUP_J/$BDDPROD.dump ";

#rotate des archives
i=`ls -c1 $ROTATEDIR|wc -l`
cd $ROTATEDIR

if [ $i -gt $NBR ]; then

        while [ $i -ne $NBR ]; do
                rm -rf `ls -rt|head -1`
                i=`ls -c1 $ROTATEDIR|wc -l`
        done
fi

#Envoi de rapport par mail#
###########################
#mail -s "Rapport de sauvegarde SQL" $CONTACT < $RAPPORT;
exit 0
